\newcommand{\etalchar}[1]{$^{#1}$}
\begin{thebibliography}{HMTdSY12}

\bibitem[AMG17]{Aleti2017}
Aldeida Aleti, I.~Moser, and Lars Grunske.
\newblock Analysing the fitness landscape of search-based software testing
  problems.
\newblock {\em Automated Software Engg.}, 24(3):603--621, September 2017.

\bibitem[CGF{\etalchar{+}}17]{CamposSSBSE2017}
Jos{\'e} Campos, Yan Ge, Gordon Fraser, Marcelo Eler, and Andrea Arcuri.
\newblock {An Empirical Evaluation of Evolutionary Algorithms for Test Suite
  Generation}.
\newblock In Tim Menzies and Justyna Petke, editors, {\em Proceedings of the
  9th International Symposium Search-Based Software Engineering (SSBSE)}, pages
  33--48. Springer International Publishing, Cham, 2017.

\bibitem[Evo]{EvoSuite}
Evosuite: Automatic test suite generation for java.
\newblock \url{http://www.evosuite.org/}.
\newblock Accessed: 2019-08-31.

\bibitem[FA11]{Fraser2011}
Gordon Fraser and Andrea Arcuri.
\newblock Evosuite: Automatic test suite generation for object-oriented
  software.
\newblock In {\em SIGSOFT/FSE 2011 - Proceedings of the 19th ACM SIGSOFT
  Symposium on Foundations of Software Engineering}, pages 416--419, September
  2011.

\bibitem[FA13]{Fraser2013}
G.~{Fraser} and A.~{Arcuri}.
\newblock Whole test suite generation.
\newblock {\em IEEE Transactions on Software Engineering}, 39(2):276--291,
  2013.

\bibitem[Gay17]{Gay2017}
Gregory Gay.
\newblock Generating effective test suites by combining coverage criteria.
\newblock In {\em Search Based Software Engineering. SSBSE 2017. Lecture Notes
  in Computer Science, vol 10452}, pages 65--82, August 2017.

\bibitem[GLM11]{jCT}
O.~Goloshchapova, M.~Lumpe, and S.~Mahmud.
\newblock jct: A java code tomograph.
\newblock In {\em 2011 26th IEEE/ACM International Conference on Automated
  Software Engineering (ASE 2011)}, pages 616--619. IEEE, 2011.

\bibitem[Het88]{Hetzel1988}
William~C. Hetzel.
\newblock {\em The Complete Guide to Software Testing}, volume 2nd ed.
\newblock QED Information Sciences, 1988.

\bibitem[HMTdSY12]{Harman2012}
Mark Harman, Phil McMinn, Jefferson Teixeira~de Souza, and Shin Yoo.
\newblock Search based software engineering: Techniques, taxonomy, tutorial.
\newblock In Bertrand Meyer and Martin Nordio, editors, {\em Empirical Software
  Engineering and Verification}, pages 1--59. Springer-Verlag, Berlin,
  Heidelberg, 2012.

\bibitem[IEE10]{IEEE2010}
IEEE.
\newblock Ieee standard classification for software anomalies.
\newblock {\em IEEE Std 1044-2009 (Revision of IEEE Std 1044-1993)}, pages
  1--23, January 2010.

\bibitem[JaC]{JaCoCo}
Jacoco java code coverage library (version: 0.8.0).
\newblock \url{https://www.eclemma.org/jacoco}.
\newblock Accessed: 2020-10-21.

\bibitem[Jus14]{Defects4J}
René Just.
\newblock defects4j.
\newblock \url{https://github.com/rjust/defects4j}, 2014.
\newblock Accessed: 2019-08-31.

\bibitem[MAT]{MATILDA}
The melbourne algorithm test instance library with data analytics (matilda).
\newblock \url{https://matilda.unimelb.edu.au/matilda/}.
\newblock Accessed: 2020-10-21.

\bibitem[{McM}11]{McMinn2011}
P.~{McMinn}.
\newblock Search-based software testing: Past, present and future.
\newblock In {\em 2011 IEEE Fourth International Conference on Software
  Testing, Verification and Validation Workshops}, pages 153--163, 2011.

\bibitem[MVBSM18]{Munoz2018}
Mario~A. Muñoz, Laura Villanova, Davaatseren Baatar, and Kate Smith-Miles.
\newblock Instance spaces for machine learning classification.
\newblock {\em Machine Learning}, 107(1):109--147, January 2018.

\bibitem[Mü20]{Muehling2020}
Sophia Mühling.
\newblock Conceptual design and implementation for a fitness landscape analysis
  of test suite augmentation problems.
\newblock {Study project}, Humboldt-Universität zu Berlin,
  Mathematisch-Naturwissenschaftliche Fakultät, Institut für Informatik,
  2020.

\bibitem[OAGS18]{Oliveira2018}
C.~{Oliveira}, A.~{Aleti}, L.~{Grunske}, and K.~{Smith-Miles}.
\newblock Mapping the effectiveness of automated test suite generation
  techniques.
\newblock {\em IEEE Transactions on Reliability}, 67(3):771--785, September
  2018.

\bibitem[Sha15]{Shamshiri2015}
Sina Shamshiri.
\newblock Automated unit test generation for evolving software.
\newblock In {\em Proceedings of the 2015 10th Joint Meeting on Foundations of
  Software Engineering}, ESEC/FSE 2015, pages 1038--1041, New York, NY, USA,
  2015. ACM.

\bibitem[Sha16]{Shamshiri2016}
Sina Shamshiri.
\newblock {\em Automated Unit Testing of Evolving Software}.
\newblock PhD thesis, The University of Sheffield, Department of Computer
  Science, Faculty of Engineering, 2016.

\bibitem[SJ11]{CKJM}
D.~Spinellis and M.~Jureczko.
\newblock Ckjm extended - an extended version of tool for calculating chidamber
  and kemerer java metrics (version: 2.2).
\newblock \url{http://gromit.iiar.pwr.wroc.pl/p_inf/ckjm/}, 2011.
\newblock last change: May 06, 2011 5:05 pm.

\bibitem[SRFM15]{shamshiri2015random}
Sina Shamshiri, Jos\'{e}~Miguel Rojas, Gordon Fraser, and Phil McMinn.
\newblock Random or genetic algorithm search for object-oriented test suite
  generation?
\newblock In {\em Genetic and Evolutionary Computation Conference (GECCO
  2015)}, pages 1367--1374. ACM, 2015.

\bibitem[SS08]{NetworkX}
D.~A. Schult and P.~Swart.
\newblock Exploring network structure, dynamics, and function using networkx.
\newblock {\em Proceedings of the 7th Python in Science Conferences (SciPy
  2008)}, 2008:11--16, 2008.

\bibitem[Vie17]{Vieregg2017}
Nicole Vieregg.
\newblock A systematic analysis of faults in the defects4j benchmark.
\newblock {B.S. Thesis}, Humboldt-Universität zu Berlin,
  Mathematisch-Naturwissenschaftliche Fakultät, Institut für Informatik,
  2017.

\bibitem[XKK{\etalchar{+}}15]{Xu2015}
Zhihong Xu, Yunho Kim, Moonzoo Kim, Myra~B. Cohen, and Gregg Rothermel.
\newblock Directed test suite augmentation: An empirical investigation.
\newblock {\em Softw. Test. Verif. Reliab.}, 25(2):77--114, March 2015.

\bibitem[Xu13]{Xu2013}
Zhihong Xu.
\newblock {\em Directed Test Suite Augmentation}.
\newblock {Doctoral dissertation}, University of Nebraska-Lincoln, May 2013.

\bibitem[YH12]{Yoo2012}
S.~Yoo and M.~Harman.
\newblock Regression testing minimization, selection and prioritization: A
  survey.
\newblock {\em Softw. Test. Verif. Reliab.}, 22(2):67--120, March 2012.

\end{thebibliography}
