\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{9}{section.1}%
\contentsline {subsection}{\numberline {1.1}Motivation}{9}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Research Questions}{10}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Outline}{10}{subsection.1.3}%
\contentsline {section}{\numberline {2}Background}{11}{section.2}%
\contentsline {subsection}{\numberline {2.1}Search-Based Software Testing (SBST) with EvoSuite}{11}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Search Based Software Engineering}{11}{subsubsection.2.1.1}%
\contentsline {paragraph}{\nonumberline Random Search}{11}{section*.6}%
\contentsline {paragraph}{\nonumberline Local searches}{12}{section*.8}%
\contentsline {paragraph}{\nonumberline Global searches: Genetic algorithms}{12}{section*.11}%
\contentsline {subsubsection}{\numberline {2.1.2}EvoSuite}{15}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Regression Testing with EvoSuiteR}{17}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Regression Testing}{17}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}EvoSuiteR: Search-based Regression Test Generation}{17}{subsubsection.2.2.2}%
\contentsline {subsection}{\numberline {2.3}Defects4J}{17}{subsection.2.3}%
\contentsline {section}{\numberline {3}Related work}{21}{section.3}%
\contentsline {subsection}{\numberline {3.1}Analyzing the fitness landscape of search-based software testing problems}{21}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Results}{21}{subsubsection.3.1.1}%
\contentsline {subsection}{\numberline {3.2}Study project: Conceptual design \& implementation for a Fitness Landscape Analysis of Test Suite Augmentation Problems}{22}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Results}{22}{subsubsection.3.2.1}%
\contentsline {subsection}{\numberline {3.3}Mapping the Effectiveness of Test Automation (META) Framework}{25}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Results}{28}{subsubsection.3.3.1}%
\contentsline {section}{\numberline {4}Approach: Mapping the Effectiveness of Automated Test Suite Augmentation}{29}{section.4}%
\contentsline {subsection}{\numberline {4.1}Conceptual design}{29}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}CUT space}{30}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}ATSGT space}{33}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Regression Suite Strategy of EvoSuiteR}{33}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Selection of ATSGTs and performance measures}{34}{subsubsection.4.3.2}%
\contentsline {subsection}{\numberline {4.4}ATSGT Effectiveness Space}{35}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Instance Space Analysis with MATILDA}{35}{subsubsection.4.4.1}%
\contentsline {section}{\numberline {5}Experiment Setup}{38}{section.5}%
\contentsline {subsection}{\numberline {5.1}Environment}{38}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Selection of Defects4J CUTs}{39}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Feature extraction with CKJM execution of Defects4J CUTs}{42}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}EvoSuiteR execution on Defects4J CUTs}{42}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Selection of Performance measures}{43}{subsection.5.5}%
\contentsline {subsubsection}{\numberline {5.5.1}Running JaCoCo on the generated test suites}{44}{subsubsection.5.5.1}%
\contentsline {subsection}{\numberline {5.6}Instance Space Analysis with MATILDA}{45}{subsection.5.6}%
\contentsline {section}{\numberline {6}Experiment implementation}{49}{section.6}%
\contentsline {section}{\numberline {7}Experiment results}{51}{section.7}%
\contentsline {subsection}{\numberline {7.1}Features}{51}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Instances}{53}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Algorithms}{56}{subsection.7.3}%
\contentsline {subsection}{\numberline {7.4}Threats to validity}{57}{subsection.7.4}%
\contentsline {section}{\numberline {8}Conclusion \& Outlook}{61}{section.8}%
\contentsline {section}{\numberline {9}Bibliography}{62}{section.9}%
\contentsline {section}{\numberline {A}Tables}{65}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}Metadata for MATILDA}{65}{subsection.A.1}%
