package org.evosuite.metric;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.evosuite.Properties;
import org.evosuite.ga.Chromosome;
import org.evosuite.testcase.TestFitnessFunction;

public class StatisticsFile<T extends Chromosome> {
	
	String title;
	
	Map<Double, Double> cr = new HashMap<Double, Double>();
	Map<Double, Double> pic = new HashMap<Double, Double>();
	Map<Double, Double> nsc = new HashMap<Double, Double>();
	
	Map<Double, Double> initialFitness = new HashMap<Double, Double>();
	Map<Double, Double> initialCR = new HashMap<Double, Double>();
	Map<Double, Double> initialPIC = new HashMap<Double, Double>();
	
	Map<Double, Double> fitness = new HashMap<Double, Double>();
	private List<List<T>> parentPopulations;
	private List<List<T>> childPopulations;
	
	String algorithm;
//	String criterions;
	int initialIterationSize;
	int iterationSize;
	int populationSize;
	double testCoverage;
	List<TestFitnessFunction> goals; //Namen und Size
//	double stateDistance;
//	double controlFlowDistance;
	long executionTime;
	double bestFitness;
	public StatisticsFile(String name) {
		this.title = name;
	}
		
	public void setAlgorithm(String name) {
		this.algorithm = name;
	}
	
	public void setIterationSize(int size) {
		this.initialIterationSize = size;
	}
	
	public void setInitialIterationSize(int size) {
		this.iterationSize = size;
	}
	
	public void setPopulationSize(int size) {
		this.populationSize = size;
	}
	
	public void setTestCoverage(double coverage) {
		this.testCoverage = coverage;
	}
	
	public void setGoals(List<TestFitnessFunction> goals) {
		this.goals = goals;
	}
//	
//	public void setStateDistance(double distance) {
//		this.stateDistance = distance;
//	}
//	
//	public void setControlFlowDistance(double distance) {
//		this.controlFlowDistance = distance;
//	}
	
	public void setFitness(double x, double y) {
		fitness.put(x, y);
	}
	
	public void setFitness(Map<Double, Double> values) {
		fitness = values;
	}
	
	public void setParentPopulations(List<T> population) {
		parentPopulations.add(population);
	}
	
	public void setChildPopulations(List<T> population) {
		childPopulations.add(population);
	}
	
	public void setExecutionTime(long time) {
		executionTime = time;
	}
	
	public void setBestFitness(double fitness) {
		bestFitness = fitness;
	}
	
	public void setCR(double x, double y) {
		cr.put(x, y);
	}
	
	public void setCR(Map<Double, Double> values) {
		cr = values;
	}
	
	public void setPIC(double x, double y) {
		pic.put(x, y);
	}
	
	public void setPIC(Map<Double, Double> values) {
		pic = values;
	}
	
	public void setNSC(double x, double y) {
		nsc.put(x, y);
	}
	
	public void setNSC(Map<Double, Double> values) {
		nsc = values;
	}
	
	
	public void setInitialFitness(double x, double y) {
		initialFitness.put(x, y);
	}
	
	public void setInitialCR(double x, double y) {
		initialCR.put(x, y);
	}
	
	public void setInitialCR(Map<Double, Double> values) {
		initialCR = values;
	}
	
	public void setInitialPIC(double x, double y) {
		initialPIC.put(x, y);
	}
	
	public void setInitialPIC(Map<Double, Double> values) {
		initialPIC = values;
	}
	
	private String getString(double i, Map<Double, Double> map) {
		if (map.containsKey(i)) {
			return String.valueOf(map.get(i));
		}
		return "null";
	}
	
	public void saveFile() {
		String eol = System.getProperty("line.separator");
		String currentTime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		String path = Properties.REPORT_DIR + File.separator + title;
		File fileInitial = new File(path+"_init.csv");
		File fileMonotonic = new File(path+"_monotonic.csv");
		File fileInfo = new File(path+"_info.csv");
		
		List<List<String>> initialRows = new ArrayList<List<String>>();
		initialRows.add(Arrays.asList("Initial Iterations", "Initial Fitness", "Initial CR", "Initial PIC"));		
		for(int i = 0; i < initialIterationSize; i++){
			List<String> row = Arrays.asList(String.valueOf(i), getString(i, initialFitness), getString(i, initialCR), getString(i, initialPIC));
			initialRows.add(row);
		}

		FileWriter csvWriterInit;
		try {
			csvWriterInit = new FileWriter(fileInitial);

			for (List<String> rowData : initialRows) {
			    csvWriterInit.append(String.join(",", rowData));
			    csvWriterInit.append("\n");
			}

			csvWriterInit.flush();
			csvWriterInit.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<List<String>> monotonicRows = new ArrayList<List<String>>();
		monotonicRows.add(Arrays.asList("Iterations", "Fitness", "CR", "PIC", "NSC"));		
		for(int i = 0; i < iterationSize; i++){
			List<String> row = Arrays.asList(""+i, getString(i, fitness), getString(i, cr), getString(i, pic), getString(i, nsc));
			monotonicRows.add(row);
		}
		
		FileWriter csvWriterMonotonic;
		try {
			csvWriterMonotonic = new FileWriter(fileMonotonic);

			for (List<String> rowData : monotonicRows) {
			    csvWriterMonotonic.append(String.join(",", rowData));
			    csvWriterMonotonic.append("\n");
			}

			csvWriterMonotonic.flush();
			csvWriterMonotonic.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		
		try (Writer writer = new FileWriter(fileInfo)) {
			writer.append("Algorithm:");
			writer.append(",");
			writer.append(algorithm);
			writer.append("\n");
			
			writer.append("Criterions:");
			writer.append(",");
			writer.append(Properties.CRITERION.toString());
			writer.append("\n");
			
			writer.append("Test Coverage:");
			writer.append(",");
			writer.append(""+testCoverage);
			writer.append("\n");
			
			writer.append("Execution Time:");
			writer.append(",");
			writer.append(""+executionTime);
			writer.append("\n");
			
			writer.append("Best Fitness:");
			writer.append(",");
			writer.append(""+bestFitness);
			writer.append("\n");
			
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

}
