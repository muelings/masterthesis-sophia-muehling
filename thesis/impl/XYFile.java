package org.evosuite.metric;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.evosuite.Properties;

public class XYFile {
	
	String title;
	
	Map<Double, Double> xyValues = new HashMap<Double, Double>();
	
	public XYFile(String name) {
		this.title = name;
	}
	
	public void setXyValues(double x, double y) {
		xyValues.put(x, y);
	}
	
	public void saveFile() {
		String eol = System.getProperty("line.separator");
		String currentTime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		String path = Properties.REPORT_DIR + File.separator + title+".csv";

		File file = new File(path);
		try (Writer writer = new FileWriter(file)) {
			for (Map.Entry<Double, Double> entry : xyValues.entrySet()) {
				writer.append(entry.getKey().toString())
					.append(',')
					.append(entry.getValue().toString())
					.append(eol);
			}
			System.out.printf("Serialized data is saved in "+path);
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

}
