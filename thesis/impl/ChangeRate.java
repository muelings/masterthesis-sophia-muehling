package org.evosuite.metric;

import java.util.ArrayList;
import java.util.List;

public class ChangeRate {
	
	private List<Integer> binaryString;
	
	public ChangeRate() {
		binaryString = new ArrayList<Integer>();
	}

	public void setBinaryString(int change) {
		this.binaryString.add(change);
	}

	public List<Integer> getBinaryString() {
		return this.binaryString;
	}

	public double getChangeRate() {
		if (this.binaryString.size() == 0) {
			return 0;
		}
		return sumBinaryString(this.binaryString)/this.binaryString.size();
	}

	private double sumBinaryString(List<Integer> list) {
		int sum = 0;
		for (int i : list) {
			sum = sum + i;
		}
		return sum;
	}
}
