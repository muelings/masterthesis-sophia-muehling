package org.evosuite.metric;

import java.util.ArrayList;
import java.util.List;

import org.evosuite.ga.Chromosome;

public class NegativeSlopeCoefficient<T extends Chromosome> {

	private List<List<T>> parentPopulations;
	private List<List<T>> childPopulations;
	
	public NegativeSlopeCoefficient() {
		parentPopulations = new ArrayList<List<T>>();
		childPopulations = new ArrayList<List<T>>();
	}
	
	public void setParentPopulations(List<T> population) {
		parentPopulations.add(population);
	}
	
	public void setChildPopulations(List<T> population) {
		childPopulations.add(population);
	}

	private List<Double> getAvgFitnesses(List<List<T>> populations) {
		List<Double> avgFitnesses = new ArrayList<Double>();
		for(List<T> e: populations) {
			avgFitnesses.add(calculateAverageFitness(e));
		}
		return avgFitnesses;
	}
	
	private double calculateAverageFitness(List<T> list) {
	  double sum = 0.0;
	  if(!list.isEmpty()) {
	    for (T element : list) {
	        sum += element.getFitness();
	    }
	    if(sum != 0) {
	    	return sum / list.size();
	    }	    
	  }
	  return sum;
	}
	
	public double getNegativeSlopeCoefficient() {
		double nsc = 0.0;
		List<Double> parentAvgFitnesses = getAvgFitnesses(parentPopulations);
		List<Double> childAvgFitnesses = getAvgFitnesses(childPopulations);
		for (int i = 0; i < parentAvgFitnesses.size()-1; i++) {
			double probability = (childAvgFitnesses.get(i + 1) - childAvgFitnesses.get(i)) / (parentAvgFitnesses.get(i + 1) - parentAvgFitnesses.get(i));
			nsc += Math.min(probability, 0);
		}
		return nsc;
	}

}
