package org.evosuite.metric;

import java.util.ArrayList;
import java.util.List;

public class PopulationInformationContent {
	
	private List<String> binaryString;

	public PopulationInformationContent() {
		binaryString = new ArrayList<String>();
	}
	
	public void setBinaryString(String change) {
		this.binaryString.add(change);
	}

	public List<String> getBinaryString() {
		return this.binaryString;
	}

	private List<String> getBinarySubstringList() {
		List<String> binarySubstringList = new ArrayList<String>();	
		for (int i=0; i < this.binaryString.size()-1; i++) {
			binarySubstringList.add(this.binaryString.get(i) + this.binaryString.get(i+1));			
		}
		return binarySubstringList;
	}

	public double getPopulationInformationContent() {
		if(log2(substringProbability("01")) == Double.NEGATIVE_INFINITY || log2(substringProbability("10")) ==  Double.NEGATIVE_INFINITY){
			return Double.NEGATIVE_INFINITY;
		}
		return -(substringProbability("01") * log2(substringProbability("01"))) - (substringProbability("10") * log2(substringProbability("10")));
	}

	private double substringProbability(String substring) {
		double count = 0.0;
		final List<String> binarySubstringList = getBinarySubstringList();
		for (String i : binarySubstringList) {
			if (i.equals(substring)) {
				count++;
			}
		}
		if(count == 0) {
			return 0;
		}
		return count / binarySubstringList.size();
	}

	private static double log2(double num)
	{
		if (num == 0) {
			return Double.NEGATIVE_INFINITY;
		}
		return (Math.log(num)/Math.log(2));
	}

}
