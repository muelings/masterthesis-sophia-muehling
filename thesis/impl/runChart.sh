#!/bin/sh

evorun(){
	x=$1
	algorithm=$2
	budget=$3
	projectname=$4
	classname=$5
	dirname=$6
	rm -r ../../../plots/$dirname/$algorithm/$budget/$x
	mkdir ../../../plots/$dirname/
	mkdir ../../../plots/$dirname/$algorithm/
	mkdir ../../../plots/$dirname/$algorithm/$budget/
	mkdir ../../../plots/$dirname/$algorithm/$budget/$x
	java -jar evosuite-master-1.0.7-SNAPSHOT.jar -regressionSuite -projectCP "../resources/defects4j/"$projectname"_fixed/build" -Dregressioncp="../resources/defects4j/"$projectname"_buggy/build" -Dregression_fitness=$algorithm -Dsearch_budget=$budget -class $classname -Dconfiguration_id=regressionSuite -Doutput_variables=TARGET_CLASS,regression_fitness,search_budget,Total_Time,Coverage,Size,Length,Fitness,CoverageTimeline,FitnessTimeline -Dtimeline_interval=1000 &> ../../../plots/$dirname/$algorithm/$budget/$x/log.txt
	mv evosuite-tests ../../../plots/$dirname/$algorithm/$budget/$x &>> ../../../plots/$dirname/$algorithm/$budget/$x/log.txt  
	cp -avr evosuite-report ../../../plots/$dirname/$algorithm/$budget/$x &>> ../../../plots/$dirname/$algorithm/$budget/$x/log.txt  
}

evo5timesrun(){
	project=$1
	class=$2
	name=$3
	for i in {1..5}
	do
		evorun $i RANDOM 600 $project $class $name
		evorun $i RANDOM 300 $project $class $name
		evorun $i RANDOM 60 $project $class $name
		evorun $i ALL_MEASURES 600 $project $class $name
		evorun $i ALL_MEASURES 300 $project $class $name
		evorun $i ALL_MEASURES 60 $project $class $name
	done;
}

evo5timesrun chart_1 org.jfree.chart.renderer.category.AbstractCategoryItemRenderer chart_1
evo5timesrun chart_2 org.jfree.data.general.DatasetUtilities chart_2
evo5timesrun chart_3 org.jfree.data.time.TimeSeries chart_3
evo5timesrun chart_4 org.jfree.chart.plot.XYPlot chart_4
evo5timesrun chart_5 org.jfree.data.xy.XYSeries chart_5

rm -rf evosuite-report