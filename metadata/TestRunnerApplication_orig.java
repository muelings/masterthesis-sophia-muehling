import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IBundleCoverage;
import org.jacoco.core.analysis.ICoverageNode;
import org.jacoco.core.tools.ExecFileLoader;
import org.jacoco.report.DirectorySourceFileLocator;
import org.jacoco.report.FileMultiReportOutput;
import org.jacoco.report.IReportVisitor;
import org.jacoco.report.csv.CSVFormatter;
import org.jacoco.report.html.HTMLFormatter;
import org.jacoco.report.xml.XMLFormatter;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
//import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestRunnerApplication {

    public static void main(String[] args) {

        // SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

        JUnitCore jUnitCore = new JUnitCore();
        try {
            URL testClassesUrl = new URL("file:///home/tom/dev/teaching/semesterproject18_19/subjects/geojson-jackson/target/test-classes/");
            URL classesUrl = new URL(("file:///home/tom/dev/teaching/semesterproject18_19/subjects/geojson-jackson/target/classes/"));

            ClassLoader classLoader = new URLClassLoader(new URL[]{testClassesUrl, classesUrl});
            Class cls = classLoader.loadClass("org.geojson.FeatureTest");
            //Class cls = classLoader.loadClass("org.geojson.GeoJsonObjectVisitorTest");

            //jUnitCore.addListener(new TextListener(System.out));

            Result result = jUnitCore.run(cls);

            System.out.println("Finished. Result: Failures: " +
                    result.getFailureCount() + ". Ignored: " +
                    result.getIgnoreCount() + ". Tests run: " +
                    result.getRunCount() + ". Time: " +
                    result.getRunTime() + "ms.");

            // load coverage info

            String projectFolder = "geojson-jackson";
            File projectDirectory = new File("/home/tom/dev/teaching/semesterproject18_19/subjects/" + projectFolder);
            File reportDirectory = new File(projectDirectory, "coveragereport");
            File classesDirectory = new File(projectDirectory, "target/classes");
            File sourceDirectory = new File(projectDirectory, "main/java");

            // Read the jacoco.exec file. Multiple data files could be merged
            // at this point
            File executionDataFile = new File(projectDirectory, "jacoco.exec");
            ExecFileLoader execFileLoader = new ExecFileLoader();
            execFileLoader.load(executionDataFile);

            // Run the structure analyzer on a single class folder to build up
            // the coverage model. The process would be similar if your classes
            // were in a jar file. Typically you would create a bundle for each
            // class folder and each jar you want in your report. If you have
            // more than one bundle you will need to add a grouping node to your
            // report
            final CoverageBuilder coverageBuilder = new CoverageBuilder();
            final Analyzer analyzer = new Analyzer(
                    execFileLoader.getExecutionDataStore(), coverageBuilder);

            analyzer.analyzeAll(classesDirectory);

            final IBundleCoverage bundleCoverage = coverageBuilder.getBundle(projectFolder);


            // Create a concrete report visitor based on some supplied
            // configuration. In this case we use the defaults
            String outputEncoding = "UTF-8";
            final List<IReportVisitor> visitors = new ArrayList<IReportVisitor>();

            final XMLFormatter xmlFormatter = new XMLFormatter();
            xmlFormatter.setOutputEncoding(outputEncoding);
            visitors.add(xmlFormatter.createVisitor(new FileOutputStream(new File(
                    reportDirectory, "jacoco.xml"))));

            final CSVFormatter formatter = new CSVFormatter();
            formatter.setOutputEncoding(outputEncoding);
            visitors.add(formatter.createVisitor(new FileOutputStream(new File(
                    reportDirectory, "jacoco.csv"))));


            final HTMLFormatter htmlFormatter = new HTMLFormatter();
            visitors.add(htmlFormatter
                    .createVisitor(new FileMultiReportOutput(reportDirectory)));


            for (IReportVisitor visitor : visitors) {
                // Initialize the report with all of the execution and session
                // information. At this point the report doesn't know about the
                // structure of the report being created
                visitor.visitInfo(execFileLoader.getSessionInfoStore().getInfos(),
                        execFileLoader.getExecutionDataStore().getContents());

                // Populate the report structure with the bundle coverage information.
                // Call visitGroup if you need groups in your report.
                checkForMissingDebugInformation(bundleCoverage);

                visitor.visitBundle(bundleCoverage, new DirectorySourceFileLocator(
                        sourceDirectory, "utf-8", 4));

                // Signal end of structure information to allow report to write all
                // information out
                visitor.visitEnd();
            }

//            Session session = sessionFactory.openSession();
//
//            session.beginTransaction();
//            TestRun testRun = new TestRun();
//            testRun.setFailedTests(result.getFailureCount());
//            testRun.setRunTime(result.getRunTime());
//            testRun.setSuccessTests(result.getRunCount() - result.getFailureCount());
//            testRun.setTestName("FeatureTest");
//            session.save(testRun);
//
//            session.getTransaction().commit();

            //TestRun testRun = new TestRun(result);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void checkForMissingDebugInformation(final ICoverageNode node) {
        if (node.getClassCounter().getTotalCount() > 0
                && node.getLineCounter().getTotalCount() == 0) {
            System.out.println(
                    "To enable source code annotation class files have to be compiled with debug information.");
        }
    }

}
