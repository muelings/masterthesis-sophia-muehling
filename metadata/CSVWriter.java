import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class CSVWriter {

	private final static String CSV_FILE_NAME = "metadata.csv";
	
	private static List<Metadata> metaDataList = new ArrayList<>();
	public static boolean append = true;
	public static ArrayList<String> statisticsList = new ArrayList<String>();
	/**
	 *  Reads file.xml (args[0]) from CKJM and statistics.csv (args[1]) from EvoSuiteR and writes metadata.csv for MATILDA.
	 */
	public static void main(String args[]) {
		try {
			File xmlFile = new File(args[0]);
			String csvFile = args[0]; // z.B.: "/Users/mkyong/csv/country.csv";
			readXML(xmlFile);
			readCsv(csvFile);
			List<String[]> dataLines = new ArrayList<>();
			
			dataLines.add(new String[] {
				"Instances",
				"feature_WMC","feature_DIT","feature_CBO","feature_RFC","feature_LCOM","feature_Ce","feature_NPM","feature_LCOM3","feature_LOC","feature_DAM","feature_MOA","feature_MFA","feature_CAM","feature_IC","feature_CBM","feature_AMC","feature_CC",
				"feature_Diff_WMC","feature_Diff_CBO","feature_Diff_RFC","feature_Diff_LCOM","feature_Diff_Ce","feature_Diff_NPM","feature_Diff_LCOM3","feature_Diff_LOC","feature_Diff_DAM","feature_Diff_MOA","feature_Diff_MFA","feature_Diff_CAM","feature_Diff_IC","feature_Diff_CBM","feature_Diff_AMC","feature_Diff_CC",
				"algo_genAlg_coverageMean","algo_genAlg_coverageMedian","algo_genAlg_coverageStd",
				"algo_GenAlg_sizeMean","algo_GenAlg_sizeMedian","algo_GenAlg_sizeStd","algo_GenAlg_lengthMean","algo_GenAlg_lengthMedian","algo_GenAlg_lengthStd",
				"algo_Default_sizeMean","algo_Default_sizeMedian","algo_Default_sizeStd","algo_Default_lengthMean","algo_Default_lengthMedian","algo_Default_lengthStd"
			});
			metaDataList.stream().forEach((element) -> dataLines.add(new String[] {
					element.getClassName(),
					element.getWmc(),element.getDit(),element.getCbo(),element.getRfc(),element.getLcom(),element.getCe(),element.getNpm(),element.getLcom3(),element.getLoc(),element.getDam(),element.getMoa(),element.getMfa(),element.getCam(),element.getIc(),element.getCbm(),element.getAmc(),element.getCc(),
					element.getWmcDiff(),element.getDitDiff(),element.getCboDiff(),element.getRfcDiff(),element.getLcomDiff(),element.getCeDiff(),element.getNpmDiff(),element.getLcom3Diff(),element.getLocDiff(),element.getDamDiff(),element.getMoaDiff(),element.getMfaDiff(),element.getCamDiff(),element.getIcDiff(),element.getCbmDiff(),element.getAmcDiff(),element.getCcDiff(),
					element.getGenAlgCoverageMean(),element.getGenAlgCoverageMed(),element.getGenAlgCoverageStd(),
					element.getGenAlgSizeMean(),element.getGenAlgSizeMed(),element.getGenAlgSizeStd(),element.getGenAlgLengthMean(),element.getGenAlgLengthMed(),element.getGenAlgLengthStd(),
					element.getDefaultSizeMean(),element.getDefaultSizeMed(),element.getDefaultSizeStd(),element.getDefaultLengthMean(),element.getDefaultLengthMed(),element.getDefaultLengthStd(),
				}
			));
			
			File csvOutputFile = new File(CSV_FILE_NAME);
		    try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
		        dataLines.stream()
		          .map(CSVWriter::convertToCSV)
		          .forEach(pw::println);
		    }
		    
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String convertToCSV(String[] data) {
	    return Stream.of(data)
	      .map(CSVWriter::escapeSpecialCharacters)
	      .collect(Collectors.joining(";"));
	}
	
	public static String escapeSpecialCharacters(String data) {
	    String escapedData = data.replaceAll("\\R", " ");
	    if (data.contains(",") || data.contains("\"") || data.contains("'")) {
	        data = data.replace("\"", "\"\"");
	        escapedData = "\"" + data + "\"";
	    }
	    return escapedData;
	}
	
	public static void readXML(File fXmlFile) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("class");
        for (int temp = 0; temp < nList.getLength(); temp++) {
        	Metadata metaElement = new Metadata();
        	Node nNode = nList.item(temp);
        	if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		        Element eElement = (Element) nNode; 
		        metaElement.setClassName(eElement.getElementsByTagName("name").item(0).getTextContent());
		        metaElement.setWmc(eElement.getElementsByTagName("wmc").item(0).getTextContent());
		        metaElement.setDit(eElement.getElementsByTagName("dit").item(0).getTextContent());
		        metaElement.setNoc(eElement.getElementsByTagName("noc").item(0).getTextContent());
		        metaElement.setCbo(eElement.getElementsByTagName("cbo").item(0).getTextContent());
		        metaElement.setRfc(eElement.getElementsByTagName("rfc").item(0).getTextContent());
		        metaElement.setLcom(eElement.getElementsByTagName("lcom").item(0).getTextContent());
		        metaElement.setCa(eElement.getElementsByTagName("ca").item(0).getTextContent());
		        metaElement.setCe(eElement.getElementsByTagName("ce").item(0).getTextContent());
		        metaElement.setNpm(eElement.getElementsByTagName("npm").item(0).getTextContent());
		        metaElement.setLcom3(eElement.getElementsByTagName("lcom3").item(0).getTextContent());
		        metaElement.setLoc(eElement.getElementsByTagName("loc").item(0).getTextContent());
		        metaElement.setDam(eElement.getElementsByTagName("dam").item(0).getTextContent());
		        metaElement.setMoa(eElement.getElementsByTagName("moa").item(0).getTextContent());
		        metaElement.setMfa(eElement.getElementsByTagName("mfa").item(0).getTextContent());
		        metaElement.setCam(eElement.getElementsByTagName("cam").item(0).getTextContent());
		        metaElement.setIc(eElement.getElementsByTagName("ic").item(0).getTextContent());
		        metaElement.setCbm(eElement.getElementsByTagName("cbm").item(0).getTextContent());
		        metaElement.setAmc(eElement.getElementsByTagName("amc").item(0).getTextContent());
		        
		        NodeList ccList = eElement.getElementsByTagName("cc");
		        double ccSum = 0;
		        for (int i = 0; i < ccList.getLength(); i++) {
		        	Node ccNode = nList.item(i);
		        	if (ccNode.getNodeType() == Node.ELEMENT_NODE) {
				        Element ccElement = (Element) ccNode; 
				        ccSum += Integer.parseInt(ccElement.getElementsByTagName("method").item(0).getTextContent());
		        	}
		        }
		        metaElement.setCc(String.valueOf(ccSum / ccList.getLength()));
        	}
        	metaDataList.add(metaElement);
        }
        
        for (int i = 1; i < metaDataList.size(); i++) {
        	if(i % 2 != 0) {
        		Metadata element1 = metaDataList.get(i-1);
        		Metadata element2 = metaDataList.get(i);
		        element1.setWmc(String.valueOf(Math.abs(Double.valueOf(element1.getWmc())-Double.valueOf(element2.getWmc()))));
		        element1.setDit(String.valueOf(Math.abs(Double.valueOf(element1.getDit())-Double.valueOf(element2.getDit()))));
		        element1.setNoc(String.valueOf(Math.abs(Double.valueOf(element1.getNoc())-Double.valueOf(element2.getNoc()))));
		        element1.setCbo(String.valueOf(Math.abs(Double.valueOf(element1.getCbo())-Double.valueOf(element2.getCbo()))));
		        element1.setRfc(String.valueOf(Math.abs(Double.valueOf(element1.getRfc())-Double.valueOf(element2.getRfc()))));
		        element1.setLcom(String.valueOf(Math.abs(Double.valueOf(element1.getLcom())-Double.valueOf(element2.getLcom()))));
		        element1.setCa(String.valueOf(Math.abs(Double.valueOf(element1.getCa())-Double.valueOf(element2.getCa()))));
		        element1.setCe(String.valueOf(Math.abs(Double.valueOf(element1.getCe())-Double.valueOf(element2.getCe()))));
		        element1.setNpm(String.valueOf(Math.abs(Double.valueOf(element1.getNpm())-Double.valueOf(element2.getNpm()))));
		        element1.setLcom3(String.valueOf(Math.abs(Double.valueOf(element1.getLcom3())-Double.valueOf(element2.getLcom3()))));
		        element1.setLoc(String.valueOf(Math.abs(Double.valueOf(element1.getLoc())-Double.valueOf(element2.getLoc()))));
		        element1.setDam(String.valueOf(Math.abs(Double.valueOf(element1.getDam())-Double.valueOf(element2.getDam()))));
		        element1.setMoa(String.valueOf(Math.abs(Double.valueOf(element1.getMoa())-Double.valueOf(element2.getMoa()))));
		        element1.setMfa(String.valueOf(Math.abs(Double.valueOf(element1.getMfa())-Double.valueOf(element2.getMfa()))));
		        element1.setCam(String.valueOf(Math.abs(Double.valueOf(element1.getCam())-Double.valueOf(element2.getCam()))));
		        element1.setIc(String.valueOf(Math.abs(Double.valueOf(element1.getIc())-Double.valueOf(element2.getIc()))));
		        element1.setCbm(String.valueOf(Math.abs(Double.valueOf(element1.getCbm())-Double.valueOf(element2.getCbm()))));
		        element1.setAmc(String.valueOf(Math.abs(Double.valueOf(element1.getAmc())-Double.valueOf(element2.getAmc()))));
		        element1.setCc(String.valueOf(Math.abs(Double.valueOf(element1.getCc())-Double.valueOf(element2.getCc()))));
        	}
        }
        
        for(int i = (metaDataList.size()&~1)-1; i>=0; i-=2) {
        	metaDataList.remove(i);
        }
	}

	
	public static void readCsv(String csvFile) throws IOException {
	    readAllLinesFromFile(csvFile);
	    convertStatistics(statisticsList);
	}

	public static ArrayList<String> readAllLinesFromFile(String path) throws IOException{
	    FileReader fileReader = new FileReader(path);
	    BufferedReader bufferedReader = new BufferedReader(fileReader);
	    String line = null;
	    while( (line = bufferedReader.readLine()) != null){
	        statisticsList.add(line);
	    }
	    bufferedReader.close();
	    return statisticsList;
	}

	public static void convertStatistics(ArrayList<String> statisticsStrings) {
	    ArrayList<Statistics> statistics = new ArrayList<>();
	    statisticsStrings.remove(0);
	    for(String statisticstring : statisticsStrings) {
	        String[] parts = statisticstring.split(",");
	        String className = parts[0];
	        String regressionFitness = parts[1];
	        String size = parts[4];
		    String length = parts[5];
		    double cov = Double.valueOf(parts[6]);
		    String coverage = "0";
		    if(cov == 0) {
		    	coverage = parts[307];
		    }
	        statistics.add(new Statistics(className, regressionFitness, size, length, coverage));
	    }
	            
        //Compare by className and then regressionFitness
        Comparator<Statistics> compareByNameAndRegressionFitness = Comparator
                                                .comparing(Statistics::getClassName)
                                                .thenComparing(Statistics::getRegressionFitness);
         
        List<Statistics> sortedStatistics = statistics.stream()
                                        .sorted(compareByNameAndRegressionFitness)
                                        .collect(Collectors.toList());
        
        String classID = sortedStatistics.get(0).getClassName();
        String strategy = sortedStatistics.get(0).getRegressionFitness();
        int[] sizes = {0,0,0,0,0,0,0,0,0,0};
        int[] lengths = {0,0,0,0,0,0,0,0,0,0};
        double[] coverages = {0,0,0,0,0,0,0,0,0,0};
        int count = 0;
        Metadata metaElement = new Metadata();
        for(Statistics stat : sortedStatistics){
        	if (!classID.equals(stat.getClassName()) || !strategy.contentEquals(stat.getRegressionFitness()) || stat == sortedStatistics.get(sortedStatistics.size())){
        		metaElement.setClassName(classID);
        		double sizeMean = Arrays.stream(sizes).average().orElse(Double.NaN);
        		double sizeMed = getMedian(copyFromIntArray(sizes));
        		double sizeStd = getDeviation(copyFromIntArray(sizes));
        		double lengthMean = Arrays.stream(lengths).average().orElse(Double.NaN);
        		double lengthMed = getMedian(copyFromIntArray(lengths));
        		double lengthStd = getDeviation(copyFromIntArray(sizes));
        		double coverageMean = Arrays.stream(coverages).average().orElse(Double.NaN);
        		double coverageMed = getMedian(coverages);
        		double coverageStd = getDeviation(coverages);
        		
        		if (strategy.equals("ALL_MEASURES")) {
        			metaElement.setGenAlgSizeMean(String.valueOf(sizeMean));
        			metaElement.setGenAlgSizeMed(String.valueOf(sizeMed));
        			metaElement.setGenAlgSizeStd(String.valueOf(sizeStd));
        			metaElement.setGenAlgLengthMean(String.valueOf(lengthMean));
        			metaElement.setGenAlgLengthMed(String.valueOf(lengthMed));
        			metaElement.setGenAlgLengthStd(String.valueOf(lengthStd));
        			metaElement.setGenAlgCoverageMean(String.valueOf(coverageMean));
        			metaElement.setGenAlgCoverageMed(String.valueOf(coverageMed));
        			metaElement.setGenAlgCoverageStd(String.valueOf(coverageStd));
        		} else if (strategy.equals("RANDOM")) {
        			metaElement.setDefaultSizeMean(String.valueOf(sizeMean));
        			metaElement.setDefaultSizeMed(String.valueOf(sizeMed));
        			metaElement.setDefaultSizeStd(String.valueOf(sizeStd));
        			metaElement.setDefaultLengthMean(String.valueOf(lengthMean));
        			metaElement.setDefaultLengthMed(String.valueOf(lengthMed));
        			metaElement.setDefaultLengthStd(String.valueOf(lengthStd));
        		}    		
        		classID = stat.getClassName();
        		strategy = stat.getRegressionFitness();
        		sizes = new int[]{0,0,0,0,0,0,0,0,0,0};
                lengths = new int[]{0,0,0,0,0,0,0,0,0,0};
                coverages = new double[]{0,0,0,0,0,0,0,0,0,0};
                count = 0;
                if(!classID.equals(stat.getClassName())) {
                	metaElement = new Metadata();
                	metaDataList.add(metaElement);
        		}
        	} 
        	metaDataList.add(metaElement);
        	
        	sizes[count]=Integer.valueOf(stat.getSize());
        	lengths[count]=Integer.valueOf(stat.getLength());
        	coverages[count]=Double.valueOf(stat.getCoverage());
	        System.out.println(stat.toString());
	    }
        
        Comparator<Metadata> compareByName = Comparator
                .comparing(Metadata::getClassName);
        
        metaDataList = metaDataList.stream().sorted(compareByName).collect(Collectors.toList());
        
        for (int i = 1; i < metaDataList.size(); i++) {
        	if(i % 2 != 0) {
        		Metadata element1 = metaDataList.get(i-1);
        		Metadata element2 = metaDataList.get(i);
        		if(element1.getClassName().equals(element2.getClassName())) {
        			element1.setGenAlgSizeMean(element2.getGenAlgSizeMean());
        			element1.setGenAlgSizeMed(element2.getGenAlgSizeMed());
        			element1.setGenAlgSizeStd(element2.getGenAlgSizeStd());
        			element1.setGenAlgLengthMean(element2.getGenAlgLengthMean());
        			element1.setGenAlgLengthMed(element2.getGenAlgLengthMed());
        			element1.setGenAlgLengthStd(element2.getGenAlgLengthStd());
        			element1.setGenAlgCoverageMean(element2.getGenAlgCoverageMean());
        			element1.setGenAlgCoverageMed(element2.getGenAlgCoverageMed());
        			element1.setGenAlgCoverageStd(element2.getGenAlgCoverageStd());
        			element1.setDefaultSizeMean(element2.getDefaultSizeMean());
        			element1.setDefaultSizeMed(element2.getDefaultSizeMed());
        			element1.setDefaultSizeStd(element2.getDefaultSizeStd());
        			element1.setDefaultLengthMean(element2.getDefaultLengthMean());
        			element1.setDefaultLengthMed(element2.getDefaultLengthMed());
        			element1.setDefaultLengthStd(element2.getDefaultLengthStd());
        		}        		
        	}
        }
        
        for(int i = (metaDataList.size()&~1)-1; i>=0; i-=2) {
        	metaDataList.remove(i);
        }
	}
	
	private static double getMedian(double[] numArray) {
		Arrays.sort(numArray);
		double median;
		if (numArray.length % 2 == 0) {
			median = ((double)numArray[numArray.length/2] + (double)numArray[numArray.length/2 - 1])/2;
		} else {
			median = (double) numArray[numArray.length/2];
		}
		return median;    
	}
	
	private static double getDeviation(double[] nums) {
		double mean = Arrays.stream(nums).average().orElse(Double.NaN);
		double squareSum = 0;
		for (int i = 0; i < nums.length; i++) { 
			squareSum += Math.pow(nums[i] - mean, 2);
		}
		return Math.sqrt((squareSum)/(nums.length - 1)); 
     }
	
	private static double[] copyFromIntArray(int[] source) {
	    double[] dest = new double[source.length];
	    for(int i=0; i<source.length; i++) {
	        dest[i] = source[i];
	    }
	    return dest;
	}
	
	public static void writeAllLinesToFile(String path, ArrayList<String> statisticsList) throws IOException {
	    FileWriter fileWriter = new FileWriter(path, append);
	    PrintWriter printWriter = new PrintWriter(fileWriter);
	    for (String line : statisticsList){
	        printWriter.printf("%s" + "%n", line);
	    }
	    printWriter.close();
	}

}
