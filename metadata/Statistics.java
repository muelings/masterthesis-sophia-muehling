
public class Statistics {
	
	private String className, regressionFitness, size, length, coverage;
	
	public Statistics(String className, String regressionFitness, String size, String length, String coverage) {
	    super();
	    this.setClassName(className);
	    this.setRegressionFitness(regressionFitness);
	    this.setSize(size);
	    this.setLength(length);
	    this.setCoverage(coverage);
	}

	public String getClassName() {
		return className;
	}
	
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	
	public String getRegressionFitness() {
		return regressionFitness;
	}
	
	
	public void setRegressionFitness(String regressionFitness) {
		this.regressionFitness = regressionFitness;
	}
	
	
	public String getSize() {
		return size;
	}
	
	
	public void setSize(String size) {
		this.size = size;
	}
	
	
	public String getLength() {
		return length;
	}
	
	
	public void setLength(String length) {
		this.length = length;
	}
	
	
	public String getCoverage() {
		return coverage;
	}
	
	
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

}