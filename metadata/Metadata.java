
public class Metadata {
	
	private String className;
	
	// CKJM data
	private String wmc;
	private String dit;
	private String noc;
	private String cbo;
	private String rfc;
    private String lcom;
    private String ca;
    private String ce;
    private String npm;
    private String lcom3;
    private String loc;
    private String dam;
    private String moa;
    private String mfa;
    private String cam;
    private String ic;
    private String cbm;
    private String amc;
    private String cc;
    
    private String wmcDiff;
	private String ditDiff;
	private String nocDiff;
	private String cboDiff;
	private String rfcDiff;
    private String lcomDiff;
    private String caDiff;
    private String ceDiff;
    private String npmDiff;
    private String lcom3Diff;
    private String locDiff;
    private String damDiff;
    private String moaDiff;
    private String mfaDiff;
    private String camDiff;
    private String icDiff;
    private String cbmDiff;
    private String amcDiff;
    private String ccDiff;
    
    
    //EvoSuiteR algorithm performance
    
    private String genAlgCoverageMean;
    private String genAlgCoverageMed;
    private String genAlgCoverageStd;
    
    private String genAlgSizeMean;
    private String genAlgSizeMed;
    private String genAlgSizeStd;
    private String genAlgLengthMean;
    private String genAlgLengthMed;
    private String genAlgLengthStd;
    
    private String defaultSizeMean;
    private String defaultSizeMed;
    private String defaultSizeStd;
    private String defaultLengthMean;
    private String defaultLengthMed;
    private String defaultLengthStd;
    
    
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getWmc() {
		return wmc;
	}
	public void setWmc(String wmc) {
		this.wmc = wmc;
	}
	public String getDit() {
		return dit;
	}
	public void setDit(String dit) {
		this.dit = dit;
	}
	public String getNoc() {
		return noc;
	}
	public void setNoc(String noc) {
		this.noc = noc;
	}
	public String getCbo() {
		return cbo;
	}
	public void setCbo(String cbo) {
		this.cbo = cbo;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getLcom() {
		return lcom;
	}
	public void setLcom(String lcom) {
		this.lcom = lcom;
	}
	public String getCa() {
		return ca;
	}
	public void setCa(String ca) {
		this.ca = ca;
	}
	public String getCe() {
		return ce;
	}
	public void setCe(String ce) {
		this.ce = ce;
	}
	public String getNpm() {
		return npm;
	}
	public void setNpm(String npm) {
		this.npm = npm;
	}
	public String getLcom3() {
		return lcom3;
	}
	public void setLcom3(String lcom3) {
		this.lcom3 = lcom3;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getDam() {
		return dam;
	}
	public void setDam(String dam) {
		this.dam = dam;
	}
	public String getMoa() {
		return moa;
	}
	public void setMoa(String moa) {
		this.moa = moa;
	}
	public String getMfa() {
		return mfa;
	}
	public void setMfa(String mfa) {
		this.mfa = mfa;
	}
	public String getCam() {
		return cam;
	}
	public void setCam(String cam) {
		this.cam = cam;
	}
	public String getIc() {
		return ic;
	}
	public void setIc(String ic) {
		this.ic = ic;
	}
	public String getCbm() {
		return cbm;
	}
	public void setCbm(String cbm) {
		this.cbm = cbm;
	}
	public String getAmc() {
		return amc;
	}
	public void setAmc(String amc) {
		this.amc = amc;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getWmcDiff() {
		return wmcDiff;
	}
	public void setWmcDiff(String wmcDiff) {
		this.wmcDiff = wmcDiff;
	}
	public String getDitDiff() {
		return ditDiff;
	}
	public void setDitDiff(String ditDiff) {
		this.ditDiff = ditDiff;
	}
	public String getNocDiff() {
		return nocDiff;
	}
	public void setNocDiff(String nocDiff) {
		this.nocDiff = nocDiff;
	}
	public String getCboDiff() {
		return cboDiff;
	}
	public void setCboDiff(String cboDiff) {
		this.cboDiff = cboDiff;
	}
	public String getLcomDiff() {
		return lcomDiff;
	}
	public void setLcomDiff(String lcomDiff) {
		this.lcomDiff = lcomDiff;
	}
	public String getRfcDiff() {
		return rfcDiff;
	}
	public void setRfcDiff(String rfcDiff) {
		this.rfcDiff = rfcDiff;
	}
	public String getCaDiff() {
		return caDiff;
	}
	public void setCaDiff(String caDiff) {
		this.caDiff = caDiff;
	}
	public String getCeDiff() {
		return ceDiff;
	}
	public void setCeDiff(String ceDiff) {
		this.ceDiff = ceDiff;
	}
	public String getNpmDiff() {
		return npmDiff;
	}
	public void setNpmDiff(String npmDiff) {
		this.npmDiff = npmDiff;
	}
	public String getLcom3Diff() {
		return lcom3Diff;
	}
	public void setLcom3Diff(String lcom3Diff) {
		this.lcom3Diff = lcom3Diff;
	}
	public String getLocDiff() {
		return locDiff;
	}
	public void setLocDiff(String locDiff) {
		this.locDiff = locDiff;
	}
	public String getDamDiff() {
		return damDiff;
	}
	public void setDamDiff(String damDiff) {
		this.damDiff = damDiff;
	}
	public String getMoaDiff() {
		return moaDiff;
	}
	public void setMoaDiff(String moaDiff) {
		this.moaDiff = moaDiff;
	}
	public String getMfaDiff() {
		return mfaDiff;
	}
	public void setMfaDiff(String mfaDiff) {
		this.mfaDiff = mfaDiff;
	}
	public String getCamDiff() {
		return camDiff;
	}
	public void setCamDiff(String camDiff) {
		this.camDiff = camDiff;
	}
	public String getIcDiff() {
		return icDiff;
	}
	public void setIcDiff(String icDiff) {
		this.icDiff = icDiff;
	}
	public String getCbmDiff() {
		return cbmDiff;
	}
	public void setCbmDiff(String cbmDiff) {
		this.cbmDiff = cbmDiff;
	}
	public String getAmcDiff() {
		return amcDiff;
	}
	public void setAmcDiff(String amcDiff) {
		this.amcDiff = amcDiff;
	}
	public String getCcDiff() {
		return ccDiff;
	}
	public void setCcDiff(String ccDiff) {
		this.ccDiff = ccDiff;
	}
	public String getGenAlgCoverageMean() {
		return genAlgCoverageMean;
	}
	public void setGenAlgCoverageMean(String genAlgCoverageMean) {
		this.genAlgCoverageMean = genAlgCoverageMean;
	}
	public String getGenAlgCoverageMed() {
		return genAlgCoverageMed;
	}
	public void setGenAlgCoverageMed(String genAlgCoverageMed) {
		this.genAlgCoverageMed = genAlgCoverageMed;
	}
	public String getGenAlgCoverageStd() {
		return genAlgCoverageStd;
	}
	public void setGenAlgCoverageStd(String genAlgCoverageStd) {
		this.genAlgCoverageStd = genAlgCoverageStd;
	}
	public String getGenAlgSizeMean() {
		return genAlgSizeMean;
	}
	public void setGenAlgSizeMean(String genAlgSizeMean) {
		this.genAlgSizeMean = genAlgSizeMean;
	}
	public String getGenAlgSizeMed() {
		return genAlgSizeMed;
	}
	public void setGenAlgSizeMed(String genAlgSizeMed) {
		this.genAlgSizeMed = genAlgSizeMed;
	}
	public String getGenAlgSizeStd() {
		return genAlgSizeStd;
	}
	public void setGenAlgSizeStd(String genAlgSizeStd) {
		this.genAlgSizeStd = genAlgSizeStd;
	}
	public String getGenAlgLengthMean() {
		return genAlgLengthMean;
	}
	public void setGenAlgLengthMean(String genAlgLengthMean) {
		this.genAlgLengthMean = genAlgLengthMean;
	}
	public String getGenAlgLengthMed() {
		return genAlgLengthMed;
	}
	public void setGenAlgLengthMed(String genAlgLengthMed) {
		this.genAlgLengthMed = genAlgLengthMed;
	}
	public String getGenAlgLengthStd() {
		return genAlgLengthStd;
	}
	public void setGenAlgLengthStd(String genAlgLengthStd) {
		this.genAlgLengthStd = genAlgLengthStd;
	}
	public String getDefaultSizeMean() {
		return defaultSizeMean;
	}
	public void setDefaultSizeMean(String defaultSizeMean) {
		this.defaultSizeMean = defaultSizeMean;
	}
	public String getDefaultSizeMed() {
		return defaultSizeMed;
	}
	public void setDefaultSizeMed(String defaultSizeMed) {
		this.defaultSizeMed = defaultSizeMed;
	}
	public String getDefaultSizeStd() {
		return defaultSizeStd;
	}
	public void setDefaultSizeStd(String defaultSizeStd) {
		this.defaultSizeStd = defaultSizeStd;
	}
	public String getDefaultLengthMean() {
		return defaultLengthMean;
	}
	public void setDefaultLengthMean(String defaultLengthMean) {
		this.defaultLengthMean = defaultLengthMean;
	}
	public String getDefaultLengthMed() {
		return defaultLengthMed;
	}
	public void setDefaultLengthMed(String defaultLengthMed) {
		this.defaultLengthMed = defaultLengthMed;
	}
	public String getDefaultLengthStd() {
		return defaultLengthStd;
	}
	public void setDefaultLengthStd(String defaultLengthStd) {
		this.defaultLengthStd = defaultLengthStd;
	}
    
    
    
    
}
