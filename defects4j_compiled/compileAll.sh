#!/bin/bash

PROPERTY_FILE=defects4j.build.properties

function getProperty {
	path=$1
	cd $path
	PROP_VALUE=`cat $PROPERTY_FILE | grep "d4j.classes.modified" | cut -d'=' -f2`
	echo $PROP_VALUE | sed -e 's/\./\//g'
	cd ..
}

compileandCKJM(){
	subject=$1
	pathToClass=$2
	i=$3

	#lib=$(defects4j export -p cp.compile -w $subject"_"$i"_buggy" | sed -e 's/\:/:.\//g')
	#echo "lib: "$lib	
	#modified=$(defects4j export -p classes.modified -w $subject"_"$i"_buggy" | sed -e 's/\./\//g')
	#pathToClass=$(defects4j export -p dir.bin.classes -w $subject"_"$i"_buggy")
	modified=$(getProperty $subject"_"$i"_buggy") 
	jar cf $subject"_"$i"_buggy"/$subject"_"$i"_buggy".jar -C $subject"_"$i"_buggy"/$pathToClass/ .
	echo $subject"_"$i"_buggy"/$pathToClass/$modified.class
	echo $subject"_"$i"_buggy"/$pathToClass/$modified | head -n 1 >> modifiedClasses.csv
	java -Djava.ext.dirs=$subject"_"$i"_buggy" -jar ckjm_ext.jar $subject"_"$i"_buggy"/$pathToClass/$modified.class | head -n 1 >> file.csv
	java -Djava.ext.dirs=$subject"_"$i"_buggy" -jar ckjm_ext.jar -x $subject"_"$i"_buggy"/$pathToClass/$modified.class >> file.xml
	
	#lib=$(defects4j export -p cp.compile -w $subject"_"$i"_fixed" | sed -e 's/\:/:.\//g')
	#echo "lib: "$lib
	#modified=$(defects4j export -p classes.modified -w $subject"_"$i"_fixed" | sed -e 's/\./\//g')
	#pathToClass=$(defects4j export -p dir.bin.classes -w $subject"_"$i"_fixed")
	modified=$(getProperty $subject"_"$i"_fixed")
	jar cf $subject"_"$i"_fixed"/$subject"_"$i"_fixed".jar -C $subject"_"$i"_fixed"/$pathToClass/ .
	echo $subject"_"$i"_fixed"/$pathToClass/$modified.class
	echo $subject"_"$i"_fixed"/$pathToClass/$modified | head -n 1 >> modifiedClasses.csv
	java -Djava.ext.dirs=$subject"_"$i"_fixed" -jar ckjm_ext.jar $subject"_"$i"_fixed"/$pathToClass/$modified.class | head -n 1 >> file.csv
	java -Djava.ext.dirs=$subject"_"$i"_fixed" -jar ckjm_ext.jar -x $subject"_"$i"_fixed"/$pathToClass/$modified.class >> file.xml
}

compileAllandCKJM(){
	subject=$1
	pathToClass=$2
	for i in {1..5}
	
	do
		compileandCKJM $subject $pathToClass $i
	done;
}

compileCollectionsandCKJM(){
	subject=$1
	pathToClass=$2
	for i in {25..28}
	do
		compileandCKJM $subject $pathToClass $i
	done;
	
}

compileLangandCKJM(){
	subject=$1
	pathToClass=$2
	compileandCKJM $subject $pathToClass 1
	
	for i in {3..5}
	do
		compileandCKJM $subject $pathToClass $i
	done;
	
}

compileAllandCKJM Chart build
compileAllandCKJM Cli target/classes/
compileAllandCKJM Closure build/classes
compileAllandCKJM Codec target/classes
compileCollectionsandCKJM Collections target/classes/
compileAllandCKJM Compress target/classes
compileAllandCKJM Csv target/classes
compileAllandCKJM Gson target/classes
compileAllandCKJM JacksonCore target/classes
compileAllandCKJM JacksonDatabind target/classes
compileAllandCKJM JacksonXml target/classes/
compileAllandCKJM Jsoup target/classes
compileAllandCKJM JxPath target/classes
compileLangandCKJM Lang target/classes
compileAllandCKJM Math target/classes
compileAllandCKJM Mockito build/classes/main/
compileAllandCKJM Time target/classes