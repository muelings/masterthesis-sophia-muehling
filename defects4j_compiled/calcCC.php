<?php


$lines =  array_map('str_getcsv', file('ckjm_result.csv'));

$current_line = "";
$sum = 0;
$i = -1;
$current_line_number = 0;
$numbers_in_current_block = 1;

foreach($lines as $line) {
  $i++;
  if($i < 1) continue;
  if(empty($line[0]) == false) {
    // output results of last block
    foreach($line as $key => $field) {
      if($key != 19) {
        echo "$field,";
      }
    }
    $avg = $sum / $numbers_in_current_block;
    echo "$avg\n";

    // get ready to read new block
    $sum = $line[19];
    $numbers_in_current_block = 1;
    $current_line_number = $i;
  } else {
    $sum += $line[19];
    $numbers_in_current_block++;
 }
}

