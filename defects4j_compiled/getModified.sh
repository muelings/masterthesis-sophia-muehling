#!/bin/bash

PROPERTY_FILE=defects4j.build.properties

function getProperty {
	path=$1
	cd $path
	PROP_VALUE=`cat $PROPERTY_FILE | grep "d4j.classes.modified" | cut -d'=' -f2`
	echo $PROP_VALUE | tr . \/
	cd ..
}

function getModified {
	subject=$1
	pathToClass=$2
	for i in {1..5}
	do
		cd $subject"_"$i"_buggy"
		echo "# Reading property from $subject $i buggy"
		modified=$(getProperty $subject"_"$i"_buggy") 
		echo "get features for modified class $modified"
		cd ..
		java -jar ckjm_ext.jar -x $subject"_"$i"_buggy"/$pathToClass/$modified.class >> file.xml
		
		cd $subject"_"$i"_fixed"
		echo "# Reading property from $subject $i fixed"
		modified=$(getProperty) 
		echo "get features for modified class $modified"
		cd ..
		java -jar ckjm_ext.jar -x $subject"_"$i"_fixed"/$pathToClass/$modified.class >> file.xml
	done;
	
}

function getModifiedCollections {
	subject=$1
	pathToClass=$2
	for i in {25..28}
	do
		cd $subject"_"$i"_buggy"
		echo "# Reading property from $subject $i buggy"
		modified=$(getProperty) 
		echo "get features for modified class $modified"
		cd ..
		java -jar ckjm_ext.jar -x $subject"_"$i"_buggy"/$pathToClass/$modified.class >> file.xml
		
		cd $subject"_"$i"_fixed"
		echo "# Reading property from $subject $i fixed"
		modified=$(getProperty) 
		echo "get features for modified class $modified"
		cd ..
		java -jar ckjm_ext.jar -x $subject"_"$i"_fixed"/$pathToClass/$modified.class >> file.xml
	done;
	
}

function getModifiedLang {
	subject=$1
	pathToClass=$2
	cd $subject"_1_buggy"
	echo "# Reading property from $subject 1 buggy"
	modified=$(getProperty) 
	echo "get features for modified class $modified"
	cd ..
	java -jar ckjm_ext.jar -x $subject"_1_buggy"/$pathToClass/$modified.class >> file.xml
	
	cd $subject"_1_fixed"
	echo "# Reading property from $subject 1 fixed"
	modified=$(getProperty) 
	echo "get features for modified class $modified"
	cd ..
	java -jar ckjm_ext.jar -x $subject"_1_fixed"/$pathToClass/$modified.class >> file.xml
	for i in {3..5}
	do
		cd $subject"_"$i"_buggy"
		echo "# Reading property from $subject $i buggy"
		modified=$(getProperty) 
		echo "get features for modified class $modified"
		cd ..
		java -jar ckjm_ext.jar -x $subject"_"$i"_buggy"/$pathToClass/$modified.class >> file.xml
		
		cd $subject"_"$i"_fixed"
		echo "# Reading property from $subject $i fixed"
		modified=$(getProperty) 
		echo "get features for modified class $modified"
		cd ..
		java -jar ckjm_ext.jar -x $subject"_"$i"_fixed"/$pathToClass/$modified.class >> file.xml
	done;
	
}

getModified Chart build
getModified Cli target/classes
getModified Closure build/classes
getModifiedCollections Collections target/classes
getModified Codec target/classes
getModified Compress target/classes
getModified Csv target/classes
getModified Gson target/classes
getModified JacksonCore target/classes
getModified JacksonDatabind target/classes
getModified JacksonXml target/classes
getModified Jsoup target/classes
getModified JxPath target/classes
getModifiedLang Lang target/classes
getModified Math target/classes
getModified Mockito build/classes/main
getModified Time target/classes