#!/bin/bash
checkoutandcompile(){
	subject=$1
	for i in {1..5}
	do		
		git rm --cached $subject"_"$i"_buggy"
		git add $subject"_"$i"_buggy"
		git rm --cached $subject"_"$i"_fixed"
		git add $subject"_"$i"_fixed"
	done;
}
checkoutandcompile Chart build
checkoutandcompile Cli target/classes/
checkoutandcompile Closure build/classes
checkoutandcompile Codec target/classes
checkoutandcompile Compress target/classes
checkoutandcompile Csv target/classes
checkoutandcompile Gson target/classes
checkoutandcompile JacksonCore target/classes
checkoutandcompile JacksonDatabind target/classes
checkoutandcompile JacksonXml target/classes/
checkoutandcompile Jsoup target/classes
checkoutandcompile JxPath target/classes
checkoutandcompile Math target/classes
checkoutandcompile Mockito build/classes/main/
checkoutandcompile Time target/classes

#git commit -m "remove submodules"
