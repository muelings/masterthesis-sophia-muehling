* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.TypeInfoFactory
* Starting client
* Connecting to master process on port 17169
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.TypeInfoFactory
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:50:23.867 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:50:23.871 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 46
* Using seed 1592207422030
* Starting evolution
[MASTER] 09:50:25.076 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.888888888888889 - fitness:91.75 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 10
[MASTER] 09:50:25.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.75, number of tests: 5, total length: 114
[MASTER] 09:50:25.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.88888888888889 - fitness:81.07142857142857 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 18
[MASTER] 09:50:25.185 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.07142857142857, number of tests: 9, total length: 202
[MASTER] 09:50:25.612 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.875 - fitness:79.54954954954955 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 09:50:25.612 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.54954954954955, number of tests: 4, total length: 126
[MASTER] 09:50:26.641 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.88888888888889 - fitness:79.17164179104478 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 09:50:26.641 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.17164179104478, number of tests: 4, total length: 127
[MASTER] 09:50:27.522 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.88888888888889 - fitness:78.84615384615384 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 09:50:27.522 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.84615384615384, number of tests: 7, total length: 178
[MASTER] 09:50:28.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.88888888888889 - fitness:78.5592105263158 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 09:50:28.180 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.5592105263158, number of tests: 7, total length: 176
[MASTER] 09:50:28.725 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.875 - fitness:78.0794701986755 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 09:50:28.725 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.0794701986755, number of tests: 5, total length: 106
[MASTER] 09:50:29.545 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.88888888888889 - fitness:78.0764705882353 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:29.546 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.0764705882353, number of tests: 4, total length: 87
[MASTER] 09:50:29.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.88888888888889 - fitness:77.87150837988827 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 09:50:29.792 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.87150837988827, number of tests: 4, total length: 104
[MASTER] 09:50:30.926 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.875 - fitness:77.68862275449102 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 09:50:30.926 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68862275449102, number of tests: 4, total length: 124
[MASTER] 09:50:31.834 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88888888888889 - fitness:77.68617021276596 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:31.834 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68617021276596, number of tests: 4, total length: 89
[MASTER] 09:50:32.139 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88888888888889 - fitness:77.51776649746193 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:32.139 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.51776649746193, number of tests: 4, total length: 89
[MASTER] 09:50:36.125 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.88888888888889 - fitness:77.36407766990291 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:36.125 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.36407766990291, number of tests: 2, total length: 47
[MASTER] 09:50:36.668 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.88888888888889 - fitness:77.22325581395349 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:36.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.22325581395349, number of tests: 2, total length: 47
[MASTER] 09:50:36.841 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.857142857142858 - fitness:77.09770114942529 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:36.841 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.09770114942529, number of tests: 2, total length: 43
[MASTER] 09:50:37.310 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.88888888888889 - fitness:76.97424892703863 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:37.310 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.97424892703863, number of tests: 3, total length: 76
[MASTER] 09:50:37.753 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.88888888888889 - fitness:76.86363636363636 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:37.753 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.86363636363636, number of tests: 3, total length: 77
[MASTER] 09:50:38.207 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.875 - fitness:76.76233183856502 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:38.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.76233183856502, number of tests: 3, total length: 77
[MASTER] 09:50:39.003 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.88888888888889 - fitness:76.7609561752988 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:39.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.7609561752988, number of tests: 3, total length: 78
[MASTER] 09:50:40.400 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.857142857142858 - fitness:76.66831683168317 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:40.400 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.66831683168317, number of tests: 3, total length: 48
[MASTER] 09:50:40.420 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.875 - fitness:76.66666666666667 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:40.420 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.66666666666667, number of tests: 2, total length: 43
[MASTER] 09:50:40.963 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.88888888888889 - fitness:76.66538461538461 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:40.963 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.66538461538461, number of tests: 2, total length: 43
[MASTER] 09:50:42.099 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.88888888888889 - fitness:76.57620817843866 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:42.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.57620817843866, number of tests: 2, total length: 43
[MASTER] 09:50:42.865 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.88888888888889 - fitness:76.49280575539568 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:42.865 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.49280575539568, number of tests: 2, total length: 47
[MASTER] 09:50:43.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.88888888888889 - fitness:76.41463414634147 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:43.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.41463414634147, number of tests: 3, total length: 78
[MASTER] 09:50:47.221 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.875 - fitness:76.34220532319392 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:47.222 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.34220532319392, number of tests: 2, total length: 43
[MASTER] 09:50:47.249 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.88888888888889 - fitness:76.34121621621621 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:50:47.249 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.34121621621621, number of tests: 3, total length: 88
[MASTER] 09:50:49.482 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.888888888888886 - fitness:76.27213114754099 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:49.482 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.27213114754099, number of tests: 2, total length: 43
[MASTER] 09:50:51.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.888888888888886 - fitness:76.20700636942675 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:51.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.20700636942675, number of tests: 2, total length: 43
[MASTER] 09:50:54.254 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.875 - fitness:76.14634146341463 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:54.254 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14634146341463, number of tests: 2, total length: 51
[MASTER] 09:50:54.626 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.888888888888886 - fitness:76.14551083591331 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:50:54.626 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14551083591331, number of tests: 2, total length: 43
[MASTER] 09:52:21.497 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.857142857142854 - fitness:76.0891472868217 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:52:21.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.0891472868217, number of tests: 3, total length: 75
[MASTER] 09:52:22.912 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.875 - fitness:76.08813559322034 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:52:22.912 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08813559322034, number of tests: 3, total length: 47
[MASTER] 09:52:23.735 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.888888888888886 - fitness:76.08734939759036 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:52:23.735 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08734939759036, number of tests: 4, total length: 73

[MASTER] 09:55:24.879 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 3344 generations, 1681732 statements, best individuals have fitness: 76.08734939759036
[MASTER] 09:55:24.881 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 46
* Number of covered goals: 2
* Generated 3 tests with total length 45
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     76 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 09:55:25.058 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:55:25.072 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 42 
[MASTER] 09:55:25.097 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 09:55:25.097 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 09:55:25.097 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:55:25.097 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:55:25.097 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:55:25.097 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 09:55:25.098 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 46
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeInfoFactory_ESTest' to evosuite-tests
* Done!

* Computation finished
