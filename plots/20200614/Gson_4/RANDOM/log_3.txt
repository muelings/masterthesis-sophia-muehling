* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.stream.JsonWriter
* Starting client
* Connecting to master process on port 7381
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.stream.JsonWriter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 111
[MASTER] 10:53:50.098 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:53:50.242 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 10:53:51.129 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/google/gson/stream/JsonWriter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:53:51.137 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 10:53:51.357 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/google/gson/stream/JsonWriter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:53:51.365 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
[MASTER] 10:53:51.365 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 1 | Tests with assertion: 1
* Generated 1 tests with total length 5
* GA-Budget:
	- MaxTime :                          1 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 10:53:51.803 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:53:51.812 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 10:53:51.819 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 10:53:51.819 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [beginObject:MockIllegalStateException, value:MockIllegalStateException, MockIllegalStateException:beforeValue-617,]
[MASTER] 10:53:51.819 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: beginObject:MockIllegalStateException at 3
[MASTER] 10:53:51.819 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [beginObject:MockIllegalStateException, value:MockIllegalStateException, MockIllegalStateException:beforeValue-617,]
[MASTER] 10:53:51.819 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: value:MockIllegalStateException at 2
[MASTER] 10:53:51.819 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [beginObject:MockIllegalStateException, value:MockIllegalStateException, MockIllegalStateException:beforeValue-617,]
[MASTER] 10:53:51.855 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 10:53:51.857 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 23%
* Total number of goals: 111
* Number of covered goals: 25
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'JsonWriter_ESTest' to evosuite-tests
* Done!

* Computation finished
