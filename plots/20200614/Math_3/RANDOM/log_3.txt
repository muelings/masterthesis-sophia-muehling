* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.util.MathArrays
* Starting client
* Connecting to master process on port 4633
* Analyzing classpath: 
  - ../defects4j_compiled/Math_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.util.MathArrays
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 02:38:59.250 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 257
[MASTER] 02:39:00.311 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 02:39:01.190 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/util/MathArrays_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:39:01.199 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 02:39:01.439 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/util/MathArrays_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:39:01.447 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 02:39:01.448 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 58 | Tests with assertion: 1
* Generated 1 tests with total length 20
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 02:39:02.315 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:39:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 18 
[MASTER] 02:39:02.334 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 02:39:02.334 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [linearCombination:ArrayIndexOutOfBoundsException, ArrayIndexOutOfBoundsException:checkOrder-376,, checkOrder:ArrayIndexOutOfBoundsException]
[MASTER] 02:39:02.334 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: checkOrder:ArrayIndexOutOfBoundsException at 17
[MASTER] 02:39:02.334 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [linearCombination:ArrayIndexOutOfBoundsException, ArrayIndexOutOfBoundsException:checkOrder-376,, checkOrder:ArrayIndexOutOfBoundsException]
[MASTER] 02:39:02.334 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: linearCombination:ArrayIndexOutOfBoundsException at 14
[MASTER] 02:39:02.334 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [linearCombination:ArrayIndexOutOfBoundsException, ArrayIndexOutOfBoundsException:checkOrder-376,, checkOrder:ArrayIndexOutOfBoundsException]
[MASTER] 02:39:02.475 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:39:02.478 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 1%
* Total number of goals: 257
* Number of covered goals: 2
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MathArrays_ESTest' to evosuite-tests
* Done!

* Computation finished
