* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 3272
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 12:49:30.205 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 12:49:30.210 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 141
* Using seed 1592218168274
* Starting evolution
[MASTER] 12:49:31.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:627.0 - branchDistance:0.0 - coverage:294.0 - ex: 0 - tex: 6
[MASTER] 12:49:31.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 627.0, number of tests: 3, total length: 80
[MASTER] 12:49:31.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:543.3333333333333 - branchDistance:0.0 - coverage:210.33333333333331 - ex: 0 - tex: 14
[MASTER] 12:49:31.093 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 543.3333333333333, number of tests: 7, total length: 196
[MASTER] 12:49:31.437 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:499.9800487586447 - branchDistance:0.0 - coverage:166.98004875864473 - ex: 0 - tex: 16
[MASTER] 12:49:31.437 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 499.9800487586447, number of tests: 9, total length: 157
[MASTER] 12:49:31.995 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:499.32338308457713 - branchDistance:0.0 - coverage:166.32338308457713 - ex: 0 - tex: 14
[MASTER] 12:49:31.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 499.32338308457713, number of tests: 7, total length: 181
[MASTER] 12:49:32.627 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:498.6465154053094 - branchDistance:0.0 - coverage:165.64651540530937 - ex: 0 - tex: 18
[MASTER] 12:49:32.627 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 498.6465154053094, number of tests: 9, total length: 207
[MASTER] 12:49:32.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:488.6465154053094 - branchDistance:0.0 - coverage:155.64651540530937 - ex: 0 - tex: 20
[MASTER] 12:49:32.929 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 488.6465154053094, number of tests: 10, total length: 223
[MASTER] 12:49:33.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:482.3133328333208 - branchDistance:0.0 - coverage:149.31333283332083 - ex: 0 - tex: 16
[MASTER] 12:49:33.297 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 482.3133328333208, number of tests: 8, total length: 189
[MASTER] 12:49:33.341 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:472.303281081877 - branchDistance:0.0 - coverage:139.30328108187703 - ex: 0 - tex: 20
[MASTER] 12:49:33.341 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 472.303281081877, number of tests: 10, total length: 244
[MASTER] 12:49:33.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:468.990099009901 - branchDistance:0.0 - coverage:135.99009900990097 - ex: 0 - tex: 18
[MASTER] 12:49:33.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 468.990099009901, number of tests: 10, total length: 231
[MASTER] 12:49:33.597 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:468.303281081877 - branchDistance:0.0 - coverage:135.30328108187703 - ex: 0 - tex: 20
[MASTER] 12:49:33.597 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 468.303281081877, number of tests: 10, total length: 265
[MASTER] 12:49:33.656 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:465.32343234323434 - branchDistance:0.0 - coverage:132.32343234323432 - ex: 0 - tex: 18
[MASTER] 12:49:33.656 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 465.32343234323434, number of tests: 10, total length: 215
[MASTER] 12:49:33.718 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:452.2707461888708 - branchDistance:0.0 - coverage:119.27074618887082 - ex: 0 - tex: 16
[MASTER] 12:49:33.718 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 452.2707461888708, number of tests: 10, total length: 226
[MASTER] 12:49:33.923 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:450.990099009901 - branchDistance:0.0 - coverage:117.99009900990099 - ex: 0 - tex: 16
[MASTER] 12:49:33.923 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 450.990099009901, number of tests: 10, total length: 226
[MASTER] 12:49:33.980 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:449.6265641639541 - branchDistance:0.0 - coverage:116.62656416395409 - ex: 0 - tex: 20
[MASTER] 12:49:33.981 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 449.6265641639541, number of tests: 10, total length: 276
[MASTER] 12:49:34.042 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:448.31011213619684 - branchDistance:0.0 - coverage:115.31011213619684 - ex: 0 - tex: 16
[MASTER] 12:49:34.042 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 448.31011213619684, number of tests: 10, total length: 230
[MASTER] 12:49:34.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:447.9699985073884 - branchDistance:0.0 - coverage:114.96999850738842 - ex: 0 - tex: 20
[MASTER] 12:49:34.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 447.9699985073884, number of tests: 10, total length: 241
[MASTER] 12:49:35.575 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:444.30333184072174 - branchDistance:0.0 - coverage:111.30333184072175 - ex: 0 - tex: 20
[MASTER] 12:49:35.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 444.30333184072174, number of tests: 11, total length: 247
[MASTER] 12:49:36.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:436.95662754150624 - branchDistance:0.0 - coverage:103.95662754150621 - ex: 0 - tex: 16
[MASTER] 12:49:36.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 436.95662754150624, number of tests: 10, total length: 226
[MASTER] 12:49:37.002 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:428.9667285516072 - branchDistance:0.0 - coverage:95.96672855160722 - ex: 0 - tex: 18
[MASTER] 12:49:37.002 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 428.9667285516072, number of tests: 11, total length: 250
[MASTER] 12:49:37.014 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:427.6134439769186 - branchDistance:0.0 - coverage:94.6134439769186 - ex: 0 - tex: 18
[MASTER] 12:49:37.014 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 427.6134439769186, number of tests: 10, total length: 243
[MASTER] 12:49:37.144 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:420.6326210327171 - branchDistance:0.0 - coverage:87.6326210327171 - ex: 0 - tex: 18
[MASTER] 12:49:37.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 420.6326210327171, number of tests: 11, total length: 259
[MASTER] 12:49:37.295 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:419.29928769938374 - branchDistance:0.0 - coverage:86.29928769938377 - ex: 0 - tex: 18
[MASTER] 12:49:37.295 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 419.29928769938374, number of tests: 11, total length: 260
[MASTER] 12:49:37.338 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:416.30975890357223 - branchDistance:0.0 - coverage:83.30975890357225 - ex: 0 - tex: 20
[MASTER] 12:49:37.338 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 416.30975890357223, number of tests: 12, total length: 280
[MASTER] 12:49:38.022 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:413.64309223690555 - branchDistance:0.0 - coverage:80.64309223690557 - ex: 0 - tex: 22
[MASTER] 12:49:38.022 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 413.64309223690555, number of tests: 13, total length: 304
[MASTER] 12:49:38.257 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:404.61634323102385 - branchDistance:0.0 - coverage:71.61634323102388 - ex: 0 - tex: 18
[MASTER] 12:49:38.257 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 404.61634323102385, number of tests: 10, total length: 244
[MASTER] 12:49:38.612 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:404.31338209197804 - branchDistance:0.0 - coverage:71.31338209197804 - ex: 0 - tex: 20
[MASTER] 12:49:38.612 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 404.31338209197804, number of tests: 12, total length: 290
[MASTER] 12:49:38.800 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:403.6162929797676 - branchDistance:0.0 - coverage:70.61629297976761 - ex: 0 - tex: 20
[MASTER] 12:49:38.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 403.6162929797676, number of tests: 11, total length: 275
[MASTER] 12:49:39.108 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:391.31338209197804 - branchDistance:0.0 - coverage:58.31338209197804 - ex: 0 - tex: 18
[MASTER] 12:49:39.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 391.31338209197804, number of tests: 12, total length: 267
[MASTER] 12:49:39.378 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:390.6263432310239 - branchDistance:0.0 - coverage:57.62634323102388 - ex: 0 - tex: 18
[MASTER] 12:49:39.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 390.6263432310239, number of tests: 12, total length: 260
[MASTER] 12:49:39.474 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:390.31338209197804 - branchDistance:0.0 - coverage:57.31338209197804 - ex: 0 - tex: 20
[MASTER] 12:49:39.474 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 390.31338209197804, number of tests: 13, total length: 296
[MASTER] 12:49:39.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:388.6263432310239 - branchDistance:0.0 - coverage:55.62634323102388 - ex: 0 - tex: 20
[MASTER] 12:49:39.650 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 388.6263432310239, number of tests: 12, total length: 281
[MASTER] 12:49:40.076 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:385.6263432310239 - branchDistance:0.0 - coverage:52.62634323102388 - ex: 0 - tex: 20
[MASTER] 12:49:40.076 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 385.6263432310239, number of tests: 13, total length: 289
[MASTER] 12:49:40.254 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:385.303481101879 - branchDistance:0.0 - coverage:52.30348110187903 - ex: 0 - tex: 22
[MASTER] 12:49:40.254 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 385.303481101879, number of tests: 12, total length: 299
[MASTER] 12:49:40.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:382.64671542531136 - branchDistance:0.0 - coverage:49.64671542531137 - ex: 0 - tex: 22
[MASTER] 12:49:40.324 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 382.64671542531136, number of tests: 14, total length: 320
[MASTER] 12:49:40.507 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:381.6368144352124 - branchDistance:0.0 - coverage:48.636814435212365 - ex: 0 - tex: 24
[MASTER] 12:49:40.507 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 381.6368144352124, number of tests: 15, total length: 332
[MASTER] 12:49:41.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:379.303481101879 - branchDistance:0.0 - coverage:46.30348110187903 - ex: 0 - tex: 24
[MASTER] 12:49:41.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 379.303481101879, number of tests: 14, total length: 334
[MASTER] 12:49:42.369 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:377.3006244257874 - branchDistance:0.0 - coverage:44.30062442578739 - ex: 0 - tex: 26
[MASTER] 12:49:42.369 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 377.3006244257874, number of tests: 15, total length: 355
[MASTER] 12:49:43.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:376.3006167598758 - branchDistance:0.0 - coverage:43.30061675987584 - ex: 0 - tex: 28
[MASTER] 12:49:43.114 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 376.3006167598758, number of tests: 16, total length: 362
[MASTER] 12:49:43.616 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:375.3006244257874 - branchDistance:0.0 - coverage:42.30062442578739 - ex: 0 - tex: 24
[MASTER] 12:49:43.616 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 375.3006244257874, number of tests: 15, total length: 349
[MASTER] 12:49:44.213 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:374.3006244257874 - branchDistance:0.0 - coverage:41.30062442578739 - ex: 0 - tex: 26
[MASTER] 12:49:44.213 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 374.3006244257874, number of tests: 16, total length: 385
[MASTER] 12:49:45.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:366.3034734286364 - branchDistance:0.0 - coverage:33.303473428636394 - ex: 0 - tex: 24
[MASTER] 12:49:45.168 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 366.3034734286364, number of tests: 15, total length: 355
[MASTER] 12:49:45.228 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:366.30346575539374 - branchDistance:0.0 - coverage:33.30346575539376 - ex: 0 - tex: 26
[MASTER] 12:49:45.228 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 366.30346575539374, number of tests: 15, total length: 358
[MASTER] 12:49:45.941 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:365.3034734286364 - branchDistance:0.0 - coverage:32.303473428636394 - ex: 0 - tex: 24
[MASTER] 12:49:45.941 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 365.3034734286364, number of tests: 15, total length: 353
[MASTER] 12:49:46.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:365.30346575539374 - branchDistance:0.0 - coverage:32.30346575539376 - ex: 0 - tex: 28
[MASTER] 12:49:46.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 365.30346575539374, number of tests: 16, total length: 374
[MASTER] 12:49:47.003 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:365.30345806733897 - branchDistance:0.0 - coverage:32.30345806733899 - ex: 0 - tex: 26
[MASTER] 12:49:47.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 365.30345806733897, number of tests: 15, total length: 358
[MASTER] 12:49:47.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:364.30346575539374 - branchDistance:0.0 - coverage:31.303465755393763 - ex: 0 - tex: 30
[MASTER] 12:49:47.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 364.30346575539374, number of tests: 17, total length: 402
[MASTER] 12:49:47.698 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:364.30345806733897 - branchDistance:0.0 - coverage:31.30345806733899 - ex: 0 - tex: 28
[MASTER] 12:49:47.698 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 364.30345806733897, number of tests: 16, total length: 371
[MASTER] 12:49:47.933 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:363.30345806733897 - branchDistance:0.0 - coverage:30.30345806733899 - ex: 0 - tex: 30
[MASTER] 12:49:47.933 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 363.30345806733897, number of tests: 17, total length: 402
[MASTER] 12:49:50.529 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:362.313359057438 - branchDistance:0.0 - coverage:29.313359057438 - ex: 0 - tex: 28
[MASTER] 12:49:50.530 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 362.313359057438, number of tests: 16, total length: 392
[MASTER] 12:49:50.650 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:362.30345806733897 - branchDistance:0.0 - coverage:29.30345806733899 - ex: 0 - tex: 28
[MASTER] 12:49:50.650 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 362.30345806733897, number of tests: 17, total length: 425
[MASTER] 12:49:51.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:361.30345806733897 - branchDistance:0.0 - coverage:28.30345806733899 - ex: 0 - tex: 30
[MASTER] 12:49:51.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 361.30345806733897, number of tests: 17, total length: 441
[MASTER] 12:49:53.873 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:361.30294239077136 - branchDistance:0.0 - coverage:28.302942390771335 - ex: 0 - tex: 30
[MASTER] 12:49:53.873 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 361.30294239077136, number of tests: 17, total length: 444
[MASTER] 12:49:54.036 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:360.30345806733897 - branchDistance:0.0 - coverage:27.303458067338994 - ex: 0 - tex: 32
[MASTER] 12:49:54.036 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 360.30345806733897, number of tests: 18, total length: 469
[MASTER] 12:49:55.603 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:360.3034580489216 - branchDistance:0.0 - coverage:27.30345804892159 - ex: 0 - tex: 28
[MASTER] 12:49:55.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 360.3034580489216, number of tests: 18, total length: 439
[MASTER] 12:49:55.652 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:360.30294239077136 - branchDistance:0.0 - coverage:27.302942390771335 - ex: 0 - tex: 30
[MASTER] 12:49:55.652 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 360.30294239077136, number of tests: 18, total length: 441
[MASTER] 12:49:55.892 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:359.30345806733897 - branchDistance:0.0 - coverage:26.303458067338994 - ex: 0 - tex: 34
[MASTER] 12:49:55.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 359.30345806733897, number of tests: 19, total length: 497
[MASTER] 12:49:57.277 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:359.3034580489216 - branchDistance:0.0 - coverage:26.30345804892159 - ex: 0 - tex: 30
[MASTER] 12:49:57.277 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 359.3034580489216, number of tests: 18, total length: 463
[MASTER] 12:49:57.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:355.97012473400565 - branchDistance:0.0 - coverage:22.97012473400566 - ex: 0 - tex: 32
[MASTER] 12:49:57.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 355.97012473400565, number of tests: 18, total length: 461
[MASTER] 12:49:58.755 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:354.97012473400565 - branchDistance:0.0 - coverage:21.97012473400566 - ex: 0 - tex: 32
[MASTER] 12:49:58.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 354.97012473400565, number of tests: 19, total length: 492
[MASTER] 12:49:59.879 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.97012471558827 - branchDistance:0.0 - coverage:20.970124715588256 - ex: 0 - tex: 32
[MASTER] 12:49:59.879 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.97012471558827, number of tests: 20, total length: 517
[MASTER] 12:50:01.097 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.97012469344503 - branchDistance:0.0 - coverage:20.970124693445044 - ex: 0 - tex: 32
[MASTER] 12:50:01.097 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.97012469344503, number of tests: 20, total length: 520
[MASTER] 12:50:01.450 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.97002570196145 - branchDistance:0.0 - coverage:20.970025701961454 - ex: 0 - tex: 34
[MASTER] 12:50:01.450 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.97002570196145, number of tests: 20, total length: 518
[MASTER] 12:50:03.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.3034580489216 - branchDistance:0.0 - coverage:20.30345804892159 - ex: 0 - tex: 34
[MASTER] 12:50:03.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.3034580489216, number of tests: 21, total length: 543
[MASTER] 12:50:03.464 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.303359057438 - branchDistance:0.0 - coverage:20.303359057438 - ex: 0 - tex: 36
[MASTER] 12:50:03.464 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.303359057438, number of tests: 21, total length: 541
[MASTER] 12:50:03.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.30335903529476 - branchDistance:0.0 - coverage:20.30335903529479 - ex: 0 - tex: 34
[MASTER] 12:50:03.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.30335903529476, number of tests: 20, total length: 520
[MASTER] 12:50:04.020 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:352.97002570196145 - branchDistance:0.0 - coverage:19.970025701961454 - ex: 0 - tex: 36
[MASTER] 12:50:04.020 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 352.97002570196145, number of tests: 21, total length: 537
[MASTER] 12:50:04.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:352.96997644330423 - branchDistance:0.0 - coverage:19.969976443304247 - ex: 0 - tex: 34
[MASTER] 12:50:04.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 352.96997644330423, number of tests: 20, total length: 518
[MASTER] 12:50:06.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:352.313260047537 - branchDistance:0.0 - coverage:19.313260047537014 - ex: 0 - tex: 38
[MASTER] 12:50:06.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 352.313260047537, number of tests: 22, total length: 552
[MASTER] 12:50:06.972 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:352.303359057438 - branchDistance:0.0 - coverage:19.303359057438 - ex: 0 - tex: 38
[MASTER] 12:50:06.972 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 352.303359057438, number of tests: 22, total length: 549
[MASTER] 12:50:07.321 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:352.3033097766376 - branchDistance:0.0 - coverage:19.303309776637583 - ex: 0 - tex: 36
[MASTER] 12:50:07.321 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 352.3033097766376, number of tests: 21, total length: 550
[MASTER] 12:50:09.202 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:351.31341698193694 - branchDistance:0.0 - coverage:18.313416981936918 - ex: 0 - tex: 38
[MASTER] 12:50:09.202 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 351.31341698193694, number of tests: 21, total length: 520
[MASTER] 12:50:09.711 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:351.3033097766376 - branchDistance:0.0 - coverage:18.303309776637583 - ex: 0 - tex: 34
[MASTER] 12:50:09.711 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 351.3033097766376, number of tests: 20, total length: 509
[MASTER] 12:50:09.807 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:350.31336002789385 - branchDistance:0.0 - coverage:17.313360027893864 - ex: 0 - tex: 36
[MASTER] 12:50:09.807 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 350.31336002789385, number of tests: 21, total length: 535
[MASTER] 12:50:10.266 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:350.3033097766376 - branchDistance:0.0 - coverage:17.303309776637583 - ex: 0 - tex: 38
[MASTER] 12:50:10.266 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 350.3033097766376, number of tests: 22, total length: 554
[MASTER] 12:50:11.366 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.31336002789385 - branchDistance:0.0 - coverage:16.313360027893864 - ex: 0 - tex: 34
[MASTER] 12:50:11.366 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.31336002789385, number of tests: 21, total length: 517
[MASTER] 12:50:11.738 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:348.98002669456054 - branchDistance:0.0 - coverage:15.980026694560529 - ex: 0 - tex: 36
[MASTER] 12:50:11.738 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 348.98002669456054, number of tests: 22, total length: 544
[MASTER] 12:50:11.886 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:348.31336002789385 - branchDistance:0.0 - coverage:15.31336002789386 - ex: 0 - tex: 38
[MASTER] 12:50:11.886 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 348.31336002789385, number of tests: 22, total length: 556
[MASTER] 12:50:11.954 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:348.313359976021 - branchDistance:0.0 - coverage:15.313359976020976 - ex: 0 - tex: 38
[MASTER] 12:50:11.954 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 348.313359976021, number of tests: 22, total length: 554
[MASTER] 12:50:16.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:348.31335994822155 - branchDistance:0.0 - coverage:15.313359948221542 - ex: 0 - tex: 36
[MASTER] 12:50:16.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 348.31335994822155, number of tests: 21, total length: 520
[MASTER] 12:50:16.410 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:347.9800266426876 - branchDistance:0.0 - coverage:14.980026642687644 - ex: 0 - tex: 34
[MASTER] 12:50:16.410 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 347.9800266426876, number of tests: 21, total length: 503
[MASTER] 12:50:21.159 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:347.9795602247772 - branchDistance:0.0 - coverage:14.979560224777195 - ex: 0 - tex: 32
[MASTER] 12:50:21.159 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 347.9795602247772, number of tests: 20, total length: 470
[MASTER] 12:50:21.828 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:346.9795602247772 - branchDistance:0.0 - coverage:13.979560224777195 - ex: 0 - tex: 32
[MASTER] 12:50:21.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 346.9795602247772, number of tests: 20, total length: 469
[MASTER] 12:50:23.797 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:346.9795601740601 - branchDistance:0.0 - coverage:13.979560174060094 - ex: 0 - tex: 32
[MASTER] 12:50:23.797 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 346.9795601740601, number of tests: 20, total length: 470
[MASTER] 12:50:23.983 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:344.9895602247772 - branchDistance:0.0 - coverage:11.989560224777197 - ex: 0 - tex: 32
[MASTER] 12:50:23.983 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 344.9895602247772, number of tests: 20, total length: 475
[MASTER] 12:50:25.355 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:344.9895601740601 - branchDistance:0.0 - coverage:11.989560174060095 - ex: 0 - tex: 32
[MASTER] 12:50:25.355 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 344.9895601740601, number of tests: 20, total length: 472
[MASTER] 12:50:31.999 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:344.98956014626066 - branchDistance:0.0 - coverage:11.989560146260661 - ex: 0 - tex: 30
[MASTER] 12:50:31.999 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 344.98956014626066, number of tests: 20, total length: 446
[MASTER] 12:50:35.039 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:343.98956014626066 - branchDistance:0.0 - coverage:10.989560146260661 - ex: 0 - tex: 32
[MASTER] 12:50:35.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 343.98956014626066, number of tests: 20, total length: 445
[MASTER] 12:50:38.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:343.98956010417425 - branchDistance:0.0 - coverage:10.98956010417424 - ex: 0 - tex: 34
[MASTER] 12:50:38.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 343.98956010417425, number of tests: 21, total length: 477
[MASTER] 12:50:46.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:343.9895600268227 - branchDistance:0.0 - coverage:10.989560026822716 - ex: 0 - tex: 32
[MASTER] 12:50:46.691 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 343.9895600268227, number of tests: 18, total length: 426
[MASTER] 12:50:50.073 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:342.9895600268227 - branchDistance:0.0 - coverage:9.989560026822716 - ex: 0 - tex: 32
[MASTER] 12:50:50.073 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 342.9895600268227, number of tests: 19, total length: 455
[MASTER] 12:51:00.116 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:342.98955996278823 - branchDistance:0.0 - coverage:9.989559962788206 - ex: 0 - tex: 32
[MASTER] 12:51:00.116 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 342.98955996278823, number of tests: 18, total length: 425
[MASTER] 12:51:12.745 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:342.989559843849 - branchDistance:0.0 - coverage:9.989559843849033 - ex: 0 - tex: 32
[MASTER] 12:51:12.745 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 342.989559843849, number of tests: 17, total length: 415
[MASTER] 12:51:30.224 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:342.9894501947262 - branchDistance:0.0 - coverage:9.989450194726228 - ex: 0 - tex: 32
[MASTER] 12:51:30.224 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 342.9894501947262, number of tests: 16, total length: 409
[MASTER] 12:52:52.417 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:342.98841581687407 - branchDistance:0.0 - coverage:9.988415816874081 - ex: 0 - tex: 30
[MASTER] 12:52:52.417 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 342.98841581687407, number of tests: 15, total length: 335
[MASTER] 12:53:02.015 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:342.98841553341083 - branchDistance:0.0 - coverage:9.988415533410839 - ex: 0 - tex: 30
[MASTER] 12:53:02.015 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 342.98841553341083, number of tests: 15, total length: 331
[MASTER] 12:53:02.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:340.9999765105157 - branchDistance:0.0 - coverage:7.9999765105157 - ex: 0 - tex: 32
[MASTER] 12:53:02.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 340.9999765105157, number of tests: 16, total length: 365
[MASTER] 12:53:04.418 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:340.99997622705246 - branchDistance:0.0 - coverage:7.999976227052458 - ex: 0 - tex: 32
[MASTER] 12:53:04.418 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 340.99997622705246, number of tests: 16, total length: 365
[MASTER] 12:53:10.561 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:339.99998448636484 - branchDistance:0.0 - coverage:6.999984486364829 - ex: 0 - tex: 30
[MASTER] 12:53:10.561 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 339.99998448636484, number of tests: 16, total length: 363
[MASTER] 12:53:12.865 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:339.99998438821916 - branchDistance:0.0 - coverage:6.999984388219178 - ex: 0 - tex: 30
[MASTER] 12:53:12.865 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 339.99998438821916, number of tests: 16, total length: 365
[MASTER] 12:53:13.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:339.9999842360903 - branchDistance:0.0 - coverage:6.9999842360903255 - ex: 0 - tex: 30
[MASTER] 12:53:13.903 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 339.9999842360903, number of tests: 16, total length: 369
[MASTER] 12:53:14.821 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:337.9999923046141 - branchDistance:0.0 - coverage:4.999992304614153 - ex: 0 - tex: 30
[MASTER] 12:53:14.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 337.9999923046141, number of tests: 16, total length: 363
[MASTER] 12:53:19.826 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:336.9999923046141 - branchDistance:0.0 - coverage:3.9999923046141532 - ex: 0 - tex: 32
[MASTER] 12:53:19.826 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 336.9999923046141, number of tests: 17, total length: 400
[MASTER] 12:53:24.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999847412109375 - fitness:116.6672218539181 - branchDistance:0.0 - coverage:4.999992304614153 - ex: 0 - tex: 30
[MASTER] 12:53:24.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.6672218539181, number of tests: 16, total length: 365
[MASTER] 12:53:33.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999847412109375 - fitness:115.6672218539181 - branchDistance:0.0 - coverage:3.9999923046141532 - ex: 0 - tex: 32
[MASTER] 12:53:33.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.6672218539181, number of tests: 17, total length: 403

[MASTER] 12:54:31.226 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 444 generations, 369092 statements, best individuals have fitness: 115.6672218539181
[MASTER] 12:54:31.229 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 98%
* Total number of goals: 141
* Number of covered goals: 138
* Generated 16 tests with total length 344
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        302 / 300          Finished!
	- ZeroFitness :                    116 / 0           
[MASTER] 12:54:33.992 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:54:35.287 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 295 
[MASTER] 12:54:35.457 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.457 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.457 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.458 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.458 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.458 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.458 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.458 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.458 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.459 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.459 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.459 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.459 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.459 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 12:54:35.459 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
* Coverage of criterion REGRESSION: 0%
[MASTER] 12:54:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Total number of goals: 141
[MASTER] 12:54:35.461 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
