/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 15 13:09:51 GMT 2020
 */

package org.apache.commons.jxpath.ri.model.beans;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.jxpath.BasicVariables;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.ri.QName;
import org.apache.commons.jxpath.ri.model.NodePointer;
import org.apache.commons.jxpath.ri.model.VariablePointer;
import org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class NullPropertyPointer_ESTest extends NullPropertyPointer_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      BasicVariables basicVariables0 = new BasicVariables();
      QName qName0 = new QName("Cannot convert value of class ");
      VariablePointer variablePointer0 = new VariablePointer(basicVariables0, qName0);
      JXPathContext jXPathContext0 = JXPathContext.newContext((Object) qName0);
      NodePointer nodePointer0 = variablePointer0.createPath(jXPathContext0, (Object) null);
      NodePointer nodePointer1 = NodePointer.newChildNodePointer(nodePointer0, qName0, (Object) null);
      NullPropertyPointer nullPropertyPointer0 = new NullPropertyPointer(nodePointer1);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.apache.commons.jxpath.JXPathAbstractFactoryException : Factory null reported success creating object for path: $Cannot convert value of class /* but object was null.  Terminating to avoid stack recursion.
      // Modified Version:
      //     java.lang.StackOverflowError : null
      // Undeclared exception!
      try { 
        nullPropertyPointer0.createChild(jXPathContext0, qName0, Integer.MIN_VALUE, (Object) null);
        fail("Expecting exception: RuntimeException");
      
      } catch(RuntimeException e) {
         //
         // Factory null reported success creating object for path: $Cannot convert value of class /* but object was null.  Terminating to avoid stack recursion.
         //
         verifyException("org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer", e);
         assertTrue(e.getMessage().equals("Factory null reported success creating object for path: $Cannot convert value of class /* but object was null.  Terminating to avoid stack recursion."));   
      }
  }
}
