/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 15 10:00:57 GMT 2020
 */

package com.fasterxml.jackson.core.util;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import com.fasterxml.jackson.core.util.BufferRecycler;
import com.fasterxml.jackson.core.util.TextBuffer;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class TextBuffer_ESTest extends TextBuffer_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      BufferRecycler bufferRecycler0 = new BufferRecycler();
      TextBuffer textBuffer0 = new TextBuffer(bufferRecycler0);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     java.lang.StringIndexOutOfBoundsException : String index out of range: 157
      // Modified Version:
      //     java.lang.NumberFormatException : Bad offset or len arguments for char[] input.
      try { 
        textBuffer0.contentsAsDecimal();
        fail("Expecting exception: NumberFormatException");
      
      } catch(NumberFormatException e) {
         //
         // Value \"\" can not be represented as BigDecimal
         //
         verifyException("com.fasterxml.jackson.core.io.NumberInput", e);
         assertTrue(e.getMessage().equals("Value \"\" can not be represented as BigDecimal"));   
      }
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      BufferRecycler bufferRecycler0 = new BufferRecycler();
      TextBuffer textBuffer0 = new TextBuffer(bufferRecycler0);
      textBuffer0.emptyAndGetCurrentSegment();
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     java.lang.NumberFormatException : Value "" can not be represented as BigDecimal
      // Modified Version:
      //     java.lang.NumberFormatException : null
      try { 
        textBuffer0.contentsAsDecimal();
        fail("Expecting exception: NumberFormatException");
      
      } catch(NumberFormatException e) {
         //
         // Value \"\" can not be represented as BigDecimal
         //
         verifyException("com.fasterxml.jackson.core.io.NumberInput", e);
         assertTrue(e.getMessage().equals("Value \"\" can not be represented as BigDecimal"));   
      }
  }
}
