* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
* Starting client
* Connecting to master process on port 2434
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 06:57:52.481 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 37
[MASTER] 06:57:56.681 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 06:57:57.594 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/tar/TarArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:57:57.618 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 06:57:57.856 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/tar/TarArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 06:57:57.894 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 06:57:57.894 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 451 | Tests with assertion: 1
* Generated 1 tests with total length 28
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                          5 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 06:57:58.354 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:57:58.371 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 23 
[MASTER] 06:57:58.392 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 06:57:58.392 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [finish:MockIOException, MockIOException:finish-114,]
[MASTER] 06:57:58.392 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: finish:MockIOException at 22
[MASTER] 06:57:58.392 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [finish:MockIOException, MockIOException:finish-114,]
[MASTER] 06:57:58.818 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
[MASTER] 06:57:58.822 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 16%
* Total number of goals: 37
* Number of covered goals: 6
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 32
* Writing JUnit test case 'TarArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
