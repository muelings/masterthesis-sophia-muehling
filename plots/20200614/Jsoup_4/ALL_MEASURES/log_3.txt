* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 17611
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 13:53:40.210 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 13:53:40.214 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 28
* Using seed 1592222017885
* Starting evolution
[MASTER] 13:53:41.105 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:121.0 - branchDistance:0.0 - coverage:60.0 - ex: 0 - tex: 16
[MASTER] 13:53:41.105 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 8, total length: 177
[MASTER] 13:53:41.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:92.0 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 12
[MASTER] 13:53:41.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.0, number of tests: 8, total length: 177
[MASTER] 13:53:41.497 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.0 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 10
[MASTER] 13:53:41.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0, number of tests: 8, total length: 145
[MASTER] 13:53:41.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.99996948149052 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 18
[MASTER] 13:53:41.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.99996948149052, number of tests: 10, total length: 206
[MASTER] 13:53:42.522 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:68.99996948335317 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 14
[MASTER] 13:53:42.522 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.99996948335317, number of tests: 9, total length: 154
[MASTER] 13:53:44.481 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:39.28888888888889 - branchDistance:0.0 - coverage:14.288888888888888 - ex: 0 - tex: 8
[MASTER] 13:53:44.482 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.28888888888889, number of tests: 6, total length: 129
[MASTER] 13:53:45.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:32.99996948335317 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 10
[MASTER] 13:53:45.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99996948335317, number of tests: 6, total length: 156
[MASTER] 13:53:45.434 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.8333333333333335 - fitness:27.796618357487922 - branchDistance:0.0 - coverage:11.144444444444444 - ex: 0 - tex: 10
[MASTER] 13:53:45.434 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.796618357487922, number of tests: 6, total length: 114
[MASTER] 13:53:45.566 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.8333333333333335 - fitness:24.652143396396646 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 13:53:45.566 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.652143396396646, number of tests: 6, total length: 88
[MASTER] 13:53:46.577 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.909090909090909 - fitness:17.684180009668957 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:46.577 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.684180009668957, number of tests: 5, total length: 80
[MASTER] 13:53:47.288 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.5 - fitness:16.99996948335317 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:47.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.99996948335317, number of tests: 5, total length: 68
[MASTER] 13:53:48.430 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.909090909090909 - fitness:16.586176379904895 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:48.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.586176379904895, number of tests: 5, total length: 56
[MASTER] 13:53:48.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.333333333333334 - fitness:14.806421096256395 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:48.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.806421096256395, number of tests: 5, total length: 69
[MASTER] 13:53:49.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.333333333333334 - fitness:14.294087130411992 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:49.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.294087130411992, number of tests: 5, total length: 76
[MASTER] 13:53:50.045 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.666666666666666 - fitness:14.142826626210312 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:50.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.142826626210312, number of tests: 4, total length: 56
[MASTER] 13:53:50.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.333333333333334 - fitness:13.864834348218032 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 13:53:50.258 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.864834348218032, number of tests: 5, total length: 76
[MASTER] 13:53:50.316 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.333333333333334 - fitness:13.186015994981076 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:50.316 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.186015994981076, number of tests: 5, total length: 79
[MASTER] 13:53:51.742 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.333333333333334 - fitness:12.913012961614038 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:51.742 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.913012961614038, number of tests: 4, total length: 62
[MASTER] 13:53:51.789 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.666666666666666 - fitness:12.829756717395723 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:53:51.789 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.829756717395723, number of tests: 4, total length: 53
[MASTER] 13:53:52.256 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.333333333333334 - fitness:12.67343887110827 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:52.256 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.67343887110827, number of tests: 5, total length: 60
[MASTER] 13:53:52.787 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.333333333333332 - fitness:12.46150794489163 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 13:53:52.787 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.46150794489163, number of tests: 4, total length: 59
[MASTER] 13:53:53.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.833333333333332 - fitness:12.025179567386783 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 13:53:53.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.025179567386783, number of tests: 4, total length: 61
[MASTER] 13:53:54.521 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.833333333333332 - fitness:11.879969483353168 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 13:53:54.521 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.879969483353168, number of tests: 4, total length: 62
[MASTER] 13:53:54.689 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.833333333333332 - fitness:11.748061086406604 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 13:53:54.689 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.748061086406604, number of tests: 5, total length: 114
[MASTER] 13:53:56.360 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.5 - fitness:11.448949075189903 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 13:53:56.360 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.448949075189903, number of tests: 5, total length: 99
[MASTER] 13:53:58.357 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.5 - fitness:11.264120426749395 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:58.357 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.264120426749395, number of tests: 3, total length: 57
[MASTER] 13:53:58.684 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.9 - fitness:11.230452754728635 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:58.684 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.230452754728635, number of tests: 3, total length: 57
[MASTER] 13:53:59.121 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.003987240829346 - fitness:11.14252152367552 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:53:59.121 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.14252152367552, number of tests: 2, total length: 59
[MASTER] 13:53:59.557 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.5 - fitness:11.105232641247905 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 13:53:59.557 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.105232641247905, number of tests: 3, total length: 95
[MASTER] 13:54:01.499 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.5 - fitness:10.967182598107268 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:01.499 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.967182598107268, number of tests: 2, total length: 58
[MASTER] 13:54:03.359 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.833333333333332 - fitness:10.945915429299115 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:03.359 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.945915429299115, number of tests: 2, total length: 54
[MASTER] 13:54:04.121 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.354545454545455 - fitness:10.854422981526813 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:04.121 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.854422981526813, number of tests: 2, total length: 53
[MASTER] 13:54:05.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.404545454545456 - fitness:10.851561572005155 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:05.479 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.851561572005155, number of tests: 2, total length: 53
[MASTER] 13:54:05.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.35454545454545 - fitness:10.798824757269768 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:05.646 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.798824757269768, number of tests: 2, total length: 52
[MASTER] 13:54:05.862 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.854545446085986 - fitness:10.772257346309448 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:05.862 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.772257346309448, number of tests: 2, total length: 55
[MASTER] 13:54:06.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.35454545454545 - fitness:10.746463264776827 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:06.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.746463264776827, number of tests: 2, total length: 52
[MASTER] 13:54:07.463 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 40.887878787770546 - fitness:10.432364766580484 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:07.463 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.432364766580484, number of tests: 2, total length: 52
[MASTER] 13:54:08.248 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 40.89696969686146 - fitness:10.432053961875601 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:08.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.432053961875601, number of tests: 2, total length: 51
[MASTER] 13:54:09.187 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 43.89696969686146 - fitness:10.336362571909307 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:09.187 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.336362571909307, number of tests: 2, total length: 50
[MASTER] 13:54:15.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 43.93787878777055 - fitness:10.335145989644527 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:15.412 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.335145989644527, number of tests: 2, total length: 44
[MASTER] 13:54:15.455 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 43.95701754375141 - fitness:10.334577588564086 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:15.455 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.334577588564086, number of tests: 2, total length: 45
[MASTER] 13:54:16.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 44.987320574054436 - fitness:10.304676938604462 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:16.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.304676938604462, number of tests: 2, total length: 44
[MASTER] 13:54:16.360 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 45.95701754375141 - fitness:10.277733769470702 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:16.360 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.277733769470702, number of tests: 2, total length: 45
[MASTER] 13:54:16.547 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 44.987320574054436 - fitness:7.304676938604462 - branchDistance:0.0 - coverage:4.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:16.547 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.304676938604462, number of tests: 2, total length: 45
[MASTER] 13:54:17.064 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 45.95701754375141 - fitness:7.277733769470702 - branchDistance:0.0 - coverage:4.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:17.064 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.277733769470702, number of tests: 2, total length: 46
[MASTER] 13:54:17.645 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 45.95701754375141 - fitness:6.277733769470702 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:17.645 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.277733769470702, number of tests: 2, total length: 48
[MASTER] 13:54:17.674 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 45.980701754277725 - fitness:6.277089614802394 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:17.674 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.277089614802394, number of tests: 3, total length: 62
[MASTER] 13:54:25.194 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 46.980701754277725 - fitness:6.25047224396881 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:25.195 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.25047224396881, number of tests: 2, total length: 44
[MASTER] 13:54:28.075 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 47.201754385856674 - fitness:6.244737454238585 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:28.075 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.244737454238585, number of tests: 2, total length: 44
[MASTER] 13:54:29.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 47.21805650479037 - fitness:6.244316608667917 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:29.008 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.244316608667917, number of tests: 2, total length: 51
[MASTER] 13:54:32.734 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 47.227455001030975 - fitness:6.244074112111153 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:32.734 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.244074112111153, number of tests: 2, total length: 44
[MASTER] 13:54:34.658 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 48.227455001030975 - fitness:6.218801535482279 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:34.658 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.218801535482279, number of tests: 2, total length: 45
[MASTER] 13:54:40.191 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 50.227455001030975 - fitness:6.171216425033026 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:40.191 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.171216425033026, number of tests: 2, total length: 44
[MASTER] 13:54:43.344 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 50.240257276991144 - fitness:6.170923791323483 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:43.344 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.170923791323483, number of tests: 2, total length: 44
[MASTER] 13:54:46.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 51.240257276991144 - fitness:6.148509003016419 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:54:46.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.148509003016419, number of tests: 2, total length: 44
[MASTER] 13:54:49.810 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 51.240257277026195 - fitness:6.148509003015649 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:54:49.810 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.148509003015649, number of tests: 2, total length: 50
[MASTER] 13:55:08.866 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 52.240257277026195 - fitness:6.126936238750325 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:55:08.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.126936238750325, number of tests: 2, total length: 45
[MASTER] 13:55:10.848 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 52.24433682827556 - fitness:6.126849891189084 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:55:10.848 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.126849891189084, number of tests: 2, total length: 46
[MASTER] 13:55:11.381 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 53.240257277026195 - fitness:6.106158926621415 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:55:11.381 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.106158926621415, number of tests: 2, total length: 45
[MASTER] 13:55:35.880 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 53.24692147547121 - fitness:6.106023031979522 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:55:35.881 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.106023031979522, number of tests: 2, total length: 44
[MASTER] 13:55:57.115 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 54.22122086029692 - fitness:6.086508300591434 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:55:57.115 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.086508300591434, number of tests: 2, total length: 46
[MASTER] 13:56:04.152 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 54.24692147547121 - fitness:6.086002847703417 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:56:04.152 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.086002847703417, number of tests: 2, total length: 50
[MASTER] 13:56:09.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 54.365107729462856 - fitness:6.0836845244795255 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:56:09.762 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.0836845244795255, number of tests: 2, total length: 45
[MASTER] 13:56:16.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 54.43265586103286 - fitness:6.082363950441617 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:56:16.977 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.082363950441617, number of tests: 2, total length: 45
[MASTER] 13:56:38.968 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 54.44545813699304 - fitness:6.082114027127227 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:56:38.969 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.082114027127227, number of tests: 3, total length: 68
[MASTER] 13:57:51.212 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 54.452122335438055 - fitness:6.081983975730055 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 4
[MASTER] 13:57:51.212 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.081983975730055, number of tests: 3, total length: 59
[MASTER] 13:58:37.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 55.452122335438055 - fitness:6.062817034831934 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 13:58:37.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.062817034831934, number of tests: 2, total length: 45
[MASTER] 13:58:41.323 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 763 generations, 628550 statements, best individuals have fitness: 6.062817034831934
[MASTER] 13:58:41.326 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 89%
* Total number of goals: 28
* Number of covered goals: 25
* Generated 2 tests with total length 45
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                      6 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 13:58:41.720 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:58:41.784 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 43 
[MASTER] 13:58:41.970 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
* Going to analyze the coverage criteria
[MASTER] 13:58:41.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 13:58:41.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
* Coverage analysis for criterion REGRESSION
[MASTER] 13:58:41.971 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 13:58:41.972 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
