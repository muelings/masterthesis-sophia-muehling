* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting client
* Connecting to master process on port 6960
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 100
[MASTER] 14:09:21.868 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:09:23.271 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:09:24.119 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:09:24.126 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 14:09:24.331 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:09:24.337 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 14:09:24.338 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 180 | Tests with assertion: 1
* Generated 1 tests with total length 7
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
[MASTER] 14:09:24.833 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:09:24.841 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 3 
[MASTER] 14:09:24.846 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 14:09:24.846 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException]
[MASTER] 14:09:24.846 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 2
[MASTER] 14:09:24.846 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException]
[MASTER] 14:09:24.875 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:09:24.878 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 40%
* Total number of goals: 100
* Number of covered goals: 40
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
