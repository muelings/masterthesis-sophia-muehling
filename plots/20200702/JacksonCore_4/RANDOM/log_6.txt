* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 4195
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 12:19:47.521 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 141
[MASTER] 12:21:20.196 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 12:21:21.321 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:21:21.326 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 12:21:21.599 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 0 assertions.
[MASTER] 12:24:33.703 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 12:24:33.874 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
Keeping 1 assertions.
[MASTER] 12:24:33.993 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 12:24:33.994 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 80346 | Tests with assertion: 1
* Generated 1 tests with total length 15
* GA-Budget:
	- MaxTime :                        286 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 12:24:34.752 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:24:34.753 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 169 seconds more than allowed.
[MASTER] 12:24:34.762 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 12:24:34.770 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 12:24:34.770 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [StringIndexOutOfBoundsException:append-478,StringIndexOutOfBoundsException:append-478, append:StringIndexOutOfBoundsException]
[MASTER] 12:24:34.770 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: append:StringIndexOutOfBoundsException at 10
[MASTER] 12:24:34.770 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [StringIndexOutOfBoundsException:append-478,StringIndexOutOfBoundsException:append-478, append:StringIndexOutOfBoundsException]
[MASTER] 12:24:34.931 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
[MASTER] 12:24:34.935 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 141
* Number of covered goals: 9
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
