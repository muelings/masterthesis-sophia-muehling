* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 20810
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 141
[MASTER] 12:00:24.563 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:00:50.164 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 12:00:50.164 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 12:00:50.182 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:00:50.182 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 12:00:51.197 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:00:51.207 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 12:00:51.207 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 12:00:51.208 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:00:51.208 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 12:00:51.500 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:00:51.512 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 12:00:51.512 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 12:00:51.513 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:00:51.513 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 12:00:51.514 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 6118 | Tests with assertion: 1
* Generated 1 tests with total length 23
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                         26 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:00:52.142 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:00:52.156 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 19 
[MASTER] 12:00:52.164 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[size]}
[MASTER] 12:00:52.172 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 12:00:52.172 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 12:00:52.172 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 12:00:52.176 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 141
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
