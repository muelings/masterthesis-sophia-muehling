/*
 * This file was automatically generated by EvoSuite
 * Fri Jul 03 00:49:18 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Parser_ESTest extends Parser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      BasicParser basicParser0 = new BasicParser();
      Option option0 = new Option((String) null, (String) null);
      Options options0 = new Options();
      option0.setRequired(true);
      Options options1 = options0.addOption(option0);
      String[] stringArray0 = new String[0];
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.apache.commons.cli.MissingOptionException : Missing required option: null
      // Modified Version:
      //     org.apache.commons.cli.MissingOptionException : null
      try { 
        basicParser0.parse(options1, stringArray0);
        fail("Expecting exception: Exception");
      
      } catch(Exception e) {
         //
         // Missing required option: null
         //
         verifyException("org.apache.commons.cli.Parser", e);
         assertTrue(e.getMessage().equals("Missing required option: null"));   
      }
  }
}
