* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 21827
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:01:08.017 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:01:08.023 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 74
* Using seed 1593738065603
* Starting evolution
[MASTER] 03:01:09.123 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:311.0 - branchDistance:0.0 - coverage:154.0 - ex: 0 - tex: 4
[MASTER] 03:01:09.123 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 311.0, number of tests: 2, total length: 58
[MASTER] 03:01:09.707 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:295.0 - branchDistance:0.0 - coverage:138.0 - ex: 0 - tex: 14
[MASTER] 03:01:09.707 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 295.0, number of tests: 8, total length: 212
[MASTER] 03:01:09.796 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.999999998603 - branchDistance:0.0 - coverage:108.99999999860302 - ex: 0 - tex: 8
[MASTER] 03:01:09.797 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.999999998603, number of tests: 6, total length: 127
[MASTER] 03:01:09.888 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:264.99999995327437 - branchDistance:0.0 - coverage:107.9999999532744 - ex: 0 - tex: 18
[MASTER] 03:01:09.888 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 264.99999995327437, number of tests: 10, total length: 245
[MASTER] 03:01:09.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.99999995292586 - branchDistance:0.0 - coverage:88.99999995292588 - ex: 0 - tex: 18
[MASTER] 03:01:09.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.99999995292586, number of tests: 10, total length: 234
[MASTER] 03:01:10.528 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.999999358641 - branchDistance:0.0 - coverage:88.999999358641 - ex: 0 - tex: 8
[MASTER] 03:01:10.529 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.999999358641, number of tests: 7, total length: 150
[MASTER] 03:01:11.373 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:244.9999999962747 - branchDistance:0.0 - coverage:87.99999999627471 - ex: 0 - tex: 14
[MASTER] 03:01:11.373 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 244.9999999962747, number of tests: 9, total length: 206
[MASTER] 03:01:11.659 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:244.99999985863855 - branchDistance:0.0 - coverage:87.99999985863856 - ex: 0 - tex: 10
[MASTER] 03:01:11.659 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 244.99999985863855, number of tests: 8, total length: 195
[MASTER] 03:01:12.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:242.99999991143972 - branchDistance:0.0 - coverage:85.9999999114397 - ex: 0 - tex: 10
[MASTER] 03:01:12.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 242.99999991143972, number of tests: 9, total length: 230
[MASTER] 03:01:12.602 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:242.99999986003553 - branchDistance:0.0 - coverage:85.99999986003554 - ex: 0 - tex: 12
[MASTER] 03:01:12.602 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 242.99999986003553, number of tests: 9, total length: 220
[MASTER] 03:01:13.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:241.99999986010528 - branchDistance:0.0 - coverage:84.99999986010528 - ex: 0 - tex: 8
[MASTER] 03:01:13.576 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 241.99999986010528, number of tests: 7, total length: 188
[MASTER] 03:01:13.780 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:239.9999998605012 - branchDistance:0.0 - coverage:82.9999998605012 - ex: 0 - tex: 8
[MASTER] 03:01:13.780 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 239.9999998605012, number of tests: 8, total length: 207
[MASTER] 03:01:14.453 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:239.99999951943767 - branchDistance:0.0 - coverage:82.99999951943767 - ex: 0 - tex: 14
[MASTER] 03:01:14.453 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 239.99999951943767, number of tests: 9, total length: 246
[MASTER] 03:01:14.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:238.99999991004273 - branchDistance:0.0 - coverage:81.99999991004272 - ex: 0 - tex: 10
[MASTER] 03:01:14.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 238.99999991004273, number of tests: 9, total length: 232
[MASTER] 03:01:14.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.99999986189817 - branchDistance:0.0 - coverage:80.99999986189819 - ex: 0 - tex: 8
[MASTER] 03:01:14.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.99999986189817, number of tests: 9, total length: 218
[MASTER] 03:01:15.289 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:234.99999986189817 - branchDistance:0.0 - coverage:77.99999986189819 - ex: 0 - tex: 12
[MASTER] 03:01:15.290 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 234.99999986189817, number of tests: 8, total length: 229
[MASTER] 03:01:16.532 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:146.99999986189817 - branchDistance:0.0 - coverage:80.99999986189819 - ex: 0 - tex: 12
[MASTER] 03:01:16.532 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.99999986189817, number of tests: 8, total length: 234
[MASTER] 03:01:17.955 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:143.99999986189817 - branchDistance:0.0 - coverage:77.99999986189819 - ex: 0 - tex: 16
[MASTER] 03:01:17.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.99999986189817, number of tests: 10, total length: 270
[MASTER] 03:01:18.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:142.99999983919167 - branchDistance:0.0 - coverage:76.99999983919166 - ex: 0 - tex: 14
[MASTER] 03:01:18.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.99999983919167, number of tests: 10, total length: 288
[MASTER] 03:01:19.694 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:139.99999983919167 - branchDistance:0.0 - coverage:73.99999983919166 - ex: 0 - tex: 16
[MASTER] 03:01:19.694 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.99999983919167, number of tests: 11, total length: 281
[MASTER] 03:01:19.753 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:138.99999983919167 - branchDistance:0.0 - coverage:72.99999983919166 - ex: 0 - tex: 12
[MASTER] 03:01:19.753 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.99999983919167, number of tests: 10, total length: 262
[MASTER] 03:01:20.695 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:136.0 - branchDistance:0.0 - coverage:70.0 - ex: 0 - tex: 18
[MASTER] 03:01:20.695 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.0, number of tests: 12, total length: 318
[MASTER] 03:01:21.867 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:133.99999983919167 - branchDistance:0.0 - coverage:67.99999983919166 - ex: 0 - tex: 18
[MASTER] 03:01:21.867 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.99999983919167, number of tests: 11, total length: 285
[MASTER] 03:01:22.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:132.99999983919167 - branchDistance:0.0 - coverage:66.99999983919166 - ex: 0 - tex: 16
[MASTER] 03:01:22.569 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.99999983919167, number of tests: 11, total length: 293
[MASTER] 03:01:23.446 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:131.99999983919167 - branchDistance:0.0 - coverage:65.99999983919166 - ex: 0 - tex: 16
[MASTER] 03:01:23.446 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.99999983919167, number of tests: 11, total length: 275
[MASTER] 03:01:26.930 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:130.99999983919167 - branchDistance:0.0 - coverage:64.99999983919166 - ex: 0 - tex: 16
[MASTER] 03:01:26.930 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.99999983919167, number of tests: 11, total length: 255
[MASTER] 03:01:27.601 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:128.99999999860302 - branchDistance:0.0 - coverage:62.999999998603016 - ex: 0 - tex: 18
[MASTER] 03:01:27.601 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.99999999860302, number of tests: 11, total length: 259
[MASTER] 03:01:28.524 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:127.99999999860302 - branchDistance:0.0 - coverage:61.999999998603016 - ex: 0 - tex: 18
[MASTER] 03:01:28.524 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.99999999860302, number of tests: 11, total length: 261
[MASTER] 03:01:31.226 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:127.99999999823784 - branchDistance:0.0 - coverage:61.99999999823784 - ex: 0 - tex: 16
[MASTER] 03:01:31.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.99999999823784, number of tests: 11, total length: 256
[MASTER] 03:01:31.362 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:127.99999999814715 - branchDistance:0.0 - coverage:61.999999998147146 - ex: 0 - tex: 18
[MASTER] 03:01:31.362 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.99999999814715, number of tests: 12, total length: 275
[MASTER] 03:01:31.853 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.99999999819295 - branchDistance:0.0 - coverage:58.99999999819294 - ex: 0 - tex: 18
[MASTER] 03:01:31.853 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.99999999819295, number of tests: 12, total length: 267
[MASTER] 03:01:33.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.99999999817497 - branchDistance:0.0 - coverage:58.99999999817497 - ex: 0 - tex: 20
[MASTER] 03:01:33.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.99999999817497, number of tests: 13, total length: 316
[MASTER] 03:01:34.390 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.99999983919166 - branchDistance:0.0 - coverage:58.99999983919166 - ex: 0 - tex: 20
[MASTER] 03:01:34.390 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.99999983919166, number of tests: 13, total length: 304
[MASTER] 03:01:34.462 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.49999999817497 - branchDistance:0.0 - coverage:58.49999999817497 - ex: 0 - tex: 20
[MASTER] 03:01:34.462 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.49999999817497, number of tests: 13, total length: 293
[MASTER] 03:01:34.582 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:122.0 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 16
[MASTER] 03:01:34.582 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.0, number of tests: 12, total length: 262
[MASTER] 03:01:35.338 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:121.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 18
[MASTER] 03:01:35.338 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 13, total length: 287
[MASTER] 03:01:36.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.5 - branchDistance:0.0 - coverage:54.5 - ex: 0 - tex: 18
[MASTER] 03:01:36.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.5, number of tests: 13, total length: 297
[MASTER] 03:01:37.029 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.0 - branchDistance:0.0 - coverage:54.0 - ex: 0 - tex: 18
[MASTER] 03:01:37.030 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.0, number of tests: 13, total length: 286
[MASTER] 03:01:38.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:119.5 - branchDistance:0.0 - coverage:53.5 - ex: 0 - tex: 20
[MASTER] 03:01:38.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.5, number of tests: 14, total length: 324
[MASTER] 03:01:54.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:118.5 - branchDistance:0.0 - coverage:52.5 - ex: 0 - tex: 16
[MASTER] 03:01:54.646 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.5, number of tests: 14, total length: 292
[MASTER] 03:01:57.256 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:116.5 - branchDistance:0.0 - coverage:50.5 - ex: 0 - tex: 16
[MASTER] 03:01:57.256 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.5, number of tests: 14, total length: 282
[MASTER] 03:02:01.745 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:115.5 - branchDistance:0.0 - coverage:49.5 - ex: 0 - tex: 18
[MASTER] 03:02:01.745 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.5, number of tests: 15, total length: 302
[MASTER] 03:02:06.042 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:115.0 - branchDistance:0.0 - coverage:49.0 - ex: 0 - tex: 18
[MASTER] 03:02:06.042 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.0, number of tests: 14, total length: 330

[MASTER] 03:06:09.075 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 995 generations, 1009210 statements, best individuals have fitness: 115.0
[MASTER] 03:06:09.081 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 64%
* Total number of goals: 74
* Number of covered goals: 47
* Generated 12 tests with total length 155
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                    115 / 0           
[MASTER] 03:06:09.303 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:06:09.338 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 123 
[MASTER] 03:06:09.405 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.406 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.406 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 1.0
[MASTER] 03:06:09.406 [logback-1] WARN  RegressionSuiteMinimizer - Test2, uniqueExceptions: [parse:MissingOptionException]
[MASTER] 03:06:09.407 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 8
[MASTER] 03:06:09.407 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 1.0
[MASTER] 03:06:09.407 [logback-1] WARN  RegressionSuiteMinimizer - Test3, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 03:06:09.407 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 6
[MASTER] 03:06:09.407 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 6
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:06:09.410 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 03:06:09.411 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:06:09.411 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 03:06:09.411 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 03:06:09.411 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 03:06:09.465 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
[MASTER] 03:06:09.466 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 22%
* Total number of goals: 74
* Number of covered goals: 16
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 9
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
