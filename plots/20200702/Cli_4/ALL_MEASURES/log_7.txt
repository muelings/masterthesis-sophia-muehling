* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 3734
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:14:09.182 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:14:09.188 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 74
* Using seed 1593738846757
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 03:14:10.256 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:311.0 - branchDistance:0.0 - coverage:154.0 - ex: 0 - tex: 2
[MASTER] 03:14:10.257 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 311.0, number of tests: 1, total length: 25
[MASTER] 03:14:10.789 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:266.99999999721143 - branchDistance:0.0 - coverage:109.99999999721143 - ex: 0 - tex: 10
[MASTER] 03:14:10.789 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 266.99999999721143, number of tests: 6, total length: 151
[MASTER] 03:14:10.900 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:266.99999995514133 - branchDistance:0.0 - coverage:109.9999999551413 - ex: 0 - tex: 14
[MASTER] 03:14:10.900 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 266.99999995514133, number of tests: 9, total length: 196
[MASTER] 03:14:10.957 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:266.99999989954676 - branchDistance:0.0 - coverage:109.99999989954675 - ex: 0 - tex: 18
[MASTER] 03:14:10.957 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 266.99999989954676, number of tests: 10, total length: 265
[MASTER] 03:14:11.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.99999997286704 - branchDistance:0.0 - coverage:108.99999997286702 - ex: 0 - tex: 10
[MASTER] 03:14:11.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.99999997286704, number of tests: 7, total length: 141
[MASTER] 03:14:11.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:260.99999987566844 - branchDistance:0.0 - coverage:103.99999987566845 - ex: 0 - tex: 12
[MASTER] 03:14:11.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 260.99999987566844, number of tests: 8, total length: 194
[MASTER] 03:14:11.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:247.99999997688388 - branchDistance:0.0 - coverage:90.99999997688387 - ex: 0 - tex: 16
[MASTER] 03:14:11.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 247.99999997688388, number of tests: 9, total length: 246
[MASTER] 03:14:11.908 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:246.9999999962747 - branchDistance:0.0 - coverage:89.99999999627471 - ex: 0 - tex: 8
[MASTER] 03:14:11.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 246.9999999962747, number of tests: 4, total length: 103
[MASTER] 03:14:12.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.9999999962747 - branchDistance:0.0 - coverage:88.99999999627471 - ex: 0 - tex: 8
[MASTER] 03:14:12.976 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.9999999962747, number of tests: 4, total length: 109
[MASTER] 03:14:13.283 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.9999996783834 - branchDistance:0.0 - coverage:88.99999967838339 - ex: 0 - tex: 8
[MASTER] 03:14:13.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.9999996783834, number of tests: 4, total length: 105
[MASTER] 03:14:13.673 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:243.9999999962747 - branchDistance:0.0 - coverage:86.99999999627471 - ex: 0 - tex: 10
[MASTER] 03:14:13.673 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.9999999962747, number of tests: 5, total length: 135
[MASTER] 03:14:13.935 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:242.9999996783834 - branchDistance:0.0 - coverage:85.99999967838339 - ex: 0 - tex: 8
[MASTER] 03:14:13.935 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 242.9999996783834, number of tests: 4, total length: 104
[MASTER] 03:14:14.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:231.99999999813735 - branchDistance:0.0 - coverage:74.99999999813735 - ex: 0 - tex: 8
[MASTER] 03:14:14.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 231.99999999813735, number of tests: 5, total length: 132
[MASTER] 03:14:15.118 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:224.99999999813735 - branchDistance:0.0 - coverage:67.99999999813735 - ex: 0 - tex: 8
[MASTER] 03:14:15.118 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 224.99999999813735, number of tests: 5, total length: 134
[MASTER] 03:14:16.650 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.99999999813735 - branchDistance:0.0 - coverage:66.99999999813735 - ex: 0 - tex: 10
[MASTER] 03:14:16.650 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.99999999813735, number of tests: 6, total length: 171
[MASTER] 03:14:16.887 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.99999997826916 - branchDistance:0.0 - coverage:66.99999997826914 - ex: 0 - tex: 8
[MASTER] 03:14:16.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.99999997826916, number of tests: 5, total length: 136
[MASTER] 03:14:16.996 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:221.99999999813735 - branchDistance:0.0 - coverage:64.99999999813735 - ex: 0 - tex: 8
[MASTER] 03:14:16.996 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 221.99999999813735, number of tests: 5, total length: 135
[MASTER] 03:14:17.519 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:221.99999997826916 - branchDistance:0.0 - coverage:64.99999997826914 - ex: 0 - tex: 8
[MASTER] 03:14:17.519 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 221.99999997826916, number of tests: 5, total length: 136
[MASTER] 03:14:18.142 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:214.99999998089288 - branchDistance:0.0 - coverage:57.999999980892866 - ex: 0 - tex: 8
[MASTER] 03:14:18.143 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 214.99999998089288, number of tests: 6, total length: 159
[MASTER] 03:14:18.642 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:211.99999996102466 - branchDistance:0.0 - coverage:54.999999961024656 - ex: 0 - tex: 10
[MASTER] 03:14:18.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 211.99999996102466, number of tests: 7, total length: 186
[MASTER] 03:14:19.512 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:210.99999997826916 - branchDistance:0.0 - coverage:53.999999978269145 - ex: 0 - tex: 10
[MASTER] 03:14:19.512 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 210.99999997826916, number of tests: 7, total length: 190
[MASTER] 03:14:20.601 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:210.99999996102466 - branchDistance:0.0 - coverage:53.999999961024656 - ex: 0 - tex: 8
[MASTER] 03:14:20.602 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 210.99999996102466, number of tests: 6, total length: 166
[MASTER] 03:14:21.205 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.99999997826916 - branchDistance:0.0 - coverage:52.999999978269145 - ex: 0 - tex: 8
[MASTER] 03:14:21.205 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.99999997826916, number of tests: 7, total length: 186
[MASTER] 03:14:22.038 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.99999996102466 - branchDistance:0.0 - coverage:52.999999961024656 - ex: 0 - tex: 10
[MASTER] 03:14:22.038 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.99999996102466, number of tests: 7, total length: 175
[MASTER] 03:14:22.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.99999975971883 - branchDistance:0.0 - coverage:52.99999975971883 - ex: 0 - tex: 12
[MASTER] 03:14:22.851 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.99999975971883, number of tests: 8, total length: 206
[MASTER] 03:14:22.996 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:208.99999999813735 - branchDistance:0.0 - coverage:51.999999998137355 - ex: 0 - tex: 10
[MASTER] 03:14:22.996 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 208.99999999813735, number of tests: 7, total length: 195
[MASTER] 03:14:23.303 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:208.99999997826916 - branchDistance:0.0 - coverage:51.999999978269145 - ex: 0 - tex: 10
[MASTER] 03:14:23.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 208.99999997826916, number of tests: 8, total length: 208
[MASTER] 03:14:26.543 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:207.99999997826916 - branchDistance:0.0 - coverage:50.999999978269145 - ex: 0 - tex: 12
[MASTER] 03:14:26.543 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 207.99999997826916, number of tests: 7, total length: 180
[MASTER] 03:14:32.861 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:206.99999997826913 - branchDistance:0.0 - coverage:49.99999997826914 - ex: 0 - tex: 14
[MASTER] 03:14:32.861 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 206.99999997826913, number of tests: 7, total length: 175
[MASTER] 03:14:34.129 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:206.9999999742955 - branchDistance:0.0 - coverage:49.9999999742955 - ex: 0 - tex: 16
[MASTER] 03:14:34.129 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 206.9999999742955, number of tests: 8, total length: 208
[MASTER] 03:14:35.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:202.0 - branchDistance:0.0 - coverage:45.0 - ex: 0 - tex: 16
[MASTER] 03:14:35.180 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 202.0, number of tests: 9, total length: 233
[MASTER] 03:14:58.803 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:112.0 - branchDistance:0.0 - coverage:46.0 - ex: 0 - tex: 14
[MASTER] 03:14:58.803 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.0, number of tests: 8, total length: 213
[MASTER] 03:15:00.547 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:111.0 - branchDistance:0.0 - coverage:45.0 - ex: 0 - tex: 14
[MASTER] 03:15:00.547 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.0, number of tests: 8, total length: 213
[MASTER] 03:15:01.988 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.8 - fitness:91.05263157894737 - branchDistance:0.0 - coverage:49.0 - ex: 0 - tex: 14
[MASTER] 03:15:01.988 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.05263157894737, number of tests: 8, total length: 216
[MASTER] 03:15:02.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.8 - fitness:90.05263157894737 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 14
[MASTER] 03:15:02.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.05263157894737, number of tests: 8, total length: 217
[MASTER] 03:15:03.082 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.8 - fitness:87.05263157894737 - branchDistance:0.0 - coverage:45.0 - ex: 0 - tex: 16
[MASTER] 03:15:03.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.05263157894737, number of tests: 9, total length: 237
[MASTER] 03:15:07.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.8 - fitness:86.05263157894737 - branchDistance:0.0 - coverage:44.0 - ex: 0 - tex: 18
[MASTER] 03:15:07.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.05263157894737, number of tests: 9, total length: 246
[MASTER] 03:15:14.263 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:76.0 - branchDistance:0.0 - coverage:45.0 - ex: 0 - tex: 18
[MASTER] 03:15:14.263 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.0, number of tests: 9, total length: 225
[MASTER] 03:15:15.039 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:75.0 - branchDistance:0.0 - coverage:44.0 - ex: 0 - tex: 16
[MASTER] 03:15:15.039 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.0, number of tests: 8, total length: 206
[MASTER] 03:15:36.123 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:71.0 - branchDistance:0.0 - coverage:40.0 - ex: 0 - tex: 16
[MASTER] 03:15:36.123 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.0, number of tests: 8, total length: 199
[MASTER] 03:15:39.279 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:70.0 - branchDistance:0.0 - coverage:39.0 - ex: 0 - tex: 18
[MASTER] 03:15:39.279 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.0, number of tests: 9, total length: 225
[MASTER] 03:15:40.009 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:69.0 - branchDistance:0.0 - coverage:38.0 - ex: 0 - tex: 18
[MASTER] 03:15:40.009 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.0, number of tests: 9, total length: 223
[MASTER] 03:15:43.666 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:68.5 - branchDistance:0.0 - coverage:37.5 - ex: 0 - tex: 22
[MASTER] 03:15:43.666 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.5, number of tests: 11, total length: 276
[MASTER] 03:16:13.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:65.5 - branchDistance:0.0 - coverage:34.5 - ex: 0 - tex: 20
[MASTER] 03:16:13.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.5, number of tests: 11, total length: 266
[MASTER] 03:16:14.179 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:64.5 - branchDistance:0.0 - coverage:33.5 - ex: 0 - tex: 18
[MASTER] 03:16:14.179 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.5, number of tests: 11, total length: 258
[MASTER] 03:16:27.763 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:62.5 - branchDistance:0.0 - coverage:31.5 - ex: 0 - tex: 20
[MASTER] 03:16:27.763 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.5, number of tests: 12, total length: 272
[MASTER] 03:16:29.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:61.5 - branchDistance:0.0 - coverage:30.5 - ex: 0 - tex: 20
[MASTER] 03:16:29.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.5, number of tests: 12, total length: 274
[MASTER] 03:16:30.361 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:61.0 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 18
[MASTER] 03:16:30.361 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 12, total length: 270
[MASTER] 03:16:31.987 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:60.5 - branchDistance:0.0 - coverage:29.5 - ex: 0 - tex: 20
[MASTER] 03:16:31.987 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.5, number of tests: 13, total length: 298
[MASTER] 03:16:52.500 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:59.5 - branchDistance:0.0 - coverage:28.5 - ex: 0 - tex: 22
[MASTER] 03:16:52.500 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.5, number of tests: 14, total length: 317
[MASTER] 03:17:18.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:58.5 - branchDistance:0.0 - coverage:27.5 - ex: 0 - tex: 18
[MASTER] 03:17:18.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.5, number of tests: 14, total length: 275
[MASTER] 03:17:25.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2 - fitness:58.0 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 18
[MASTER] 03:17:25.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.0, number of tests: 13, total length: 273
[MASTER] 03:19:10.213 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 968 generations, 1175954 statements, best individuals have fitness: 58.0
[MASTER] 03:19:10.217 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 80%
* Total number of goals: 74
* Number of covered goals: 59
* Generated 12 tests with total length 235
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     58 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 03:19:10.455 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:19:10.490 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 195 
[MASTER] 03:19:10.564 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.565 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.568 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.568 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.568 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.568 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.569 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 03:19:10.570 [logback-1] WARN  RegressionSuiteMinimizer - Test7, uniqueExceptions: []
[MASTER] 03:19:10.570 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 9
[MASTER] 03:19:10.570 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 1.0
[MASTER] 03:19:10.570 [logback-1] WARN  RegressionSuiteMinimizer - Test8, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 03:19:10.570 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 13
[MASTER] 03:19:10.571 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 13
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:19:10.577 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:19:10.578 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:19:10.578 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 03:19:10.578 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 03:19:10.578 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 03:19:10.695 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
[MASTER] 03:19:10.696 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 22%
* Total number of goals: 74
* Number of covered goals: 16
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
