* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting client
* Connecting to master process on port 9747
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 17:05:44.314 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 100
[MASTER] 17:05:44.668 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 17:05:45.683 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:05:45.703 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 17:05:46.035 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:05:46.059 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 17:05:46.060 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 5 | Tests with assertion: 1
* Generated 1 tests with total length 30
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          1 / 300         
[MASTER] 17:05:46.761 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:05:46.778 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 15 
[MASTER] 17:05:46.796 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 17:05:46.797 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragmentRelaxed:MockIllegalArgumentException, MockIllegalArgumentException,]
[MASTER] 17:05:46.797 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parseBodyFragmentRelaxed:MockIllegalArgumentException at 14
[MASTER] 17:05:46.797 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragmentRelaxed:MockIllegalArgumentException, MockIllegalArgumentException,]
[MASTER] 17:05:46.797 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 3
[MASTER] 17:05:46.797 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragmentRelaxed:MockIllegalArgumentException, MockIllegalArgumentException,]
[MASTER] 17:05:47.098 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
[MASTER] 17:05:47.105 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 38%
* Total number of goals: 100
* Number of covered goals: 38
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
