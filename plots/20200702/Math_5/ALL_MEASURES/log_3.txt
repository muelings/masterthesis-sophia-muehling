* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 20578
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 21:33:17.941 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 21:33:17.951 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 183
* Using seed 1593631994222
* Starting evolution
[MASTER] 21:33:20.197 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:632.9999999981374 - branchDistance:0.0 - coverage:203.99999999813735 - ex: 0 - tex: 0
[MASTER] 21:33:20.198 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 632.9999999981374, number of tests: 1, total length: 36
[MASTER] 21:33:20.368 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:533.9999999952572 - branchDistance:0.0 - coverage:104.99999999525724 - ex: 0 - tex: 2
[MASTER] 21:33:20.368 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 533.9999999952572, number of tests: 9, total length: 151
[MASTER] 21:33:20.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:528.9999999961791 - branchDistance:0.0 - coverage:99.99999999617913 - ex: 0 - tex: 2
[MASTER] 21:33:20.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 528.9999999961791, number of tests: 9, total length: 177
[MASTER] 21:33:21.123 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:177.59999999627473 - branchDistance:0.0 - coverage:90.99999999627471 - ex: 0 - tex: 4
[MASTER] 21:33:21.123 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 177.59999999627473, number of tests: 10, total length: 197
[MASTER] 21:33:21.537 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:168.93333332723319 - branchDistance:0.0 - coverage:82.33333332723316 - ex: 0 - tex: 0
[MASTER] 21:33:21.537 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 168.93333332723319, number of tests: 9, total length: 148
[MASTER] 21:33:22.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:140.6428571484513 - branchDistance:0.0 - coverage:78.49999999308658 - ex: 0 - tex: 6
[MASTER] 21:33:22.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.6428571484513, number of tests: 9, total length: 196
[MASTER] 21:33:26.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:138.64285715040708 - branchDistance:0.0 - coverage:76.49999999504234 - ex: 0 - tex: 6
[MASTER] 21:33:26.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.64285715040708, number of tests: 10, total length: 226
[MASTER] 21:33:28.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:130.14285715138496 - branchDistance:0.0 - coverage:67.99999999602024 - ex: 0 - tex: 2
[MASTER] 21:33:28.589 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.14285715138496, number of tests: 9, total length: 197
[MASTER] 21:33:31.195 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:129.64285715040708 - branchDistance:0.0 - coverage:67.49999999504234 - ex: 0 - tex: 6
[MASTER] 21:33:31.195 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.64285715040708, number of tests: 9, total length: 202
[MASTER] 21:33:31.245 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:120.64285715154631 - branchDistance:0.0 - coverage:58.49999999618157 - ex: 0 - tex: 6
[MASTER] 21:33:31.245 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.64285715154631, number of tests: 10, total length: 230
[MASTER] 21:33:31.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:113.14285520197049 - branchDistance:0.0 - coverage:50.99999804660577 - ex: 0 - tex: 2
[MASTER] 21:33:31.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.14285520197049, number of tests: 11, total length: 259
[MASTER] 21:33:33.583 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:108.64285715154631 - branchDistance:0.0 - coverage:46.49999999618158 - ex: 0 - tex: 4
[MASTER] 21:33:33.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.64285715154631, number of tests: 10, total length: 229
[MASTER] 21:33:36.816 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:106.64285715252419 - branchDistance:0.0 - coverage:44.49999999715946 - ex: 0 - tex: 2
[MASTER] 21:33:36.816 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.64285715252419, number of tests: 9, total length: 250
[MASTER] 21:33:38.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:102.14285610455556 - branchDistance:0.0 - coverage:39.999998949190825 - ex: 0 - tex: 2
[MASTER] 21:33:38.514 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.14285610455556, number of tests: 11, total length: 300
[MASTER] 21:33:40.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:101.64285520197271 - branchDistance:0.0 - coverage:39.499998046607985 - ex: 0 - tex: 2
[MASTER] 21:33:40.044 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.64285520197271, number of tests: 11, total length: 265
[MASTER] 21:33:44.048 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:101.14285520197049 - branchDistance:0.0 - coverage:38.99999804660577 - ex: 0 - tex: 2
[MASTER] 21:33:44.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.14285520197049, number of tests: 12, total length: 308
[MASTER] 21:33:44.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:93.1428571525242 - branchDistance:0.0 - coverage:30.999999997159467 - ex: 0 - tex: 2
[MASTER] 21:33:44.093 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.1428571525242, number of tests: 10, total length: 286
[MASTER] 21:33:48.090 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:91.6428571525242 - branchDistance:0.0 - coverage:29.499999997159467 - ex: 0 - tex: 2
[MASTER] 21:33:48.090 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.6428571525242, number of tests: 11, total length: 326
[MASTER] 21:33:52.282 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:88.33333333909104 - branchDistance:0.0 - coverage:33.83333332951491 - ex: 0 - tex: 0
[MASTER] 21:33:52.282 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.33333333909104, number of tests: 13, total length: 321
[MASTER] 21:33:54.992 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:86.33333333879949 - branchDistance:0.0 - coverage:31.833333329223372 - ex: 0 - tex: 0
[MASTER] 21:33:54.992 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.33333333879949, number of tests: 14, total length: 337
[MASTER] 21:33:58.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:85.83333333879949 - branchDistance:0.0 - coverage:31.333333329223372 - ex: 0 - tex: 2
[MASTER] 21:33:58.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.83333333879949, number of tests: 15, total length: 353
[MASTER] 21:34:01.575 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:85.3333333387995 - branchDistance:0.0 - coverage:30.833333329223375 - ex: 0 - tex: 0
[MASTER] 21:34:01.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.3333333387995, number of tests: 14, total length: 337
[MASTER] 21:34:02.103 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:84.8333333387995 - branchDistance:0.0 - coverage:30.333333329223375 - ex: 0 - tex: 0
[MASTER] 21:34:02.103 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.8333333387995, number of tests: 14, total length: 340
[MASTER] 21:34:02.146 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:83.83333333705293 - branchDistance:0.0 - coverage:29.33333332747681 - ex: 0 - tex: 0
[MASTER] 21:34:02.146 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.83333333705293, number of tests: 14, total length: 386
[MASTER] 21:34:04.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:83.00000000546616 - branchDistance:0.0 - coverage:28.49999999589004 - ex: 0 - tex: 2
[MASTER] 21:34:04.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.00000000546616, number of tests: 14, total length: 347
[MASTER] 21:34:04.973 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:80.83333333891558 - branchDistance:0.0 - coverage:26.333333329339457 - ex: 0 - tex: 0
[MASTER] 21:34:04.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.83333333891558, number of tests: 15, total length: 421
[MASTER] 21:34:05.485 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:80.50000000360352 - branchDistance:0.0 - coverage:25.999999994027394 - ex: 0 - tex: 2
[MASTER] 21:34:05.485 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.50000000360352, number of tests: 15, total length: 386
[MASTER] 21:34:09.488 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:78.49999996762386 - branchDistance:0.0 - coverage:23.999999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:09.488 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.49999996762386, number of tests: 14, total length: 408
[MASTER] 21:34:11.096 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:76.55555552214749 - branchDistance:0.0 - coverage:27.999999959025622 - ex: 0 - tex: 2
[MASTER] 21:34:11.097 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.55555552214749, number of tests: 13, total length: 369
[MASTER] 21:34:13.920 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:75.55555552214749 - branchDistance:0.0 - coverage:26.999999959025622 - ex: 0 - tex: 2
[MASTER] 21:34:13.920 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.55555552214749, number of tests: 14, total length: 375
[MASTER] 21:34:14.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:74.55555552214749 - branchDistance:0.0 - coverage:25.999999959025622 - ex: 0 - tex: 2
[MASTER] 21:34:14.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.55555552214749, number of tests: 14, total length: 377
[MASTER] 21:34:18.816 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:70.5555555211696 - branchDistance:0.0 - coverage:21.999999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:18.816 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.5555555211696, number of tests: 14, total length: 411
[MASTER] 21:34:23.342 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:70.0555555211696 - branchDistance:0.0 - coverage:21.499999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:23.342 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.0555555211696, number of tests: 15, total length: 444
[MASTER] 21:34:27.044 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:69.5555555211696 - branchDistance:0.0 - coverage:20.999999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:27.045 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.5555555211696, number of tests: 15, total length: 442
[MASTER] 21:34:30.317 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:68.0555555211696 - branchDistance:0.0 - coverage:19.499999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:30.318 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.0555555211696, number of tests: 16, total length: 437
[MASTER] 21:34:38.770 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:66.0555555211696 - branchDistance:0.0 - coverage:17.499999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:38.770 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0555555211696, number of tests: 14, total length: 388
[MASTER] 21:34:47.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:65.0555555211696 - branchDistance:0.0 - coverage:16.499999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:47.396 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0555555211696, number of tests: 14, total length: 394
[MASTER] 21:34:52.966 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:64.05555552214749 - branchDistance:0.0 - coverage:15.499999959025622 - ex: 0 - tex: 2
[MASTER] 21:34:52.966 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.05555552214749, number of tests: 15, total length: 412
[MASTER] 21:34:55.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:63.055555522147486 - branchDistance:0.0 - coverage:14.499999959025622 - ex: 0 - tex: 2
[MASTER] 21:34:55.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.055555522147486, number of tests: 15, total length: 414
[MASTER] 21:34:59.433 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:62.555555521169595 - branchDistance:0.0 - coverage:13.999999958047734 - ex: 0 - tex: 2
[MASTER] 21:34:59.434 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.555555521169595, number of tests: 15, total length: 429
[MASTER] 21:35:02.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:62.055555522147486 - branchDistance:0.0 - coverage:13.499999959025622 - ex: 0 - tex: 2
[MASTER] 21:35:02.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.055555522147486, number of tests: 16, total length: 446
[MASTER] 21:35:05.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:61.555555522147486 - branchDistance:0.0 - coverage:12.999999959025622 - ex: 0 - tex: 4
[MASTER] 21:35:05.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.555555522147486, number of tests: 16, total length: 441
[MASTER] 21:35:07.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:61.555555521169595 - branchDistance:0.0 - coverage:12.999999958047734 - ex: 0 - tex: 2
[MASTER] 21:35:07.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.555555521169595, number of tests: 15, total length: 430
[MASTER] 21:35:10.708 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:61.55511052333787 - branchDistance:0.0 - coverage:12.999554960216008 - ex: 0 - tex: 2
[MASTER] 21:35:10.708 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.55511052333787, number of tests: 16, total length: 466
[MASTER] 21:35:10.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:60.555555521169595 - branchDistance:0.0 - coverage:11.999999958047734 - ex: 0 - tex: 2
[MASTER] 21:35:10.799 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.555555521169595, number of tests: 16, total length: 462
[MASTER] 21:35:16.126 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:59.555555522147486 - branchDistance:0.0 - coverage:10.999999959025622 - ex: 0 - tex: 2
[MASTER] 21:35:16.126 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.555555522147486, number of tests: 16, total length: 456
[MASTER] 21:35:17.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:59.555110524315765 - branchDistance:0.0 - coverage:10.999554961193898 - ex: 0 - tex: 2
[MASTER] 21:35:17.093 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.555110524315765, number of tests: 16, total length: 475
[MASTER] 21:35:19.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:58.555555522147486 - branchDistance:0.0 - coverage:9.999999959025622 - ex: 0 - tex: 2
[MASTER] 21:35:19.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.555555522147486, number of tests: 16, total length: 467
[MASTER] 21:35:25.091 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:58.555546728143675 - branchDistance:0.0 - coverage:9.999991165021811 - ex: 0 - tex: 2
[MASTER] 21:35:25.091 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.555546728143675, number of tests: 16, total length: 466
[MASTER] 21:35:32.357 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:57.555555522147486 - branchDistance:0.0 - coverage:8.999999959025622 - ex: 0 - tex: 2
[MASTER] 21:35:32.357 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.555555522147486, number of tests: 16, total length: 466
[MASTER] 21:35:35.370 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:57.555546728143675 - branchDistance:0.0 - coverage:8.999991165021811 - ex: 0 - tex: 2
[MASTER] 21:35:35.370 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.555546728143675, number of tests: 16, total length: 467
[MASTER] 21:35:52.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:57.54074074732916 - branchDistance:0.0 - coverage:8.985185184207296 - ex: 0 - tex: 2
[MASTER] 21:35:52.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.54074074732916, number of tests: 17, total length: 493
[MASTER] 21:36:09.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:52.78518519033601 - branchDistance:0.0 - coverage:8.985185184207296 - ex: 0 - tex: 2
[MASTER] 21:36:09.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.78518519033601, number of tests: 16, total length: 465
[MASTER] 21:36:15.159 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:50.7851851913139 - branchDistance:0.0 - coverage:6.985185185185185 - ex: 0 - tex: 2
[MASTER] 21:36:15.159 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.7851851913139, number of tests: 17, total length: 501
[MASTER] 21:36:38.928 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.7851851913139 - branchDistance:0.0 - coverage:5.985185185185185 - ex: 0 - tex: 2
[MASTER] 21:36:38.928 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.7851851913139, number of tests: 17, total length: 499
[MASTER] 21:37:05.938 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:48.7851851913139 - branchDistance:0.0 - coverage:4.985185185185185 - ex: 0 - tex: 2
[MASTER] 21:37:05.938 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.7851851913139, number of tests: 16, total length: 456
[MASTER] 21:37:30.401 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.999999998568057 - fitness:44.89427609934115 - branchDistance:0.0 - coverage:4.985185185185185 - ex: 0 - tex: 2
[MASTER] 21:37:30.401 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 44.89427609934115, number of tests: 16, total length: 448
[MASTER] 21:37:38.758 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.999999998568057 - fitness:43.89427609934115 - branchDistance:0.0 - coverage:3.985185185185185 - ex: 0 - tex: 2
[MASTER] 21:37:38.758 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 43.89427609934115, number of tests: 16, total length: 446
[MASTER] 21:38:19.320 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 177 generations, 317758 statements, best individuals have fitness: 43.89427609934115
[MASTER] 21:38:19.326 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 98%
* Total number of goals: 183
* Number of covered goals: 180
* Generated 16 tests with total length 417
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                     44 / 0           
	- MaxTime :                        302 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 21:38:20.834 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:38:21.164 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 351 
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
* Going to analyze the coverage criteria
[MASTER] 21:38:21.969 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
* Number of covered goals: 0
[MASTER] 21:38:21.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
* Generated 0 tests with total length 0
[MASTER] 21:38:21.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
* Resulting test suite's coverage: 0%
[MASTER] 21:38:21.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 21:38:21.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 21:38:21.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 21:38:21.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 21:38:21.971 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Compiling and checking tests
[MASTER] 21:38:21.972 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
