* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 11303
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:35:47.691 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:35:47.696 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 128
* Using seed 1593668144714
* Starting evolution
[MASTER] 07:35:55.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:521.5011575042535 - branchDistance:0.0 - coverage:240.50115750425348 - ex: 0 - tex: 18
[MASTER] 07:35:55.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 521.5011575042535, number of tests: 9, total length: 239
[MASTER] 07:35:55.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:486.06660959602135 - branchDistance:0.0 - coverage:205.06660959602135 - ex: 0 - tex: 8
[MASTER] 07:35:55.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 486.06660959602135, number of tests: 4, total length: 83
[MASTER] 07:35:55.383 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9696969696969697 - fitness:308.35232388173563 - branchDistance:0.0 - coverage:213.06660959602135 - ex: 0 - tex: 18
[MASTER] 07:35:55.383 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 308.35232388173563, number of tests: 9, total length: 193
[MASTER] 07:35:55.789 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4923076923076923 - fitness:256.30869235339884 - branchDistance:0.0 - coverage:175.13248089965435 - ex: 0 - tex: 6
[MASTER] 07:35:55.790 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 256.30869235339884, number of tests: 5, total length: 120
[MASTER] 07:35:57.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4923076923076923 - fitness:252.92685289638354 - branchDistance:0.0 - coverage:171.75064144263902 - ex: 0 - tex: 16
[MASTER] 07:35:57.093 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 252.92685289638354, number of tests: 10, total length: 274
[MASTER] 07:35:57.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4923076923076923 - fitness:250.92685289638354 - branchDistance:0.0 - coverage:169.75064144263902 - ex: 0 - tex: 14
[MASTER] 07:35:57.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 250.92685289638354, number of tests: 10, total length: 252
[MASTER] 07:35:58.200 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4923076923076923 - fitness:237.50039974140935 - branchDistance:0.0 - coverage:156.32418828766487 - ex: 0 - tex: 16
[MASTER] 07:35:58.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.50039974140935, number of tests: 10, total length: 264
[MASTER] 07:35:58.241 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4444444444444446 - fitness:232.75064144263902 - branchDistance:0.0 - coverage:168.75064144263902 - ex: 0 - tex: 16
[MASTER] 07:35:58.241 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.75064144263902, number of tests: 10, total length: 241
[MASTER] 07:35:58.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4923076923076923 - fitness:225.46839111400504 - branchDistance:0.0 - coverage:144.29217966026053 - ex: 0 - tex: 8
[MASTER] 07:35:58.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.46839111400504, number of tests: 8, total length: 210
[MASTER] 07:36:00.325 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4444444444444446 - fitness:216.6336384039078 - branchDistance:0.0 - coverage:152.6336384039078 - ex: 0 - tex: 10
[MASTER] 07:36:00.325 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 216.6336384039078, number of tests: 7, total length: 177
[MASTER] 07:36:00.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4444444444444446 - fitness:216.21795098512442 - branchDistance:0.0 - coverage:152.21795098512442 - ex: 0 - tex: 12
[MASTER] 07:36:00.569 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 216.21795098512442, number of tests: 9, total length: 198
[MASTER] 07:36:01.525 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4923076923076923 - fitness:200.6413246512945 - branchDistance:0.0 - coverage:137.31255752800683 - ex: 0 - tex: 20
[MASTER] 07:36:01.526 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 200.6413246512945, number of tests: 11, total length: 321
[MASTER] 07:36:02.124 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4923076923076923 - fitness:191.78761345021488 - branchDistance:0.0 - coverage:128.45884632692722 - ex: 0 - tex: 14
[MASTER] 07:36:02.124 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 191.78761345021488, number of tests: 9, total length: 233
[MASTER] 07:36:03.740 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4923076923076923 - fitness:178.14248215554798 - branchDistance:0.0 - coverage:114.8137150322603 - ex: 0 - tex: 16
[MASTER] 07:36:03.740 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 178.14248215554798, number of tests: 11, total length: 298
[MASTER] 07:36:04.736 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4923076923076923 - fitness:174.12907219386216 - branchDistance:0.0 - coverage:110.80030507057448 - ex: 0 - tex: 16
[MASTER] 07:36:04.736 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 174.12907219386216, number of tests: 12, total length: 330
[MASTER] 07:36:05.432 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4923076923076923 - fitness:173.12907219386216 - branchDistance:0.0 - coverage:109.80030507057448 - ex: 0 - tex: 14
[MASTER] 07:36:05.433 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 173.12907219386216, number of tests: 11, total length: 290
[MASTER] 07:36:05.597 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4923076923076923 - fitness:170.19803771110352 - branchDistance:0.0 - coverage:106.86927058781586 - ex: 0 - tex: 14
[MASTER] 07:36:05.598 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 170.19803771110352, number of tests: 10, total length: 233
[MASTER] 07:36:06.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4923076923076923 - fitness:168.19803771110352 - branchDistance:0.0 - coverage:104.86927058781586 - ex: 0 - tex: 10
[MASTER] 07:36:06.489 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 168.19803771110352, number of tests: 10, total length: 244
[MASTER] 07:36:07.372 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.293827160493827 - fitness:149.35730511115835 - branchDistance:0.0 - coverage:103.86927058781586 - ex: 0 - tex: 14
[MASTER] 07:36:07.372 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 149.35730511115835, number of tests: 10, total length: 232
[MASTER] 07:36:09.165 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.293827160493827 - fitness:144.5111512650045 - branchDistance:0.0 - coverage:99.02311674166201 - ex: 0 - tex: 16
[MASTER] 07:36:09.165 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.5111512650045, number of tests: 10, total length: 246
[MASTER] 07:36:10.824 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.293827160493827 - fitness:142.5111512650045 - branchDistance:0.0 - coverage:97.02311674166201 - ex: 0 - tex: 16
[MASTER] 07:36:10.824 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.5111512650045, number of tests: 10, total length: 249
[MASTER] 07:36:12.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.293827160493827 - fitness:141.5111512650045 - branchDistance:0.0 - coverage:96.02311674166201 - ex: 0 - tex: 12
[MASTER] 07:36:12.489 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.5111512650045, number of tests: 10, total length: 242
[MASTER] 07:36:12.560 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.293827160493827 - fitness:139.5111512650045 - branchDistance:0.0 - coverage:94.02311674166201 - ex: 0 - tex: 16
[MASTER] 07:36:12.561 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.5111512650045, number of tests: 11, total length: 278
[MASTER] 07:36:13.251 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.29438202247191 - fitness:138.40882222533423 - branchDistance:0.0 - coverage:99.02311674166201 - ex: 0 - tex: 16
[MASTER] 07:36:13.251 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.40882222533423, number of tests: 11, total length: 264
[MASTER] 07:36:13.911 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.29438202247191 - fitness:137.40882222533423 - branchDistance:0.0 - coverage:98.02311674166201 - ex: 0 - tex: 20
[MASTER] 07:36:13.911 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.40882222533423, number of tests: 12, total length: 305
[MASTER] 07:36:14.497 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.293827160493827 - fitness:137.17781793167117 - branchDistance:0.0 - coverage:91.68978340832868 - ex: 0 - tex: 16
[MASTER] 07:36:14.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.17781793167117, number of tests: 12, total length: 293
[MASTER] 07:36:14.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.29438202247191 - fitness:136.40882222533423 - branchDistance:0.0 - coverage:97.02311674166201 - ex: 0 - tex: 16
[MASTER] 07:36:14.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.40882222533423, number of tests: 10, total length: 256
[MASTER] 07:36:14.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.29438202247191 - fitness:135.40882222533423 - branchDistance:0.0 - coverage:96.02311674166201 - ex: 0 - tex: 20
[MASTER] 07:36:14.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.40882222533423, number of tests: 12, total length: 315
[MASTER] 07:36:15.793 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.29438202247191 - fitness:133.40882222533423 - branchDistance:0.0 - coverage:94.02311674166201 - ex: 0 - tex: 18
[MASTER] 07:36:15.793 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.40882222533423, number of tests: 12, total length: 316
[MASTER] 07:36:17.814 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.29438202247191 - fitness:129.40882222533423 - branchDistance:0.0 - coverage:90.02311674166201 - ex: 0 - tex: 16
[MASTER] 07:36:17.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.40882222533423, number of tests: 13, total length: 359
[MASTER] 07:36:20.356 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.29438202247191 - fitness:125.78090595867636 - branchDistance:0.0 - coverage:91.02311674166201 - ex: 0 - tex: 14
[MASTER] 07:36:20.357 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.78090595867636, number of tests: 11, total length: 302
[MASTER] 07:36:34.529 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.29438202247191 - fitness:124.78090595867636 - branchDistance:0.0 - coverage:90.02311674166201 - ex: 0 - tex: 14
[MASTER] 07:36:34.529 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.78090595867636, number of tests: 11, total length: 304
[MASTER] 07:36:35.973 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.29438202247191 - fitness:123.78090595867636 - branchDistance:0.0 - coverage:89.02311674166201 - ex: 0 - tex: 12
[MASTER] 07:36:35.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.78090595867636, number of tests: 12, total length: 334
[MASTER] 07:36:36.755 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.29438202247191 - fitness:122.78090595867636 - branchDistance:0.0 - coverage:88.02311674166201 - ex: 0 - tex: 16
[MASTER] 07:36:36.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.78090595867636, number of tests: 12, total length: 351
[MASTER] 07:36:43.486 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.29438202247191 - fitness:120.78090595867639 - branchDistance:0.0 - coverage:86.02311674166202 - ex: 0 - tex: 12
[MASTER] 07:36:43.486 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.78090595867639, number of tests: 11, total length: 319
[MASTER] 07:36:44.922 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:120.77902029622331 - branchDistance:0.0 - coverage:86.02311674166202 - ex: 0 - tex: 14
[MASTER] 07:36:44.922 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.77902029622331, number of tests: 11, total length: 327
[MASTER] 07:36:47.580 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.29438202247191 - fitness:120.77505800546001 - branchDistance:0.0 - coverage:86.01726878844565 - ex: 0 - tex: 12
[MASTER] 07:36:47.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.77505800546001, number of tests: 11, total length: 316
[MASTER] 07:36:48.739 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:120.77317234300693 - branchDistance:0.0 - coverage:86.01726878844565 - ex: 0 - tex: 12
[MASTER] 07:36:48.739 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.77317234300693, number of tests: 11, total length: 316
[MASTER] 07:36:52.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:119.11235362955662 - branchDistance:0.0 - coverage:84.35645007499535 - ex: 0 - tex: 14
[MASTER] 07:36:52.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.11235362955662, number of tests: 11, total length: 287
[MASTER] 07:36:54.298 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:119.10650567634025 - branchDistance:0.0 - coverage:84.35060212177898 - ex: 0 - tex: 14
[MASTER] 07:36:54.298 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.10650567634025, number of tests: 11, total length: 311
[MASTER] 07:36:57.830 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:118.10650567634025 - branchDistance:0.0 - coverage:83.35060212177898 - ex: 0 - tex: 18
[MASTER] 07:36:57.830 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.10650567634025, number of tests: 12, total length: 348
[MASTER] 07:36:59.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:117.10650567634028 - branchDistance:0.0 - coverage:82.35060212177899 - ex: 0 - tex: 16
[MASTER] 07:36:59.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.10650567634028, number of tests: 12, total length: 335
[MASTER] 07:36:59.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:117.10650567634025 - branchDistance:0.0 - coverage:82.35060212177898 - ex: 0 - tex: 20
[MASTER] 07:36:59.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.10650567634025, number of tests: 13, total length: 371
[MASTER] 07:37:02.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:115.10650567634025 - branchDistance:0.0 - coverage:80.35060212177898 - ex: 0 - tex: 12
[MASTER] 07:37:02.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.10650567634025, number of tests: 11, total length: 300
[MASTER] 07:37:07.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.294845360824742 - fitness:114.9728569721033 - branchDistance:0.0 - coverage:80.21695341754203 - ex: 0 - tex: 14
[MASTER] 07:37:07.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.9728569721033, number of tests: 12, total length: 295
[MASTER] 07:37:08.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.449795918367347 - fitness:110.14538377946198 - branchDistance:0.0 - coverage:82.35060212177899 - ex: 0 - tex: 18
[MASTER] 07:37:08.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.14538377946198, number of tests: 14, total length: 397
[MASTER] 07:37:09.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.449795918367347 - fitness:108.14538377946197 - branchDistance:0.0 - coverage:80.35060212177898 - ex: 0 - tex: 14
[MASTER] 07:37:09.761 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.14538377946197, number of tests: 13, total length: 359
[MASTER] 07:37:15.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.449795918367347 - fitness:106.65134031129516 - branchDistance:0.0 - coverage:81.19675596793283 - ex: 0 - tex: 14
[MASTER] 07:37:15.043 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.65134031129516, number of tests: 12, total length: 317
[MASTER] 07:37:15.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.449795918367347 - fitness:106.14538377946198 - branchDistance:0.0 - coverage:78.35060212177899 - ex: 0 - tex: 14
[MASTER] 07:37:15.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.14538377946198, number of tests: 13, total length: 341
[MASTER] 07:37:18.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.449795918367347 - fitness:106.14538377946197 - branchDistance:0.0 - coverage:78.35060212177898 - ex: 0 - tex: 12
[MASTER] 07:37:18.976 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.14538377946197, number of tests: 12, total length: 317
[MASTER] 07:37:19.282 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.449795918367347 - fitness:91.98797637205456 - branchDistance:0.0 - coverage:64.19319471437157 - ex: 0 - tex: 10
[MASTER] 07:37:19.282 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.98797637205456, number of tests: 13, total length: 336
[MASTER] 07:37:20.040 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.449795918367347 - fitness:91.85432766781761 - branchDistance:0.0 - coverage:64.05954601013462 - ex: 0 - tex: 12
[MASTER] 07:37:20.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.85432766781761, number of tests: 13, total length: 342
[MASTER] 07:37:24.580 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.449795918367347 - fitness:90.85432766781761 - branchDistance:0.0 - coverage:63.059546010134625 - ex: 0 - tex: 10
[MASTER] 07:37:24.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.85432766781761, number of tests: 13, total length: 370
[MASTER] 07:37:25.359 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.456745311554748 - fitness:90.83652026526914 - branchDistance:0.0 - coverage:63.059546010134625 - ex: 0 - tex: 10
[MASTER] 07:37:25.359 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.83652026526914, number of tests: 13, total length: 342
[MASTER] 07:37:29.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.466123657904479 - fitness:90.81252630657242 - branchDistance:0.0 - coverage:63.059546010134625 - ex: 0 - tex: 14
[MASTER] 07:37:29.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.81252630657242, number of tests: 14, total length: 364
[MASTER] 07:37:30.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.456745311554748 - fitness:90.80010569944281 - branchDistance:0.0 - coverage:63.0231314443083 - ex: 0 - tex: 14
[MASTER] 07:37:30.348 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.80010569944281, number of tests: 13, total length: 347
[MASTER] 07:37:31.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.466123657904479 - fitness:90.77611174074609 - branchDistance:0.0 - coverage:63.0231314443083 - ex: 0 - tex: 14
[MASTER] 07:37:31.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.77611174074609, number of tests: 14, total length: 361
[MASTER] 07:37:32.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.466123657904479 - fitness:90.76023872487308 - branchDistance:0.0 - coverage:63.00725842843528 - ex: 0 - tex: 10
[MASTER] 07:37:32.551 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.76023872487308, number of tests: 13, total length: 347
[MASTER] 07:37:32.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.456745311554748 - fitness:89.46677236610947 - branchDistance:0.0 - coverage:61.68979811097496 - ex: 0 - tex: 14
[MASTER] 07:37:32.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.46677236610947, number of tests: 13, total length: 376
[MASTER] 07:37:33.529 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.449795918367347 - fitness:88.47771578767063 - branchDistance:0.0 - coverage:63.0231314443083 - ex: 0 - tex: 12
[MASTER] 07:37:33.529 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.47771578767063, number of tests: 13, total length: 345
[MASTER] 07:37:36.207 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:87.12954887663132 - branchDistance:0.0 - coverage:61.68979811097496 - ex: 0 - tex: 14
[MASTER] 07:37:36.207 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.12954887663132, number of tests: 13, total length: 377
[MASTER] 07:37:39.288 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.449795918367347 - fitness:86.81870569588588 - branchDistance:0.0 - coverage:61.36412135252355 - ex: 0 - tex: 14
[MASTER] 07:37:39.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.81870569588588, number of tests: 13, total length: 381
[MASTER] 07:37:41.206 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.449795918367347 - fitness:86.14438245433729 - branchDistance:0.0 - coverage:60.68979811097496 - ex: 0 - tex: 10
[MASTER] 07:37:41.206 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.14438245433729, number of tests: 15, total length: 403
[MASTER] 07:37:42.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:85.8038721181799 - branchDistance:0.0 - coverage:60.36412135252355 - ex: 0 - tex: 16
[MASTER] 07:37:42.576 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.8038721181799, number of tests: 13, total length: 399
[MASTER] 07:37:45.619 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:85.16288220996466 - branchDistance:0.0 - coverage:59.7231314443083 - ex: 0 - tex: 12
[MASTER] 07:37:45.619 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.16288220996466, number of tests: 13, total length: 375
[MASTER] 07:37:46.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:84.16288220996466 - branchDistance:0.0 - coverage:58.7231314443083 - ex: 0 - tex: 14
[MASTER] 07:37:46.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.16288220996466, number of tests: 13, total length: 383
[MASTER] 07:37:49.556 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:84.16288220996465 - branchDistance:0.0 - coverage:58.723131444308294 - ex: 0 - tex: 14
[MASTER] 07:37:49.556 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.16288220996465, number of tests: 13, total length: 382
[MASTER] 07:37:55.322 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:83.16288220996465 - branchDistance:0.0 - coverage:57.723131444308294 - ex: 0 - tex: 14
[MASTER] 07:37:55.322 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.16288220996465, number of tests: 14, total length: 394
[MASTER] 07:37:59.065 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:82.16288220996465 - branchDistance:0.0 - coverage:56.723131444308294 - ex: 0 - tex: 14
[MASTER] 07:37:59.065 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.16288220996465, number of tests: 14, total length: 383
[MASTER] 07:38:04.948 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:82.12484227870821 - branchDistance:0.0 - coverage:56.68509151305186 - ex: 0 - tex: 14
[MASTER] 07:38:04.948 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.12484227870821, number of tests: 13, total length: 366
[MASTER] 07:38:05.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:81.7112749402765 - branchDistance:0.0 - coverage:56.27152417462014 - ex: 0 - tex: 10
[MASTER] 07:38:05.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.7112749402765, number of tests: 13, total length: 375
[MASTER] 07:38:09.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:81.45586368486525 - branchDistance:0.0 - coverage:56.01611291920889 - ex: 0 - tex: 10
[MASTER] 07:38:09.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.45586368486525, number of tests: 13, total length: 366
[MASTER] 07:38:12.594 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:79.84182859714593 - branchDistance:0.0 - coverage:54.402077831489585 - ex: 0 - tex: 10
[MASTER] 07:38:12.594 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.84182859714593, number of tests: 14, total length: 397
[MASTER] 07:38:15.978 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:79.83054266086 - branchDistance:0.0 - coverage:54.390791895203655 - ex: 0 - tex: 8
[MASTER] 07:38:15.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.83054266086, number of tests: 14, total length: 385
[MASTER] 07:38:27.303 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.456745311554748 - fitness:79.81466964498699 - branchDistance:0.0 - coverage:54.374918879330636 - ex: 0 - tex: 10
[MASTER] 07:38:27.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.81466964498699, number of tests: 14, total length: 397
[MASTER] 07:38:32.652 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.462004662004663 - fitness:79.80345544485573 - branchDistance:0.0 - coverage:54.374918879330636 - ex: 0 - tex: 8
[MASTER] 07:38:32.652 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.80345544485573, number of tests: 13, total length: 362
[MASTER] 07:38:40.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.466123657904479 - fitness:79.79467993842007 - branchDistance:0.0 - coverage:54.374918879330636 - ex: 0 - tex: 8
[MASTER] 07:38:40.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.79467993842007, number of tests: 13, total length: 342
[MASTER] 07:38:40.815 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.476370170709794 - fitness:77.48536032509662 - branchDistance:0.0 - coverage:55.708252212663965 - ex: 0 - tex: 8
[MASTER] 07:38:40.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.48536032509662, number of tests: 13, total length: 345
[MASTER] 07:38:41.756 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.476370170709794 - fitness:76.1520269917633 - branchDistance:0.0 - coverage:54.374918879330636 - ex: 0 - tex: 6
[MASTER] 07:38:41.756 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.1520269917633, number of tests: 13, total length: 341
[MASTER] 07:38:55.470 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.476370170709794 - fitness:76.14814821409786 - branchDistance:0.0 - coverage:54.3710401016652 - ex: 0 - tex: 8
[MASTER] 07:38:55.470 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14814821409786, number of tests: 13, total length: 352
[MASTER] 07:38:55.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.476370170709794 - fitness:75.42980476954108 - branchDistance:0.0 - coverage:53.652696657108415 - ex: 0 - tex: 10
[MASTER] 07:38:55.933 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.42980476954108, number of tests: 13, total length: 385
[MASTER] 07:39:14.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.476370170709794 - fitness:74.42980476954108 - branchDistance:0.0 - coverage:52.652696657108415 - ex: 0 - tex: 6
[MASTER] 07:39:14.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.42980476954108, number of tests: 12, total length: 363
[MASTER] 07:39:23.318 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.476370170709794 - fitness:74.41450381389994 - branchDistance:0.0 - coverage:52.637395701467284 - ex: 0 - tex: 6
[MASTER] 07:39:23.318 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.41450381389994, number of tests: 12, total length: 357
[MASTER] 07:39:38.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.478031361589816 - fitness:74.4119429990759 - branchDistance:0.0 - coverage:52.637395701467284 - ex: 0 - tex: 6
[MASTER] 07:39:38.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.4119429990759, number of tests: 11, total length: 335
[MASTER] 07:39:49.194 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.487611447711934 - fitness:74.39718709117993 - branchDistance:0.0 - coverage:52.637395701467284 - ex: 0 - tex: 6
[MASTER] 07:39:49.194 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.39718709117993, number of tests: 11, total length: 311
[MASTER] 07:39:50.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.478031361589816 - fitness:73.4119429990759 - branchDistance:0.0 - coverage:51.637395701467284 - ex: 0 - tex: 6
[MASTER] 07:39:50.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.4119429990759, number of tests: 11, total length: 306
[MASTER] 07:39:51.165 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.487611447711934 - fitness:73.39718709117993 - branchDistance:0.0 - coverage:51.637395701467284 - ex: 0 - tex: 6
[MASTER] 07:39:51.165 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.39718709117993, number of tests: 11, total length: 308
[MASTER] 07:39:53.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.488083845978583 - fitness:73.39646001314718 - branchDistance:0.0 - coverage:51.637395701467284 - ex: 0 - tex: 6
[MASTER] 07:39:53.134 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.39646001314718, number of tests: 11, total length: 312
[MASTER] 07:39:58.213 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.488083845978583 - fitness:71.89646001314718 - branchDistance:0.0 - coverage:50.137395701467284 - ex: 0 - tex: 6
[MASTER] 07:39:58.213 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.89646001314718, number of tests: 11, total length: 302
[MASTER] 07:40:02.883 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.488521540607957 - fitness:71.89578639366022 - branchDistance:0.0 - coverage:50.137395701467284 - ex: 0 - tex: 6
[MASTER] 07:40:02.883 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.89578639366022, number of tests: 11, total length: 302
[MASTER] 07:40:12.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.488521540607957 - fitness:70.1815006793745 - branchDistance:0.0 - coverage:48.423109987181576 - ex: 0 - tex: 6
[MASTER] 07:40:12.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.1815006793745, number of tests: 11, total length: 294
[MASTER] 07:40:42.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.488521540607957 - fitness:70.16799899587281 - branchDistance:0.0 - coverage:48.40960830367989 - ex: 0 - tex: 6
[MASTER] 07:40:42.750 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.16799899587281, number of tests: 11, total length: 268
[MASTER] 07:40:46.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.488521540607957 - fitness:69.93551121075345 - branchDistance:0.0 - coverage:48.17712051856053 - ex: 0 - tex: 6
[MASTER] 07:40:46.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.93551121075345, number of tests: 11, total length: 266
[MASTER] 07:40:48.718 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 417 generations, 593573 statements, best individuals have fitness: 69.93551121075345
[MASTER] 07:40:48.721 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 78%
* Total number of goals: 128
* Number of covered goals: 100
* Generated 11 tests with total length 262
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     70 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 07:40:49.067 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:40:49.181 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 206 
[MASTER] 07:40:49.379 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 07:40:49.379 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 07:40:49.382 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 07:40:49.382 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:40:49.382 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:40:49.382 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 07:40:49.383 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 07:40:49.384 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 128
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 130
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
