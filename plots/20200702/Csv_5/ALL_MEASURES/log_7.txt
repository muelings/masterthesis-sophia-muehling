* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 18179
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:57:39.433 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:57:39.439 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 128
* Using seed 1593669456611
* Starting evolution
[MASTER] 07:57:45.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:555.0 - branchDistance:0.0 - coverage:274.0 - ex: 0 - tex: 20
[MASTER] 07:57:45.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 555.0, number of tests: 10, total length: 239
[MASTER] 07:57:46.609 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9411764705882355 - fitness:229.18252017695204 - branchDistance:0.0 - coverage:157.13774405754907 - ex: 0 - tex: 16
[MASTER] 07:57:46.609 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 229.18252017695204, number of tests: 9, total length: 231
[MASTER] 07:57:59.454 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9411764705882355 - fitness:228.18252017695204 - branchDistance:0.0 - coverage:156.13774405754907 - ex: 0 - tex: 14
[MASTER] 07:57:59.455 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 228.18252017695204, number of tests: 9, total length: 237
[MASTER] 07:57:59.607 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9411764705882355 - fitness:226.18252017695204 - branchDistance:0.0 - coverage:154.13774405754907 - ex: 0 - tex: 16
[MASTER] 07:57:59.608 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 226.18252017695204, number of tests: 9, total length: 230
[MASTER] 07:58:00.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9411764705882355 - fitness:210.61471216396416 - branchDistance:0.0 - coverage:138.56993604456116 - ex: 0 - tex: 14
[MASTER] 07:58:00.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 210.61471216396416, number of tests: 9, total length: 239
[MASTER] 07:58:01.655 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9411764705882355 - fitness:207.61471216396416 - branchDistance:0.0 - coverage:135.56993604456116 - ex: 0 - tex: 14
[MASTER] 07:58:01.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 207.61471216396416, number of tests: 9, total length: 236
[MASTER] 07:58:01.785 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9411764705882355 - fitness:194.68367768120555 - branchDistance:0.0 - coverage:122.63890156180256 - ex: 0 - tex: 14
[MASTER] 07:58:01.785 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 194.68367768120555, number of tests: 9, total length: 239
[MASTER] 07:58:03.310 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9411764705882355 - fitness:194.62753267678465 - branchDistance:0.0 - coverage:122.58275655738169 - ex: 0 - tex: 12
[MASTER] 07:58:03.310 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 194.62753267678465, number of tests: 9, total length: 256
[MASTER] 07:58:03.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:180.09051446502838 - branchDistance:0.0 - coverage:122.63890156180256 - ex: 0 - tex: 14
[MASTER] 07:58:03.706 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 180.09051446502838, number of tests: 9, total length: 275
[MASTER] 07:58:06.057 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:179.08540590819564 - branchDistance:0.0 - coverage:121.63379300496985 - ex: 0 - tex: 14
[MASTER] 07:58:06.057 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 179.08540590819564, number of tests: 9, total length: 262
[MASTER] 07:58:06.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:161.51070255072756 - branchDistance:0.0 - coverage:94.542116163293 - ex: 0 - tex: 16
[MASTER] 07:58:06.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 161.51070255072756, number of tests: 10, total length: 279
[MASTER] 07:58:07.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:160.51070255072756 - branchDistance:0.0 - coverage:93.54211616329302 - ex: 0 - tex: 14
[MASTER] 07:58:07.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 160.51070255072756, number of tests: 10, total length: 262
[MASTER] 07:58:07.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:151.99372906651882 - branchDistance:0.0 - coverage:94.542116163293 - ex: 0 - tex: 12
[MASTER] 07:58:07.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 151.99372906651882, number of tests: 9, total length: 251
[MASTER] 07:58:09.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:149.92476354927743 - branchDistance:0.0 - coverage:92.47315064605164 - ex: 0 - tex: 12
[MASTER] 07:58:09.363 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 149.92476354927743, number of tests: 9, total length: 253
[MASTER] 07:58:09.982 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:147.99372906651882 - branchDistance:0.0 - coverage:90.542116163293 - ex: 0 - tex: 16
[MASTER] 07:58:09.982 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 147.99372906651882, number of tests: 11, total length: 326
[MASTER] 07:58:11.767 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:143.99372906651882 - branchDistance:0.0 - coverage:86.542116163293 - ex: 0 - tex: 14
[MASTER] 07:58:11.767 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.99372906651882, number of tests: 10, total length: 297
[MASTER] 07:58:12.805 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:142.99372906651882 - branchDistance:0.0 - coverage:85.542116163293 - ex: 0 - tex: 16
[MASTER] 07:58:12.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.99372906651882, number of tests: 11, total length: 310
[MASTER] 07:58:14.542 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:141.99372906651882 - branchDistance:0.0 - coverage:84.542116163293 - ex: 0 - tex: 12
[MASTER] 07:58:14.542 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.99372906651882, number of tests: 10, total length: 278
[MASTER] 07:58:15.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:140.99372906651882 - branchDistance:0.0 - coverage:83.542116163293 - ex: 0 - tex: 14
[MASTER] 07:58:15.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.99372906651882, number of tests: 10, total length: 297
[MASTER] 07:58:18.087 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:139.99372906651882 - branchDistance:0.0 - coverage:82.542116163293 - ex: 0 - tex: 14
[MASTER] 07:58:18.088 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.99372906651882, number of tests: 10, total length: 285
[MASTER] 07:58:21.472 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.991735537190083 - fitness:138.63483139508108 - branchDistance:0.0 - coverage:81.542116163293 - ex: 0 - tex: 14
[MASTER] 07:58:21.472 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.63483139508108, number of tests: 10, total length: 283
[MASTER] 07:58:21.540 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9696969696969697 - fitness:134.88357957792715 - branchDistance:0.0 - coverage:77.542116163293 - ex: 0 - tex: 14
[MASTER] 07:58:21.540 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.88357957792715, number of tests: 10, total length: 286
[MASTER] 07:58:24.701 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9696969696969697 - fitness:134.60287782354118 - branchDistance:0.0 - coverage:77.26141440890704 - ex: 0 - tex: 14
[MASTER] 07:58:24.701 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.60287782354118, number of tests: 10, total length: 294
[MASTER] 07:58:26.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.979591836734694 - fitness:132.49092260562836 - branchDistance:0.0 - coverage:75.26141440890704 - ex: 0 - tex: 16
[MASTER] 07:58:26.043 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.49092260562836, number of tests: 10, total length: 299
[MASTER] 07:58:30.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.982456140350877 - fitness:132.4585975074986 - branchDistance:0.0 - coverage:75.26141440890704 - ex: 0 - tex: 16
[MASTER] 07:58:30.851 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.4585975074986, number of tests: 10, total length: 296
[MASTER] 07:58:31.512 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9846153846153847 - fitness:132.4342539150799 - branchDistance:0.0 - coverage:75.26141440890704 - ex: 0 - tex: 16
[MASTER] 07:58:31.512 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.4342539150799, number of tests: 10, total length: 297
[MASTER] 07:58:31.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9931034482758623 - fitness:131.33876247520539 - branchDistance:0.0 - coverage:74.26141440890706 - ex: 0 - tex: 16
[MASTER] 07:58:31.872 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.33876247520539, number of tests: 10, total length: 296
[MASTER] 07:58:32.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9934640522875817 - fitness:131.33471283822644 - branchDistance:0.0 - coverage:74.26141440890706 - ex: 0 - tex: 10
[MASTER] 07:58:32.043 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.33471283822644, number of tests: 10, total length: 274
[MASTER] 07:58:38.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9934640522875817 - fitness:130.33471283822644 - branchDistance:0.0 - coverage:73.26141440890706 - ex: 0 - tex: 10
[MASTER] 07:58:38.701 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.33471283822644, number of tests: 11, total length: 290
[MASTER] 07:58:41.581 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.005876951331498 - fitness:111.68954705576414 - branchDistance:0.0 - coverage:85.24859389608653 - ex: 0 - tex: 12
[MASTER] 07:58:41.581 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.68954705576414, number of tests: 11, total length: 292
[MASTER] 07:58:42.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.005876951331498 - fitness:100.70236756858466 - branchDistance:0.0 - coverage:74.26141440890706 - ex: 0 - tex: 14
[MASTER] 07:58:42.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.70236756858466, number of tests: 12, total length: 315
[MASTER] 07:58:43.915 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.005876951331498 - fitness:99.70236756858466 - branchDistance:0.0 - coverage:73.26141440890706 - ex: 0 - tex: 12
[MASTER] 07:58:43.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.70236756858466, number of tests: 12, total length: 322
[MASTER] 07:58:47.559 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.005876951331498 - fitness:99.69710441068992 - branchDistance:0.0 - coverage:73.25615125101231 - ex: 0 - tex: 8
[MASTER] 07:58:47.559 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.69710441068992, number of tests: 12, total length: 293
[MASTER] 07:58:48.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.078922207735769 - fitness:99.53463095017786 - branchDistance:0.0 - coverage:73.26141440890706 - ex: 0 - tex: 8
[MASTER] 07:58:48.093 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.53463095017786, number of tests: 11, total length: 255
[MASTER] 07:58:48.495 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.005876951331498 - fitness:92.07577961752862 - branchDistance:0.0 - coverage:65.63482645785102 - ex: 0 - tex: 8
[MASTER] 07:58:48.495 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.07577961752862, number of tests: 12, total length: 305
[MASTER] 07:58:48.590 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.005876951331498 - fitness:92.07577961752861 - branchDistance:0.0 - coverage:65.634826457851 - ex: 0 - tex: 10
[MASTER] 07:58:48.590 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.07577961752861, number of tests: 12, total length: 300
[MASTER] 07:58:49.276 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.078922207735769 - fitness:91.9080429991218 - branchDistance:0.0 - coverage:65.634826457851 - ex: 0 - tex: 12
[MASTER] 07:58:49.276 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.9080429991218, number of tests: 12, total length: 323
[MASTER] 07:58:49.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.005876951331498 - fitness:91.07577961752862 - branchDistance:0.0 - coverage:64.63482645785102 - ex: 0 - tex: 8
[MASTER] 07:58:49.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.07577961752862, number of tests: 12, total length: 311
[MASTER] 07:58:50.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.694765840220384 - fitness:85.68923011725637 - branchDistance:0.0 - coverage:65.63482645785102 - ex: 0 - tex: 8
[MASTER] 07:58:50.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.68923011725637, number of tests: 12, total length: 307
[MASTER] 07:58:52.121 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.694765840220384 - fitness:84.68923011725637 - branchDistance:0.0 - coverage:64.63482645785102 - ex: 0 - tex: 6
[MASTER] 07:58:52.121 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.68923011725637, number of tests: 12, total length: 329
[MASTER] 07:58:53.542 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.80199913081269 - fitness:84.54592706443835 - branchDistance:0.0 - coverage:64.62956329995629 - ex: 0 - tex: 8
[MASTER] 07:58:53.542 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.54592706443835, number of tests: 12, total length: 335
[MASTER] 07:58:55.739 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.80199913081269 - fitness:82.2995100957948 - branchDistance:0.0 - coverage:64.63482645785102 - ex: 0 - tex: 8
[MASTER] 07:58:55.739 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.2995100957948, number of tests: 12, total length: 354
[MASTER] 07:58:55.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.80199913081269 - fitness:82.29424693790008 - branchDistance:0.0 - coverage:64.62956329995629 - ex: 0 - tex: 8
[MASTER] 07:58:55.895 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.29424693790008, number of tests: 12, total length: 323
[MASTER] 07:58:59.712 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.80199913081269 - fitness:77.60193924559238 - branchDistance:0.0 - coverage:59.93725560764859 - ex: 0 - tex: 10
[MASTER] 07:58:59.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.60193924559238, number of tests: 13, total length: 340
[MASTER] 07:59:04.803 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.80199913081269 - fitness:77.55740942165079 - branchDistance:0.0 - coverage:59.892725783707 - ex: 0 - tex: 10
[MASTER] 07:59:04.803 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.55740942165079, number of tests: 12, total length: 330
[MASTER] 07:59:06.711 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.858722544811917 - fitness:77.54586870130004 - branchDistance:0.0 - coverage:59.93725560764859 - ex: 0 - tex: 8
[MASTER] 07:59:06.711 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.54586870130004, number of tests: 12, total length: 293
[MASTER] 07:59:08.120 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.858722544811917 - fitness:77.08985891588861 - branchDistance:0.0 - coverage:59.48124582223717 - ex: 0 - tex: 10
[MASTER] 07:59:08.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.08985891588861, number of tests: 12, total length: 293
[MASTER] 07:59:10.187 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.80199913081269 - fitness:76.55740942165079 - branchDistance:0.0 - coverage:58.892725783707 - ex: 0 - tex: 8
[MASTER] 07:59:10.187 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.55740942165079, number of tests: 12, total length: 292
[MASTER] 07:59:10.476 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.858722544811917 - fitness:76.08985891588861 - branchDistance:0.0 - coverage:58.48124582223717 - ex: 0 - tex: 10
[MASTER] 07:59:10.476 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08985891588861, number of tests: 12, total length: 299
[MASTER] 07:59:12.491 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.858722544811917 - fitness:75.75652558255527 - branchDistance:0.0 - coverage:58.147912488903835 - ex: 0 - tex: 10
[MASTER] 07:59:12.491 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.75652558255527, number of tests: 13, total length: 317
[MASTER] 07:59:13.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.195652626105044 - fitness:74.06788194521093 - branchDistance:0.0 - coverage:58.48124582223717 - ex: 0 - tex: 10
[MASTER] 07:59:13.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.06788194521093, number of tests: 12, total length: 301
[MASTER] 07:59:17.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.195652626105044 - fitness:73.7345486118776 - branchDistance:0.0 - coverage:58.147912488903835 - ex: 0 - tex: 8
[MASTER] 07:59:17.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.7345486118776, number of tests: 12, total length: 292
[MASTER] 07:59:17.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.195652626105044 - fitness:72.69150195066351 - branchDistance:0.0 - coverage:58.48124582223717 - ex: 0 - tex: 8
[MASTER] 07:59:17.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.69150195066351, number of tests: 12, total length: 290
[MASTER] 07:59:19.041 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.195652626105044 - fitness:72.35816861733018 - branchDistance:0.0 - coverage:58.147912488903835 - ex: 0 - tex: 8
[MASTER] 07:59:19.041 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.35816861733018, number of tests: 12, total length: 298
[MASTER] 07:59:21.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.31959332899772 - fitness:71.154982940335 - branchDistance:0.0 - coverage:58.147912488903835 - ex: 0 - tex: 8
[MASTER] 07:59:21.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.154982940335, number of tests: 12, total length: 300
[MASTER] 07:59:23.801 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.31959332899772 - fitness:70.00107819571072 - branchDistance:0.0 - coverage:56.99400774427954 - ex: 0 - tex: 8
[MASTER] 07:59:23.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.00107819571072, number of tests: 12, total length: 313
[MASTER] 07:59:26.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.31959332899772 - fitness:69.50735816145692 - branchDistance:0.0 - coverage:56.99400774427954 - ex: 0 - tex: 10
[MASTER] 07:59:26.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.50735816145692, number of tests: 12, total length: 308
[MASTER] 07:59:30.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.31959332899772 - fitness:67.75735816145692 - branchDistance:0.0 - coverage:55.24400774427954 - ex: 0 - tex: 10
[MASTER] 07:59:30.440 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.75735816145692, number of tests: 12, total length: 316
[MASTER] 07:59:33.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.31959332899772 - fitness:66.75735816145692 - branchDistance:0.0 - coverage:54.24400774427955 - ex: 0 - tex: 10
[MASTER] 07:59:33.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.75735816145692, number of tests: 12, total length: 306
[MASTER] 07:59:38.108 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.31959332899772 - fitness:65.88246999177593 - branchDistance:0.0 - coverage:54.24400774427955 - ex: 0 - tex: 10
[MASTER] 07:59:38.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.88246999177593, number of tests: 12, total length: 301
[MASTER] 07:59:38.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.31959332899772 - fitness:65.75735816145692 - branchDistance:0.0 - coverage:53.24400774427955 - ex: 0 - tex: 10
[MASTER] 07:59:38.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.75735816145692, number of tests: 12, total length: 301
[MASTER] 07:59:41.149 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.31959332899772 - fitness:64.88246999177593 - branchDistance:0.0 - coverage:53.24400774427955 - ex: 0 - tex: 12
[MASTER] 07:59:41.149 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.88246999177593, number of tests: 12, total length: 303
[MASTER] 07:59:41.662 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.31959332899772 - fitness:64.13115537197149 - branchDistance:0.0 - coverage:53.24400774427955 - ex: 0 - tex: 10
[MASTER] 07:59:41.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.13115537197149, number of tests: 12, total length: 309
[MASTER] 07:59:48.817 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.31959332899772 - fitness:64.13115537197147 - branchDistance:0.0 - coverage:53.24400774427954 - ex: 0 - tex: 10
[MASTER] 07:59:48.817 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.13115537197147, number of tests: 12, total length: 293
[MASTER] 07:59:49.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.31959332899772 - fitness:63.47896010533934 - branchDistance:0.0 - coverage:53.24400774427955 - ex: 0 - tex: 10
[MASTER] 07:59:49.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.47896010533934, number of tests: 12, total length: 287
[MASTER] 07:59:52.945 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.31959332899772 - fitness:63.478960105339326 - branchDistance:0.0 - coverage:53.24400774427954 - ex: 0 - tex: 10
[MASTER] 07:59:52.945 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.478960105339326, number of tests: 12, total length: 290
[MASTER] 08:00:01.129 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.31959332899772 - fitness:63.4465460239185 - branchDistance:0.0 - coverage:53.21159366285872 - ex: 0 - tex: 10
[MASTER] 08:00:01.129 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.4465460239185, number of tests: 12, total length: 285
[MASTER] 08:00:10.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:63.42542943600562 - branchDistance:0.0 - coverage:53.21159366285872 - ex: 0 - tex: 10
[MASTER] 08:00:10.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.42542943600562, number of tests: 12, total length: 278
[MASTER] 08:00:16.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:63.4023525129287 - branchDistance:0.0 - coverage:53.1885167397818 - ex: 0 - tex: 10
[MASTER] 08:00:16.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.4023525129287, number of tests: 11, total length: 264
[MASTER] 08:00:37.293 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:63.39650455971233 - branchDistance:0.0 - coverage:53.18266878656542 - ex: 0 - tex: 8
[MASTER] 08:00:37.293 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.39650455971233, number of tests: 12, total length: 284
[MASTER] 08:00:44.642 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:63.32812849133626 - branchDistance:0.0 - coverage:53.114292718189354 - ex: 0 - tex: 12
[MASTER] 08:00:44.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.32812849133626, number of tests: 12, total length: 298
[MASTER] 08:00:58.950 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:63.097178026650234 - branchDistance:0.0 - coverage:52.88334225350333 - ex: 0 - tex: 6
[MASTER] 08:00:58.950 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.097178026650234, number of tests: 11, total length: 258
[MASTER] 08:00:59.519 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:62.097178026650234 - branchDistance:0.0 - coverage:51.88334225350333 - ex: 0 - tex: 10
[MASTER] 08:00:59.519 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.097178026650234, number of tests: 11, total length: 272
[MASTER] 08:01:00.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:62.09717802665023 - branchDistance:0.0 - coverage:51.88334225350332 - ex: 0 - tex: 10
[MASTER] 08:01:00.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.09717802665023, number of tests: 11, total length: 269
[MASTER] 08:01:05.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:60.39616737069589 - branchDistance:0.0 - coverage:50.18233159754899 - ex: 0 - tex: 12
[MASTER] 08:01:05.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.39616737069589, number of tests: 12, total length: 316
[MASTER] 08:01:05.450 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.38908082299891 - fitness:59.39616737069589 - branchDistance:0.0 - coverage:49.18233159754899 - ex: 0 - tex: 14
[MASTER] 08:01:05.450 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.39616737069589, number of tests: 12, total length: 347
[MASTER] 08:01:21.541 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.8938210578349 - fitness:58.9614615579341 - branchDistance:0.0 - coverage:49.18233159754899 - ex: 0 - tex: 10
[MASTER] 08:01:21.541 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.9614615579341, number of tests: 12, total length: 298
[MASTER] 08:01:36.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.8938210578349 - fitness:58.4614615579341 - branchDistance:0.0 - coverage:49.18233159754899 - ex: 1 - tex: 11
[MASTER] 08:01:36.132 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.4614615579341, number of tests: 12, total length: 291
[MASTER] 08:01:40.824 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.8938210578349 - fitness:58.294794891267436 - branchDistance:0.0 - coverage:49.18233159754899 - ex: 2 - tex: 10
[MASTER] 08:01:40.824 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.294794891267436, number of tests: 12, total length: 318
[MASTER] 08:01:45.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.8938210578349 - fitness:58.2114615579341 - branchDistance:0.0 - coverage:49.18233159754899 - ex: 3 - tex: 9
[MASTER] 08:01:45.749 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.2114615579341, number of tests: 12, total length: 321
[MASTER] 08:01:59.202 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.973333333333333 - fitness:58.18962934567242 - branchDistance:0.0 - coverage:49.18233159754899 - ex: 3 - tex: 11
[MASTER] 08:01:59.202 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.18962934567242, number of tests: 12, total length: 310
[MASTER] 08:02:16.952 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.973333333333333 - fitness:58.18124425250472 - branchDistance:0.0 - coverage:49.173946504381284 - ex: 3 - tex: 9
[MASTER] 08:02:16.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.18124425250472, number of tests: 12, total length: 295
[MASTER] 08:02:21.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.973333333333333 - fitness:58.13124425250472 - branchDistance:0.0 - coverage:49.173946504381284 - ex: 4 - tex: 10
[MASTER] 08:02:21.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.13124425250472, number of tests: 13, total length: 326
[MASTER] 08:02:27.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.973333333333333 - fitness:58.09791091917138 - branchDistance:0.0 - coverage:49.173946504381284 - ex: 5 - tex: 11
[MASTER] 08:02:27.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.09791091917138, number of tests: 14, total length: 351
[MASTER] 08:02:34.968 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.973333333333333 - fitness:58.074101395361865 - branchDistance:0.0 - coverage:49.173946504381284 - ex: 6 - tex: 12
[MASTER] 08:02:34.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.074101395361865, number of tests: 15, total length: 376
[MASTER] 08:02:40.494 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 409 generations, 531296 statements, best individuals have fitness: 58.074101395361865
[MASTER] 08:02:40.501 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 77%
* Total number of goals: 128
* Number of covered goals: 99
* Generated 15 tests with total length 365
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     58 / 0           
[MASTER] 08:02:40.837 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:02:40.914 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 279 
[MASTER] 08:02:41.106 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 1.0
[MASTER] 08:02:41.107 [logback-1] WARN  RegressionSuiteMinimizer - Test3, uniqueExceptions: [println:IOException]
[MASTER] 08:02:41.107 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: println:IOException at 16
[MASTER] 08:02:41.107 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 1.0
[MASTER] 08:02:41.107 [logback-1] WARN  RegressionSuiteMinimizer - Test4, uniqueExceptions: [println:IOException]
[MASTER] 08:02:41.107 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: println:IOException at 20
[MASTER] 08:02:41.107 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: println()V
[MASTER] 08:02:41.107 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 08:02:41.116 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 1.0
[MASTER] 08:02:41.116 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: [println:IOException]
[MASTER] 08:02:41.116 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: println:IOException at 20
[MASTER] 08:02:41.116 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: println()V
[MASTER] 08:02:41.116 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 08:02:41.126 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 08:02:41.126 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: [println:IOException]
[MASTER] 08:02:41.126 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: println:IOException at 12
[MASTER] 08:02:41.127 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: println()V
[MASTER] 08:02:41.127 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 12
[MASTER] 08:02:41.131 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 08:02:41.131 [logback-1] WARN  RegressionSuiteMinimizer - Test7, uniqueExceptions: [println:IOException]
[MASTER] 08:02:41.131 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: println:IOException at 20
[MASTER] 08:02:41.131 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: println()V
[MASTER] 08:02:41.131 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 08:02:41.140 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 1.0
[MASTER] 08:02:41.140 [logback-1] WARN  RegressionSuiteMinimizer - Test8, uniqueExceptions: [println:IOException]
[MASTER] 08:02:41.140 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: println:IOException at 20
[MASTER] 08:02:41.140 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: println()V
[MASTER] 08:02:41.140 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 08:02:41.157 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 08:02:41.158 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 08:02:41.159 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 08:02:41.159 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [println:IOException]
[MASTER] 08:02:41.159 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:02:41.159 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:02:41.159 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 08:02:41.160 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 08:02:41.366 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
[MASTER] 08:02:41.368 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 20%
* Total number of goals: 128
* Number of covered goals: 26
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 129
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
