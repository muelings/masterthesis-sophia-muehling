* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 15632
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 128
[MASTER] 07:30:13.517 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:30:18.809 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(360L, var1.length());  // (Inspector) Original Value: 360 | Regression Value: 392
[MASTER] 07:30:18.840 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:30:18.840 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:30:19.950 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:30:19.976 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(360L, var1.length());  // (Inspector) Original Value: 360 | Regression Value: 392
[MASTER] 07:30:19.978 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:30:19.978 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 07:30:20.289 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:30:20.307 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(360L, var1.length());  // (Inspector) Original Value: 360 | Regression Value: 392
[MASTER] 07:30:20.309 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:30:20.309 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:30:20.310 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 103 | Tests with assertion: 1
* Generated 1 tests with total length 19
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          6 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 07:30:20.916 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:30:20.932 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 17 
[MASTER] 07:30:20.949 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[length]}
[MASTER] 07:30:20.953 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.InspectorAssertion@6f53144c] invalid assertion(s) to be removed
[MASTER] 07:30:20.955 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:30:20.955 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 07:30:20.958 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 128
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
