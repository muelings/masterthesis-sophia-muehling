* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.TypeInfoFactory
* Starting client
* Connecting to master process on port 7901
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.TypeInfoFactory
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:25:32.383 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:25:32.391 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 46
* Using seed 1593674729927
* Starting evolution
[MASTER] 09:25:34.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.875 - fitness:83.79746835443038 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 18
[MASTER] 09:25:34.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.79746835443038, number of tests: 9, total length: 170
[MASTER] 09:25:34.323 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.88888888888889 - fitness:80.4766355140187 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 09:25:34.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.4766355140187, number of tests: 10, total length: 235
[MASTER] 09:25:34.967 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.88888888888889 - fitness:79.544 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 18
[MASTER] 09:25:34.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.544, number of tests: 9, total length: 165
[MASTER] 09:25:38.090 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.875 - fitness:79.17647058823529 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 16
[MASTER] 09:25:38.091 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.17647058823529, number of tests: 9, total length: 154
[MASTER] 09:25:39.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.88888888888889 - fitness:79.17164179104478 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 09:25:39.021 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.17164179104478, number of tests: 9, total length: 154
[MASTER] 09:25:39.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.88888888888889 - fitness:78.84615384615384 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 09:25:39.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.84615384615384, number of tests: 8, total length: 102
[MASTER] 09:25:41.415 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.88888888888889 - fitness:78.5592105263158 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 09:25:41.415 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.5592105263158, number of tests: 8, total length: 83
[MASTER] 09:25:43.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.857142857142858 - fitness:78.312 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 09:25:43.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.312, number of tests: 6, total length: 84
[MASTER] 09:25:43.531 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.875 - fitness:78.3076923076923 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 09:25:43.531 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.3076923076923, number of tests: 7, total length: 102
[MASTER] 09:25:44.065 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.88888888888889 - fitness:78.30434782608695 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:25:44.065 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.30434782608695, number of tests: 3, total length: 50
[MASTER] 09:25:44.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.875 - fitness:77.87421383647799 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 09:25:44.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.87421383647799, number of tests: 5, total length: 74
[MASTER] 09:25:44.714 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.875 - fitness:77.68862275449102 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:25:44.714 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68862275449102, number of tests: 4, total length: 56
[MASTER] 09:25:45.368 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88888888888889 - fitness:77.68617021276596 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 09:25:45.368 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68617021276596, number of tests: 4, total length: 63
[MASTER] 09:25:45.583 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88888888888889 - fitness:77.51776649746193 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:25:45.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.51776649746193, number of tests: 4, total length: 57
[MASTER] 09:25:46.030 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.857142857142858 - fitness:77.22754491017965 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 09:25:46.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.22754491017965, number of tests: 5, total length: 82
[MASTER] 09:25:46.182 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.875 - fitness:77.22513089005236 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:25:46.183 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.22513089005236, number of tests: 4, total length: 49
[MASTER] 09:25:46.926 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.88888888888889 - fitness:77.09375 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 09:25:46.926 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.09375, number of tests: 4, total length: 64
[MASTER] 09:25:48.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.88888888888889 - fitness:76.97424892703863 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:48.551 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.97424892703863, number of tests: 2, total length: 43
[MASTER] 09:25:49.262 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.88888888888889 - fitness:76.86363636363636 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:49.263 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.86363636363636, number of tests: 3, total length: 44
[MASTER] 09:25:50.202 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.88888888888889 - fitness:76.7609561752988 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:50.202 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.7609561752988, number of tests: 2, total length: 43
[MASTER] 09:25:53.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.88888888888889 - fitness:76.66538461538461 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:53.137 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.66538461538461, number of tests: 2, total length: 43
[MASTER] 09:25:54.669 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.875 - fitness:76.57740585774059 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:54.669 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.57740585774059, number of tests: 2, total length: 43
[MASTER] 09:25:55.231 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.88888888888889 - fitness:76.57620817843866 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:55.231 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.57620817843866, number of tests: 2, total length: 43
[MASTER] 09:25:55.911 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.88888888888889 - fitness:76.49280575539568 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:55.912 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.49280575539568, number of tests: 2, total length: 46
[MASTER] 09:25:56.320 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.88888888888889 - fitness:76.41463414634147 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:56.321 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.41463414634147, number of tests: 2, total length: 44
[MASTER] 09:25:59.964 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.875 - fitness:76.34220532319392 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:25:59.964 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.34220532319392, number of tests: 2, total length: 43
[MASTER] 09:26:00.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.88888888888889 - fitness:76.34121621621621 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:26:00.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.34121621621621, number of tests: 2, total length: 42
[MASTER] 09:26:00.239 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.888888888888886 - fitness:76.27213114754099 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:26:00.239 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.27213114754099, number of tests: 2, total length: 58
[MASTER] 09:26:00.922 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.888888888888886 - fitness:76.20700636942675 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:26:00.923 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.20700636942675, number of tests: 2, total length: 43
[MASTER] 09:26:01.941 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.875 - fitness:76.14634146341463 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:26:01.941 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14634146341463, number of tests: 3, total length: 54
[MASTER] 09:26:02.998 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.888888888888886 - fitness:76.14551083591331 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:26:02.998 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14551083591331, number of tests: 3, total length: 54
[MASTER] 09:26:04.213 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.888888888888886 - fitness:76.08734939759036 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 09:26:04.213 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08734939759036, number of tests: 3, total length: 46
[MASTER] 09:30:33.406 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 3062 generations, 1429744 statements, best individuals have fitness: 76.08734939759036
[MASTER] 09:30:33.409 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 46
* Number of covered goals: 2
* Generated 3 tests with total length 45
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     76 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 09:30:33.599 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:30:33.615 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 42 
[MASTER] 09:30:33.639 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 09:30:33.639 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 09:30:33.639 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:30:33.639 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:30:33.639 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:30:33.639 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 09:30:33.640 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 46
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'TypeInfoFactory_ESTest' to evosuite-tests
* Done!

* Computation finished
