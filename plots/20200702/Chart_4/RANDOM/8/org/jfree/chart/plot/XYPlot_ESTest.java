/*
 * This file was automatically generated by EvoSuite
 * Wed Jul 01 15:31:18 GMT 2020
 */

package org.jfree.chart.plot;

import org.junit.Test;
import static org.junit.Assert.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jfree.chart.axis.PeriodAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.YIntervalSeriesCollection;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYPlot_ESTest extends XYPlot_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      YIntervalSeriesCollection yIntervalSeriesCollection0 = new YIntervalSeriesCollection();
      PeriodAxis periodAxis0 = new PeriodAxis("");
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NullPointerException : null
      XYPlot xYPlot0 = new XYPlot(yIntervalSeriesCollection0, periodAxis0, periodAxis0, (XYItemRenderer) null);
  }
}
