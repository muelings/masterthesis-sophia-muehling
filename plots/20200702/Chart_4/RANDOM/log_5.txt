* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.plot.XYPlot
* Starting client
* Connecting to master process on port 7437
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_4_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.plot.XYPlot
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 17:11:05.523 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1079
[MASTER] 17:11:24.285 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 17:11:25.520 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:11:25.531 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 17:11:25.947 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:11:25.957 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
[MASTER] 17:11:25.958 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 481 | Tests with assertion: 1
* Generated 1 tests with total length 7
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         20 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 17:11:29.159 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:11:29.183 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 5 
[MASTER] 17:11:29.193 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 17:11:29.193 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IllegalArgumentException,, NullPointerException, IllegalArgumentException]
[MASTER] 17:11:29.193 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: IllegalArgumentException at 4
[MASTER] 17:11:29.193 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IllegalArgumentException,, NullPointerException, IllegalArgumentException]
[MASTER] 17:11:29.193 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NullPointerException at 3
[MASTER] 17:11:29.193 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [IllegalArgumentException,, NullPointerException, IllegalArgumentException]
[MASTER] 17:11:29.246 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
[MASTER] 17:11:29.251 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 5%
* Total number of goals: 1079
* Number of covered goals: 59
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYPlot_ESTest' to evosuite-tests
* Done!

* Computation finished
