* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 12364
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 53
[MASTER] 17:58:33.204 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:00:18.937 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 18:00:20.348 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/beans/NullPropertyPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 18:00:20.373 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 18:00:20.692 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/beans/NullPropertyPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 18:00:20.723 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*=*=*=* Total tests: 18216 | Tests with assertion: 1
[MASTER] 18:00:20.723 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 11
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        107 / 300         
[MASTER] 18:00:21.357 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:00:21.369 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 18:00:21.409 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 18:00:21.410 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [JXPathAbstractFactoryException:createBadFactoryException-234,, createPath:JXPathAbstractFactoryException]
[MASTER] 18:00:21.410 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createPath:JXPathAbstractFactoryException at 7
[MASTER] 18:00:21.410 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [JXPathAbstractFactoryException:createBadFactoryException-234,, createPath:JXPathAbstractFactoryException]
[MASTER] 18:00:21.777 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
[MASTER] 18:00:21.781 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 23%
* Total number of goals: 53
* Number of covered goals: 12
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
