* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 17914
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 18:36:55.930 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 18:36:55.935 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 53
* Using seed 1593707813170
* Starting evolution
[MASTER] 18:36:57.558 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.0 - branchDistance:0.0 - coverage:109.0 - ex: 0 - tex: 8
[MASTER] 18:36:57.559 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.0, number of tests: 6, total length: 102
[MASTER] 18:36:57.685 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:196.0 - branchDistance:0.0 - coverage:86.0 - ex: 0 - tex: 10
[MASTER] 18:36:57.685 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 196.0, number of tests: 7, total length: 116
[MASTER] 18:36:58.285 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:186.0 - branchDistance:0.0 - coverage:76.0 - ex: 0 - tex: 12
[MASTER] 18:36:58.285 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 186.0, number of tests: 6, total length: 132
[MASTER] 18:36:58.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:158.0 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 12
[MASTER] 18:36:58.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 158.0, number of tests: 7, total length: 163
[MASTER] 18:37:00.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:155.5 - branchDistance:0.0 - coverage:45.5 - ex: 0 - tex: 12
[MASTER] 18:37:00.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 155.5, number of tests: 7, total length: 208
[MASTER] 18:37:01.184 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:153.5 - branchDistance:0.0 - coverage:43.5 - ex: 0 - tex: 12
[MASTER] 18:37:01.184 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 153.5, number of tests: 7, total length: 209
[MASTER] 18:37:02.542 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:147.0 - branchDistance:0.0 - coverage:37.0 - ex: 0 - tex: 12
[MASTER] 18:37:02.542 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 147.0, number of tests: 8, total length: 180
[MASTER] 18:37:03.444 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:146.5 - branchDistance:0.0 - coverage:36.5 - ex: 0 - tex: 14
[MASTER] 18:37:03.445 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.5, number of tests: 10, total length: 231
[MASTER] 18:37:03.523 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:146.0 - branchDistance:0.0 - coverage:36.0 - ex: 0 - tex: 12
[MASTER] 18:37:03.523 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.0, number of tests: 7, total length: 145
[MASTER] 18:37:03.621 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:144.0 - branchDistance:0.0 - coverage:34.0 - ex: 0 - tex: 10
[MASTER] 18:37:03.621 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.0, number of tests: 9, total length: 188
[MASTER] 18:37:03.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:142.0 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 10
[MASTER] 18:37:03.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.0, number of tests: 10, total length: 219
[MASTER] 18:37:04.228 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:138.0 - branchDistance:0.0 - coverage:28.0 - ex: 0 - tex: 10
[MASTER] 18:37:04.228 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.0, number of tests: 9, total length: 180
[MASTER] 18:37:05.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:136.0 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 14
[MASTER] 18:37:05.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.0, number of tests: 10, total length: 249
[MASTER] 18:37:06.376 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:134.0 - branchDistance:0.0 - coverage:24.0 - ex: 0 - tex: 12
[MASTER] 18:37:06.376 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.0, number of tests: 11, total length: 214
[MASTER] 18:37:06.919 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:133.0 - branchDistance:0.0 - coverage:23.0 - ex: 0 - tex: 12
[MASTER] 18:37:06.919 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.0, number of tests: 11, total length: 225
[MASTER] 18:37:07.926 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:132.0 - branchDistance:0.0 - coverage:22.0 - ex: 0 - tex: 12
[MASTER] 18:37:07.927 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.0, number of tests: 11, total length: 236
[MASTER] 18:37:08.320 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:131.0 - branchDistance:0.0 - coverage:21.0 - ex: 0 - tex: 12
[MASTER] 18:37:08.320 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.0, number of tests: 11, total length: 227
[MASTER] 18:37:08.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:130.0 - branchDistance:0.0 - coverage:20.0 - ex: 0 - tex: 12
[MASTER] 18:37:08.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.0, number of tests: 12, total length: 244
[MASTER] 18:37:09.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:127.0 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 14
[MASTER] 18:37:09.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.0, number of tests: 12, total length: 261
[MASTER] 18:37:13.075 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:126.0 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 14
[MASTER] 18:37:13.075 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.0, number of tests: 12, total length: 253
[MASTER] 18:37:14.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:125.0 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 12
[MASTER] 18:37:14.324 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.0, number of tests: 11, total length: 243
[MASTER] 18:37:15.909 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:124.0 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 14
[MASTER] 18:37:15.909 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.0, number of tests: 11, total length: 227
[MASTER] 18:37:20.106 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:123.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 18
[MASTER] 18:37:20.106 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.0, number of tests: 12, total length: 253
[MASTER] 18:37:26.938 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:122.0 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 14
[MASTER] 18:37:26.938 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.0, number of tests: 10, total length: 196
[MASTER] 18:37:30.987 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:120.5 - branchDistance:0.0 - coverage:10.5 - ex: 0 - tex: 16
[MASTER] 18:37:30.987 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.5, number of tests: 10, total length: 200
[MASTER] 18:37:32.725 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:120.0 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 18
[MASTER] 18:37:32.725 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.0, number of tests: 11, total length: 224
[MASTER] 18:37:45.847 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:119.0 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 18
[MASTER] 18:37:45.847 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.0, number of tests: 10, total length: 197
[MASTER] 18:37:50.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:118.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 20
[MASTER] 18:37:50.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.0, number of tests: 11, total length: 210
[MASTER] 18:37:50.774 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:117.5 - branchDistance:0.0 - coverage:7.5 - ex: 0 - tex: 20
[MASTER] 18:37:50.774 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.5, number of tests: 11, total length: 199
[MASTER] 18:37:52.912 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:117.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 20
[MASTER] 18:37:52.912 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.0, number of tests: 11, total length: 193
[MASTER] 18:38:11.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:116.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 18
[MASTER] 18:38:11.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.0, number of tests: 10, total length: 171
[MASTER] 18:38:25.179 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999556304907267 - fitness:50.600773817975295 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 18
[MASTER] 18:38:25.179 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.600773817975295, number of tests: 10, total length: 163
[MASTER] 18:39:40.709 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999556304907267 - fitness:50.100773817975295 - branchDistance:0.0 - coverage:5.5 - ex: 0 - tex: 18
[MASTER] 18:39:40.709 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.100773817975295, number of tests: 10, total length: 139
[MASTER] 18:39:42.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999556304907267 - fitness:49.600773817975295 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 20
[MASTER] 18:39:42.615 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.600773817975295, number of tests: 11, total length: 151
[MASTER] 18:40:54.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999556304907267 - fitness:49.100773817975295 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 20
[MASTER] 18:40:54.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.100773817975295, number of tests: 11, total length: 153
[MASTER] 18:40:57.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999556304907267 - fitness:48.600773817975295 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 22
[MASTER] 18:40:57.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.600773817975295, number of tests: 12, total length: 164
[MASTER] 18:41:56.964 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1602 generations, 1112342 statements, best individuals have fitness: 48.600773817975295
[MASTER] 18:41:56.968 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 91%
* Total number of goals: 53
* Number of covered goals: 48
* Generated 12 tests with total length 153
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     49 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 18:41:57.340 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:41:57.398 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 130 
[MASTER] 18:41:57.505 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.505 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.506 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.507 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 18:41:57.507 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 18:41:57.507 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 18:41:57.507 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 18:41:57.507 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 18:41:57.507 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 18:41:57.507 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 18:41:57.508 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 18:41:57.508 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 18:41:57.508 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 18:41:57.508 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 18:41:57.508 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 18:41:57.508 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 18:41:57.508 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 18:41:57.509 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
