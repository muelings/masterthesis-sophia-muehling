* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 7396
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 18:45:22.695 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 18:45:22.700 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 53
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593708319959
* Starting evolution
[MASTER] 18:45:24.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.0 - branchDistance:0.0 - coverage:109.0 - ex: 0 - tex: 16
[MASTER] 18:45:24.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.0, number of tests: 9, total length: 266
[MASTER] 18:45:24.671 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:200.0 - branchDistance:0.0 - coverage:90.0 - ex: 0 - tex: 10
[MASTER] 18:45:24.671 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 200.0, number of tests: 5, total length: 91
[MASTER] 18:45:25.011 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:181.0 - branchDistance:0.0 - coverage:71.0 - ex: 0 - tex: 16
[MASTER] 18:45:25.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 181.0, number of tests: 10, total length: 228
[MASTER] 18:45:25.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:176.0 - branchDistance:0.0 - coverage:66.0 - ex: 0 - tex: 16
[MASTER] 18:45:25.270 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 176.0, number of tests: 10, total length: 212
[MASTER] 18:45:25.536 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:153.6 - branchDistance:0.0 - coverage:109.0 - ex: 0 - tex: 10
[MASTER] 18:45:25.536 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 153.6, number of tests: 7, total length: 124
[MASTER] 18:45:27.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:143.6 - branchDistance:0.0 - coverage:99.0 - ex: 0 - tex: 8
[MASTER] 18:45:27.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.6, number of tests: 6, total length: 133
[MASTER] 18:45:27.248 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:129.6 - branchDistance:0.0 - coverage:85.0 - ex: 0 - tex: 12
[MASTER] 18:45:27.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.6, number of tests: 9, total length: 172
[MASTER] 18:45:27.623 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:116.6 - branchDistance:0.0 - coverage:72.0 - ex: 0 - tex: 8
[MASTER] 18:45:27.623 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.6, number of tests: 7, total length: 153
[MASTER] 18:45:27.899 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:109.6 - branchDistance:0.0 - coverage:65.0 - ex: 0 - tex: 12
[MASTER] 18:45:27.899 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.6, number of tests: 9, total length: 170
[MASTER] 18:45:27.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:98.6 - branchDistance:0.0 - coverage:54.0 - ex: 0 - tex: 16
[MASTER] 18:45:27.918 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.6, number of tests: 10, total length: 176
[MASTER] 18:45:28.630 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:96.6 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 18
[MASTER] 18:45:28.630 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.6, number of tests: 11, total length: 195
[MASTER] 18:45:28.648 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:85.6 - branchDistance:0.0 - coverage:41.0 - ex: 0 - tex: 16
[MASTER] 18:45:28.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.6, number of tests: 10, total length: 194
[MASTER] 18:45:29.277 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:79.1 - branchDistance:0.0 - coverage:34.5 - ex: 0 - tex: 18
[MASTER] 18:45:29.277 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.1, number of tests: 11, total length: 224
[MASTER] 18:45:31.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:78.1 - branchDistance:0.0 - coverage:33.5 - ex: 0 - tex: 20
[MASTER] 18:45:31.918 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.1, number of tests: 11, total length: 232
[MASTER] 18:45:32.045 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:75.6 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 20
[MASTER] 18:45:32.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.6, number of tests: 12, total length: 248
[MASTER] 18:45:33.228 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:75.1 - branchDistance:0.0 - coverage:30.5 - ex: 0 - tex: 18
[MASTER] 18:45:33.228 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.1, number of tests: 11, total length: 239
[MASTER] 18:45:33.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:73.1 - branchDistance:0.0 - coverage:28.5 - ex: 0 - tex: 16
[MASTER] 18:45:33.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.1, number of tests: 11, total length: 229
[MASTER] 18:45:33.561 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:71.6 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 20
[MASTER] 18:45:33.561 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.6, number of tests: 12, total length: 252
[MASTER] 18:45:33.890 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:71.1 - branchDistance:0.0 - coverage:26.5 - ex: 0 - tex: 16
[MASTER] 18:45:33.890 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.1, number of tests: 11, total length: 230
[MASTER] 18:45:34.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:69.6 - branchDistance:0.0 - coverage:25.0 - ex: 0 - tex: 18
[MASTER] 18:45:34.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.6, number of tests: 11, total length: 241
[MASTER] 18:45:35.622 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:68.6 - branchDistance:0.0 - coverage:24.0 - ex: 0 - tex: 18
[MASTER] 18:45:35.622 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.6, number of tests: 12, total length: 262
[MASTER] 18:45:36.285 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:66.6 - branchDistance:0.0 - coverage:22.0 - ex: 0 - tex: 16
[MASTER] 18:45:36.285 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.6, number of tests: 12, total length: 262
[MASTER] 18:45:37.117 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:64.6 - branchDistance:0.0 - coverage:20.0 - ex: 0 - tex: 16
[MASTER] 18:45:37.117 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.6, number of tests: 12, total length: 264
[MASTER] 18:45:38.757 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:63.6 - branchDistance:0.0 - coverage:19.0 - ex: 0 - tex: 20
[MASTER] 18:45:38.757 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.6, number of tests: 14, total length: 290
[MASTER] 18:45:38.897 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:62.6 - branchDistance:0.0 - coverage:18.0 - ex: 0 - tex: 18
[MASTER] 18:45:38.897 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.6, number of tests: 13, total length: 299
[MASTER] 18:45:41.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:61.6 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 20
[MASTER] 18:45:41.551 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.6, number of tests: 13, total length: 285
[MASTER] 18:45:42.046 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:60.6 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 20
[MASTER] 18:45:42.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.6, number of tests: 12, total length: 259
[MASTER] 18:45:43.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:59.6 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 22
[MASTER] 18:45:43.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.6, number of tests: 13, total length: 300
[MASTER] 18:45:43.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:58.6 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 26
[MASTER] 18:45:43.570 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.6, number of tests: 13, total length: 318
[MASTER] 18:45:44.682 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:57.6 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 24
[MASTER] 18:45:44.682 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.6, number of tests: 13, total length: 317
[MASTER] 18:46:01.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:56.6 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 20
[MASTER] 18:46:01.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.6, number of tests: 13, total length: 242
[MASTER] 18:46:19.224 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:55.6 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 18
[MASTER] 18:46:19.224 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.6, number of tests: 11, total length: 207
[MASTER] 18:46:20.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:49.33333333333333 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 20
[MASTER] 18:46:20.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.33333333333333, number of tests: 11, total length: 211
[MASTER] 18:46:20.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:48.33333333333333 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 22
[MASTER] 18:46:20.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.33333333333333, number of tests: 12, total length: 244
[MASTER] 18:46:31.127 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:46.83333333333333 - branchDistance:0.0 - coverage:9.5 - ex: 0 - tex: 22
[MASTER] 18:46:31.127 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.83333333333333, number of tests: 12, total length: 246
[MASTER] 18:46:37.384 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:46.33333333333333 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 24
[MASTER] 18:46:37.384 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.33333333333333, number of tests: 13, total length: 264
[MASTER] 18:46:39.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:45.33333333333333 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 26
[MASTER] 18:46:39.691 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.33333333333333, number of tests: 14, total length: 295
[MASTER] 18:47:07.029 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:44.33333333333333 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 28
[MASTER] 18:47:07.029 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 44.33333333333333, number of tests: 16, total length: 330
[MASTER] 18:47:10.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:42.83333333333333 - branchDistance:0.0 - coverage:5.5 - ex: 0 - tex: 30
[MASTER] 18:47:10.226 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.83333333333333, number of tests: 16, total length: 337
[MASTER] 18:47:29.980 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:42.33333333333333 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 26
[MASTER] 18:47:29.980 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.33333333333333, number of tests: 14, total length: 317
[MASTER] 18:48:07.443 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:41.83333333333333 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 26
[MASTER] 18:48:07.444 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.83333333333333, number of tests: 14, total length: 269
[MASTER] 18:48:09.873 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:41.33333333333333 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 30
[MASTER] 18:48:09.873 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.33333333333333, number of tests: 16, total length: 325
[MASTER] 18:48:47.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:40.83333333333333 - branchDistance:0.0 - coverage:3.5 - ex: 0 - tex: 26
[MASTER] 18:48:47.754 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.83333333333333, number of tests: 15, total length: 271

* Search finished after 302s and 770 generations, 648560 statements, best individuals have fitness: 40.83333333333333
[MASTER] 18:50:24.504 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:50:24.511 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 92%
* Total number of goals: 53
* Number of covered goals: 49
* Generated 14 tests with total length 246
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        302 / 300          Finished!
	- ZeroFitness :                     41 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 18:50:24.933 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:50:24.967 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 206 
[MASTER] 18:50:26.795 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.796 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.810 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 1.0
[MASTER] 18:50:26.810 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 18:50:26.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 18:50:26.812 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 18:50:26.812 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 18:51:16.951 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 10 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 18:51:16.954 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 25%
* Total number of goals: 53
* Number of covered goals: 13
* Generated 1 tests with total length 10
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 7
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
