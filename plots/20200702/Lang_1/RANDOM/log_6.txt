* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting client
* Connecting to master process on port 8254
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 20:30:32.009 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 371
[MASTER] 20:30:56.252 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 20:30:57.405 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 20:30:57.409 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 20:30:57.699 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 20:30:57.706 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 5505 | Tests with assertion: 1
[MASTER] 20:30:57.707 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 2
* GA-Budget:
	- MaxTime :                         25 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 20:30:58.654 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 20:30:58.670 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 1 
[MASTER] 20:30:58.674 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 20:30:58.674 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 20:30:58.674 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createNumber:NumberFormatException at 0
[MASTER] 20:30:58.674 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 20:30:58.681 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 20:30:58.687 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 5%
* Total number of goals: 371
* Number of covered goals: 18
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NumberUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
