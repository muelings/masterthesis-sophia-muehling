* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting client
* Connecting to master process on port 7288
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 371
[MASTER] 20:41:33.470 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 20:41:46.453 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 20:41:47.564 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 20:41:47.569 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 20:41:47.910 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 20:41:47.916 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 20:41:47.917 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 2640 | Tests with assertion: 1
* Generated 1 tests with total length 10
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                         14 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 20:41:48.905 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 20:41:48.917 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 6 
[MASTER] 20:41:48.925 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 20:41:48.926 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 20:41:48.926 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createNumber:NumberFormatException at 5
[MASTER] 20:41:48.926 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 20:41:48.977 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 20:41:48.980 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 371
* Number of covered goals: 13
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NumberUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
