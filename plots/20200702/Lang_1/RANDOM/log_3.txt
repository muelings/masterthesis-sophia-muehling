* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting client
* Connecting to master process on port 20155
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 20:13:04.382 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 371
[MASTER] 20:13:36.125 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 20:13:37.307 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 20:13:37.313 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 20:13:37.656 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 20:13:37.665 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 20:13:37.665 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 8548 | Tests with assertion: 1
* Generated 1 tests with total length 9
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                         33 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 20:13:38.687 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 20:13:38.697 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 20:13:38.701 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 20:13:38.701 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 20:13:38.701 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createNumber:NumberFormatException at 3
[MASTER] 20:13:38.701 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 20:13:38.723 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
[MASTER] 20:13:38.727 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 371
* Number of covered goals: 16
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NumberUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
