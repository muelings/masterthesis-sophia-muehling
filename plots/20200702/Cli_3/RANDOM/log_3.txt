* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.TypeHandler
* Starting client
* Connecting to master process on port 16594
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_3_fixed/target/classes/
[MASTER] 01:55:17.129 [logback-1] WARN  CheapPurityAnalyzer - org.apache.commons.lang.math.NumberUtils was not found in the inheritance tree. Using DEFAULT value for cheap-purity analysis
[MASTER] 01:55:17.129 [logback-1] WARN  InheritanceTree - Class not in inheritance graph: org.apache.commons.lang.math.NumberUtils
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.TypeHandler
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 01:55:17.210 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 31
[MASTER] 01:55:17.328 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 01:55:18.593 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 01:55:18.614 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 01:55:18.925 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 01:55:18.965 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 01:55:18.965 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 1 | Tests with assertion: 1
* Generated 1 tests with total length 19
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          1 / 300         
	- RMIStoppingCondition
[MASTER] 01:55:19.375 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:55:19.401 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 01:55:19.424 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 01:55:19.424 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createNumber:NoClassDefFoundError]
[MASTER] 01:55:19.424 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: createNumber:NoClassDefFoundError at 10
[MASTER] 01:55:19.424 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createNumber:NoClassDefFoundError]
[MASTER] 01:55:19.696 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 01:55:19.700 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 31
* Number of covered goals: 2
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeHandler_ESTest' to evosuite-tests
* Done!

* Computation finished
