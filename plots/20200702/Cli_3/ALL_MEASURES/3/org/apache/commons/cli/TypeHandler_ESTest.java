/*
 * This file was automatically generated by EvoSuite
 * Fri Jul 03 00:00:28 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.cli.TypeHandler;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class TypeHandler_ESTest extends TypeHandler_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Object object0 = TypeHandler.createObject("&COJ`Cv~@b,4x");
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NoClassDefFoundError : org/apache/commons/lang/math/NumberUtils
      TypeHandler.createValue((String) null, ((PatternOptionBuilder) object0).NUMBER_VALUE);
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      // Undeclared exception!
      try { 
        TypeHandler.createValue("org.apache.commons.cli.PatternOptionBuilder", (Object) "LovR|LQ@!V$wK0");
        fail("Expecting exception: ClassCastException");
      
      } catch(ClassCastException e) {
         //
         // no message in exception (getMessage() returned null)
         //
      }
  }

  @Test(timeout = 4000)
  public void test2()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NoClassDefFoundError : org/apache/commons/lang/math/NumberUtils
      TypeHandler.createNumber((String) null);
  }
}
