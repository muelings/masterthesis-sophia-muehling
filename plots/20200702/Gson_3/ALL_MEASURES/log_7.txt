* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.ConstructorConstructor
* Starting client
* Connecting to master process on port 11162
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.ConstructorConstructor
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 11:07:36.247 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 11:07:36.252 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 66
* Using seed 1593680853403
* Starting evolution
[MASTER] 11:08:48.810 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.0 - branchDistance:0.0 - coverage:104.0 - ex: 0 - tex: 0
[MASTER] 11:08:48.810 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.0, number of tests: 6, total length: 0

[MASTER] 11:12:37.618 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 190 generations, 0 statements, best individuals have fitness: 209.0
[MASTER] 11:12:37.622 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 35
* Number of covered goals: 0
* Generated 2 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                    209 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 11:12:37.791 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:12:37.803 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 11:12:37.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 11:12:37.811 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 35
[MASTER] 11:12:37.811 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 11:12:37.812 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'ConstructorConstructor_ESTest' to evosuite-tests
* Done!

* Computation finished
