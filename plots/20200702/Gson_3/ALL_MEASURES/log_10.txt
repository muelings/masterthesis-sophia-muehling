* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.ConstructorConstructor
* Starting client
* Connecting to master process on port 6419
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.ConstructorConstructor
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 11:38:13.461 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 11:38:13.468 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 66
* Using seed 1593682690608
* Starting evolution
[MASTER] 11:39:21.394 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.0 - branchDistance:0.0 - coverage:104.0 - ex: 0 - tex: 0
[MASTER] 11:39:21.394 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.0, number of tests: 3, total length: 0
[MASTER] 11:43:14.727 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 190 generations, 0 statements, best individuals have fitness: 209.0
[MASTER] 11:43:14.731 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 35
* Number of covered goals: 0
* Generated 8 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                    209 / 0           
[MASTER] 11:43:14.921 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:43:14.941 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 11:43:14.961 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 11:43:14.963 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 35
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'ConstructorConstructor_ESTest' to evosuite-tests
* Done!

* Computation finished
