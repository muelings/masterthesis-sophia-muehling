* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.JsonPointer
* Starting client
* Connecting to master process on port 2088
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.JsonPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 13:46:00.584 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:46:04.914 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 13:46:05.967 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 13:46:05.977 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 13:46:06.340 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 843 | Tests with assertion: 1
* Generated 1 tests with total length 4
* GA-Budget:
[MASTER] 13:46:06.347 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 13:46:06.347 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          5 / 300         
[MASTER] 13:46:06.819 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:46:06.837 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 13:46:06.844 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 13:46:06.844 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:compile-96,, _parseTail:NumberFormatException, compile:MockIllegalArgumentException]
[MASTER] 13:46:06.844 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: compile:MockIllegalArgumentException at 1
[MASTER] 13:46:06.844 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:compile-96,, _parseTail:NumberFormatException, compile:MockIllegalArgumentException]
[MASTER] 13:46:06.844 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: _parseTail:NumberFormatException at 0
[MASTER] 13:46:06.844 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [MockIllegalArgumentException:compile-96,, _parseTail:NumberFormatException, compile:MockIllegalArgumentException]
[MASTER] 13:46:06.865 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 13:46:06.869 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 20%
* Total number of goals: 70
* Number of covered goals: 14
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'JsonPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
