* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.text.translate.LookupTranslator
* Starting client
* Connecting to master process on port 2802
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.text.translate.LookupTranslator
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 21:14:02.023 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 21:14:02.027 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 14
* Using seed 1593717239786
* Starting evolution
[MASTER] 21:14:03.068 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:60.0 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 14
[MASTER] 21:14:03.068 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.0, number of tests: 7, total length: 384
[MASTER] 21:14:03.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:49.333333332402006 - branchDistance:0.0 - coverage:16.33333333240201 - ex: 0 - tex: 6
[MASTER] 21:14:03.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.333333332402006, number of tests: 4, total length: 90
[MASTER] 21:14:03.193 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:48.99999999906868 - branchDistance:0.0 - coverage:15.999999999068677 - ex: 0 - tex: 8
[MASTER] 21:14:03.193 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.99999999906868, number of tests: 5, total length: 235
[MASTER] 21:14:03.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.345238095238095 - fitness:5.092092574734812 - branchDistance:0.0 - coverage:1.5 - ex: 0 - tex: 12
[MASTER] 21:14:03.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.092092574734812, number of tests: 9, total length: 360
[MASTER] 21:14:04.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.345238095238095 - fitness:3.5920925747348122 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 21:14:04.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.5920925747348122, number of tests: 9, total length: 368
[MASTER] 21:14:07.165 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.444444444444445 - fitness:3.3801652892561984 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:14:07.165 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.3801652892561984, number of tests: 9, total length: 340
[MASTER] 21:14:10.074 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.551587301587302 - fitness:3.057667772390916 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 21:14:10.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.057667772390916, number of tests: 8, total length: 202
[MASTER] 21:14:13.525 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.967142857142857 - fitness:2.6871281162913307 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:14:13.525 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6871281162913307, number of tests: 8, total length: 185
[MASTER] 21:14:17.420 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.23575284364758 - fitness:2.581359500051685 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 21:14:17.420 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.581359500051685, number of tests: 8, total length: 146
[MASTER] 21:14:18.697 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.399205902456675 - fitness:2.5686885142987963 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:14:18.697 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5686885142987963, number of tests: 8, total length: 143
[MASTER] 21:14:19.076 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.478066800594682 - fitness:2.489892004578084 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:14:19.076 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.489892004578084, number of tests: 9, total length: 149
[MASTER] 21:14:20.486 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.66226176336818 - fitness:2.4772234012107353 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 21:14:20.487 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.4772234012107353, number of tests: 8, total length: 139
[MASTER] 21:14:21.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.513573232323232 - fitness:2.4213647771406137 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:14:21.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.4213647771406137, number of tests: 7, total length: 129
[MASTER] 21:14:21.870 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.562001760649487 - fitness:2.4183138685775383 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:14:21.871 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.4183138685775383, number of tests: 7, total length: 121
[MASTER] 21:14:23.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.625510735667767 - fitness:2.414332713804949 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:14:23.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.414332713804949, number of tests: 7, total length: 119
[MASTER] 21:14:31.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.635942662072583 - fitness:2.3538702668858984 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:14:31.143 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.3538702668858984, number of tests: 7, total length: 121
[MASTER] 21:14:31.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.625510735667767 - fitness:2.2487555986722123 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:14:31.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2487555986722123, number of tests: 7, total length: 123
[MASTER] 21:14:34.938 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.871066291223325 - fitness:1.8405350062542216 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 7
[MASTER] 21:14:34.938 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.8405350062542216, number of tests: 7, total length: 189
[MASTER] 21:14:36.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.625510735667767 - fitness:1.748755598672212 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 5
[MASTER] 21:14:36.259 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.748755598672212, number of tests: 6, total length: 132
[MASTER] 21:14:36.865 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.871066291223325 - fitness:1.6738683395875549 - branchDistance:0.0 - coverage:0.0 - ex: 2 - tex: 4
[MASTER] 21:14:36.865 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6738683395875549, number of tests: 7, total length: 136
[MASTER] 21:14:38.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.350725384798615 - fitness:1.647462563338801 - branchDistance:0.0 - coverage:0.0 - ex: 2 - tex: 6
[MASTER] 21:14:38.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.647462563338801, number of tests: 6, total length: 122
[MASTER] 21:14:40.689 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.871066291223325 - fitness:1.6199689614685202 - branchDistance:0.0 - coverage:0.0 - ex: 2 - tex: 4
[MASTER] 21:14:40.689 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6199689614685202, number of tests: 7, total length: 121
[MASTER] 21:14:41.545 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.350725384798615 - fitness:1.5641292300054677 - branchDistance:0.0 - coverage:0.0 - ex: 3 - tex: 7
[MASTER] 21:14:41.545 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.5641292300054677, number of tests: 7, total length: 163
[MASTER] 21:14:41.979 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.871066291223325 - fitness:1.536635628135187 - branchDistance:0.0 - coverage:0.0 - ex: 3 - tex: 7
[MASTER] 21:14:41.979 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.536635628135187, number of tests: 7, total length: 163
[MASTER] 21:14:45.714 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.871066291223325 - fitness:1.486635628135187 - branchDistance:0.0 - coverage:0.0 - ex: 4 - tex: 6
[MASTER] 21:14:45.714 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.486635628135187, number of tests: 7, total length: 199
[MASTER] 21:14:47.082 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.350725384798615 - fitness:1.4143878216901147 - branchDistance:0.0 - coverage:0.0 - ex: 4 - tex: 8
[MASTER] 21:14:47.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.4143878216901147, number of tests: 8, total length: 236
[MASTER] 21:15:01.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.350725384798615 - fitness:1.3810544883567815 - branchDistance:0.0 - coverage:0.0 - ex: 5 - tex: 9
[MASTER] 21:15:01.008 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.3810544883567815, number of tests: 9, total length: 321
[MASTER] 21:15:04.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.146200497920788 - fitness:1.3369207720368472 - branchDistance:0.0 - coverage:0.0 - ex: 4 - tex: 8
[MASTER] 21:15:04.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.3369207720368472, number of tests: 8, total length: 229
[MASTER] 21:15:18.089 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.146200497920788 - fitness:1.27977791489399 - branchDistance:0.0 - coverage:0.0 - ex: 6 - tex: 8
[MASTER] 21:15:18.089 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.27977791489399, number of tests: 9, total length: 271
[MASTER] 21:15:32.199 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.146200497920788 - fitness:1.2619207720368473 - branchDistance:0.0 - coverage:0.0 - ex: 7 - tex: 9
[MASTER] 21:15:32.199 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.2619207720368473, number of tests: 10, total length: 303
[MASTER] 21:15:34.762 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.33918347470799 - fitness:1.2335487099072546 - branchDistance:0.0 - coverage:0.0 - ex: 6 - tex: 8
[MASTER] 21:15:34.762 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.2335487099072546, number of tests: 9, total length: 263
[MASTER] 21:15:38.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.33918347470799 - fitness:1.2156915670501118 - branchDistance:0.0 - coverage:0.0 - ex: 7 - tex: 9
[MASTER] 21:15:38.549 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.2156915670501118, number of tests: 9, total length: 301
[MASTER] 21:15:40.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.33918347470799 - fitness:1.201802678161223 - branchDistance:0.0 - coverage:0.0 - ex: 8 - tex: 10
[MASTER] 21:15:40.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.201802678161223, number of tests: 10, total length: 346
[MASTER] 21:15:41.541 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.095296435588068 - fitness:1.1882890780288045 - branchDistance:0.0 - coverage:0.0 - ex: 7 - tex: 9
[MASTER] 21:15:41.541 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1882890780288045, number of tests: 9, total length: 302
[MASTER] 21:15:44.238 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.095296435588068 - fitness:1.1744001891399156 - branchDistance:0.0 - coverage:0.0 - ex: 8 - tex: 10
[MASTER] 21:15:44.238 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1744001891399156, number of tests: 10, total length: 341
[MASTER] 21:15:44.589 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.352710866209165 - fitness:1.1653826765423463 - branchDistance:0.0 - coverage:0.0 - ex: 8 - tex: 10
[MASTER] 21:15:44.589 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1653826765423463, number of tests: 10, total length: 341
[MASTER] 21:15:51.028 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.352710866209165 - fitness:1.1542715654312352 - branchDistance:0.0 - coverage:0.0 - ex: 9 - tex: 11
[MASTER] 21:15:51.028 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1542715654312352, number of tests: 11, total length: 381
[MASTER] 21:15:51.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.155448047657543 - fitness:1.1382187922797649 - branchDistance:0.0 - coverage:0.0 - ex: 8 - tex: 10
[MASTER] 21:15:51.449 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1382187922797649, number of tests: 10, total length: 341
[MASTER] 21:15:55.182 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.155448047657543 - fitness:1.1271076811686538 - branchDistance:0.0 - coverage:0.0 - ex: 9 - tex: 11
[MASTER] 21:15:55.182 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1271076811686538, number of tests: 11, total length: 380
[MASTER] 21:16:25.614 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.155448047657543 - fitness:1.1180167720777445 - branchDistance:0.0 - coverage:0.0 - ex: 10 - tex: 12
[MASTER] 21:16:25.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1180167720777445, number of tests: 12, total length: 421
[MASTER] 21:16:31.714 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.920649660706236 - fitness:1.1135969734020483 - branchDistance:0.0 - coverage:0.0 - ex: 8 - tex: 12
[MASTER] 21:16:31.715 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1135969734020483, number of tests: 11, total length: 382
[MASTER] 21:16:34.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.920649660706236 - fitness:1.1024858622909373 - branchDistance:0.0 - coverage:0.0 - ex: 9 - tex: 13
[MASTER] 21:16:34.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.1024858622909373, number of tests: 12, total length: 421
[MASTER] 21:16:42.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.920649660706236 - fitness:1.093394953200028 - branchDistance:0.0 - coverage:0.0 - ex: 10 - tex: 14
[MASTER] 21:16:42.457 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.093394953200028, number of tests: 13, total length: 452
[MASTER] 21:16:43.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.444584749311304 - fitness:1.0862971046556316 - branchDistance:0.0 - coverage:0.0 - ex: 9 - tex: 13
[MASTER] 21:16:43.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.0862971046556316, number of tests: 12, total length: 419
[MASTER] 21:17:05.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.444584749311304 - fitness:1.0772061955647225 - branchDistance:0.0 - coverage:0.0 - ex: 10 - tex: 14
[MASTER] 21:17:05.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.0772061955647225, number of tests: 13, total length: 454
[MASTER] 21:17:10.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.444584749311304 - fitness:1.069630437988965 - branchDistance:0.0 - coverage:0.0 - ex: 11 - tex: 15
[MASTER] 21:17:10.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.069630437988965, number of tests: 14, total length: 491
[MASTER] 21:17:12.183 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.444584749311304 - fitness:1.0632201815787086 - branchDistance:0.0 - coverage:0.0 - ex: 12 - tex: 16
[MASTER] 21:17:12.183 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.0632201815787086, number of tests: 15, total length: 540
[MASTER] 21:17:16.233 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.444584749311304 - fitness:1.057725676084203 - branchDistance:0.0 - coverage:0.0 - ex: 13 - tex: 15
[MASTER] 21:17:16.233 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.057725676084203, number of tests: 15, total length: 531
[MASTER] 21:17:25.726 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.444584749311304 - fitness:1.0529637713222983 - branchDistance:0.0 - coverage:0.0 - ex: 14 - tex: 16
[MASTER] 21:17:25.726 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.0529637713222983, number of tests: 16, total length: 569
[MASTER] 21:17:33.636 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.444584749311304 - fitness:1.0487971046556317 - branchDistance:0.0 - coverage:0.0 - ex: 15 - tex: 17
[MASTER] 21:17:33.636 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.0487971046556317, number of tests: 17, total length: 609
[MASTER] 21:17:37.103 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.19658091215824 - fitness:1.035383265578414 - branchDistance:0.0 - coverage:0.0 - ex: 13 - tex: 15
[MASTER] 21:17:37.103 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.035383265578414, number of tests: 16, total length: 567
[MASTER] 21:17:41.215 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.19658091215824 - fitness:1.0306213608165093 - branchDistance:0.0 - coverage:0.0 - ex: 14 - tex: 16
[MASTER] 21:17:41.215 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.0306213608165093, number of tests: 17, total length: 605
[MASTER] 21:17:46.435 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.19658091215824 - fitness:1.002432732935085 - branchDistance:0.0 - coverage:0.0 - ex: 14 - tex: 16
[MASTER] 21:17:46.435 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.002432732935085, number of tests: 17, total length: 607
[MASTER] 21:18:11.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.19658091215824 - fitness:0.9982660662684184 - branchDistance:0.0 - coverage:0.0 - ex: 15 - tex: 17
[MASTER] 21:18:11.412 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 0.9982660662684184, number of tests: 18, total length: 635
[MASTER] 21:18:39.584 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.19658091215824 - fitness:0.9945895956801831 - branchDistance:0.0 - coverage:0.0 - ex: 16 - tex: 18
[MASTER] 21:18:39.584 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 0.9945895956801831, number of tests: 19, total length: 670
[MASTER] 21:19:03.120 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 270 generations, 339639 statements, best individuals have fitness: 0.9945895956801831
[MASTER] 21:19:03.127 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 14
* Number of covered goals: 14
* Generated 19 tests with total length 660
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                      1 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 21:19:03.449 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:19:03.538 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 536 
[MASTER] 21:19:04.072 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.072 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.072 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: [translate:NullPointerException, NullPointerException:translate-79,]
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: translate:NullPointerException at 30
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - Test8, uniqueExceptions: [translate:NullPointerException, NullPointerException:translate-79,]
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: translate:NullPointerException at 30
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: translate(Ljava/lang/CharSequence;ILjava/io/Writer;)I
[MASTER] 21:19:04.073 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 30
[MASTER] 21:19:04.109 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.110 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.110 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.110 [logback-1] WARN  RegressionSuiteMinimizer - Test14, uniqueExceptions: [translate:NullPointerException, NullPointerException:translate-79,]
[MASTER] 21:19:04.110 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: translate:NullPointerException at 31
[MASTER] 21:19:04.110 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 31
[MASTER] 21:19:04.134 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.134 [logback-1] WARN  RegressionSuiteMinimizer - Test15, uniqueExceptions: [translate:NullPointerException, NullPointerException:translate-79,]
[MASTER] 21:19:04.134 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: translate:NullPointerException at 26
[MASTER] 21:19:04.134 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 26
[MASTER] 21:19:04.182 [logback-1] WARN  RegressionSuiteMinimizer - Test17 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.182 [logback-1] WARN  RegressionSuiteMinimizer - Test17, uniqueExceptions: [translate:NullPointerException, NullPointerException:translate-79,]
[MASTER] 21:19:04.182 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: translate:NullPointerException at 30
[MASTER] 21:19:04.182 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 30
[MASTER] 21:19:04.205 [logback-1] WARN  RegressionSuiteMinimizer - Test18 - Difference in number of exceptions: 1.0
[MASTER] 21:19:04.205 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [translate:NullPointerException, NullPointerException:translate-79,]
[MASTER] 21:19:04.205 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 21:19:04.205 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:19:04.205 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 21:19:04.206 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 21:19:04.206 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 21:19:04.206 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 21:19:04.206 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 21:19:04.206 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 21:19:04.206 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 21:19:04.206 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 21:19:06.639 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 25 
* Going to analyze the coverage criteria
[MASTER] 21:19:06.640 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 93%
* Total number of goals: 14
* Number of covered goals: 13
* Generated 9 tests with total length 25
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'LookupTranslator_ESTest' to evosuite-tests
* Done!

* Computation finished
