* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.text.translate.LookupTranslator
* Starting client
* Connecting to master process on port 15177
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.text.translate.LookupTranslator
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 21:45:43.816 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 21:45:43.820 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 14
* Using seed 1593719141551
* Starting evolution
[MASTER] 21:45:44.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:35.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 18
[MASTER] 21:45:44.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.0, number of tests: 9, total length: 487
[MASTER] 21:45:45.894 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:17.139253539253538 - branchDistance:0.0 - coverage:6.996396396396396 - ex: 0 - tex: 14
[MASTER] 21:45:45.894 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.139253539253538, number of tests: 8, total length: 399
[MASTER] 21:45:46.011 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:16.142857142857142 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 12
[MASTER] 21:45:46.012 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.142857142857142, number of tests: 8, total length: 428
[MASTER] 21:45:46.390 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:12.142857142857142 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 10
[MASTER] 21:45:46.390 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.142857142857142, number of tests: 8, total length: 401
[MASTER] 21:45:47.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:11.142857142857142 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 21:45:47.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.142857142857142, number of tests: 9, total length: 444
[MASTER] 21:45:49.567 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:10.642857142857142 - branchDistance:0.0 - coverage:0.5 - ex: 0 - tex: 10
[MASTER] 21:45:49.567 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.642857142857142, number of tests: 8, total length: 289
[MASTER] 21:45:51.220 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:10.142857142857142 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:45:51.220 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.142857142857142, number of tests: 8, total length: 293
[MASTER] 21:45:51.437 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2444444444444445 - fitness:7.601694915254237 - branchDistance:0.0 - coverage:0.5 - ex: 0 - tex: 8
[MASTER] 21:45:51.437 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.601694915254237, number of tests: 8, total length: 256
[MASTER] 21:45:52.539 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.2444444444444445 - fitness:7.101694915254237 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:45:52.539 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.101694915254237, number of tests: 9, total length: 269
[MASTER] 21:45:53.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.28 - fitness:6.095541401273885 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:45:53.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.095541401273885, number of tests: 8, total length: 268
[MASTER] 21:45:55.319 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.291228070175439 - fitness:5.3888354186718 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:45:55.319 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.3888354186718, number of tests: 8, total length: 265
[MASTER] 21:45:56.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.3133333333333335 - fitness:4.849238171611869 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 21:45:56.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.849238171611869, number of tests: 8, total length: 244
[MASTER] 21:45:57.698 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.326484018264841 - fitness:4.431089351285189 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 21:45:57.698 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.431089351285189, number of tests: 9, total length: 236
[MASTER] 21:45:58.786 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.326484018264841 - fitness:4.098828211364139 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 21:45:58.786 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.098828211364139, number of tests: 8, total length: 166
[MASTER] 21:45:59.065 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.330374753451677 - fitness:4.097661097852029 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 21:45:59.065 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.097661097852029, number of tests: 8, total length: 161
[MASTER] 21:45:59.129 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.078178694158076 - fitness:3.44682388491099 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 21:45:59.129 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.44682388491099, number of tests: 8, total length: 179
[MASTER] 21:45:59.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.07648401826484 - fitness:3.1497778618016827 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 5
[MASTER] 21:45:59.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1497778618016827, number of tests: 6, total length: 87
[MASTER] 21:46:01.020 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.079885057471264 - fitness:3.1490318283457825 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 5
[MASTER] 21:46:01.020 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1490318283457825, number of tests: 6, total length: 67
[MASTER] 21:46:01.356 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.299456991237813 - fitness:3.1017408754546594 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 5
[MASTER] 21:46:01.356 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1017408754546594, number of tests: 6, total length: 69
[MASTER] 21:46:01.811 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.082955974842768 - fitness:2.772250233343307 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:46:01.811 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.772250233343307, number of tests: 6, total length: 69
[MASTER] 21:46:06.000 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.582955974842768 - fitness:2.5535256630167615 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:46:06.000 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5535256630167615, number of tests: 3, total length: 54
[MASTER] 21:46:06.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.076504361939541 - fitness:2.4904824630756597 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:46:06.720 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.4904824630756597, number of tests: 4, total length: 56
[MASTER] 21:46:07.303 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.24963530269876 - fitness:2.080275373934029 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:46:07.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.080275373934029, number of tests: 4, total length: 51
[MASTER] 21:46:07.816 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.249635833940278 - fitness:2.080275332476104 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:46:07.817 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.080275332476104, number of tests: 3, total length: 50
[MASTER] 21:46:08.507 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.749801381525693 - fitness:2.0421834364395783 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:08.507 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.0421834364395783, number of tests: 3, total length: 46
[MASTER] 21:46:11.458 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.74980254986592 - fitness:1.9712777243211006 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:11.458 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9712777243211006, number of tests: 2, total length: 41
[MASTER] 21:46:13.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.74990946590542 - fitness:1.9712704919605457 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:13.259 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9712704919605457, number of tests: 2, total length: 44
[MASTER] 21:46:14.583 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.749819423852852 - fitness:1.7929387262179264 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:14.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7929387262179264, number of tests: 2, total length: 45
[MASTER] 21:46:20.313 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.949891422248132 - fitness:1.7825707117693184 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:20.313 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7825707117693184, number of tests: 2, total length: 45
[MASTER] 21:46:27.914 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.949891451858907 - fitness:1.782570710247151 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:46:27.914 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.782570710247151, number of tests: 2, total length: 52
[MASTER] 21:46:30.743 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.94989142271941 - fitness:1.7331458147059395 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:30.743 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7331458147059395, number of tests: 2, total length: 43
[MASTER] 21:46:35.379 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.99990947039162 - fitness:1.730773516209401 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:35.379 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.730773516209401, number of tests: 2, total length: 43
[MASTER] 21:46:42.048 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.999913995155637 - fitness:1.7307733020179341 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:42.048 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7307733020179341, number of tests: 2, total length: 43
[MASTER] 21:46:43.415 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.999959415284817 - fitness:1.7307711519421791 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:46:43.415 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7307711519421791, number of tests: 3, total length: 97
[MASTER] 21:46:44.031 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.035636659160648 - fitness:1.7290845973509463 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:44.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7290845973509463, number of tests: 2, total length: 43
[MASTER] 21:46:59.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.035677903971383 - fitness:1.7290826502780956 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:46:59.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7290826502780956, number of tests: 2, total length: 43
[MASTER] 21:47:15.574 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.035679787680152 - fitness:1.7290825613526752 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:47:15.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7290825613526752, number of tests: 2, total length: 49
[MASTER] 21:47:21.592 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.035679788371944 - fitness:1.7290825613200176 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:47:21.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7290825613200176, number of tests: 2, total length: 42
[MASTER] 21:47:46.827 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.035679788922188 - fitness:1.7290825612940417 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:47:46.827 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7290825612940417, number of tests: 2, total length: 42
[MASTER] 21:48:04.205 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.035679789105533 - fitness:1.6836210611169806 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:48:04.205 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6836210611169806, number of tests: 2, total length: 41
[MASTER] 21:48:35.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.03567978914784 - fitness:1.6414026783251636 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:48:35.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6414026783251636, number of tests: 3, total length: 104
[MASTER] 21:48:38.717 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.035680647047776 - fitness:1.6414026433979114 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:48:38.717 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6414026433979114, number of tests: 2, total length: 51
[MASTER] 21:49:40.911 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.035680648883215 - fitness:1.641402643323186 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:49:40.911 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.641402643323186, number of tests: 2, total length: 41
[MASTER] 21:49:47.117 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.035680700178307 - fitness:1.6414026412348346 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:49:47.117 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6414026412348346, number of tests: 2, total length: 43
[MASTER] 21:49:52.068 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.035681962185734 - fitness:1.6414025898553601 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:49:52.068 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6414025898553601, number of tests: 2, total length: 42
[MASTER] 21:50:01.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.035683211978757 - fitness:1.6414025389731688 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 21:50:01.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6414025389731688, number of tests: 2, total length: 42
[MASTER] 21:50:09.306 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.035683213012177 - fitness:1.6414025389310958 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 21:50:09.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6414025389310958, number of tests: 2, total length: 65
[MASTER] 21:50:45.723 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 251 generations, 249539 statements, best individuals have fitness: 1.6414025389310958
[MASTER] 21:50:45.726 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 14
* Number of covered goals: 14
* Generated 2 tests with total length 42
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                      2 / 0           
	- RMIStoppingCondition
	- MaxTime :                        303 / 300          Finished!
[MASTER] 21:50:47.837 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:50:48.841 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 36 
[MASTER] 21:50:48.944 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 21:50:48.945 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [translate:NullPointerException, NullPointerException,]
[MASTER] 21:50:48.945 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: translate:NullPointerException at 34
[MASTER] 21:50:48.945 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [translate:NullPointerException, NullPointerException,]
[MASTER] 21:50:48.945 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:50:49.666 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 21:50:49.668 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 93%
* Total number of goals: 14
* Number of covered goals: 13
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'LookupTranslator_ESTest' to evosuite-tests
* Done!

* Computation finished
