* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.text.translate.LookupTranslator
* Starting client
* Connecting to master process on port 7465
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.text.translate.LookupTranslator
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 14
[MASTER] 21:45:36.505 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:45:37.157 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 21:45:38.212 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/text/translate/LookupTranslator_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 21:45:38.222 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 21:45:38.457 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/text/translate/LookupTranslator_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 21:45:38.463 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 21:45:38.463 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 37 | Tests with assertion: 1
* Generated 1 tests with total length 8
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          1 / 300         
[MASTER] 21:45:38.789 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:45:38.800 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 21:45:38.808 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 21:45:38.808 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [ArrayIndexOutOfBoundsException]
[MASTER] 21:45:38.808 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: ArrayIndexOutOfBoundsException at 7
[MASTER] 21:45:38.808 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [ArrayIndexOutOfBoundsException]
[MASTER] 21:45:38.871 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
[MASTER] 21:45:38.876 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 14%
* Total number of goals: 14
* Number of covered goals: 2
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'LookupTranslator_ESTest' to evosuite-tests
* Done!

* Computation finished
