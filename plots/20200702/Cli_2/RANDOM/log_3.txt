* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.PosixParser
* Starting client
* Connecting to master process on port 3390
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.PosixParser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 45
[MASTER] 00:59:46.756 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:59:48.247 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:59:49.382 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/PosixParser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:59:49.392 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:59:49.700 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/PosixParser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:59:49.710 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:59:49.711 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 81 | Tests with assertion: 1
* Generated 1 tests with total length 22
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                          2 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 00:59:50.169 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:59:50.181 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 00:59:50.203 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 00:59:50.204 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnrecognizedOptionException,UnrecognizedOptionException, parse:UnrecognizedOptionException]
[MASTER] 00:59:50.204 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:UnrecognizedOptionException at 12
[MASTER] 00:59:50.204 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnrecognizedOptionException,UnrecognizedOptionException, parse:UnrecognizedOptionException]
[MASTER] 00:59:50.393 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 13 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 00:59:50.400 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 33%
* Total number of goals: 45
* Number of covered goals: 15
* Generated 1 tests with total length 13
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'PosixParser_ESTest' to evosuite-tests
* Done!

* Computation finished
