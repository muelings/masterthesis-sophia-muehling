* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.PosixParser
* Starting client
* Connecting to master process on port 12344
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.PosixParser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 00:49:13.514 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 45
[MASTER] 00:49:15.034 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:49:16.131 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/PosixParser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:49:16.141 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:49:16.498 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/PosixParser_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 00:49:16.506 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
*=*=*=* Total tests: 141 | Tests with assertion: 1
[MASTER] 00:49:16.506 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 10
* GA-Budget:
	- MaxTime :                          3 / 300         
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 00:49:16.980 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:49:16.992 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 00:49:17.000 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 00:49:17.000 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnrecognizedOptionException,UnrecognizedOptionException, parse:UnrecognizedOptionException]
[MASTER] 00:49:17.000 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:UnrecognizedOptionException at 6
[MASTER] 00:49:17.000 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnrecognizedOptionException,UnrecognizedOptionException, parse:UnrecognizedOptionException]
[MASTER] 00:49:17.085 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
* Going to analyze the coverage criteria
[MASTER] 00:49:17.092 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 33%
* Total number of goals: 45
* Number of covered goals: 15
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'PosixParser_ESTest' to evosuite-tests
* Done!

* Computation finished
