/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 02 23:28:48 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.util.Properties;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class PosixParser_ESTest extends PosixParser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      PosixParser posixParser0 = new PosixParser();
      Options options0 = new Options();
      String[] stringArray0 = new String[2];
      stringArray0[0] = "-opt contains illegal character vaMue '";
      stringArray0[1] = "Y0";
      Properties properties0 = new Properties();
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.apache.commons.cli.UnrecognizedOptionException : Unrecognized option: -opt contains illegal character vaMue '
      try { 
        posixParser0.parse(options0, stringArray0, properties0, false);
        fail("Expecting exception: Exception");
      
      } catch(Exception e) {
         //
         // Unrecognized option: -opt contains illegal character vaMue '
         //
         verifyException("org.apache.commons.cli.Parser", e);
         assertTrue(e.getMessage().equals("Unrecognized option: -opt contains illegal character vaMue '"));   
      }
  }
}
