/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 02 21:55:53 GMT 2020
 */

package org.joda.time;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.joda.time.Period;
import org.joda.time.chrono.GregorianChronology;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Period_ESTest extends Period_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      GregorianChronology gregorianChronology0 = GregorianChronology.getInstanceUTC();
      Period period0 = new Period(627L, 3600000L, gregorianChronology0);
      Period period1 = period0.minusYears((-292269054));
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockArithmeticException : Value cannot fit in an int: 4294967296
      // Undeclared exception!
      try { 
        period1.normalizedStandard();
        fail("Expecting exception: ArithmeticException");
      
      } catch(ArithmeticException e) {
         //
         // Value cannot fit in an int: 4294967296
         //
         verifyException("org.joda.time.field.FieldUtils", e);
         assertTrue(e.getMessage().equals("Value cannot fit in an int: 4294967296"));   
      }
  }
}
