* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Period
* Starting client
* Connecting to master process on port 12727
* Analyzing classpath: 
  - ../defects4j_compiled/Time_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Period
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 23:53:10.233 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 131
[MASTER] 23:53:13.760 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 23:53:15.289 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 23:53:15.298 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 23:53:15.635 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 23:53:15.645 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 23:53:15.645 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 99 | Tests with assertion: 1
* Generated 1 tests with total length 8
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          5 / 300         
[MASTER] 23:53:16.617 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:53:16.632 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 23:53:16.643 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 23:53:16.643 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 23:53:16.643 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: normalizedStandard:UnsupportedOperationException at 6
[MASTER] 23:53:16.643 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 23:53:16.717 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:53:16.721 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 13%
* Total number of goals: 131
* Number of covered goals: 17
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Period_ESTest' to evosuite-tests
* Done!

* Computation finished
