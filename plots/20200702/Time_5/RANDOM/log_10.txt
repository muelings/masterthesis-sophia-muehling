* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Period
* Starting client
* Connecting to master process on port 17505
* Analyzing classpath: 
  - ../defects4j_compiled/Time_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Period
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 23:56:13.894 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 131
[MASTER] 23:56:16.271 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 23:56:17.778 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 23:56:17.819 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 23:56:18.132 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 23:56:18.168 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 23:56:18.169 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 32 | Tests with assertion: 1
* Generated 1 tests with total length 30
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          4 / 300         
[MASTER] 23:56:19.217 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:56:19.239 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 25 
[MASTER] 23:56:19.264 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 23:56:19.264 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 23:56:19.264 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: normalizedStandard:UnsupportedOperationException at 24
[MASTER] 23:56:19.264 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 23:56:19.851 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
[MASTER] 23:56:19.854 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 13%
* Total number of goals: 131
* Number of covered goals: 17
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Period_ESTest' to evosuite-tests
* Done!

* Computation finished
