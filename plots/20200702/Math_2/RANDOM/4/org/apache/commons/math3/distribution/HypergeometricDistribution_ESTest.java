/*
 * This file was automatically generated by EvoSuite
 * Wed Jul 01 18:45:00 GMT 2020
 */

package org.apache.commons.math3.distribution;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.SynchronizedRandomGenerator;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class HypergeometricDistribution_ESTest extends HypergeometricDistribution_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      JDKRandomGenerator jDKRandomGenerator0 = new JDKRandomGenerator();
      SynchronizedRandomGenerator synchronizedRandomGenerator0 = new SynchronizedRandomGenerator(jDKRandomGenerator0);
      HypergeometricDistribution hypergeometricDistribution0 = new HypergeometricDistribution(synchronizedRandomGenerator0, 3342, 237, 237);
      double double0 = hypergeometricDistribution0.getNumericalMean();
      assertEquals(16.807001795332134, double0, 0.01); // (Primitive) Original Value: 16.807001795332134 | Regression Value: 16.807001795332138
  }
}
