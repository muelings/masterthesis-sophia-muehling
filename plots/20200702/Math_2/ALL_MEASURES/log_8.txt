* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 13191
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 21:06:28.220 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 21:06:28.226 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 38
* Using seed 1593630384223
* Starting evolution
[MASTER] 21:06:29.204 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:137.0 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 16
[MASTER] 21:06:29.204 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.0, number of tests: 9, total length: 213
[MASTER] 21:06:29.579 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:119.33333333333334 - branchDistance:0.0 - coverage:30.333333333333336 - ex: 0 - tex: 2
[MASTER] 21:06:29.579 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.33333333333334, number of tests: 2, total length: 42
[MASTER] 21:06:30.368 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:101.0 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 12
[MASTER] 21:06:30.369 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.0, number of tests: 9, total length: 191
[MASTER] 21:06:35.810 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.99780461031833 - branchDistance:0.0 - coverage:8.997804610318331 - ex: 0 - tex: 16
[MASTER] 21:06:35.810 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.99780461031833, number of tests: 9, total length: 183
[MASTER] 21:06:37.245 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.99421965317919 - branchDistance:0.0 - coverage:8.99421965317919 - ex: 0 - tex: 16
[MASTER] 21:06:37.245 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.99421965317919, number of tests: 10, total length: 182
[MASTER] 21:06:37.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:96.99780461031833 - branchDistance:0.0 - coverage:7.997804610318331 - ex: 0 - tex: 12
[MASTER] 21:06:37.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.99780461031833, number of tests: 8, total length: 143
[MASTER] 21:06:39.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:94.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 12
[MASTER] 21:06:39.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.0, number of tests: 9, total length: 144
[MASTER] 21:06:40.289 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 14
[MASTER] 21:06:40.289 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.0, number of tests: 9, total length: 179
[MASTER] 21:06:42.102 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:92.99642218246869 - branchDistance:0.0 - coverage:3.9964221824686943 - ex: 0 - tex: 16
[MASTER] 21:06:42.102 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.99642218246869, number of tests: 11, total length: 214
[MASTER] 21:06:42.375 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.99732977303071 - branchDistance:0.0 - coverage:2.9973297730307076 - ex: 0 - tex: 12
[MASTER] 21:06:42.376 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.99732977303071, number of tests: 10, total length: 202
[MASTER] 21:06:42.780 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 12
[MASTER] 21:06:42.780 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0, number of tests: 9, total length: 158
[MASTER] 21:06:45.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:90.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 21:06:45.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.0, number of tests: 9, total length: 178
[MASTER] 21:06:47.098 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:06:47.098 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.0, number of tests: 9, total length: 196
[MASTER] 21:07:32.681 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:47.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 14
[MASTER] 21:07:32.681 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.0, number of tests: 10, total length: 107
[MASTER] 21:07:33.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:45.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:07:33.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.0, number of tests: 10, total length: 106
[MASTER] 21:07:37.368 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:30.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 21:07:37.368 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.333333333333332, number of tests: 11, total length: 138
[MASTER] 21:07:49.138 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:27.995967741935484 - branchDistance:0.0 - coverage:4.995967741935484 - ex: 0 - tex: 14
[MASTER] 21:07:49.139 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.995967741935484, number of tests: 10, total length: 92
[MASTER] 21:07:51.011 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:23.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:07:51.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.0, number of tests: 10, total length: 91
[MASTER] 21:08:27.334 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:18.6 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:08:27.334 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.6, number of tests: 10, total length: 77
[MASTER] 21:08:38.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:15.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:08:38.600 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.666666666666666, number of tests: 10, total length: 80
[MASTER] 21:08:42.451 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:13.571428571428571 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:08:42.451 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.571428571428571, number of tests: 10, total length: 80
[MASTER] 21:08:44.246 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:12.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:08:44.246 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.0, number of tests: 10, total length: 80
[MASTER] 21:08:51.369 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:10.777777777777777 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:08:51.369 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.777777777777777, number of tests: 10, total length: 72
[MASTER] 21:08:57.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.0 - fitness:9.8 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 21:08:57.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.8, number of tests: 10, total length: 66
[MASTER] 21:09:05.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.0 - fitness:9.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:09:05.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.0, number of tests: 10, total length: 65
[MASTER] 21:09:14.046 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.0 - fitness:8.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:09:14.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333333333333332, number of tests: 10, total length: 65
[MASTER] 21:09:34.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.0 - fitness:7.76923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:09:34.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.76923076923077, number of tests: 10, total length: 64
[MASTER] 21:09:42.552 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.0 - fitness:7.285714285714286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:09:42.552 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.285714285714286, number of tests: 10, total length: 66
[MASTER] 21:09:47.927 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.0 - fitness:6.866666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 21:09:47.927 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.866666666666666, number of tests: 10, total length: 67
[MASTER] 21:09:49.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.0 - fitness:6.5 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 21:09:49.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5, number of tests: 10, total length: 68
[MASTER] 21:09:53.498 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.0 - fitness:6.176470588235294 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 21:09:53.498 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.176470588235294, number of tests: 10, total length: 70
[MASTER] 21:10:01.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.0 - fitness:5.888888888888888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:01.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888888888888888, number of tests: 9, total length: 73
[MASTER] 21:10:06.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.0 - fitness:5.631578947368421 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:06.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.631578947368421, number of tests: 9, total length: 68
[MASTER] 21:10:09.340 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.0 - fitness:5.4 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:09.340 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.4, number of tests: 9, total length: 69
[MASTER] 21:10:28.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.0 - fitness:5.19047619047619 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:28.754 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.19047619047619, number of tests: 9, total length: 66
[MASTER] 21:10:32.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.0 - fitness:5.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:32.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.0, number of tests: 9, total length: 68
[MASTER] 21:10:36.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.0 - fitness:4.826086956521739 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:36.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.826086956521739, number of tests: 9, total length: 69
[MASTER] 21:10:40.641 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.0 - fitness:4.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:40.641 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.666666666666666, number of tests: 9, total length: 75
[MASTER] 21:10:54.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.0 - fitness:4.52 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:10:54.851 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.52, number of tests: 9, total length: 72
[MASTER] 21:11:00.664 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.0 - fitness:4.384615384615385 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 21:11:00.664 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.384615384615385, number of tests: 9, total length: 77
[MASTER] 21:11:01.763 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.0 - fitness:4.2592592592592595 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:11:01.763 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.2592592592592595, number of tests: 9, total length: 76
[MASTER] 21:11:15.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.0 - fitness:4.142857142857142 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:11:15.600 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.142857142857142, number of tests: 9, total length: 75
[MASTER] 21:11:24.987 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.0 - fitness:4.0344827586206895 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 21:11:24.987 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0344827586206895, number of tests: 9, total length: 75
[MASTER] 21:11:29.248 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1044 generations, 451533 statements, best individuals have fitness: 4.0344827586206895
[MASTER] 21:11:29.251 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 38
* Number of covered goals: 38
* Generated 9 tests with total length 74
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                      4 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 21:11:30.210 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:11:30.242 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 53 
[MASTER] 21:11:30.322 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 21:11:30.323 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 21:11:30.324 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 21:11:30.325 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 21:11:30.326 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 21:11:30.326 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 21:11:30.326 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 21:11:30.327 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:11:30.327 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 21:11:30.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 21:11:30.329 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 21:11:30.330 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 21:11:30.331 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 21:11:30.331 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 21:11:30.332 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 21:11:30.332 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 21:11:30.333 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
