* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 7228
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 28
[MASTER] 15:45:46.673 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:45:47.665 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 15:45:48.785 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:45:49.670 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("k&comma;sHJNb&sol;&Hat;g", var4);  // (Primitive) Original Value: k&comma;sHJNb&sol;&Hat;g | Regression Value: k&comma;sHJNb&sol;&hat;g
[MASTER] 15:45:49.673 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:45:49.673 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 15:45:50.022 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:45:50.034 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("k&comma;sHJNb&sol;&Hat;g", var4);  // (Primitive) Original Value: k&comma;sHJNb&sol;&Hat;g | Regression Value: k&comma;sHJNb&sol;&hat;g
[MASTER] 15:45:50.036 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:45:50.036 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 15:45:50.344 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:45:50.363 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("k&comma;sHJNb&sol;&Hat;g", var4);  // (Primitive) Original Value: k&comma;sHJNb&sol;&Hat;g | Regression Value: k&comma;sHJNb&sol;&hat;g
[MASTER] 15:45:50.365 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:45:50.365 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 15:45:50.366 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 138 | Tests with assertion: 1
* Generated 1 tests with total length 18
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          3 / 300         
[MASTER] 15:45:51.044 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:45:51.057 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 15:45:51.069 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 15:45:51.251 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
[MASTER] 15:45:51.254 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 29%
* Total number of goals: 28
* Number of covered goals: 8
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
