/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 02 14:17:45 GMT 2020
 */

package org.jsoup.nodes;

import org.junit.Test;
import static org.junit.Assert.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jsoup.nodes.Entities;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Entities_ESTest extends Entities_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Charset charset0 = Charset.defaultCharset();
      CharsetEncoder charsetEncoder0 = charset0.newEncoder();
      Entities.EscapeMode entities_EscapeMode0 = Entities.EscapeMode.extended;
      String string0 = Entities.escape("bf/|(^@X ", charsetEncoder0, entities_EscapeMode0);
      assertEquals("bf&sol;&vert;&lpar;&Hat;&commat;X ", string0); // (Primitive) Original Value: bf&sol;&vert;&lpar;&Hat;&commat;X  | Regression Value: bf&sol;&vert;&lpar;&hat;&commat;X 
  }
}
