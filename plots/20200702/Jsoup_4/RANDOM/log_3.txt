* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 21138
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 28
[MASTER] 15:56:25.340 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:56:28.083 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&dollar;rH&Hat;&quest;f", var5);  // (Primitive) Original Value: &dollar;rH&Hat;&quest;f | Regression Value: &dollar;rH&hat;&quest;f
[MASTER] 15:56:28.089 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:56:28.089 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 15:56:29.374 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:56:29.388 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&dollar;rH&Hat;&quest;f", var5);  // (Primitive) Original Value: &dollar;rH&Hat;&quest;f | Regression Value: &dollar;rH&hat;&quest;f
[MASTER] 15:56:29.390 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:56:29.390 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 15:56:29.761 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&bsol;jrOC8&vert;2&Hat;&amp;&apos;", var10);  // (Primitive) Original Value: &bsol;jrOC8&vert;2&Hat;&amp;&apos; | Regression Value: &bsol;jrOC8&vert;2&hat;&amp;&apos;
[MASTER] 15:56:29.763 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:56:29.763 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 15:56:30.146 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:56:30.157 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&bsol;jrOC8&vert;2&Hat;&amp;&apos;", var10);  // (Primitive) Original Value: &bsol;jrOC8&vert;2&Hat;&amp;&apos; | Regression Value: &bsol;jrOC8&vert;2&hat;&amp;&apos;
[MASTER] 15:56:30.158 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:56:30.158 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 15:56:30.522 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:56:30.535 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&bsol;jrOC8&vert;2&Hat;&amp;&apos;", var10);  // (Primitive) Original Value: &bsol;jrOC8&vert;2&Hat;&amp;&apos; | Regression Value: &bsol;jrOC8&vert;2&hat;&amp;&apos;
[MASTER] 15:56:30.536 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:56:30.536 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 15:56:30.537 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 312 | Tests with assertion: 1
* Generated 1 tests with total length 12
* GA-Budget:
	- MaxTime :                          5 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 15:56:31.196 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:56:31.221 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 10 
[MASTER] 15:56:31.236 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 15:56:31.239 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@6b9fabad] invalid assertion(s) to be removed
[MASTER] 15:56:31.240 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 15:56:31.241 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 15:56:31.285 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
