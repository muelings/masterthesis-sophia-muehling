* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 14453
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 28
[MASTER] 16:12:19.105 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 16:12:23.445 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("Uy&dollar;&colon;w9M&rcub;~WMz8&Hat;", var8);  // (Primitive) Original Value: Uy&dollar;&colon;w9M&rcub;~WMz8&Hat; | Regression Value: Uy&dollar;&colon;w9M&rcub;~WMz8&hat;
[MASTER] 16:12:23.454 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:12:23.454 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 16:12:24.624 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 16:12:24.635 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("Uy&dollar;&colon;w9M&rcub;~WMz8&Hat;", var8);  // (Primitive) Original Value: Uy&dollar;&colon;w9M&rcub;~WMz8&Hat; | Regression Value: Uy&dollar;&colon;w9M&rcub;~WMz8&hat;
[MASTER] 16:12:24.637 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:12:24.637 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 16:12:24.923 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 16:12:24.933 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("Uy&dollar;&colon;w9M&rcub;~WMz8&Hat;", var8);  // (Primitive) Original Value: Uy&dollar;&colon;w9M&rcub;~WMz8&Hat; | Regression Value: Uy&dollar;&colon;w9M&rcub;~WMz8&hat;
[MASTER] 16:12:24.934 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Keeping 1 assertions.
[MASTER] 16:12:24.934 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
*=*=*=* Total tests: 497 | Tests with assertion: 1
* Generated 1 tests with total length 11
[MASTER] 16:12:24.935 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                          5 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 16:12:25.628 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 16:12:25.652 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 16:12:25.665 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 16:12:25.665 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 16:12:25.781 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 16:12:25.786 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 29%
* Total number of goals: 28
* Number of covered goals: 8
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
