/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 02 14:07:07 GMT 2020
 */

package org.jsoup.nodes;

import org.junit.Test;
import static org.junit.Assert.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Entities_ESTest extends Entities_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Document document0 = Document.createShell("R^,Hm^X");
      Document.OutputSettings document_OutputSettings0 = document0.new OutputSettings();
      Entities.EscapeMode entities_EscapeMode0 = Entities.EscapeMode.extended;
      Document.OutputSettings document_OutputSettings1 = document_OutputSettings0.escapeMode(entities_EscapeMode0);
      String string0 = Entities.escape("R^,Hm^X", document_OutputSettings1);
      assertEquals("R&Hat;&comma;Hm&Hat;X", string0); // (Primitive) Original Value: R&Hat;&comma;Hm&Hat;X | Regression Value: R&hat;&comma;Hm&hat;X
  }
}
