* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 8738
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 16:07:12.504 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 16:07:12.509 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 28
* Using seed 1593698829795
* Starting evolution
[MASTER] 16:07:13.471 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.98058252427185 - branchDistance:0.0 - coverage:36.980582524271846 - ex: 0 - tex: 12
[MASTER] 16:07:13.471 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.98058252427185, number of tests: 6, total length: 108
[MASTER] 16:07:13.655 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:76.0 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 16
[MASTER] 16:07:13.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.0, number of tests: 10, total length: 240
[MASTER] 16:07:14.061 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:62.0 - branchDistance:0.0 - coverage:37.0 - ex: 0 - tex: 4
[MASTER] 16:07:14.061 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 2, total length: 41
[MASTER] 16:07:14.604 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:58.0 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 8
[MASTER] 16:07:14.604 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.0, number of tests: 6, total length: 69
[MASTER] 16:07:14.701 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:57.0 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 14
[MASTER] 16:07:14.701 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.0, number of tests: 9, total length: 115
[MASTER] 16:07:14.776 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:56.0 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 16
[MASTER] 16:07:14.776 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.0, number of tests: 9, total length: 171
[MASTER] 16:07:15.400 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:55.0 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 14
[MASTER] 16:07:15.400 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.0, number of tests: 10, total length: 129
[MASTER] 16:07:15.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:53.0 - branchDistance:0.0 - coverage:28.0 - ex: 0 - tex: 10
[MASTER] 16:07:15.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.0, number of tests: 5, total length: 137
[MASTER] 16:07:15.967 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:52.0 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 12
[MASTER] 16:07:15.967 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.0, number of tests: 7, total length: 143
[MASTER] 16:07:16.359 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:40.0 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 12
[MASTER] 16:07:16.359 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.0, number of tests: 9, total length: 212
[MASTER] 16:07:16.846 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:36.999969481956185 - branchDistance:0.0 - coverage:11.999969481956185 - ex: 0 - tex: 8
[MASTER] 16:07:16.846 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.999969481956185, number of tests: 9, total length: 187
[MASTER] 16:07:17.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:32.99996948335317 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 12
[MASTER] 16:07:17.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99996948335317, number of tests: 10, total length: 213
[MASTER] 16:07:18.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.5 - fitness:29.333333333333332 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 14
[MASTER] 16:07:18.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.333333333333332, number of tests: 9, total length: 238
[MASTER] 16:07:18.826 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.5 - fitness:26.333302815289517 - branchDistance:0.0 - coverage:11.999969481956185 - ex: 0 - tex: 4
[MASTER] 16:07:18.826 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.333302815289517, number of tests: 8, total length: 126
[MASTER] 16:07:19.424 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.5 - fitness:22.333333333333332 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 12
[MASTER] 16:07:19.424 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.333333333333332, number of tests: 9, total length: 212
[MASTER] 16:07:20.393 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.5 - fitness:22.3333028166865 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 12
[MASTER] 16:07:20.393 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.3333028166865, number of tests: 10, total length: 177
[MASTER] 16:07:28.278 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:19.90906039244408 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 16:07:28.278 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.90906039244408, number of tests: 7, total length: 59
[MASTER] 16:07:28.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:16.199969483353172 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 16:07:28.749 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.199969483353172, number of tests: 7, total length: 66
[MASTER] 16:07:30.657 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:15.428540911924596 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 16:07:30.657 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.428540911924596, number of tests: 7, total length: 66
[MASTER] 16:07:31.074 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.166666666666666 - fitness:14.373103811711378 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 16:07:31.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.373103811711378, number of tests: 7, total length: 78
[MASTER] 16:07:32.882 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.166666666666666 - fitness:13.931476332668238 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 16:07:32.882 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.931476332668238, number of tests: 6, total length: 66
[MASTER] 16:07:38.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.15635738831615 - fitness:13.23834792733909 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:38.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.23834792733909, number of tests: 4, total length: 41
[MASTER] 16:07:39.031 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.146048109965635 - fitness:12.71604908322547 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:39.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.71604908322547, number of tests: 4, total length: 43
[MASTER] 16:07:40.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.146048109965637 - fitness:12.499318115480637 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:40.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.499318115480637, number of tests: 4, total length: 45
[MASTER] 16:07:41.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.165675095025616 - fitness:12.49531701077941 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:41.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.49531701077941, number of tests: 4, total length: 45
[MASTER] 16:07:41.802 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.166546827251484 - fitness:12.495139513982243 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:41.802 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.495139513982243, number of tests: 4, total length: 46
[MASTER] 16:07:44.070 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.146048109965637 - fitness:12.306474549161331 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:44.070 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.306474549161331, number of tests: 4, total length: 44
[MASTER] 16:07:45.750 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.16147186147186 - fitness:12.303666477608646 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:45.750 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.303666477608646, number of tests: 4, total length: 42
[MASTER] 16:07:45.989 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.146048109965637 - fitness:12.133775460199619 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 16:07:45.990 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.133775460199619, number of tests: 5, total length: 90
[MASTER] 16:07:47.813 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.165488116283637 - fitness:12.130627283581784 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 2
[MASTER] 16:07:47.814 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.130627283581784, number of tests: 3, total length: 40
[MASTER] 16:07:48.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.165980089255065 - fitness:12.130516406629873 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:48.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.130516406629873, number of tests: 3, total length: 43
[MASTER] 16:07:48.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.166649225769508 - fitness:12.130407114534428 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:48.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.130407114534428, number of tests: 4, total length: 43
[MASTER] 16:07:50.004 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.14896755162242 - fitness:11.977820071737062 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 2
[MASTER] 16:07:50.004 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.977820071737062, number of tests: 3, total length: 44
[MASTER] 16:07:51.020 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.13573883161512 - fitness:11.838763077369915 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 16:07:51.020 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.838763077369915, number of tests: 3, total length: 47
[MASTER] 16:07:51.858 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.13573883161512 - fitness:11.485940058375453 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 2
[MASTER] 16:07:51.858 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.485940058375453, number of tests: 3, total length: 44
[MASTER] 16:07:52.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.13573883161512 - fitness:11.485909541728622 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:52.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.485909541728622, number of tests: 3, total length: 45
[MASTER] 16:07:53.585 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.166663857722614 - fitness:11.482728392619274 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:53.585 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.482728392619274, number of tests: 3, total length: 46
[MASTER] 16:07:54.040 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.166663955015515 - fitness:11.4827283826239 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:54.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.4827283826239, number of tests: 4, total length: 54
[MASTER] 16:07:58.218 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.13573883161512 - fitness:11.38700892555701 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:58.218 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.38700892555701, number of tests: 3, total length: 50
[MASTER] 16:07:58.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.166441051866336 - fitness:11.384096816905977 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:07:58.449 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.384096816905977, number of tests: 3, total length: 50
[MASTER] 16:08:00.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.16666378178776 - fitness:11.384075716910433 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:00.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.384075716910433, number of tests: 3, total length: 49
[MASTER] 16:08:06.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.166663819678067 - fitness:11.38407571332098 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:06.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.38407571332098, number of tests: 3, total length: 48
[MASTER] 16:08:07.759 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.166666663880445 - fitness:11.384075443882017 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:07.759 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.384075443882017, number of tests: 4, total length: 52
[MASTER] 16:08:16.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.166666663880445 - fitness:9.717408777215349 - branchDistance:0.0 - coverage:6.333302816686502 - ex: 0 - tex: 2
[MASTER] 16:08:16.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.717408777215349, number of tests: 2, total length: 47
[MASTER] 16:08:16.979 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.166666663880445 - fitness:7.384075443882017 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 4
[MASTER] 16:08:16.979 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.384075443882017, number of tests: 3, total length: 59
[MASTER] 16:08:27.527 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.161319073083778 - fitness:7.2934318211039955 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:27.527 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.2934318211039955, number of tests: 3, total length: 49
[MASTER] 16:08:29.888 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.166663737622756 - fitness:7.29296337059953 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:29.888 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.29296337059953, number of tests: 3, total length: 48
[MASTER] 16:08:37.086 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.166663820715176 - fitness:7.292963363318113 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:37.086 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.292963363318113, number of tests: 3, total length: 48
[MASTER] 16:08:37.150 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.166663872332457 - fitness:7.292963358794872 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:37.150 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.292963358794872, number of tests: 3, total length: 47
[MASTER] 16:08:37.236 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.166663998541424 - fitness:7.292963347735137 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:37.236 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.292963347735137, number of tests: 3, total length: 46
[MASTER] 16:08:45.501 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.16666593535907 - fitness:7.292963178011146 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:45.501 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.292963178011146, number of tests: 3, total length: 46
[MASTER] 16:08:52.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.494152046783626 - fitness:7.182251733698804 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:08:52.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.182251733698804, number of tests: 3, total length: 46
[MASTER] 16:08:55.522 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.494152046783626 - fitness:7.105664711675385 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:08:55.522 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.105664711675385, number of tests: 3, total length: 47
[MASTER] 16:08:59.080 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.499999104417995 - fitness:7.10523270740355 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:08:59.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.10523270740355, number of tests: 3, total length: 45
[MASTER] 16:08:59.119 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.499983432096446 - fitness:7.033868930724352 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:08:59.119 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.033868930724352, number of tests: 4, total length: 59
[MASTER] 16:08:59.219 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.49999856745633 - fitness:7.033867887205654 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 4
[MASTER] 16:08:59.219 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.033867887205654, number of tests: 4, total length: 59
[MASTER] 16:09:06.957 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.499999444187637 - fitness:7.033867826758791 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:06.958 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.033867826758791, number of tests: 3, total length: 45
[MASTER] 16:09:08.654 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.49999776892122 - fitness:6.967182742009161 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:09:08.654 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.967182742009161, number of tests: 4, total length: 82
[MASTER] 16:09:13.991 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.499999444187637 - fitness:6.967182633956494 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:13.992 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.967182633956494, number of tests: 3, total length: 45
[MASTER] 16:09:15.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.494152046783626 - fitness:6.9050850715065435 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:15.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.9050850715065435, number of tests: 3, total length: 47
[MASTER] 16:09:18.748 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.49610118183641 - fitness:6.904967173499072 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:09:18.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.904967173499072, number of tests: 4, total length: 68
[MASTER] 16:09:20.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.49999930233353 - fitness:6.904731430302012 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:20.551 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.904731430302012, number of tests: 3, total length: 46
[MASTER] 16:09:28.239 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.56299623001089 - fitness:6.900929695202464 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:28.239 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.900929695202464, number of tests: 3, total length: 46
[MASTER] 16:09:29.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.566666110854307 - fitness:6.900708693166929 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:29.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.900708693166929, number of tests: 3, total length: 45
[MASTER] 16:09:30.160 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.57565660810323 - fitness:6.900167498066892 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:30.160 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.900167498066892, number of tests: 3, total length: 45
[MASTER] 16:09:33.798 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.611162079510706 - fitness:6.898033206827922 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 4
[MASTER] 16:09:33.798 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.898033206827922, number of tests: 4, total length: 75
[MASTER] 16:09:34.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.616665600447718 - fitness:6.897702810397953 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:34.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.897702810397953, number of tests: 3, total length: 45
[MASTER] 16:09:36.056 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.61666564017532 - fitness:6.897702808013374 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:36.056 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.897702808013374, number of tests: 3, total length: 45
[MASTER] 16:09:45.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.611162079510706 - fitness:6.8398303216057705 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:45.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.8398303216057705, number of tests: 3, total length: 45
[MASTER] 16:09:50.226 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.612767341207004 - fitness:6.83973976019774 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:50.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.83973976019774, number of tests: 3, total length: 45
[MASTER] 16:09:50.813 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.612873439364222 - fitness:6.839733774946657 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:50.813 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.839733774946657, number of tests: 3, total length: 50
[MASTER] 16:09:50.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.615375583130323 - fitness:6.839592634313719 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:50.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.839592634313719, number of tests: 3, total length: 45
[MASTER] 16:09:51.610 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.616331736914084 - fitness:6.839538705421572 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:51.610 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.839538705421572, number of tests: 3, total length: 45
[MASTER] 16:09:52.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.61299590147894 - fitness:6.784993352569185 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:52.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.784993352569185, number of tests: 3, total length: 45
[MASTER] 16:09:55.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.61645729237662 - fitness:6.7848095537435285 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:09:55.008 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.7848095537435285, number of tests: 3, total length: 48
[MASTER] 16:09:55.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.616656843258795 - fitness:6.784798958805066 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:09:55.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.784798958805066, number of tests: 3, total length: 50
[MASTER] 16:09:55.386 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.61666481338825 - fitness:6.784798535642267 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:09:55.386 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.784798535642267, number of tests: 4, total length: 48
[MASTER] 16:09:59.073 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.61666587848084 - fitness:6.784798479092691 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:09:59.073 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.784798479092691, number of tests: 3, total length: 45
[MASTER] 16:10:11.031 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.73237420072282 - fitness:6.5901159098892075 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:10:11.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5901159098892075, number of tests: 3, total length: 47
[MASTER] 16:10:14.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.737876495003746 - fitness:6.5898840618256385 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:10:14.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5898840618256385, number of tests: 3, total length: 47
[MASTER] 16:10:14.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.7378778603626 - fitness:6.5898840043024265 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:10:14.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5898840043024265, number of tests: 3, total length: 47
[MASTER] 16:10:31.584 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.73787683194944 - fitness:6.548841153844785 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:10:31.584 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.548841153844785, number of tests: 3, total length: 48
[MASTER] 16:10:33.321 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.7378777013069 - fitness:6.548841119084927 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:10:33.321 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.548841119084927, number of tests: 3, total length: 53
[MASTER] 16:10:35.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.73787780380794 - fitness:6.548841114986588 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:10:35.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.548841114986588, number of tests: 3, total length: 54
[MASTER] 16:10:36.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.73787808765115 - fitness:6.548841103637574 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 2
[MASTER] 16:10:36.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.548841103637574, number of tests: 3, total length: 58
[MASTER] 16:10:41.234 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.73787809235515 - fitness:6.548841103449492 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:10:41.235 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.548841103449492, number of tests: 3, total length: 47
[MASTER] 16:11:08.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.73787822546607 - fitness:6.548841098127268 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:08.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.548841098127268, number of tests: 3, total length: 47
[MASTER] 16:11:14.115 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.76156069646587 - fitness:6.5478947712394895 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:14.115 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5478947712394895, number of tests: 3, total length: 47
[MASTER] 16:11:17.477 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.758724777023836 - fitness:6.509072210779513 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:17.477 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.509072210779513, number of tests: 3, total length: 49
[MASTER] 16:11:20.182 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.76156137336003 - fitness:6.508964551142423 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:20.182 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.508964551142423, number of tests: 3, total length: 51
[MASTER] 16:11:21.567 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.76156215472945 - fitness:6.508964521488593 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:21.567 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.508964521488593, number of tests: 3, total length: 51
[MASTER] 16:11:25.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.761562269026285 - fitness:6.508964517150902 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:25.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.508964517150902, number of tests: 3, total length: 49
[MASTER] 16:11:31.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.76156229361079 - fitness:6.508964516217894 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:31.549 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.508964516217894, number of tests: 3, total length: 57
[MASTER] 16:11:46.064 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.7615623175749 - fitness:6.5089645153084295 - branchDistance:0.0 - coverage:3.999969483353169 - ex: 0 - tex: 0
[MASTER] 16:11:46.064 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5089645153084295, number of tests: 3, total length: 49
[MASTER] 16:12:13.572 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1043 generations, 690274 statements, best individuals have fitness: 6.5089645153084295
[MASTER] 16:12:13.577 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 89%
* Total number of goals: 28
* Number of covered goals: 25
* Generated 3 tests with total length 49
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                      7 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 16:12:13.910 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 16:12:13.946 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 39 
[MASTER] 16:12:14.036 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 16:12:14.036 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 16:12:14.036 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 16:12:14.036 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 16:12:14.037 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
