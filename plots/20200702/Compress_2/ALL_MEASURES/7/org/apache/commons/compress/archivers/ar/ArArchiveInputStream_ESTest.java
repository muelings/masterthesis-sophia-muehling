/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 02 00:46:19 GMT 2020
 */

package org.apache.commons.compress.archivers.ar;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.PushbackInputStream;
import org.apache.commons.compress.archivers.ar.ArArchiveInputStream;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class ArArchiveInputStream_ESTest extends ArArchiveInputStream_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      byte[] byteArray0 = new byte[1];
      ByteArrayInputStream byteArrayInputStream0 = new ByteArrayInputStream(byteArray0, (-3522), (byte)0);
      DataInputStream dataInputStream0 = new DataInputStream(byteArrayInputStream0);
      PushbackInputStream pushbackInputStream0 = new PushbackInputStream(dataInputStream0, 255);
      ArArchiveInputStream arArchiveInputStream0 = new ArArchiveInputStream(pushbackInputStream0);
      pushbackInputStream0.unread(0);
      arArchiveInputStream0.read();
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.io.MockIOException : failed to read header
      arArchiveInputStream0.getNextEntry();
  }
}
