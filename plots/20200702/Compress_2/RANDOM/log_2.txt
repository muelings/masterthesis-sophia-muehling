* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting client
* Connecting to master process on port 3665
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 59
[MASTER] 01:57:32.861 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:57:49.835 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:58:06.128 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:58:22.410 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:58:39.221 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:58:56.687 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:59:12.789 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:59:28.820 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:59:44.926 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:59:44.947 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 01:59:46.019 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 01:59:46.244 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 299 | Tests with assertion: 1
* Generated 1 tests with total length 11
* GA-Budget:
[MASTER] 01:59:46.245 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        133 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 01:59:46.811 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:59:46.812 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 16 seconds more than allowed.
[MASTER] 01:59:46.824 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 01:59:46.831 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 01:59:46.831 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 01:59:46.831 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: read:ArrayIndexOutOfBoundsException at 6
[MASTER] 01:59:46.831 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 01:59:46.889 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 01:59:46.893 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 59
* Number of covered goals: 2
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
