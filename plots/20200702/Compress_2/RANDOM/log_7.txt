* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting client
* Connecting to master process on port 19071
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 02:39:36.596 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 59
[MASTER] 02:39:53.925 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 02:40:11.237 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 02:40:27.466 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 02:40:43.605 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 02:40:59.729 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 02:40:59.760 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:41:01.045 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/ar/ArArchiveInputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 1 assertions.
[MASTER] 02:41:01.058 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:41:01.359 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/ar/ArArchiveInputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 02:41:01.370 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 02:41:01.371 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 287 | Tests with assertion: 1
* Generated 1 tests with total length 20
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                         84 / 300         
[MASTER] 02:41:01.927 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:41:01.941 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 02:41:01.951 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 02:41:01.951 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 02:41:01.951 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: read:ArrayIndexOutOfBoundsException at 10
[MASTER] 02:41:01.951 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 02:41:02.037 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:41:02.041 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 59
* Number of covered goals: 2
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
