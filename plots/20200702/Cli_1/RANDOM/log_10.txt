* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.CommandLine
* Starting client
* Connecting to master process on port 8716
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_1_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.CommandLine
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 32
[MASTER] 00:43:57.656 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:43:58.030 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:43:59.244 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:43:59.276 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:43:59.650 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 00:43:59.680 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*=*=*=* Total tests: 5 | Tests with assertion: 1
[MASTER] 00:43:59.680 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 24
* GA-Budget:
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 00:44:00.060 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:44:00.080 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 15 
[MASTER] 00:44:00.102 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 00:44:00.102 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException:resolveOption-165,, hasOption:NullPointerException]
[MASTER] 00:44:00.102 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: hasOption:NullPointerException at 14
[MASTER] 00:44:00.102 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException:resolveOption-165,, hasOption:NullPointerException]
[MASTER] 00:44:00.406 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 00:44:00.410 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 32
* Number of covered goals: 1
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CommandLine_ESTest' to evosuite-tests
* Done!

* Computation finished
