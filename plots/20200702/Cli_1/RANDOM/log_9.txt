* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.CommandLine
* Starting client
* Connecting to master process on port 3793
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_1_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.CommandLine
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 32
[MASTER] 00:38:42.931 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:38:43.519 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:38:44.549 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:38:44.565 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:38:44.892 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:38:44.905 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:38:44.906 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 12 | Tests with assertion: 1
* Generated 1 tests with total length 14
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                          1 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 00:38:45.286 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:38:45.305 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 00:38:45.325 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 00:38:45.326 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [addOption:NullPointerException]
[MASTER] 00:38:45.326 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: addOption:NullPointerException at 6
[MASTER] 00:38:45.326 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [addOption:NullPointerException]
[MASTER] 00:38:45.442 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 00:38:45.446 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 32
* Number of covered goals: 2
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CommandLine_ESTest' to evosuite-tests
* Done!

* Computation finished
