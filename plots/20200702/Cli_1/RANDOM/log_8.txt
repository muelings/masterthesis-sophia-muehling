* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.CommandLine
* Starting client
* Connecting to master process on port 6705
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_1_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.CommandLine
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 32
[MASTER] 00:33:27.940 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:33:28.422 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:33:29.516 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:33:29.525 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:33:29.815 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 00:33:29.828 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 00:33:29.829 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 9 | Tests with assertion: 1
* Generated 1 tests with total length 6
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          1 / 300         
	- RMIStoppingCondition
[MASTER] 00:33:30.252 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:33:30.266 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 3 
[MASTER] 00:33:30.274 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 00:33:30.275 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 00:33:30.276 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: hasOption:NullPointerException at 2
[MASTER] 00:33:30.276 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException:resolveOption-165,, hasOption:NullPointerException]
[MASTER] 00:33:30.324 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 00:33:30.330 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 32
* Number of covered goals: 1
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CommandLine_ESTest' to evosuite-tests
* Done!

* Computation finished
