* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.DoubleMetaphone
* Starting client
* Connecting to master process on port 11127
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.DoubleMetaphone
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 504
[MASTER] 23:51:07.222 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:51:38.080 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 23:53:58.937 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 23:54:59.858 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 23:55:25.975 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 23:55:49.101 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
*** Random test generation finished.
[MASTER] 23:56:16.629 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 26514 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                        309 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 23:56:16.663 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:56:16.663 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 191 seconds more than allowed.
[MASTER] 23:56:16.667 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 23:56:16.667 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:56:16.669 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 504
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'DoubleMetaphone_ESTest' to evosuite-tests
* Done!

* Computation finished
