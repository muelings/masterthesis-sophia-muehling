* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.trie.AbstractPatriciaTrie
* Starting client
* Connecting to master process on port 18812
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_28_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.trie.AbstractPatriciaTrie
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 608
[MASTER] 10:54:39.415 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
