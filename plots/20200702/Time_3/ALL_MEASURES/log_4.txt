* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 5784
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:05:38.889 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:05:38.905 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 151
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593723934641
* Starting evolution
[MASTER] 23:05:41.250 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:615.0 - branchDistance:0.0 - coverage:306.0 - ex: 0 - tex: 4
[MASTER] 23:05:41.250 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 615.0, number of tests: 2, total length: 64
[MASTER] 23:05:41.837 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:607.0 - branchDistance:0.0 - coverage:298.0 - ex: 0 - tex: 12
[MASTER] 23:05:41.837 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 607.0, number of tests: 6, total length: 110
[MASTER] 23:05:41.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.5 - fitness:514.3333333333333 - branchDistance:0.0 - coverage:308.0 - ex: 0 - tex: 8
[MASTER] 23:05:41.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 514.3333333333333, number of tests: 4, total length: 93
[MASTER] 23:05:44.371 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.5 - fitness:463.43333333333334 - branchDistance:0.0 - coverage:257.1 - ex: 0 - tex: 8
[MASTER] 23:05:44.371 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 463.43333333333334, number of tests: 4, total length: 115
[MASTER] 23:05:45.727 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:405.1 - branchDistance:0.0 - coverage:250.10000000000002 - ex: 0 - tex: 10
[MASTER] 23:05:45.727 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 405.1, number of tests: 5, total length: 141
* Computation finished
