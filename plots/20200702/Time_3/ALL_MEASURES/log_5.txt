* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 4952
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:11:00.454 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:11:00.461 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 151
* Using seed 1593724256166
* Starting evolution
[MASTER] 23:11:02.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:568.1 - branchDistance:0.0 - coverage:259.1 - ex: 0 - tex: 12
[MASTER] 23:11:02.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 568.1, number of tests: 8, total length: 143
[MASTER] 23:11:03.210 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:553.4333333333334 - branchDistance:0.0 - coverage:244.43333333333334 - ex: 0 - tex: 18
[MASTER] 23:11:03.210 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 553.4333333333334, number of tests: 10, total length: 245
[MASTER] 23:11:03.610 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:549.1 - branchDistance:0.0 - coverage:240.10000000000002 - ex: 0 - tex: 14
[MASTER] 23:11:03.610 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 549.1, number of tests: 9, total length: 243
[MASTER] 23:11:04.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:542.1 - branchDistance:0.0 - coverage:233.10000000000002 - ex: 0 - tex: 16
[MASTER] 23:11:04.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 542.1, number of tests: 10, total length: 158
* Computation finished
