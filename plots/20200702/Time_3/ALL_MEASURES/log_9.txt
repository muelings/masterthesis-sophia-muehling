* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 20025
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:22:36.659 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:22:36.667 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 151
* Using seed 1593724952127
* Starting evolution
[MASTER] 23:22:38.952 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:576.1 - branchDistance:0.0 - coverage:267.1 - ex: 0 - tex: 10
[MASTER] 23:22:38.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 576.1, number of tests: 5, total length: 94
[MASTER] 23:22:39.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:566.1 - branchDistance:0.0 - coverage:257.1 - ex: 0 - tex: 16
[MASTER] 23:22:39.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 566.1, number of tests: 8, total length: 179
[MASTER] 23:22:39.827 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:554.1 - branchDistance:0.0 - coverage:245.10000000000002 - ex: 0 - tex: 12
[MASTER] 23:22:39.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 554.1, number of tests: 9, total length: 156
[MASTER] 23:22:41.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:546.1 - branchDistance:0.0 - coverage:237.10000000000002 - ex: 0 - tex: 16
[MASTER] 23:22:41.449 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 546.1, number of tests: 10, total length: 134
[MASTER] 23:22:41.843 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:535.1 - branchDistance:0.0 - coverage:226.10000000000002 - ex: 0 - tex: 20
[MASTER] 23:22:41.843 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 535.1, number of tests: 10, total length: 268
* Computation finished
