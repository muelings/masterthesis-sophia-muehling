* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 3201
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 01:30:36.217 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
[MASTER] 01:30:52.670 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:30:52.761 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var0.toString());  // (Inspector) Original Value: 0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: 
[MASTER] 01:30:52.761 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124, var0.size());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 01:30:52.763 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 01:30:52.763 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 01:30:53.737 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 01:30:53.757 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var0.toString());  // (Inspector) Original Value: 0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: 
[MASTER] 01:30:53.757 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124, var0.size());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 01:30:53.759 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 01:30:53.759 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 01:30:54.109 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 01:30:54.132 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var0.toString());  // (Inspector) Original Value: 0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: 
[MASTER] 01:30:54.132 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124, var0.size());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 01:30:54.133 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 01:30:54.133 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
[MASTER] 01:30:54.134 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 20 | Tests with assertion: 1
* Generated 1 tests with total length 3
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                         18 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 01:30:54.823 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:30:54.841 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 3 
[MASTER] 01:30:54.855 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[toString, size]}
[MASTER] 01:30:54.857 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.InspectorAssertion@91122110, org.evosuite.assertion.InspectorAssertion@4a049fdf] invalid assertion(s) to be removed
[MASTER] 01:30:54.858 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 01:30:54.858 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 01:30:54.862 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
