* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 14578
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 00:52:51.785 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
[MASTER] 00:52:52.754 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 00:52:53.889 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:52:53.897 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:52:54.240 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 00:52:54.249 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*=*=*=* Total tests: 66 | Tests with assertion: 1
* Generated 1 tests with total length 3
* GA-Budget:
	- RMIStoppingCondition
[MASTER] 00:52:54.249 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 00:52:54.885 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:52:54.896 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 3 
[MASTER] 00:52:54.901 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 00:52:54.901 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IOException:writeHeader-176,, close:IOException]
[MASTER] 00:52:54.901 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:IOException at 2
[MASTER] 00:52:54.901 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [IOException:writeHeader-176,, close:IOException]
[MASTER] 00:52:54.923 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
* Going to analyze the coverage criteria
[MASTER] 00:52:54.925 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 25%
* Total number of goals: 97
* Number of covered goals: 24
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
