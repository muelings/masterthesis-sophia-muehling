* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.map.MultiValueMap
* Starting client
* Connecting to master process on port 5294
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_27_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.map.MultiValueMap
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:05:03.819 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:05:03.828 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 87
* Using seed 1593759900670
* Starting evolution
[MASTER] 09:05:52.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:287.3333333333333 - branchDistance:0.0 - coverage:112.33333333333331 - ex: 0 - tex: 8
[MASTER] 09:05:52.407 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 287.3333333333333, number of tests: 10, total length: 151
[MASTER] 09:05:53.065 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:282.0 - branchDistance:0.0 - coverage:107.0 - ex: 0 - tex: 10
[MASTER] 09:05:53.065 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 282.0, number of tests: 8, total length: 210
[MASTER] 09:05:53.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:280.3333333333333 - branchDistance:0.0 - coverage:105.33333333333331 - ex: 0 - tex: 10
[MASTER] 09:05:53.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 280.3333333333333, number of tests: 9, total length: 172
[MASTER] 09:05:53.725 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.0 - branchDistance:0.0 - coverage:96.0 - ex: 0 - tex: 2
[MASTER] 09:05:53.725 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.0, number of tests: 5, total length: 100
[MASTER] 09:05:53.948 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:247.33333333333334 - branchDistance:0.0 - coverage:72.33333333333334 - ex: 0 - tex: 10
[MASTER] 09:05:53.948 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 247.33333333333334, number of tests: 10, total length: 250
[MASTER] 09:05:57.830 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:246.33333333333334 - branchDistance:0.0 - coverage:71.33333333333334 - ex: 0 - tex: 14
[MASTER] 09:05:57.830 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 246.33333333333334, number of tests: 10, total length: 272
[MASTER] 09:06:01.997 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:239.33333333333331 - branchDistance:0.0 - coverage:64.33333333333333 - ex: 0 - tex: 12
[MASTER] 09:06:01.997 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 239.33333333333331, number of tests: 10, total length: 274
[MASTER] 09:06:13.422 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.66666666666666 - branchDistance:0.0 - coverage:62.66666666666666 - ex: 0 - tex: 10
[MASTER] 09:06:13.423 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.66666666666666, number of tests: 10, total length: 259
[MASTER] 09:06:17.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:234.33333333333331 - branchDistance:0.0 - coverage:59.33333333333333 - ex: 0 - tex: 12
[MASTER] 09:06:17.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 234.33333333333331, number of tests: 10, total length: 285
[MASTER] 09:06:23.960 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.33333333333331 - branchDistance:0.0 - coverage:58.33333333333333 - ex: 0 - tex: 12
[MASTER] 09:06:23.960 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.33333333333331, number of tests: 11, total length: 285
[MASTER] 09:06:29.027 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.33333333333331 - branchDistance:0.0 - coverage:57.33333333333333 - ex: 0 - tex: 10
[MASTER] 09:06:29.027 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.33333333333331, number of tests: 11, total length: 257
[MASTER] 09:06:31.125 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:230.33333333333331 - branchDistance:0.0 - coverage:55.33333333333333 - ex: 0 - tex: 10
[MASTER] 09:06:31.125 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 230.33333333333331, number of tests: 12, total length: 305
[MASTER] 09:06:38.157 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:229.66666666666666 - branchDistance:0.0 - coverage:54.66666666666666 - ex: 0 - tex: 10
[MASTER] 09:06:38.157 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 229.66666666666666, number of tests: 11, total length: 293
[MASTER] 09:06:44.850 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:228.83333333333331 - branchDistance:0.0 - coverage:53.83333333333333 - ex: 0 - tex: 12
[MASTER] 09:06:44.850 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 228.83333333333331, number of tests: 10, total length: 253
[MASTER] 09:06:56.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:228.16666666666666 - branchDistance:0.0 - coverage:53.16666666666666 - ex: 0 - tex: 12
[MASTER] 09:06:56.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 228.16666666666666, number of tests: 10, total length: 285
[MASTER] 09:07:01.835 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:226.16666666666666 - branchDistance:0.0 - coverage:51.16666666666666 - ex: 0 - tex: 12
[MASTER] 09:07:01.835 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 226.16666666666666, number of tests: 11, total length: 280
[MASTER] 09:07:04.030 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.83333333333331 - branchDistance:0.0 - coverage:50.83333333333333 - ex: 0 - tex: 10
[MASTER] 09:07:04.030 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.83333333333331, number of tests: 11, total length: 264
[MASTER] 09:07:05.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.16666666666666 - branchDistance:0.0 - coverage:50.16666666666666 - ex: 0 - tex: 10
[MASTER] 09:07:05.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.16666666666666, number of tests: 11, total length: 278
[MASTER] 09:07:13.641 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:224.83333333333331 - branchDistance:0.0 - coverage:49.83333333333333 - ex: 0 - tex: 12
[MASTER] 09:07:13.641 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 224.83333333333331, number of tests: 11, total length: 275
[MASTER] 09:07:22.110 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:224.16666666666666 - branchDistance:0.0 - coverage:49.16666666666666 - ex: 0 - tex: 12
[MASTER] 09:07:22.110 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 224.16666666666666, number of tests: 11, total length: 280
[MASTER] 09:07:35.532 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.83333333333331 - branchDistance:0.0 - coverage:48.83333333333333 - ex: 0 - tex: 14
[MASTER] 09:07:35.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.83333333333331, number of tests: 12, total length: 280
[MASTER] 09:07:43.240 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.16666666666666 - branchDistance:0.0 - coverage:48.16666666666666 - ex: 0 - tex: 12
[MASTER] 09:07:43.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.16666666666666, number of tests: 12, total length: 281
[MASTER] 09:07:48.905 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:222.83333333333331 - branchDistance:0.0 - coverage:47.83333333333333 - ex: 0 - tex: 14
[MASTER] 09:07:48.905 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 222.83333333333331, number of tests: 12, total length: 277
[MASTER] 09:07:58.927 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:222.16666666666666 - branchDistance:0.0 - coverage:47.16666666666666 - ex: 0 - tex: 16
[MASTER] 09:07:58.927 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 222.16666666666666, number of tests: 12, total length: 289
[MASTER] 09:08:26.267 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:221.16666666666666 - branchDistance:0.0 - coverage:46.16666666666666 - ex: 0 - tex: 14
[MASTER] 09:08:26.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 221.16666666666666, number of tests: 12, total length: 263
[MASTER] 09:09:48.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:220.16666666666666 - branchDistance:0.0 - coverage:45.16666666666666 - ex: 0 - tex: 10
[MASTER] 09:09:48.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 220.16666666666666, number of tests: 12, total length: 210
[MASTER] 09:10:01.137 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.66666666666666 - branchDistance:0.0 - coverage:52.16666666666666 - ex: 0 - tex: 10
[MASTER] 09:10:01.137 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.66666666666666, number of tests: 12, total length: 227
[MASTER] 09:10:04.088 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:119.66666666666666 - branchDistance:0.0 - coverage:46.16666666666666 - ex: 0 - tex: 10
[MASTER] 09:10:04.088 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.66666666666666, number of tests: 12, total length: 229
[MASTER] 09:10:04.912 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 68 generations, 70510 statements, best individuals have fitness: 119.66666666666666
[MASTER] 09:10:04.917 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 66%
* Total number of goals: 61
* Number of covered goals: 40
* Generated 12 tests with total length 229
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                    120 / 0           
[MASTER] 09:10:05.474 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:10:05.519 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 203 
[MASTER] 09:10:05.621 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 09:10:05.621 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 09:10:05.621 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 09:10:05.621 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 09:10:05.621 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 09:10:05.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 09:10:05.623 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 09:10:05.624 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 81
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiValueMap_ESTest' to evosuite-tests
* Done!

* Computation finished
