* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.map.MultiValueMap
* Starting client
* Connecting to master process on port 3072
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_27_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.map.MultiValueMap
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 09:51:53.052 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 87
*** Random test generation finished.
*=*=*=* Total tests: 1324 | Tests with assertion: 0
[MASTER] 09:56:54.243 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 0 tests with total length 0
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 09:56:54.288 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:56:54.289 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 184 seconds more than allowed.
[MASTER] 09:56:54.293 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 09:56:54.294 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 09:56:54.296 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 87
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiValueMap_ESTest' to evosuite-tests
* Done!

* Computation finished
