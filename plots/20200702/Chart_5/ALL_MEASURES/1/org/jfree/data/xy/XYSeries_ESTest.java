/*
 * This file was automatically generated by EvoSuite
 * Wed Jul 01 15:54:40 GMT 2020
 */

package org.jfree.data.xy;

import org.junit.Test;
import static org.junit.Assert.*;
import java.time.Instant;
import java.util.Date;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.evosuite.runtime.mock.java.time.MockInstant;
import org.jfree.data.time.SerialDate;
import org.jfree.data.xy.XYSeries;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYSeries_ESTest extends XYSeries_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Instant instant0 = MockInstant.now();
      Date date0 = Date.from(instant0);
      SerialDate serialDate0 = SerialDate.createInstance(date0);
      XYSeries xYSeries0 = new XYSeries(serialDate0, true);
      xYSeries0.add((double) 0, (-1388.0));
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.IndexOutOfBoundsException : Index: -1, Size: 4
      xYSeries0.addOrUpdate((Number) 0, (Number) 2958465);
  }
}
