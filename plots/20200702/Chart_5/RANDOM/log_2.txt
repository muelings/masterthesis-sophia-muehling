* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.xy.XYSeries
* Starting client
* Connecting to master process on port 4637
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_5_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.xy.XYSeries
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 17:54:50.111 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 103
[MASTER] 17:54:50.574 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:54:51.911 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:54:52.353 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:54:52.641 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_3_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 1 assertions.
[MASTER] 17:54:52.652 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:54:52.887 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:54:52.897 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:54:52.897 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 29 | Tests with assertion: 1
* Generated 1 tests with total length 8
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          2 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 17:54:53.478 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:54:53.490 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 6 
[MASTER] 17:54:53.502 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 17:54:53.502 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [addOrUpdate:IndexOutOfBoundsException]
[MASTER] 17:54:53.502 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: addOrUpdate:IndexOutOfBoundsException at 5
[MASTER] 17:54:53.502 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [addOrUpdate:IndexOutOfBoundsException]
[MASTER] 17:54:53.579 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 17:54:53.582 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 18%
* Total number of goals: 103
* Number of covered goals: 19
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYSeries_ESTest' to evosuite-tests
* Done!

* Computation finished
