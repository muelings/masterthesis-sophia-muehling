* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.xy.XYSeries
* Starting client
* Connecting to master process on port 20822
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_5_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.xy.XYSeries
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 103
[MASTER] 18:00:18.196 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:00:27.833 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 18:00:28.930 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 18:00:28.971 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 18:00:29.319 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 18:00:29.358 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 18:00:29.359 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 942 | Tests with assertion: 1
* Generated 1 tests with total length 22
* GA-Budget:
	- MaxTime :                         11 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 18:00:30.047 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:00:30.083 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 18:00:30.115 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 18:00:30.115 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [addOrUpdate:IndexOutOfBoundsException, getX:IndexOutOfBoundsException, IndexOutOfBoundsException:getDataItem-454,]
[MASTER] 18:00:30.115 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: getX:IndexOutOfBoundsException at 12
[MASTER] 18:00:30.115 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [addOrUpdate:IndexOutOfBoundsException, getX:IndexOutOfBoundsException, IndexOutOfBoundsException:getDataItem-454,]
[MASTER] 18:00:30.115 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: addOrUpdate:IndexOutOfBoundsException at 10
[MASTER] 18:00:30.115 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [addOrUpdate:IndexOutOfBoundsException, getX:IndexOutOfBoundsException, IndexOutOfBoundsException:getDataItem-454,]
[MASTER] 18:00:30.747 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 18:00:30.753 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 18%
* Total number of goals: 103
* Number of covered goals: 19
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYSeries_ESTest' to evosuite-tests
* Done!

* Computation finished
