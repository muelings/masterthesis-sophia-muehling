* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Util
* Starting client
* Connecting to master process on port 15541
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_5_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Util
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 11
[MASTER] 04:02:46.074 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:02:46.307 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 04:02:47.551 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:02:47.572 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 04:02:47.947 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 6 | Tests with assertion: 1
* Generated 1 tests with total length 19
* GA-Budget:
[MASTER] 04:02:47.971 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 04:02:47.971 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- MaxTime :                          1 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:02:48.298 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:02:48.328 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 04:02:48.343 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 04:02:48.343 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 04:02:48.343 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: stripLeadingHyphens:NullPointerException at 4
[MASTER] 04:02:48.343 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 04:02:48.569 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:02:48.573 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 9%
* Total number of goals: 11
* Number of covered goals: 1
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Util_ESTest' to evosuite-tests
* Done!

* Computation finished
