* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 11750
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 23:39:06.643 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 134
[MASTER] 23:40:14.891 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 23:40:15.996 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 23:40:16.010 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 23:40:16.332 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 23:40:16.346 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 23:40:16.347 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 5400 | Tests with assertion: 1
* Generated 1 tests with total length 24
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                         69 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 23:40:17.093 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:40:17.112 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 16 
[MASTER] 23:40:17.124 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 23:40:17.125 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [withFieldAddWrapped:MockIllegalArgumentException]
[MASTER] 23:40:17.125 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: withFieldAddWrapped:MockIllegalArgumentException at 15
[MASTER] 23:40:17.125 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [withFieldAddWrapped:MockIllegalArgumentException]
[MASTER] 23:40:17.381 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:40:17.385 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 134
* Number of covered goals: 8
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Partial_ESTest' to evosuite-tests
* Done!

* Computation finished
