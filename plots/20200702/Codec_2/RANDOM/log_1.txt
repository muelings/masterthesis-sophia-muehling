* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 9552
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 22:15:57.740 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 181
[MASTER] 22:16:02.293 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 22:16:03.516 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:16:03.529 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 22:16:03.897 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 22:16:03.912 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 22:16:03.912 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 405 | Tests with assertion: 1
* Generated 1 tests with total length 21
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          6 / 300         
[MASTER] 22:16:04.709 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:16:04.723 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 22:16:04.733 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 22:16:04.733 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [DecoderException:decode-638,, readResults:ArrayIndexOutOfBoundsException, decode:DecoderException]
[MASTER] 22:16:04.733 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: decode:DecoderException at 12
[MASTER] 22:16:04.733 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [DecoderException:decode-638,, readResults:ArrayIndexOutOfBoundsException, decode:DecoderException]
[MASTER] 22:16:04.733 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 11
[MASTER] 22:16:04.733 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [DecoderException:decode-638,, readResults:ArrayIndexOutOfBoundsException, decode:DecoderException]
[MASTER] 22:16:04.843 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:16:04.848 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 14%
* Total number of goals: 181
* Number of covered goals: 26
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
