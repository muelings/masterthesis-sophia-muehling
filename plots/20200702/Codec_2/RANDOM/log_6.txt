* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 21628
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 181
[MASTER] 22:43:00.977 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:43:12.826 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var22);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 22:43:12.840 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 22:43:12.841 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:43:13.907 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:43:13.916 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var22);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 22:43:13.918 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 22:43:13.918 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 22:43:14.252 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:43:14.269 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var22);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 22:43:14.270 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 22:43:14.270 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:43:14.270 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 1023 | Tests with assertion: 1
* Generated 1 tests with total length 25
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         13 / 300         
	- RMIStoppingCondition
[MASTER] 22:43:14.988 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:43:14.998 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 15 
[MASTER] 22:43:15.010 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 22:43:15.174 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:43:15.178 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 12%
* Total number of goals: 181
* Number of covered goals: 22
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
