* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 7996
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:04:46.410 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:04:46.417 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 181
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593637484000
* Starting evolution
[MASTER] 23:04:47.545 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:602.4895721446474 - branchDistance:0.0 - coverage:199.48957214464733 - ex: 0 - tex: 4
[MASTER] 23:04:47.545 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 602.4895721446474, number of tests: 3, total length: 56
[MASTER] 23:04:48.173 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:490.248051945258 - branchDistance:0.0 - coverage:87.24805194525797 - ex: 0 - tex: 16
[MASTER] 23:04:48.173 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 490.248051945258, number of tests: 9, total length: 245
[MASTER] 23:04:48.251 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:339.4274374784879 - branchDistance:0.0 - coverage:187.6774374784879 - ex: 0 - tex: 2
[MASTER] 23:04:48.251 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 339.4274374784879, number of tests: 4, total length: 91
[MASTER] 23:04:49.098 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:289.0833900198818 - branchDistance:0.0 - coverage:137.3333900198818 - ex: 0 - tex: 10
[MASTER] 23:04:49.098 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 289.0833900198818, number of tests: 7, total length: 186
[MASTER] 23:04:49.349 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:280.9286641920186 - branchDistance:0.0 - coverage:129.17866419201857 - ex: 0 - tex: 8
[MASTER] 23:04:49.349 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 280.9286641920186, number of tests: 6, total length: 163
[MASTER] 23:04:51.168 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:239.10262078911256 - branchDistance:0.0 - coverage:145.3333900198818 - ex: 0 - tex: 8
[MASTER] 23:04:51.168 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 239.10262078911256, number of tests: 6, total length: 179
[MASTER] 23:04:52.111 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:226.21822344229213 - branchDistance:0.0 - coverage:132.44899267306135 - ex: 0 - tex: 12
[MASTER] 23:04:52.111 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 226.21822344229213, number of tests: 8, total length: 215
[MASTER] 23:04:52.711 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:196.31210436156178 - branchDistance:0.0 - coverage:102.542873592331 - ex: 0 - tex: 10
[MASTER] 23:04:52.711 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 196.31210436156178, number of tests: 7, total length: 202
[MASTER] 23:04:53.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:193.47853028520768 - branchDistance:0.0 - coverage:99.7092995159769 - ex: 0 - tex: 2
[MASTER] 23:04:53.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 193.47853028520768, number of tests: 9, total length: 225
[MASTER] 23:04:53.699 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:183.18003781652956 - branchDistance:0.0 - coverage:89.41080704729879 - ex: 0 - tex: 10
[MASTER] 23:04:53.699 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 183.18003781652956, number of tests: 7, total length: 190
[MASTER] 23:04:53.996 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:181.43350102185542 - branchDistance:0.0 - coverage:87.66427025262465 - ex: 0 - tex: 4
[MASTER] 23:04:53.996 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 181.43350102185542, number of tests: 8, total length: 196
[MASTER] 23:04:54.261 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:169.80813610899048 - branchDistance:0.0 - coverage:76.03890533975972 - ex: 0 - tex: 14
[MASTER] 23:04:54.261 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 169.80813610899048, number of tests: 9, total length: 277
[MASTER] 23:04:54.605 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:167.0728949593867 - branchDistance:0.0 - coverage:73.30366419015593 - ex: 0 - tex: 10
[MASTER] 23:04:54.605 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 167.0728949593867, number of tests: 8, total length: 226
[MASTER] 23:04:54.899 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:157.43657195238265 - branchDistance:0.0 - coverage:63.66734118315187 - ex: 0 - tex: 8
[MASTER] 23:04:54.900 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 157.43657195238265, number of tests: 8, total length: 202
[MASTER] 23:04:57.623 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:156.43657195238265 - branchDistance:0.0 - coverage:62.66734118315187 - ex: 0 - tex: 10
[MASTER] 23:04:57.623 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 156.43657195238265, number of tests: 9, total length: 208
[MASTER] 23:04:58.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:156.35808358028962 - branchDistance:0.0 - coverage:62.58885281105884 - ex: 0 - tex: 10
[MASTER] 23:04:58.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 156.35808358028962, number of tests: 8, total length: 208
[MASTER] 23:04:59.091 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:152.43657195238265 - branchDistance:0.0 - coverage:58.66734118315187 - ex: 0 - tex: 12
[MASTER] 23:04:59.091 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 152.43657195238265, number of tests: 10, total length: 212
[MASTER] 23:05:00.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:146.10323862091195 - branchDistance:0.0 - coverage:52.33400785168118 - ex: 0 - tex: 10
[MASTER] 23:05:00.008 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.10323862091195, number of tests: 10, total length: 212
[MASTER] 23:05:01.376 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:144.77280219500824 - branchDistance:0.0 - coverage:51.00357142577745 - ex: 0 - tex: 12
[MASTER] 23:05:01.376 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.77280219500824, number of tests: 10, total length: 251
[MASTER] 23:05:01.780 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:142.58386632653966 - branchDistance:0.0 - coverage:48.81463555730888 - ex: 0 - tex: 10
[MASTER] 23:05:01.780 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.58386632653966, number of tests: 10, total length: 261
[MASTER] 23:05:01.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:139.8605230013337 - branchDistance:0.0 - coverage:63.48552300133369 - ex: 0 - tex: 8
[MASTER] 23:05:01.852 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.8605230013337, number of tests: 8, total length: 206
[MASTER] 23:05:02.176 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:135.48446886353753 - branchDistance:0.0 - coverage:41.71523809430677 - ex: 0 - tex: 8
[MASTER] 23:05:02.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.48446886353753, number of tests: 10, total length: 248
[MASTER] 23:05:03.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:127.95705979787047 - branchDistance:0.0 - coverage:51.58205979787048 - ex: 0 - tex: 10
[MASTER] 23:05:03.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.95705979787047, number of tests: 9, total length: 249
[MASTER] 23:05:03.460 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:126.95705979787047 - branchDistance:0.0 - coverage:50.58205979787048 - ex: 0 - tex: 8
[MASTER] 23:05:03.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.95705979787047, number of tests: 10, total length: 266
[MASTER] 23:05:04.216 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:126.53580704729879 - branchDistance:0.0 - coverage:50.16080704729879 - ex: 0 - tex: 8
[MASTER] 23:05:04.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.53580704729879, number of tests: 10, total length: 280
[MASTER] 23:05:04.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:122.62199486466818 - branchDistance:0.0 - coverage:46.24699486466819 - ex: 0 - tex: 10
[MASTER] 23:05:04.348 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.62199486466818, number of tests: 9, total length: 249
[MASTER] 23:05:05.898 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:122.42357142577745 - branchDistance:0.0 - coverage:46.04857142577745 - ex: 0 - tex: 12
[MASTER] 23:05:05.898 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.42357142577745, number of tests: 11, total length: 288
[MASTER] 23:05:06.546 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:118.09023809430677 - branchDistance:0.0 - coverage:41.71523809430677 - ex: 0 - tex: 12
[MASTER] 23:05:06.546 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.09023809430677, number of tests: 11, total length: 268
[MASTER] 23:05:07.953 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:118.08114718521585 - branchDistance:0.0 - coverage:41.70614718521586 - ex: 0 - tex: 14
[MASTER] 23:05:07.954 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.08114718521585, number of tests: 12, total length: 291
[MASTER] 23:05:08.587 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:115.26829004235871 - branchDistance:0.0 - coverage:38.893290042358714 - ex: 0 - tex: 8
[MASTER] 23:05:08.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.26829004235871, number of tests: 11, total length: 297
[MASTER] 23:05:09.483 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:114.26829004235871 - branchDistance:0.0 - coverage:37.893290042358714 - ex: 0 - tex: 8
[MASTER] 23:05:09.483 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.26829004235871, number of tests: 11, total length: 306
[MASTER] 23:05:10.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:114.09023809430677 - branchDistance:0.0 - coverage:37.71523809430677 - ex: 0 - tex: 12
[MASTER] 23:05:10.489 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.09023809430677, number of tests: 11, total length: 275
[MASTER] 23:05:10.898 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:113.01829004235873 - branchDistance:0.0 - coverage:36.64329004235872 - ex: 0 - tex: 10
[MASTER] 23:05:10.899 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.01829004235873, number of tests: 12, total length: 339
[MASTER] 23:05:12.864 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:111.26829004235873 - branchDistance:0.0 - coverage:34.89329004235872 - ex: 0 - tex: 12
[MASTER] 23:05:12.864 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.26829004235873, number of tests: 12, total length: 323
[MASTER] 23:05:14.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:111.01829004235873 - branchDistance:0.0 - coverage:34.64329004235872 - ex: 0 - tex: 12
[MASTER] 23:05:14.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.01829004235873, number of tests: 12, total length: 349
[MASTER] 23:05:14.984 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:110.16114718521587 - branchDistance:0.0 - coverage:33.78614718521586 - ex: 0 - tex: 12
[MASTER] 23:05:14.984 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.16114718521587, number of tests: 12, total length: 346
[MASTER] 23:05:15.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:110.01829004235873 - branchDistance:0.0 - coverage:33.64329004235872 - ex: 0 - tex: 16
[MASTER] 23:05:15.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.01829004235873, number of tests: 15, total length: 439
[MASTER] 23:05:16.686 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:108.16114718521587 - branchDistance:0.0 - coverage:31.786147185215867 - ex: 0 - tex: 14
[MASTER] 23:05:16.686 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.16114718521587, number of tests: 14, total length: 371
[MASTER] 23:05:18.525 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:107.16114718521587 - branchDistance:0.0 - coverage:30.786147185215867 - ex: 0 - tex: 14
[MASTER] 23:05:18.525 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.16114718521587, number of tests: 14, total length: 380
[MASTER] 23:05:22.393 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:106.16114718521587 - branchDistance:0.0 - coverage:29.786147185215867 - ex: 0 - tex: 14
[MASTER] 23:05:22.393 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.16114718521587, number of tests: 14, total length: 355
[MASTER] 23:05:23.683 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:105.16114718521587 - branchDistance:0.0 - coverage:28.786147185215867 - ex: 0 - tex: 18
[MASTER] 23:05:23.683 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.16114718521587, number of tests: 15, total length: 406
[MASTER] 23:05:28.982 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:105.15746054927116 - branchDistance:0.0 - coverage:28.782460549271164 - ex: 0 - tex: 18
[MASTER] 23:05:28.982 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.15746054927116, number of tests: 16, total length: 413
[MASTER] 23:05:29.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:104.64350012639233 - branchDistance:0.0 - coverage:28.268500126392333 - ex: 0 - tex: 16
[MASTER] 23:05:29.754 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.64350012639233, number of tests: 15, total length: 381
[MASTER] 23:05:30.431 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:103.66114718521587 - branchDistance:0.0 - coverage:27.286147185215867 - ex: 0 - tex: 14
[MASTER] 23:05:30.431 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.66114718521587, number of tests: 14, total length: 356
[MASTER] 23:05:34.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:103.64350012639233 - branchDistance:0.0 - coverage:27.268500126392333 - ex: 0 - tex: 14
[MASTER] 23:05:34.302 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.64350012639233, number of tests: 14, total length: 355
[MASTER] 23:05:34.787 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:102.66114718521587 - branchDistance:0.0 - coverage:26.286147185215867 - ex: 0 - tex: 14
[MASTER] 23:05:34.788 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.66114718521587, number of tests: 14, total length: 355
[MASTER] 23:05:36.227 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:102.64350012639233 - branchDistance:0.0 - coverage:26.268500126392333 - ex: 0 - tex: 14
[MASTER] 23:05:36.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.64350012639233, number of tests: 14, total length: 352
[MASTER] 23:05:36.648 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:98.63796296203164 - branchDistance:0.0 - coverage:22.26296296203164 - ex: 0 - tex: 18
[MASTER] 23:05:36.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.63796296203164, number of tests: 15, total length: 380
[MASTER] 23:05:38.320 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:98.14642857049725 - branchDistance:0.0 - coverage:21.77142857049725 - ex: 0 - tex: 18
[MASTER] 23:05:38.320 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.14642857049725, number of tests: 15, total length: 382
[MASTER] 23:05:38.457 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:98.13796296203164 - branchDistance:0.0 - coverage:21.762962962031636 - ex: 0 - tex: 16
[MASTER] 23:05:38.457 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.13796296203164, number of tests: 15, total length: 366
[MASTER] 23:05:39.670 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:97.14642857049725 - branchDistance:0.0 - coverage:20.77142857049725 - ex: 0 - tex: 16
[MASTER] 23:05:39.670 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.14642857049725, number of tests: 15, total length: 367
[MASTER] 23:05:39.859 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:97.13796296203164 - branchDistance:0.0 - coverage:20.762962962031636 - ex: 0 - tex: 16
[MASTER] 23:05:39.859 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.13796296203164, number of tests: 15, total length: 366
[MASTER] 23:05:40.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:97.12031590320811 - branchDistance:0.0 - coverage:20.74531590320811 - ex: 0 - tex: 16
[MASTER] 23:05:40.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.12031590320811, number of tests: 15, total length: 367
[MASTER] 23:05:41.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:97.01574073980942 - branchDistance:0.0 - coverage:20.640740739809416 - ex: 0 - tex: 18
[MASTER] 23:05:41.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.01574073980942, number of tests: 16, total length: 380
[MASTER] 23:05:41.511 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:96.13796296203164 - branchDistance:0.0 - coverage:19.762962962031636 - ex: 0 - tex: 16
[MASTER] 23:05:41.511 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.13796296203164, number of tests: 15, total length: 366
[MASTER] 23:05:44.594 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:95.13796296203164 - branchDistance:0.0 - coverage:18.762962962031636 - ex: 0 - tex: 16
[MASTER] 23:05:44.594 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.13796296203164, number of tests: 16, total length: 407
[MASTER] 23:05:44.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:95.12031590320811 - branchDistance:0.0 - coverage:18.74531590320811 - ex: 0 - tex: 16
[MASTER] 23:05:44.872 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.12031590320811, number of tests: 16, total length: 416
[MASTER] 23:05:46.998 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:95.11735294024515 - branchDistance:0.0 - coverage:18.742352940245148 - ex: 0 - tex: 18
[MASTER] 23:05:46.998 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.11735294024515, number of tests: 17, total length: 441
[MASTER] 23:05:47.378 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:94.13796296203164 - branchDistance:0.0 - coverage:17.76296296203164 - ex: 0 - tex: 16
[MASTER] 23:05:47.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.13796296203164, number of tests: 16, total length: 424
[MASTER] 23:05:48.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:94.12031590320811 - branchDistance:0.0 - coverage:17.74531590320811 - ex: 0 - tex: 16
[MASTER] 23:05:48.701 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.12031590320811, number of tests: 16, total length: 417
[MASTER] 23:05:50.756 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:93.98796296203165 - branchDistance:0.0 - coverage:17.612962962031638 - ex: 0 - tex: 16
[MASTER] 23:05:50.756 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.98796296203165, number of tests: 16, total length: 419
[MASTER] 23:05:53.540 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:83.21900011373444 - branchDistance:0.0 - coverage:18.74531590320811 - ex: 0 - tex: 18
[MASTER] 23:05:53.540 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.21900011373444, number of tests: 16, total length: 387
[MASTER] 23:05:53.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:82.21900011373444 - branchDistance:0.0 - coverage:17.74531590320811 - ex: 0 - tex: 16
[MASTER] 23:05:53.706 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.21900011373444, number of tests: 17, total length: 431
[MASTER] 23:05:54.827 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:82.08664717255796 - branchDistance:0.0 - coverage:17.612962962031638 - ex: 0 - tex: 16
[MASTER] 23:05:54.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.08664717255796, number of tests: 17, total length: 443
[MASTER] 23:05:55.647 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.08664717255796 - branchDistance:0.0 - coverage:16.612962962031638 - ex: 0 - tex: 16
[MASTER] 23:05:55.647 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.08664717255796, number of tests: 17, total length: 452
[MASTER] 23:05:58.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:73.43114478021346 - branchDistance:0.0 - coverage:17.612962962031638 - ex: 0 - tex: 20
[MASTER] 23:05:58.324 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.43114478021346, number of tests: 18, total length: 481
[MASTER] 23:05:59.273 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:72.43114478021346 - branchDistance:0.0 - coverage:16.612962962031638 - ex: 0 - tex: 18
[MASTER] 23:05:59.273 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.43114478021346, number of tests: 18, total length: 477
[MASTER] 23:06:11.278 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:72.34932659839528 - branchDistance:0.0 - coverage:16.531144780213456 - ex: 0 - tex: 14
[MASTER] 23:06:11.278 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.34932659839528, number of tests: 18, total length: 432
[MASTER] 23:06:12.630 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:72.31433566340435 - branchDistance:0.0 - coverage:16.496153845222523 - ex: 0 - tex: 16
[MASTER] 23:06:12.630 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.31433566340435, number of tests: 18, total length: 462
[MASTER] 23:06:33.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:72.28100233007102 - branchDistance:0.0 - coverage:16.46282051188919 - ex: 0 - tex: 18
[MASTER] 23:06:33.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.28100233007102, number of tests: 19, total length: 481
[MASTER] 23:06:35.414 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:71.31433566340435 - branchDistance:0.0 - coverage:15.496153845222524 - ex: 0 - tex: 16
[MASTER] 23:06:35.414 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.31433566340435, number of tests: 19, total length: 469
[MASTER] 23:06:39.393 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:71.28100233007102 - branchDistance:0.0 - coverage:15.462820511889191 - ex: 0 - tex: 18
[MASTER] 23:06:39.393 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.28100233007102, number of tests: 20, total length: 502
[MASTER] 23:06:46.616 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:71.03484848391717 - branchDistance:0.0 - coverage:15.216666665735344 - ex: 0 - tex: 18
[MASTER] 23:06:46.616 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.03484848391717, number of tests: 21, total length: 515
[MASTER] 23:06:50.807 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:70.95151515058384 - branchDistance:0.0 - coverage:15.13333333240201 - ex: 0 - tex: 18
[MASTER] 23:06:50.807 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.95151515058384, number of tests: 21, total length: 523
[MASTER] 23:06:55.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:70.79913419820288 - branchDistance:0.0 - coverage:14.980952380021057 - ex: 0 - tex: 20
[MASTER] 23:06:55.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.79913419820288, number of tests: 21, total length: 520
[MASTER] 23:06:57.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:69.28484848391717 - branchDistance:0.0 - coverage:13.466666665735344 - ex: 0 - tex: 18
[MASTER] 23:06:57.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.28484848391717, number of tests: 22, total length: 545
[MASTER] 23:07:00.009 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:64.22095238002106 - branchDistance:0.0 - coverage:14.980952380021057 - ex: 0 - tex: 18
[MASTER] 23:07:00.009 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.22095238002106, number of tests: 20, total length: 481
[MASTER] 23:07:04.986 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:63.5542857133544 - branchDistance:0.0 - coverage:14.31428571335439 - ex: 0 - tex: 18
[MASTER] 23:07:04.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.5542857133544, number of tests: 20, total length: 470
[MASTER] 23:07:05.228 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:63.069629628698316 - branchDistance:0.0 - coverage:13.829629628698306 - ex: 0 - tex: 16
[MASTER] 23:07:05.228 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.069629628698316, number of tests: 21, total length: 484
[MASTER] 23:07:05.762 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:62.706666665735355 - branchDistance:0.0 - coverage:13.466666665735344 - ex: 0 - tex: 18
[MASTER] 23:07:05.762 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.706666665735355, number of tests: 22, total length: 523
[MASTER] 23:07:06.338 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:62.5542857133544 - branchDistance:0.0 - coverage:13.31428571335439 - ex: 0 - tex: 20
[MASTER] 23:07:06.338 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.5542857133544, number of tests: 22, total length: 550
[MASTER] 23:07:39.267 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:57.38571428478296 - branchDistance:0.0 - coverage:13.31428571335439 - ex: 0 - tex: 16
[MASTER] 23:07:39.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.38571428478296, number of tests: 21, total length: 439
[MASTER] 23:07:44.307 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:55.67142857049725 - branchDistance:0.0 - coverage:11.599999999068677 - ex: 0 - tex: 16
[MASTER] 23:07:44.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.67142857049725, number of tests: 20, total length: 445
[MASTER] 23:07:44.955 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.333333333333334 - fitness:53.403225805520286 - branchDistance:0.0 - coverage:13.499999999068677 - ex: 0 - tex: 16
[MASTER] 23:07:44.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.403225805520286, number of tests: 21, total length: 445
[MASTER] 23:07:46.515 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.333333333333334 - fitness:53.217511519805996 - branchDistance:0.0 - coverage:13.31428571335439 - ex: 0 - tex: 16
[MASTER] 23:07:46.515 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.217511519805996, number of tests: 21, total length: 437
[MASTER] 23:07:48.630 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.333333333333334 - fitness:52.217511519805996 - branchDistance:0.0 - coverage:12.31428571335439 - ex: 0 - tex: 18
[MASTER] 23:07:48.630 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.217511519805996, number of tests: 22, total length: 447
[MASTER] 23:07:50.162 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.333333333333334 - fitness:48.78487394864851 - branchDistance:0.0 - coverage:12.31428571335439 - ex: 0 - tex: 18
[MASTER] 23:07:50.162 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.78487394864851, number of tests: 22, total length: 452
[MASTER] 23:08:05.240 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.333333333333334 - fitness:48.14783691161147 - branchDistance:0.0 - coverage:11.677248676317353 - ex: 0 - tex: 16
[MASTER] 23:08:05.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.14783691161147, number of tests: 22, total length: 426
[MASTER] 23:08:05.532 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.333333333333334 - fitness:47.78487394864851 - branchDistance:0.0 - coverage:11.31428571335439 - ex: 0 - tex: 18
[MASTER] 23:08:05.532 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.78487394864851, number of tests: 22, total length: 428
[MASTER] 23:08:36.990 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.333333333333334 - fitness:46.18487394864851 - branchDistance:0.0 - coverage:9.714285713354391 - ex: 0 - tex: 16
[MASTER] 23:08:36.991 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.18487394864851, number of tests: 21, total length: 378
[MASTER] 23:09:06.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.333333333333334 - fitness:43.30888030794898 - branchDistance:0.0 - coverage:9.714285713354391 - ex: 0 - tex: 16
[MASTER] 23:09:06.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 43.30888030794898, number of tests: 20, total length: 340
[MASTER] 23:09:33.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.333333333333334 - fitness:43.09459459366327 - branchDistance:0.0 - coverage:9.499999999068677 - ex: 0 - tex: 16
[MASTER] 23:09:33.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 43.09459459366327, number of tests: 19, total length: 327
[MASTER] 23:09:36.362 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.333333333333334 - fitness:42.9279279269966 - branchDistance:0.0 - coverage:9.33333333240201 - ex: 0 - tex: 16
[MASTER] 23:09:36.362 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.9279279269966, number of tests: 19, total length: 319
[MASTER] 23:09:45.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.541666666666666 - fitness:41.88648947858141 - branchDistance:0.0 - coverage:9.33333333240201 - ex: 1 - tex: 17
[MASTER] 23:09:45.489 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.88648947858141, number of tests: 20, total length: 333
[MASTER] 23:09:47.461 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 407 generations, 494856 statements, best individuals have fitness: 41.88648947858141
[MASTER] 23:09:47.465 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 96%
* Total number of goals: 181
* Number of covered goals: 173
* Generated 19 tests with total length 319
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     42 / 0           
[MASTER] 23:09:47.924 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:09:48.030 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 221 
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.147 [logback-1] WARN  RegressionSuiteMinimizer - Test17 - Difference in number of exceptions: 0.0
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
* Going to analyze the coverage criteria
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
* Coverage analysis for criterion REGRESSION
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 23:09:48.148 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 23:09:48.149 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 181
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
