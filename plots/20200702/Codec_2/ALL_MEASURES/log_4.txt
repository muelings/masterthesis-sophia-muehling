* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 15667
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 22:32:25.255 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 22:32:25.263 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 181
* Using seed 1593635543015
* Starting evolution
[MASTER] 22:32:26.375 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:589.8864357855044 - branchDistance:0.0 - coverage:186.88643578550446 - ex: 0 - tex: 10
[MASTER] 22:32:26.375 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 589.8864357855044, number of tests: 6, total length: 157
[MASTER] 22:32:26.568 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:522.1980831969496 - branchDistance:0.0 - coverage:119.1980831969496 - ex: 0 - tex: 16
[MASTER] 22:32:26.568 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 522.1980831969496, number of tests: 10, total length: 228
[MASTER] 22:32:26.816 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:504.21025096814316 - branchDistance:0.0 - coverage:101.21025096814317 - ex: 0 - tex: 12
[MASTER] 22:32:26.816 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 504.21025096814316, number of tests: 9, total length: 221
[MASTER] 22:32:27.183 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:277.859814218051 - branchDistance:0.0 - coverage:126.10981421805099 - ex: 0 - tex: 8
[MASTER] 22:32:27.183 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 277.859814218051, number of tests: 9, total length: 176
[MASTER] 22:32:39.541 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:262.40130609135036 - branchDistance:0.0 - coverage:110.65130609135038 - ex: 0 - tex: 12
[MASTER] 22:32:39.541 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 262.40130609135036, number of tests: 7, total length: 188
[MASTER] 22:32:41.277 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:253.41560382053456 - branchDistance:0.0 - coverage:101.66560382053456 - ex: 0 - tex: 10
[MASTER] 22:32:41.277 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 253.41560382053456, number of tests: 7, total length: 181
[MASTER] 22:32:41.902 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:245.45008657915525 - branchDistance:0.0 - coverage:93.70008657915525 - ex: 0 - tex: 12
[MASTER] 22:32:41.902 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.45008657915525, number of tests: 7, total length: 183
[MASTER] 22:32:42.173 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.8333333333333335 - fitness:243.74074486160697 - branchDistance:0.0 - coverage:100.85839192043052 - ex: 0 - tex: 16
[MASTER] 22:32:42.173 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.74074486160697, number of tests: 8, total length: 238
[MASTER] 22:32:42.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:226.25265068171936 - branchDistance:0.0 - coverage:74.50265068171936 - ex: 0 - tex: 10
[MASTER] 22:32:42.691 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 226.25265068171936, number of tests: 7, total length: 193
[MASTER] 22:32:43.288 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.8333333333333335 - fitness:224.22468691508686 - branchDistance:0.0 - coverage:81.34233397391039 - ex: 0 - tex: 10
[MASTER] 22:32:43.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 224.22468691508686, number of tests: 7, total length: 190
[MASTER] 22:32:43.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:223.71698634605502 - branchDistance:0.0 - coverage:71.96698634605502 - ex: 0 - tex: 16
[MASTER] 22:32:43.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.71698634605502, number of tests: 9, total length: 246
[MASTER] 22:32:44.397 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:219.8834199124886 - branchDistance:0.0 - coverage:68.13341991248859 - ex: 0 - tex: 14
[MASTER] 22:32:44.397 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.8834199124886, number of tests: 9, total length: 257
[MASTER] 22:32:45.063 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:213.0859840150527 - branchDistance:0.0 - coverage:61.33598401505269 - ex: 0 - tex: 8
[MASTER] 22:32:45.063 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 213.0859840150527, number of tests: 7, total length: 168
[MASTER] 22:32:45.535 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:206.5701465192152 - branchDistance:0.0 - coverage:54.8201465192152 - ex: 0 - tex: 16
[MASTER] 22:32:45.536 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 206.5701465192152, number of tests: 10, total length: 245
[MASTER] 22:32:46.925 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:203.9777056267743 - branchDistance:0.0 - coverage:52.2277056267743 - ex: 0 - tex: 8
[MASTER] 22:32:46.925 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 203.9777056267743, number of tests: 10, total length: 174
[MASTER] 22:32:48.417 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:197.62142857049724 - branchDistance:0.0 - coverage:45.871428570497244 - ex: 0 - tex: 10
[MASTER] 22:32:48.417 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 197.62142857049724, number of tests: 10, total length: 254
[MASTER] 22:32:48.756 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:193.65153180060048 - branchDistance:0.0 - coverage:41.90153180060047 - ex: 0 - tex: 8
[MASTER] 22:32:48.756 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 193.65153180060048, number of tests: 9, total length: 206
[MASTER] 22:32:50.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:142.03207038482327 - branchDistance:0.0 - coverage:48.262839615592505 - ex: 0 - tex: 14
[MASTER] 22:32:50.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.03207038482327, number of tests: 9, total length: 242
[MASTER] 22:32:51.593 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:138.75604395511263 - branchDistance:0.0 - coverage:44.986813185881864 - ex: 0 - tex: 10
[MASTER] 22:32:51.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.75604395511263, number of tests: 9, total length: 191
[MASTER] 22:32:52.738 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:136.58937728844597 - branchDistance:0.0 - coverage:42.82014651921519 - ex: 0 - tex: 10
[MASTER] 22:32:52.738 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.58937728844597, number of tests: 9, total length: 221
[MASTER] 22:32:53.217 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:135.58937728844597 - branchDistance:0.0 - coverage:41.82014651921519 - ex: 0 - tex: 14
[MASTER] 22:32:53.217 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.58937728844597, number of tests: 11, total length: 284
[MASTER] 22:32:53.454 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:134.4227106217793 - branchDistance:0.0 - coverage:40.65347985254853 - ex: 0 - tex: 12
[MASTER] 22:32:53.454 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.4227106217793, number of tests: 9, total length: 236
[MASTER] 22:32:53.487 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:134.25604395511263 - branchDistance:0.0 - coverage:40.48681318588186 - ex: 0 - tex: 12
[MASTER] 22:32:53.487 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.25604395511263, number of tests: 10, total length: 253
[MASTER] 22:32:53.496 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:132.4227106217793 - branchDistance:0.0 - coverage:38.65347985254853 - ex: 0 - tex: 14
[MASTER] 22:32:53.496 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.4227106217793, number of tests: 10, total length: 278
[MASTER] 22:32:54.212 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:130.3073260063947 - branchDistance:0.0 - coverage:36.538095237163915 - ex: 0 - tex: 12
[MASTER] 22:32:54.212 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.3073260063947, number of tests: 10, total length: 261
[MASTER] 22:32:55.866 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:129.3073260063947 - branchDistance:0.0 - coverage:35.538095237163915 - ex: 0 - tex: 12
[MASTER] 22:32:55.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.3073260063947, number of tests: 11, total length: 290
[MASTER] 22:32:57.800 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:128.6040959031646 - branchDistance:0.0 - coverage:34.83486513393381 - ex: 0 - tex: 14
[MASTER] 22:32:57.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.6040959031646, number of tests: 12, total length: 301
[MASTER] 22:32:59.767 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:127.60409590316459 - branchDistance:0.0 - coverage:33.83486513393381 - ex: 0 - tex: 12
[MASTER] 22:32:59.767 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.60409590316459, number of tests: 12, total length: 279
[MASTER] 22:32:59.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:127.30732600639469 - branchDistance:0.0 - coverage:33.538095237163915 - ex: 0 - tex: 12
[MASTER] 22:32:59.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.30732600639469, number of tests: 11, total length: 266
[MASTER] 22:33:00.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:126.7220446211133 - branchDistance:0.0 - coverage:32.95281385188253 - ex: 0 - tex: 12
[MASTER] 22:33:00.513 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.7220446211133, number of tests: 12, total length: 273
[MASTER] 22:33:01.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:124.65537795444664 - branchDistance:0.0 - coverage:30.88614718521587 - ex: 0 - tex: 14
[MASTER] 22:33:01.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.65537795444664, number of tests: 11, total length: 309
[MASTER] 22:33:01.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:111.20986513393382 - branchDistance:0.0 - coverage:34.83486513393381 - ex: 0 - tex: 14
[MASTER] 22:33:01.931 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.20986513393382, number of tests: 12, total length: 300
[MASTER] 22:33:03.501 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:104.54642857049726 - branchDistance:0.0 - coverage:28.17142857049725 - ex: 0 - tex: 14
[MASTER] 22:33:03.501 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.54642857049726, number of tests: 13, total length: 312
[MASTER] 22:33:05.443 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:102.54642857049726 - branchDistance:0.0 - coverage:26.17142857049725 - ex: 0 - tex: 12
[MASTER] 22:33:05.443 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.54642857049726, number of tests: 12, total length: 277
[MASTER] 22:33:07.723 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:101.34274193455255 - branchDistance:0.0 - coverage:24.96774193455255 - ex: 0 - tex: 14
[MASTER] 22:33:07.723 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.34274193455255, number of tests: 12, total length: 314
[MASTER] 22:33:07.791 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:100.54642857049726 - branchDistance:0.0 - coverage:24.17142857049725 - ex: 0 - tex: 14
[MASTER] 22:33:07.791 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.54642857049726, number of tests: 13, total length: 313
[MASTER] 22:33:12.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:100.54469696876565 - branchDistance:0.0 - coverage:24.169696968765646 - ex: 0 - tex: 14
[MASTER] 22:33:12.043 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.54469696876565, number of tests: 13, total length: 324
[MASTER] 22:33:12.051 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:99.87803030209898 - branchDistance:0.0 - coverage:23.50303030209898 - ex: 0 - tex: 14
[MASTER] 22:33:12.051 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.87803030209898, number of tests: 13, total length: 303
[MASTER] 22:33:12.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:99.54642857049726 - branchDistance:0.0 - coverage:23.17142857049725 - ex: 0 - tex: 16
[MASTER] 22:33:12.549 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.54642857049726, number of tests: 13, total length: 296
[MASTER] 22:33:13.337 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:98.87803030209898 - branchDistance:0.0 - coverage:22.50303030209898 - ex: 0 - tex: 16
[MASTER] 22:33:13.337 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.87803030209898, number of tests: 13, total length: 280
[MASTER] 22:33:13.856 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:88.9767145126253 - branchDistance:0.0 - coverage:24.50303030209898 - ex: 0 - tex: 14
[MASTER] 22:33:13.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.9767145126253, number of tests: 13, total length: 304
[MASTER] 22:33:14.583 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:87.9767145126253 - branchDistance:0.0 - coverage:23.50303030209898 - ex: 0 - tex: 14
[MASTER] 22:33:14.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.9767145126253, number of tests: 13, total length: 303
[MASTER] 22:33:15.452 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:86.9767145126253 - branchDistance:0.0 - coverage:22.50303030209898 - ex: 0 - tex: 16
[MASTER] 22:33:15.452 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.9767145126253, number of tests: 14, total length: 321
[MASTER] 22:33:16.224 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:85.9767145126253 - branchDistance:0.0 - coverage:21.50303030209898 - ex: 0 - tex: 16
[MASTER] 22:33:16.224 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.9767145126253, number of tests: 14, total length: 320
[MASTER] 22:33:18.452 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:84.9767145126253 - branchDistance:0.0 - coverage:20.50303030209898 - ex: 0 - tex: 18
[MASTER] 22:33:18.453 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.9767145126253, number of tests: 14, total length: 337
[MASTER] 22:33:20.555 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:84.92286835877916 - branchDistance:0.0 - coverage:20.44918414825283 - ex: 0 - tex: 20
[MASTER] 22:33:20.555 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.92286835877916, number of tests: 15, total length: 374
[MASTER] 22:33:20.719 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:83.9767145126253 - branchDistance:0.0 - coverage:19.50303030209898 - ex: 0 - tex: 16
[MASTER] 22:33:20.719 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.9767145126253, number of tests: 15, total length: 363
[MASTER] 22:33:23.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:83.84338117929197 - branchDistance:0.0 - coverage:19.36969696876565 - ex: 0 - tex: 16
[MASTER] 22:33:23.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.84338117929197, number of tests: 15, total length: 371
[MASTER] 22:33:25.289 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:82.12286835877916 - branchDistance:0.0 - coverage:17.64918414825283 - ex: 0 - tex: 18
[MASTER] 22:33:25.289 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.12286835877916, number of tests: 16, total length: 380
[MASTER] 22:33:29.490 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.1767145126253 - branchDistance:0.0 - coverage:16.70303030209898 - ex: 0 - tex: 18
[MASTER] 22:33:29.490 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.1767145126253, number of tests: 16, total length: 381
[MASTER] 22:33:29.532 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.12286835877914 - branchDistance:0.0 - coverage:16.649184148252825 - ex: 0 - tex: 18
[MASTER] 22:33:29.532 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.12286835877914, number of tests: 16, total length: 390
[MASTER] 22:33:36.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.12091332456605 - branchDistance:0.0 - coverage:16.647229114039728 - ex: 0 - tex: 16
[MASTER] 22:33:36.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.12091332456605, number of tests: 15, total length: 360
[MASTER] 22:33:36.677 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.08953502544581 - branchDistance:0.0 - coverage:16.615850814919494 - ex: 0 - tex: 18
[MASTER] 22:33:36.677 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.08953502544581, number of tests: 16, total length: 405
[MASTER] 22:33:37.222 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:80.94650472241551 - branchDistance:0.0 - coverage:16.472820511889193 - ex: 0 - tex: 20
[MASTER] 22:33:37.222 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.94650472241551, number of tests: 15, total length: 363
[MASTER] 22:33:38.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:79.94650472241551 - branchDistance:0.0 - coverage:15.472820511889191 - ex: 0 - tex: 18
[MASTER] 22:33:38.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.94650472241551, number of tests: 15, total length: 369
[MASTER] 22:33:42.577 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:79.85035087626167 - branchDistance:0.0 - coverage:15.376666665735344 - ex: 0 - tex: 18
[MASTER] 22:33:42.577 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.85035087626167, number of tests: 15, total length: 360
[MASTER] 22:33:49.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:79.783684209595 - branchDistance:0.0 - coverage:15.309999999068676 - ex: 0 - tex: 20
[MASTER] 22:33:49.701 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.783684209595, number of tests: 15, total length: 338
[MASTER] 22:34:10.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.648809523809524 - fitness:66.4684806466615 - branchDistance:0.0 - coverage:18.988095237163915 - ex: 0 - tex: 16
[MASTER] 22:34:10.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.4684806466615, number of tests: 15, total length: 286
[MASTER] 22:34:12.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.648809523809524 - fitness:62.790385408566266 - branchDistance:0.0 - coverage:15.309999999068676 - ex: 0 - tex: 16
[MASTER] 22:34:12.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.790385408566266, number of tests: 15, total length: 314
[MASTER] 22:34:21.989 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.648809523809524 - fitness:62.69038540856627 - branchDistance:0.0 - coverage:15.209999999068678 - ex: 0 - tex: 16
[MASTER] 22:34:21.989 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.69038540856627, number of tests: 15, total length: 307
[MASTER] 22:34:28.410 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.651960784313726 - fitness:62.67345608972023 - branchDistance:0.0 - coverage:15.209999999068678 - ex: 0 - tex: 16
[MASTER] 22:34:28.410 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.67345608972023, number of tests: 15, total length: 303
[MASTER] 22:34:32.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.651960784313726 - fitness:62.66082451077286 - branchDistance:0.0 - coverage:15.19736842012131 - ex: 0 - tex: 16
[MASTER] 22:34:32.820 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.66082451077286, number of tests: 15, total length: 303
[MASTER] 22:34:36.560 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:62.59957383031155 - branchDistance:0.0 - coverage:15.209999999068678 - ex: 0 - tex: 16
[MASTER] 22:34:36.560 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.59957383031155, number of tests: 15, total length: 302
[MASTER] 22:34:38.025 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:62.58694225136418 - branchDistance:0.0 - coverage:15.19736842012131 - ex: 0 - tex: 16
[MASTER] 22:34:38.025 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.58694225136418, number of tests: 15, total length: 303
[MASTER] 22:34:43.511 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:62.33694225136418 - branchDistance:0.0 - coverage:14.94736842012131 - ex: 0 - tex: 18
[MASTER] 22:34:43.511 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.33694225136418, number of tests: 16, total length: 337
[MASTER] 22:34:43.914 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:62.08694225136418 - branchDistance:0.0 - coverage:14.69736842012131 - ex: 0 - tex: 14
[MASTER] 22:34:43.914 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.08694225136418, number of tests: 16, total length: 316
[MASTER] 22:34:51.038 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:61.08694225136418 - branchDistance:0.0 - coverage:13.69736842012131 - ex: 0 - tex: 16
[MASTER] 22:34:51.038 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.08694225136418, number of tests: 17, total length: 349
[MASTER] 22:34:57.467 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:61.048664739402454 - branchDistance:0.0 - coverage:13.659090908159587 - ex: 0 - tex: 16
[MASTER] 22:34:57.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.048664739402454, number of tests: 17, total length: 349
[MASTER] 22:35:04.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:61.01295045368817 - branchDistance:0.0 - coverage:13.623376622445301 - ex: 0 - tex: 16
[MASTER] 22:35:04.977 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.01295045368817, number of tests: 17, total length: 353
[MASTER] 22:35:16.459 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:60.96100240174012 - branchDistance:0.0 - coverage:13.571428570497249 - ex: 0 - tex: 16
[MASTER] 22:35:16.459 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.96100240174012, number of tests: 17, total length: 340
[MASTER] 22:35:42.900 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:59.96100240174012 - branchDistance:0.0 - coverage:12.571428570497249 - ex: 0 - tex: 16
[MASTER] 22:35:42.900 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.96100240174012, number of tests: 18, total length: 332
[MASTER] 22:35:50.266 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:58.96100240174012 - branchDistance:0.0 - coverage:11.571428570497249 - ex: 0 - tex: 16
[MASTER] 22:35:50.266 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.96100240174012, number of tests: 17, total length: 315
[MASTER] 22:35:52.492 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:58.58005002078774 - branchDistance:0.0 - coverage:11.190476189544869 - ex: 0 - tex: 16
[MASTER] 22:35:52.492 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.58005002078774, number of tests: 17, total length: 308
[MASTER] 22:36:03.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.6657403118727805 - fitness:58.24671668745441 - branchDistance:0.0 - coverage:10.857142856211535 - ex: 0 - tex: 18
[MASTER] 22:36:03.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.24671668745441, number of tests: 18, total length: 335
[MASTER] 22:36:45.186 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.665747971214209 - fitness:58.24667568538675 - branchDistance:0.0 - coverage:10.857142856211535 - ex: 0 - tex: 16
[MASTER] 22:36:45.186 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.24667568538675, number of tests: 16, total length: 263
[MASTER] 22:36:59.256 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.665747971214209 - fitness:57.24667568538675 - branchDistance:0.0 - coverage:9.857142856211535 - ex: 0 - tex: 18
[MASTER] 22:36:59.256 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.24667568538675, number of tests: 17, total length: 265

* Search finished after 301s and 398 generations, 440882 statements, best individuals have fitness: 57.24667568538675
[MASTER] 22:37:26.312 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:37:26.317 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 96%
* Total number of goals: 181
* Number of covered goals: 173
* Generated 17 tests with total length 243
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     57 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 22:37:26.785 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:37:26.933 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 182 
[MASTER] 22:37:27.040 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.041 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.041 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.041 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.041 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.041 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Test16 - Difference in number of exceptions: 0.0
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 22:37:27.042 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 22:37:27.043 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 22:37:27.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 22:37:27.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 22:37:27.044 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 22:37:27.045 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 181
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
