* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 5568
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 23
[MASTER] 07:17:39.282 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:20:18.826 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("MultiKey[[Ljava.lang.Object;@27d07190, MultiKey[zZ[t\"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds\", #`--oQ O8@~OW%<ds\"], zZ[t\"B6_Nh-kNpSW, zZ[t\"B6_Nh-kNpSW, MultiKey[zZ[t\"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds\", #`--oQ O8@~OW%<ds\"], zZ[t\"B6_Nh-kNpSW, zZ[t\"B6_Nh-kNpSW]", var17);  // (Primitive) Original Value: MultiKey[[Ljava.lang.Object;@27d07190, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW] | Regression Value: MultiKey[[Ljava.lang.Object;@3390e7ce, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW]
[MASTER] 07:20:18.831 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:20:18.832 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:20:19.674 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("MultiKey[[Ljava.lang.Object;@3f035dfe, MultiKey[zZ[t\"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds\", #`--oQ O8@~OW%<ds\"], zZ[t\"B6_Nh-kNpSW, zZ[t\"B6_Nh-kNpSW, MultiKey[zZ[t\"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds\", #`--oQ O8@~OW%<ds\"], zZ[t\"B6_Nh-kNpSW, zZ[t\"B6_Nh-kNpSW]", var17);  // (Primitive) Original Value: MultiKey[[Ljava.lang.Object;@3f035dfe, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW] | Regression Value: MultiKey[[Ljava.lang.Object;@661dea48, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW, MultiKey[zZ[t"B6_Nh-kNpSW, , , #`--oQ O8@~OW%<ds", #`--oQ O8@~OW%<ds"], zZ[t"B6_Nh-kNpSW, zZ[t"B6_Nh-kNpSW]
[MASTER] 07:20:19.677 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:20:19.677 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 07:22:40.285 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 41908 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 07:22:40.330 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:22:40.331 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 183 seconds more than allowed.
[MASTER] 07:22:40.341 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 07:22:40.342 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 07:22:40.344 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
