* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 3520
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:12:32.739 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:12:32.744 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 23
* Using seed 1593753149776
* Starting evolution
[MASTER] 07:12:33.936 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:70.0 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 2
[MASTER] 07:12:33.936 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.0, number of tests: 2, total length: 41
[MASTER] 07:12:34.080 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:60.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 07:12:34.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.0, number of tests: 4, total length: 63
[MASTER] 07:12:34.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.8888888888888888 - fitness:23.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 4
[MASTER] 07:12:34.410 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.0, number of tests: 6, total length: 140
[MASTER] 07:12:34.988 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9565217391304348 - fitness:19.58823529411765 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 07:12:34.988 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.58823529411765, number of tests: 3, total length: 130
[MASTER] 07:12:36.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9473684210526314 - fitness:15.173333333333334 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 07:12:36.513 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.173333333333334, number of tests: 3, total length: 116
[MASTER] 07:12:37.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9545454545454546 - fitness:15.149425287356323 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 07:12:37.412 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.149425287356323, number of tests: 3, total length: 117
[MASTER] 07:12:37.428 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.875 - fitness:11.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:12:37.428 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.666666666666666, number of tests: 5, total length: 174
[MASTER] 07:12:38.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.857142857142857 - fitness:10.878048780487806 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 07:12:38.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.878048780487806, number of tests: 5, total length: 145
[MASTER] 07:12:39.526 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.857142857142857 - fitness:9.878048780487806 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:39.526 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.878048780487806, number of tests: 5, total length: 149
[MASTER] 07:12:40.335 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.875 - fitness:9.851063829787234 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:12:40.335 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.851063829787234, number of tests: 6, total length: 208
[MASTER] 07:12:40.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.888888888888889 - fitness:8.548387096774192 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:40.706 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.548387096774192, number of tests: 7, total length: 191
[MASTER] 07:12:42.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.947368421052632 - fitness:8.484848484848484 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:42.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.484848484848484, number of tests: 7, total length: 192
[MASTER] 07:12:43.012 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.954545454545455 - fitness:8.477124183006534 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:12:43.012 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.477124183006534, number of tests: 6, total length: 187
[MASTER] 07:12:43.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.888888888888889 - fitness:7.591549295774648 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:12:43.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.591549295774648, number of tests: 6, total length: 158
[MASTER] 07:12:43.508 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.875 - fitness:6.859154929577465 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:43.508 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.859154929577465, number of tests: 6, total length: 216
[MASTER] 07:12:43.945 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.888888888888889 - fitness:6.85 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:43.945 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.85, number of tests: 7, total length: 215
[MASTER] 07:12:44.517 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.875 - fitness:6.265822784810126 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:44.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.265822784810126, number of tests: 6, total length: 216
[MASTER] 07:12:46.613 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.88888888888889 - fitness:6.258426966292134 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:46.613 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.258426966292134, number of tests: 6, total length: 190
[MASTER] 07:12:48.704 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.833333333333334 - fitness:5.8 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:48.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.8, number of tests: 6, total length: 187
[MASTER] 07:12:48.818 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.88888888888889 - fitness:5.775510204081632 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:48.818 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.775510204081632, number of tests: 6, total length: 169
[MASTER] 07:12:49.917 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.88888888888889 - fitness:5.373831775700935 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:49.917 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.373831775700935, number of tests: 6, total length: 149
[MASTER] 07:12:50.360 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.96551724137931 - fitness:5.345821325648416 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:50.360 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.345821325648416, number of tests: 6, total length: 155
[MASTER] 07:12:51.017 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.969696969696969 - fitness:5.3443037974683545 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:51.017 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.3443037974683545, number of tests: 6, total length: 149
[MASTER] 07:12:51.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.972222222222221 - fitness:5.343387470997681 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:51.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.343387470997681, number of tests: 6, total length: 148
[MASTER] 07:12:52.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.984615384615385 - fitness:5.338896020539153 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:52.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.338896020539153, number of tests: 6, total length: 148
[MASTER] 07:12:54.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.989690721649485 - fitness:5.337059329320722 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:54.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.337059329320722, number of tests: 6, total length: 127
[MASTER] 07:12:55.346 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.992248062015504 - fitness:5.336134453781512 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:55.346 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.336134453781512, number of tests: 6, total length: 121
[MASTER] 07:12:56.686 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.992248062015504 - fitness:5.002386634844869 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:56.686 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.002386634844869, number of tests: 6, total length: 114
[MASTER] 07:12:57.721 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.996884735202492 - fitness:5.000958772770853 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:12:57.721 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.000958772770853, number of tests: 6, total length: 111
[MASTER] 07:12:59.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.993788819875776 - fitness:4.7159343098091435 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:12:59.820 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.7159343098091435, number of tests: 5, total length: 103
[MASTER] 07:13:00.652 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.996884735202492 - fitness:4.7151123970620965 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:00.652 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.7151123970620965, number of tests: 5, total length: 91
[MASTER] 07:13:01.627 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.99644128113879 - fitness:4.46748932130992 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:13:01.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.46748932130992, number of tests: 5, total length: 88
[MASTER] 07:13:02.326 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.99644128113879 - fitness:4.250723025583982 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:13:02.326 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.250723025583982, number of tests: 5, total length: 90
[MASTER] 07:13:03.149 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.996884735202492 - fitness:4.2506329113924055 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:03.149 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.2506329113924055, number of tests: 5, total length: 88
[MASTER] 07:13:03.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.995850622406639 - fitness:4.0595703125 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:13:03.576 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0595703125, number of tests: 5, total length: 90
[MASTER] 07:13:05.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.99644128113879 - fitness:4.059463986599665 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:05.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.059463986599665, number of tests: 5, total length: 106
[MASTER] 07:13:06.272 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.996884735202492 - fitness:4.059384164222873 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:13:06.272 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.059384164222873, number of tests: 6, total length: 106
[MASTER] 07:13:07.467 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.996884735202492 - fitness:3.737290915054116 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:13:07.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.737290915054116, number of tests: 6, total length: 120
[MASTER] 07:13:09.859 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.996884735202492 - fitness:3.4765578635014838 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:13:09.859 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4765578635014838, number of tests: 7, total length: 126
[MASTER] 07:13:12.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.996884735202492 - fitness:3.3639711089080864 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:13:12.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.3639711089080864, number of tests: 7, total length: 121
[MASTER] 07:13:16.491 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.997506234413965 - fitness:3.363904319238182 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:13:16.491 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.363904319238182, number of tests: 6, total length: 93
[MASTER] 07:13:21.613 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.997506234413965 - fitness:3.2611147256560398 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:21.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2611147256560398, number of tests: 6, total length: 87
[MASTER] 07:13:33.915 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.997506234413965 - fitness:3.1668918216772317 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:33.915 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1668918216772317, number of tests: 5, total length: 69
[MASTER] 07:13:35.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.997150997150996 - fitness:3.0802370640528838 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:35.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0802370640528838, number of tests: 5, total length: 69
[MASTER] 07:13:38.755 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.997506234413965 - fitness:3.0802075019952118 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:38.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0802075019952118, number of tests: 4, total length: 75
[MASTER] 07:13:43.664 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.997506234413965 - fitness:3.0001918465227817 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:43.664 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0001918465227817, number of tests: 5, total length: 70
[MASTER] 07:13:50.407 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.997150997150996 - fitness:2.926129168425496 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:50.407 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926129168425496, number of tests: 4, total length: 64
[MASTER] 07:13:55.909 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.997506234413965 - fitness:2.926103824127101 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:13:55.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926103824127101, number of tests: 4, total length: 75
[MASTER] 07:14:04.296 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.997150997150996 - fitness:2.8573318408466473 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:14:04.296 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8573318408466473, number of tests: 5, total length: 101
[MASTER] 07:14:08.580 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.997506234413965 - fitness:2.8573082746949314 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:14:08.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8573082746949314, number of tests: 5, total length: 101
[MASTER] 07:14:18.847 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.997782705099777 - fitness:2.857289934267839 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:14:18.847 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.857289934267839, number of tests: 4, total length: 72
[MASTER] 07:14:20.290 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.998336106489184 - fitness:2.857253223985262 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:14:20.290 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.857253223985262, number of tests: 5, total length: 71
[MASTER] 07:14:31.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.997340425531917 - fitness:2.7932679079152525 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:14:31.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7932679079152525, number of tests: 4, total length: 65
[MASTER] 07:14:36.283 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.99809885931559 - fitness:2.7932210057037956 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:14:36.283 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7932210057037956, number of tests: 4, total length: 65
[MASTER] 07:14:36.461 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.998336106489184 - fitness:2.7932063346339224 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:14:36.461 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7932063346339224, number of tests: 4, total length: 65
[MASTER] 07:15:13.808 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.998336106489184 - fitness:2.733429474735149 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:15:13.808 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.733429474735149, number of tests: 4, total length: 57
[MASTER] 07:16:51.098 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.998336106489184 - fitness:2.6775093934514222 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:16:51.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6775093934514222, number of tests: 3, total length: 52
[MASTER] 07:17:02.664 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.998336106489184 - fitness:2.6250844989860123 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:17:02.664 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6250844989860123, number of tests: 3, total length: 52
[MASTER] 07:17:29.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.99809885931559 - fitness:2.575848360891859 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:17:29.706 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.575848360891859, number of tests: 3, total length: 53
[MASTER] 07:17:31.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.998336106489184 - fitness:2.575837031060912 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:17:31.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.575837031060912, number of tests: 3, total length: 53
[MASTER] 07:17:33.759 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 613 generations, 606604 statements, best individuals have fitness: 2.575837031060912
[MASTER] 07:17:33.763 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 23
* Number of covered goals: 23
* Generated 3 tests with total length 53
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                      3 / 0           
[MASTER] 07:17:34.149 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:17:34.166 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 49 
[MASTER] 07:17:34.212 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 07:17:34.213 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 07:17:34.213 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:17:34.213 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:17:34.213 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:17:34.213 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 07:17:34.214 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
