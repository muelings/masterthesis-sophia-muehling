* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 14021
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:43:12.446 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:43:12.451 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 23
* Using seed 1593754989586
* Starting evolution
[MASTER] 07:43:13.832 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:105.0 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 6
[MASTER] 07:43:13.832 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.0, number of tests: 7, total length: 166
[MASTER] 07:43:13.934 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:67.0 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 4
[MASTER] 07:43:13.934 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.0, number of tests: 5, total length: 81
[MASTER] 07:43:14.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.928571428571429 - fitness:13.550724637681158 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 4
[MASTER] 07:43:14.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.550724637681158, number of tests: 8, total length: 214
[MASTER] 07:43:14.231 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9545454545454546 - fitness:13.495412844036696 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 8
[MASTER] 07:43:14.231 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.495412844036696, number of tests: 9, total length: 148
[MASTER] 07:43:14.533 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.96 - fitness:13.483870967741936 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 12
[MASTER] 07:43:14.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.483870967741936, number of tests: 9, total length: 242
[MASTER] 07:43:15.715 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.967741935483871 - fitness:12.467532467532468 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 07:43:15.715 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.467532467532468, number of tests: 9, total length: 219
[MASTER] 07:43:16.701 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9696969696969697 - fitness:12.463414634146343 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 07:43:16.702 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.463414634146343, number of tests: 10, total length: 224
[MASTER] 07:43:17.248 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.9655172413793105 - fitness:10.716763005780347 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 07:43:17.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.716763005780347, number of tests: 10, total length: 220
[MASTER] 07:43:17.335 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.954545454545455 - fitness:7.807106598984771 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 12
[MASTER] 07:43:17.335 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.807106598984771, number of tests: 10, total length: 211
[MASTER] 07:43:18.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.984615384615385 - fitness:7.787671232876712 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 07:43:18.480 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.787671232876712, number of tests: 10, total length: 256
[MASTER] 07:43:19.897 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.954545454545455 - fitness:6.807106598984771 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 07:43:19.897 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.807106598984771, number of tests: 10, total length: 162
[MASTER] 07:43:20.727 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.98360655737705 - fitness:6.7343283582089555 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 07:43:20.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.7343283582089555, number of tests: 10, total length: 221
[MASTER] 07:43:20.888 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.954545454545455 - fitness:6.223744292237443 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 07:43:20.888 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.223744292237443, number of tests: 10, total length: 155
[MASTER] 07:43:21.198 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.984615384615385 - fitness:6.2080123266563945 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:21.198 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.2080123266563945, number of tests: 10, total length: 271
[MASTER] 07:43:21.338 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.98360655737705 - fitness:5.7343283582089555 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:21.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.7343283582089555, number of tests: 10, total length: 211
[MASTER] 07:43:22.341 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.984615384615385 - fitness:5.733893557422969 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:22.342 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.733893557422969, number of tests: 10, total length: 209
[MASTER] 07:43:22.473 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.992 - fitness:5.730713245997089 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:22.473 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.730713245997089, number of tests: 10, total length: 196
[MASTER] 07:43:22.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.982456140350877 - fitness:5.339677891654466 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:22.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.339677891654466, number of tests: 10, total length: 199
[MASTER] 07:43:23.710 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.991150442477876 - fitness:5.336531365313654 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:23.710 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.336531365313654, number of tests: 10, total length: 203
[MASTER] 07:43:24.079 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.981132075471699 - fitness:5.005813953488372 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:24.079 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.005813953488372, number of tests: 10, total length: 197
[MASTER] 07:43:24.195 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.987654320987655 - fitness:5.003802281368821 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:24.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.003802281368821, number of tests: 10, total length: 204
[MASTER] 07:43:24.697 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.992248062015504 - fitness:5.002386634844869 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:43:24.697 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.002386634844869, number of tests: 10, total length: 203
[MASTER] 07:43:25.418 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.984615384615385 - fitness:4.718371837183718 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:25.418 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.718371837183718, number of tests: 10, total length: 198
[MASTER] 07:43:26.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.989690721649485 - fitness:4.717022844509948 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:43:26.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.717022844509948, number of tests: 10, total length: 200
[MASTER] 07:43:26.251 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.96551724137931 - fitness:4.474654377880185 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:26.251 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.474654377880185, number of tests: 10, total length: 205
[MASTER] 07:43:27.631 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.982456140350877 - fitness:4.47072599531616 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 07:43:27.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.47072599531616, number of tests: 10, total length: 205
[MASTER] 07:43:28.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.989690721649485 - fitness:4.4690508940852816 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 07:43:28.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.4690508940852816, number of tests: 11, total length: 259
[MASTER] 07:43:28.366 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.992248062015504 - fitness:4.468459152016546 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 07:43:28.366 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.468459152016546, number of tests: 11, total length: 208
[MASTER] 07:43:28.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.992248062015504 - fitness:4.251575375666505 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:43:28.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.251575375666505, number of tests: 12, total length: 213
[MASTER] 07:43:35.380 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.993788819875776 - fitness:4.25126213592233 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:35.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.25126213592233, number of tests: 9, total length: 115
[MASTER] 07:43:35.574 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.99492385786802 - fitness:4.251031418597271 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:35.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.251031418597271, number of tests: 10, total length: 123
[MASTER] 07:43:36.198 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.990099009900991 - fitness:4.0606060606060606 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:43:36.199 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0606060606060606, number of tests: 10, total length: 136
[MASTER] 07:43:36.619 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.991735537190083 - fitness:4.060311284046692 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:36.619 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.060311284046692, number of tests: 9, total length: 129
[MASTER] 07:43:37.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.996047430830039 - fitness:4.05953488372093 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:37.977 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.05953488372093, number of tests: 8, total length: 120
[MASTER] 07:43:39.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.996539792387543 - fitness:4.059446254071661 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:39.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.059446254071661, number of tests: 7, total length: 97
[MASTER] 07:43:41.958 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.996539792387544 - fitness:3.889444337627379 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:43:41.958 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.889444337627379, number of tests: 8, total length: 120
[MASTER] 07:43:49.054 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.996835443037973 - fitness:3.8893968700545103 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:49.054 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8893968700545103, number of tests: 5, total length: 85
[MASTER] 07:43:49.617 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.997229916897506 - fitness:3.8893335385562566 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:49.617 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8893335385562566, number of tests: 5, total length: 85
[MASTER] 07:43:51.092 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.997229916897506 - fitness:3.73724117818606 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:51.092 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.73724117818606, number of tests: 5, total length: 86
[MASTER] 07:43:59.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.997229916897506 - fitness:3.600360160687076 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:43:59.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.600360160687076, number of tests: 5, total length: 78
[MASTER] 07:44:07.371 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.997481108312343 - fitness:3.476487523992322 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:07.371 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.476487523992322, number of tests: 5, total length: 74
[MASTER] 07:44:09.183 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.997840172786177 - fitness:3.476445175889735 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:09.183 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.476445175889735, number of tests: 5, total length: 74
[MASTER] 07:44:09.917 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.998109640831757 - fitness:3.4764133957508103 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:09.917 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4764133957508103, number of tests: 5, total length: 74
[MASTER] 07:44:10.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.998109640831757 - fitness:3.3638394775285727 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:10.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.3638394775285727, number of tests: 5, total length: 78
[MASTER] 07:44:21.806 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.998217468805706 - fitness:3.363827890770602 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:21.806 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.363827890770602, number of tests: 5, total length: 99
[MASTER] 07:44:22.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.998109640831757 - fitness:3.261055400295907 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:22.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.261055400295907, number of tests: 5, total length: 78
[MASTER] 07:44:25.833 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.99865410497981 - fitness:3.2610018726591763 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:25.833 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2610018726591763, number of tests: 5, total length: 77
[MASTER] 07:44:25.913 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.997229916897506 - fitness:3.1669167724806653 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:44:25.913 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1669167724806653, number of tests: 5, total length: 88
[MASTER] 07:44:28.982 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.996835443037973 - fitness:3.0802633244714523 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:44:28.983 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0802633244714523, number of tests: 5, total length: 101
[MASTER] 07:44:29.411 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.997229916897506 - fitness:3.080230496453901 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:44:29.411 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.080230496453901, number of tests: 6, total length: 130
[MASTER] 07:44:32.046 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.997229916897506 - fitness:3.000213106020245 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:44:32.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.000213106020245, number of tests: 5, total length: 103
[MASTER] 07:44:56.949 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.997229916897506 - fitness:2.926123537861687 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:44:56.949 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926123537861687, number of tests: 4, total length: 59
[MASTER] 07:45:00.316 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.997732426303855 - fitness:2.9260876868805643 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:45:00.316 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.9260876868805643, number of tests: 3, total length: 55
[MASTER] 07:45:10.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.997732426303855 - fitness:2.8572932696201505 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:45:10.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8572932696201505, number of tests: 3, total length: 54
[MASTER] 07:46:20.936 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.997409326424872 - fitness:2.793263646922183 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:46:20.936 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793263646922183, number of tests: 3, total length: 53
[MASTER] 07:46:24.202 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.997732426303855 - fitness:2.793243665936816 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:46:24.202 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793243665936816, number of tests: 4, total length: 87
[MASTER] 07:46:27.766 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.997409326424872 - fitness:2.733483029622593 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:46:27.766 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.733483029622593, number of tests: 5, total length: 116
[MASTER] 07:46:35.715 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.997732426303855 - fitness:2.733464358606093 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:46:35.715 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.733464358606093, number of tests: 5, total length: 116
[MASTER] 07:46:46.087 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.997409326424872 - fitness:2.6775595486836608 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:46:46.087 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6775595486836608, number of tests: 5, total length: 108
[MASTER] 07:46:53.390 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.997732426303855 - fitness:2.6775420629114848 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 07:46:53.390 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6775420629114848, number of tests: 5, total length: 107
[MASTER] 07:48:13.497 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 484 generations, 478842 statements, best individuals have fitness: 2.6775420629114848
[MASTER] 07:48:13.501 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 23
* Number of covered goals: 23
* Generated 4 tests with total length 80
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                      3 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 07:48:13.849 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:48:13.867 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 74 
[MASTER] 07:48:13.922 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 07:48:13.922 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:48:13.922 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:48:13.922 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:48:13.922 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:48:13.922 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 07:48:13.923 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
