* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 15045
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:53:24.971 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:53:24.977 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 23
* Using seed 1593755602179
* Starting evolution
[MASTER] 07:53:26.221 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:86.0 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 2
[MASTER] 07:53:26.221 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.0, number of tests: 1, total length: 10
[MASTER] 07:53:26.371 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 07:53:26.372 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 4, total length: 93
[MASTER] 07:53:26.724 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.989010989010989 - fitness:21.508940852819805 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 0
[MASTER] 07:53:26.724 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.508940852819805, number of tests: 2, total length: 45
[MASTER] 07:53:27.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9411764705882355 - fitness:11.523809523809524 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 07:53:27.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.523809523809524, number of tests: 10, total length: 311
[MASTER] 07:53:27.891 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.989010989010989 - fitness:10.508940852819807 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 6
[MASTER] 07:53:27.891 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.508940852819807, number of tests: 5, total length: 105
[MASTER] 07:53:29.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.990566037735849 - fitness:10.507674144037779 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 6
[MASTER] 07:53:29.115 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.507674144037779, number of tests: 5, total length: 109
[MASTER] 07:53:29.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.989010989010989 - fitness:9.508940852819807 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 6
[MASTER] 07:53:29.270 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.508940852819807, number of tests: 5, total length: 106
[MASTER] 07:53:30.035 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.990566037735849 - fitness:9.507674144037779 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 6
[MASTER] 07:53:30.036 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.507674144037779, number of tests: 5, total length: 130
[MASTER] 07:53:30.521 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.989010989010989 - fitness:8.508940852819807 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 07:53:30.521 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.508940852819807, number of tests: 6, total length: 121
[MASTER] 07:53:31.101 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.991735537190083 - fitness:7.783088235294118 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 07:53:31.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.783088235294118, number of tests: 6, total length: 146
[MASTER] 07:53:32.920 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.991735537190083 - fitness:7.204301075268817 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 07:53:32.920 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.204301075268817, number of tests: 7, total length: 171
[MASTER] 07:53:34.246 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.99056603773585 - fitness:6.731330472103004 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 07:53:34.246 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.731330472103004, number of tests: 5, total length: 102
[MASTER] 07:53:35.265 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.99056603773585 - fitness:5.731330472103004 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:53:35.265 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.731330472103004, number of tests: 5, total length: 99
[MASTER] 07:53:35.645 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.991735537190083 - fitness:5.730827067669173 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:53:35.645 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.730827067669173, number of tests: 5, total length: 98
[MASTER] 07:53:36.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.996539792387543 - fitness:5.728760226557584 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:53:36.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.728760226557584, number of tests: 6, total length: 113
[MASTER] 07:53:36.574 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.99672131147541 - fitness:5.728682170542636 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:53:36.574 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.728682170542636, number of tests: 5, total length: 97
[MASTER] 07:53:39.589 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.99740932642487 - fitness:5.728386336866903 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:53:39.589 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.728386336866903, number of tests: 5, total length: 79
[MASTER] 07:53:40.147 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.997732426303855 - fitness:5.728247422680412 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:53:40.147 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.728247422680412, number of tests: 5, total length: 80
[MASTER] 07:53:42.228 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.997732426303855 - fitness:5.334152334152334 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:53:42.228 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.334152334152334, number of tests: 5, total length: 89
[MASTER] 07:53:48.019 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.997732426303855 - fitness:5.000697836706211 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:53:48.019 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.000697836706211, number of tests: 4, total length: 75
[MASTER] 07:53:54.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.997732426303855 - fitness:4.714887412927264 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:53:54.449 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.714887412927264, number of tests: 4, total length: 70
[MASTER] 07:53:59.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.998787878787878 - fitness:4.714607325309551 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:53:59.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.714607325309551, number of tests: 4, total length: 69
[MASTER] 07:54:03.735 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.998019801980199 - fitness:4.467124372854503 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:03.735 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.467124372854503, number of tests: 4, total length: 67
[MASTER] 07:54:06.746 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.997737556561086 - fitness:4.250459623815585 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:54:06.746 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.250459623815585, number of tests: 5, total length: 116
[MASTER] 07:54:13.748 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.998019801980199 - fitness:4.250402277509592 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:13.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.250402277509592, number of tests: 4, total length: 69
[MASTER] 07:54:20.774 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.997361477572559 - fitness:4.059298354548277 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:20.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.059298354548277, number of tests: 4, total length: 81
[MASTER] 07:54:25.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.998019801980199 - fitness:4.059179869524697 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:25.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.059179869524697, number of tests: 4, total length: 62
[MASTER] 07:54:36.537 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.9980198019802 - fitness:3.889206733414017 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:54:36.537 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.889206733414017, number of tests: 5, total length: 87
[MASTER] 07:54:40.685 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.98901098901099 - fitness:3.7384259259259256 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:40.685 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.7384259259259256, number of tests: 4, total length: 58
[MASTER] 07:54:41.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.99056603773585 - fitness:3.7382016890213614 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:41.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.7382016890213614, number of tests: 4, total length: 58
[MASTER] 07:54:41.465 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.99173553719008 - fitness:3.738033072236728 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:54:41.465 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.738033072236728, number of tests: 4, total length: 59
[MASTER] 07:54:44.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.99173553719008 - fitness:3.601074824307565 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:44.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.601074824307565, number of tests: 4, total length: 72
[MASTER] 07:54:45.632 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.986842105263158 - fitness:3.4777429467084637 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:45.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4777429467084637, number of tests: 4, total length: 58
[MASTER] 07:54:46.061 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.99056603773585 - fitness:3.4773033707865166 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:46.061 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4773033707865166, number of tests: 4, total length: 58
[MASTER] 07:54:46.077 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.99173553719008 - fitness:3.4771653543307086 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:46.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4771653543307086, number of tests: 4, total length: 57
[MASTER] 07:54:46.504 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.99290780141844 - fitness:3.477027027027027 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:46.504 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.477027027027027, number of tests: 5, total length: 86
[MASTER] 07:54:47.232 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.99337748344371 - fitness:3.4769716088328075 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:47.232 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4769716088328075, number of tests: 5, total length: 86
[MASTER] 07:54:47.712 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.993788819875775 - fitness:3.476923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:47.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.476923076923077, number of tests: 5, total length: 87
[MASTER] 07:54:47.755 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.99290780141844 - fitness:3.3643985811028703 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:47.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.3643985811028703, number of tests: 5, total length: 84
[MASTER] 07:54:49.005 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.993788819875775 - fitness:3.3643038689635696 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:49.005 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.3643038689635696, number of tests: 5, total length: 84
[MASTER] 07:54:51.413 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.99755501222494 - fitness:3.363899077470268 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:51.413 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.363899077470268, number of tests: 5, total length: 79
[MASTER] 07:54:53.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.997772828507795 - fitness:3.363875670750228 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:53.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.363875670750228, number of tests: 5, total length: 78
[MASTER] 07:54:53.242 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.9979035639413 - fitness:3.363861622033737 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:53.242 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.363861622033737, number of tests: 5, total length: 78
[MASTER] 07:54:54.204 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.998165137614677 - fitness:3.36383351405455 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:54:54.204 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.36383351405455, number of tests: 5, total length: 75
[MASTER] 07:55:03.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.9979035639413 - fitness:3.2610756608933453 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:03.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2610756608933453, number of tests: 5, total length: 65
[MASTER] 07:55:03.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.998165137614677 - fitness:3.261049944151907 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:03.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.261049944151907, number of tests: 5, total length: 66
[MASTER] 07:55:10.585 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.99706744868035 - fitness:3.166931443235977 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:10.585 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.166931443235977, number of tests: 5, total length: 63
[MASTER] 07:55:13.238 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.998165137614677 - fitness:3.1668323266304768 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:13.238 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1668323266304768, number of tests: 5, total length: 63
[MASTER] 07:55:15.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.998165137614677 - fitness:3.0801526717557257 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:15.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0801526717557257, number of tests: 5, total length: 95
[MASTER] 07:55:21.380 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.9979035639413 - fitness:3.0001612773163453 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:21.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0001612773163453, number of tests: 4, total length: 51
[MASTER] 07:55:23.581 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.998165137614677 - fitness:2.9260568166372165 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:23.581 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.9260568166372165, number of tests: 4, total length: 54
[MASTER] 07:55:25.128 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.998322147651006 - fitness:2.9260456155614945 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:25.128 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.9260456155614945, number of tests: 5, total length: 58
[MASTER] 07:55:25.810 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.998165137614677 - fitness:2.857264565174651 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:25.810 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.857264565174651, number of tests: 4, total length: 53
[MASTER] 07:55:30.089 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.99755501222494 - fitness:2.793254637436762 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:30.089 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793254637436762, number of tests: 5, total length: 58
[MASTER] 07:55:31.871 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.9979035639413 - fitness:2.793233082706767 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:31.871 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793233082706767, number of tests: 4, total length: 54
[MASTER] 07:55:34.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.998165137614677 - fitness:2.793216907112124 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:34.417 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793216907112124, number of tests: 4, total length: 55
[MASTER] 07:55:53.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.9979035639413 - fitness:2.733454469215179 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:53.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.733454469215179, number of tests: 4, total length: 53
[MASTER] 07:55:55.377 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.998165137614677 - fitness:2.7334393540889352 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:55:55.377 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7334393540889352, number of tests: 4, total length: 55
[MASTER] 07:56:07.913 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.998165137614677 - fitness:2.67751864567302 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 07:56:07.913 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.67751864567302, number of tests: 5, total length: 62
[MASTER] 07:57:27.407 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.9979035639413 - fitness:2.62510646661862 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:57:27.407 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.62510646661862, number of tests: 4, total length: 53
[MASTER] 07:57:32.814 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.998165137614677 - fitness:2.625093181948506 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:57:32.814 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.625093181948506, number of tests: 4, total length: 53
[MASTER] 07:57:51.986 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.998165137614677 - fitness:2.5758451957295376 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 07:57:51.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5758451957295376, number of tests: 4, total length: 62
[MASTER] 07:58:26.028 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 829 generations, 611306 statements, best individuals have fitness: 2.5758451957295376
[MASTER] 07:58:26.032 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 23
* Number of covered goals: 23
* Generated 4 tests with total length 53
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                      3 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 07:58:26.381 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:58:26.404 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 50 
[MASTER] 07:58:26.453 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 07:58:26.454 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 07:58:26.454 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:58:26.454 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:58:26.454 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:58:26.454 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:58:26.454 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 07:58:26.456 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
