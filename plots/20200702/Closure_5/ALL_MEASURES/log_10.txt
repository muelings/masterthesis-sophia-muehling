* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.javascript.jscomp.InlineObjectLiterals
* Starting client
* Connecting to master process on port 12284
* Analyzing classpath: 
  - ../defects4j_compiled/Closure_5_fixed/build/classes
* Finished analyzing classpath
* Generating tests for class com.google.javascript.jscomp.InlineObjectLiterals
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 20:23:34.386 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 20:23:34.392 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 126
* Using seed 1593627810106
* Starting evolution
[MASTER] 20:23:34.505 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:275.0 - branchDistance:0.0 - coverage:137.0 - ex: 0 - tex: 0
[MASTER] 20:23:34.505 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 275.0, number of tests: 10, total length: 0

[MASTER] 20:28:35.401 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 135259 generations, 0 statements, best individuals have fitness: 275.0
[MASTER] 20:28:35.404 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 2
* Number of covered goals: 0
* Generated 5 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                    275 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 20:28:36.036 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 20:28:36.073 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 20:28:36.084 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 20:28:36.084 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 20:28:36.084 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 20:28:36.084 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 20:28:36.084 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
* Going to analyze the coverage criteria
[MASTER] 20:28:36.084 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Coverage analysis for criterion REGRESSION
[MASTER] 20:28:36.085 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 2
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'InlineObjectLiterals_ESTest' to evosuite-tests
* Done!

* Computation finished
