* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 16196
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:04:39.725 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:04:39.730 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593655477754
* Starting evolution
[MASTER] 04:04:41.265 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:113.0 - branchDistance:0.0 - coverage:50.0 - ex: 0 - tex: 18
[MASTER] 04:04:41.265 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.0, number of tests: 10, total length: 289
[MASTER] 04:04:41.377 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:109.0 - branchDistance:0.0 - coverage:46.0 - ex: 0 - tex: 4
[MASTER] 04:04:41.377 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.0, number of tests: 2, total length: 56
[MASTER] 04:04:42.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:101.66666666666666 - branchDistance:0.0 - coverage:38.666666666666664 - ex: 0 - tex: 12
[MASTER] 04:04:42.324 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.66666666666666, number of tests: 8, total length: 207
[MASTER] 04:04:42.805 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.26666666666667 - branchDistance:0.0 - coverage:30.266666666666666 - ex: 0 - tex: 12
[MASTER] 04:04:42.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.26666666666667, number of tests: 10, total length: 196
[MASTER] 04:04:44.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.65378908320085 - branchDistance:0.0 - coverage:20.65378908320085 - ex: 0 - tex: 8
[MASTER] 04:04:44.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.65378908320085, number of tests: 5, total length: 135
[MASTER] 04:04:53.948 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.15378908320085 - branchDistance:0.0 - coverage:20.15378908320085 - ex: 0 - tex: 8
[MASTER] 04:04:53.948 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.15378908320085, number of tests: 5, total length: 140
[MASTER] 04:04:55.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.95411140583555 - branchDistance:0.0 - coverage:19.954111405835544 - ex: 0 - tex: 6
[MASTER] 04:04:55.576 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.95411140583555, number of tests: 8, total length: 182
[MASTER] 04:04:58.188 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.64332700822264 - branchDistance:0.0 - coverage:19.643327008222645 - ex: 0 - tex: 4
[MASTER] 04:04:58.188 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.64332700822264, number of tests: 6, total length: 156
[MASTER] 04:04:58.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.15378908320085 - branchDistance:0.0 - coverage:19.15378908320085 - ex: 0 - tex: 6
[MASTER] 04:04:58.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.15378908320085, number of tests: 5, total length: 125
[MASTER] 04:04:58.865 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.97666034155597 - branchDistance:0.0 - coverage:18.976660341555977 - ex: 0 - tex: 6
[MASTER] 04:04:58.865 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.97666034155597, number of tests: 6, total length: 185
[MASTER] 04:05:03.110 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.95411140583555 - branchDistance:0.0 - coverage:18.954111405835544 - ex: 0 - tex: 6
[MASTER] 04:05:03.110 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.95411140583555, number of tests: 7, total length: 192
[MASTER] 04:05:16.854 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 4
[MASTER] 04:05:16.854 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 7, total length: 150

[MASTER] 04:09:40.746 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 207 generations, 170344 statements, best individuals have fitness: 81.91304347826087
[MASTER] 04:09:40.750 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 4 tests with total length 33
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     82 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:09:41.135 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:09:41.269 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 27 
[MASTER] 04:09:41.309 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 04:09:41.309 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 04:09:41.309 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 04:09:41.309 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 04:09:41.309 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 04:09:41.309 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 04:09:41.310 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
