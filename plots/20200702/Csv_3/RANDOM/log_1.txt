* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.Lexer
* Starting client
* Connecting to master process on port 13548
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.Lexer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 05:39:29.763 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:39:30.240 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var10);  // (Primitive) Original Value: -1 | Regression Value: 92
[MASTER] 05:39:30.257 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 05:39:30.257 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:39:31.384 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/Lexer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:39:31.422 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var10);  // (Primitive) Original Value: -1 | Regression Value: 92
[MASTER] 05:39:31.425 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Generated test with 1 assertions.
[MASTER] 05:39:31.425 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:39:31.750 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/Lexer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:39:31.770 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var10);  // (Primitive) Original Value: -1 | Regression Value: 92
[MASTER] 05:39:31.772 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 05:39:31.772 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 8 | Tests with assertion: 1
* Generated 1 tests with total length 16
* GA-Budget:
	- RMIStoppingCondition
[MASTER] 05:39:31.772 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- MaxTime :                          2 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 05:39:32.257 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:39:32.275 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 05:39:32.304 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 05:39:32.560 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:39:32.565 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 33%
* Total number of goals: 70
* Number of covered goals: 23
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Lexer_ESTest' to evosuite-tests
* Done!

* Computation finished
