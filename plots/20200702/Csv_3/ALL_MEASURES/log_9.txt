* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.Lexer
* Starting client
* Connecting to master process on port 9619
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.Lexer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:21:37.487 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:21:37.492 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 70
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593663695450
* Starting evolution
[MASTER] 06:21:38.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.98989898989899 - fitness:119.10681278180513 - branchDistance:0.0 - coverage:79.50934442737474 - ex: 0 - tex: 8
[MASTER] 06:21:38.903 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.10681278180513, number of tests: 4, total length: 77
[MASTER] 06:21:43.696 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9859154929577465 - fitness:109.67816481997212 - branchDistance:0.0 - coverage:57.10269312185892 - ex: 0 - tex: 16
[MASTER] 06:21:43.696 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.67816481997212, number of tests: 10, total length: 210
[MASTER] 06:21:44.168 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.99009900990099 - fitness:108.3481727479932 - branchDistance:0.0 - coverage:55.84486148971506 - ex: 0 - tex: 12
[MASTER] 06:21:44.168 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.3481727479932, number of tests: 8, total length: 188
[MASTER] 06:21:46.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9743589743589745 - fitness:106.38404806105757 - branchDistance:0.0 - coverage:53.60818599209206 - ex: 0 - tex: 10
[MASTER] 06:21:46.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.38404806105757, number of tests: 7, total length: 186
[MASTER] 06:21:49.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.991150442477876 - fitness:99.92825793292724 - branchDistance:0.0 - coverage:47.443050832335516 - ex: 0 - tex: 12
[MASTER] 06:21:49.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.92825793292724, number of tests: 9, total length: 198
[MASTER] 06:21:51.303 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.991150442477876 - fitness:99.38294824589839 - branchDistance:0.0 - coverage:46.89774114530667 - ex: 0 - tex: 10
[MASTER] 06:21:51.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.38294824589839, number of tests: 6, total length: 150
[MASTER] 06:21:52.314 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.99009900990099 - fitness:97.02753553388075 - branchDistance:0.0 - coverage:44.524224275602606 - ex: 0 - tex: 10
[MASTER] 06:21:52.314 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.02753553388075, number of tests: 7, total length: 170
[MASTER] 06:21:52.510 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.99009900990099 - fitness:94.99415286909351 - branchDistance:0.0 - coverage:55.3986193703342 - ex: 0 - tex: 10
[MASTER] 06:21:52.510 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.99415286909351, number of tests: 8, total length: 213
[MASTER] 06:21:55.083 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9855072463768115 - fitness:90.4868720188885 - branchDistance:0.0 - coverage:50.846872018888504 - ex: 0 - tex: 14
[MASTER] 06:21:55.083 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.4868720188885, number of tests: 9, total length: 232
[MASTER] 06:21:55.296 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.99009900990099 - fitness:90.17937838551461 - branchDistance:0.0 - coverage:50.58384488675531 - ex: 0 - tex: 12
[MASTER] 06:21:55.296 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.17937838551461, number of tests: 8, total length: 189
[MASTER] 06:21:55.776 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.99009900990099 - fitness:89.5158158147978 - branchDistance:0.0 - coverage:49.92028231603851 - ex: 0 - tex: 12
[MASTER] 06:21:55.776 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.5158158147978, number of tests: 8, total length: 178
[MASTER] 06:21:57.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.99009900990099 - fitness:84.19115577456718 - branchDistance:0.0 - coverage:44.59562227580788 - ex: 0 - tex: 12
[MASTER] 06:21:57.678 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.19115577456718, number of tests: 8, total length: 242
[MASTER] 06:21:58.719 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.99009900990099 - fitness:84.04832920293333 - branchDistance:0.0 - coverage:44.45279570417404 - ex: 0 - tex: 10
[MASTER] 06:21:58.719 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.04832920293333, number of tests: 7, total length: 192
[MASTER] 06:21:58.924 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.99009900990099 - fitness:83.84179219616115 - branchDistance:0.0 - coverage:44.24625869740184 - ex: 0 - tex: 10
[MASTER] 06:21:58.924 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.84179219616115, number of tests: 7, total length: 191
[MASTER] 06:22:00.076 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.411739241151006 - fitness:69.5426682902339 - branchDistance:0.0 - coverage:44.524224275602606 - ex: 0 - tex: 10
[MASTER] 06:22:00.076 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.5426682902339, number of tests: 7, total length: 168
[MASTER] 06:22:01.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.260839018429895 - fitness:67.54944514821022 - branchDistance:0.0 - coverage:49.92028231603851 - ex: 0 - tex: 12
[MASTER] 06:22:01.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.54944514821022, number of tests: 8, total length: 179
[MASTER] 06:22:02.488 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.260839018429895 - fitness:62.08195853634575 - branchDistance:0.0 - coverage:44.45279570417404 - ex: 0 - tex: 10
[MASTER] 06:22:02.488 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.08195853634575, number of tests: 7, total length: 195
[MASTER] 06:22:05.460 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.260839018429895 - fitness:62.08194256015047 - branchDistance:0.0 - coverage:44.45277972797876 - ex: 0 - tex: 10
[MASTER] 06:22:05.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.08194256015047, number of tests: 7, total length: 195
[MASTER] 06:22:05.658 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.260839018429895 - fitness:61.87542152957356 - branchDistance:0.0 - coverage:44.24625869740184 - ex: 0 - tex: 10
[MASTER] 06:22:05.659 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.87542152957356, number of tests: 7, total length: 192
[MASTER] 06:22:08.170 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.260839018429895 - fitness:61.3570369670837 - branchDistance:0.0 - coverage:43.727874134911985 - ex: 0 - tex: 14
[MASTER] 06:22:08.170 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.3570369670837, number of tests: 9, total length: 231
[MASTER] 06:22:12.009 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.260839018429895 - fitness:59.881958536345756 - branchDistance:0.0 - coverage:42.25279570417404 - ex: 0 - tex: 14
[MASTER] 06:22:12.009 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.881958536345756, number of tests: 9, total length: 240
[MASTER] 06:22:12.425 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.841897971309736 - fitness:59.041929186311734 - branchDistance:0.0 - coverage:42.39454080157866 - ex: 0 - tex: 18
[MASTER] 06:22:12.425 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.041929186311734, number of tests: 12, total length: 329
[MASTER] 06:22:15.942 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.841897971309736 - fitness:58.69364708213492 - branchDistance:0.0 - coverage:42.046258697401846 - ex: 0 - tex: 16
[MASTER] 06:22:15.942 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.69364708213492, number of tests: 11, total length: 298
[MASTER] 06:22:17.192 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.260839018429895 - fitness:56.231847153308436 - branchDistance:0.0 - coverage:38.60268432113672 - ex: 0 - tex: 16
[MASTER] 06:22:17.193 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.231847153308436, number of tests: 10, total length: 290
[MASTER] 06:22:19.964 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.841897971309736 - fitness:55.2650756535635 - branchDistance:0.0 - coverage:38.61768726883042 - ex: 0 - tex: 14
[MASTER] 06:22:19.964 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.2650756535635, number of tests: 10, total length: 284
[MASTER] 06:22:20.923 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:55.239688463277844 - branchDistance:0.0 - coverage:42.39454080157866 - ex: 0 - tex: 22
[MASTER] 06:22:20.924 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.239688463277844, number of tests: 13, total length: 369
[MASTER] 06:22:23.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.833110208892942 - fitness:55.20763101281882 - branchDistance:0.0 - coverage:38.546258697401846 - ex: 0 - tex: 14
[MASTER] 06:22:23.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.20763101281882, number of tests: 10, total length: 290
[MASTER] 06:22:24.111 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:54.89140635910104 - branchDistance:0.0 - coverage:42.046258697401846 - ex: 0 - tex: 18
[MASTER] 06:22:24.111 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.89140635910104, number of tests: 12, total length: 335
[MASTER] 06:22:24.492 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.841897971309736 - fitness:54.19364708213492 - branchDistance:0.0 - coverage:37.546258697401846 - ex: 0 - tex: 14
[MASTER] 06:22:24.492 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.19364708213492, number of tests: 11, total length: 309
[MASTER] 06:22:27.061 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.841897971309736 - fitness:53.67621233855703 - branchDistance:0.0 - coverage:37.02882395382395 - ex: 0 - tex: 16
[MASTER] 06:22:27.061 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.67621233855703, number of tests: 11, total length: 320
[MASTER] 06:22:27.471 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:51.73970418185249 - branchDistance:0.0 - coverage:38.894556520153294 - ex: 0 - tex: 20
[MASTER] 06:22:27.471 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.73970418185249, number of tests: 12, total length: 330
[MASTER] 06:22:28.880 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:51.73016465375403 - branchDistance:0.0 - coverage:38.885016992054844 - ex: 0 - tex: 20
[MASTER] 06:22:28.881 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.73016465375403, number of tests: 12, total length: 332
[MASTER] 06:22:29.497 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:51.656370848519146 - branchDistance:0.0 - coverage:38.81122318681996 - ex: 0 - tex: 20
[MASTER] 06:22:29.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.656370848519146, number of tests: 12, total length: 330
[MASTER] 06:22:29.780 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:51.46283493052961 - branchDistance:0.0 - coverage:38.61768726883042 - ex: 0 - tex: 20
[MASTER] 06:22:29.780 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.46283493052961, number of tests: 12, total length: 340
[MASTER] 06:22:29.808 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:50.39140635910104 - branchDistance:0.0 - coverage:37.546258697401846 - ex: 0 - tex: 16
[MASTER] 06:22:29.808 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.39140635910104, number of tests: 11, total length: 315
[MASTER] 06:22:32.255 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:49.873971615523146 - branchDistance:0.0 - coverage:37.02882395382395 - ex: 0 - tex: 20
[MASTER] 06:22:32.255 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.873971615523146, number of tests: 12, total length: 347
[MASTER] 06:22:33.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:49.39140635910104 - branchDistance:0.0 - coverage:36.546258697401846 - ex: 0 - tex: 18
[MASTER] 06:22:33.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.39140635910104, number of tests: 12, total length: 331
[MASTER] 06:22:34.894 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:48.873971615523146 - branchDistance:0.0 - coverage:36.02882395382395 - ex: 0 - tex: 18
[MASTER] 06:22:34.894 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.873971615523146, number of tests: 11, total length: 333
[MASTER] 06:22:38.786 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:48.85449109604262 - branchDistance:0.0 - coverage:36.009343434343435 - ex: 0 - tex: 18
[MASTER] 06:22:38.786 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.85449109604262, number of tests: 11, total length: 319
[MASTER] 06:22:40.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:47.873971615523146 - branchDistance:0.0 - coverage:35.02882395382395 - ex: 0 - tex: 16
[MASTER] 06:22:40.551 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.873971615523146, number of tests: 11, total length: 344
[MASTER] 06:22:41.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:47.85449109604262 - branchDistance:0.0 - coverage:35.009343434343435 - ex: 0 - tex: 20
[MASTER] 06:22:41.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.85449109604262, number of tests: 12, total length: 335
[MASTER] 06:22:50.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:47.771157762709294 - branchDistance:0.0 - coverage:34.9260101010101 - ex: 0 - tex: 18
[MASTER] 06:22:50.784 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.771157762709294, number of tests: 13, total length: 360
[MASTER] 06:22:52.354 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:45.19060220715373 - branchDistance:0.0 - coverage:32.345454545454544 - ex: 0 - tex: 16
[MASTER] 06:22:52.355 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.19060220715373, number of tests: 12, total length: 305
[MASTER] 06:22:56.889 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:45.1072688738204 - branchDistance:0.0 - coverage:32.262121212121215 - ex: 0 - tex: 20
[MASTER] 06:22:56.889 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.1072688738204, number of tests: 13, total length: 357
[MASTER] 06:23:02.435 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:40.1486108651624 - branchDistance:0.0 - coverage:27.303463203463206 - ex: 0 - tex: 16
[MASTER] 06:23:02.435 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.1486108651624, number of tests: 12, total length: 305
[MASTER] 06:23:05.614 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:40.06527753182907 - branchDistance:0.0 - coverage:27.220129870129874 - ex: 0 - tex: 16
[MASTER] 06:23:05.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.06527753182907, number of tests: 12, total length: 309
[MASTER] 06:23:13.454 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:39.66809138464292 - branchDistance:0.0 - coverage:26.822943722943727 - ex: 0 - tex: 18
[MASTER] 06:23:13.454 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.66809138464292, number of tests: 13, total length: 314
[MASTER] 06:23:13.462 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:39.64384896040049 - branchDistance:0.0 - coverage:26.7987012987013 - ex: 0 - tex: 18
[MASTER] 06:23:13.462 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.64384896040049, number of tests: 13, total length: 336
[MASTER] 06:23:19.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:39.22761519416673 - branchDistance:0.0 - coverage:26.382467532467537 - ex: 0 - tex: 16
[MASTER] 06:23:19.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.22761519416673, number of tests: 13, total length: 334
[MASTER] 06:23:20.005 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:38.70229051884205 - branchDistance:0.0 - coverage:25.857142857142858 - ex: 0 - tex: 16
[MASTER] 06:23:20.005 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.70229051884205, number of tests: 13, total length: 323
[MASTER] 06:23:22.153 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:38.663329479881014 - branchDistance:0.0 - coverage:25.81818181818182 - ex: 0 - tex: 16
[MASTER] 06:23:22.153 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.663329479881014, number of tests: 13, total length: 319
[MASTER] 06:23:22.698 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:38.4594333759849 - branchDistance:0.0 - coverage:25.614285714285714 - ex: 0 - tex: 16
[MASTER] 06:23:22.698 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.4594333759849, number of tests: 13, total length: 314
[MASTER] 06:23:24.594 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:38.33042904698057 - branchDistance:0.0 - coverage:25.485281385281382 - ex: 0 - tex: 16
[MASTER] 06:23:24.594 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.33042904698057, number of tests: 13, total length: 312
[MASTER] 06:23:24.659 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:36.49709571364724 - branchDistance:0.0 - coverage:23.651948051948054 - ex: 0 - tex: 18
[MASTER] 06:23:24.659 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.49709571364724, number of tests: 13, total length: 333
[MASTER] 06:23:29.702 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:36.23519095174248 - branchDistance:0.0 - coverage:23.390043290043288 - ex: 0 - tex: 20
[MASTER] 06:23:29.702 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.23519095174248, number of tests: 14, total length: 362
[MASTER] 06:23:34.598 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:35.46823324728478 - branchDistance:0.0 - coverage:22.62308558558559 - ex: 0 - tex: 18
[MASTER] 06:23:34.598 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.46823324728478, number of tests: 12, total length: 334
[MASTER] 06:23:38.135 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:35.40857415637569 - branchDistance:0.0 - coverage:22.5634264946765 - ex: 0 - tex: 20
[MASTER] 06:23:38.135 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.40857415637569, number of tests: 13, total length: 344
[MASTER] 06:23:40.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:34.39014663794816 - branchDistance:0.0 - coverage:21.544998976248976 - ex: 0 - tex: 22
[MASTER] 06:23:40.406 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.39014663794816, number of tests: 14, total length: 379
[MASTER] 06:23:50.227 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:34.0562587728103 - branchDistance:0.0 - coverage:21.211111111111112 - ex: 0 - tex: 20
[MASTER] 06:23:50.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.0562587728103, number of tests: 14, total length: 385
[MASTER] 06:23:59.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:33.5562587728103 - branchDistance:0.0 - coverage:20.711111111111112 - ex: 0 - tex: 20
[MASTER] 06:23:59.363 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.5562587728103, number of tests: 14, total length: 363
[MASTER] 06:24:06.337 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:33.38959210614364 - branchDistance:0.0 - coverage:20.544444444444444 - ex: 0 - tex: 20
[MASTER] 06:24:06.337 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.38959210614364, number of tests: 15, total length: 368
[MASTER] 06:24:12.224 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:32.278480995032524 - branchDistance:0.0 - coverage:19.433333333333334 - ex: 0 - tex: 18
[MASTER] 06:24:12.224 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.278480995032524, number of tests: 13, total length: 336
[MASTER] 06:24:15.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:31.778480995032524 - branchDistance:0.0 - coverage:18.933333333333334 - ex: 0 - tex: 22
[MASTER] 06:24:15.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.778480995032524, number of tests: 15, total length: 412
[MASTER] 06:24:28.334 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:30.000703217254745 - branchDistance:0.0 - coverage:17.155555555555555 - ex: 0 - tex: 20
[MASTER] 06:24:28.335 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.000703217254745, number of tests: 14, total length: 355
[MASTER] 06:24:35.276 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:29.778480995032524 - branchDistance:0.0 - coverage:16.933333333333334 - ex: 0 - tex: 18
[MASTER] 06:24:35.277 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.778480995032524, number of tests: 13, total length: 336
[MASTER] 06:24:48.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.001104283229227 - fitness:28.445147661699192 - branchDistance:0.0 - coverage:15.600000000000001 - ex: 0 - tex: 14
[MASTER] 06:24:48.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.445147661699192, number of tests: 14, total length: 310
[MASTER] 06:24:51.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.1974282068852 - fitness:24.797646048028376 - branchDistance:0.0 - coverage:17.433333333333334 - ex: 0 - tex: 16
[MASTER] 06:24:51.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.797646048028376, number of tests: 13, total length: 307
[MASTER] 06:24:53.344 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.1974282068852 - fitness:24.297646048028376 - branchDistance:0.0 - coverage:16.933333333333334 - ex: 0 - tex: 14
[MASTER] 06:24:53.344 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.297646048028376, number of tests: 14, total length: 322
[MASTER] 06:24:56.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.1974282068852 - fitness:23.464312714695048 - branchDistance:0.0 - coverage:16.1 - ex: 0 - tex: 14
[MASTER] 06:24:56.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.464312714695048, number of tests: 14, total length: 312
[MASTER] 06:25:03.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.1974282068852 - fitness:22.964312714695048 - branchDistance:0.0 - coverage:15.600000000000001 - ex: 0 - tex: 16
[MASTER] 06:25:03.297 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.964312714695048, number of tests: 15, total length: 354
[MASTER] 06:25:07.866 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.1974282068852 - fitness:20.53097938136171 - branchDistance:0.0 - coverage:13.166666666666666 - ex: 0 - tex: 14
[MASTER] 06:25:07.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 20.53097938136171, number of tests: 14, total length: 296
[MASTER] 06:25:14.978 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.1974282068852 - fitness:20.197646048028375 - branchDistance:0.0 - coverage:12.833333333333332 - ex: 0 - tex: 12
[MASTER] 06:25:14.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 20.197646048028375, number of tests: 15, total length: 301
[MASTER] 06:25:39.866 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.1974282068852 - fitness:19.197646048028375 - branchDistance:0.0 - coverage:11.833333333333332 - ex: 0 - tex: 10
[MASTER] 06:25:39.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.197646048028375, number of tests: 14, total length: 246

[MASTER] 06:26:38.530 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 200 generations, 215552 statements, best individuals have fitness: 19.197646048028375
[MASTER] 06:26:38.533 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 81%
* Total number of goals: 70
* Number of covered goals: 57
* Generated 12 tests with total length 170
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     19 / 0           
	- RMIStoppingCondition
[MASTER] 06:26:39.176 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:26:39.399 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 131 
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 06:26:39.480 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
* Going to analyze the coverage criteria
[MASTER] 06:26:39.481 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 06:26:39.481 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 70
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Lexer_ESTest' to evosuite-tests
* Done!

* Computation finished
