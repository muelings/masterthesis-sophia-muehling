* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream
* Starting client
* Connecting to master process on port 11095
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonXml_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 140
[MASTER] 04:40:16.882 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Computation finished
