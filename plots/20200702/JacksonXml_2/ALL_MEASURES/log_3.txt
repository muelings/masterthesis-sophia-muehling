* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream
* Starting client
* Connecting to master process on port 3717
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonXml_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:40:20.997 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:40:21.004 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 140
* Using seed 1593744019061
* Starting evolution
[MASTER] 04:40:22.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:585.0 - branchDistance:0.0 - coverage:291.0 - ex: 0 - tex: 12
[MASTER] 04:40:22.093 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 585.0, number of tests: 6, total length: 131
[MASTER] 04:45:22.015 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 6104 generations, 511134 statements, best individual has fitness: 585.0
[MASTER] 04:45:22.018 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 140
* Number of covered goals: 0
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                    585 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 04:45:22.212 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:45:22.223 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 04:45:22.227 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 04:45:22.227 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 04:45:22.227 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 04:45:22.228 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 140
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XmlTokenStream_ESTest' to evosuite-tests
* Done!

* Computation finished
