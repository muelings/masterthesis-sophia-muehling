* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream
* Starting client
* Connecting to master process on port 13793
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonXml_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:35:11.181 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:35:11.185 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 140
* Using seed 1593743708927
* Starting evolution
[MASTER] 04:35:12.337 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:585.0 - branchDistance:0.0 - coverage:291.0 - ex: 0 - tex: 14
[MASTER] 04:35:12.337 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 585.0, number of tests: 7, total length: 115
[MASTER] 04:40:12.197 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 6150 generations, 510828 statements, best individual has fitness: 585.0
[MASTER] 04:40:12.200 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 140
* Number of covered goals: 0
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                    585 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 04:40:12.382 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:40:12.394 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 04:40:12.398 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 04:40:12.398 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 04:40:12.398 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 04:40:12.399 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 140
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'XmlTokenStream_ESTest' to evosuite-tests
* Done!

* Computation finished
