/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 02 03:13:18 GMT 2020
 */

package org.apache.commons.csv;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.util.HashMap;
import org.apache.commons.csv.CSVRecord;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class CSVRecord_ESTest extends CSVRecord_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      String[] stringArray0 = new String[1];
      HashMap<String, Integer> hashMap0 = new HashMap<String, Integer>();
      CSVRecord cSVRecord0 = new CSVRecord(stringArray0, hashMap0, "SE", 1823L);
      Integer integer0 = new Integer(716);
      hashMap0.put((String) null, integer0);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Index for header 'null' is 716 but CSVRecord only has 1 values!
      // Modified Version:
      //     java.lang.ArrayIndexOutOfBoundsException : 716
      // Undeclared exception!
      try { 
        cSVRecord0.get((String) null);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Index for header 'null' is 716 but CSVRecord only has 1 values!
         //
         verifyException("org.apache.commons.csv.CSVRecord", e);
         assertTrue(e.getMessage().equals("Index for header 'null' is 716 but CSVRecord only has 1 values!"));   
      }
  }
}
