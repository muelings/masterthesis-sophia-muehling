/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 02 02:57:30 GMT 2020
 */

package org.apache.commons.csv;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.util.HashMap;
import org.apache.commons.csv.CSVRecord;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class CSVRecord_ESTest extends CSVRecord_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      String[] stringArray0 = new String[0];
      HashMap<String, Integer> hashMap0 = new HashMap<String, Integer>();
      CSVRecord cSVRecord0 = new CSVRecord(stringArray0, hashMap0, "", 0L);
      Integer integer0 = new Integer((-1));
      hashMap0.put("", integer0);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Index for header '' is -1 but CSVRecord only has 0 values!
      // Modified Version:
      //     java.lang.ArrayIndexOutOfBoundsException : -1
      // Undeclared exception!
      try { 
        cSVRecord0.get("");
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Index for header '' is -1 but CSVRecord only has 0 values!
         //
         verifyException("org.apache.commons.csv.CSVRecord", e);
         assertTrue(e.getMessage().equals("Index for header '' is -1 but CSVRecord only has 0 values!"));   
      }
  }
}
