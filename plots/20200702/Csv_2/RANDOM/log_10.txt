* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 8458
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 25
[MASTER] 05:34:13.088 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:34:15.238 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:34:16.520 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVRecord_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:34:16.534 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 05:34:16.858 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVRecord_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 05:34:16.866 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 05:34:16.867 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 89 | Tests with assertion: 1
* Generated 1 tests with total length 15
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          3 / 300         
[MASTER] 05:34:17.272 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:34:17.286 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 05:34:17.303 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:34:17.303 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 05:34:17.303 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: get:MockIllegalArgumentException at 8
[MASTER] 05:34:17.303 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 05:34:17.410 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:34:17.413 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 12%
* Total number of goals: 25
* Number of covered goals: 3
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
