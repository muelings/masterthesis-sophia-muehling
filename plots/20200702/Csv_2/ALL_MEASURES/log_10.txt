* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 11704
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:34:22.578 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:34:22.583 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593660860564
* Starting evolution
[MASTER] 05:34:24.272 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:74.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 6
[MASTER] 05:34:24.272 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.0, number of tests: 9, total length: 190
[MASTER] 05:34:25.371 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.04761904761905 - branchDistance:0.0 - coverage:11.047619047619047 - ex: 0 - tex: 8
[MASTER] 05:34:25.371 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.04761904761905, number of tests: 10, total length: 261
[MASTER] 05:34:25.579 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.0 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 2
[MASTER] 05:34:25.579 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.0, number of tests: 5, total length: 141
[MASTER] 05:34:26.011 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:70.33333333333333 - branchDistance:0.0 - coverage:9.333333333333332 - ex: 0 - tex: 2
[MASTER] 05:34:26.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.33333333333333, number of tests: 8, total length: 155
[MASTER] 05:34:26.254 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:70.0 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 2
[MASTER] 05:34:26.254 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.0, number of tests: 9, total length: 153
[MASTER] 05:34:26.308 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:69.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 05:34:26.308 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.0, number of tests: 7, total length: 175
[MASTER] 05:34:26.866 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:68.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 05:34:26.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.0, number of tests: 8, total length: 152
[MASTER] 05:34:26.955 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:67.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 6
[MASTER] 05:34:26.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.0, number of tests: 9, total length: 201
[MASTER] 05:34:27.573 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 10
[MASTER] 05:34:27.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0, number of tests: 9, total length: 207
[MASTER] 05:34:28.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 8
[MASTER] 05:34:28.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0, number of tests: 9, total length: 198
[MASTER] 05:34:28.579 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 6
[MASTER] 05:34:28.579 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 9, total length: 180
[MASTER] 05:34:28.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 10
[MASTER] 05:34:28.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 10, total length: 212
[MASTER] 05:34:29.737 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 05:34:29.737 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 10, total length: 218
[MASTER] 05:34:32.160 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 05:34:32.160 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 10, total length: 210
[MASTER] 05:39:23.596 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1887 generations, 1347508 statements, best individuals have fitness: 61.0
[MASTER] 05:39:23.599 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 6 tests with total length 66
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     61 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 05:39:23.709 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:39:23.731 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 49 
[MASTER] 05:39:23.783 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 1.0
[MASTER] 05:39:23.783 [logback-1] WARN  RegressionSuiteMinimizer - Test3, uniqueExceptions: [get:MockIllegalArgumentException]
[MASTER] 05:39:23.783 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: get:MockIllegalArgumentException at 8
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 05:39:23.784 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 05:39:23.982 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:39:23.983 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 40%
* Total number of goals: 25
* Number of covered goals: 10
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
