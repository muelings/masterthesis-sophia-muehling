* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 19656
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:29:07.319 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:29:07.324 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593660545287
* Starting evolution
[MASTER] 05:29:08.351 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:121.0 - branchDistance:0.0 - coverage:60.0 - ex: 0 - tex: 2
[MASTER] 05:29:08.351 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 1, total length: 19
[MASTER] 05:29:08.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.0 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 2
[MASTER] 05:29:08.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.0, number of tests: 5, total length: 78
[MASTER] 05:29:08.967 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:74.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 8
[MASTER] 05:29:08.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.0, number of tests: 9, total length: 219
[MASTER] 05:29:09.161 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 12
[MASTER] 05:29:09.161 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0, number of tests: 10, total length: 281
[MASTER] 05:29:10.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 6
[MASTER] 05:29:10.918 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 10, total length: 254
[MASTER] 05:29:11.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 6
[MASTER] 05:29:11.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 10, total length: 256
[MASTER] 05:29:11.873 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 05:29:11.873 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 10, total length: 261
[MASTER] 05:29:16.511 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:29:16.511 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 11, total length: 284

[MASTER] 05:34:08.352 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 1759 generations, 1476648 statements, best individuals have fitness: 61.0
[MASTER] 05:34:08.361 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 4 tests with total length 45
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     61 / 0           
[MASTER] 05:34:08.502 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:34:08.524 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 33 
[MASTER] 05:34:08.571 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 05:34:08.572 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 05:34:08.572 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:34:08.573 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 05:34:08.573 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 05:34:08.573 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 05:34:08.573 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 05:34:08.574 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 25
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
