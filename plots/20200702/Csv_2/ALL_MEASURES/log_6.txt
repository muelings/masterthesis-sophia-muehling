* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 15628
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:13:22.933 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:13:22.938 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593659600899
* Starting evolution
[MASTER] 05:13:24.845 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:70.0 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 4
[MASTER] 05:13:24.845 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.0, number of tests: 10, total length: 187
[MASTER] 05:13:25.736 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 6
[MASTER] 05:13:25.736 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0, number of tests: 8, total length: 177
[MASTER] 05:13:27.230 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 10
[MASTER] 05:13:27.230 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0, number of tests: 9, total length: 208
[MASTER] 05:13:27.881 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 10
[MASTER] 05:13:27.881 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 9, total length: 208
[MASTER] 05:13:28.879 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 16
[MASTER] 05:13:28.879 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 10, total length: 257
[MASTER] 05:13:34.604 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 05:13:34.604 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 9, total length: 173
[MASTER] 05:13:36.404 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 05:13:36.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 10, total length: 201
[MASTER] 05:18:24.001 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 1753 generations, 1439802 statements, best individuals have fitness: 61.0
[MASTER] 05:18:24.005 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 5 tests with total length 51
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     61 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 05:18:24.131 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:18:24.156 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 36 
[MASTER] 05:18:24.215 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:18:24.215 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 05:18:24.215 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: get:MockIllegalArgumentException at 10
[MASTER] 05:18:24.215 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 05:18:24.215 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,]
[MASTER] 05:18:24.215 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 05:18:24.216 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 05:18:24.216 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 05:18:24.216 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 05:18:24.304 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:18:24.306 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 40%
* Total number of goals: 25
* Number of covered goals: 10
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
