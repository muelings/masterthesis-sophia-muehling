* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVParser
* Starting client
* Connecting to master process on port 21638
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVParser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 06:37:18.362 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:37:18.631 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 06:37:19.815 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:37:19.848 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 06:37:20.230 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 06:37:20.252 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 06:37:20.252 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 1 | Tests with assertion: 1
* Generated 1 tests with total length 12
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                          1 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 06:37:20.747 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:37:20.765 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 06:37:20.790 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 06:37:20.790 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MalformedURLException,, URL:MalformedURLException, getHeaderMap:NullPointerException]
[MASTER] 06:37:20.790 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: URL:MalformedURLException at 7
[MASTER] 06:37:20.790 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MalformedURLException,, URL:MalformedURLException, getHeaderMap:NullPointerException]
[MASTER] 06:37:20.791 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getHeaderMap:NullPointerException at 5
[MASTER] 06:37:20.791 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [MalformedURLException,, URL:MalformedURLException, getHeaderMap:NullPointerException]
[MASTER] 06:37:21.026 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
[MASTER] 06:37:21.029 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 70
* Number of covered goals: 3
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CSVParser_ESTest' to evosuite-tests
* Done!

* Computation finished
