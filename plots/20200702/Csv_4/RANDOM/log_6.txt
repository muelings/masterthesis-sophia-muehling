* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVParser
* Starting client
* Connecting to master process on port 12489
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVParser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 06:58:30.116 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:58:30.639 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 06:58:31.780 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:58:31.807 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 06:58:32.117 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 06:58:32.151 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 06:58:32.152 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 5 | Tests with assertion: 1
* Generated 1 tests with total length 22
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
[MASTER] 06:58:32.701 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:58:32.724 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 16 
[MASTER] 06:58:32.751 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 06:58:32.752 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [getHeaderMap:NullPointerException, IOException:nextRecord-427,, nextRecord:IOException]
[MASTER] 06:58:32.752 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: nextRecord:IOException at 15
[MASTER] 06:58:32.752 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [getHeaderMap:NullPointerException, IOException:nextRecord-427,, nextRecord:IOException]
[MASTER] 06:58:32.752 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getHeaderMap:NullPointerException at 8
[MASTER] 06:58:32.752 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [getHeaderMap:NullPointerException, IOException:nextRecord-427,, nextRecord:IOException]
[MASTER] 06:58:33.201 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 06:58:33.207 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 7%
* Total number of goals: 70
* Number of covered goals: 5
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CSVParser_ESTest' to evosuite-tests
* Done!

* Computation finished
