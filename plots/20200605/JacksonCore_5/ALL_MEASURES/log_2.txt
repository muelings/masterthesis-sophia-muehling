* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.JsonPointer
* Starting Client-0
* Connecting to master process on port 2186
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.JsonPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:20:25.401 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:20:25.484 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 70
* Using seed 1591334401171
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 07:20:33.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:258.1463241436926 - branchDistance:0.0 - coverage:95.14632414369257 - ex: 0 - tex: 14
[MASTER] 07:20:33.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 258.1463241436926, number of tests: 7, total length: 138
[MASTER] 07:20:33.722 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:251.00131720430107 - branchDistance:0.0 - coverage:88.00131720430107 - ex: 0 - tex: 14
[MASTER] 07:20:33.751 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 251.00131720430107, number of tests: 8, total length: 169
[MASTER] 07:20:37.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.5361111111111 - branchDistance:0.0 - coverage:70.53611111111111 - ex: 0 - tex: 16
[MASTER] 07:20:37.552 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.5361111111111, number of tests: 10, total length: 181
[MASTER] 07:20:50.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.66944444444445 - branchDistance:0.0 - coverage:62.669444444444444 - ex: 0 - tex: 16
[MASTER] 07:20:50.427 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.66944444444445, number of tests: 10, total length: 210
[MASTER] 07:20:54.998 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:214.20277777777778 - branchDistance:0.0 - coverage:51.20277777777778 - ex: 0 - tex: 16
[MASTER] 07:20:55.004 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 214.20277777777778, number of tests: 11, total length: 193
[MASTER] 07:21:00.237 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:213.86944444444444 - branchDistance:0.0 - coverage:50.86944444444445 - ex: 0 - tex: 18
[MASTER] 07:21:00.245 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 213.86944444444444, number of tests: 12, total length: 237
[MASTER] 07:21:08.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:213.83022875816994 - branchDistance:0.0 - coverage:50.83022875816994 - ex: 0 - tex: 16
[MASTER] 07:21:08.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 213.83022875816994, number of tests: 11, total length: 227
[MASTER] 07:21:11.255 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:212.44151326219637 - branchDistance:0.0 - coverage:49.44151326219637 - ex: 0 - tex: 14
[MASTER] 07:21:11.259 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 212.44151326219637, number of tests: 10, total length: 205
[MASTER] 07:21:11.945 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.5361111111111 - branchDistance:0.0 - coverage:46.53611111111111 - ex: 0 - tex: 12
[MASTER] 07:21:11.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.5361111111111, number of tests: 10, total length: 182
[MASTER] 07:21:18.083 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.1392857142857 - branchDistance:0.0 - coverage:46.13928571428571 - ex: 0 - tex: 14
[MASTER] 07:21:18.096 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.1392857142857, number of tests: 11, total length: 218
[MASTER] 07:21:24.837 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:207.58666666666667 - branchDistance:0.0 - coverage:44.586666666666666 - ex: 0 - tex: 18
[MASTER] 07:21:24.845 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 207.58666666666667, number of tests: 12, total length: 230
[MASTER] 07:21:30.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:205.20277777777778 - branchDistance:0.0 - coverage:42.20277777777778 - ex: 0 - tex: 12
[MASTER] 07:21:30.304 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 205.20277777777778, number of tests: 10, total length: 192
[MASTER] 07:21:30.496 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:204.625 - branchDistance:0.0 - coverage:41.625 - ex: 0 - tex: 14
[MASTER] 07:21:30.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 204.625, number of tests: 11, total length: 195
[MASTER] 07:21:35.703 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:203.58666666666667 - branchDistance:0.0 - coverage:40.586666666666666 - ex: 0 - tex: 18
[MASTER] 07:21:35.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 203.58666666666667, number of tests: 12, total length: 230
[MASTER] 07:21:38.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:201.92000000000002 - branchDistance:0.0 - coverage:38.92 - ex: 0 - tex: 16
[MASTER] 07:21:38.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 201.92000000000002, number of tests: 10, total length: 149
[MASTER] 07:21:40.983 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:200.625 - branchDistance:0.0 - coverage:37.625 - ex: 0 - tex: 14
[MASTER] 07:21:40.988 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 200.625, number of tests: 11, total length: 197
[MASTER] 07:21:44.790 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:198.29166666666669 - branchDistance:0.0 - coverage:35.29166666666667 - ex: 0 - tex: 16
[MASTER] 07:21:44.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 198.29166666666669, number of tests: 11, total length: 212
[MASTER] 07:21:48.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:194.6977777777778 - branchDistance:0.0 - coverage:31.69777777777778 - ex: 0 - tex: 18
[MASTER] 07:21:48.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 194.6977777777778, number of tests: 11, total length: 220
[MASTER] 07:21:51.657 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:194.625 - branchDistance:0.0 - coverage:31.625 - ex: 0 - tex: 16
[MASTER] 07:21:51.673 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 194.625, number of tests: 11, total length: 166
[MASTER] 07:21:52.719 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:194.0 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 16
[MASTER] 07:21:52.727 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 194.0, number of tests: 11, total length: 176
[MASTER] 07:21:55.227 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:193.6977777777778 - branchDistance:0.0 - coverage:30.697777777777777 - ex: 0 - tex: 16
[MASTER] 07:21:55.239 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 193.6977777777778, number of tests: 11, total length: 180
[MASTER] 07:21:55.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:193.625 - branchDistance:0.0 - coverage:30.625 - ex: 0 - tex: 18
[MASTER] 07:21:55.442 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 193.625, number of tests: 12, total length: 210
[MASTER] 07:21:57.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:192.51388888888889 - branchDistance:0.0 - coverage:29.51388888888889 - ex: 0 - tex: 16
[MASTER] 07:21:57.226 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 192.51388888888889, number of tests: 11, total length: 217
[MASTER] 07:22:04.816 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:189.73856209150327 - branchDistance:0.0 - coverage:26.73856209150327 - ex: 0 - tex: 18
[MASTER] 07:22:04.832 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 189.73856209150327, number of tests: 12, total length: 233
[MASTER] 07:22:07.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:189.61111111111111 - branchDistance:0.0 - coverage:26.611111111111114 - ex: 0 - tex: 20
[MASTER] 07:22:07.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 189.61111111111111, number of tests: 12, total length: 184
[MASTER] 07:22:09.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:187.61111111111111 - branchDistance:0.0 - coverage:24.611111111111114 - ex: 0 - tex: 20
[MASTER] 07:22:09.753 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 187.61111111111111, number of tests: 12, total length: 180
[MASTER] 07:22:12.237 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:187.52777777777777 - branchDistance:0.0 - coverage:24.52777777777778 - ex: 0 - tex: 20
[MASTER] 07:22:12.238 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 187.52777777777777, number of tests: 12, total length: 157
[MASTER] 07:22:17.640 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:185.61111111111111 - branchDistance:0.0 - coverage:22.611111111111114 - ex: 0 - tex: 18
[MASTER] 07:22:17.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 185.61111111111111, number of tests: 12, total length: 176
[MASTER] 07:22:18.527 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:185.52777777777777 - branchDistance:0.0 - coverage:22.52777777777778 - ex: 0 - tex: 18
[MASTER] 07:22:18.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 185.52777777777777, number of tests: 12, total length: 173
[MASTER] 07:22:20.146 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:185.45726495726495 - branchDistance:0.0 - coverage:22.45726495726496 - ex: 0 - tex: 20
[MASTER] 07:22:20.152 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 185.45726495726495, number of tests: 13, total length: 202
[MASTER] 07:22:25.356 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:184.86111111111111 - branchDistance:0.0 - coverage:21.86111111111111 - ex: 0 - tex: 20
[MASTER] 07:22:25.360 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 184.86111111111111, number of tests: 14, total length: 205
[MASTER] 07:22:26.137 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:184.19444444444446 - branchDistance:0.0 - coverage:21.194444444444443 - ex: 0 - tex: 18
[MASTER] 07:22:26.138 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 184.19444444444446, number of tests: 12, total length: 202
[MASTER] 07:22:32.803 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:184.19444444339743 - branchDistance:0.0 - coverage:21.19444444339743 - ex: 0 - tex: 18
[MASTER] 07:22:32.816 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 184.19444444339743, number of tests: 12, total length: 150
[MASTER] 07:22:34.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:183.52777777777777 - branchDistance:0.0 - coverage:20.527777777777775 - ex: 0 - tex: 20
[MASTER] 07:22:34.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 183.52777777777777, number of tests: 15, total length: 202
[MASTER] 07:22:38.613 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:183.52777777673077 - branchDistance:0.0 - coverage:20.527777776730762 - ex: 0 - tex: 18
[MASTER] 07:22:38.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 183.52777777673077, number of tests: 13, total length: 160
[MASTER] 07:22:43.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:183.37393162393164 - branchDistance:0.0 - coverage:20.373931623931625 - ex: 0 - tex: 20
[MASTER] 07:22:43.157 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 183.37393162393164, number of tests: 14, total length: 206
[MASTER] 07:22:48.777 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:183.04059828955127 - branchDistance:0.0 - coverage:20.040598289551276 - ex: 0 - tex: 18
[MASTER] 07:22:48.778 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 183.04059828955127, number of tests: 13, total length: 178
[MASTER] 07:22:51.548 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.3739316228846 - branchDistance:0.0 - coverage:19.37393162288461 - ex: 0 - tex: 16
[MASTER] 07:22:51.549 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.3739316228846, number of tests: 13, total length: 186
[MASTER] 07:22:52.721 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.37393162253466 - branchDistance:0.0 - coverage:19.37393162253464 - ex: 0 - tex: 16
[MASTER] 07:22:52.722 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.37393162253466, number of tests: 13, total length: 186
[MASTER] 07:23:02.398 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.37393157447445 - branchDistance:0.0 - coverage:19.37393157447444 - ex: 0 - tex: 16
[MASTER] 07:23:02.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.37393157447445, number of tests: 13, total length: 182
[MASTER] 07:23:09.249 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.36111110971413 - branchDistance:0.0 - coverage:19.361111109714127 - ex: 0 - tex: 16
[MASTER] 07:23:09.250 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.36111110971413, number of tests: 13, total length: 184
[MASTER] 07:23:09.467 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.0405982892013 - branchDistance:0.0 - coverage:19.04059828920131 - ex: 0 - tex: 18
[MASTER] 07:23:09.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.0405982892013, number of tests: 14, total length: 196
[MASTER] 07:23:16.622 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.0277777763808 - branchDistance:0.0 - coverage:19.027777776380795 - ex: 0 - tex: 18
[MASTER] 07:23:16.636 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.0277777763808, number of tests: 14, total length: 198
[MASTER] 07:23:21.129 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.004884003487 - branchDistance:0.0 - coverage:19.00488400348702 - ex: 0 - tex: 20
[MASTER] 07:23:21.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.004884003487, number of tests: 14, total length: 205
[MASTER] 07:23:22.179 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:181.0405982892013 - branchDistance:0.0 - coverage:18.040598289201306 - ex: 0 - tex: 20
[MASTER] 07:23:22.183 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 181.0405982892013, number of tests: 15, total length: 234
[MASTER] 07:23:28.554 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:181.00488400305036 - branchDistance:0.0 - coverage:18.004884003050357 - ex: 0 - tex: 18
[MASTER] 07:23:28.567 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 181.00488400305036, number of tests: 15, total length: 234
[MASTER] 07:23:31.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:180.95726495586797 - branchDistance:0.0 - coverage:17.957264955867977 - ex: 0 - tex: 20
[MASTER] 07:23:31.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 180.95726495586797, number of tests: 15, total length: 226
[MASTER] 07:23:36.255 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:180.9572649554313 - branchDistance:0.0 - coverage:17.957264955431313 - ex: 0 - tex: 18
[MASTER] 07:23:36.260 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 180.9572649554313, number of tests: 15, total length: 234
[MASTER] 07:23:37.883 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:180.0405982892013 - branchDistance:0.0 - coverage:17.040598289201306 - ex: 0 - tex: 20
[MASTER] 07:23:37.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 180.0405982892013, number of tests: 16, total length: 243
[MASTER] 07:23:42.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:179.95726495586797 - branchDistance:0.0 - coverage:16.957264955867977 - ex: 0 - tex: 20
[MASTER] 07:23:42.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 179.95726495586797, number of tests: 16, total length: 240
[MASTER] 07:23:45.070 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:179.9572649554313 - branchDistance:0.0 - coverage:16.957264955431313 - ex: 0 - tex: 16
[MASTER] 07:23:45.075 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 179.9572649554313, number of tests: 15, total length: 219
[MASTER] 07:23:47.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:179.94444444304747 - branchDistance:0.0 - coverage:16.944444443047463 - ex: 0 - tex: 20
[MASTER] 07:23:47.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 179.94444444304747, number of tests: 16, total length: 243
[MASTER] 07:23:51.609 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:179.11111110971413 - branchDistance:0.0 - coverage:16.11111110971413 - ex: 0 - tex: 22
[MASTER] 07:23:51.618 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 179.11111110971413, number of tests: 17, total length: 276
[MASTER] 07:24:23.493 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:179.04444444304747 - branchDistance:0.0 - coverage:16.04444444304746 - ex: 0 - tex: 22
[MASTER] 07:24:23.494 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 179.04444444304747, number of tests: 16, total length: 246
[MASTER] 07:24:35.848 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:178.94444444304747 - branchDistance:0.0 - coverage:15.94444444304746 - ex: 0 - tex: 22
[MASTER] 07:24:35.849 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 178.94444444304747, number of tests: 16, total length: 223
[MASTER] 07:25:00.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:178.7777777763808 - branchDistance:0.0 - coverage:15.777777776380795 - ex: 0 - tex: 24
[MASTER] 07:25:00.343 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 178.7777777763808, number of tests: 16, total length: 217
[MASTER] 07:25:19.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:177.7777777763808 - branchDistance:0.0 - coverage:14.777777776380793 - ex: 0 - tex: 24
[MASTER] 07:25:19.055 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 177.7777777763808, number of tests: 16, total length: 210
[MASTER] 07:25:26.939 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 135 generations, 60068 statements, best individuals have fitness: 177.7777777763808
[MASTER] 07:25:26.964 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 86%
* Total number of goals: 70
* Number of covered goals: 60
* Generated 15 tests with total length 203
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                    178 / 0           
	- MaxTime :                        303 / 300          Finished!
[MASTER] 07:25:29.356 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:25:29.673 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 165 
[MASTER] 07:25:30.526 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.531 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.533 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.533 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.533 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.540 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.540 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.541 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.543 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.543 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.544 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 07:25:30.544 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:25:30.544 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:25:30.551 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:25:30.552 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:25:30.553 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 07:25:30.554 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 07:25:30.555 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 07:25:30.555 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 07:25:30.556 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 07:25:30.563 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 07:25:30.572 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 07:25:30.572 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 07:25:30.574 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 07:25:30.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 07:25:30.582 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 07:25:30.584 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 07:25:30.594 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 70
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing tests to file
* Writing JUnit test case 'JsonPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
