/*
 * This file was automatically generated by EvoSuite
 * Fri Jun 05 05:33:35 GMT 2020
 */

package com.fasterxml.jackson.core;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import com.fasterxml.jackson.core.JsonPointer;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class JsonPointer_ESTest extends JsonPointer_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NumberFormatException : For input string: "0f"
      JsonPointer._parseTail("R0f");
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Invalid input: JSON Pointer expression must start with '/': "GzqaULHH+-WVo"
      // Undeclared exception!
      try { 
        JsonPointer.valueOf("GzqaULHH+-WVo");
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Invalid input: JSON Pointer expression must start with '/': \"GzqaULHH+-WVo\"
         //
         verifyException("com.fasterxml.jackson.core.JsonPointer", e);
         assertTrue(e.getMessage().equals("Invalid input: JSON Pointer expression must start with '/': \"GzqaULHH+-WVo\""));   
      }
  }
}
