* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.JsonPointer
* Starting Client-0
* Connecting to master process on port 2090
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.JsonPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 07:33:00.399 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 70
[MASTER] 07:33:14.577 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 07:33:23.728 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:33:23.807 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 07:33:25.215 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:33:25.288 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 256 | Tests with assertion: 1
* Generated 1 tests with total length 9
[MASTER] 07:33:25.312 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                         25 / 300         
[MASTER] 07:33:30.355 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:33:30.529 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 5 
[MASTER] 07:33:30.612 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 07:33:30.613 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 07:33:30.614 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: valueOf:MockIllegalArgumentException at 4
[MASTER] 07:33:30.614 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:compile-96,, valueOf:MockIllegalArgumentException]
[MASTER] 07:33:30.619 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: _parseTail:NumberFormatException at 1
[MASTER] 07:33:30.622 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [MockIllegalArgumentException:compile-96,, valueOf:MockIllegalArgumentException, _parseTail:NumberFormatException]
[MASTER] 07:33:30.995 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 07:33:31.029 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 21%
* Total number of goals: 70
* Number of covered goals: 15
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'JsonPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
