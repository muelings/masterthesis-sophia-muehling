/*
 * This file was automatically generated by EvoSuite
 * Fri Jun 05 05:26:36 GMT 2020
 */

package com.fasterxml.jackson.core;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import com.fasterxml.jackson.core.JsonPointer;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class JsonPointer_ESTest extends JsonPointer_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NumberFormatException : For input string: "0&"
      JsonPointer._parseTail("_0&");
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     java.lang.IndexOutOfBoundsException : start 1, end 2701, s.length() 3
      // Undeclared exception!
      try { 
        JsonPointer._parseQuotedTail("_0&", 2702);
        fail("Expecting exception: IndexOutOfBoundsException");
      
      } catch(IndexOutOfBoundsException e) {
         //
         // start 1, end 2701, s.length() 3
         //
         verifyException("java.lang.AbstractStringBuilder", e);
         assertTrue(e.getMessage().equals("start 1, end 2701, s.length() 3"));   
      }
  }
}
