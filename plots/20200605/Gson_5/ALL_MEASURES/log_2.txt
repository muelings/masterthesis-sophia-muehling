* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.util.ISO8601Utils
* Starting Client-0
* Connecting to master process on port 14235
* Analyzing classpath: 
  - ../../../defects4j_compiled/Gson_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.util.ISO8601Utils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:25:16.190 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:25:16.289 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
* Using seed 1591320275245
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 03:25:24.750 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:380.0 - branchDistance:0.0 - coverage:175.0 - ex: 0 - tex: 20
[MASTER] 03:25:24.764 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 380.0, number of tests: 10, total length: 193
[MASTER] 03:25:25.417 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:354.83333333333337 - branchDistance:0.0 - coverage:149.83333333333334 - ex: 0 - tex: 12
[MASTER] 03:25:25.418 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 354.83333333333337, number of tests: 8, total length: 157
[MASTER] 03:25:42.844 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:291.3809523809524 - branchDistance:0.0 - coverage:86.38095238095238 - ex: 0 - tex: 14
[MASTER] 03:25:42.852 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 291.3809523809524, number of tests: 9, total length: 178
[MASTER] 03:25:49.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:290.04761904761904 - branchDistance:0.0 - coverage:85.04761904761905 - ex: 0 - tex: 14
[MASTER] 03:25:49.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 290.04761904761904, number of tests: 9, total length: 186
[MASTER] 03:26:01.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:287.3809523809524 - branchDistance:0.0 - coverage:82.38095238095238 - ex: 0 - tex: 16
[MASTER] 03:26:01.966 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 287.3809523809524, number of tests: 9, total length: 184
[MASTER] 03:26:04.010 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:278.9127649789106 - branchDistance:0.0 - coverage:73.91276497891062 - ex: 0 - tex: 16
[MASTER] 03:26:04.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 278.9127649789106, number of tests: 9, total length: 190
[MASTER] 03:26:12.446 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:278.5794316455773 - branchDistance:0.0 - coverage:73.57943164557727 - ex: 0 - tex: 14
[MASTER] 03:26:12.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 278.5794316455773, number of tests: 8, total length: 174
[MASTER] 03:26:14.031 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:277.5794316455773 - branchDistance:0.0 - coverage:72.57943164557727 - ex: 0 - tex: 16
[MASTER] 03:26:14.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 277.5794316455773, number of tests: 9, total length: 195
[MASTER] 03:26:15.518 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:276.5794316455773 - branchDistance:0.0 - coverage:71.57943164557727 - ex: 0 - tex: 16
[MASTER] 03:26:15.539 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 276.5794316455773, number of tests: 9, total length: 235
[MASTER] 03:26:17.744 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:274.5794316455773 - branchDistance:0.0 - coverage:69.57943164557727 - ex: 0 - tex: 18
[MASTER] 03:26:17.745 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 274.5794316455773, number of tests: 9, total length: 233
[MASTER] 03:26:20.290 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:273.5794316455773 - branchDistance:0.0 - coverage:68.57943164557727 - ex: 0 - tex: 18
[MASTER] 03:26:20.295 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 273.5794316455773, number of tests: 9, total length: 238
[MASTER] 03:26:22.393 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:272.9127649789106 - branchDistance:0.0 - coverage:67.91276497891062 - ex: 0 - tex: 14
[MASTER] 03:26:22.402 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 272.9127649789106, number of tests: 9, total length: 188
[MASTER] 03:26:25.429 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:272.5794316455773 - branchDistance:0.0 - coverage:67.57943164557727 - ex: 0 - tex: 18
[MASTER] 03:26:25.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 272.5794316455773, number of tests: 9, total length: 281
[MASTER] 03:26:26.988 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.5794316455773 - branchDistance:0.0 - coverage:66.57943164557727 - ex: 0 - tex: 14
[MASTER] 03:26:26.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.5794316455773, number of tests: 9, total length: 192
[MASTER] 03:26:34.952 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.5770591663365 - branchDistance:0.0 - coverage:66.57705916633647 - ex: 0 - tex: 16
[MASTER] 03:26:34.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.5770591663365, number of tests: 9, total length: 246
[MASTER] 03:26:37.901 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.9127649789106 - branchDistance:0.0 - coverage:65.91276497891062 - ex: 0 - tex: 18
[MASTER] 03:26:37.901 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.9127649789106, number of tests: 10, total length: 259
[MASTER] 03:26:44.554 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.0207014868471 - branchDistance:0.0 - coverage:65.02070148684712 - ex: 0 - tex: 16
[MASTER] 03:26:44.568 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.0207014868471, number of tests: 9, total length: 229
[MASTER] 03:26:50.716 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.35403482018046 - branchDistance:0.0 - coverage:64.35403482018044 - ex: 0 - tex: 14
[MASTER] 03:26:50.724 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.35403482018046, number of tests: 9, total length: 205
[MASTER] 03:26:52.735 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.0207014868471 - branchDistance:0.0 - coverage:64.02070148684712 - ex: 0 - tex: 16
[MASTER] 03:26:52.740 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.0207014868471, number of tests: 9, total length: 195
[MASTER] 03:26:54.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.94927291541853 - branchDistance:0.0 - coverage:63.94927291541855 - ex: 0 - tex: 14
[MASTER] 03:26:54.476 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.94927291541853, number of tests: 9, total length: 186
[MASTER] 03:27:02.801 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.35403482018046 - branchDistance:0.0 - coverage:63.354034820180445 - ex: 0 - tex: 16
[MASTER] 03:27:02.803 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.35403482018046, number of tests: 10, total length: 200
[MASTER] 03:27:10.577 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.94927291541853 - branchDistance:0.0 - coverage:62.94927291541855 - ex: 0 - tex: 18
[MASTER] 03:27:10.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.94927291541853, number of tests: 11, total length: 213
[MASTER] 03:27:18.451 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.92979094076657 - branchDistance:0.0 - coverage:62.92979094076654 - ex: 0 - tex: 16
[MASTER] 03:27:18.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.92979094076657, number of tests: 11, total length: 200
[MASTER] 03:27:21.233 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.7984792646249 - branchDistance:0.0 - coverage:62.79847926462489 - ex: 0 - tex: 16
[MASTER] 03:27:21.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.7984792646249, number of tests: 10, total length: 183
[MASTER] 03:27:21.794 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.0207014868471 - branchDistance:0.0 - coverage:62.020701486847116 - ex: 0 - tex: 16
[MASTER] 03:27:21.799 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.0207014868471, number of tests: 10, total length: 195
[MASTER] 03:27:25.821 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:266.0012195121951 - branchDistance:0.0 - coverage:61.00121951219512 - ex: 0 - tex: 16
[MASTER] 03:27:25.830 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 266.0012195121951, number of tests: 11, total length: 230
[MASTER] 03:27:49.833 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.9237179487179 - branchDistance:0.0 - coverage:60.92371794871795 - ex: 0 - tex: 16
[MASTER] 03:27:49.840 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.9237179487179, number of tests: 11, total length: 216
[MASTER] 03:29:21.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.2973684210526 - branchDistance:0.0 - coverage:60.297368421052624 - ex: 0 - tex: 12
[MASTER] 03:29:21.413 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.2973684210526, number of tests: 8, total length: 134
[MASTER] 03:29:32.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.0473684210526 - branchDistance:0.0 - coverage:60.047368421052624 - ex: 0 - tex: 14
[MASTER] 03:29:32.166 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.0473684210526, number of tests: 8, total length: 163
[MASTER] 03:30:17.566 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 127 generations, 94467 statements, best individuals have fitness: 265.0473684210526
[MASTER] 03:30:17.597 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 97
* Number of covered goals: 58
* Generated 7 tests with total length 139
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        306 / 300          Finished!
	- ZeroFitness :                    265 / 0           
[MASTER] 03:30:22.721 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:30:23.245 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 110 
[MASTER] 03:30:23.803 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 03:30:23.813 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 03:30:23.828 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 03:30:23.829 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 03:30:23.835 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 03:30:23.836 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 03:30:23.843 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 03:30:23.845 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:30:23.845 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:30:23.846 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:30:23.846 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:30:23.851 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:30:23.851 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:30:23.853 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:30:23.860 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 03:30:23.875 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing tests to file
* Writing JUnit test case 'ISO8601Utils_ESTest' to evosuite-tests
* Done!

* Computation finished
