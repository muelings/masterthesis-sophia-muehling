* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.util.ISO8601Utils
* Starting Client-0
* Connecting to master process on port 19581
* Analyzing classpath: 
  - ../../../defects4j_compiled/Gson_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.util.ISO8601Utils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:00:51.682 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:00:51.769 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
* Using seed 1591322411241
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 04:01:00.960 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:359.979797979798 - branchDistance:0.0 - coverage:154.97979797979798 - ex: 0 - tex: 14
[MASTER] 04:01:00.964 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 359.979797979798, number of tests: 9, total length: 135
[MASTER] 04:01:02.937 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.0 - branchDistance:0.0 - coverage:148.0 - ex: 0 - tex: 16
[MASTER] 04:01:02.964 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.0, number of tests: 9, total length: 162
[MASTER] 04:01:04.774 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:309.0 - branchDistance:0.0 - coverage:104.0 - ex: 0 - tex: 16
[MASTER] 04:01:04.788 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 309.0, number of tests: 9, total length: 207
[MASTER] 04:01:41.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:307.6666666666667 - branchDistance:0.0 - coverage:102.66666666666667 - ex: 0 - tex: 18
[MASTER] 04:01:41.413 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 307.6666666666667, number of tests: 10, total length: 222
[MASTER] 04:01:46.003 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:307.33333333333337 - branchDistance:0.0 - coverage:102.33333333333334 - ex: 0 - tex: 20
[MASTER] 04:01:46.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 307.33333333333337, number of tests: 10, total length: 275
[MASTER] 04:01:48.171 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:306.6666666666667 - branchDistance:0.0 - coverage:101.66666666666667 - ex: 0 - tex: 20
[MASTER] 04:01:48.184 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 306.6666666666667, number of tests: 11, total length: 251
[MASTER] 04:01:51.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:306.33333333333337 - branchDistance:0.0 - coverage:101.33333333333334 - ex: 0 - tex: 16
[MASTER] 04:01:51.060 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 306.33333333333337, number of tests: 8, total length: 177
[MASTER] 04:01:52.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:303.6666666666667 - branchDistance:0.0 - coverage:98.66666666666667 - ex: 0 - tex: 18
[MASTER] 04:01:52.711 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 303.6666666666667, number of tests: 11, total length: 258
[MASTER] 04:01:54.704 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:302.6666666666667 - branchDistance:0.0 - coverage:97.66666666666667 - ex: 0 - tex: 20
[MASTER] 04:01:54.711 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 302.6666666666667, number of tests: 12, total length: 270
[MASTER] 04:02:06.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:297.5739467849224 - branchDistance:0.0 - coverage:92.57394678492238 - ex: 0 - tex: 18
[MASTER] 04:02:06.724 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 297.5739467849224, number of tests: 10, total length: 231
[MASTER] 04:02:09.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:292.5739467849224 - branchDistance:0.0 - coverage:87.57394678492238 - ex: 0 - tex: 18
[MASTER] 04:02:09.065 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 292.5739467849224, number of tests: 11, total length: 259
[MASTER] 04:02:12.485 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:290.9072801182557 - branchDistance:0.0 - coverage:85.90728011825573 - ex: 0 - tex: 18
[MASTER] 04:02:12.492 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 290.9072801182557, number of tests: 12, total length: 234
[MASTER] 04:02:24.355 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:280.72546193643757 - branchDistance:0.0 - coverage:75.72546193643754 - ex: 0 - tex: 18
[MASTER] 04:02:24.356 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 280.72546193643757, number of tests: 9, total length: 194
[MASTER] 04:02:25.986 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:279.72546193643757 - branchDistance:0.0 - coverage:74.72546193643754 - ex: 0 - tex: 20
[MASTER] 04:02:25.992 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 279.72546193643757, number of tests: 10, total length: 236
[MASTER] 04:02:26.902 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:275.72546193643757 - branchDistance:0.0 - coverage:70.72546193643754 - ex: 0 - tex: 18
[MASTER] 04:02:26.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 275.72546193643757, number of tests: 10, total length: 205
[MASTER] 04:02:30.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:274.0587952697709 - branchDistance:0.0 - coverage:69.05879526977088 - ex: 0 - tex: 16
[MASTER] 04:02:30.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 274.0587952697709, number of tests: 11, total length: 203
[MASTER] 04:02:36.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:273.0587952697709 - branchDistance:0.0 - coverage:68.05879526977088 - ex: 0 - tex: 18
[MASTER] 04:02:36.188 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 273.0587952697709, number of tests: 11, total length: 225
[MASTER] 04:02:39.201 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:272.72546193643757 - branchDistance:0.0 - coverage:67.72546193643754 - ex: 0 - tex: 20
[MASTER] 04:02:39.202 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 272.72546193643757, number of tests: 12, total length: 244
[MASTER] 04:02:47.205 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.0587952697709 - branchDistance:0.0 - coverage:65.05879526977087 - ex: 0 - tex: 20
[MASTER] 04:02:47.212 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.0587952697709, number of tests: 12, total length: 249
[MASTER] 04:02:55.671 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.7973684210526 - branchDistance:0.0 - coverage:63.797368421052624 - ex: 0 - tex: 20
[MASTER] 04:02:55.672 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.7973684210526, number of tests: 12, total length: 250
[MASTER] 04:03:03.478 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.0587952697709 - branchDistance:0.0 - coverage:63.05879526977087 - ex: 0 - tex: 20
[MASTER] 04:03:03.484 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.0587952697709, number of tests: 12, total length: 276
[MASTER] 04:03:11.403 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:266.7973684210526 - branchDistance:0.0 - coverage:61.797368421052624 - ex: 0 - tex: 18
[MASTER] 04:03:11.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 266.7973684210526, number of tests: 12, total length: 248
[MASTER] 04:03:29.768 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.7973684210526 - branchDistance:0.0 - coverage:60.797368421052624 - ex: 0 - tex: 18
[MASTER] 04:03:29.769 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.7973684210526, number of tests: 12, total length: 240
[MASTER] 04:03:57.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:264.2973684210526 - branchDistance:0.0 - coverage:59.297368421052624 - ex: 0 - tex: 18
[MASTER] 04:03:57.137 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 264.2973684210526, number of tests: 13, total length: 244
[MASTER] 04:04:05.471 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:264.1949874686717 - branchDistance:0.0 - coverage:59.19498746867167 - ex: 0 - tex: 18
[MASTER] 04:04:05.488 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 264.1949874686717, number of tests: 13, total length: 245
[MASTER] 04:04:18.203 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:263.6949874686717 - branchDistance:0.0 - coverage:58.69498746867167 - ex: 0 - tex: 18
[MASTER] 04:04:18.204 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 263.6949874686717, number of tests: 12, total length: 234
[MASTER] 04:05:33.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:263.5473684210526 - branchDistance:0.0 - coverage:58.547368421052624 - ex: 0 - tex: 16
[MASTER] 04:05:33.760 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 263.5473684210526, number of tests: 12, total length: 202
[MASTER] 04:05:52.886 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 111 generations, 84900 statements, best individuals have fitness: 263.5473684210526
[MASTER] 04:05:52.906 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 61%
* Total number of goals: 97
* Number of covered goals: 59
* Generated 11 tests with total length 185
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        305 / 300          Finished!
	- ZeroFitness :                    264 / 0           
[MASTER] 04:05:57.496 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:05:57.835 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 123 
[MASTER] 04:05:58.349 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 04:05:58.366 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 04:05:58.379 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 04:05:58.387 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 04:05:58.415 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 04:05:58.428 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 04:05:58.429 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 04:05:58.453 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 04:05:58.455 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing tests to file
* Writing JUnit test case 'ISO8601Utils_ESTest' to evosuite-tests
* Done!

* Computation finished
