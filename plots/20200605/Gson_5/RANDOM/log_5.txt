* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.util.ISO8601Utils
* Starting Client-0
* Connecting to master process on port 20736
* Analyzing classpath: 
  - ../../../defects4j_compiled/Gson_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.util.ISO8601Utils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 03:54:58.657 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
*** Random test generation finished.
*=*=*=* Total tests: 10371 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 03:59:59.737 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 04:00:00.405 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:00:00.420 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 221 seconds more than allowed.
[MASTER] 04:00:00.478 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 04:00:00.517 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:00:00.590 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing tests to file
* Writing JUnit test case 'ISO8601Utils_ESTest' to evosuite-tests
* Done!

* Computation finished
