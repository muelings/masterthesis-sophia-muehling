* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Starting Client-0
* Connecting to master process on port 10644
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:11:10.463 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:11:10.588 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1256
* Using seed 1591330219865
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 06:11:42.202 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:5294.25 - branchDistance:0.0 - coverage:2642.0 - ex: 3 - tex: 11
[MASTER] 06:11:42.207 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5294.25, number of tests: 5, total length: 147
[MASTER] 06:11:44.417 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.75 - fitness:3589.8636363636365 - branchDistance:0.0 - coverage:2625.0 - ex: 1 - tex: 9
[MASTER] 06:11:44.422 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3589.8636363636365, number of tests: 5, total length: 118
[MASTER] 06:11:54.978 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.8888888888888888 - fitness:3541.2 - branchDistance:0.0 - coverage:2623.0 - ex: 4 - tex: 18
[MASTER] 06:11:54.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3541.2, number of tests: 10, total length: 296
[MASTER] 06:11:55.674 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.999000499750125 - fitness:3178.5060481855444 - branchDistance:0.0 - coverage:2647.0 - ex: 0 - tex: 18
[MASTER] 06:11:55.675 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3178.5060481855444, number of tests: 9, total length: 269
[MASTER] 06:12:06.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.982905982905983 - fitness:3166.719554030875 - branchDistance:0.0 - coverage:2634.0 - ex: 1 - tex: 13
[MASTER] 06:12:06.879 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3166.719554030875, number of tests: 7, total length: 169
[MASTER] 06:12:49.386 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.999423132391116 - fitness:3155.961201176946 - branchDistance:0.0 - coverage:2625.0 - ex: 1 - tex: 9
[MASTER] 06:12:49.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3155.961201176946, number of tests: 5, total length: 134
* Computation finished
