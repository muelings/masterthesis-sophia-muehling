* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Starting Client-0
* Connecting to master process on port 12865
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:55:06.212 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:55:06.372 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1256
* Using seed 1591329257690
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 05:55:24.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:5304.0 - branchDistance:0.0 - coverage:2651.0 - ex: 0 - tex: 6
[MASTER] 05:55:24.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5304.0, number of tests: 3, total length: 78
[MASTER] 05:55:29.657 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:5297.5 - branchDistance:0.0 - coverage:2645.0 - ex: 1 - tex: 9
[MASTER] 05:55:29.658 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5297.5, number of tests: 5, total length: 118
[MASTER] 05:55:39.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4893617021276595 - fitness:3717.583333333333 - branchDistance:0.0 - coverage:2652.0 - ex: 3 - tex: 17
[MASTER] 05:55:39.960 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3717.583333333333, number of tests: 9, total length: 256
[MASTER] 05:55:42.536 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.971014492753623 - fitness:3180.4927113702624 - branchDistance:0.0 - coverage:2646.0 - ex: 0 - tex: 6
[MASTER] 05:55:42.537 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3180.4927113702624, number of tests: 4, total length: 101
[MASTER] 05:56:05.211 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.833333333333333 - fitness:3038.0975609756097 - branchDistance:0.0 - coverage:2649.0 - ex: 0 - tex: 12
[MASTER] 05:56:05.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3038.0975609756097, number of tests: 6, total length: 195
* Computation finished
