* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Starting Client-0
* Connecting to master process on port 18610
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:59:22.094 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:59:22.225 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1256
* Using seed 1591329510880
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 05:59:39.848 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:5281.5 - branchDistance:0.0 - coverage:2629.0 - ex: 1 - tex: 5
[MASTER] 05:59:39.849 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5281.5, number of tests: 3, total length: 80
[MASTER] 05:59:48.591 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.8888888888888888 - fitness:3534.0 - branchDistance:0.0 - coverage:2615.5 - ex: 1 - tex: 19
[MASTER] 05:59:48.592 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3534.0, number of tests: 10, total length: 263
[MASTER] 05:59:55.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.938461538461539 - fitness:2945.6953528399313 - branchDistance:0.0 - coverage:2648.0 - ex: 0 - tex: 12
[MASTER] 05:59:55.515 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2945.6953528399313, number of tests: 7, total length: 168
* Computation finished
