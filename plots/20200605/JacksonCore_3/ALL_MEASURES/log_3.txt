* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Starting Client-0
* Connecting to master process on port 10104
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:03:20.278 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:03:20.428 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1256
* Using seed 1591329745656
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 06:03:52.228 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999999999999982 - fitness:3713.800000000001 - branchDistance:0.0 - coverage:2652.0 - ex: 0 - tex: 20
[MASTER] 06:03:52.237 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3713.800000000001, number of tests: 9, total length: 281
[MASTER] 06:04:16.764 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.999448275862069 - fitness:3177.9585333554046 - branchDistance:0.0 - coverage:2647.0 - ex: 1 - tex: 17
[MASTER] 06:04:16.765 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3177.9585333554046, number of tests: 9, total length: 249
[MASTER] 06:04:18.020 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.993399339933993 - fitness:3160.1011235955057 - branchDistance:0.0 - coverage:2628.0 - ex: 0 - tex: 20
[MASTER] 06:04:18.021 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3160.1011235955057, number of tests: 10, total length: 277
* Computation finished
