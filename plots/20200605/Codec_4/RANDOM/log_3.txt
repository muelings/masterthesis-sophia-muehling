* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting Client-0
* Connecting to master process on port 7021
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 15:25:31.058 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 197
[MASTER] 15:25:50.354 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 15:25:52.883 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("ixce", var13);  // (Primitive) Original Value: ixce | Regression Value: ixce  
[MASTER] 15:25:52.884 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var13.equals((java.lang.Object)var7));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 15:25:52.924 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:25:52.926 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 15:26:00.284 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:26:00.453 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("ixce", var13);  // (Primitive) Original Value: ixce | Regression Value: ixce  
[MASTER] 15:26:00.453 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var13.equals((java.lang.Object)var7));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 15:26:00.468 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:26:00.469 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 15:26:04.992 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("DKys+1Y=", var12);  // (Primitive) Original Value: DKys+1Y= | Regression Value: DKys+1Y=  
[MASTER] 15:26:04.993 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:26:04.993 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 15:26:06.075 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:26:06.119 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("DKys+1Y=", var12);  // (Primitive) Original Value: DKys+1Y= | Regression Value: DKys+1Y=  
[MASTER] 15:26:06.136 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:26:06.138 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 15:26:07.165 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:26:07.228 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("DKys+1Y=", var12);  // (Primitive) Original Value: DKys+1Y= | Regression Value: DKys+1Y=  
[MASTER] 15:26:07.229 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:26:07.229 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 83 | Tests with assertion: 1
* Generated 1 tests with total length 13
[MASTER] 15:26:07.262 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         36 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 15:26:15.182 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:26:15.279 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 15:26:15.409 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 15:26:15.591 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@cd8e6760] invalid assertion(s) to be removed
[MASTER] 15:26:15.680 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@4ec80698] invalid assertion(s) to be removed
[MASTER] 15:26:15.751 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@2a6c7081] invalid assertion(s) to be removed
[MASTER] 15:26:15.815 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@d7f3734e] invalid assertion(s) to be removed
[MASTER] 15:26:15.972 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@631d9cdd] invalid assertion(s) to be removed
[MASTER] 15:26:16.111 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 15:26:16.140 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 23%
* Total number of goals: 197
* Number of covered goals: 45
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
