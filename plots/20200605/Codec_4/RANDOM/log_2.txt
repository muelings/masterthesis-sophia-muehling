* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting Client-0
* Connecting to master process on port 2298
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 15:18:56.601 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 197
[MASTER] 15:19:02.579 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("ZwQkZg==", var10);  // (Primitive) Original Value: ZwQkZg== | Regression Value: ZwQkZg==  
[MASTER] 15:19:02.629 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:19:02.630 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 15:19:09.884 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:19:09.940 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("ZwQkZg==", var10);  // (Primitive) Original Value: ZwQkZg== | Regression Value: ZwQkZg==  
[MASTER] 15:19:09.943 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:19:09.948 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 15:19:11.084 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:19:11.152 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("ZwQkZg==", var10);  // (Primitive) Original Value: ZwQkZg== | Regression Value: ZwQkZg==  
[MASTER] 15:19:11.153 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 15:19:11.153 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 39 | Tests with assertion: 1
* Generated 1 tests with total length 11
[MASTER] 15:19:11.204 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                         14 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 15:19:18.746 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:19:18.895 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 15:19:19.004 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 15:19:19.196 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@95b5999a] invalid assertion(s) to be removed
[MASTER] 15:19:19.315 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@fc685003] invalid assertion(s) to be removed
[MASTER] 15:19:19.419 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@3860631b] invalid assertion(s) to be removed
[MASTER] 15:19:19.528 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@8cb3c009] invalid assertion(s) to be removed
[MASTER] 15:19:19.785 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
* Going to analyze the coverage criteria
[MASTER] 15:19:19.823 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 22%
* Total number of goals: 197
* Number of covered goals: 44
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
