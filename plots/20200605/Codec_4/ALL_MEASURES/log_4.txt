* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting Client-0
* Connecting to master process on port 10042
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 15:33:38.554 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 15:33:38.644 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 197
* Using seed 1591277594273
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 15:33:47.726 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:616.226401797334 - branchDistance:0.0 - coverage:177.22640179733398 - ex: 0 - tex: 0
[MASTER] 15:33:47.740 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 616.226401797334, number of tests: 1, total length: 23
[MASTER] 15:34:05.009 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:508.3 - branchDistance:0.0 - coverage:438.0 - ex: 0 - tex: 18
[MASTER] 15:34:05.012 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 508.3, number of tests: 10, total length: 221
[MASTER] 15:34:05.626 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:207.90298308124596 - branchDistance:0.0 - coverage:137.60298308124595 - ex: 0 - tex: 6
[MASTER] 15:34:05.664 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 207.90298308124596, number of tests: 9, total length: 171
[MASTER] 15:34:13.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:178.0226890744661 - branchDistance:0.0 - coverage:107.7226890744661 - ex: 0 - tex: 8
[MASTER] 15:34:13.505 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 178.0226890744661, number of tests: 9, total length: 259
[MASTER] 15:34:15.921 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.987012987012987 - fitness:145.70167126601234 - branchDistance:0.0 - coverage:89.86264687576843 - ex: 0 - tex: 4
[MASTER] 15:34:15.924 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 145.70167126601234, number of tests: 8, total length: 196
[MASTER] 15:34:25.470 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:141.29649435322239 - branchDistance:0.0 - coverage:70.9964943532224 - ex: 0 - tex: 14
[MASTER] 15:34:25.476 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.29649435322239, number of tests: 10, total length: 254
[MASTER] 15:34:36.774 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:135.2964943532224 - branchDistance:0.0 - coverage:64.99649435322242 - ex: 0 - tex: 12
[MASTER] 15:34:36.776 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.2964943532224, number of tests: 10, total length: 252
[MASTER] 15:34:46.527 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:131.2964943532224 - branchDistance:0.0 - coverage:60.99649435322241 - ex: 0 - tex: 10
[MASTER] 15:34:46.535 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.2964943532224, number of tests: 10, total length: 253
[MASTER] 15:34:48.279 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:130.6007575736233 - branchDistance:0.0 - coverage:60.300757573623294 - ex: 0 - tex: 6
[MASTER] 15:34:48.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.6007575736233, number of tests: 7, total length: 200
[MASTER] 15:34:50.215 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:125.60075757359557 - branchDistance:0.0 - coverage:55.300757573595575 - ex: 0 - tex: 8
[MASTER] 15:34:50.228 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.60075757359557, number of tests: 8, total length: 240
[MASTER] 15:34:56.057 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:121.03588829257754 - branchDistance:0.0 - coverage:50.73588829257754 - ex: 0 - tex: 12
[MASTER] 15:34:56.064 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.03588829257754, number of tests: 10, total length: 259
[MASTER] 15:34:56.666 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:114.93588829257754 - branchDistance:0.0 - coverage:44.63588829257754 - ex: 0 - tex: 12
[MASTER] 15:34:56.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.93588829257754, number of tests: 11, total length: 291
[MASTER] 15:35:11.543 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:113.93588829257754 - branchDistance:0.0 - coverage:43.63588829257754 - ex: 0 - tex: 14
[MASTER] 15:35:11.544 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.93588829257754, number of tests: 12, total length: 315
[MASTER] 15:35:21.957 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:112.93588829257754 - branchDistance:0.0 - coverage:42.63588829257754 - ex: 0 - tex: 14
[MASTER] 15:35:21.963 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.93588829257754, number of tests: 12, total length: 324
[MASTER] 15:35:23.457 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.320346320346321 - fitness:110.93588829257754 - branchDistance:0.0 - coverage:40.63588829257754 - ex: 0 - tex: 12
[MASTER] 15:35:23.464 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.93588829257754, number of tests: 11, total length: 293
[MASTER] 15:35:25.717 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:98.88785888785466 - branchDistance:0.0 - coverage:52.51655843942416 - ex: 0 - tex: 12
[MASTER] 15:35:25.723 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.88785888785466, number of tests: 9, total length: 279
[MASTER] 15:35:33.249 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:97.88785888785466 - branchDistance:0.0 - coverage:51.51655843942416 - ex: 0 - tex: 12
[MASTER] 15:35:33.255 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.88785888785466, number of tests: 9, total length: 281
[MASTER] 15:35:33.724 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:95.38785888785466 - branchDistance:0.0 - coverage:49.01655843942416 - ex: 0 - tex: 12
[MASTER] 15:35:33.739 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.38785888785466, number of tests: 10, total length: 305
[MASTER] 15:35:35.773 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:90.72119222118798 - branchDistance:0.0 - coverage:44.34989177275749 - ex: 0 - tex: 14
[MASTER] 15:35:35.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.72119222118798, number of tests: 11, total length: 323
[MASTER] 15:35:36.452 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:90.57920087919663 - branchDistance:0.0 - coverage:44.207900430766145 - ex: 0 - tex: 14
[MASTER] 15:35:36.459 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.57920087919663, number of tests: 10, total length: 292
[MASTER] 15:35:38.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:89.99983579983157 - branchDistance:0.0 - coverage:43.62853535140107 - ex: 0 - tex: 10
[MASTER] 15:35:38.407 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.99983579983157, number of tests: 11, total length: 310
[MASTER] 15:35:43.491 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:88.99983579983157 - branchDistance:0.0 - coverage:42.62853535140107 - ex: 0 - tex: 10
[MASTER] 15:35:43.491 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.99983579983157, number of tests: 11, total length: 315
[MASTER] 15:35:43.894 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:88.05452555452132 - branchDistance:0.0 - coverage:41.68322510609082 - ex: 0 - tex: 16
[MASTER] 15:35:43.900 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.05452555452132, number of tests: 12, total length: 340
[MASTER] 15:35:44.505 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:88.0031325040984 - branchDistance:0.0 - coverage:41.6318320556679 - ex: 0 - tex: 14
[MASTER] 15:35:44.508 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.0031325040984, number of tests: 11, total length: 304
[MASTER] 15:35:45.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:77.99983579983157 - branchDistance:0.0 - coverage:31.62853535140107 - ex: 0 - tex: 12
[MASTER] 15:35:45.163 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.99983579983157, number of tests: 11, total length: 329
[MASTER] 15:35:54.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:75.99983579983157 - branchDistance:0.0 - coverage:29.62853535140107 - ex: 0 - tex: 12
[MASTER] 15:35:54.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.99983579983157, number of tests: 11, total length: 328
[MASTER] 15:35:55.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:75.49983580080169 - branchDistance:0.0 - coverage:29.1285353523712 - ex: 0 - tex: 12
[MASTER] 15:35:55.141 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.49983580080169, number of tests: 11, total length: 307
[MASTER] 15:36:00.531 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:75.49983579983157 - branchDistance:0.0 - coverage:29.12853535140107 - ex: 0 - tex: 12
[MASTER] 15:36:00.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.49983579983157, number of tests: 11, total length: 317
[MASTER] 15:36:10.970 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:73.8331691331649 - branchDistance:0.0 - coverage:27.46186868473441 - ex: 0 - tex: 12
[MASTER] 15:36:10.981 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.8331691331649, number of tests: 12, total length: 337
[MASTER] 15:36:17.920 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:71.95816913413503 - branchDistance:0.0 - coverage:25.586868685704538 - ex: 0 - tex: 14
[MASTER] 15:36:17.921 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.95816913413503, number of tests: 12, total length: 338
[MASTER] 15:36:38.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:71.83316913413503 - branchDistance:0.0 - coverage:25.461868685704538 - ex: 0 - tex: 14
[MASTER] 15:36:38.132 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.83316913413503, number of tests: 12, total length: 322
[MASTER] 15:36:48.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:70.9581691331649 - branchDistance:0.0 - coverage:24.58686868473441 - ex: 0 - tex: 14
[MASTER] 15:36:48.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.9581691331649, number of tests: 12, total length: 364
[MASTER] 15:36:50.789 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:70.83316913413503 - branchDistance:0.0 - coverage:24.461868685704534 - ex: 0 - tex: 14
[MASTER] 15:36:50.800 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.83316913413503, number of tests: 12, total length: 323
[MASTER] 15:36:54.590 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:70.03753421350011 - branchDistance:0.0 - coverage:23.666233765069613 - ex: 0 - tex: 18
[MASTER] 15:36:54.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.03753421350011, number of tests: 14, total length: 384
[MASTER] 15:37:09.221 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:69.96018933615522 - branchDistance:0.0 - coverage:23.588888887724735 - ex: 0 - tex: 16
[MASTER] 15:37:09.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.96018933615522, number of tests: 13, total length: 367
[MASTER] 15:37:10.039 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:69.9581691331649 - branchDistance:0.0 - coverage:23.586868684734405 - ex: 0 - tex: 14
[MASTER] 15:37:10.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.9581691331649, number of tests: 12, total length: 369
[MASTER] 15:37:13.375 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:69.81296711393301 - branchDistance:0.0 - coverage:23.441666665502513 - ex: 0 - tex: 16
[MASTER] 15:37:13.387 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.81296711393301, number of tests: 13, total length: 356
[MASTER] 15:37:16.288 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:68.93796711393301 - branchDistance:0.0 - coverage:22.566666665502513 - ex: 0 - tex: 16
[MASTER] 15:37:16.295 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.93796711393301, number of tests: 13, total length: 350
[MASTER] 15:37:20.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:68.83316913413503 - branchDistance:0.0 - coverage:22.461868685704538 - ex: 0 - tex: 14
[MASTER] 15:37:20.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.83316913413503, number of tests: 14, total length: 364
[MASTER] 15:37:24.946 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:67.95816913413503 - branchDistance:0.0 - coverage:21.586868685704538 - ex: 0 - tex: 16
[MASTER] 15:37:24.954 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.95816913413503, number of tests: 14, total length: 387
[MASTER] 15:37:28.261 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:67.03753421350011 - branchDistance:0.0 - coverage:20.666233765069613 - ex: 0 - tex: 18
[MASTER] 15:37:28.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.03753421350011, number of tests: 15, total length: 375
[MASTER] 15:37:35.374 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.653679653679653 - fitness:66.95816913413503 - branchDistance:0.0 - coverage:20.586868685704538 - ex: 0 - tex: 18
[MASTER] 15:37:35.375 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.95816913413503, number of tests: 15, total length: 416
[MASTER] 15:37:45.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.334632031906866 - fitness:50.229031952117765 - branchDistance:0.0 - coverage:20.666233765069613 - ex: 0 - tex: 18
[MASTER] 15:37:45.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.229031952117765, number of tests: 15, total length: 378
[MASTER] 15:37:53.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.334632031906866 - fitness:50.14966687275269 - branchDistance:0.0 - coverage:20.586868685704538 - ex: 0 - tex: 20
[MASTER] 15:37:53.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.14966687275269, number of tests: 16, total length: 440
[MASTER] 15:37:58.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.334632032705947 - fitness:50.149666871264294 - branchDistance:0.0 - coverage:20.586868685704538 - ex: 0 - tex: 20
[MASTER] 15:37:58.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.149666871264294, number of tests: 16, total length: 440
[MASTER] 15:38:03.314 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.334632031906866 - fitness:48.37188909497492 - branchDistance:0.0 - coverage:18.80909090792676 - ex: 0 - tex: 18
[MASTER] 15:38:03.320 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.37188909497492, number of tests: 15, total length: 438
[MASTER] 15:38:13.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.334632031906866 - fitness:46.12946485371482 - branchDistance:0.0 - coverage:16.566666666666666 - ex: 0 - tex: 16
[MASTER] 15:38:13.463 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.12946485371482, number of tests: 15, total length: 436
[MASTER] 15:38:32.145 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.334632032705947 - fitness:44.907242630004205 - branchDistance:0.0 - coverage:15.344444444444445 - ex: 0 - tex: 20
[MASTER] 15:38:32.146 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 44.907242630004205, number of tests: 18, total length: 491
[MASTER] 15:38:39.864 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 53 generations, 80680 statements, best individuals have fitness: 44.907242630004205
[MASTER] 15:38:39.882 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 93%
* Total number of goals: 197
* Number of covered goals: 184
* Generated 18 tests with total length 491
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     45 / 0           
	- MaxTime :                        304 / 300          Finished!
[MASTER] 15:38:44.543 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:38:45.915 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 355 
[MASTER] 15:38:47.459 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.464 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.472 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.479 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.482 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.484 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.485 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.490 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.490 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.491 [logback-1] WARN  RegressionSuiteMinimizer - Test16 - Difference in number of exceptions: 0.0
[MASTER] 15:38:47.492 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 15:38:47.493 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 15:38:47.499 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 15:38:47.503 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 15:38:47.511 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 15:38:47.520 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 15:38:47.520 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 15:38:47.521 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 15:38:47.521 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 15:38:47.527 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 15:38:47.528 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 15:38:47.528 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 15:38:47.529 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 15:38:47.535 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 15:38:47.535 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 15:38:47.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 15:38:47.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 15:38:47.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 15:38:47.539 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 15:38:47.548 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 197
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing tests to file
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
