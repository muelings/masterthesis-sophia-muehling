* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
* Starting Client-0
* Connecting to master process on port 17615
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 22:04:03.936 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 22:04:04.006 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 99
* Using seed 1591301012594
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 22:04:25.390 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:429.0 - branchDistance:0.0 - coverage:214.0 - ex: 0 - tex: 14
[MASTER] 22:04:25.391 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 429.0, number of tests: 7, total length: 151
[MASTER] 22:04:28.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:408.0 - branchDistance:0.0 - coverage:193.0 - ex: 0 - tex: 0
[MASTER] 22:04:28.089 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 408.0, number of tests: 1, total length: 11
[MASTER] 22:04:41.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:399.28765432098766 - branchDistance:0.0 - coverage:184.28765432098766 - ex: 0 - tex: 16
[MASTER] 22:04:41.457 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 399.28765432098766, number of tests: 10, total length: 237
[MASTER] 22:04:43.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:398.6488095238095 - branchDistance:0.0 - coverage:183.64880952380952 - ex: 0 - tex: 10
[MASTER] 22:04:43.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 398.6488095238095, number of tests: 5, total length: 108
[MASTER] 22:04:44.969 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:396.6095238095238 - branchDistance:0.0 - coverage:181.6095238095238 - ex: 0 - tex: 12
[MASTER] 22:04:44.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 396.6095238095238, number of tests: 8, total length: 164
[MASTER] 22:04:48.128 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:377.0 - branchDistance:0.0 - coverage:162.0 - ex: 0 - tex: 10
[MASTER] 22:04:48.137 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 377.0, number of tests: 6, total length: 180
[MASTER] 22:05:09.442 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:365.5771505376344 - branchDistance:0.0 - coverage:150.5771505376344 - ex: 0 - tex: 8
[MASTER] 22:05:09.447 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 365.5771505376344, number of tests: 5, total length: 148
[MASTER] 22:05:11.945 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:360.6406926388217 - branchDistance:0.0 - coverage:145.6406926388217 - ex: 0 - tex: 10
[MASTER] 22:05:11.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 360.6406926388217, number of tests: 7, total length: 201
[MASTER] 22:05:18.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:360.6060606041897 - branchDistance:0.0 - coverage:145.60606060418968 - ex: 0 - tex: 10
[MASTER] 22:05:18.578 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 360.6060606041897, number of tests: 7, total length: 173
[MASTER] 22:05:23.340 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:357.5761765116604 - branchDistance:0.0 - coverage:142.57617651166038 - ex: 0 - tex: 12
[MASTER] 22:05:23.340 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 357.5761765116604, number of tests: 7, total length: 177
[MASTER] 22:05:28.565 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:355.55121951219513 - branchDistance:0.0 - coverage:140.55121951219513 - ex: 0 - tex: 6
[MASTER] 22:05:28.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 355.55121951219513, number of tests: 5, total length: 115
[MASTER] 22:05:32.759 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:355.53939393846144 - branchDistance:0.0 - coverage:140.53939393846142 - ex: 0 - tex: 6
[MASTER] 22:05:32.787 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 355.53939393846144, number of tests: 5, total length: 108
[MASTER] 22:05:37.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.575 - branchDistance:0.0 - coverage:138.575 - ex: 0 - tex: 6
[MASTER] 22:05:37.793 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.575, number of tests: 6, total length: 155
[MASTER] 22:05:41.230 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:352.55121951219513 - branchDistance:0.0 - coverage:137.55121951219513 - ex: 0 - tex: 4
[MASTER] 22:05:41.230 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 352.55121951219513, number of tests: 5, total length: 113
[MASTER] 22:05:42.808 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:351.4 - branchDistance:0.0 - coverage:136.4 - ex: 0 - tex: 8
[MASTER] 22:05:42.808 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 351.4, number of tests: 8, total length: 188
[MASTER] 22:05:47.144 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:350.575 - branchDistance:0.0 - coverage:135.575 - ex: 0 - tex: 6
[MASTER] 22:05:47.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 350.575, number of tests: 6, total length: 144
[MASTER] 22:05:52.784 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:350.4 - branchDistance:0.0 - coverage:135.4 - ex: 0 - tex: 8
[MASTER] 22:05:52.796 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 350.4, number of tests: 8, total length: 196
[MASTER] 22:05:55.176 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.4 - branchDistance:0.0 - coverage:134.4 - ex: 0 - tex: 6
[MASTER] 22:05:55.179 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.4, number of tests: 6, total length: 125
[MASTER] 22:06:43.762 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.37142857142857 - branchDistance:0.0 - coverage:134.37142857142857 - ex: 0 - tex: 6
[MASTER] 22:06:43.762 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.37142857142857, number of tests: 6, total length: 125
[MASTER] 22:06:58.234 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.3142857142857 - branchDistance:0.0 - coverage:134.31428571428572 - ex: 0 - tex: 6
[MASTER] 22:06:58.234 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.3142857142857, number of tests: 6, total length: 124
[MASTER] 22:07:15.351 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.2857142857143 - branchDistance:0.0 - coverage:134.28571428571428 - ex: 0 - tex: 6
[MASTER] 22:07:15.359 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.2857142857143, number of tests: 7, total length: 130

* Search finished after 302s and 49 generations, 55440 statements, best individuals have fitness: 349.2857142857143
[MASTER] 22:09:05.232 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:09:05.280 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 29%
* Total number of goals: 99
* Number of covered goals: 29
* Generated 6 tests with total length 100
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                    349 / 0           
	- MaxTime :                        303 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 22:09:07.851 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:09:08.183 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 61 
[MASTER] 22:09:08.594 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 22:09:08.615 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 22:09:08.615 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 22:09:08.615 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 22:09:08.615 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 22:09:08.615 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 22:09:08.615 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 22:09:08.615 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 22:09:08.616 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:09:08.631 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 99
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing tests to file
* Writing JUnit test case 'ZipArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
