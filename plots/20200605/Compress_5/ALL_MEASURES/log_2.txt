* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
* Starting Client-0
* Connecting to master process on port 12174
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 21:29:13.871 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 21:29:13.934 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 99
* Using seed 1591298926991
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 21:29:29.970 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:410.8333333333333 - branchDistance:0.0 - coverage:196.33333333333331 - ex: 1 - tex: 15
[MASTER] 21:29:29.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 410.8333333333333, number of tests: 10, total length: 283
[MASTER] 21:29:49.323 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:376.93333333333334 - branchDistance:0.0 - coverage:161.93333333333334 - ex: 0 - tex: 18
[MASTER] 21:29:49.325 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 376.93333333333334, number of tests: 9, total length: 206
[MASTER] 21:29:59.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:370.4666666666667 - branchDistance:0.0 - coverage:155.46666666666667 - ex: 0 - tex: 16
[MASTER] 21:29:59.088 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 370.4666666666667, number of tests: 8, total length: 225
[MASTER] 21:30:39.734 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:369.54049445865303 - branchDistance:0.0 - coverage:154.54049445865303 - ex: 0 - tex: 14
[MASTER] 21:30:39.735 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 369.54049445865303, number of tests: 7, total length: 231
[MASTER] 21:30:43.352 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:360.5666382495027 - branchDistance:0.0 - coverage:145.5666382495027 - ex: 0 - tex: 8
[MASTER] 21:30:43.352 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 360.5666382495027, number of tests: 6, total length: 136
[MASTER] 21:31:03.784 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:360.55501878690285 - branchDistance:0.0 - coverage:145.55501878690285 - ex: 0 - tex: 10
[MASTER] 21:31:03.785 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 360.55501878690285, number of tests: 5, total length: 151
[MASTER] 21:31:04.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:358.3797101430639 - branchDistance:0.0 - coverage:143.37971014306393 - ex: 0 - tex: 14
[MASTER] 21:31:04.224 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 358.3797101430639, number of tests: 8, total length: 207
[MASTER] 21:31:08.741 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:356.3797101430639 - branchDistance:0.0 - coverage:141.37971014306393 - ex: 0 - tex: 16
[MASTER] 21:31:08.756 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 356.3797101430639, number of tests: 9, total length: 226
[MASTER] 21:31:23.953 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:353.3797101430649 - branchDistance:0.0 - coverage:138.3797101430649 - ex: 0 - tex: 12
[MASTER] 21:31:23.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.3797101430649, number of tests: 7, total length: 206
[MASTER] 21:31:47.060 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:351.37971014492757 - branchDistance:0.0 - coverage:136.37971014492754 - ex: 0 - tex: 14
[MASTER] 21:31:47.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 351.37971014492757, number of tests: 8, total length: 240
[MASTER] 21:32:21.359 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:350.37971014492757 - branchDistance:0.0 - coverage:135.37971014492754 - ex: 0 - tex: 14
[MASTER] 21:32:21.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 350.37971014492757, number of tests: 9, total length: 258
[MASTER] 21:32:32.335 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:350.3614035087719 - branchDistance:0.0 - coverage:135.36140350877193 - ex: 0 - tex: 10
[MASTER] 21:32:32.335 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 350.3614035087719, number of tests: 7, total length: 157
[MASTER] 21:32:34.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:350.2166666666667 - branchDistance:0.0 - coverage:135.21666666666667 - ex: 0 - tex: 10
[MASTER] 21:32:34.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 350.2166666666667, number of tests: 7, total length: 203
[MASTER] 21:32:38.863 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.3614035087719 - branchDistance:0.0 - coverage:134.36140350877193 - ex: 0 - tex: 10
[MASTER] 21:32:38.863 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.3614035087719, number of tests: 7, total length: 176
[MASTER] 21:32:45.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.2166666666667 - branchDistance:0.0 - coverage:134.21666666666667 - ex: 0 - tex: 8
[MASTER] 21:32:45.807 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.2166666666667, number of tests: 7, total length: 171
[MASTER] 21:33:44.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:348.4166666666667 - branchDistance:0.0 - coverage:133.41666666666669 - ex: 0 - tex: 8
[MASTER] 21:33:44.285 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 348.4166666666667, number of tests: 8, total length: 192
[MASTER] 21:34:14.832 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:348.26666666666665 - branchDistance:0.0 - coverage:133.26666666666665 - ex: 0 - tex: 8
[MASTER] 21:34:14.832 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 348.26666666666665, number of tests: 9, total length: 227
[MASTER] 21:34:16.128 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 303s and 37 generations, 42618 statements, best individuals have fitness: 348.26666666666665
[MASTER] 21:34:16.138 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 29%
* Total number of goals: 99
* Number of covered goals: 29
* Generated 9 tests with total length 227
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                    348 / 0           
	- MaxTime :                        304 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 21:34:19.067 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:34:19.587 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 153 
[MASTER] 21:34:20.698 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 21:34:20.704 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 21:34:20.708 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 21:34:20.716 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 21:34:20.717 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 21:34:20.717 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:34:20.718 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 21:34:20.719 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 21:34:20.719 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 21:34:20.720 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 21:34:20.720 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 21:34:20.721 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 21:34:20.727 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 21:34:20.736 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 21:34:20.739 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 99
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing tests to file
* Writing JUnit test case 'ZipArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
