* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting Client-0
* Connecting to master process on port 8574
* Analyzing classpath: 
  - ../../../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 11:14:28.886 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 11:14:28.951 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 53
* Using seed 1591348436776
* Starting evolution
[MASTER] 11:14:50.320 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:173.0 - branchDistance:0.0 - coverage:63.0 - ex: 0 - tex: 14
[MASTER] 11:14:50.321 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 173.0, number of tests: 8, total length: 183
[MASTER] 11:15:21.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:169.0 - branchDistance:0.0 - coverage:59.0 - ex: 0 - tex: 14
[MASTER] 11:15:21.086 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 169.0, number of tests: 8, total length: 172
[MASTER] 11:15:25.408 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:167.0 - branchDistance:0.0 - coverage:57.0 - ex: 0 - tex: 14
[MASTER] 11:15:25.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 167.0, number of tests: 7, total length: 145
[MASTER] 11:15:26.279 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:109.60262271414823 - branchDistance:0.0 - coverage:65.0 - ex: 0 - tex: 14
[MASTER] 11:15:26.280 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.60262271414823, number of tests: 8, total length: 181
[MASTER] 11:15:31.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:106.60262271414823 - branchDistance:0.0 - coverage:62.0 - ex: 0 - tex: 14
[MASTER] 11:15:31.874 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.60262271414823, number of tests: 8, total length: 165
[MASTER] 11:15:44.476 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:102.60262271414823 - branchDistance:0.0 - coverage:58.0 - ex: 0 - tex: 16
[MASTER] 11:15:44.477 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.60262271414823, number of tests: 9, total length: 217
[MASTER] 11:15:46.262 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:100.60262271414823 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 14
[MASTER] 11:15:46.268 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.60262271414823, number of tests: 8, total length: 168
[MASTER] 11:15:56.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:96.60262271414823 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 16
[MASTER] 11:15:56.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.60262271414823, number of tests: 9, total length: 185
[MASTER] 11:16:01.818 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:94.60262271414823 - branchDistance:0.0 - coverage:50.0 - ex: 0 - tex: 14
[MASTER] 11:16:01.819 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.60262271414823, number of tests: 8, total length: 162
[MASTER] 11:16:07.682 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:91.60262271414823 - branchDistance:0.0 - coverage:47.0 - ex: 0 - tex: 14
[MASTER] 11:16:07.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.60262271414823, number of tests: 8, total length: 179
[MASTER] 11:16:13.423 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:90.10262271414823 - branchDistance:0.0 - coverage:45.5 - ex: 0 - tex: 18
[MASTER] 11:16:13.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.10262271414823, number of tests: 9, total length: 212
[MASTER] 11:16:19.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:89.60262271414823 - branchDistance:0.0 - coverage:45.0 - ex: 0 - tex: 20
[MASTER] 11:16:19.197 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.60262271414823, number of tests: 10, total length: 218
[MASTER] 11:16:20.703 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:88.60262271414823 - branchDistance:0.0 - coverage:44.0 - ex: 0 - tex: 14
[MASTER] 11:16:20.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.60262271414823, number of tests: 9, total length: 185
[MASTER] 11:16:21.089 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:85.60262271414823 - branchDistance:0.0 - coverage:41.0 - ex: 0 - tex: 14
[MASTER] 11:16:21.096 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.60262271414823, number of tests: 8, total length: 186
[MASTER] 11:16:28.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:83.60262271414823 - branchDistance:0.0 - coverage:39.0 - ex: 0 - tex: 16
[MASTER] 11:16:28.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.60262271414823, number of tests: 9, total length: 207
[MASTER] 11:16:33.400 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:82.60262271414823 - branchDistance:0.0 - coverage:38.0 - ex: 0 - tex: 16
[MASTER] 11:16:33.491 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.60262271414823, number of tests: 8, total length: 190
[MASTER] 11:16:37.701 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:79.60262271414823 - branchDistance:0.0 - coverage:35.0 - ex: 0 - tex: 14
[MASTER] 11:16:37.707 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.60262271414823, number of tests: 8, total length: 172
[MASTER] 11:16:40.382 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:77.60262271414823 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 16
[MASTER] 11:16:40.382 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.60262271414823, number of tests: 8, total length: 195
[MASTER] 11:16:44.492 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:76.10262271414823 - branchDistance:0.0 - coverage:31.5 - ex: 0 - tex: 18
[MASTER] 11:16:44.494 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.10262271414823, number of tests: 9, total length: 221
[MASTER] 11:16:46.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:75.60262271414823 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 16
[MASTER] 11:16:46.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.60262271414823, number of tests: 9, total length: 206
[MASTER] 11:16:46.402 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:74.10262271414823 - branchDistance:0.0 - coverage:29.5 - ex: 0 - tex: 16
[MASTER] 11:16:46.424 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.10262271414823, number of tests: 9, total length: 206
[MASTER] 11:16:52.942 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:73.60262271414823 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 18
[MASTER] 11:16:52.951 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.60262271414823, number of tests: 9, total length: 225
[MASTER] 11:16:54.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:72.10262271414823 - branchDistance:0.0 - coverage:27.5 - ex: 0 - tex: 18
[MASTER] 11:16:54.226 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.10262271414823, number of tests: 10, total length: 230
[MASTER] 11:17:01.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:71.10262271414823 - branchDistance:0.0 - coverage:26.5 - ex: 0 - tex: 18
[MASTER] 11:17:01.123 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.10262271414823, number of tests: 9, total length: 234
[MASTER] 11:17:02.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:70.10262271414823 - branchDistance:0.0 - coverage:25.5 - ex: 0 - tex: 16
[MASTER] 11:17:02.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.10262271414823, number of tests: 10, total length: 234
[MASTER] 11:17:07.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:69.10262271414823 - branchDistance:0.0 - coverage:24.5 - ex: 0 - tex: 20
[MASTER] 11:17:07.147 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.10262271414823, number of tests: 11, total length: 278
[MASTER] 11:17:07.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:68.10262271414823 - branchDistance:0.0 - coverage:23.5 - ex: 0 - tex: 18
[MASTER] 11:17:07.827 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.10262271414823, number of tests: 9, total length: 228
[MASTER] 11:17:08.165 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:66.10262271414823 - branchDistance:0.0 - coverage:21.5 - ex: 0 - tex: 18
[MASTER] 11:17:08.165 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.10262271414823, number of tests: 10, total length: 246
[MASTER] 11:17:24.240 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:64.10262271414823 - branchDistance:0.0 - coverage:19.5 - ex: 0 - tex: 18
[MASTER] 11:17:24.241 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.10262271414823, number of tests: 11, total length: 263
[MASTER] 11:17:27.849 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:63.60262271414822 - branchDistance:0.0 - coverage:19.0 - ex: 0 - tex: 22
[MASTER] 11:17:27.863 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.60262271414822, number of tests: 12, total length: 289
[MASTER] 11:17:30.674 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:62.10262271414822 - branchDistance:0.0 - coverage:17.5 - ex: 0 - tex: 20
[MASTER] 11:17:30.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.10262271414822, number of tests: 11, total length: 274
[MASTER] 11:17:44.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:61.60262271414822 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 22
[MASTER] 11:17:44.279 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.60262271414822, number of tests: 11, total length: 284
[MASTER] 11:18:25.930 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:59.60262271414822 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 22
[MASTER] 11:18:25.953 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.60262271414822, number of tests: 11, total length: 280
[MASTER] 11:18:55.938 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998496240601504 - fitness:58.10262271414822 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 24
[MASTER] 11:18:55.944 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.10262271414822, number of tests: 12, total length: 302
[MASTER] 11:19:30.776 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 67 generations, 69743 statements, best individuals have fitness: 58.10262271414822
[MASTER] 11:19:30.807 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 74%
* Total number of goals: 53
* Number of covered goals: 39
* Generated 12 tests with total length 295
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     58 / 0           
	- MaxTime :                        304 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 11:19:33.503 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:19:33.992 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 256 
[MASTER] 11:19:35.428 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.429 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.436 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.440 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.441 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.448 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.449 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.449 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.450 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.452 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.459 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 11:19:35.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 11:19:35.461 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 11:19:35.467 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 11:19:35.468 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 11:19:35.469 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 11:19:35.469 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 11:19:35.469 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 11:19:35.471 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 11:19:35.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 11:19:35.479 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 11:19:35.480 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 11:19:35.488 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 11:19:35.500 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 11:19:35.516 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing tests to file
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
