* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting Client-0
* Connecting to master process on port 5856
* Analyzing classpath: 
  - ../../../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 11:44:28.398 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 53
*** Random test generation finished.
*=*=*=* Total tests: 4503 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 11:49:29.512 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 11:49:30.307 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:49:30.317 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 212 seconds more than allowed.
[MASTER] 11:49:30.434 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 11:49:30.459 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 11:49:30.527 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 5
* Writing tests to file
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
