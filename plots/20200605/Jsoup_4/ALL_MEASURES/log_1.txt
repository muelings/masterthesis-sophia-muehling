* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting Client-0
* Connecting to master process on port 12845
* Analyzing classpath: 
  - ../../../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:10:33.251 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:10:33.302 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 28
* Using seed 1591341002822
[Progress:>                             0%] [Cov:>                                  0%]* Starting evolution
[MASTER] 09:10:46.026 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:108.0 - branchDistance:0.0 - coverage:47.0 - ex: 0 - tex: 18
[MASTER] 09:10:46.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.0, number of tests: 9, total length: 211
[MASTER] 09:10:49.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:98.98058252427185 - branchDistance:0.0 - coverage:37.980582524271846 - ex: 0 - tex: 16
[MASTER] 09:10:49.277 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.98058252427185, number of tests: 10, total length: 171
[MASTER] 09:10:49.733 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:96.98058252427185 - branchDistance:0.0 - coverage:35.980582524271846 - ex: 0 - tex: 10
[MASTER] 09:10:49.739 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.98058252427185, number of tests: 6, total length: 84
[MASTER] 09:10:51.293 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.98058252427185 - branchDistance:0.0 - coverage:34.980582524271846 - ex: 0 - tex: 8
[MASTER] 09:10:51.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.98058252427185, number of tests: 4, total length: 80
[MASTER] 09:10:53.128 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:92.0 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 14
[MASTER] 09:10:53.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.0, number of tests: 9, total length: 149
[MASTER] 09:10:54.868 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.0 - branchDistance:0.0 - coverage:28.0 - ex: 0 - tex: 14
[MASTER] 09:10:54.869 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.0, number of tests: 8, total length: 153
[MASTER] 09:11:00.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:78.0 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 20
[MASTER] 09:11:00.553 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.0, number of tests: 10, total length: 302
[MASTER] 09:11:20.089 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:76.0 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 14
[MASTER] 09:11:20.098 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.0, number of tests: 8, total length: 142
[MASTER] 09:11:27.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.99996948172335 - branchDistance:0.0 - coverage:11.999969481723355 - ex: 0 - tex: 12
[MASTER] 09:11:27.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.99996948172335, number of tests: 7, total length: 112
[MASTER] 09:11:44.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.99996948149052 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 10
[MASTER] 09:11:44.886 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.99996948149052, number of tests: 7, total length: 104
[MASTER] 09:12:03.484 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999983748108687 - fitness:32.99998031632343 - branchDistance:0.0 - coverage:11.999969481723355 - ex: 0 - tex: 14
[MASTER] 09:12:03.485 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99998031632343, number of tests: 8, total length: 111
[MASTER] 09:12:07.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999983748108687 - fitness:32.9999803160906 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 14
[MASTER] 09:12:07.152 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.9999803160906, number of tests: 8, total length: 131
[MASTER] 09:12:12.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999983830517958 - fitness:32.99998026138386 - branchDistance:0.0 - coverage:11.999969481723355 - ex: 0 - tex: 12
[MASTER] 09:12:12.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99998026138386, number of tests: 8, total length: 88
[MASTER] 09:12:17.618 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999993225304031 - fitness:32.99997399795552 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 10
[MASTER] 09:12:17.618 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99997399795552, number of tests: 7, total length: 88
[MASTER] 09:12:18.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.999999323139361 - fitness:32.99997399412864 - branchDistance:0.0 - coverage:11.999969481723355 - ex: 0 - tex: 12
[MASTER] 09:12:18.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99997399412864, number of tests: 8, total length: 89
[MASTER] 09:12:19.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999993247919514 - fitness:32.99997398287853 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 10
[MASTER] 09:12:19.113 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99997398287853, number of tests: 7, total length: 135
[MASTER] 09:12:21.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999993309143658 - fitness:32.99997394206241 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 10
[MASTER] 09:12:21.576 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.99997394206241, number of tests: 7, total length: 99
[MASTER] 09:12:23.073 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999171156237048 - fitness:29.000522061128493 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 12
[MASTER] 09:12:23.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.000522061128493, number of tests: 7, total length: 125
[MASTER] 09:12:27.418 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999983830517958 - fitness:28.999980263013676 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 12
[MASTER] 09:12:27.418 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.999980263013676, number of tests: 8, total length: 108
[MASTER] 09:12:29.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999993247919514 - fitness:28.999973984741175 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 12
[MASTER] 09:12:29.475 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.999973984741175, number of tests: 8, total length: 144
[MASTER] 09:12:38.504 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999993309143658 - fitness:28.999973943925056 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 12
[MASTER] 09:12:38.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.999973943925056, number of tests: 8, total length: 117
[MASTER] 09:12:47.825 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999993319013751 - fitness:28.999973937344993 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 10
[MASTER] 09:12:47.831 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.999973937344993, number of tests: 8, total length: 140
[MASTER] 09:13:03.447 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.666666666666667 - fitness:17.99996948335317 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 10
[MASTER] 09:13:03.447 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.99996948335317, number of tests: 8, total length: 162
[MASTER] 09:13:49.453 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.666666666666667 - fitness:16.826056439874908 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:13:49.454 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.826056439874908, number of tests: 5, total length: 71
[MASTER] 09:14:03.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.666666666666667 - fitness:15.92304640643009 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:14:03.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.92304640643009, number of tests: 4, total length: 72
[MASTER] 09:14:05.101 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.666666666666666 - fitness:15.206866035077308 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:14:05.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.206866035077308, number of tests: 4, total length: 75
[MASTER] 09:14:21.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.666666666666666 - fitness:14.142826626210312 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:14:21.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.142826626210312, number of tests: 4, total length: 65
[MASTER] 09:14:37.241 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.8 - fitness:14.084715246065034 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:14:37.242 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.084715246065034, number of tests: 4, total length: 67
[MASTER] 09:14:51.962 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.666666666666666 - fitness:13.736811588616327 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 09:14:51.963 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.736811588616327, number of tests: 4, total length: 68
[MASTER] 09:14:58.335 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.8 - fitness:13.68746948335317 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 09:14:58.335 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.68746948335317, number of tests: 4, total length: 65
[MASTER] 09:15:05.192 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.5 - fitness:13.444413927797614 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:15:05.192 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.444413927797614, number of tests: 5, total length: 90
[MASTER] 09:15:15.415 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.666666666666666 - fitness:13.390213385792194 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:15:15.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.390213385792194, number of tests: 5, total length: 91
[MASTER] 09:15:31.802 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.5 - fitness:13.137900517835927 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:15:31.802 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.137900517835927, number of tests: 5, total length: 90

* Search finished after 301s and 120 generations, 81975 statements, best individuals have fitness: 13.137900517835927
[MASTER] 09:15:34.484 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:15:34.502 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 82%
* Total number of goals: 28
* Number of covered goals: 23
* Generated 5 tests with total length 76
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        303 / 300          Finished!
	- ZeroFitness :                     13 / 0           
[MASTER] 09:15:37.136 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:15:37.370 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 52 
[MASTER] 09:15:37.884 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 09:15:37.904 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 09:15:37.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:15:37.916 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:15:37.916 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:15:37.916 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 09:15:37.923 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 09:15:37.926 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:15:37.928 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing tests to file
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
