* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting Client-0
* Connecting to master process on port 12868
* Analyzing classpath: 
  - ../../../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:18:14.049 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:18:14.094 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 28
* Using seed 1591341462270
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 09:18:24.755 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:114.0 - branchDistance:0.0 - coverage:53.0 - ex: 0 - tex: 12
[MASTER] 09:18:24.760 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.0, number of tests: 6, total length: 134
[MASTER] 09:18:26.860 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:94.0 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 16
[MASTER] 09:18:26.869 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.0, number of tests: 8, total length: 117
[MASTER] 09:18:33.357 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.0 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 12
[MASTER] 09:18:33.358 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0, number of tests: 6, total length: 185
[MASTER] 09:18:35.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:76.28333333333333 - branchDistance:0.0 - coverage:15.283333333333333 - ex: 0 - tex: 4
[MASTER] 09:18:35.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.28333333333333, number of tests: 2, total length: 51
[MASTER] 09:18:40.559 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:47.14285714285714 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 18
[MASTER] 09:18:40.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.14285714285714, number of tests: 10, total length: 165
[MASTER] 09:18:56.776 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:33.14285714285714 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 12
[MASTER] 09:18:56.777 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.14285714285714, number of tests: 8, total length: 135
[MASTER] 09:19:24.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:30.142826624813328 - branchDistance:0.0 - coverage:11.999969481956185 - ex: 0 - tex: 6
[MASTER] 09:19:24.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.142826624813328, number of tests: 6, total length: 71
[MASTER] 09:19:28.785 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.5 - fitness:29.333333333333332 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 10
[MASTER] 09:19:28.785 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.333333333333332, number of tests: 7, total length: 112
[MASTER] 09:19:49.703 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:24.181818181818183 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 8
[MASTER] 09:19:49.705 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.181818181818183, number of tests: 7, total length: 75
[MASTER] 09:19:56.083 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.8333333333333335 - fitness:21.41377784512486 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 4
[MASTER] 09:19:56.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.41377784512486, number of tests: 6, total length: 62
[MASTER] 09:20:10.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.833333333333333 - fitness:19.28569902739087 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 6
[MASTER] 09:20:10.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.28569902739087, number of tests: 6, total length: 65
[MASTER] 09:20:13.159 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.833333333333333 - fitness:17.780472546554634 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 4
[MASTER] 09:20:13.159 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.780472546554634, number of tests: 5, total length: 57
[MASTER] 09:20:24.613 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.121212121212121 - fitness:14.928128454251436 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 6
[MASTER] 09:20:24.613 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.928128454251436, number of tests: 5, total length: 51
[MASTER] 09:20:31.735 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.121212121212121 - fitness:14.395080109523995 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 6
[MASTER] 09:20:31.735 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.395080109523995, number of tests: 6, total length: 60
[MASTER] 09:20:34.147 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.121212121212121 - fitness:13.949984741676584 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 6
[MASTER] 09:20:34.155 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.949984741676584, number of tests: 5, total length: 50
[MASTER] 09:20:36.603 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.121212121212121 - fitness:13.572733009574968 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 4
[MASTER] 09:20:36.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.572733009574968, number of tests: 5, total length: 57
[MASTER] 09:20:42.460 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.196969696969697 - fitness:13.546483019518146 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 6
[MASTER] 09:20:42.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.546483019518146, number of tests: 5, total length: 50
[MASTER] 09:20:47.663 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.196969696969697 - fitness:13.226238743811056 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 4
[MASTER] 09:20:47.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.226238743811056, number of tests: 4, total length: 52
[MASTER] 09:20:53.383 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.196969696969697 - fitness:12.948140275076385 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 4
[MASTER] 09:20:53.384 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.948140275076385, number of tests: 4, total length: 56
[MASTER] 09:21:04.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.196969696969697 - fitness:12.94812501675297 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:21:04.772 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.94812501675297, number of tests: 4, total length: 56
[MASTER] 09:21:14.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.196969696969697 - fitness:12.704366115719868 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 09:21:14.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.704366115719868, number of tests: 5, total length: 80
[MASTER] 09:21:16.861 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.237878787878788 - fitness:12.695033400494161 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:21:16.861 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.695033400494161, number of tests: 4, total length: 78
[MASTER] 09:21:25.391 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.23787878787879 - fitness:12.480691430610399 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 4
[MASTER] 09:21:25.391 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.480691430610399, number of tests: 4, total length: 72
[MASTER] 09:21:29.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.23787878787879 - fitness:12.480676172286984 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:21:29.239 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.480676172286984, number of tests: 4, total length: 74
[MASTER] 09:21:41.123 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.23787878787879 - fitness:12.118816455078774 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 09:21:41.123 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.118816455078774, number of tests: 4, total length: 61
[MASTER] 09:21:43.227 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.23787878787879 - fitness:11.964707074129542 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:21:43.228 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.964707074129542, number of tests: 4, total length: 67
[MASTER] 09:21:51.377 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.23787878787879 - fitness:11.698068549919906 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 09:21:51.379 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.698068549919906, number of tests: 4, total length: 58
[MASTER] 09:22:00.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.23787878787879 - fitness:11.581976004635443 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 2
[MASTER] 09:22:00.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.581976004635443, number of tests: 4, total length: 50
[MASTER] 09:22:04.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.23787878787879 - fitness:11.581960746312028 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 09:22:04.739 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.581960746312028, number of tests: 4, total length: 56
[MASTER] 09:22:10.548 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.23787878787879 - fitness:11.475448891204621 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 2
[MASTER] 09:22:10.555 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.475448891204621, number of tests: 4, total length: 49
[MASTER] 09:22:13.462 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.23787878787879 - fitness:11.475433632881206 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:22:13.468 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.475433632881206, number of tests: 4, total length: 54
[MASTER] 09:22:22.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.23787878787879 - fitness:11.377348363103424 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 09:22:22.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.377348363103424, number of tests: 4, total length: 57
[MASTER] 09:22:24.417 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.484848484848484 - fitness:11.265415707609462 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:22:24.417 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.265415707609462, number of tests: 4, total length: 58
[MASTER] 09:22:29.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.703896103896103 - fitness:11.246832628961574 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 09:22:29.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.246832628961574, number of tests: 4, total length: 58
[MASTER] 09:22:53.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.720562770562772 - fitness:11.245431171985722 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 09:22:53.182 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.245431171985722, number of tests: 4, total length: 50
[MASTER] 09:23:15.271 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 120 generations, 73002 statements, best individuals have fitness: 11.245431171985722
[MASTER] 09:23:15.334 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 82%
* Total number of goals: 28
* Number of covered goals: 23
* Generated 3 tests with total length 46
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     11 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        303 / 300          Finished!
[MASTER] 09:23:17.863 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:23:18.221 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 37 
[MASTER] 09:23:19.106 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 09:23:19.111 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:23:19.112 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:23:19.127 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:23:19.146 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:23:19.151 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing tests to file
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
