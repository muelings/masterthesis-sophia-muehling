* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting Client-0
* Connecting to master process on port 21623
* Analyzing classpath: 
  - ../../../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 09:31:22.017 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 28
[MASTER] 09:31:27.719 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&rpar;H4q&rcub;&Hat;AZ", var11);  // (Primitive) Original Value: &rpar;H4q&rcub;&Hat;AZ | Regression Value: &rpar;H4q&rcub;&hat;AZ
[MASTER] 09:31:27.746 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 09:31:27.759 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:31:37.552 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:31:37.769 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&rpar;H4q&rcub;&Hat;AZ", var11);  // (Primitive) Original Value: &rpar;H4q&rcub;&Hat;AZ | Regression Value: &rpar;H4q&rcub;&hat;AZ
[MASTER] 09:31:37.773 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 09:31:37.788 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 09:31:39.542 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:31:39.721 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("&rpar;H4q&rcub;&Hat;AZ", var11);  // (Primitive) Original Value: &rpar;H4q&rcub;&Hat;AZ | Regression Value: &rpar;H4q&rcub;&hat;AZ
[MASTER] 09:31:39.751 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 09:31:39.752 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 26 | Tests with assertion: 1
* Generated 1 tests with total length 14
[MASTER] 09:31:39.792 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                         17 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 09:31:47.284 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:31:47.525 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 12 
[MASTER] 09:31:47.793 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 09:31:50.061 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:31:50.098 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 29%
* Total number of goals: 28
* Number of covered goals: 8
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
