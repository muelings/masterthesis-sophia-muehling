* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting Client-0
* Connecting to master process on port 11470
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 17:34:00.147 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 17:34:00.211 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 59
* Using seed 1591284818823
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 17:34:09.525 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:144.25 - branchDistance:0.0 - coverage:100.5 - ex: 0 - tex: 14
[MASTER] 17:34:09.531 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.25, number of tests: 7, total length: 116
[MASTER] 17:34:21.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:117.55 - branchDistance:0.0 - coverage:73.8 - ex: 0 - tex: 18
[MASTER] 17:34:21.979 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.55, number of tests: 10, total length: 190
[MASTER] 17:35:47.183 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:115.75 - branchDistance:0.0 - coverage:72.0 - ex: 0 - tex: 20
[MASTER] 17:35:47.188 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.75, number of tests: 10, total length: 239
[MASTER] 17:35:57.280 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:112.41666666666667 - branchDistance:0.0 - coverage:68.66666666666667 - ex: 0 - tex: 10
[MASTER] 17:35:57.281 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.41666666666667, number of tests: 10, total length: 153
[MASTER] 17:36:24.377 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:112.41666666573533 - branchDistance:0.0 - coverage:68.66666666573533 - ex: 0 - tex: 8
[MASTER] 17:36:24.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.41666666573533, number of tests: 8, total length: 188
[MASTER] 17:36:26.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:111.91666666573533 - branchDistance:0.0 - coverage:68.16666666573533 - ex: 0 - tex: 10
[MASTER] 17:36:26.656 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.91666666573533, number of tests: 7, total length: 186
[MASTER] 17:36:30.126 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:111.583333332402 - branchDistance:0.0 - coverage:67.833333332402 - ex: 0 - tex: 10
[MASTER] 17:36:30.142 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.583333332402, number of tests: 8, total length: 202
[MASTER] 17:36:45.476 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:96.693333332402 - branchDistance:0.0 - coverage:68.333333332402 - ex: 0 - tex: 14
[MASTER] 17:36:45.477 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.693333332402, number of tests: 10, total length: 218
[MASTER] 17:37:46.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:96.52666666573535 - branchDistance:0.0 - coverage:68.16666666573533 - ex: 0 - tex: 14
[MASTER] 17:37:46.316 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.52666666573535, number of tests: 10, total length: 226
[MASTER] 17:38:05.168 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:96.14787878694747 - branchDistance:0.0 - coverage:67.78787878694746 - ex: 0 - tex: 14
[MASTER] 17:38:05.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.14787878694747, number of tests: 11, total length: 227
[MASTER] 17:38:06.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:95.693333332402 - branchDistance:0.0 - coverage:67.333333332402 - ex: 0 - tex: 14
[MASTER] 17:38:06.886 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.693333332402, number of tests: 10, total length: 219
[MASTER] 17:38:26.176 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:95.62190476097345 - branchDistance:0.0 - coverage:67.26190476097344 - ex: 0 - tex: 10
[MASTER] 17:38:26.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.62190476097345, number of tests: 9, total length: 211
[MASTER] 17:38:27.881 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:95.14787878694747 - branchDistance:0.0 - coverage:66.78787878694746 - ex: 0 - tex: 14
[MASTER] 17:38:27.881 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.14787878694747, number of tests: 11, total length: 228
[MASTER] 17:38:36.474 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:92.14102564102564 - branchDistance:0.0 - coverage:64.83333333333333 - ex: 0 - tex: 14
[MASTER] 17:38:36.476 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.14102564102564, number of tests: 10, total length: 222
[MASTER] 17:38:41.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:92.06959706959707 - branchDistance:0.0 - coverage:64.76190476190476 - ex: 0 - tex: 12
[MASTER] 17:38:41.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.06959706959707, number of tests: 11, total length: 228
[MASTER] 17:39:01.467 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 21 generations, 21358 statements, best individuals have fitness: 92.06959706959707
[MASTER] 17:39:01.486 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 36%
* Total number of goals: 59
* Number of covered goals: 21
* Generated 10 tests with total length 205
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        303 / 300          Finished!
	- ZeroFitness :                     92 / 0           
	- RMIStoppingCondition
[MASTER] 17:39:06.113 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:39:07.901 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 145 
[MASTER] 17:39:08.439 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 17:39:08.444 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 17:39:08.445 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 17:39:08.446 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 17:39:08.459 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 17:39:08.460 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 17:39:08.461 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 17:39:08.462 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 17:39:08.467 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 17:39:08.468 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 17:39:08.469 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 17:39:08.471 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 17:39:08.471 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 17:39:08.471 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 17:39:08.480 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 17:39:08.485 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 17:39:08.497 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 59
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing tests to file
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
