* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting Client-0
* Connecting to master process on port 19836
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 17:58:07.727 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 17:58:07.782 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 59
* Using seed 1591286266274
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 17:58:15.577 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:145.75 - branchDistance:0.0 - coverage:102.0 - ex: 0 - tex: 2
[MASTER] 17:58:15.578 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 145.75, number of tests: 1, total length: 21
[MASTER] 17:58:27.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:121.0 - branchDistance:0.0 - coverage:97.5 - ex: 0 - tex: 10
[MASTER] 17:58:27.999 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 5, total length: 98
[MASTER] 18:00:00.016 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:115.0 - branchDistance:0.0 - coverage:91.5 - ex: 0 - tex: 8
[MASTER] 18:00:00.016 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.0, number of tests: 6, total length: 120
[MASTER] 18:00:11.187 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:109.5 - branchDistance:0.0 - coverage:86.0 - ex: 0 - tex: 10
[MASTER] 18:00:11.198 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.5, number of tests: 7, total length: 132
[MASTER] 18:00:15.150 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:103.375 - branchDistance:0.0 - coverage:79.875 - ex: 0 - tex: 12
[MASTER] 18:00:15.179 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.375, number of tests: 9, total length: 176
[MASTER] 18:00:38.581 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:101.5 - branchDistance:0.0 - coverage:78.0 - ex: 0 - tex: 10
[MASTER] 18:00:38.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.5, number of tests: 6, total length: 100
[MASTER] 18:00:40.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:95.5 - branchDistance:0.0 - coverage:72.0 - ex: 0 - tex: 12
[MASTER] 18:00:40.173 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.5, number of tests: 7, total length: 135
[MASTER] 18:00:44.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:94.66666666666666 - branchDistance:0.0 - coverage:71.16666666666666 - ex: 0 - tex: 14
[MASTER] 18:00:44.215 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.66666666666666, number of tests: 8, total length: 130
[MASTER] 18:00:45.390 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:91.833333332402 - branchDistance:0.0 - coverage:68.333333332402 - ex: 0 - tex: 16
[MASTER] 18:00:45.396 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.833333332402, number of tests: 8, total length: 145
[MASTER] 18:00:56.930 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:91.5 - branchDistance:0.0 - coverage:68.0 - ex: 0 - tex: 12
[MASTER] 18:00:56.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.5, number of tests: 8, total length: 174
[MASTER] 18:01:11.344 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:90.66666666573533 - branchDistance:0.0 - coverage:67.16666666573533 - ex: 0 - tex: 10
[MASTER] 18:01:11.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.66666666573533, number of tests: 7, total length: 150
[MASTER] 18:01:13.430 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:88.16666666666666 - branchDistance:0.0 - coverage:64.66666666666666 - ex: 0 - tex: 14
[MASTER] 18:01:13.436 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.16666666666666, number of tests: 7, total length: 139
[MASTER] 18:01:27.425 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:87.5 - branchDistance:0.0 - coverage:64.0 - ex: 0 - tex: 14
[MASTER] 18:01:27.435 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.5, number of tests: 7, total length: 141
[MASTER] 18:01:34.027 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:87.3 - branchDistance:0.0 - coverage:63.8 - ex: 0 - tex: 12
[MASTER] 18:01:34.028 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.3, number of tests: 9, total length: 197
[MASTER] 18:01:46.137 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:86.39393939393938 - branchDistance:0.0 - coverage:64.66666666666666 - ex: 0 - tex: 12
[MASTER] 18:01:46.146 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.39393939393938, number of tests: 8, total length: 165
[MASTER] 18:01:48.293 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:85.49999999906868 - branchDistance:0.0 - coverage:61.99999999906868 - ex: 0 - tex: 12
[MASTER] 18:01:48.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.49999999906868, number of tests: 8, total length: 171
[MASTER] 18:02:02.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:83.59120879120879 - branchDistance:0.0 - coverage:63.8 - ex: 0 - tex: 16
[MASTER] 18:02:02.683 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.59120879120879, number of tests: 9, total length: 158
[MASTER] 18:02:06.229 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:83.59120879027746 - branchDistance:0.0 - coverage:63.799999999068675 - ex: 0 - tex: 16
[MASTER] 18:02:06.232 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.59120879027746, number of tests: 9, total length: 176
[MASTER] 18:02:12.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:81.79120879027747 - branchDistance:0.0 - coverage:61.99999999906868 - ex: 0 - tex: 16
[MASTER] 18:02:12.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.79120879027747, number of tests: 8, total length: 163
[MASTER] 18:02:19.784 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:81.16620878934614 - branchDistance:0.0 - coverage:61.374999998137355 - ex: 0 - tex: 18
[MASTER] 18:02:19.799 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.16620878934614, number of tests: 9, total length: 184
[MASTER] 18:02:23.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:80.95787545694414 - branchDistance:0.0 - coverage:61.16666666573534 - ex: 0 - tex: 18
[MASTER] 18:02:23.188 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.95787545694414, number of tests: 10, total length: 181
[MASTER] 18:02:40.476 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:78.79120879120879 - branchDistance:0.0 - coverage:59.0 - ex: 0 - tex: 16
[MASTER] 18:02:40.482 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.79120879120879, number of tests: 8, total length: 161
[MASTER] 18:03:03.793 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:75.45787545787546 - branchDistance:0.0 - coverage:55.666666666666664 - ex: 0 - tex: 18
[MASTER] 18:03:03.793 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.45787545787546, number of tests: 9, total length: 191
[MASTER] 18:03:07.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.066666666666666 - fitness:72.79120879120879 - branchDistance:0.0 - coverage:53.0 - ex: 0 - tex: 18
[MASTER] 18:03:08.001 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.79120879120879, number of tests: 9, total length: 187
[MASTER] 18:03:09.367 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 37 generations, 38012 statements, best individuals have fitness: 72.79120879120879
[MASTER] 18:03:09.408 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 44%
* Total number of goals: 59
* Number of covered goals: 26
* Generated 9 tests with total length 187
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        304 / 300          Finished!
	- ZeroFitness :                     73 / 0           
[MASTER] 18:03:12.312 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:03:12.742 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 134 
[MASTER] 18:03:13.308 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.316 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.317 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.318 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.331 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.333 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.334 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.339 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.341 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 18:03:13.342 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 18:03:13.350 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 18:03:13.359 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 18:03:13.373 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 18:03:13.375 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 18:03:13.380 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 18:03:13.395 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 18:03:13.396 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 18:03:13.401 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 18:03:13.409 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 18:03:13.419 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 59
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing tests to file
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
