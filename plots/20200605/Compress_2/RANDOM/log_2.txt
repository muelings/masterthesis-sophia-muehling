* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting Client-0
* Connecting to master process on port 8094
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 17:28:22.502 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 59
[MASTER] 17:28:44.709 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:29:07.800 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:29:24.097 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:29:41.134 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:30:02.691 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:30:18.993 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:30:36.261 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:30:55.872 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:31:12.801 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:31:30.434 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:31:47.364 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:32:05.104 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:32:21.493 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:32:37.780 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:32:54.436 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:33:11.151 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 17:33:28.337 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
*** Random test generation finished.
*=*=*=* Total tests: 571 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 17:33:28.360 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                        306 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 17:33:28.897 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:33:28.914 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 207 seconds more than allowed.
[MASTER] 17:33:28.977 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 17:33:29.020 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 17:33:29.073 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 59
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing tests to file
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
