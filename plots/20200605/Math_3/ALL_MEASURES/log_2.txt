* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.util.MathArrays
* Starting Client-0
* Connecting to master process on port 20050
* Analyzing classpath: 
  - ../../../defects4j_compiled/Math_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.util.MathArrays
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:49:57.954 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:49:58.032 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 257
* Using seed 1591256957307
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 09:50:04.594 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:985.3333333333333 - branchDistance:0.0 - coverage:414.3333333333333 - ex: 0 - tex: 6
[MASTER] 09:50:04.595 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 985.3333333333333, number of tests: 6, total length: 128
* Computation finished
