* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.util.MathArrays
* Starting Client-0
* Connecting to master process on port 7102
* Analyzing classpath: 
  - ../../../defects4j_compiled/Math_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.util.MathArrays
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 09:45:44.514 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 257
[MASTER] 09:45:48.371 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:45:55.472 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/util/MathArrays_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:45:55.519 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 09:45:56.941 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/util/MathArrays_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:45:56.993 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 18 | Tests with assertion: 1
* Generated 1 tests with total length 6
[MASTER] 09:45:57.039 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         12 / 300         
	- RMIStoppingCondition
[MASTER] 09:46:12.525 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:46:12.622 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 5 
[MASTER] 09:46:12.739 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 09:46:12.740 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 09:46:12.741 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: linearCombination:ArrayIndexOutOfBoundsException at 2
[MASTER] 09:46:12.741 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [linearCombination:ArrayIndexOutOfBoundsException]
[MASTER] 09:46:13.148 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:46:13.173 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 1%
* Total number of goals: 257
* Number of covered goals: 2
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'MathArrays_ESTest' to evosuite-tests
* Done!

* Computation finished
