* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Starting Client-0
* Connecting to master process on port 21272
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonDatabind_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:30:12.942 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:30:13.030 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 288
* Using seed 1591338575794
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 08:30:14.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1305.0 - branchDistance:0.0 - coverage:652.0 - ex: 0 - tex: 0
[MASTER] 08:30:14.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1305.0, number of tests: 1, total length: 0
[MASTER] 08:35:17.092 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 305s and 31963 generations, 0 statements, best individuals have fitness: 1305.0
[MASTER] 08:35:17.108 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 2 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                  1,305 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        307 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 08:35:20.442 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:35:20.701 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 08:35:20.751 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:35:20.761 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:35:20.771 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:35:20.774 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'AnnotatedClass_ESTest' to evosuite-tests
* Done!

* Computation finished
