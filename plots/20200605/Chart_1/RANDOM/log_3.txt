* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.renderer.category.AbstractCategoryItemRenderer
* Starting Client-0
* Connecting to master process on port 12680
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_1_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.renderer.category.AbstractCategoryItemRenderer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 02:02:56.509 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 274
[MASTER] 02:03:20.402 [logback-1] WARN  TestUsageChecker - class org.jfree.chart.renderer.category.MinMaxCategoryRenderer$1 looks like an anonymous class, ignoring it (although reflection says false) MinMaxCategoryRenderer$1
[MASTER] 02:03:38.034 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 02:04:28.605 [logback-1] WARN  TestUsageChecker - class org.jfree.chart.renderer.category.MinMaxCategoryRenderer$2 looks like an anonymous class, ignoring it (although reflection says false) MinMaxCategoryRenderer$2
[MASTER] 02:04:41.326 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:07:52.348 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
*** Random test generation finished.
*=*=*=* Total tests: 1056 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 02:07:57.801 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 02:07:58.546 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:07:58.560 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 313 seconds more than allowed.
[MASTER] 02:07:58.649 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 02:07:58.666 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:07:58.732 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 274
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.io.FilePermission: 
         write /tmp/+~JT8756056066799932879.tmp: 1
         write /tmp/+~JT289956978989019287.tmp: 1
         execute /usr/bin/xprop: 1
  - java.lang.RuntimePermission: 
         loadLibrary.javalcms: 1
* Writing tests to file
* Writing JUnit test case 'AbstractCategoryItemRenderer_ESTest' to evosuite-tests
* Done!

* Computation finished
