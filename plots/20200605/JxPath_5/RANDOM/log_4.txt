* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.NodePointer
* Starting Client-0
* Connecting to master process on port 4589
* Analyzing classpath: 
  - ../../../defects4j_compiled/JxPath_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.NodePointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 12:35:13.502 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 158
[MASTER] 12:36:13.011 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
* Computation finished
