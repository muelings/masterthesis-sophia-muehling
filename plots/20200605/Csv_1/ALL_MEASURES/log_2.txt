* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting Client-0
* Connecting to master process on port 4534
* Analyzing classpath: 
  - ../../../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 22:26:30.114 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 22:26:30.203 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
* Using seed 1591302369646
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 22:26:47.667 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:103.76666666666667 - branchDistance:0.0 - coverage:40.766666666666666 - ex: 0 - tex: 12
[MASTER] 22:26:47.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.76666666666667, number of tests: 7, total length: 152
[MASTER] 22:27:01.210 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.2 - branchDistance:0.0 - coverage:32.2 - ex: 0 - tex: 14
[MASTER] 22:27:01.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.2, number of tests: 9, total length: 271
[MASTER] 22:27:18.330 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:88.68857142857144 - branchDistance:0.0 - coverage:25.68857142857143 - ex: 0 - tex: 10
[MASTER] 22:27:18.335 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.68857142857144, number of tests: 8, total length: 174
[MASTER] 22:28:05.497 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:87.0 - branchDistance:0.0 - coverage:24.0 - ex: 0 - tex: 10
[MASTER] 22:28:05.498 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.0, number of tests: 6, total length: 160
[MASTER] 22:28:17.444 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:86.56666666666666 - branchDistance:0.0 - coverage:23.566666666666666 - ex: 0 - tex: 10
[MASTER] 22:28:17.445 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.56666666666666, number of tests: 8, total length: 177
[MASTER] 22:28:19.840 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:84.33333333333333 - branchDistance:0.0 - coverage:21.333333333333332 - ex: 0 - tex: 10
[MASTER] 22:28:19.844 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.33333333333333, number of tests: 6, total length: 158
[MASTER] 22:28:26.077 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.43333333333334 - branchDistance:0.0 - coverage:19.433333333333334 - ex: 0 - tex: 10
[MASTER] 22:28:26.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.43333333333334, number of tests: 9, total length: 165
[MASTER] 22:29:10.777 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 14
[MASTER] 22:29:10.778 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 8, total length: 240
[MASTER] 22:31:31.821 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 23 generations, 27856 statements, best individuals have fitness: 81.91304347826087
[MASTER] 22:31:31.838 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 8 tests with total length 138
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        303 / 300          Finished!
	- ZeroFitness :                     82 / 0           
	- RMIStoppingCondition
[MASTER] 22:31:35.642 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:31:36.638 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 101 
[MASTER] 22:31:36.996 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 22:31:37.008 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 22:31:37.020 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 22:31:37.035 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 22:31:37.036 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 22:31:37.037 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 22:31:37.037 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 22:31:37.038 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 22:31:37.038 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 22:31:37.038 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 22:31:37.039 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 22:31:37.047 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 22:31:37.058 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:31:37.072 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 5
* Writing tests to file
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
