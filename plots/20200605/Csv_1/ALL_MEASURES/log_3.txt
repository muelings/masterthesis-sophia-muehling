* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting Client-0
* Connecting to master process on port 20747
* Analyzing classpath: 
  - ../../../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 22:37:41.217 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 22:37:41.314 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
* Using seed 1591303040532
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 22:37:55.265 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:118.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 6
[MASTER] 22:37:55.272 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.0, number of tests: 5, total length: 81
[MASTER] 22:38:00.850 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.24334344455679 - branchDistance:0.0 - coverage:30.243343444556793 - ex: 0 - tex: 16
[MASTER] 22:38:00.864 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.24334344455679, number of tests: 9, total length: 288
[MASTER] 22:38:16.552 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.76666666666667 - branchDistance:0.0 - coverage:19.766666666666666 - ex: 0 - tex: 10
[MASTER] 22:38:16.554 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.76666666666667, number of tests: 10, total length: 132
[MASTER] 22:40:01.014 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.5848484848485 - branchDistance:0.0 - coverage:19.584848484848486 - ex: 0 - tex: 6
[MASTER] 22:40:01.023 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.5848484848485, number of tests: 7, total length: 105
[MASTER] 22:40:02.386 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.26666666666667 - branchDistance:0.0 - coverage:19.266666666666666 - ex: 0 - tex: 2
[MASTER] 22:40:02.386 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.26666666666667, number of tests: 6, total length: 74
[MASTER] 22:40:58.211 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.21435897435897 - branchDistance:0.0 - coverage:19.214358974358973 - ex: 0 - tex: 0
[MASTER] 22:40:58.217 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.21435897435897, number of tests: 6, total length: 88
[MASTER] 22:41:14.195 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.07971014492753 - branchDistance:0.0 - coverage:19.079710144927535 - ex: 0 - tex: 4
[MASTER] 22:41:14.195 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.07971014492753, number of tests: 7, total length: 107
[MASTER] 22:41:50.517 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.01744680851064 - branchDistance:0.0 - coverage:19.017446808510638 - ex: 0 - tex: 4
[MASTER] 22:41:50.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.01744680851064, number of tests: 7, total length: 107
[MASTER] 22:42:42.535 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 18 generations, 18398 statements, best individuals have fitness: 82.01744680851064
[MASTER] 22:42:42.560 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 7 tests with total length 107
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        303 / 300          Finished!
	- ZeroFitness :                     82 / 0           
[MASTER] 22:42:45.669 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:42:46.803 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 83 
[MASTER] 22:42:47.300 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 22:42:47.301 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 22:42:47.302 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 22:42:47.310 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 22:42:47.312 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 22:42:47.320 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 22:42:47.320 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 22:42:47.320 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 22:42:47.321 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 22:42:47.333 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:42:47.341 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 6
* Writing tests to file
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
