* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting Client-0
* Connecting to master process on port 8319
* Analyzing classpath: 
  - ../../../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 22:49:00.477 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 22:49:00.555 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
* Using seed 1591303716035
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 22:49:10.094 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:125.0 - branchDistance:0.0 - coverage:62.0 - ex: 0 - tex: 2
[MASTER] 22:49:10.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.0, number of tests: 1, total length: 14
[MASTER] 22:49:24.110 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:108.33333333333334 - branchDistance:0.0 - coverage:45.333333333333336 - ex: 0 - tex: 8
[MASTER] 22:49:24.115 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.33333333333334, number of tests: 4, total length: 107
[MASTER] 22:49:33.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:99.76666666666667 - branchDistance:0.0 - coverage:36.766666666666666 - ex: 0 - tex: 18
[MASTER] 22:49:33.745 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.76666666666667, number of tests: 9, total length: 258
[MASTER] 22:49:41.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:99.48095238095237 - branchDistance:0.0 - coverage:36.48095238095238 - ex: 0 - tex: 12
[MASTER] 22:49:41.579 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.48095238095237, number of tests: 7, total length: 157
[MASTER] 22:49:55.191 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:96.1 - branchDistance:0.0 - coverage:33.099999999999994 - ex: 0 - tex: 14
[MASTER] 22:49:55.199 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.1, number of tests: 8, total length: 232
[MASTER] 22:50:02.337 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.69090909090909 - branchDistance:0.0 - coverage:26.69090909090909 - ex: 0 - tex: 16
[MASTER] 22:50:02.338 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.69090909090909, number of tests: 9, total length: 241
[MASTER] 22:50:07.915 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:88.81009538950715 - branchDistance:0.0 - coverage:25.810095389507154 - ex: 0 - tex: 10
[MASTER] 22:50:07.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.81009538950715, number of tests: 7, total length: 199
[MASTER] 22:50:35.429 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.09575757575757 - branchDistance:0.0 - coverage:19.095757575757577 - ex: 0 - tex: 14
[MASTER] 22:50:35.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.09575757575757, number of tests: 10, total length: 192
[MASTER] 22:51:25.744 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.07971014492753 - branchDistance:0.0 - coverage:19.079710144927535 - ex: 0 - tex: 10
[MASTER] 22:51:25.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.07971014492753, number of tests: 8, total length: 165
[MASTER] 22:53:42.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 6
[MASTER] 22:53:42.291 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 9, total length: 149
[MASTER] 22:54:01.857 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 17 generations, 18588 statements, best individuals have fitness: 81.91304347826087
[MASTER] 22:54:01.895 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 8 tests with total length 116
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     82 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        305 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 22:54:07.780 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:54:10.163 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 90 
[MASTER] 22:54:10.612 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 22:54:10.613 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 22:54:10.619 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 22:54:10.620 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 22:54:10.621 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 22:54:10.621 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 22:54:10.622 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 22:54:10.628 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 22:54:10.628 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 22:54:10.630 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 22:54:10.630 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 22:54:10.638 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:54:10.654 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 6
* Writing tests to file
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
