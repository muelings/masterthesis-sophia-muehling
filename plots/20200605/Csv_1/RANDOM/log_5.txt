* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting Client-0
* Connecting to master process on port 16252
* Analyzing classpath: 
  - ../../../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 22:54:43.780 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
*** Random test generation finished.
*=*=*=* Total tests: 10716 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 22:59:44.844 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 22:59:45.592 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:59:45.602 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 202 seconds more than allowed.
[MASTER] 22:59:45.685 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 22:59:45.732 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:59:45.780 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing tests to file
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
