* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting Client-0
* Connecting to master process on port 16335
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 14:15:40.213 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 181
[MASTER] 14:15:52.783 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var8);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:15:52.901 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:15:52.901 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:16:00.421 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:16:00.476 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var8);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:16:00.485 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:16:00.495 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:16:01.950 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var21);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:16:01.966 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:16:01.980 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:16:03.281 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:16:03.386 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var21);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:16:03.394 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:16:03.396 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 14:16:04.585 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:16:04.695 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var21);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:16:04.707 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:16:04.708 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 166 | Tests with assertion: 1
* Generated 1 tests with total length 23
[MASTER] 14:16:04.739 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                         24 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 14:16:12.389 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:16:12.613 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 18 
[MASTER] 14:16:12.738 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 14:16:14.550 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:16:14.574 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 12%
* Total number of goals: 181
* Number of covered goals: 22
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
