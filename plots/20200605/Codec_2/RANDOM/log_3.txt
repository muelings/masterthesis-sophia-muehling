* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting Client-0
* Connecting to master process on port 5016
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 14:00:51.798 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 181
[MASTER] 14:01:09.750 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 14:01:32.634 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 14:01:40.099 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var25);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:01:40.101 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var25 == var18);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 14:01:40.119 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:01:40.145 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 14:01:47.908 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:01:48.033 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var25);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:01:48.033 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var25 == var18);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 14:01:48.042 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:01:48.047 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 14:01:49.464 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:01:49.594 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var25);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 14:01:49.599 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var25 == var18);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 14:01:49.608 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:01:49.616 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 233 | Tests with assertion: 1
* Generated 1 tests with total length 26
[MASTER] 14:01:49.640 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                         58 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 14:01:59.740 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:02:00.109 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 17 
[MASTER] 14:02:00.340 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[], EqualsAssertion=[]}
[MASTER] 14:02:02.487 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:02:02.516 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 13%
* Total number of goals: 181
* Number of covered goals: 23
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
