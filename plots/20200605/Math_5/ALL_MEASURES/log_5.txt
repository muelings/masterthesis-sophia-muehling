* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting Client-0
* Connecting to master process on port 7043
* Analyzing classpath: 
  - ../../../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:52:57.656 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:52:57.729 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 183
* Using seed 1591260748165
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 10:53:23.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:516.9999999962747 - branchDistance:0.0 - coverage:87.99999999627471 - ex: 0 - tex: 2
[MASTER] 10:53:23.406 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 516.9999999962747, number of tests: 10, total length: 155
[MASTER] 10:53:24.234 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:220.0999999947827 - branchDistance:0.0 - coverage:133.49999999478268 - ex: 0 - tex: 0
[MASTER] 10:53:24.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 220.0999999947827, number of tests: 4, total length: 71
[MASTER] 10:53:47.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:184.9333333276523 - branchDistance:0.0 - coverage:98.33333332765227 - ex: 0 - tex: 0
[MASTER] 10:53:47.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 184.9333333276523, number of tests: 7, total length: 122
[MASTER] 10:53:48.767 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:171.81428160326556 - branchDistance:0.0 - coverage:85.21428160326555 - ex: 0 - tex: 6
[MASTER] 10:53:48.776 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 171.81428160326556, number of tests: 9, total length: 186
[MASTER] 10:53:52.389 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:163.595381060404 - branchDistance:0.0 - coverage:76.99538106040401 - ex: 0 - tex: 0
[MASTER] 10:53:52.412 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 163.595381060404, number of tests: 10, total length: 140
[MASTER] 10:53:58.023 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:161.5999999933362 - branchDistance:0.0 - coverage:74.99999999333622 - ex: 0 - tex: 4
[MASTER] 10:53:58.036 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 161.5999999933362, number of tests: 9, total length: 200
[MASTER] 10:54:24.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:141.93333332949703 - branchDistance:0.0 - coverage:55.33333332949702 - ex: 0 - tex: 0
[MASTER] 10:54:24.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.93333332949703, number of tests: 10, total length: 267
[MASTER] 10:54:53.305 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:139.9333333276344 - branchDistance:0.0 - coverage:53.33333332763438 - ex: 0 - tex: 0
[MASTER] 10:54:53.306 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.9333333276344, number of tests: 10, total length: 266
[MASTER] 10:55:01.727 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:139.93333332577174 - branchDistance:0.0 - coverage:53.333333325771726 - ex: 0 - tex: 0
[MASTER] 10:55:01.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.93333332577174, number of tests: 10, total length: 263
[MASTER] 10:55:02.848 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:139.59999999236072 - branchDistance:0.0 - coverage:52.9999999923607 - ex: 0 - tex: 4
[MASTER] 10:55:02.848 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.59999999236072, number of tests: 9, total length: 235
[MASTER] 10:55:04.012 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:135.4333333263173 - branchDistance:0.0 - coverage:48.83333332631729 - ex: 0 - tex: 0
[MASTER] 10:55:04.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.4333333263173, number of tests: 11, total length: 304
[MASTER] 10:55:22.850 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:122.83333332752446 - branchDistance:0.0 - coverage:50.49999999419113 - ex: 0 - tex: 4
[MASTER] 10:55:22.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.83333332752446, number of tests: 11, total length: 262
[MASTER] 10:55:40.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:121.83333332752446 - branchDistance:0.0 - coverage:49.49999999419113 - ex: 0 - tex: 4
[MASTER] 10:55:40.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.83333332752446, number of tests: 11, total length: 268
[MASTER] 10:55:44.388 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:118.66666666283035 - branchDistance:0.0 - coverage:46.33333332949702 - ex: 0 - tex: 0
[MASTER] 10:55:44.389 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.66666666283035, number of tests: 12, total length: 329
[MASTER] 10:55:50.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:116.1666666609677 - branchDistance:0.0 - coverage:43.83333332763438 - ex: 0 - tex: 2
[MASTER] 10:55:50.637 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.1666666609677, number of tests: 12, total length: 339
[MASTER] 10:55:52.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:115.16666666272044 - branchDistance:0.0 - coverage:42.83333332938711 - ex: 0 - tex: 2
[MASTER] 10:55:52.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.16666666272044, number of tests: 12, total length: 299
[MASTER] 10:56:06.874 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:112.66666666380823 - branchDistance:0.0 - coverage:40.33333333047491 - ex: 0 - tex: 0
[MASTER] 10:56:06.875 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.66666666380823, number of tests: 13, total length: 360
[MASTER] 10:56:20.750 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:108.16666666194558 - branchDistance:0.0 - coverage:35.83333332861226 - ex: 0 - tex: 2
[MASTER] 10:56:20.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.16666666194558, number of tests: 13, total length: 381
[MASTER] 10:56:31.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:107.16666666194558 - branchDistance:0.0 - coverage:34.83333332861226 - ex: 0 - tex: 2
[MASTER] 10:56:31.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.16666666194558, number of tests: 14, total length: 412
[MASTER] 10:56:37.097 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:106.83333332861226 - branchDistance:0.0 - coverage:34.49999999527893 - ex: 0 - tex: 4
[MASTER] 10:56:37.100 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.83333332861226, number of tests: 14, total length: 392
[MASTER] 10:56:44.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:106.66666666196103 - branchDistance:0.0 - coverage:34.3333333286277 - ex: 0 - tex: 2
[MASTER] 10:56:44.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.66666666196103, number of tests: 13, total length: 376
[MASTER] 10:56:53.992 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:106.16666666194558 - branchDistance:0.0 - coverage:33.83333332861226 - ex: 0 - tex: 2
[MASTER] 10:56:53.992 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.16666666194558, number of tests: 14, total length: 409
[MASTER] 10:57:23.686 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:104.83333332861226 - branchDistance:0.0 - coverage:32.49999999527893 - ex: 0 - tex: 6
[MASTER] 10:57:23.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.83333332861226, number of tests: 15, total length: 439
[MASTER] 10:57:26.793 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:103.33333332861226 - branchDistance:0.0 - coverage:30.999999995278927 - ex: 0 - tex: 4
[MASTER] 10:57:26.796 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.33333332861226, number of tests: 15, total length: 412
[MASTER] 10:57:43.115 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:102.33333332763436 - branchDistance:0.0 - coverage:29.99999999430104 - ex: 0 - tex: 4
[MASTER] 10:57:43.116 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.33333332763436, number of tests: 16, total length: 431
[MASTER] 10:57:44.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:101.83333332861226 - branchDistance:0.0 - coverage:29.499999995278927 - ex: 0 - tex: 6
[MASTER] 10:57:44.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.83333332861226, number of tests: 16, total length: 472
[MASTER] 10:57:47.098 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:101.3333333304749 - branchDistance:0.0 - coverage:28.999999997141572 - ex: 0 - tex: 4
[MASTER] 10:57:47.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.3333333304749, number of tests: 17, total length: 477
[MASTER] 10:57:59.992 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 27 generations, 51329 statements, best individuals have fitness: 101.3333333304749
[MASTER] 10:58:00.023 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 87%
* Total number of goals: 183
* Number of covered goals: 160
* Generated 17 tests with total length 477
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                    101 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        308 / 300          Finished!
[MASTER] 10:58:07.475 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:58:08.796 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 400 
[MASTER] 10:58:12.907 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 10:58:12.927 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 10:58:12.929 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 10:58:12.930 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 10:58:12.937 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 10:58:12.938 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 10:58:12.939 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 10:58:12.944 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 10:58:12.945 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 10:58:12.945 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 10:58:12.946 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 10:58:12.952 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 10:58:12.953 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 10:58:12.953 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 10:58:12.953 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 10:58:12.954 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 10:58:12.968 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 10:58:12.975 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 10:58:12.976 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 10:58:12.983 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 10:58:12.985 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 5
* Writing tests to file
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
