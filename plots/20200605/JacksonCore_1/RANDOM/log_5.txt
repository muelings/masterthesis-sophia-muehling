* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting Client-0
* Connecting to master process on port 18547
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 05:08:52.849 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 133
[MASTER] 05:08:55.402 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:09:03.877 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:09:04.141 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 05:09:05.760 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:09:05.911 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 8 | Tests with assertion: 1
* Generated 1 tests with total length 37
[MASTER] 05:09:05.914 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                         13 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 05:09:13.926 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:09:14.157 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 25 
[MASTER] 05:09:14.382 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:09:14.384 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [contentsAsDecimal:NumberFormatException, NumberFormatException:contentsAsDecimal-398,]
[MASTER] 05:09:14.384 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: contentsAsDecimal:NumberFormatException at 24
[MASTER] 05:09:14.384 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [contentsAsDecimal:NumberFormatException, NumberFormatException:contentsAsDecimal-398,]
[MASTER] 05:09:17.414 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:09:17.438 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 8%
* Total number of goals: 133
* Number of covered goals: 10
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
