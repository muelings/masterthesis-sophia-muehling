* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
* Starting Client-0
* Connecting to master process on port 2099
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 18:36:07.230 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 37
[MASTER] 18:36:57.861 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 18:37:16.274 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 18:37:48.232 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 18:37:55.272 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 18:37:56.467 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 1113 | Tests with assertion: 1
* Generated 1 tests with total length 27
[MASTER] 18:37:56.490 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                        109 / 300         
[MASTER] 18:38:03.836 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:38:03.845 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 25 seconds more than allowed.
[MASTER] 18:38:04.148 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 18 
[MASTER] 18:38:04.364 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 18:38:04.365 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [close:MockIOException, MockIOException:finish-114,MockIOException:finish-114]
[MASTER] 18:38:04.365 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:MockIOException at 17
[MASTER] 18:38:04.365 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [close:MockIOException, MockIOException:finish-114,MockIOException:finish-114]
[MASTER] 18:38:12.450 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 18:38:12.487 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 19%
* Total number of goals: 37
* Number of covered goals: 7
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 58
* Writing tests to file
* Writing JUnit test case 'TarArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
