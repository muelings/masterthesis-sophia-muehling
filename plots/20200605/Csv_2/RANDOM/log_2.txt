* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting Client-0
* Connecting to master process on port 7490
* Analyzing classpath: 
  - ../../../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 23:13:01.681 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 25
[MASTER] 23:13:14.288 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 23:13:24.407 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVRecord_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 23:13:24.496 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 23:13:26.272 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVRecord_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 26 | Tests with assertion: 1
[MASTER] 23:13:26.365 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
* Generated 1 tests with total length 21
[MASTER] 23:13:26.381 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         24 / 300         
[MASTER] 23:13:31.525 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:13:31.766 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 14 
[MASTER] 23:13:32.123 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 23:13:32.124 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 23:13:32.124 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: get:MockIllegalArgumentException at 13
[MASTER] 23:13:32.124 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 23:13:33.936 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:13:33.984 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 12%
* Total number of goals: 25
* Number of covered goals: 3
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
