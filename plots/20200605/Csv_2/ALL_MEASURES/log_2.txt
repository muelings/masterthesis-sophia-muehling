* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting Client-0
* Connecting to master process on port 17905
* Analyzing classpath: 
  - ../../../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:14:20.597 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:14:20.713 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 25
* Using seed 1591305233149
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 23:14:45.741 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:99.8 - branchDistance:0.0 - coverage:39.3 - ex: 1 - tex: 3
[MASTER] 23:14:45.742 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.8, number of tests: 5, total length: 129
[MASTER] 23:14:48.116 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:74.33333333333333 - branchDistance:0.0 - coverage:13.333333333333334 - ex: 0 - tex: 6
[MASTER] 23:14:48.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.33333333333333, number of tests: 7, total length: 175
[MASTER] 23:14:49.478 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:74.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 4
[MASTER] 23:14:49.487 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.0, number of tests: 9, total length: 238
[MASTER] 23:14:54.262 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:69.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 23:14:54.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.0, number of tests: 10, total length: 269
[MASTER] 23:15:10.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:68.66666666666667 - branchDistance:0.0 - coverage:7.666666666666667 - ex: 0 - tex: 6
[MASTER] 23:15:10.426 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.66666666666667, number of tests: 9, total length: 219
[MASTER] 23:15:25.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.66666666666667 - branchDistance:0.0 - coverage:5.666666666666667 - ex: 0 - tex: 6
[MASTER] 23:15:25.930 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.66666666666667, number of tests: 10, total length: 254
[MASTER] 23:15:33.556 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.66666666666667 - branchDistance:0.0 - coverage:4.666666666666667 - ex: 0 - tex: 6
[MASTER] 23:15:33.567 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.66666666666667, number of tests: 8, total length: 226
[MASTER] 23:15:41.192 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 4
[MASTER] 23:15:41.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0, number of tests: 7, total length: 212
[MASTER] 23:16:13.530 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 10
[MASTER] 23:16:13.537 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 10, total length: 278
[MASTER] 23:17:03.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 23:17:03.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 9, total length: 248
[MASTER] 23:18:06.878 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 23:18:06.884 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 8, total length: 230
[MASTER] 23:19:22.120 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 56 generations, 99090 statements, best individuals have fitness: 61.0
[MASTER] 23:19:22.129 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 7 tests with total length 173
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        303 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     61 / 0           
[MASTER] 23:19:24.244 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:19:24.547 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 131 
[MASTER] 23:19:25.832 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 1.0
[MASTER] 23:19:25.841 [logback-1] WARN  RegressionSuiteMinimizer - Test1, uniqueExceptions: []
[MASTER] 23:19:25.842 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: get:MockIllegalArgumentException at 15
[MASTER] 23:19:25.842 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 23:19:25.843 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 23:19:25.852 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 23:19:25.853 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 23:19:25.859 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 23:19:25.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 23:19:25.861 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 23:19:25.862 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 23:19:25.863 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 23:19:28.612 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:19:28.614 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 24%
* Total number of goals: 25
* Number of covered goals: 6
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing tests to file
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
