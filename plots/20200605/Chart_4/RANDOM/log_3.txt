* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.plot.XYPlot
* Starting Client-0
* Connecting to master process on port 9170
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_4_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.plot.XYPlot
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 05:17:00.062 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1079
[MASTER] 05:17:26.380 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:19:27.605 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:19:29.116 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 05:19:30.257 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 288 | Tests with assertion: 1
* Generated 1 tests with total length 13
[MASTER] 05:19:30.295 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                        150 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 05:20:02.754 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:20:02.771 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 187 seconds more than allowed.
[MASTER] 05:20:02.999 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 05:20:03.145 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:20:03.159 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException]
[MASTER] 05:20:03.159 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NullPointerException at 6
[MASTER] 05:20:03.159 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException]
[MASTER] 05:20:04.572 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:20:04.612 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 1079
* Number of covered goals: 62
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.io.FilePermission: 
         execute /usr/bin/xprop: 1
  - java.lang.RuntimePermission: 
         loadLibrary.javalcms: 1
* Writing tests to file
* Writing JUnit test case 'XYPlot_ESTest' to evosuite-tests
* Done!

* Computation finished
