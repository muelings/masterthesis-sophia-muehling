* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.plot.XYPlot
* Starting Client-0
* Connecting to master process on port 15632
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_4_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.plot.XYPlot
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 05:30:22.376 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1079
[MASTER] 05:31:11.325 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:31:18.535 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 05:31:19.844 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 65 | Tests with assertion: 1
* Generated 1 tests with total length 8
[MASTER] 05:31:19.871 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                         57 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 05:31:51.395 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:31:51.405 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 91 seconds more than allowed.
[MASTER] 05:31:51.607 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 05:31:51.945 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:31:51.964 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 05:31:51.976 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NullPointerException at 2
[MASTER] 05:31:51.976 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException]
[MASTER] 05:31:52.496 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:31:52.536 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 1079
* Number of covered goals: 47
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.io.FilePermission: 
         execute /usr/bin/xprop: 1
* Writing tests to file
* Writing JUnit test case 'XYPlot_ESTest' to evosuite-tests
* Done!

* Computation finished
