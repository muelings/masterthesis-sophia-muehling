* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.plot.XYPlot
* Starting Client-0
* Connecting to master process on port 6692
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_4_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.plot.XYPlot
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:50:42.213 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1079
[MASTER] 04:51:42.893 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:51:50.344 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 04:51:51.740 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 89 | Tests with assertion: 1
* Generated 1 tests with total length 7
[MASTER] 04:51:51.769 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                         69 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 04:52:24.550 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:52:24.558 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 112 seconds more than allowed.
[MASTER] 04:52:24.764 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 5 
[MASTER] 04:52:24.880 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 04:52:24.881 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 04:52:24.881 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: getStartX:IndexOutOfBoundsException at 4
[MASTER] 04:52:24.882 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IndexOutOfBoundsException,, getStartX:IndexOutOfBoundsException]
[MASTER] 04:52:24.883 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NullPointerException at 2
[MASTER] 04:52:24.891 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException, IndexOutOfBoundsException,, getStartX:IndexOutOfBoundsException]
[MASTER] 04:52:25.768 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:52:25.786 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 5%
* Total number of goals: 1079
* Number of covered goals: 59
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.io.FilePermission: 
         execute /usr/bin/xprop: 1
* Writing tests to file
* Writing JUnit test case 'XYPlot_ESTest' to evosuite-tests
* Done!

* Computation finished
