* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting Client-0
* Connecting to master process on port 10665
* Analyzing classpath: 
  - ../../../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 12:49:32.343 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 12:49:32.475 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 371
* Using seed 1591354114853
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 12:49:44.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1487.047619047619 - branchDistance:0.0 - coverage:682.047619047619 - ex: 0 - tex: 8
[MASTER] 12:49:44.276 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1487.047619047619, number of tests: 6, total length: 174
[MASTER] 12:49:46.493 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1471.4204173329251 - branchDistance:0.0 - coverage:666.4204173329251 - ex: 0 - tex: 4
[MASTER] 12:49:46.512 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1471.4204173329251, number of tests: 4, total length: 116
[MASTER] 12:49:49.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1458.2952260550237 - branchDistance:0.0 - coverage:653.2952260550238 - ex: 0 - tex: 6
[MASTER] 12:49:49.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1458.2952260550237, number of tests: 8, total length: 197
[MASTER] 12:49:50.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1394.9999999345591 - branchDistance:0.0 - coverage:589.9999999345591 - ex: 0 - tex: 10
[MASTER] 12:49:50.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1394.9999999345591, number of tests: 8, total length: 178
[MASTER] 12:49:56.359 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1358.2146528108651 - branchDistance:0.0 - coverage:553.2146528108651 - ex: 0 - tex: 10
[MASTER] 12:49:56.361 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1358.2146528108651, number of tests: 8, total length: 201
[MASTER] 12:50:21.473 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1356.919999360492 - branchDistance:0.0 - coverage:551.919999360492 - ex: 0 - tex: 10
[MASTER] 12:50:21.476 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1356.919999360492, number of tests: 9, total length: 235
[MASTER] 12:50:27.494 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1352.99999846583 - branchDistance:0.0 - coverage:547.99999846583 - ex: 0 - tex: 20
[MASTER] 12:50:27.525 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1352.99999846583, number of tests: 10, total length: 259
[MASTER] 12:50:34.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1350.9999999233567 - branchDistance:0.0 - coverage:545.9999999233567 - ex: 0 - tex: 16
[MASTER] 12:50:34.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1350.9999999233567, number of tests: 10, total length: 225
[MASTER] 12:50:39.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1350.2146528099333 - branchDistance:0.0 - coverage:545.2146528099333 - ex: 0 - tex: 12
[MASTER] 12:50:39.314 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1350.2146528099333, number of tests: 8, total length: 185
[MASTER] 12:50:46.394 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1334.3024135933392 - branchDistance:0.0 - coverage:529.3024135933392 - ex: 0 - tex: 16
[MASTER] 12:50:46.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1334.3024135933392, number of tests: 9, total length: 199
[MASTER] 12:50:55.510 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1331.2946531296873 - branchDistance:0.0 - coverage:526.2946531296873 - ex: 0 - tex: 14
[MASTER] 12:50:55.526 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1331.2946531296873, number of tests: 9, total length: 230
[MASTER] 12:51:00.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1314.9999999339916 - branchDistance:0.0 - coverage:509.9999999339916 - ex: 0 - tex: 10
[MASTER] 12:51:00.283 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1314.9999999339916, number of tests: 9, total length: 239
[MASTER] 12:51:00.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1311.2946531296873 - branchDistance:0.0 - coverage:506.2946531296873 - ex: 0 - tex: 14
[MASTER] 12:51:00.723 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1311.2946531296873, number of tests: 10, total length: 253
[MASTER] 12:51:07.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1310.355106024618 - branchDistance:0.0 - coverage:505.35510602461795 - ex: 0 - tex: 12
[MASTER] 12:51:07.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1310.355106024618, number of tests: 9, total length: 274
[MASTER] 12:51:07.849 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1308.270831813107 - branchDistance:0.0 - coverage:503.27083181310695 - ex: 0 - tex: 18
[MASTER] 12:51:07.859 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1308.270831813107, number of tests: 10, total length: 284
[MASTER] 12:51:09.921 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1297.9999996890733 - branchDistance:0.0 - coverage:492.9999996890732 - ex: 0 - tex: 10
[MASTER] 12:51:09.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1297.9999996890733, number of tests: 9, total length: 252
[MASTER] 12:51:16.039 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1292.9374999990678 - branchDistance:0.0 - coverage:487.93749999906777 - ex: 0 - tex: 14
[MASTER] 12:51:16.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1292.9374999990678, number of tests: 9, total length: 240
[MASTER] 12:51:33.325 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1274.9374997541493 - branchDistance:0.0 - coverage:469.9374997541493 - ex: 0 - tex: 14
[MASTER] 12:51:33.326 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1274.9374997541493, number of tests: 10, total length: 278
[MASTER] 12:51:35.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1263.9374997541493 - branchDistance:0.0 - coverage:458.9374997541493 - ex: 0 - tex: 14
[MASTER] 12:51:35.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1263.9374997541493, number of tests: 11, total length: 304
[MASTER] 12:51:51.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1252.362680594814 - branchDistance:0.0 - coverage:447.36268059481404 - ex: 0 - tex: 14
[MASTER] 12:51:51.753 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1252.362680594814, number of tests: 10, total length: 280
[MASTER] 12:52:07.298 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1245.0585133284305 - branchDistance:0.0 - coverage:440.05851332843054 - ex: 0 - tex: 20
[MASTER] 12:52:07.309 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1245.0585133284305, number of tests: 13, total length: 353
[MASTER] 12:52:08.420 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1237.243362401849 - branchDistance:0.0 - coverage:432.2433624018489 - ex: 0 - tex: 20
[MASTER] 12:52:08.420 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1237.243362401849, number of tests: 12, total length: 360
[MASTER] 12:52:28.193 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1235.2433626467673 - branchDistance:0.0 - coverage:430.2433626467674 - ex: 0 - tex: 18
[MASTER] 12:52:28.194 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1235.2433626467673, number of tests: 12, total length: 358
[MASTER] 12:52:28.492 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1215.551569483703 - branchDistance:0.0 - coverage:410.5515694837029 - ex: 0 - tex: 16
[MASTER] 12:52:28.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1215.551569483703, number of tests: 12, total length: 335
[MASTER] 12:52:46.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1208.158138928988 - branchDistance:0.0 - coverage:403.15813892898797 - ex: 0 - tex: 18
[MASTER] 12:52:46.962 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1208.158138928988, number of tests: 13, total length: 381
[MASTER] 12:53:07.605 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1204.1825218646554 - branchDistance:0.0 - coverage:399.1825218646553 - ex: 0 - tex: 20
[MASTER] 12:53:07.607 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1204.1825218646554, number of tests: 13, total length: 376
[MASTER] 12:53:17.319 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1204.158138928988 - branchDistance:0.0 - coverage:399.15813892898797 - ex: 0 - tex: 18
[MASTER] 12:53:17.328 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1204.158138928988, number of tests: 13, total length: 389
[MASTER] 12:53:30.978 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1201.5653842326942 - branchDistance:0.0 - coverage:396.5653842326943 - ex: 0 - tex: 18
[MASTER] 12:53:30.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1201.5653842326942, number of tests: 13, total length: 401
[MASTER] 12:53:35.780 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1197.9344318526742 - branchDistance:0.0 - coverage:392.93443185267415 - ex: 0 - tex: 16
[MASTER] 12:53:35.789 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1197.9344318526742, number of tests: 14, total length: 423
[MASTER] 12:53:44.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1197.0608207527098 - branchDistance:0.0 - coverage:392.0608207527099 - ex: 0 - tex: 16
[MASTER] 12:53:44.116 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1197.0608207527098, number of tests: 13, total length: 397
[MASTER] 12:53:52.090 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1196.0972003797278 - branchDistance:0.0 - coverage:391.09720037972784 - ex: 0 - tex: 22
[MASTER] 12:53:52.098 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1196.0972003797278, number of tests: 14, total length: 423
[MASTER] 12:53:54.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1195.9344318526742 - branchDistance:0.0 - coverage:390.93443185267415 - ex: 0 - tex: 16
[MASTER] 12:53:54.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1195.9344318526742, number of tests: 14, total length: 451
[MASTER] 12:53:55.659 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1193.730741668714 - branchDistance:0.0 - coverage:388.730741668714 - ex: 0 - tex: 20
[MASTER] 12:53:55.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1193.730741668714, number of tests: 14, total length: 417
[MASTER] 12:54:00.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1191.5608209976285 - branchDistance:0.0 - coverage:386.5608209976284 - ex: 0 - tex: 16
[MASTER] 12:54:00.960 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1191.5608209976285, number of tests: 14, total length: 435
[MASTER] 12:54:03.097 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1188.558138928988 - branchDistance:0.0 - coverage:383.558138928988 - ex: 0 - tex: 22
[MASTER] 12:54:03.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1188.558138928988, number of tests: 15, total length: 453
[MASTER] 12:54:08.578 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1187.4248055956546 - branchDistance:0.0 - coverage:382.4248055956546 - ex: 0 - tex: 20
[MASTER] 12:54:08.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1187.4248055956546, number of tests: 15, total length: 444
[MASTER] 12:54:15.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1186.3658476859875 - branchDistance:0.0 - coverage:381.36584768598755 - ex: 0 - tex: 22
[MASTER] 12:54:15.079 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1186.3658476859875, number of tests: 15, total length: 452
[MASTER] 12:54:24.145 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1185.7597870799268 - branchDistance:0.0 - coverage:380.7597870799269 - ex: 0 - tex: 22
[MASTER] 12:54:24.146 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1185.7597870799268, number of tests: 15, total length: 461
[MASTER] 12:54:25.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1185.3658476859875 - branchDistance:0.0 - coverage:380.36584768598755 - ex: 0 - tex: 20
[MASTER] 12:54:25.459 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1185.3658476859875, number of tests: 15, total length: 456
[MASTER] 12:54:27.831 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1184.5653842317618 - branchDistance:0.0 - coverage:379.5653842317618 - ex: 0 - tex: 18
[MASTER] 12:54:27.841 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1184.5653842317618, number of tests: 15, total length: 455
[MASTER] 12:54:29.535 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1183.758138928988 - branchDistance:0.0 - coverage:378.75813892898793 - ex: 0 - tex: 20
[MASTER] 12:54:29.536 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1183.758138928988, number of tests: 16, total length: 475
[MASTER] 12:54:30.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1182.426453747526 - branchDistance:0.0 - coverage:377.42645374752607 - ex: 0 - tex: 22
[MASTER] 12:54:30.507 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1182.426453747526, number of tests: 16, total length: 466
[MASTER] 12:54:31.073 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1165.2010985184083 - branchDistance:0.0 - coverage:360.20109851840834 - ex: 0 - tex: 16
[MASTER] 12:54:31.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1165.2010985184083, number of tests: 14, total length: 449
[MASTER] 12:54:34.166 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 36 generations, 53010 statements, best individuals have fitness: 1165.2010985184083
[MASTER] 12:54:34.194 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 47%
* Total number of goals: 371
* Number of covered goals: 175
* Generated 14 tests with total length 449
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        308 / 300          Finished!
	- ZeroFitness :                  1,165 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:54:42.077 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:54:42.517 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 326 
[MASTER] 12:54:43.708 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.710 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.716 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.725 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.726 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.731 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.745 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.760 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 12:54:43.761 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 12:54:43.765 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 12:54:43.766 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 12:54:43.771 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 12:54:43.772 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 12:54:43.772 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 12:54:43.780 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 12:54:43.791 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 12:54:43.795 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 12:54:43.821 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 12:54:43.821 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 12:54:43.821 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 12:54:43.821 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 12:54:43.822 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 12:54:43.822 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 12:54:43.832 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 371
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing tests to file
* Writing JUnit test case 'NumberUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
