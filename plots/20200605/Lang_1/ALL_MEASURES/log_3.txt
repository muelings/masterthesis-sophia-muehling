* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting Client-0
* Connecting to master process on port 8519
* Analyzing classpath: 
  - ../../../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 13:11:07.290 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 13:11:07.508 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 371
* Using seed 1591355405083
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 13:11:24.624 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1420.0 - branchDistance:0.0 - coverage:615.0 - ex: 0 - tex: 16
[MASTER] 13:11:24.633 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1420.0, number of tests: 10, total length: 260
[MASTER] 13:11:31.014 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1404.498394818609 - branchDistance:0.0 - coverage:599.4983948186092 - ex: 0 - tex: 8
[MASTER] 13:11:31.024 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1404.498394818609, number of tests: 8, total length: 244
[MASTER] 13:11:42.943 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1400.1030810398956 - branchDistance:0.0 - coverage:595.1030810398955 - ex: 0 - tex: 10
[MASTER] 13:11:42.956 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1400.1030810398956, number of tests: 9, total length: 198
[MASTER] 13:11:47.825 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1397.6320061933202 - branchDistance:0.0 - coverage:592.6320061933201 - ex: 0 - tex: 6
[MASTER] 13:11:47.826 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1397.6320061933202, number of tests: 5, total length: 172
[MASTER] 13:11:53.045 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1396.9369647171225 - branchDistance:0.0 - coverage:591.9369647171224 - ex: 0 - tex: 6
[MASTER] 13:11:53.055 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1396.9369647171225, number of tests: 10, total length: 260
[MASTER] 13:11:58.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1383.9999998074027 - branchDistance:0.0 - coverage:578.9999998074026 - ex: 0 - tex: 10
[MASTER] 13:11:58.260 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1383.9999998074027, number of tests: 10, total length: 229
[MASTER] 13:12:11.930 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1345.2533332361033 - branchDistance:0.0 - coverage:540.2533332361033 - ex: 0 - tex: 10
[MASTER] 13:12:11.972 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1345.2533332361033, number of tests: 9, total length: 202
[MASTER] 13:12:26.095 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1315.9580223385615 - branchDistance:0.0 - coverage:510.9580223385615 - ex: 0 - tex: 14
[MASTER] 13:12:26.096 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1315.9580223385615, number of tests: 10, total length: 276
[MASTER] 13:12:49.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1300.958022339493 - branchDistance:0.0 - coverage:495.9580223394931 - ex: 0 - tex: 12
[MASTER] 13:12:49.330 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1300.958022339493, number of tests: 10, total length: 289
[MASTER] 13:13:00.422 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1294.8246889880343 - branchDistance:0.0 - coverage:489.82468898803415 - ex: 0 - tex: 14
[MASTER] 13:13:00.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1294.8246889880343, number of tests: 9, total length: 266
[MASTER] 13:13:08.805 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1292.2959781052493 - branchDistance:0.0 - coverage:487.2959781052493 - ex: 0 - tex: 18
[MASTER] 13:13:08.826 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1292.2959781052493, number of tests: 11, total length: 316
[MASTER] 13:13:09.856 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1281.7338213381759 - branchDistance:0.0 - coverage:476.73382133817597 - ex: 0 - tex: 14
[MASTER] 13:13:09.857 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1281.7338213381759, number of tests: 10, total length: 263
[MASTER] 13:13:17.543 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1265.5999297149751 - branchDistance:0.0 - coverage:460.599929714975 - ex: 0 - tex: 16
[MASTER] 13:13:17.548 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1265.5999297149751, number of tests: 10, total length: 320
[MASTER] 13:13:20.639 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1264.4913556537688 - branchDistance:0.0 - coverage:459.4913556537688 - ex: 0 - tex: 14
[MASTER] 13:13:20.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1264.4913556537688, number of tests: 10, total length: 301
[MASTER] 13:13:22.279 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1258.9115996569003 - branchDistance:0.0 - coverage:453.9115996569002 - ex: 0 - tex: 14
[MASTER] 13:13:22.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1258.9115996569003, number of tests: 11, total length: 339
[MASTER] 13:13:28.427 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1257.9115995578354 - branchDistance:0.0 - coverage:452.9115995578354 - ex: 0 - tex: 16
[MASTER] 13:13:28.429 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1257.9115995578354, number of tests: 12, total length: 368
[MASTER] 13:13:31.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1256.2959781043162 - branchDistance:0.0 - coverage:451.29597810431625 - ex: 0 - tex: 20
[MASTER] 13:13:31.217 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1256.2959781043162, number of tests: 12, total length: 356
[MASTER] 13:13:33.194 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1243.4917449471816 - branchDistance:0.0 - coverage:438.49174494718153 - ex: 0 - tex: 12
[MASTER] 13:13:33.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1243.4917449471816, number of tests: 13, total length: 373
[MASTER] 13:13:34.753 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1242.127974441122 - branchDistance:0.0 - coverage:437.12797444112203 - ex: 0 - tex: 16
[MASTER] 13:13:34.766 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1242.127974441122, number of tests: 12, total length: 354
[MASTER] 13:13:39.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1235.5985927175145 - branchDistance:0.0 - coverage:430.5985927175145 - ex: 0 - tex: 18
[MASTER] 13:13:39.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1235.5985927175145, number of tests: 11, total length: 357
[MASTER] 13:13:41.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1234.8246889871011 - branchDistance:0.0 - coverage:429.8246889871011 - ex: 0 - tex: 20
[MASTER] 13:13:41.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1234.8246889871011, number of tests: 12, total length: 321
[MASTER] 13:13:41.928 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1234.599929714042 - branchDistance:0.0 - coverage:429.599929714042 - ex: 0 - tex: 18
[MASTER] 13:13:41.934 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1234.599929714042, number of tests: 11, total length: 360
[MASTER] 13:13:45.446 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1232.574999314361 - branchDistance:0.0 - coverage:427.57499931436115 - ex: 0 - tex: 18
[MASTER] 13:13:45.452 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1232.574999314361, number of tests: 12, total length: 349
[MASTER] 13:14:00.277 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1229.1584116138483 - branchDistance:0.0 - coverage:424.1584116138482 - ex: 0 - tex: 16
[MASTER] 13:14:00.308 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1229.1584116138483, number of tests: 14, total length: 452
[MASTER] 13:14:00.751 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1229.0961168901313 - branchDistance:0.0 - coverage:424.09611689013116 - ex: 0 - tex: 14
[MASTER] 13:14:00.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1229.0961168901313, number of tests: 14, total length: 389
[MASTER] 13:14:01.713 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1218.5985927175145 - branchDistance:0.0 - coverage:413.5985927175145 - ex: 0 - tex: 20
[MASTER] 13:14:01.719 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1218.5985927175145, number of tests: 12, total length: 361
[MASTER] 13:14:02.702 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1212.58758244302 - branchDistance:0.0 - coverage:407.5875824430201 - ex: 0 - tex: 18
[MASTER] 13:14:02.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1212.58758244302, number of tests: 15, total length: 477
[MASTER] 13:14:05.620 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1188.5028334070018 - branchDistance:0.0 - coverage:383.5028334070018 - ex: 0 - tex: 20
[MASTER] 13:14:05.621 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1188.5028334070018, number of tests: 13, total length: 347
[MASTER] 13:14:15.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1175.5277638257403 - branchDistance:0.0 - coverage:370.52776382574024 - ex: 0 - tex: 20
[MASTER] 13:14:15.260 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1175.5277638257403, number of tests: 14, total length: 344
[MASTER] 13:14:21.482 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1165.1610969790836 - branchDistance:0.0 - coverage:360.1610969790835 - ex: 0 - tex: 22
[MASTER] 13:14:21.491 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1165.1610969790836, number of tests: 13, total length: 414
[MASTER] 13:14:24.121 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1162.401321052038 - branchDistance:0.0 - coverage:357.40132105203793 - ex: 0 - tex: 22
[MASTER] 13:14:24.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1162.401321052038, number of tests: 15, total length: 493
[MASTER] 13:14:33.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1153.5378295832231 - branchDistance:0.0 - coverage:348.5378295832231 - ex: 0 - tex: 24
[MASTER] 13:14:33.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1153.5378295832231, number of tests: 16, total length: 578
[MASTER] 13:14:44.117 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1153.298941186245 - branchDistance:0.0 - coverage:348.298941186245 - ex: 0 - tex: 24
[MASTER] 13:14:44.122 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1153.298941186245, number of tests: 15, total length: 450
[MASTER] 13:14:53.909 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1149.3610970964587 - branchDistance:0.0 - coverage:344.3610970964587 - ex: 0 - tex: 24
[MASTER] 13:14:53.925 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1149.3610970964587, number of tests: 15, total length: 431
