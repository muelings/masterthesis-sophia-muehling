* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting Client-0
* Connecting to master process on port 14575
* Analyzing classpath: 
  - ../../../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 13:01:29.061 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 13:01:29.184 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 371
* Using seed 1591354853644
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 13:01:35.779 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1534.999999878928 - branchDistance:0.0 - coverage:729.9999998789281 - ex: 0 - tex: 4
[MASTER] 13:01:35.793 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1534.999999878928, number of tests: 2, total length: 33
[MASTER] 13:01:36.484 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1478.3333333333335 - branchDistance:0.0 - coverage:673.3333333333334 - ex: 0 - tex: 8
[MASTER] 13:01:36.485 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1478.3333333333335, number of tests: 6, total length: 132
[MASTER] 13:01:37.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1376.9999999782603 - branchDistance:0.0 - coverage:571.9999999782603 - ex: 0 - tex: 10
[MASTER] 13:01:37.308 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1376.9999999782603, number of tests: 9, total length: 216
[MASTER] 13:01:47.219 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1325.5263978299174 - branchDistance:0.0 - coverage:520.5263978299174 - ex: 0 - tex: 14
[MASTER] 13:01:47.220 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1325.5263978299174, number of tests: 8, total length: 197
[MASTER] 13:01:50.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1317.8398306066374 - branchDistance:0.0 - coverage:512.8398306066374 - ex: 0 - tex: 10
[MASTER] 13:01:50.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1317.8398306066374, number of tests: 9, total length: 260
[MASTER] 13:01:56.670 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1292.1111110531501 - branchDistance:0.0 - coverage:487.11111105315 - ex: 0 - tex: 12
[MASTER] 13:01:56.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1292.1111110531501, number of tests: 10, total length: 230
[MASTER] 13:02:00.734 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1285.111111052218 - branchDistance:0.0 - coverage:480.111111052218 - ex: 0 - tex: 14
[MASTER] 13:02:00.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1285.111111052218, number of tests: 11, total length: 263
[MASTER] 13:02:10.338 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1258.7307396701472 - branchDistance:0.0 - coverage:453.73073967014705 - ex: 0 - tex: 8
[MASTER] 13:02:10.376 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1258.7307396701472, number of tests: 9, total length: 257
[MASTER] 13:02:31.772 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1250.7307396701472 - branchDistance:0.0 - coverage:445.73073967014705 - ex: 0 - tex: 14
[MASTER] 13:02:31.800 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1250.7307396701472, number of tests: 12, total length: 332
[MASTER] 13:02:33.407 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1230.9999999420388 - branchDistance:0.0 - coverage:425.99999994203887 - ex: 0 - tex: 14
[MASTER] 13:02:33.452 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1230.9999999420388, number of tests: 10, total length: 293
[MASTER] 13:02:39.743 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1220.9999999420388 - branchDistance:0.0 - coverage:415.99999994203887 - ex: 0 - tex: 16
[MASTER] 13:02:39.747 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1220.9999999420388, number of tests: 11, total length: 334
[MASTER] 13:02:44.618 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1214.999999926506 - branchDistance:0.0 - coverage:409.9999999265061 - ex: 0 - tex: 18
[MASTER] 13:02:44.620 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1214.999999926506, number of tests: 12, total length: 366
[MASTER] 13:02:51.511 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1208.999999926506 - branchDistance:0.0 - coverage:403.9999999265061 - ex: 0 - tex: 20
[MASTER] 13:02:51.512 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1208.999999926506, number of tests: 13, total length: 404
[MASTER] 13:02:54.327 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1197.7699256091273 - branchDistance:0.0 - coverage:392.76992560912737 - ex: 0 - tex: 18
[MASTER] 13:02:54.336 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1197.7699256091273, number of tests: 12, total length: 341
[MASTER] 13:02:54.719 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1196.7699256091273 - branchDistance:0.0 - coverage:391.76992560912737 - ex: 0 - tex: 16
[MASTER] 13:02:54.727 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1196.7699256091273, number of tests: 12, total length: 341
[MASTER] 13:03:11.461 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1191.7699256091273 - branchDistance:0.0 - coverage:386.76992560912737 - ex: 0 - tex: 16
[MASTER] 13:03:11.461 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1191.7699256091273, number of tests: 12, total length: 347
[MASTER] 13:03:15.814 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1190.6319399930226 - branchDistance:0.0 - coverage:385.6319399930226 - ex: 0 - tex: 20
[MASTER] 13:03:15.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1190.6319399930226, number of tests: 14, total length: 421
[MASTER] 13:03:17.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1167.4304628496645 - branchDistance:0.0 - coverage:362.4304628496646 - ex: 0 - tex: 22
[MASTER] 13:03:17.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1167.4304628496645, number of tests: 14, total length: 417
[MASTER] 13:03:28.170 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1162.4304628496645 - branchDistance:0.0 - coverage:357.4304628496646 - ex: 0 - tex: 24
[MASTER] 13:03:28.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1162.4304628496645, number of tests: 15, total length: 457
[MASTER] 13:03:37.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1161.4304628496645 - branchDistance:0.0 - coverage:356.4304628496646 - ex: 0 - tex: 22
[MASTER] 13:03:37.330 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1161.4304628496645, number of tests: 14, total length: 423
[MASTER] 13:03:43.116 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1160.7637961829978 - branchDistance:0.0 - coverage:355.7637961829979 - ex: 0 - tex: 16
[MASTER] 13:03:43.119 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1160.7637961829978, number of tests: 14, total length: 385
[MASTER] 13:03:45.429 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1158.4304628496645 - branchDistance:0.0 - coverage:353.4304628496646 - ex: 0 - tex: 24
[MASTER] 13:03:45.436 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1158.4304628496645, number of tests: 15, total length: 466
[MASTER] 13:03:46.623 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1154.3092507284525 - branchDistance:0.0 - coverage:349.30925072845247 - ex: 0 - tex: 22
[MASTER] 13:03:46.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1154.3092507284525, number of tests: 15, total length: 431
[MASTER] 13:03:54.540 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1147.7637961829978 - branchDistance:0.0 - coverage:342.7637961829979 - ex: 0 - tex: 24
[MASTER] 13:03:54.560 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1147.7637961829978, number of tests: 16, total length: 470
[MASTER] 13:03:59.672 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1144.4225263417281 - branchDistance:0.0 - coverage:339.4225263417281 - ex: 0 - tex: 24
[MASTER] 13:03:59.673 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1144.4225263417281, number of tests: 15, total length: 444
[MASTER] 13:04:05.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1143.2824297233706 - branchDistance:0.0 - coverage:338.2824297233706 - ex: 0 - tex: 26
[MASTER] 13:04:05.255 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1143.2824297233706, number of tests: 16, total length: 485
[MASTER] 13:04:10.552 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1142.6225981225086 - branchDistance:0.0 - coverage:337.62259812250863 - ex: 0 - tex: 22
[MASTER] 13:04:10.554 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1142.6225981225086, number of tests: 16, total length: 447
[MASTER] 13:04:11.137 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1140.615763056704 - branchDistance:0.0 - coverage:335.61576305670394 - ex: 0 - tex: 20
[MASTER] 13:04:11.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1140.615763056704, number of tests: 15, total length: 427
[MASTER] 13:04:16.592 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1140.4214250201421 - branchDistance:0.0 - coverage:335.4214250201422 - ex: 0 - tex: 26
[MASTER] 13:04:16.595 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1140.4214250201421, number of tests: 16, total length: 464
[MASTER] 13:04:17.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1139.9784204619195 - branchDistance:0.0 - coverage:334.97842046191965 - ex: 0 - tex: 26
[MASTER] 13:04:17.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1139.9784204619195, number of tests: 16, total length: 476
[MASTER] 13:04:22.794 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1139.4225262211821 - branchDistance:0.0 - coverage:334.4225262211822 - ex: 0 - tex: 28
[MASTER] 13:04:22.794 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1139.4225262211821, number of tests: 17, total length: 476
[MASTER] 13:04:24.235 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1135.7547583534756 - branchDistance:0.0 - coverage:330.7547583534755 - ex: 0 - tex: 22
[MASTER] 13:04:24.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1135.7547583534756, number of tests: 16, total length: 436
[MASTER] 13:04:28.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1135.4148887709707 - branchDistance:0.0 - coverage:330.4148887709707 - ex: 0 - tex: 28
[MASTER] 13:04:28.749 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1135.4148887709707, number of tests: 17, total length: 491
[MASTER] 13:04:38.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1134.614661735118 - branchDistance:0.0 - coverage:329.61466173511803 - ex: 0 - tex: 22
[MASTER] 13:04:38.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1134.614661735118, number of tests: 16, total length: 440
[MASTER] 13:04:43.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1133.7547583534756 - branchDistance:0.0 - coverage:328.7547583534755 - ex: 0 - tex: 24
[MASTER] 13:04:43.543 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1133.7547583534756, number of tests: 17, total length: 472
[MASTER] 13:04:48.246 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1133.6146519455929 - branchDistance:0.0 - coverage:328.6146519455928 - ex: 0 - tex: 24
[MASTER] 13:04:48.252 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1133.6146519455929, number of tests: 17, total length: 476
[MASTER] 13:04:50.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1133.2747921526134 - branchDistance:0.0 - coverage:328.27479215261326 - ex: 0 - tex: 22
[MASTER] 13:04:50.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1133.2747921526134, number of tests: 16, total length: 448
[MASTER] 13:04:55.471 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1132.2747921526134 - branchDistance:0.0 - coverage:327.27479215261326 - ex: 0 - tex: 22
[MASTER] 13:04:55.483 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1132.2747921526134, number of tests: 16, total length: 446
[MASTER] 13:04:56.317 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1131.2041964403272 - branchDistance:0.0 - coverage:326.2041964403272 - ex: 0 - tex: 24
[MASTER] 13:04:56.324 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1131.2041964403272, number of tests: 17, total length: 493
[MASTER] 13:05:03.292 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1129.266171462958 - branchDistance:0.0 - coverage:324.2661714629581 - ex: 0 - tex: 28
[MASTER] 13:05:03.305 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1129.266171462958, number of tests: 18, total length: 517
[MASTER] 13:05:14.376 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1128.7482220868087 - branchDistance:0.0 - coverage:323.7482220868088 - ex: 0 - tex: 26
[MASTER] 13:05:14.379 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1128.7482220868087, number of tests: 18, total length: 489
[MASTER] 13:05:16.782 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1126.7482221043042 - branchDistance:0.0 - coverage:321.7482221043041 - ex: 0 - tex: 28
[MASTER] 13:05:16.808 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1126.7482221043042, number of tests: 19, total length: 516
[MASTER] 13:05:22.238 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1120.2723072527658 - branchDistance:0.0 - coverage:315.27230725276587 - ex: 0 - tex: 26
[MASTER] 13:05:22.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1120.2723072527658, number of tests: 19, total length: 541
[MASTER] 13:05:24.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1119.6081264414827 - branchDistance:0.0 - coverage:314.6081264414826 - ex: 0 - tex: 28
[MASTER] 13:05:24.721 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1119.6081264414827, number of tests: 18, total length: 512
[MASTER] 13:05:27.865 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1119.2723072527658 - branchDistance:0.0 - coverage:314.27230725276587 - ex: 0 - tex: 26
[MASTER] 13:05:27.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1119.2723072527658, number of tests: 19, total length: 556
[MASTER] 13:05:34.524 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1118.2561508589238 - branchDistance:0.0 - coverage:313.2561508589238 - ex: 0 - tex: 30
[MASTER] 13:05:34.525 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1118.2561508589238, number of tests: 20, total length: 555
[MASTER] 13:05:38.094 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1115.6081264414827 - branchDistance:0.0 - coverage:310.6081264414826 - ex: 0 - tex: 26
[MASTER] 13:05:38.100 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1115.6081264414827, number of tests: 19, total length: 543
[MASTER] 13:05:40.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1114.6081264414827 - branchDistance:0.0 - coverage:309.6081264414826 - ex: 0 - tex: 28
[MASTER] 13:05:40.220 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1114.6081264414827, number of tests: 20, total length: 587
[MASTER] 13:05:50.447 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1114.4625087741258 - branchDistance:0.0 - coverage:309.4625087741258 - ex: 0 - tex: 26
[MASTER] 13:05:50.452 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1114.4625087741258, number of tests: 19, total length: 544
[MASTER] 13:05:51.627 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1113.8572485620327 - branchDistance:0.0 - coverage:308.85724856203274 - ex: 0 - tex: 28
[MASTER] 13:05:51.635 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1113.8572485620327, number of tests: 19, total length: 520
[MASTER] 13:05:54.902 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1107.4699258485489 - branchDistance:0.0 - coverage:302.4699258485488 - ex: 0 - tex: 30
[MASTER] 13:05:54.911 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1107.4699258485489, number of tests: 21, total length: 607
[MASTER] 13:06:03.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1106.4699258485489 - branchDistance:0.0 - coverage:301.4699258485488 - ex: 0 - tex: 30
[MASTER] 13:06:03.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1106.4699258485489, number of tests: 21, total length: 607
[MASTER] 13:06:07.850 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1106.4699258260696 - branchDistance:0.0 - coverage:301.4699258260696 - ex: 0 - tex: 32
[MASTER] 13:06:07.862 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1106.4699258260696, number of tests: 22, total length: 647
[MASTER] 13:06:22.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1105.5948646386246 - branchDistance:0.0 - coverage:300.5948646386247 - ex: 0 - tex: 32
[MASTER] 13:06:22.415 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1105.5948646386246, number of tests: 21, total length: 561
[MASTER] 13:06:26.354 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1104.5948646386246 - branchDistance:0.0 - coverage:299.5948646386247 - ex: 0 - tex: 32
[MASTER] 13:06:26.359 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1104.5948646386246, number of tests: 22, total length: 600
[MASTER] 13:06:30.996 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 56 generations, 76012 statements, best individuals have fitness: 1104.5948646386246
[MASTER] 13:06:31.054 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 54%
* Total number of goals: 371
* Number of covered goals: 200
* Generated 22 tests with total length 600
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                  1,105 / 0           
	- MaxTime :                        308 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 13:06:37.797 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:06:38.317 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 478 
[MASTER] 13:06:39.658 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.667 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.671 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.675 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.676 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.678 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.685 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.693 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.700 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.701 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.702 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.702 [logback-1] WARN  RegressionSuiteMinimizer - Test17 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.708 [logback-1] WARN  RegressionSuiteMinimizer - Test18 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.716 [logback-1] WARN  RegressionSuiteMinimizer - Test19 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.717 [logback-1] WARN  RegressionSuiteMinimizer - Test20 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.717 [logback-1] WARN  RegressionSuiteMinimizer - Test21 - Difference in number of exceptions: 0.0
[MASTER] 13:06:39.717 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 13:06:39.718 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 13:06:39.723 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 13:06:39.724 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 13:06:39.725 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 13:06:39.725 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 13:06:39.726 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 13:06:39.731 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 13:06:39.732 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 13:06:39.733 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 13:06:39.739 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 13:06:39.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 13:06:39.744 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 13:06:39.752 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 13:06:39.752 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 13:06:39.753 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 13:06:39.753 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 13:06:39.753 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 13:06:39.754 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 13:06:39.754 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 19: no assertions
[MASTER] 13:06:39.755 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 20: no assertions
[MASTER] 13:06:39.755 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 21: no assertions
[MASTER] 13:06:39.767 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 13:06:39.775 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 371
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'NumberUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
