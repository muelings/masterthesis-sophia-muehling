* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.DoubleMetaphone
* Starting Client-0
* Connecting to master process on port 14913
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.DoubleMetaphone
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 14:41:31.625 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 504
[MASTER] 14:44:12.759 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:44:18.808 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 14:44:19.561 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 3418 | Tests with assertion: 1
* Generated 1 tests with total length 12
[MASTER] 14:44:19.576 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                        168 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 14:44:32.165 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:44:32.196 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 87 seconds more than allowed.
[MASTER] 14:44:32.336 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 6 
[MASTER] 14:44:32.420 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 14:44:32.959 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:44:32.988 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 504
* Number of covered goals: 2
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'DoubleMetaphone_ESTest' to evosuite-tests
* Done!

* Computation finished
