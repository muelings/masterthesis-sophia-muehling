* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.SoundexUtils
* Starting Client-0
* Connecting to master process on port 4865
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.SoundexUtils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 13:35:11.166 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 20
*** Random test generation finished.
*=*=*=* Total tests: 9578 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 13:40:12.232 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 13:40:12.838 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:40:12.845 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 201 seconds more than allowed.
[MASTER] 13:40:12.924 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 13:40:12.949 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 13:40:12.996 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 20
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing tests to file
* Writing JUnit test case 'SoundexUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
