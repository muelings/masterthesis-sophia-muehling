* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.SoundexUtils
* Starting Client-0
* Connecting to master process on port 21907
* Analyzing classpath: 
  - ../../../defects4j_compiled/Codec_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.SoundexUtils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 12:56:30.916 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 12:56:30.988 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 20
* Using seed 1591268171225
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 12:56:38.640 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:45.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 12:56:38.645 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.0, number of tests: 6, total length: 112

* Search finished after 302s and 388 generations, 222284 statements, best individuals have fitness: 45.0
[MASTER] 13:01:32.088 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:01:32.091 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 20
* Number of covered goals: 20
* Generated 2 tests with total length 13
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     45 / 0           
	- RMIStoppingCondition
[MASTER] 13:01:32.934 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:01:33.089 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 13:01:33.173 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 13:01:33.174 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 13:01:33.247 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 20
* Number of covered goals: 0
[MASTER] 13:01:33.249 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing tests to file
* Writing JUnit test case 'SoundexUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
