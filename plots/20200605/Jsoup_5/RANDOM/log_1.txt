* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting Client-0
* Connecting to master process on port 14952
* Analyzing classpath: 
  - ../../../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 09:45:40.232 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 100
[MASTER] 09:45:50.912 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 09:45:59.073 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:45:59.218 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 09:46:00.799 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:46:00.932 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 74 | Tests with assertion: 1
* Generated 1 tests with total length 26
[MASTER] 09:46:00.968 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                         20 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 09:46:07.723 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:46:08.047 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 09:46:08.232 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 09:46:08.232 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragmentRelaxed:MockIllegalArgumentException, MockIllegalArgumentException,, parse:StringIndexOutOfBoundsException]
[MASTER] 09:46:08.232 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parseBodyFragmentRelaxed:MockIllegalArgumentException at 12
[MASTER] 09:46:08.232 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragmentRelaxed:MockIllegalArgumentException, MockIllegalArgumentException,, parse:StringIndexOutOfBoundsException]
[MASTER] 09:46:08.232 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 2
[MASTER] 09:46:08.232 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parseBodyFragmentRelaxed:MockIllegalArgumentException, MockIllegalArgumentException,, parse:StringIndexOutOfBoundsException]
[MASTER] 09:46:10.431 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:46:10.463 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 50%
* Total number of goals: 100
* Number of covered goals: 50
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
