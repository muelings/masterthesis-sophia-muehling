* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting Client-0
* Connecting to master process on port 13819
* Analyzing classpath: 
  - ../../../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:17:26.054 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:17:26.132 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 100
* Using seed 1591345016704
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 10:17:34.685 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:393.0 - branchDistance:0.0 - coverage:174.0 - ex: 0 - tex: 10
[MASTER] 10:17:34.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 393.0, number of tests: 6, total length: 150
[MASTER] 10:17:37.040 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:390.0 - branchDistance:0.0 - coverage:171.0 - ex: 0 - tex: 12
[MASTER] 10:17:37.048 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 390.0, number of tests: 10, total length: 196
[MASTER] 10:17:38.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:352.99999999813735 - branchDistance:0.0 - coverage:133.99999999813735 - ex: 0 - tex: 6
[MASTER] 10:17:38.647 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 352.99999999813735, number of tests: 7, total length: 144
[MASTER] 10:17:40.419 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:342.9999999981833 - branchDistance:0.0 - coverage:123.9999999981833 - ex: 0 - tex: 6
[MASTER] 10:17:40.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 342.9999999981833, number of tests: 5, total length: 58
[MASTER] 10:17:43.362 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:307.99999999869124 - branchDistance:0.0 - coverage:88.99999999869125 - ex: 0 - tex: 10
[MASTER] 10:17:43.368 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 307.99999999869124, number of tests: 6, total length: 133
[MASTER] 10:18:01.913 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:306.99999999869124 - branchDistance:0.0 - coverage:87.99999999869125 - ex: 0 - tex: 12
[MASTER] 10:18:01.914 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 306.99999999869124, number of tests: 7, total length: 160
[MASTER] 10:18:02.871 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:300.99999997068306 - branchDistance:0.0 - coverage:81.99999997068305 - ex: 0 - tex: 10
[MASTER] 10:18:02.872 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 300.99999997068306, number of tests: 6, total length: 125
[MASTER] 10:18:13.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:299.49999999930884 - branchDistance:0.0 - coverage:80.49999999930883 - ex: 0 - tex: 14
[MASTER] 10:18:13.919 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 299.49999999930884, number of tests: 7, total length: 176
[MASTER] 10:18:14.122 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:293.9999999982206 - branchDistance:0.0 - coverage:74.99999999822057 - ex: 0 - tex: 12
[MASTER] 10:18:14.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 293.9999999982206, number of tests: 10, total length: 174
[MASTER] 10:18:21.880 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:292.9999999982206 - branchDistance:0.0 - coverage:73.99999999822057 - ex: 0 - tex: 10
[MASTER] 10:18:21.881 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 292.9999999982206, number of tests: 9, total length: 163
[MASTER] 10:18:24.837 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:291.9999999982206 - branchDistance:0.0 - coverage:72.99999999822057 - ex: 0 - tex: 12
[MASTER] 10:18:24.838 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 291.9999999982206, number of tests: 10, total length: 185
[MASTER] 10:18:31.161 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:289.9999999982206 - branchDistance:0.0 - coverage:70.99999999822057 - ex: 0 - tex: 12
[MASTER] 10:18:31.168 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 289.9999999982206, number of tests: 10, total length: 201
[MASTER] 10:18:33.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:288.9999999982206 - branchDistance:0.0 - coverage:69.99999999822057 - ex: 0 - tex: 12
[MASTER] 10:18:33.695 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 288.9999999982206, number of tests: 11, total length: 192
[MASTER] 10:18:36.626 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:283.9999999982206 - branchDistance:0.0 - coverage:64.99999999822057 - ex: 0 - tex: 10
[MASTER] 10:18:36.631 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 283.9999999982206, number of tests: 9, total length: 183
[MASTER] 10:18:39.453 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:280.9999999982206 - branchDistance:0.0 - coverage:61.999999998220574 - ex: 0 - tex: 14
[MASTER] 10:18:39.462 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 280.9999999982206, number of tests: 10, total length: 220
[MASTER] 10:18:45.229 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:278.9999999982206 - branchDistance:0.0 - coverage:59.999999998220574 - ex: 0 - tex: 10
[MASTER] 10:18:45.229 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 278.9999999982206, number of tests: 10, total length: 199
[MASTER] 10:18:49.248 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:277.9999999982206 - branchDistance:0.0 - coverage:58.999999998220574 - ex: 0 - tex: 12
[MASTER] 10:18:49.255 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 277.9999999982206, number of tests: 10, total length: 204
[MASTER] 10:18:58.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:277.4999999982206 - branchDistance:0.0 - coverage:58.999999998220574 - ex: 1 - tex: 11
[MASTER] 10:18:58.697 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 277.4999999982206, number of tests: 10, total length: 203
[MASTER] 10:18:59.586 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:272.9999999982206 - branchDistance:0.0 - coverage:53.999999998220574 - ex: 0 - tex: 12
[MASTER] 10:18:59.592 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 272.9999999982206, number of tests: 11, total length: 214
[MASTER] 10:19:06.396 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:272.99999999635793 - branchDistance:0.0 - coverage:53.99999999635793 - ex: 0 - tex: 12
[MASTER] 10:19:06.397 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 272.99999999635793, number of tests: 11, total length: 215
[MASTER] 10:19:20.215 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.9999999982206 - branchDistance:0.0 - coverage:52.999999998220574 - ex: 0 - tex: 12
[MASTER] 10:19:20.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.9999999982206, number of tests: 11, total length: 214
[MASTER] 10:19:24.519 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.99999998043086 - branchDistance:0.0 - coverage:52.999999980430864 - ex: 0 - tex: 14
[MASTER] 10:19:24.520 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.99999998043086, number of tests: 11, total length: 192
[MASTER] 10:19:25.384 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.49999999635793 - branchDistance:0.0 - coverage:52.99999999635793 - ex: 1 - tex: 13
[MASTER] 10:19:25.385 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.49999999635793, number of tests: 11, total length: 223
[MASTER] 10:19:35.635 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.49999999631876 - branchDistance:0.0 - coverage:52.99999999631876 - ex: 1 - tex: 15
[MASTER] 10:19:35.636 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.49999999631876, number of tests: 11, total length: 222
[MASTER] 10:19:41.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.49999999635793 - branchDistance:0.0 - coverage:51.99999999635793 - ex: 1 - tex: 11
[MASTER] 10:19:41.142 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.49999999635793, number of tests: 10, total length: 198
[MASTER] 10:19:46.793 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.49999999631876 - branchDistance:0.0 - coverage:51.99999999631876 - ex: 1 - tex: 15
[MASTER] 10:19:46.807 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.49999999631876, number of tests: 11, total length: 226
[MASTER] 10:19:52.736 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.99999999635793 - branchDistance:0.0 - coverage:51.49999999635793 - ex: 1 - tex: 11
[MASTER] 10:19:52.736 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.99999999635793, number of tests: 11, total length: 210
[MASTER] 10:19:58.766 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.99999999631876 - branchDistance:0.0 - coverage:51.49999999631876 - ex: 1 - tex: 11
[MASTER] 10:19:58.772 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.99999999631876, number of tests: 11, total length: 205
[MASTER] 10:19:59.153 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.49999999631876 - branchDistance:0.0 - coverage:50.99999999631876 - ex: 1 - tex: 15
[MASTER] 10:19:59.160 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.49999999631876, number of tests: 12, total length: 230
[MASTER] 10:20:05.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.49999999631876 - branchDistance:0.0 - coverage:49.99999999631876 - ex: 1 - tex: 17
[MASTER] 10:20:05.762 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.49999999631876, number of tests: 13, total length: 285
[MASTER] 10:20:21.995 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.3333333296521 - branchDistance:0.0 - coverage:49.99999999631876 - ex: 2 - tex: 14
[MASTER] 10:20:22.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.3333333296521, number of tests: 12, total length: 258
[MASTER] 10:20:28.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.24999999631876 - branchDistance:0.0 - coverage:49.99999999631876 - ex: 3 - tex: 21
[MASTER] 10:20:28.519 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.24999999631876, number of tests: 14, total length: 298
[MASTER] 10:20:34.573 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.8333333296521 - branchDistance:0.0 - coverage:49.49999999631876 - ex: 2 - tex: 18
[MASTER] 10:20:34.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.8333333296521, number of tests: 13, total length: 284
[MASTER] 10:20:43.079 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.74999999631876 - branchDistance:0.0 - coverage:49.49999999631876 - ex: 3 - tex: 19
[MASTER] 10:20:43.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.74999999631876, number of tests: 13, total length: 310
[MASTER] 10:20:44.778 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.6999999756809 - branchDistance:0.0 - coverage:49.499999975680936 - ex: 4 - tex: 20
[MASTER] 10:20:44.791 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.6999999756809, number of tests: 15, total length: 362
[MASTER] 10:20:51.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.66666666298545 - branchDistance:0.0 - coverage:49.49999999631876 - ex: 5 - tex: 19
[MASTER] 10:20:51.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.66666666298545, number of tests: 15, total length: 360
[MASTER] 10:21:10.496 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.64285713917593 - branchDistance:0.0 - coverage:49.49999999631876 - ex: 6 - tex: 18
[MASTER] 10:21:10.499 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.64285713917593, number of tests: 15, total length: 377
[MASTER] 10:21:25.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.62499999631876 - branchDistance:0.0 - coverage:49.49999999631876 - ex: 7 - tex: 15
[MASTER] 10:21:25.039 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.62499999631876, number of tests: 16, total length: 376
[MASTER] 10:21:30.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:267.61111110742985 - branchDistance:0.0 - coverage:49.49999999631876 - ex: 8 - tex: 16
[MASTER] 10:21:30.173 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 267.61111110742985, number of tests: 16, total length: 393
[MASTER] 10:21:33.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:260.66666666298545 - branchDistance:0.0 - coverage:42.49999999631876 - ex: 5 - tex: 17
[MASTER] 10:21:33.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 260.66666666298545, number of tests: 15, total length: 336
[MASTER] 10:21:35.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:260.64285713917593 - branchDistance:0.0 - coverage:42.49999999631876 - ex: 6 - tex: 18
[MASTER] 10:21:35.905 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 260.64285713917593, number of tests: 15, total length: 352
[MASTER] 10:21:37.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:260.62499999631876 - branchDistance:0.0 - coverage:42.49999999631876 - ex: 7 - tex: 17
[MASTER] 10:21:37.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 260.62499999631876, number of tests: 16, total length: 371
[MASTER] 10:21:40.019 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:257.64285713917593 - branchDistance:0.0 - coverage:39.49999999631876 - ex: 6 - tex: 20
[MASTER] 10:21:40.020 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 257.64285713917593, number of tests: 16, total length: 371
[MASTER] 10:21:41.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:257.62499999631876 - branchDistance:0.0 - coverage:39.49999999631876 - ex: 7 - tex: 15
[MASTER] 10:21:41.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 257.62499999631876, number of tests: 16, total length: 374
[MASTER] 10:21:42.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:257.61111110742985 - branchDistance:0.0 - coverage:39.49999999631876 - ex: 8 - tex: 18
[MASTER] 10:21:42.448 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 257.61111110742985, number of tests: 16, total length: 396
[MASTER] 10:21:57.681 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:257.61111110739563 - branchDistance:0.0 - coverage:39.499999996284565 - ex: 8 - tex: 18
[MASTER] 10:21:57.681 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 257.61111110739563, number of tests: 16, total length: 397
[MASTER] 10:21:59.899 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:257.5999999963188 - branchDistance:0.0 - coverage:39.49999999631876 - ex: 9 - tex: 19
[MASTER] 10:21:59.911 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 257.5999999963188, number of tests: 17, total length: 430
[MASTER] 10:22:06.391 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:256.61111110742985 - branchDistance:0.0 - coverage:38.49999999631876 - ex: 8 - tex: 18
[MASTER] 10:22:06.391 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 256.61111110742985, number of tests: 17, total length: 402
[MASTER] 10:22:16.702 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:256.5999999963188 - branchDistance:0.0 - coverage:38.49999999631876 - ex: 9 - tex: 19
[MASTER] 10:22:16.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 256.5999999963188, number of tests: 17, total length: 428
[MASTER] 10:22:28.138 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:22:28.162 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 303s and 89 generations, 92272 statements, best individuals have fitness: 256.5999999963188
* Coverage of criterion REGRESSION: 74%
* Total number of goals: 100
* Number of covered goals: 74
* Generated 16 tests with total length 402
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        305 / 300          Finished!
	- ZeroFitness :                    257 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 10:22:31.944 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:22:32.697 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 222 
[MASTER] 10:22:33.571 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 10:22:33.580 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 10:22:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 10:22:33.595 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 10:22:33.596 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.596 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: []
[MASTER] 10:22:33.603 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.603 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.605 [logback-1] WARN  RegressionSuiteMinimizer - Test7, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.605 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.607 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:33.621 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:33.683 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.683 [logback-1] WARN  RegressionSuiteMinimizer - Test7, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.683 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.683 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.683 [logback-1] WARN  RegressionSuiteMinimizer - Test9, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.683 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.684 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:33.684 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:33.724 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.724 [logback-1] WARN  RegressionSuiteMinimizer - Test9, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.724 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.724 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parse(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:33.724 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:33.780 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.780 [logback-1] WARN  RegressionSuiteMinimizer - Test10, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.780 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.780 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:33.780 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:33.838 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.838 [logback-1] WARN  RegressionSuiteMinimizer - Test10, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.838 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.838 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parse(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:33.838 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:33.911 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.911 [logback-1] WARN  RegressionSuiteMinimizer - Test11, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.911 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.912 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:33.912 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:33.956 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 1.0
[MASTER] 10:22:33.956 [logback-1] WARN  RegressionSuiteMinimizer - Test11, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:33.956 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:33.956 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parse(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:33.956 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.012 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.012 [logback-1] WARN  RegressionSuiteMinimizer - Test12, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.012 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:34.012 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.012 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.056 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.056 [logback-1] WARN  RegressionSuiteMinimizer - Test12, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.056 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:34.056 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parse(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.056 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.111 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.112 [logback-1] WARN  RegressionSuiteMinimizer - Test13, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.112 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:34.112 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.112 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.167 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.167 [logback-1] WARN  RegressionSuiteMinimizer - Test14, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.167 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:34.167 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.167 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.200 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.200 [logback-1] WARN  RegressionSuiteMinimizer - Test14, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.200 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:34.200 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parse(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.200 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.232 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.232 [logback-1] WARN  RegressionSuiteMinimizer - Test14, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragment:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.232 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragment:StringIndexOutOfBoundsException at 5
[MASTER] 10:22:34.232 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.232 [logback-1] WARN  RegressionSuiteMinimizer - Test15, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragment:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.232 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:34.233 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragmentRelaxed(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.233 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.287 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.287 [logback-1] WARN  RegressionSuiteMinimizer - Test15, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragment:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.287 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 4
[MASTER] 10:22:34.287 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parse(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.288 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 10:22:34.328 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 1.0
[MASTER] 10:22:34.328 [logback-1] WARN  RegressionSuiteMinimizer - Test15, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragment:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.328 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragment:StringIndexOutOfBoundsException at 5
[MASTER] 10:22:34.328 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: parseBodyFragment(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/nodes/Document;
[MASTER] 10:22:34.328 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 5
[MASTER] 10:22:34.375 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException, parseBodyFragment:StringIndexOutOfBoundsException, parse:StringIndexOutOfBoundsException]
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 10:22:34.376 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 10:22:34.386 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 10:22:36.944 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 10:22:36.946 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 57%
* Total number of goals: 100
* Number of covered goals: 57
* Generated 3 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing tests to file
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
