* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Starting Client-0
* Connecting to master process on port 17485
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:51:03.801 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:51:03.925 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1255
* Using seed 1591329015037
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 05:51:23.624 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:5246.3521674615795 - branchDistance:0.0 - coverage:2610.3521674615795 - ex: 0 - tex: 16
[MASTER] 05:51:23.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5246.3521674615795, number of tests: 8, total length: 187
[MASTER] 05:51:44.526 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:3385.8571428571427 - branchDistance:0.0 - coverage:2632.0 - ex: 0 - tex: 10
[MASTER] 05:51:44.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3385.8571428571427, number of tests: 6, total length: 192
[MASTER] 05:51:59.988 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:3380.3571428571427 - branchDistance:0.0 - coverage:2627.0 - ex: 1 - tex: 17
[MASTER] 05:51:59.991 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3380.3571428571427, number of tests: 9, total length: 243
* Computation finished
