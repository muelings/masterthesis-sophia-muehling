* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Starting Client-0
* Connecting to master process on port 4670
* Analyzing classpath: 
  - ../../../defects4j_compiled/JacksonCore_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:34:54.739 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:34:54.863 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1255
* Using seed 1591328043110
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 05:35:19.964 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:5259.333333333333 - branchDistance:0.0 - coverage:2624.0 - ex: 2 - tex: 12
[MASTER] 05:35:19.965 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5259.333333333333, number of tests: 8, total length: 204
[MASTER] 05:35:29.071 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.8571428571428572 - fitness:3528.5 - branchDistance:0.0 - coverage:2606.0 - ex: 3 - tex: 17
[MASTER] 05:35:29.071 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3528.5, number of tests: 9, total length: 269
[MASTER] 05:35:54.742 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:3371.3571428571427 - branchDistance:0.0 - coverage:2618.0 - ex: 1 - tex: 9
[MASTER] 05:35:54.743 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3371.3571428571427, number of tests: 6, total length: 141
[MASTER] 05:36:11.224 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.607142857142857 - fitness:3196.687984496124 - branchDistance:0.0 - coverage:2624.5 - ex: 3 - tex: 15
[MASTER] 05:36:11.232 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3196.687984496124, number of tests: 9, total length: 260
* Computation finished
