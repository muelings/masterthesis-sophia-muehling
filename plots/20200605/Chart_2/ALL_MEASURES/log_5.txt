* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.general.DatasetUtilities
* Starting Client-0
* Connecting to master process on port 13983
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_2_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.general.DatasetUtilities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:44:50.288 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:44:50.366 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 564
* Using seed 1591235010915
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 03:45:28.386 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2395.0 - branchDistance:0.0 - coverage:1196.0 - ex: 0 - tex: 2
[MASTER] 03:45:28.392 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2395.0, number of tests: 1, total length: 21
[MASTER] 03:45:29.739 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2361.0 - branchDistance:0.0 - coverage:1162.0 - ex: 0 - tex: 14
[MASTER] 03:45:29.740 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2361.0, number of tests: 7, total length: 280
[MASTER] 03:45:41.389 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2337.0 - branchDistance:0.0 - coverage:1138.0 - ex: 0 - tex: 14
[MASTER] 03:45:41.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2337.0, number of tests: 8, total length: 234
[MASTER] 03:45:42.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2303.3333333333335 - branchDistance:0.0 - coverage:1104.3333333333335 - ex: 0 - tex: 8
[MASTER] 03:45:42.782 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2303.3333333333335, number of tests: 4, total length: 84
[MASTER] 03:45:54.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2276.0 - branchDistance:0.0 - coverage:1077.0 - ex: 0 - tex: 8
[MASTER] 03:45:54.036 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2276.0, number of tests: 7, total length: 157
[MASTER] 03:46:11.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2262.0 - branchDistance:0.0 - coverage:1063.0 - ex: 0 - tex: 6
[MASTER] 03:46:11.051 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2262.0, number of tests: 7, total length: 187
[MASTER] 03:46:15.616 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2236.0 - branchDistance:0.0 - coverage:1037.0 - ex: 0 - tex: 8
[MASTER] 03:46:15.623 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2236.0, number of tests: 8, total length: 189
[MASTER] 03:46:24.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2200.3333333333335 - branchDistance:0.0 - coverage:1001.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:46:24.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2200.3333333333335, number of tests: 7, total length: 153
[MASTER] 03:46:28.828 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2185.3333333333335 - branchDistance:0.0 - coverage:986.3333333333334 - ex: 0 - tex: 6
[MASTER] 03:46:28.830 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2185.3333333333335, number of tests: 7, total length: 151
[MASTER] 03:46:41.779 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2184.3333333333335 - branchDistance:0.0 - coverage:985.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:46:41.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2184.3333333333335, number of tests: 7, total length: 154
[MASTER] 03:46:43.310 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2175.3333333333335 - branchDistance:0.0 - coverage:976.3333333333334 - ex: 0 - tex: 6
[MASTER] 03:46:43.325 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2175.3333333333335, number of tests: 7, total length: 188
[MASTER] 03:46:50.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2173.3333333333335 - branchDistance:0.0 - coverage:974.3333333333334 - ex: 0 - tex: 10
[MASTER] 03:46:50.296 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2173.3333333333335, number of tests: 7, total length: 147
[MASTER] 03:46:51.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2157.3333333333335 - branchDistance:0.0 - coverage:958.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:46:51.635 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2157.3333333333335, number of tests: 7, total length: 145
[MASTER] 03:46:56.847 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2141.6666666666665 - branchDistance:0.0 - coverage:942.6666666666666 - ex: 0 - tex: 10
[MASTER] 03:46:56.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2141.6666666666665, number of tests: 8, total length: 207
[MASTER] 03:46:58.491 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1454.5516625866994 - branchDistance:0.0 - coverage:974.3333333333334 - ex: 0 - tex: 10
[MASTER] 03:46:58.498 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1454.5516625866994, number of tests: 8, total length: 202
[MASTER] 03:47:01.418 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1428.5516625866994 - branchDistance:0.0 - coverage:948.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:47:01.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1428.5516625866994, number of tests: 9, total length: 206
[MASTER] 03:47:08.290 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1426.5516625866994 - branchDistance:0.0 - coverage:946.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:47:08.304 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1426.5516625866994, number of tests: 8, total length: 174
[MASTER] 03:47:09.667 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1422.8849959200327 - branchDistance:0.0 - coverage:942.6666666666666 - ex: 0 - tex: 6
[MASTER] 03:47:09.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1422.8849959200327, number of tests: 7, total length: 147
[MASTER] 03:47:13.967 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1421.8849959200327 - branchDistance:0.0 - coverage:941.6666666666666 - ex: 0 - tex: 6
[MASTER] 03:47:13.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1421.8849959200327, number of tests: 7, total length: 147
[MASTER] 03:47:14.578 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1414.5516625866994 - branchDistance:0.0 - coverage:934.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:47:14.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1414.5516625866994, number of tests: 8, total length: 163
[MASTER] 03:47:16.338 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1400.8849959200327 - branchDistance:0.0 - coverage:920.6666666666666 - ex: 0 - tex: 6
[MASTER] 03:47:16.344 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1400.8849959200327, number of tests: 8, total length: 154
[MASTER] 03:47:24.129 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1396.8849959200327 - branchDistance:0.0 - coverage:916.6666666666666 - ex: 0 - tex: 6
[MASTER] 03:47:24.129 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1396.8849959200327, number of tests: 9, total length: 208
[MASTER] 03:47:24.901 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1395.8849959200327 - branchDistance:0.0 - coverage:915.6666666666666 - ex: 0 - tex: 8
[MASTER] 03:47:24.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1395.8849959200327, number of tests: 7, total length: 151
[MASTER] 03:47:34.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1389.8849959200327 - branchDistance:0.0 - coverage:909.6666666666666 - ex: 0 - tex: 8
[MASTER] 03:47:34.115 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1389.8849959200327, number of tests: 8, total length: 190
[MASTER] 03:47:37.422 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1382.8849959200327 - branchDistance:0.0 - coverage:902.6666666666666 - ex: 0 - tex: 8
[MASTER] 03:47:37.427 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1382.8849959200327, number of tests: 7, total length: 151
[MASTER] 03:47:39.213 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999043794224516 - fitness:1371.8849959200327 - branchDistance:0.0 - coverage:891.6666666666666 - ex: 0 - tex: 8
[MASTER] 03:47:39.222 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1371.8849959200327, number of tests: 8, total length: 192
[MASTER] 03:47:47.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1255.9613516441277 - branchDistance:0.0 - coverage:912.6666666666666 - ex: 0 - tex: 10
[MASTER] 03:47:47.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1255.9613516441277, number of tests: 8, total length: 218
[MASTER] 03:47:51.255 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1230.9613516441277 - branchDistance:0.0 - coverage:887.6666666666666 - ex: 0 - tex: 6
[MASTER] 03:47:51.256 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1230.9613516441277, number of tests: 9, total length: 209
[MASTER] 03:47:59.076 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1230.6280183107942 - branchDistance:0.0 - coverage:887.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:47:59.076 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1230.6280183107942, number of tests: 10, total length: 256
[MASTER] 03:48:01.170 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1227.9613516441277 - branchDistance:0.0 - coverage:884.6666666666666 - ex: 0 - tex: 6
[MASTER] 03:48:01.186 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1227.9613516441277, number of tests: 9, total length: 210
[MASTER] 03:48:05.025 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1227.6280183107942 - branchDistance:0.0 - coverage:884.3333333333334 - ex: 0 - tex: 8
[MASTER] 03:48:05.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1227.6280183107942, number of tests: 10, total length: 256
[MASTER] 03:48:10.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1226.4613516441277 - branchDistance:0.0 - coverage:883.1666666666666 - ex: 0 - tex: 12
[MASTER] 03:48:10.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1226.4613516441277, number of tests: 9, total length: 236
[MASTER] 03:48:11.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1214.4613516441277 - branchDistance:0.0 - coverage:871.1666666666666 - ex: 0 - tex: 12
[MASTER] 03:48:11.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1214.4613516441277, number of tests: 9, total length: 237
[MASTER] 03:48:19.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1211.9613516441277 - branchDistance:0.0 - coverage:868.6666666666666 - ex: 0 - tex: 8
[MASTER] 03:48:19.407 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1211.9613516441277, number of tests: 10, total length: 226
[MASTER] 03:48:25.146 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1205.9613516441277 - branchDistance:0.0 - coverage:862.6666666666666 - ex: 0 - tex: 8
[MASTER] 03:48:25.160 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1205.9613516441277, number of tests: 10, total length: 226
[MASTER] 03:48:25.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1196.9613516441277 - branchDistance:0.0 - coverage:853.6666666666666 - ex: 0 - tex: 8
[MASTER] 03:48:25.767 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1196.9613516441277, number of tests: 10, total length: 230
[MASTER] 03:48:29.846 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1194.9613516441277 - branchDistance:0.0 - coverage:851.6666666666666 - ex: 0 - tex: 10
[MASTER] 03:48:29.852 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1194.9613516441277, number of tests: 11, total length: 281
[MASTER] 03:48:30.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1194.4613516441277 - branchDistance:0.0 - coverage:851.1666666666666 - ex: 0 - tex: 10
[MASTER] 03:48:30.703 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1194.4613516441277, number of tests: 10, total length: 188
[MASTER] 03:48:35.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1169.9613516441277 - branchDistance:0.0 - coverage:826.6666666666666 - ex: 0 - tex: 12
[MASTER] 03:48:35.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1169.9613516441277, number of tests: 10, total length: 275
[MASTER] 03:48:43.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1163.9613516441277 - branchDistance:0.0 - coverage:820.6666666666666 - ex: 0 - tex: 14
[MASTER] 03:48:43.943 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1163.9613516441277, number of tests: 10, total length: 279
[MASTER] 03:48:48.464 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1160.6280183107942 - branchDistance:0.0 - coverage:817.3333333333334 - ex: 0 - tex: 12
[MASTER] 03:48:48.465 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1160.6280183107942, number of tests: 10, total length: 261
[MASTER] 03:48:52.200 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1157.9613516441277 - branchDistance:0.0 - coverage:814.6666666666666 - ex: 0 - tex: 12
[MASTER] 03:48:52.207 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1157.9613516441277, number of tests: 10, total length: 245
[MASTER] 03:48:57.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1155.9613516441277 - branchDistance:0.0 - coverage:812.6666666666666 - ex: 0 - tex: 12
[MASTER] 03:48:57.488 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1155.9613516441277, number of tests: 10, total length: 243
[MASTER] 03:48:59.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1152.6280183107942 - branchDistance:0.0 - coverage:809.3333333333334 - ex: 0 - tex: 12
[MASTER] 03:48:59.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1152.6280183107942, number of tests: 9, total length: 190
[MASTER] 03:49:00.825 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1145.1280183107942 - branchDistance:0.0 - coverage:801.8333333333333 - ex: 0 - tex: 12
[MASTER] 03:49:00.826 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1145.1280183107942, number of tests: 11, total length: 225
[MASTER] 03:49:03.654 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1143.9613516441277 - branchDistance:0.0 - coverage:800.6666666666666 - ex: 0 - tex: 14
[MASTER] 03:49:03.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1143.9613516441277, number of tests: 12, total length: 278
[MASTER] 03:49:07.741 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1130.6280183107942 - branchDistance:0.0 - coverage:787.3333333333333 - ex: 0 - tex: 12
[MASTER] 03:49:07.747 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1130.6280183107942, number of tests: 10, total length: 195
[MASTER] 03:49:11.083 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1126.6280183107942 - branchDistance:0.0 - coverage:783.3333333333334 - ex: 0 - tex: 16
[MASTER] 03:49:11.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1126.6280183107942, number of tests: 11, total length: 269
[MASTER] 03:49:14.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1117.6280183107942 - branchDistance:0.0 - coverage:774.3333333333333 - ex: 0 - tex: 12
[MASTER] 03:49:14.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1117.6280183107942, number of tests: 11, total length: 242
[MASTER] 03:49:16.875 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1106.6280183107942 - branchDistance:0.0 - coverage:763.3333333333333 - ex: 0 - tex: 16
[MASTER] 03:49:16.880 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1106.6280183107942, number of tests: 12, total length: 335
[MASTER] 03:49:30.075 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1092.6280183107942 - branchDistance:0.0 - coverage:749.3333333333333 - ex: 0 - tex: 18
[MASTER] 03:49:30.076 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1092.6280183107942, number of tests: 12, total length: 286
[MASTER] 03:49:32.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1084.6280183107942 - branchDistance:0.0 - coverage:741.3333333333333 - ex: 0 - tex: 18
[MASTER] 03:49:32.999 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1084.6280183107942, number of tests: 13, total length: 328
[MASTER] 03:49:39.117 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1082.6280183107942 - branchDistance:0.0 - coverage:739.3333333333333 - ex: 0 - tex: 18
[MASTER] 03:49:39.118 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1082.6280183107942, number of tests: 12, total length: 288
[MASTER] 03:49:40.587 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999082737112457 - fitness:1074.6280183107942 - branchDistance:0.0 - coverage:731.3333333333334 - ex: 0 - tex: 18
[MASTER] 03:49:40.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1074.6280183107942, number of tests: 13, total length: 354
[MASTER] 03:49:52.183 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 54 generations, 63080 statements, best individuals have fitness: 1074.6280183107942
[MASTER] 03:49:52.214 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 32%
* Total number of goals: 564
* Number of covered goals: 181
* Generated 13 tests with total length 354
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        308 / 300          Finished!
	- ZeroFitness :                  1,075 / 0           
[MASTER] 03:49:59.331 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:49:59.701 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 295 
[MASTER] 03:50:01.061 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.079 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.080 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.080 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.081 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.082 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.083 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.088 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.095 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 03:50:01.096 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:50:01.096 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:50:01.096 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:50:01.097 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:50:01.103 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:50:01.104 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:50:01.105 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:50:01.105 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 03:50:01.105 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:50:01.106 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 03:50:01.106 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 03:50:01.106 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 03:50:01.107 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 03:50:01.119 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 03:50:01.123 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 564
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.io.FilePermission: 
         execute /usr/bin/xprop: 1
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing tests to file
* Writing JUnit test case 'DatasetUtilities_ESTest' to evosuite-tests
* Done!

* Computation finished
