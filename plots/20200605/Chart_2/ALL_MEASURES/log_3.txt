* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.general.DatasetUtilities
* Starting Client-0
* Connecting to master process on port 7974
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_2_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.general.DatasetUtilities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:19:30.130 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:19:30.218 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 564
* Using seed 1591233489253
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 03:20:09.673 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2397.0 - branchDistance:0.0 - coverage:1198.0 - ex: 0 - tex: 2
[MASTER] 03:20:09.687 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2397.0, number of tests: 1, total length: 39
[MASTER] 03:20:20.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2377.0 - branchDistance:0.0 - coverage:1178.0 - ex: 0 - tex: 6
[MASTER] 03:20:20.761 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2377.0, number of tests: 3, total length: 57
[MASTER] 03:20:21.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2359.0 - branchDistance:0.0 - coverage:1160.0 - ex: 0 - tex: 4
[MASTER] 03:20:21.053 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2359.0, number of tests: 3, total length: 83
[MASTER] 03:20:22.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2267.0 - branchDistance:0.0 - coverage:1068.0 - ex: 0 - tex: 16
[MASTER] 03:20:22.512 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2267.0, number of tests: 9, total length: 284
[MASTER] 03:20:45.950 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:2259.0 - branchDistance:0.0 - coverage:1060.0 - ex: 0 - tex: 16
[MASTER] 03:20:45.967 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2259.0, number of tests: 9, total length: 328
[MASTER] 03:20:50.874 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998935943817835 - fitness:1678.220396697029 - branchDistance:0.0 - coverage:1198.0 - ex: 0 - tex: 10
[MASTER] 03:20:50.876 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1678.220396697029, number of tests: 7, total length: 301
[MASTER] 03:21:27.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999053926206245 - fitness:1678.2181350287617 - branchDistance:0.0 - coverage:1198.0 - ex: 0 - tex: 14
[MASTER] 03:21:27.280 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1678.2181350287617, number of tests: 10, total length: 287
[MASTER] 03:21:33.424 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999760111308353 - fitness:1678.204598230564 - branchDistance:0.0 - coverage:1198.0 - ex: 0 - tex: 14
[MASTER] 03:21:33.425 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1678.204598230564, number of tests: 8, total length: 263
[MASTER] 03:21:52.361 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4999909234483635 - fitness:1541.286601937719 - branchDistance:0.0 - coverage:1198.0 - ex: 0 - tex: 12
[MASTER] 03:21:52.367 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1541.286601937719, number of tests: 8, total length: 217
[MASTER] 03:21:54.878 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1392.2227591965825 - branchDistance:0.0 - coverage:1125.0 - ex: 0 - tex: 12
[MASTER] 03:21:54.883 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1392.2227591965825, number of tests: 8, total length: 201
[MASTER] 03:22:09.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1382.2227591965825 - branchDistance:0.0 - coverage:1115.0 - ex: 0 - tex: 10
[MASTER] 03:22:09.798 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1382.2227591965825, number of tests: 8, total length: 180
[MASTER] 03:22:12.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1378.2227591965825 - branchDistance:0.0 - coverage:1111.0 - ex: 0 - tex: 10
[MASTER] 03:22:12.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1378.2227591965825, number of tests: 8, total length: 158
[MASTER] 03:22:13.316 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1338.2227591965825 - branchDistance:0.0 - coverage:1071.0 - ex: 0 - tex: 6
[MASTER] 03:22:13.324 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1338.2227591965825, number of tests: 8, total length: 186
[MASTER] 03:22:17.873 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1320.2227591965825 - branchDistance:0.0 - coverage:1053.0 - ex: 0 - tex: 4
[MASTER] 03:22:17.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1320.2227591965825, number of tests: 8, total length: 141
[MASTER] 03:22:29.879 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1285.5560925299158 - branchDistance:0.0 - coverage:1018.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:22:29.880 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1285.5560925299158, number of tests: 8, total length: 148
[MASTER] 03:22:34.152 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1283.5560925299158 - branchDistance:0.0 - coverage:1016.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:22:34.153 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1283.5560925299158, number of tests: 9, total length: 155
[MASTER] 03:22:35.492 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1274.5560925299158 - branchDistance:0.0 - coverage:1007.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:22:35.493 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1274.5560925299158, number of tests: 8, total length: 168
[MASTER] 03:22:38.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1266.5560925299158 - branchDistance:0.0 - coverage:999.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:22:38.893 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1266.5560925299158, number of tests: 8, total length: 170
[MASTER] 03:22:43.983 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1263.5560925299158 - branchDistance:0.0 - coverage:996.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:22:43.984 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1263.5560925299158, number of tests: 9, total length: 226
[MASTER] 03:22:45.505 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1245.5560925299158 - branchDistance:0.0 - coverage:978.3333333333334 - ex: 0 - tex: 2
[MASTER] 03:22:45.505 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1245.5560925299158, number of tests: 9, total length: 172
[MASTER] 03:22:51.802 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1237.5560925299158 - branchDistance:0.0 - coverage:970.3333333333334 - ex: 0 - tex: 4
[MASTER] 03:22:51.808 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1237.5560925299158, number of tests: 10, total length: 181
[MASTER] 03:22:56.388 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1225.5560925299158 - branchDistance:0.0 - coverage:958.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:22:56.389 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1225.5560925299158, number of tests: 9, total length: 241
[MASTER] 03:22:56.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1216.5560925299158 - branchDistance:0.0 - coverage:949.3333333333334 - ex: 0 - tex: 4
[MASTER] 03:22:56.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1216.5560925299158, number of tests: 10, total length: 264
[MASTER] 03:23:01.581 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1206.5560925299158 - branchDistance:0.0 - coverage:939.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:23:01.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1206.5560925299158, number of tests: 10, total length: 268
[MASTER] 03:23:13.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1198.5560925299158 - branchDistance:0.0 - coverage:931.3333333333334 - ex: 0 - tex: 0
[MASTER] 03:23:13.317 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1198.5560925299158, number of tests: 10, total length: 271
[MASTER] 03:23:21.014 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1177.2227591965825 - branchDistance:0.0 - coverage:910.0 - ex: 0 - tex: 8
[MASTER] 03:23:21.020 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1177.2227591965825, number of tests: 12, total length: 294
[MASTER] 03:23:22.946 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4999909234483635 - fitness:1175.5560925299158 - branchDistance:0.0 - coverage:908.3333333333334 - ex: 0 - tex: 2
[MASTER] 03:23:22.960 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1175.5560925299158, number of tests: 9, total length: 229
* Computation finished
