* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.general.DatasetUtilities
* Starting Client-0
* Connecting to master process on port 20497
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_2_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.general.DatasetUtilities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 02:46:22.787 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 564
[MASTER] 02:48:50.995 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 02:50:48.623 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
*** Random test generation finished.
*=*=*=* Total tests: 2435 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 02:51:23.877 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 02:51:24.437 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:51:24.439 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 265 seconds more than allowed.
[MASTER] 02:51:24.525 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 02:51:24.527 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:51:24.587 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 564
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.io.FilePermission: 
         execute /usr/bin/xprop: 1
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 5
* Writing tests to file
* Writing JUnit test case 'DatasetUtilities_ESTest' to evosuite-tests
* Done!

* Computation finished
