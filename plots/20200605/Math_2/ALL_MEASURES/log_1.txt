* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting Client-0
* Connecting to master process on port 16363
* Analyzing classpath: 
  - ../../../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:10:48.003 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:10:48.086 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 38
* Using seed 1591254619334
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 09:11:04.163 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:177.0 - branchDistance:0.0 - coverage:88.0 - ex: 0 - tex: 6
[MASTER] 09:11:04.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 177.0, number of tests: 2, total length: 33
[MASTER] 09:11:19.834 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:137.0 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 6
[MASTER] 09:11:19.840 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.0, number of tests: 3, total length: 69
[MASTER] 09:11:27.443 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:128.33333333333331 - branchDistance:0.0 - coverage:39.33333333333333 - ex: 0 - tex: 4
[MASTER] 09:11:27.447 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.33333333333331, number of tests: 3, total length: 81
[MASTER] 09:11:30.093 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:108.966952937135 - branchDistance:0.0 - coverage:19.966952937135005 - ex: 0 - tex: 6
[MASTER] 09:11:30.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.966952937135, number of tests: 5, total length: 136
[MASTER] 09:11:52.925 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:104.98170876742306 - branchDistance:0.0 - coverage:15.981708767423052 - ex: 0 - tex: 10
[MASTER] 09:11:52.926 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.98170876742306, number of tests: 7, total length: 135
[MASTER] 09:12:09.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:102.98507462686567 - branchDistance:0.0 - coverage:13.985074626865671 - ex: 0 - tex: 12
[MASTER] 09:12:09.630 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.98507462686567, number of tests: 8, total length: 224
[MASTER] 09:13:15.998 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.496124031007752 - fitness:89.25337468804567 - branchDistance:0.0 - coverage:53.4987163029525 - ex: 1 - tex: 7
[MASTER] 09:13:16.000 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.25337468804567, number of tests: 5, total length: 132
[MASTER] 09:13:36.436 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4968944099378882 - fitness:88.23797626168756 - branchDistance:0.0 - coverage:52.4941951671602 - ex: 1 - tex: 5
[MASTER] 09:13:36.436 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.23797626168756, number of tests: 5, total length: 155
[MASTER] 09:13:43.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.496124031007752 - fitness:86.75209428252907 - branchDistance:0.0 - coverage:50.9974358974359 - ex: 1 - tex: 11
[MASTER] 09:13:43.793 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.75209428252907, number of tests: 6, total length: 157
[MASTER] 09:14:25.768 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.496124031007752 - fitness:86.7507673345095 - branchDistance:0.0 - coverage:50.99610894941634 - ex: 1 - tex: 7
[MASTER] 09:14:25.769 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.7507673345095, number of tests: 6, total length: 157
[MASTER] 09:15:03.797 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4968944099378882 - fitness:86.73599899336006 - branchDistance:0.0 - coverage:50.992217898832685 - ex: 1 - tex: 7
[MASTER] 09:15:03.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.73599899336006, number of tests: 7, total length: 151
[MASTER] 09:15:12.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4992784992784993 - fitness:86.20759756025345 - branchDistance:0.0 - coverage:50.4974358974359 - ex: 1 - tex: 9
[MASTER] 09:15:12.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.20759756025345, number of tests: 6, total length: 165
[MASTER] 09:15:48.113 [logback-1] WARN  ResourceController - Shutting down the search due to running out of computational resources

* Search finished after 301s and 6 generations, 7123 statements, best individuals have fitness: 86.20759756025345
[MASTER] 09:15:48.504 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:15:48.523 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 79%
* Total number of goals: 38
* Number of covered goals: 30
* Generated 6 tests with total length 165
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        320 / 300          Finished!
	- ZeroFitness :                     86 / 0           
	- org.evosuite.utils.ResourceController@52fa8a03
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 09:16:12.004 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:16:13.667 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 141 
[MASTER] 09:16:18.632 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 09:16:18.641 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 09:16:18.656 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 09:16:18.676 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 09:16:18.695 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:16:18.737 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:16:18.759 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:16:18.769 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 09:16:18.785 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 09:16:18.786 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 09:16:18.796 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:16:18.805 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing tests to file
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
