* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.time.TimeSeries
* Starting Client-0
* Connecting to master process on port 20535
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_3_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.time.TimeSeries
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:56:54.929 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:56:55.001 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 227
* Using seed 1591235742842
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 03:57:27.000 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4997157475838545 - fitness:712.2231066636343 - branchDistance:0.0 - coverage:508.0 - ex: 0 - tex: 12
[MASTER] 03:57:27.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 712.2231066636343, number of tests: 6, total length: 124
[MASTER] 03:57:30.580 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998910912655194 - fitness:698.2088524875838 - branchDistance:0.0 - coverage:494.0 - ex: 0 - tex: 20
[MASTER] 03:57:30.581 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 698.2088524875838, number of tests: 10, total length: 250
[MASTER] 03:57:44.155 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.996389891696751 - fitness:616.1147244805782 - branchDistance:0.0 - coverage:488.0 - ex: 0 - tex: 0
[MASTER] 03:57:44.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 616.1147244805782, number of tests: 1, total length: 10
* Computation finished
