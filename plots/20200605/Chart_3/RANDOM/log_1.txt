* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.time.TimeSeries
* Starting Client-0
* Connecting to master process on port 17787
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_3_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.time.TimeSeries
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 03:51:31.585 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 227
[MASTER] 03:55:26.087 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 03:55:33.759 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 201 seconds more than allowed.
* Computation finished
