* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.time.TimeSeries
* Starting Client-0
* Connecting to master process on port 8082
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_3_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.time.TimeSeries
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:25:35.242 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 227
*** Random test generation finished.
*=*=*=* Total tests: 5539 | Tests with assertion: 0
* Generated 0 tests with total length 0
[MASTER] 04:30:38.632 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                        303 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 04:30:39.065 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:30:39.067 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 258 seconds more than allowed.
[MASTER] 04:30:39.138 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 04:30:39.177 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:30:39.224 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 227
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.io.FilePermission: 
         execute /usr/bin/xprop: 1
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing tests to file
* Writing JUnit test case 'TimeSeries_ESTest' to evosuite-tests
* Done!

* Computation finished
