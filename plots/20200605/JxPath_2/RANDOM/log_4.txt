* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.compiler.Expression
* Starting Client-0
* Connecting to master process on port 3411
* Analyzing classpath: 
  - ../../../defects4j_compiled/JxPath_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.compiler.Expression
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 23
[MASTER] 10:50:37.145 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:50:53.456 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 10:51:04.462 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/compiler/Expression_1_tmp__ESTest.class' should be in target project, but could not be found!
* Computation finished
