* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.compiler.Expression
* Starting Client-0
* Connecting to master process on port 16928
* Analyzing classpath: 
  - ../../../defects4j_compiled/JxPath_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.compiler.Expression
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:48:13.682 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:48:13.801 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 23
* Using seed 1591346863168
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 10:48:49.461 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:74.0 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 10
[MASTER] 10:48:49.471 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.0, number of tests: 5, total length: 175
[MASTER] 10:49:04.060 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:69.0 - branchDistance:0.0 - coverage:28.0 - ex: 0 - tex: 10
[MASTER] 10:49:04.071 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.0, number of tests: 10, total length: 241
[MASTER] 10:49:37.500 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.0 - branchDistance:0.0 - coverage:25.0 - ex: 0 - tex: 12
[MASTER] 10:49:37.501 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0, number of tests: 10, total length: 306
* Computation finished
