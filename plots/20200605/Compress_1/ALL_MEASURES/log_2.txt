* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting Client-0
* Connecting to master process on port 16449
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 16:51:48.101 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 16:51:48.154 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
* Using seed 1591282280925
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 16:51:57.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:397.5642857142857 - branchDistance:0.0 - coverage:182.56428571428572 - ex: 0 - tex: 18
[MASTER] 16:51:57.192 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 397.5642857142857, number of tests: 9, total length: 179
[MASTER] 16:51:58.025 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:203.7723544973545 - branchDistance:0.0 - coverage:141.3464285714286 - ex: 0 - tex: 16
[MASTER] 16:51:58.026 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 203.7723544973545, number of tests: 8, total length: 169
[MASTER] 16:52:29.417 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:143.25925925925924 - branchDistance:0.0 - coverage:80.83333333333333 - ex: 0 - tex: 10
[MASTER] 16:52:29.419 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.25925925925924, number of tests: 5, total length: 88
[MASTER] 16:52:35.404 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:138.4330502847111 - branchDistance:0.0 - coverage:104.34642857142858 - ex: 0 - tex: 14
[MASTER] 16:52:35.411 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.4330502847111, number of tests: 8, total length: 196
[MASTER] 16:52:44.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:137.9330502847111 - branchDistance:0.0 - coverage:103.84642857142858 - ex: 0 - tex: 14
[MASTER] 16:52:44.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.9330502847111, number of tests: 8, total length: 176
[MASTER] 16:52:46.273 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:134.9330502847111 - branchDistance:0.0 - coverage:100.84642857142858 - ex: 0 - tex: 16
[MASTER] 16:52:46.280 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.9330502847111, number of tests: 9, total length: 187
[MASTER] 16:52:46.940 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:131.5925925925926 - branchDistance:0.0 - coverage:69.16666666666667 - ex: 0 - tex: 14
[MASTER] 16:52:46.942 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.5925925925926, number of tests: 7, total length: 149
[MASTER] 16:52:49.925 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:130.5925925925926 - branchDistance:0.0 - coverage:68.16666666666667 - ex: 0 - tex: 14
[MASTER] 16:52:49.936 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.5925925925926, number of tests: 8, total length: 191
[MASTER] 16:52:50.794 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:111.2532883799492 - branchDistance:0.0 - coverage:77.16666666666667 - ex: 0 - tex: 10
[MASTER] 16:52:50.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.2532883799492, number of tests: 7, total length: 126
[MASTER] 16:52:53.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:108.2532883799492 - branchDistance:0.0 - coverage:74.16666666666667 - ex: 0 - tex: 10
[MASTER] 16:52:53.693 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.2532883799492, number of tests: 6, total length: 114
[MASTER] 16:52:56.480 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:105.2532883799492 - branchDistance:0.0 - coverage:71.16666666666667 - ex: 0 - tex: 12
[MASTER] 16:52:56.481 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.2532883799492, number of tests: 7, total length: 154
[MASTER] 16:53:01.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:104.2532883799492 - branchDistance:0.0 - coverage:70.16666666666667 - ex: 0 - tex: 10
[MASTER] 16:53:01.056 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.2532883799492, number of tests: 7, total length: 153
[MASTER] 16:53:06.092 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:99.2532883799492 - branchDistance:0.0 - coverage:65.16666666666667 - ex: 0 - tex: 10
[MASTER] 16:53:06.100 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.2532883799492, number of tests: 6, total length: 124
[MASTER] 16:53:14.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:93.2532883799492 - branchDistance:0.0 - coverage:59.16666666666667 - ex: 0 - tex: 12
[MASTER] 16:53:14.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.2532883799492, number of tests: 7, total length: 158
[MASTER] 16:53:29.393 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:92.7532883799492 - branchDistance:0.0 - coverage:58.66666666666667 - ex: 0 - tex: 12
[MASTER] 16:53:29.402 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.7532883799492, number of tests: 7, total length: 145
[MASTER] 16:53:34.188 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:90.2532883799492 - branchDistance:0.0 - coverage:56.16666666666667 - ex: 0 - tex: 14
[MASTER] 16:53:34.189 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.2532883799492, number of tests: 8, total length: 221
[MASTER] 16:53:35.307 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:88.91995504661585 - branchDistance:0.0 - coverage:54.833333333333336 - ex: 0 - tex: 14
[MASTER] 16:53:35.308 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.91995504661585, number of tests: 8, total length: 180
[MASTER] 16:53:44.170 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:88.2532883799492 - branchDistance:0.0 - coverage:54.16666666666667 - ex: 0 - tex: 14
[MASTER] 16:53:44.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.2532883799492, number of tests: 8, total length: 198
[MASTER] 16:53:44.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:82.7532883799492 - branchDistance:0.0 - coverage:48.66666666666667 - ex: 0 - tex: 14
[MASTER] 16:53:44.340 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.7532883799492, number of tests: 8, total length: 226
[MASTER] 16:53:52.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:82.69796830505229 - branchDistance:0.0 - coverage:54.833333333333336 - ex: 0 - tex: 12
[MASTER] 16:53:52.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.69796830505229, number of tests: 7, total length: 163
[MASTER] 16:54:12.103 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.467870967741936 - fitness:79.91995504661585 - branchDistance:0.0 - coverage:45.833333333333336 - ex: 0 - tex: 14
[MASTER] 16:54:12.103 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.91995504661585, number of tests: 7, total length: 156
[MASTER] 16:54:13.022 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:78.53130163838561 - branchDistance:0.0 - coverage:50.66666666666667 - ex: 0 - tex: 12
[MASTER] 16:54:13.022 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.53130163838561, number of tests: 7, total length: 164
[MASTER] 16:54:13.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:74.79796830505228 - branchDistance:0.0 - coverage:46.93333333333334 - ex: 0 - tex: 12
[MASTER] 16:54:13.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.79796830505228, number of tests: 7, total length: 168
[MASTER] 16:54:14.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:73.19796830505229 - branchDistance:0.0 - coverage:45.333333333333336 - ex: 0 - tex: 12
[MASTER] 16:54:14.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.19796830505229, number of tests: 7, total length: 160
[MASTER] 16:54:24.266 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:72.69796830505229 - branchDistance:0.0 - coverage:44.833333333333336 - ex: 0 - tex: 12
[MASTER] 16:54:24.276 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.69796830505229, number of tests: 7, total length: 163
[MASTER] 16:54:33.217 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:70.69796830505229 - branchDistance:0.0 - coverage:42.833333333333336 - ex: 0 - tex: 14
[MASTER] 16:54:33.217 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.69796830505229, number of tests: 8, total length: 185
[MASTER] 16:54:45.930 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:70.53130163838561 - branchDistance:0.0 - coverage:42.66666666666667 - ex: 0 - tex: 14
[MASTER] 16:54:45.930 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.53130163838561, number of tests: 8, total length: 190
[MASTER] 16:54:48.659 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:70.19796830505229 - branchDistance:0.0 - coverage:42.333333333333336 - ex: 0 - tex: 14
[MASTER] 16:54:48.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.19796830505229, number of tests: 8, total length: 196
[MASTER] 16:55:10.525 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:69.69796830505229 - branchDistance:0.0 - coverage:42.333333333333336 - ex: 1 - tex: 15
[MASTER] 16:55:10.532 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.69796830505229, number of tests: 9, total length: 198
[MASTER] 16:55:21.007 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:69.19796830505229 - branchDistance:0.0 - coverage:41.333333333333336 - ex: 0 - tex: 16
[MASTER] 16:55:21.023 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.19796830505229, number of tests: 9, total length: 222
[MASTER] 16:55:41.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:68.19796830505229 - branchDistance:0.0 - coverage:40.333333333333336 - ex: 0 - tex: 22
[MASTER] 16:55:41.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.19796830505229, number of tests: 11, total length: 284
[MASTER] 16:55:44.908 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:67.86463497171894 - branchDistance:0.0 - coverage:40.0 - ex: 0 - tex: 14
[MASTER] 16:55:44.909 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.86463497171894, number of tests: 8, total length: 203
[MASTER] 16:55:45.281 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:66.69796830505229 - branchDistance:0.0 - coverage:39.333333333333336 - ex: 1 - tex: 21
[MASTER] 16:55:45.283 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.69796830505229, number of tests: 11, total length: 269
[MASTER] 16:55:58.094 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:62.36463497171894 - branchDistance:0.0 - coverage:34.5 - ex: 0 - tex: 18
[MASTER] 16:55:58.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.36463497171894, number of tests: 10, total length: 261
[MASTER] 16:56:06.056 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:60.86463497171894 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 22
[MASTER] 16:56:06.067 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.86463497171894, number of tests: 11, total length: 296
[MASTER] 16:56:13.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:59.86463497171894 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 24
[MASTER] 16:56:13.055 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.86463497171894, number of tests: 12, total length: 303
[MASTER] 16:56:20.824 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:57.86463497171894 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 22
[MASTER] 16:56:20.825 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.86463497171894, number of tests: 11, total length: 305
[MASTER] 16:56:25.586 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.965862935613421 - fitness:56.86463497171894 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 22
[MASTER] 16:56:25.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.86463497171894, number of tests: 11, total length: 292
[MASTER] 16:56:49.464 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 64 generations, 49849 statements, best individuals have fitness: 56.86463497171894
[MASTER] 16:56:49.496 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 79%
* Total number of goals: 97
* Number of covered goals: 77
* Generated 11 tests with total length 288
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        303 / 300          Finished!
	- RMIStoppingCondition
	- ZeroFitness :                     57 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 16:56:53.134 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 16:56:53.995 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 233 
[MASTER] 16:56:55.931 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 16:56:55.932 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 16:56:55.940 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:MockIOException at 13
[MASTER] 16:56:55.940 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [close:MockIOException, MockIOException:closeArchiveEntry-254,]
[MASTER] 16:56:55.940 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: writeEntryHeader:ArrayIndexOutOfBoundsException at 15
[MASTER] 16:56:55.941 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.942 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.942 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.943 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.943 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.945 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.951 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.952 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.953 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.953 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 16:56:55.953 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [close:MockIOException, writeEntryHeader:ArrayIndexOutOfBoundsException, MockIOException:closeArchiveEntry-254,]
[MASTER] 16:56:55.954 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 16:56:55.958 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 16:56:55.960 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 16:56:55.967 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 16:56:55.970 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 16:56:55.970 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 16:56:55.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 16:56:55.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 16:56:55.971 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 16:56:55.973 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 16:56:58.320 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 16:56:58.339 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 52%
* Total number of goals: 97
* Number of covered goals: 50
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 58
* Writing tests to file
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
