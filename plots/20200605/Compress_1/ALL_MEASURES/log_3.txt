* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting Client-0
* Connecting to master process on port 6521
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 16:58:49.881 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 16:58:49.953 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
* Using seed 1591282704579
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 16:58:59.425 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:391.5642857142857 - branchDistance:0.0 - coverage:176.56428571428572 - ex: 0 - tex: 18
[MASTER] 16:58:59.440 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 391.5642857142857, number of tests: 9, total length: 186
[MASTER] 16:59:00.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.992 - fitness:213.58834988540872 - branchDistance:0.0 - coverage:141.06428571428572 - ex: 0 - tex: 20
[MASTER] 16:59:00.472 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 213.58834988540872, number of tests: 10, total length: 233
[MASTER] 16:59:19.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:209.49021164021164 - branchDistance:0.0 - coverage:147.06428571428572 - ex: 0 - tex: 12
[MASTER] 16:59:19.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.49021164021164, number of tests: 6, total length: 167
[MASTER] 16:59:29.580 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:201.2723544973545 - branchDistance:0.0 - coverage:138.8464285714286 - ex: 0 - tex: 12
[MASTER] 16:59:29.581 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 201.2723544973545, number of tests: 6, total length: 167
[MASTER] 16:59:40.523 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:172.7723544973545 - branchDistance:0.0 - coverage:110.34642857142858 - ex: 0 - tex: 20
[MASTER] 16:59:40.524 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 172.7723544973545, number of tests: 11, total length: 242
[MASTER] 16:59:46.574 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:170.3080687830688 - branchDistance:0.0 - coverage:107.88214285714285 - ex: 0 - tex: 12
[MASTER] 16:59:46.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 170.3080687830688, number of tests: 8, total length: 164
[MASTER] 16:59:50.452 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:151.54338487147476 - branchDistance:0.0 - coverage:89.11745894554883 - ex: 0 - tex: 16
[MASTER] 16:59:50.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 151.54338487147476, number of tests: 8, total length: 187
[MASTER] 16:59:51.240 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:146.3767182048081 - branchDistance:0.0 - coverage:83.95079227888218 - ex: 0 - tex: 16
[MASTER] 16:59:51.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.3767182048081, number of tests: 8, total length: 193
[MASTER] 16:59:55.858 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:145.38783068783067 - branchDistance:0.0 - coverage:82.96190476190476 - ex: 0 - tex: 10
[MASTER] 16:59:55.863 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 145.38783068783067, number of tests: 7, total length: 176
[MASTER] 16:59:56.772 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:144.38783068783067 - branchDistance:0.0 - coverage:81.96190476190476 - ex: 0 - tex: 10
[MASTER] 16:59:56.773 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.38783068783067, number of tests: 8, total length: 202
[MASTER] 16:59:58.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:144.3767182048081 - branchDistance:0.0 - coverage:81.95079227888218 - ex: 0 - tex: 18
[MASTER] 16:59:58.983 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.3767182048081, number of tests: 9, total length: 207
[MASTER] 16:59:59.862 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:143.36345786068895 - branchDistance:0.0 - coverage:80.93753193476303 - ex: 0 - tex: 14
[MASTER] 16:59:59.877 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.36345786068895, number of tests: 8, total length: 218
[MASTER] 17:00:03.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:142.35687830687831 - branchDistance:0.0 - coverage:79.93095238095239 - ex: 0 - tex: 14
[MASTER] 17:00:03.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.35687830687831, number of tests: 8, total length: 155
[MASTER] 17:00:04.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:141.8767182048081 - branchDistance:0.0 - coverage:79.45079227888218 - ex: 0 - tex: 16
[MASTER] 17:00:04.325 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.8767182048081, number of tests: 8, total length: 210
[MASTER] 17:00:08.209 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:140.05449735449736 - branchDistance:0.0 - coverage:77.62857142857143 - ex: 0 - tex: 12
[MASTER] 17:00:08.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.05449735449736, number of tests: 8, total length: 162
[MASTER] 17:00:10.756 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:138.55449735449736 - branchDistance:0.0 - coverage:76.12857142857143 - ex: 0 - tex: 12
[MASTER] 17:00:10.757 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.55449735449736, number of tests: 9, total length: 172
[MASTER] 17:00:13.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:137.55449735449736 - branchDistance:0.0 - coverage:75.12857142857143 - ex: 0 - tex: 14
[MASTER] 17:00:13.048 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.55449735449736, number of tests: 8, total length: 200
[MASTER] 17:00:16.360 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:136.38783068783067 - branchDistance:0.0 - coverage:73.96190476190476 - ex: 0 - tex: 10
[MASTER] 17:00:16.360 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.38783068783067, number of tests: 7, total length: 183
[MASTER] 17:00:21.367 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:134.88783068783067 - branchDistance:0.0 - coverage:72.46190476190476 - ex: 0 - tex: 14
[MASTER] 17:00:21.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.88783068783067, number of tests: 9, total length: 209
[MASTER] 17:00:35.852 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:133.88783068783067 - branchDistance:0.0 - coverage:71.46190476190476 - ex: 0 - tex: 16
[MASTER] 17:00:35.859 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.88783068783067, number of tests: 10, total length: 196
[MASTER] 17:00:40.546 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:130.88783068783067 - branchDistance:0.0 - coverage:68.46190476190476 - ex: 0 - tex: 16
[MASTER] 17:00:40.546 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.88783068783067, number of tests: 10, total length: 244
[MASTER] 17:00:53.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:129.88783068783067 - branchDistance:0.0 - coverage:67.46190476190476 - ex: 0 - tex: 20
[MASTER] 17:00:53.210 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.88783068783067, number of tests: 10, total length: 227
[MASTER] 17:01:00.301 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:125.38783068783069 - branchDistance:0.0 - coverage:62.96190476190476 - ex: 0 - tex: 20
[MASTER] 17:01:00.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.38783068783069, number of tests: 11, total length: 238
[MASTER] 17:01:24.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:102.9419253616379 - branchDistance:0.0 - coverage:62.96190476190476 - ex: 0 - tex: 20
[MASTER] 17:01:24.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.9419253616379, number of tests: 11, total length: 229
[MASTER] 17:01:44.380 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:101.9419253616379 - branchDistance:0.0 - coverage:61.96190476190476 - ex: 0 - tex: 18
[MASTER] 17:01:44.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.9419253616379, number of tests: 10, total length: 215
[MASTER] 17:01:54.703 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:101.22763964735219 - branchDistance:0.0 - coverage:61.247619047619054 - ex: 0 - tex: 18
[MASTER] 17:01:54.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.22763964735219, number of tests: 10, total length: 212
[MASTER] 17:01:54.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:100.9419253616379 - branchDistance:0.0 - coverage:60.96190476190476 - ex: 0 - tex: 20
[MASTER] 17:01:54.936 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.9419253616379, number of tests: 11, total length: 236
[MASTER] 17:01:55.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:100.29430631401885 - branchDistance:0.0 - coverage:60.31428571428572 - ex: 0 - tex: 18
[MASTER] 17:01:55.936 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.29430631401885, number of tests: 11, total length: 237
[MASTER] 17:01:59.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:99.9419253616379 - branchDistance:0.0 - coverage:59.96190476190476 - ex: 0 - tex: 18
[MASTER] 17:01:59.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.9419253616379, number of tests: 10, total length: 198
[MASTER] 17:01:59.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:97.9419253616379 - branchDistance:0.0 - coverage:57.96190476190476 - ex: 0 - tex: 22
[MASTER] 17:01:59.696 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.9419253616379, number of tests: 12, total length: 263
[MASTER] 17:02:01.670 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.489991967871486 - fitness:94.28813237841115 - branchDistance:0.0 - coverage:60.31428571428572 - ex: 0 - tex: 20
[MASTER] 17:02:01.670 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.28813237841115, number of tests: 11, total length: 237
[MASTER] 17:02:03.210 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.489991967871486 - fitness:92.53336397280358 - branchDistance:0.0 - coverage:62.96190476190476 - ex: 0 - tex: 22
[MASTER] 17:02:03.211 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.53336397280358, number of tests: 12, total length: 249
[MASTER] 17:02:08.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.489991967871486 - fitness:91.53336397280358 - branchDistance:0.0 - coverage:61.96190476190476 - ex: 0 - tex: 22
[MASTER] 17:02:08.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.53336397280358, number of tests: 12, total length: 276
[MASTER] 17:02:11.972 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.489991967871486 - fitness:88.38574492518455 - branchDistance:0.0 - coverage:58.81428571428572 - ex: 0 - tex: 24
[MASTER] 17:02:11.979 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.38574492518455, number of tests: 12, total length: 237
[MASTER] 17:02:15.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.489991967871486 - fitness:86.88574492518455 - branchDistance:0.0 - coverage:57.31428571428572 - ex: 0 - tex: 20
[MASTER] 17:02:15.739 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.88574492518455, number of tests: 12, total length: 239
[MASTER] 17:02:20.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.489991967871486 - fitness:86.38574492518455 - branchDistance:0.0 - coverage:56.81428571428572 - ex: 0 - tex: 24
[MASTER] 17:02:20.555 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.38574492518455, number of tests: 12, total length: 243
[MASTER] 17:02:22.316 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:83.5204344138176 - branchDistance:0.0 - coverage:57.31428571428572 - ex: 0 - tex: 20
[MASTER] 17:02:22.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.5204344138176, number of tests: 12, total length: 242
[MASTER] 17:02:32.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:82.16805346143664 - branchDistance:0.0 - coverage:55.96190476190476 - ex: 0 - tex: 22
[MASTER] 17:02:32.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.16805346143664, number of tests: 13, total length: 295
[MASTER] 17:02:34.943 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:81.5204344138176 - branchDistance:0.0 - coverage:55.31428571428572 - ex: 0 - tex: 22
[MASTER] 17:02:34.947 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.5204344138176, number of tests: 13, total length: 260
[MASTER] 17:03:05.057 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:80.0204344138176 - branchDistance:0.0 - coverage:53.81428571428572 - ex: 0 - tex: 24
[MASTER] 17:03:05.067 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.0204344138176, number of tests: 14, total length: 275
[MASTER] 17:03:11.698 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:79.0204344138176 - branchDistance:0.0 - coverage:52.81428571428572 - ex: 0 - tex: 24
[MASTER] 17:03:11.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.0204344138176, number of tests: 14, total length: 289
[MASTER] 17:03:19.426 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:78.68710108048427 - branchDistance:0.0 - coverage:52.48095238095238 - ex: 0 - tex: 26
[MASTER] 17:03:19.427 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.68710108048427, number of tests: 15, total length: 286
[MASTER] 17:03:23.444 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:78.5204344138176 - branchDistance:0.0 - coverage:52.31428571428572 - ex: 0 - tex: 24
[MASTER] 17:03:23.444 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.5204344138176, number of tests: 14, total length: 293
[MASTER] 17:03:25.524 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:78.35376774715093 - branchDistance:0.0 - coverage:52.147619047619045 - ex: 0 - tex: 22
[MASTER] 17:03:25.525 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.35376774715093, number of tests: 13, total length: 256
[MASTER] 17:03:31.334 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.489991967871486 - fitness:78.18710108048427 - branchDistance:0.0 - coverage:51.98095238095238 - ex: 0 - tex: 28
[MASTER] 17:03:31.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.18710108048427, number of tests: 15, total length: 307
[MASTER] 17:03:45.690 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.489991967871486 - fitness:75.19769082054576 - branchDistance:0.0 - coverage:51.647619047619045 - ex: 0 - tex: 26
[MASTER] 17:03:45.695 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.19769082054576, number of tests: 14, total length: 301
[MASTER] 17:03:51.526 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 87 generations, 66058 statements, best individuals have fitness: 75.19769082054576
[MASTER] 17:03:51.565 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 64%
* Total number of goals: 97
* Number of covered goals: 62
* Generated 14 tests with total length 301
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     75 / 0           
	- MaxTime :                        303 / 300          Finished!
[MASTER] 17:03:53.934 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:03:54.624 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 213 
[MASTER] 17:03:57.279 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.280 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.281 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.287 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 1.0
[MASTER] 17:03:57.287 [logback-1] WARN  RegressionSuiteMinimizer - Test3, uniqueExceptions: []
[MASTER] 17:03:57.288 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: write:MockIOException at 29
[MASTER] 17:03:57.303 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.305 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.305 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.306 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.307 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.311 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.311 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.312 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.312 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 17:03:57.312 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [write:MockIOException]
[MASTER] 17:03:57.313 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 17:03:57.313 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 17:03:57.320 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 17:03:57.321 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 17:03:57.322 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 17:03:57.322 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 17:03:57.322 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 17:03:57.327 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 17:03:57.327 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 17:03:57.335 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 17:03:57.336 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 17:03:57.336 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 17:03:57.336 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 17:03:58.473 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 17:03:58.487 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 24%
* Total number of goals: 97
* Number of covered goals: 23
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 89
* Writing tests to file
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
