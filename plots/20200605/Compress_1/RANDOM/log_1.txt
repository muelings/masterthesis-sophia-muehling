* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting Client-0
* Connecting to master process on port 10473
* Analyzing classpath: 
  - ../../../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 16:43:11.355 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
[MASTER] 16:43:16.951 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("\u0001\uFFFD\u06AC\u00190707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var1.toString());  // (Inspector) Original Value: �ڬ0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: �ڬ
[MASTER] 16:43:16.954 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(129, var1.size());  // (Inspector) Original Value: 129 | Regression Value: 5
[MASTER] 16:43:17.007 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:43:17.020 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 16:43:25.112 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 16:43:25.784 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("\u0001\uFFFD\u06AC\u00190707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var1.toString());  // (Inspector) Original Value: �ڬ0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: �ڬ
[MASTER] 16:43:25.792 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(129, var1.size());  // (Inspector) Original Value: 129 | Regression Value: 5
[MASTER] 16:43:25.808 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:43:25.809 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 16:43:27.384 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 16:43:27.545 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("\u0001\uFFFD\u06AC\u00190707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var1.toString());  // (Inspector) Original Value: �ڬ0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: �ڬ
[MASTER] 16:43:27.552 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(129, var1.size());  // (Inspector) Original Value: 129 | Regression Value: 5
[MASTER] 16:43:27.555 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:43:27.561 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 51 | Tests with assertion: 1
* Generated 1 tests with total length 16
[MASTER] 16:43:27.596 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         16 / 300         
	- RMIStoppingCondition
[MASTER] 16:43:35.306 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 16:43:35.630 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 10 
[MASTER] 16:43:35.831 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[toString, size]}
[MASTER] 16:43:35.859 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.InspectorAssertion@b45c9538, org.evosuite.assertion.InspectorAssertion@6bb99003] invalid assertion(s) to be removed
[MASTER] 16:43:35.875 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 16:43:35.902 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 16:43:35.928 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing tests to file
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
