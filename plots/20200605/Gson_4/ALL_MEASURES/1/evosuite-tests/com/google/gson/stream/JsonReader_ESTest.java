/*
 * This file was automatically generated by EvoSuite
 * Fri Jun 05 00:01:57 GMT 2020
 */

package com.google.gson.stream;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.StringReader;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class JsonReader_ESTest extends JsonReader_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      StringReader stringReader0 = new StringReader("*\"");
      JsonReader jsonReader0 = new JsonReader(stringReader0);
      stringReader0.read();
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalStateException : Expected BEGIN_ARRAY but was STRING at line 1 column 2 path $
      // Modified Version:
      //     com.google.gson.stream.MalformedJsonException : Use JsonReader.setLenient(true) to accept malformed JSON at line 1 column 2 path $
      // Undeclared exception!
      try { 
        jsonReader0.beginArray();
        fail("Expecting exception: IllegalStateException");
      
      } catch(IllegalStateException e) {
         //
         // Expected BEGIN_ARRAY but was STRING at line 1 column 2 path $
         //
         verifyException("com.google.gson.stream.JsonReader", e);
         assertTrue(e.getMessage().equals("Expected BEGIN_ARRAY but was STRING at line 1 column 2 path $"));   
      }
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      StringReader stringReader0 = new StringReader("}");
      JsonReader jsonReader0 = new JsonReader(stringReader0);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     com.google.gson.stream.MalformedJsonException : Expected value at line 1 column 1 path $
      // Modified Version:
      //     com.google.gson.stream.MalformedJsonException : Use JsonReader.setLenient(true) to accept malformed JSON at line 1 column 1 path $
      try { 
        jsonReader0.nextString();
        fail("Expecting exception: IOException");
      
      } catch(IOException e) {
         //
         // Expected value at line 1 column 1 path $
         //
         verifyException("com.google.gson.stream.JsonReader", e);
         assertTrue(e.getMessage().equals("Expected value at line 1 column 1 path $"));   
      }
  }

  @Test(timeout = 4000)
  public void test2()  throws Throwable  {
      StringReader stringReader0 = new StringReader("NULL");
      JsonReader jsonReader0 = new JsonReader(stringReader0);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalStateException : Expected END_OBJECT but was NULL at line 1 column 5 path $
      // Modified Version:
      //     com.google.gson.stream.MalformedJsonException : Use JsonReader.setLenient(true) to accept malformed JSON at line 1 column 1 path $
      // Undeclared exception!
      try { 
        jsonReader0.endObject();
        fail("Expecting exception: IllegalStateException");
      
      } catch(IllegalStateException e) {
         //
         // Expected END_OBJECT but was NULL at line 1 column 5 path $
         //
         verifyException("com.google.gson.stream.JsonReader", e);
         assertTrue(e.getMessage().equals("Expected END_OBJECT but was NULL at line 1 column 5 path $"));   
      }
  }

  @Test(timeout = 4000)
  public void test3()  throws Throwable  {
      StringReader stringReader0 = new StringReader("false");
      JsonReader jsonReader0 = new JsonReader(stringReader0);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalStateException : Expected a long but was BOOLEAN at line 1 column 6 path $
      // Modified Version:
      //     com.google.gson.stream.MalformedJsonException : Use JsonReader.setLenient(true) to accept malformed JSON at line 1 column 1 path $
      // Undeclared exception!
      try { 
        jsonReader0.nextLong();
        fail("Expecting exception: IllegalStateException");
      
      } catch(IllegalStateException e) {
         //
         // Expected a long but was BOOLEAN at line 1 column 6 path $
         //
         verifyException("com.google.gson.stream.JsonReader", e);
         assertTrue(e.getMessage().equals("Expected a long but was BOOLEAN at line 1 column 6 path $"));   
      }
  }

  @Test(timeout = 4000)
  public void test4()  throws Throwable  {
      StringReader stringReader0 = new StringReader("3");
      JsonReader jsonReader0 = new JsonReader(stringReader0);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     com.google.gson.stream.MalformedJsonException : Use JsonReader.setLenient(true) to accept malformed JSON at line 1 column 1 path $
      jsonReader0.nextInt();
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalStateException : Expected BEGIN_OBJECT but was END_DOCUMENT at line 1 column 2 path $
      // Undeclared exception!
      try { 
        jsonReader0.beginObject();
        fail("Expecting exception: IllegalStateException");
      
      } catch(IllegalStateException e) {
         //
         // Expected BEGIN_OBJECT but was END_DOCUMENT at line 1 column 2 path $
         //
         verifyException("com.google.gson.stream.JsonReader", e);
         assertTrue(e.getMessage().equals("Expected BEGIN_OBJECT but was END_DOCUMENT at line 1 column 2 path $"));   
      }
  }
}
