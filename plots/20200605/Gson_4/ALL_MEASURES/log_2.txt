* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.stream.JsonWriter
* Starting Client-0
* Connecting to master process on port 12492
* Analyzing classpath: 
  - ../../../defects4j_compiled/Gson_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.stream.JsonWriter
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 02:40:52.881 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 02:40:52.933 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 111
* Using seed 1591317626452
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 02:41:03.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:529.0 - branchDistance:0.0 - coverage:264.0 - ex: 0 - tex: 2
[MASTER] 02:41:03.500 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 529.0, number of tests: 1, total length: 35
[MASTER] 02:41:04.582 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4827586206896552 - fitness:290.28181818181815 - branchDistance:0.0 - coverage:182.94848484848484 - ex: 0 - tex: 10
[MASTER] 02:41:04.611 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 290.28181818181815, number of tests: 5, total length: 124
[MASTER] 02:41:06.038 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9444444444444446 - fitness:208.66207563126736 - branchDistance:0.0 - coverage:154.26881720430106 - ex: 0 - tex: 16
[MASTER] 02:41:06.060 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 208.66207563126736, number of tests: 9, total length: 139
[MASTER] 02:41:26.533 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9444444444444446 - fitness:204.66207563126736 - branchDistance:0.0 - coverage:150.26881720430106 - ex: 0 - tex: 12
[MASTER] 02:41:26.544 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 204.66207563126736, number of tests: 7, total length: 94
[MASTER] 02:41:26.848 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9864864864864864 - fitness:203.46392276422765 - branchDistance:0.0 - coverage:149.52083333333334 - ex: 0 - tex: 10
[MASTER] 02:41:26.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 203.46392276422765, number of tests: 6, total length: 94
[MASTER] 02:41:27.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:193.23034467713785 - branchDistance:0.0 - coverage:163.20416666666665 - ex: 0 - tex: 10
[MASTER] 02:41:27.088 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 193.23034467713785, number of tests: 5, total length: 137
[MASTER] 02:41:28.137 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.48 - fitness:191.69738863287247 - branchDistance:0.0 - coverage:131.76881720430106 - ex: 0 - tex: 18
[MASTER] 02:41:28.344 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 191.69738863287247, number of tests: 10, total length: 228
[MASTER] 02:41:31.335 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.490196078431373 - fitness:185.74342037595622 - branchDistance:0.0 - coverage:136.65770609024193 - ex: 0 - tex: 14
[MASTER] 02:41:31.344 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 185.74342037595622, number of tests: 8, total length: 174
[MASTER] 02:41:33.336 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9444444444444446 - fitness:183.58515255434432 - branchDistance:0.0 - coverage:129.19189412737802 - ex: 0 - tex: 16
[MASTER] 02:41:33.338 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 183.58515255434432, number of tests: 9, total length: 135
[MASTER] 02:41:35.436 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:181.29499521477226 - branchDistance:0.0 - coverage:151.26881720430106 - ex: 0 - tex: 10
[MASTER] 02:41:35.437 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 181.29499521477226, number of tests: 6, total length: 98
[MASTER] 02:41:36.666 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:174.89701134380454 - branchDistance:0.0 - coverage:144.87083333333334 - ex: 0 - tex: 20
[MASTER] 02:41:36.676 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 174.89701134380454, number of tests: 10, total length: 260
[MASTER] 02:41:40.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:169.81721743404648 - branchDistance:0.0 - coverage:139.79103942357528 - ex: 0 - tex: 18
[MASTER] 02:41:40.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 169.81721743404648, number of tests: 9, total length: 214
[MASTER] 02:41:42.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:154.18388410071313 - branchDistance:0.0 - coverage:124.15770609024193 - ex: 0 - tex: 16
[MASTER] 02:41:42.779 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.18388410071313, number of tests: 9, total length: 190
[MASTER] 02:41:53.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:150.18388410071313 - branchDistance:0.0 - coverage:120.15770609024193 - ex: 0 - tex: 16
[MASTER] 02:41:53.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 150.18388410071313, number of tests: 9, total length: 164
[MASTER] 02:41:59.007 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:141.01721743404647 - branchDistance:0.0 - coverage:110.99103942357527 - ex: 0 - tex: 18
[MASTER] 02:41:59.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.01721743404647, number of tests: 10, total length: 227
[MASTER] 02:42:03.940 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:133.51721743404647 - branchDistance:0.0 - coverage:103.49103942357527 - ex: 0 - tex: 20
[MASTER] 02:42:03.940 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.51721743404647, number of tests: 10, total length: 230
[MASTER] 02:42:07.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:132.4505507673798 - branchDistance:0.0 - coverage:102.42437275690861 - ex: 0 - tex: 18
[MASTER] 02:42:07.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.4505507673798, number of tests: 10, total length: 212
[MASTER] 02:42:10.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:126.18388410071316 - branchDistance:0.0 - coverage:96.15770609024194 - ex: 0 - tex: 20
[MASTER] 02:42:10.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.18388410071316, number of tests: 10, total length: 231
[MASTER] 02:42:17.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:120.1172174340465 - branchDistance:0.0 - coverage:90.09103942357528 - ex: 0 - tex: 20
[MASTER] 02:42:17.905 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.1172174340465, number of tests: 10, total length: 216
[MASTER] 02:42:22.561 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:115.95055076737981 - branchDistance:0.0 - coverage:85.92437275690861 - ex: 0 - tex: 20
[MASTER] 02:42:22.567 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.95055076737981, number of tests: 10, total length: 214
[MASTER] 02:42:25.633 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:114.18388410071316 - branchDistance:0.0 - coverage:84.15770609024194 - ex: 0 - tex: 20
[MASTER] 02:42:25.634 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.18388410071316, number of tests: 10, total length: 233
[MASTER] 02:42:27.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:110.83539925222829 - branchDistance:0.0 - coverage:80.80922124175709 - ex: 0 - tex: 20
[MASTER] 02:42:27.923 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.83539925222829, number of tests: 10, total length: 234
[MASTER] 02:42:30.759 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:107.28388410071315 - branchDistance:0.0 - coverage:77.25770609024194 - ex: 0 - tex: 20
[MASTER] 02:42:30.759 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.28388410071315, number of tests: 10, total length: 209
[MASTER] 02:42:35.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:106.95055076737981 - branchDistance:0.0 - coverage:76.92437275690861 - ex: 0 - tex: 26
[MASTER] 02:42:35.182 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.95055076737981, number of tests: 13, total length: 282
[MASTER] 02:42:38.407 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:106.16337128020032 - branchDistance:0.0 - coverage:76.13719326972912 - ex: 0 - tex: 26
[MASTER] 02:42:38.408 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.16337128020032, number of tests: 13, total length: 305
[MASTER] 02:42:38.889 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:98.1172174340465 - branchDistance:0.0 - coverage:68.09103942357528 - ex: 0 - tex: 22
[MASTER] 02:42:38.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.1172174340465, number of tests: 11, total length: 247
[MASTER] 02:42:39.639 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:92.46337128020033 - branchDistance:0.0 - coverage:62.43719326972912 - ex: 0 - tex: 26
[MASTER] 02:42:39.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.46337128020033, number of tests: 13, total length: 290
[MASTER] 02:42:45.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:92.29670461353368 - branchDistance:0.0 - coverage:62.270526603062464 - ex: 0 - tex: 24
[MASTER] 02:42:45.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.29670461353368, number of tests: 12, total length: 233
[MASTER] 02:42:46.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:90.37362769045674 - branchDistance:0.0 - coverage:60.34744967998553 - ex: 0 - tex: 24
[MASTER] 02:42:46.272 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.37362769045674, number of tests: 12, total length: 273
[MASTER] 02:42:47.727 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:89.46337128314835 - branchDistance:0.0 - coverage:59.43719327267715 - ex: 0 - tex: 26
[MASTER] 02:42:47.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.46337128314835, number of tests: 13, total length: 301
[MASTER] 02:42:50.041 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:88.86721743404647 - branchDistance:0.0 - coverage:58.84103942357527 - ex: 0 - tex: 26
[MASTER] 02:42:50.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.86721743404647, number of tests: 13, total length: 287
[MASTER] 02:42:55.426 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:87.49839307621195 - branchDistance:0.0 - coverage:60.34744967998553 - ex: 0 - tex: 24
[MASTER] 02:42:55.432 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.49839307621195, number of tests: 12, total length: 274
[MASTER] 02:42:57.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.095238095238095 - fitness:86.71337128020033 - branchDistance:0.0 - coverage:56.68719326972912 - ex: 0 - tex: 24
[MASTER] 02:42:57.891 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.71337128020033, number of tests: 12, total length: 234
[MASTER] 02:42:58.998 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:86.08813666595555 - branchDistance:0.0 - coverage:58.93719326972912 - ex: 0 - tex: 26
[MASTER] 02:42:59.004 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.08813666595555, number of tests: 13, total length: 249
[MASTER] 02:43:03.986 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:83.83813666595555 - branchDistance:0.0 - coverage:56.68719326972912 - ex: 0 - tex: 24
[MASTER] 02:43:04.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.83813666595555, number of tests: 12, total length: 222
[MASTER] 02:43:12.933 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:83.33813666595555 - branchDistance:0.0 - coverage:56.18719326972912 - ex: 0 - tex: 26
[MASTER] 02:43:12.940 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.33813666595555, number of tests: 13, total length: 239
[MASTER] 02:43:17.708 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:82.83813666595555 - branchDistance:0.0 - coverage:55.68719326972912 - ex: 0 - tex: 26
[MASTER] 02:43:17.710 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.83813666595555, number of tests: 13, total length: 258
[MASTER] 02:43:21.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:81.83813666595555 - branchDistance:0.0 - coverage:54.68719326972912 - ex: 0 - tex: 24
[MASTER] 02:43:21.351 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.83813666595555, number of tests: 12, total length: 226
[MASTER] 02:43:21.747 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:80.83813666595555 - branchDistance:0.0 - coverage:53.68719326972912 - ex: 0 - tex: 26
[MASTER] 02:43:21.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.83813666595555, number of tests: 14, total length: 254
[MASTER] 02:43:25.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:80.22702555484443 - branchDistance:0.0 - coverage:53.07608215861801 - ex: 0 - tex: 26
[MASTER] 02:43:25.171 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.22702555484443, number of tests: 14, total length: 260
[MASTER] 02:43:33.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:79.33813666595555 - branchDistance:0.0 - coverage:52.18719326972912 - ex: 0 - tex: 26
[MASTER] 02:43:33.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.33813666595555, number of tests: 14, total length: 272
[MASTER] 02:43:34.617 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:78.22702555484443 - branchDistance:0.0 - coverage:51.07608215861801 - ex: 0 - tex: 26
[MASTER] 02:43:34.623 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.22702555484443, number of tests: 14, total length: 262
[MASTER] 02:43:39.578 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:77.33813666595555 - branchDistance:0.0 - coverage:50.18719326972912 - ex: 0 - tex: 26
[MASTER] 02:43:39.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.33813666595555, number of tests: 14, total length: 273
[MASTER] 02:43:40.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:76.72702555484443 - branchDistance:0.0 - coverage:49.57608215861801 - ex: 0 - tex: 28
[MASTER] 02:43:40.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.72702555484443, number of tests: 15, total length: 285
[MASTER] 02:43:43.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:75.72702555484443 - branchDistance:0.0 - coverage:48.57608215861801 - ex: 0 - tex: 28
[MASTER] 02:43:43.873 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.72702555484443, number of tests: 15, total length: 285
[MASTER] 02:43:52.584 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:74.88087170869059 - branchDistance:0.0 - coverage:47.72992831246417 - ex: 0 - tex: 30
[MASTER] 02:43:52.585 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.88087170869059, number of tests: 16, total length: 321
[MASTER] 02:43:54.920 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:74.38087170869059 - branchDistance:0.0 - coverage:47.22992831246417 - ex: 0 - tex: 32
[MASTER] 02:43:54.921 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.38087170869059, number of tests: 17, total length: 332
[MASTER] 02:43:58.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:73.88087170869059 - branchDistance:0.0 - coverage:46.72992831246417 - ex: 0 - tex: 30
[MASTER] 02:43:58.776 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.88087170869059, number of tests: 16, total length: 319
[MASTER] 02:44:00.829 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:72.13087170869875 - branchDistance:0.0 - coverage:44.97992831247234 - ex: 0 - tex: 28
[MASTER] 02:44:00.830 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.13087170869875, number of tests: 15, total length: 308
[MASTER] 02:44:06.443 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:69.01976059757948 - branchDistance:0.0 - coverage:41.86881720135305 - ex: 0 - tex: 30
[MASTER] 02:44:06.447 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.01976059757948, number of tests: 16, total length: 321
[MASTER] 02:44:14.705 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:68.86591444373332 - branchDistance:0.0 - coverage:41.71497104750691 - ex: 0 - tex: 28
[MASTER] 02:44:14.706 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.86591444373332, number of tests: 16, total length: 300
[MASTER] 02:44:21.077 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:67.01976059757948 - branchDistance:0.0 - coverage:39.86881720135305 - ex: 0 - tex: 28
[MASTER] 02:44:21.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.01976059757948, number of tests: 16, total length: 307
[MASTER] 02:44:30.529 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:65.51976059757948 - branchDistance:0.0 - coverage:38.36881720135305 - ex: 0 - tex: 28
[MASTER] 02:44:30.530 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.51976059757948, number of tests: 16, total length: 311
[MASTER] 02:44:43.146 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:64.51976059757948 - branchDistance:0.0 - coverage:37.36881720135305 - ex: 0 - tex: 28
[MASTER] 02:44:43.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.51976059757948, number of tests: 16, total length: 308
[MASTER] 02:44:47.766 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:64.01976059757948 - branchDistance:0.0 - coverage:36.86881720135305 - ex: 0 - tex: 30
[MASTER] 02:44:47.772 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.01976059757948, number of tests: 17, total length: 326
[MASTER] 02:44:53.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.095238095238095 - fitness:63.51976059757947 - branchDistance:0.0 - coverage:36.36881720135305 - ex: 0 - tex: 28
[MASTER] 02:44:53.187 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.51976059757947, number of tests: 16, total length: 314
[MASTER] 02:44:57.814 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.121212121212121 - fitness:63.452649536682394 - branchDistance:0.0 - coverage:36.36881720135305 - ex: 0 - tex: 28
[MASTER] 02:44:57.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.452649536682394, number of tests: 16, total length: 313
[MASTER] 02:45:03.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.761904761904763 - fitness:63.39979065268048 - branchDistance:0.0 - coverage:37.86881720135305 - ex: 0 - tex: 28
[MASTER] 02:45:03.147 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.39979065268048, number of tests: 16, total length: 310
[MASTER] 02:45:09.662 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.761904761904763 - fitness:62.39979065268048 - branchDistance:0.0 - coverage:36.86881720135305 - ex: 0 - tex: 28
[MASTER] 02:45:09.670 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.39979065268048, number of tests: 16, total length: 316
[MASTER] 02:45:15.552 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.27777777777778 - fitness:55.15874004446739 - branchDistance:0.0 - coverage:40.21497104750691 - ex: 1 - tex: 27
[MASTER] 02:45:15.559 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.15874004446739, number of tests: 16, total length: 315
[MASTER] 02:45:22.232 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.27777777777778 - fitness:51.81258619831354 - branchDistance:0.0 - coverage:36.86881720135305 - ex: 1 - tex: 27
[MASTER] 02:45:22.238 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.81258619831354, number of tests: 16, total length: 301
[MASTER] 02:45:26.149 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.27777777777778 - fitness:50.81258619831354 - branchDistance:0.0 - coverage:35.86881720135305 - ex: 1 - tex: 27
[MASTER] 02:45:26.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.81258619831354, number of tests: 16, total length: 321
[MASTER] 02:45:34.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.27777777777778 - fitness:50.31258619831354 - branchDistance:0.0 - coverage:35.36881720135305 - ex: 1 - tex: 29
[MASTER] 02:45:34.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.31258619831354, number of tests: 16, total length: 314
[MASTER] 02:45:44.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.27777777777778 - fitness:50.31043566067913 - branchDistance:0.0 - coverage:35.36666666371865 - ex: 1 - tex: 29
[MASTER] 02:45:44.449 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.31043566067913, number of tests: 16, total length: 337
[MASTER] 02:45:54.345 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 92 generations, 63468 statements, best individuals have fitness: 50.31043566067913
[MASTER] 02:45:54.361 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 80%
* Total number of goals: 111
* Number of covered goals: 89
* Generated 16 tests with total length 337
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     50 / 0           
	- MaxTime :                        303 / 300          Finished!
[MASTER] 02:45:56.952 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:45:57.385 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 262 
[MASTER] 02:45:58.848 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 02:45:58.855 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: []
[MASTER] 02:45:58.855 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: value:MockIllegalStateException at 18
[MASTER] 02:45:58.863 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.864 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.864 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.865 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.866 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 2.0
[MASTER] 02:45:58.866 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: [value:MockIllegalStateException]
[MASTER] 02:45:58.872 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: jsonValue:MockIllegalStateException at 6
[MASTER] 02:45:58.873 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: [jsonValue:MockIllegalStateException, MockIllegalStateException:beforeName-601,, value:MockIllegalStateException]
[MASTER] 02:45:58.873 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: value:MockIllegalStateException at 4
[MASTER] 02:45:58.874 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;
[MASTER] 02:45:58.880 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 4
[MASTER] 02:45:58.913 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.913 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.914 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.919 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.920 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.920 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.921 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.921 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.922 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.922 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 02:45:58.923 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [jsonValue:MockIllegalStateException, MockIllegalStateException:beforeName-601,, value:MockIllegalStateException]
[MASTER] 02:45:58.927 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 02:45:58.928 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 02:45:58.928 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 02:45:58.928 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 02:45:58.928 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 02:45:58.930 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 02:45:58.935 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 02:45:58.936 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 02:45:58.944 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 02:45:58.945 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 02:45:58.945 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 02:45:58.946 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 02:45:58.946 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 02:45:58.951 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 02:45:58.951 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 02:46:01.000 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:46:01.009 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 21%
* Total number of goals: 111
* Number of covered goals: 23
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing tests to file
* Writing JUnit test case 'JsonWriter_ESTest' to evosuite-tests
* Done!

* Computation finished
