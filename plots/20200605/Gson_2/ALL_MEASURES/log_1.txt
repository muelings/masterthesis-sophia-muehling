* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.TypeAdapters
* Starting Client-0
* Connecting to master process on port 21013
* Analyzing classpath: 
  - ../../../defects4j_compiled/Gson_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.TypeAdapters
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 00:40:23.997 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 00:40:24.109 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 268
* Using seed 1591310382149
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 00:40:55.798 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:647.0 - branchDistance:0.0 - coverage:323.0 - ex: 0 - tex: 2
[MASTER] 00:40:55.800 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 647.0, number of tests: 1, total length: 12
[MASTER] 00:41:14.686 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.499967559852073 - fitness:416.2865696542775 - branchDistance:0.0 - coverage:323.0 - ex: 0 - tex: 4
[MASTER] 00:41:14.690 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 416.2865696542775, number of tests: 3, total length: 84
[MASTER] 00:41:17.681 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.566666666666666 - fitness:373.18781725888323 - branchDistance:0.0 - coverage:323.0 - ex: 0 - tex: 12
[MASTER] 00:41:17.682 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 373.18781725888323, number of tests: 6, total length: 214
[MASTER] 00:41:34.940 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666342265187395 - fitness:372.02060354716315 - branchDistance:0.0 - coverage:294.0 - ex: 1 - tex: 7
[MASTER] 00:41:34.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 372.02060354716315, number of tests: 5, total length: 137
[MASTER] 00:41:59.099 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.06663422651874 - fitness:353.18683254444267 - branchDistance:0.0 - coverage:323.0 - ex: 0 - tex: 8
[MASTER] 00:41:59.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 353.18683254444267, number of tests: 7, total length: 191
* Computation finished
