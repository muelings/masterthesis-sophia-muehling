* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.TypeAdapters
* Starting Client-0
* Connecting to master process on port 17744
* Analyzing classpath: 
  - ../../../defects4j_compiled/Gson_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.TypeAdapters
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 00:51:56.206 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 00:51:56.320 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 268
* Using seed 1591311079957
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 00:52:36.237 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0664405246494795 - fitness:369.70900991429795 - branchDistance:0.0 - coverage:323.0 - ex: 0 - tex: 8
[MASTER] 00:52:36.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 369.70900991429795, number of tests: 6, total length: 100
[MASTER] 00:53:10.641 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.566666666666666 - fitness:366.68722466960355 - branchDistance:0.0 - coverage:323.0 - ex: 0 - tex: 6
[MASTER] 00:53:10.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 366.68722466960355, number of tests: 8, total length: 180
* Computation finished
