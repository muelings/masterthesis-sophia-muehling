* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 11886
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 128
[MASTER] 01:45:45.471 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:45:45.657 [logback-2] ERROR TestCluster - Failed to check cache for java.util.ArrayDeque<E> : Type points to itself
[MASTER] 01:45:45.657 [logback-2] ERROR TestCluster - Failed to check cache for java.util.ArrayList<E> : Type points to itself
[MASTER] 01:45:45.657 [logback-2] ERROR TestCluster - Failed to check cache for java.util.EnumSet<E> : Type points to itself
[MASTER] 01:45:45.657 [logback-2] ERROR TestCluster - Failed to check cache for java.util.HashSet<E> : Type points to itself
[MASTER] 01:45:45.657 [logback-2] ERROR TestCluster - Failed to check cache for java.util.LinkedHashSet<E> : Type points to itself
[MASTER] 01:45:45.658 [logback-2] ERROR TestCluster - Failed to check cache for java.util.LinkedList<E> : Type points to itself
[MASTER] 01:45:45.658 [logback-2] ERROR TestCluster - Failed to check cache for java.util.PriorityQueue<E> : Type points to itself
[MASTER] 01:45:45.658 [logback-2] ERROR TestCluster - Failed to check cache for java.util.ServiceLoader<S> : Type points to itself
[MASTER] 01:45:45.658 [logback-2] ERROR TestCluster - Failed to check cache for java.util.Stack<E> : Type points to itself
[MASTER] 01:45:45.658 [logback-2] ERROR TestCluster - Failed to check cache for java.util.TreeSet<E> : Type points to itself
[MASTER] 01:45:45.659 [logback-2] ERROR TestCluster - Failed to check cache for java.util.Vector<E> : Type points to itself
[MASTER] 01:46:05.211 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:46:24.089 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 01:46:28.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 01:46:28.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 01:46:28.976 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 01:46:28.976 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 01:46:30.224 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 01:46:30.232 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 01:46:30.232 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 01:46:30.234 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 01:46:30.234 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 01:46:30.410 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 01:46:30.420 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 01:46:30.420 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 01:46:30.422 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 01:46:30.422 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 01:46:30.422 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 490 | Tests with assertion: 1
* Generated 1 tests with total length 15
* GA-Budget:
	- MaxTime :                         44 / 300         
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 01:46:30.993 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:46:31.005 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 01:46:31.013 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[size, toString]}
[MASTER] 01:46:31.099 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
[MASTER] 01:46:31.103 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 2%
* Total number of goals: 128
* Number of covered goals: 3
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 20
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
