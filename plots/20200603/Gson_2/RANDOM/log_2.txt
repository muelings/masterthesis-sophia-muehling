* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.TypeAdapters
* Starting client
* Connecting to master process on port 2447
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.TypeAdapters
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 02:12:30.287 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 268
[MASTER] 02:12:31.267 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:12:32.211 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/google/gson/internal/bind/TypeAdapters_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:12:32.227 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 02:12:32.499 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/google/gson/internal/bind/TypeAdapters_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:12:32.514 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 4 | Tests with assertion: 1
[MASTER] 02:12:32.514 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 21
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          2 / 300         
	- RMIStoppingCondition
[MASTER] 02:12:33.273 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:12:33.298 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 20 
[MASTER] 02:12:33.318 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 02:12:33.319 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [newFactory:CodeUnderTestException]
[MASTER] 02:12:33.319 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: newFactory:CodeUnderTestException at 7
[MASTER] 02:12:33.319 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [newFactory:CodeUnderTestException]
[MASTER] 02:12:33.545 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:12:33.549 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 2%
* Total number of goals: 268
* Number of covered goals: 6
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeAdapters_ESTest' to evosuite-tests
* Done!

* Computation finished
