* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.geometry.euclidean.twod.SubLine
* Starting client
* Connecting to master process on port 12677
* Analyzing classpath: 
  - ../defects4j_compiled/Math_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.geometry.euclidean.twod.SubLine
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 39
[MASTER] 20:59:48.244 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 20:59:48.450 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 20:59:49.391 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/geometry/euclidean/twod/SubLine_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 20:59:49.425 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 20:59:49.646 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/geometry/euclidean/twod/SubLine_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 20:59:49.672 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 20:59:49.673 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 2 | Tests with assertion: 1
* Generated 1 tests with total length 24
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          1 / 300         
	- RMIStoppingCondition
[MASTER] 20:59:50.148 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 20:59:50.176 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 20 
[MASTER] 20:59:50.204 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 20:59:50.204 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [intersection:NullPointerException]
[MASTER] 20:59:50.204 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: intersection:NullPointerException at 11
[MASTER] 20:59:50.204 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [intersection:NullPointerException]
[MASTER] 20:59:50.515 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 20:59:50.519 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 8%
* Total number of goals: 39
* Number of covered goals: 3
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'SubLine_ESTest' to evosuite-tests
* Done!

* Computation finished
