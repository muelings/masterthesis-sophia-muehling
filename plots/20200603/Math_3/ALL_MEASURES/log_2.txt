* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.util.MathArrays
* Starting client
* Connecting to master process on port 16966
* Analyzing classpath: 
  - ../defects4j_compiled/Math_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.util.MathArrays
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 20:53:46.153 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 20:53:46.159 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 257
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1590260023057
* Starting evolution
[MASTER] 20:53:46.714 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1006.8333333333333 - branchDistance:0.0 - coverage:435.8333333333333 - ex: 0 - tex: 14
[MASTER] 20:53:46.714 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1006.8333333333333, number of tests: 8, total length: 248
[MASTER] 20:53:47.267 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1005.0 - branchDistance:0.0 - coverage:434.0 - ex: 0 - tex: 12
[MASTER] 20:53:47.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1005.0, number of tests: 10, total length: 277
[MASTER] 20:53:47.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:992.9166666656863 - branchDistance:0.0 - coverage:421.9166666656863 - ex: 0 - tex: 8
[MASTER] 20:53:47.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 992.9166666656863, number of tests: 8, total length: 287
[MASTER] 20:53:47.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:915.7666666666667 - branchDistance:0.0 - coverage:344.76666666666665 - ex: 0 - tex: 10
[MASTER] 20:53:47.406 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 915.7666666666667, number of tests: 7, total length: 229
[MASTER] 20:53:47.904 [logback-2] ERROR FunctionalMockStatement - Failed to use Mockito on interface org.apache.commons.math3.Field: Mismatch between callee's class class org.apache.commons.math3.Field$MockitoMock$151143663 and method's class interface org.apache.commons.math3.Field
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@3fab946f vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@27ecfa95
[MASTER] 20:53:47.908 [logback-2] ERROR TestCaseExecutor - ExecutionException (this is likely a serious error in the framework)
java.util.concurrent.ExecutionException: org.evosuite.testcase.execution.EvosuiteError: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class org.apache.commons.math3.Field$MockitoMock$151143663 and method's class interface org.apache.commons.math3.Field
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@3fab946f vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@27ecfa95
	at java.util.concurrent.FutureTask.report(FutureTask.java:122) [na:1.8.0_221]
	at java.util.concurrent.FutureTask.get(FutureTask.java:206) [na:1.8.0_221]
	at org.evosuite.testcase.execution.TimeoutHandler.executeWithTimeout(TimeoutHandler.java:107) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TimeoutHandler.execute(TimeoutHandler.java:96) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.execute(TestCaseExecutor.java:334) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.execute(TestCaseExecutor.java:277) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.execute(TestCaseExecutor.java:264) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.runTest(TestCaseExecutor.java:140) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.regression.RegressionSuiteFitness.executeChangedTestsAndUpdateResults(RegressionSuiteFitness.java:133) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.regression.RegressionSuiteFitness.getFitness(RegressionSuiteFitness.java:205) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.regression.RegressionSuiteFitness.getFitness(RegressionSuiteFitness.java:50) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.ga.metaheuristics.MonotonicGA.evolve(MonotonicGA.java:141) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.ga.metaheuristics.MonotonicGA.generateSolution(MonotonicGA.java:245) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.strategy.RegressionSuiteStrategy.generateTests(RegressionSuiteStrategy.java:131) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.TestSuiteGenerator.generateTests(TestSuiteGenerator.java:653) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.TestSuiteGenerator.generateTestSuite(TestSuiteGenerator.java:236) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.rmi.service.ClientNodeImpl$1.run(ClientNodeImpl.java:145) [evosuite-1.0.6.jar:1.0.6]
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [na:1.8.0_221]
	at java.util.concurrent.FutureTask.run(FutureTask.java:266) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [na:1.8.0_221]
	at java.lang.Thread.run(Thread.java:748) [na:1.8.0_221]
Caused by: org.evosuite.testcase.execution.EvosuiteError: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class org.apache.commons.math3.Field$MockitoMock$151143663 and method's class interface org.apache.commons.math3.Field
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@3fab946f vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@27ecfa95
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:744) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.AbstractStatement.exceptionHandler(AbstractStatement.java:169) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.FunctionalMockStatement.execute(FunctionalMockStatement.java:596) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.executeStatements(TestRunnable.java:307) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:213) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:55) ~[evosuite-1.0.6.jar:1.0.6]
	... 4 common frames omitted
Caused by: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class org.apache.commons.math3.Field$MockitoMock$151143663 and method's class interface org.apache.commons.math3.Field
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@3fab946f vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@27ecfa95
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:648) ~[evosuite-1.0.6.jar:1.0.6]
	... 9 common frames omitted
* Computation finished
[MASTER] 20:53:47.909 [logback-2] ERROR ClientNodeImpl - Error when generating tests for: org.apache.commons.math3.util.MathArrays with seed 1590260023057. Configuration id : regressionSuite
org.evosuite.testcase.execution.EvosuiteError: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class org.apache.commons.math3.Field$MockitoMock$151143663 and method's class interface org.apache.commons.math3.Field
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@3fab946f vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@27ecfa95
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:744) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.AbstractStatement.exceptionHandler(AbstractStatement.java:169) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.FunctionalMockStatement.execute(FunctionalMockStatement.java:596) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.executeStatements(TestRunnable.java:307) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:213) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:55) ~[evosuite-1.0.6.jar:1.0.6]
	at java.util.concurrent.FutureTask.run(FutureTask.java:266) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [na:1.8.0_221]
	at java.lang.Thread.run(Thread.java:748) [na:1.8.0_221]
Caused by: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class org.apache.commons.math3.Field$MockitoMock$151143663 and method's class interface org.apache.commons.math3.Field
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@3fab946f vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@27ecfa95
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:648) ~[evosuite-1.0.6.jar:1.0.6]
	... 9 common frames omitted
