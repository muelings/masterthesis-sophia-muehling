* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 11457
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 01:03:13.598 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 01:03:13.603 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 30
* Using seed 1590274991811
* Starting evolution
[MASTER] 01:03:14.435 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:115.0 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 0
[MASTER] 01:03:14.435 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.0, number of tests: 1, total length: 18
[MASTER] 01:03:14.863 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.76666666666667 - branchDistance:0.0 - coverage:28.766666666666666 - ex: 0 - tex: 12
[MASTER] 01:03:14.864 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.76666666666667, number of tests: 7, total length: 141
[MASTER] 01:03:23.400 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.65050505050505 - branchDistance:0.0 - coverage:20.65050505050505 - ex: 0 - tex: 14
[MASTER] 01:03:23.401 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.65050505050505, number of tests: 9, total length: 249
[MASTER] 01:03:26.980 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.6207780725022 - branchDistance:0.0 - coverage:20.62077807250221 - ex: 0 - tex: 6
[MASTER] 01:03:26.981 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.6207780725022, number of tests: 6, total length: 154
[MASTER] 01:03:26.990 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.65050505050505 - branchDistance:0.0 - coverage:19.65050505050505 - ex: 0 - tex: 12
[MASTER] 01:03:26.990 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.65050505050505, number of tests: 8, total length: 242
[MASTER] 01:03:29.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.6470238095238 - branchDistance:0.0 - coverage:19.64702380952381 - ex: 0 - tex: 10
[MASTER] 01:03:29.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.6470238095238, number of tests: 9, total length: 212
[MASTER] 01:03:34.110 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.54176065162908 - branchDistance:0.0 - coverage:19.541760651629072 - ex: 0 - tex: 10
[MASTER] 01:03:34.110 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.54176065162908, number of tests: 9, total length: 210
[MASTER] 01:03:35.179 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.4966588966589 - branchDistance:0.0 - coverage:19.496658896658897 - ex: 0 - tex: 12
[MASTER] 01:03:35.179 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.4966588966589, number of tests: 8, total length: 233
[MASTER] 01:03:37.080 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.44702380952381 - branchDistance:0.0 - coverage:19.44702380952381 - ex: 0 - tex: 10
[MASTER] 01:03:37.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.44702380952381, number of tests: 10, total length: 225
[MASTER] 01:03:38.470 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.31369047619047 - branchDistance:0.0 - coverage:19.313690476190477 - ex: 0 - tex: 12
[MASTER] 01:03:38.470 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.31369047619047, number of tests: 10, total length: 251
[MASTER] 01:03:46.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.2463768115942 - branchDistance:0.0 - coverage:19.246376811594203 - ex: 0 - tex: 12
[MASTER] 01:03:46.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.2463768115942, number of tests: 10, total length: 251
[MASTER] 01:04:04.056 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.17971014492754 - branchDistance:0.0 - coverage:19.179710144927537 - ex: 0 - tex: 14
[MASTER] 01:04:04.057 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.17971014492754, number of tests: 12, total length: 285
[MASTER] 01:04:05.635 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 6
[MASTER] 01:04:05.635 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 9, total length: 187
[MASTER] 01:08:14.621 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 268 generations, 206520 statements, best individuals have fitness: 81.91304347826087
[MASTER] 01:08:14.623 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 4 tests with total length 31
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     82 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 01:08:14.884 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:08:14.975 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 25 
[MASTER] 01:08:15.000 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 01:08:15.000 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 01:08:15.000 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 01:08:15.000 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 01:08:15.001 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 01:08:15.001 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
