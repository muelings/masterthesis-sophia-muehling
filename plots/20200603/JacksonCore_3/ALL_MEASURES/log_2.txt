* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Starting client
* Connecting to master process on port 2635
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.json.UTF8StreamJsonParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:47:15.441 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:47:15.450 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 1256
* Using seed 1590284831936
* Starting evolution
[MASTER] 03:47:16.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.999000499750125 - fitness:3143.5060481855444 - branchDistance:0.0 - coverage:2612.0 - ex: 0 - tex: 10
[MASTER] 03:47:16.729 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3143.5060481855444, number of tests: 7, total length: 175
[MASTER] 03:47:16.871 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9844961240310077 - fitness:3115.0497667185073 - branchDistance:0.0 - coverage:2582.0 - ex: 0 - tex: 18
[MASTER] 03:47:16.871 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3115.0497667185073, number of tests: 9, total length: 264
[MASTER] 03:47:27.500 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.981818181818182 - fitness:3024.84375 - branchDistance:0.0 - coverage:2644.0 - ex: 0 - tex: 14
[MASTER] 03:47:27.500 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3024.84375, number of tests: 9, total length: 285
[MASTER] 03:47:50.039 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.981818181818182 - fitness:2982.84375 - branchDistance:0.0 - coverage:2602.0 - ex: 0 - tex: 14
[MASTER] 03:47:50.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2982.84375, number of tests: 8, total length: 253
[MASTER] 03:47:50.105 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.981818181818182 - fitness:2970.1770833333335 - branchDistance:0.0 - coverage:2589.3333333333335 - ex: 0 - tex: 12
[MASTER] 03:47:50.105 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2970.1770833333335, number of tests: 9, total length: 253
[MASTER] 03:47:50.444 [logback-2] ERROR FunctionalMockStatement - Failed to use Mockito on class com.fasterxml.jackson.core.io.IOContext: Mismatch between callee's class class com.fasterxml.jackson.core.io.IOContext$MockitoMock$1176913454 and method's class class com.fasterxml.jackson.core.io.IOContext
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@5be3c81a vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@1ada1a33
[MASTER] 03:47:50.448 [logback-2] ERROR TestCaseExecutor - ExecutionException (this is likely a serious error in the framework)
java.util.concurrent.ExecutionException: org.evosuite.testcase.execution.EvosuiteError: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class com.fasterxml.jackson.core.io.IOContext$MockitoMock$1176913454 and method's class class com.fasterxml.jackson.core.io.IOContext
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@5be3c81a vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@1ada1a33
	at java.util.concurrent.FutureTask.report(FutureTask.java:122) [na:1.8.0_221]
	at java.util.concurrent.FutureTask.get(FutureTask.java:206) [na:1.8.0_221]
	at org.evosuite.testcase.execution.TimeoutHandler.executeWithTimeout(TimeoutHandler.java:107) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TimeoutHandler.execute(TimeoutHandler.java:96) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.execute(TestCaseExecutor.java:334) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.execute(TestCaseExecutor.java:277) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.execute(TestCaseExecutor.java:264) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestCaseExecutor.runTest(TestCaseExecutor.java:140) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.regression.RegressionSuiteFitness.executeChangedTestsAndUpdateResults(RegressionSuiteFitness.java:133) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.regression.RegressionSuiteFitness.getFitness(RegressionSuiteFitness.java:205) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.regression.RegressionSuiteFitness.getFitness(RegressionSuiteFitness.java:50) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.ga.metaheuristics.MonotonicGA.evolve(MonotonicGA.java:141) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.ga.metaheuristics.MonotonicGA.generateSolution(MonotonicGA.java:245) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.strategy.RegressionSuiteStrategy.generateTests(RegressionSuiteStrategy.java:131) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.TestSuiteGenerator.generateTests(TestSuiteGenerator.java:653) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.TestSuiteGenerator.generateTestSuite(TestSuiteGenerator.java:236) [evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.rmi.service.ClientNodeImpl$1.run(ClientNodeImpl.java:145) [evosuite-1.0.6.jar:1.0.6]
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [na:1.8.0_221]
	at java.util.concurrent.FutureTask.run(FutureTask.java:266) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [na:1.8.0_221]
	at java.lang.Thread.run(Thread.java:748) [na:1.8.0_221]
Caused by: org.evosuite.testcase.execution.EvosuiteError: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class com.fasterxml.jackson.core.io.IOContext$MockitoMock$1176913454 and method's class class com.fasterxml.jackson.core.io.IOContext
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@5be3c81a vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@1ada1a33
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:744) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.AbstractStatement.exceptionHandler(AbstractStatement.java:169) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.FunctionalMockStatement.execute(FunctionalMockStatement.java:596) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.executeStatements(TestRunnable.java:307) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:213) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:55) ~[evosuite-1.0.6.jar:1.0.6]
	... 4 common frames omitted
Caused by: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class com.fasterxml.jackson.core.io.IOContext$MockitoMock$1176913454 and method's class class com.fasterxml.jackson.core.io.IOContext
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@5be3c81a vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@1ada1a33
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:648) ~[evosuite-1.0.6.jar:1.0.6]
	... 9 common frames omitted
[MASTER] 03:47:50.449 [logback-2] ERROR ClientNodeImpl - Error when generating tests for: com.fasterxml.jackson.core.json.UTF8StreamJsonParser with seed 1590284831936. Configuration id : regressionSuite
org.evosuite.testcase.execution.EvosuiteError: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class com.fasterxml.jackson.core.io.IOContext$MockitoMock$1176913454 and method's class class com.fasterxml.jackson.core.io.IOContext
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@5be3c81a vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@1ada1a33
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:744) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.AbstractStatement.exceptionHandler(AbstractStatement.java:169) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.statements.FunctionalMockStatement.execute(FunctionalMockStatement.java:596) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.executeStatements(TestRunnable.java:307) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:213) ~[evosuite-1.0.6.jar:1.0.6]
	at org.evosuite.testcase.execution.TestRunnable.call(TestRunnable.java:55) ~[evosuite-1.0.6.jar:1.0.6]
	at java.util.concurrent.FutureTask.run(FutureTask.java:266) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [na:1.8.0_221]
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [na:1.8.0_221]
	at java.lang.Thread.run(Thread.java:748) [na:1.8.0_221]
Caused by: org.evosuite.testcase.execution.EvosuiteError: Mismatch between callee's class class com.fasterxml.jackson.core.io.IOContext$MockitoMock$1176913454 and method's class class com.fasterxml.jackson.core.io.IOContext
Target class classloader org.evosuite.instrumentation.InstrumentingClassLoader@5be3c81a vs method's classloader org.evosuite.instrumentation.InstrumentingClassLoader@1ada1a33
	at org.evosuite.testcase.statements.FunctionalMockStatement$1.execute(FunctionalMockStatement.java:648) ~[evosuite-1.0.6.jar:1.0.6]
	... 9 common frames omitted
* Computation finished
