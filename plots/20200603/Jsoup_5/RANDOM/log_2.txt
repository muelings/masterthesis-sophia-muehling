* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting client
* Connecting to master process on port 16399
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 100
[MASTER] 04:54:02.301 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:54:06.048 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 04:54:06.867 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:54:06.883 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 04:54:07.042 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 04:54:07.056 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
*=*=*=* Total tests: 757 | Tests with assertion: 1
[MASTER] 04:54:07.056 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 35
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          4 / 300         
[MASTER] 04:54:07.552 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:54:07.567 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 19 
[MASTER] 04:54:07.583 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 04:54:07.584 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragment:StringIndexOutOfBoundsException]
[MASTER] 04:54:07.584 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragment:StringIndexOutOfBoundsException at 16
[MASTER] 04:54:07.584 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parseBodyFragment:StringIndexOutOfBoundsException]
[MASTER] 04:54:07.779 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:54:07.782 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 38%
* Total number of goals: 100
* Number of covered goals: 38
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
