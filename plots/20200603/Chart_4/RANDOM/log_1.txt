* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.plot.XYPlot
* Starting client
* Connecting to master process on port 21204
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_4_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.plot.XYPlot
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 14:36:41.753 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1079
[MASTER] 14:36:49.179 [logback-2] ERROR TestCluster - Failed to check cache for java.util.LinkedList<E> : Type points to itself
[MASTER] 14:36:49.229 [logback-2] ERROR TestCluster - Failed to check cache for java.util.HashMap<K, V> : Type points to itself
[MASTER] 14:37:01.774 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:37:04.665 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:37:12.764 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:37:13.804 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:37:13.848 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 14:37:14.775 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:37:14.811 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:37:14.811 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 153 | Tests with assertion: 1
* Generated 1 tests with total length 5
* GA-Budget:
	- MaxTime :                         33 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 14:37:21.155 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:37:21.195 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 14:37:21.219 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 14:37:21.219 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException]
[MASTER] 14:37:21.219 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NullPointerException at 2
[MASTER] 14:37:21.219 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException]
[MASTER] 14:37:21.409 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
[MASTER] 14:37:21.417 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 5%
* Total number of goals: 1079
* Number of covered goals: 59
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYPlot_ESTest' to ..plots/Chart_4/RANDOM/1
* Done!

* Computation finished
[MASTER] 14:37:31.293 [main] WARN  CSVStatisticsBackend - Error while writing statistics: C:\Users\Sophia M�hling\Documents\Studium\TestGenAug\Masterarbeit\masterthesis-sophia-muehling\evosuite\..\plots\statistics.csv (Der Prozess kann nicht auf die Datei zugreifen, da sie von einem anderen Prozess verwendet wird)
