* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.util.ISO8601Utils
* Starting client
* Connecting to master process on port 18294
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.util.ISO8601Utils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:07:18.781 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:07:18.788 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
* Using seed 1590282435699
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 03:07:19.386 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:308.33333333333337 - branchDistance:0.0 - coverage:103.33333333333334 - ex: 0 - tex: 8
[MASTER] 03:07:19.386 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 308.33333333333337, number of tests: 5, total length: 103
[MASTER] 03:07:19.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:306.33333333333337 - branchDistance:0.0 - coverage:101.33333333333334 - ex: 0 - tex: 20
[MASTER] 03:07:19.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 306.33333333333337, number of tests: 10, total length: 216
[MASTER] 03:07:19.878 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:296.17857142857144 - branchDistance:0.0 - coverage:91.17857142857144 - ex: 0 - tex: 4
[MASTER] 03:07:19.878 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 296.17857142857144, number of tests: 2, total length: 47
[MASTER] 03:07:20.433 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:293.8452380952381 - branchDistance:0.0 - coverage:88.8452380952381 - ex: 0 - tex: 6
[MASTER] 03:07:20.433 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 293.8452380952381, number of tests: 5, total length: 77
[MASTER] 03:07:20.655 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:292.42857142857144 - branchDistance:0.0 - coverage:87.42857142857144 - ex: 0 - tex: 8
[MASTER] 03:07:20.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 292.42857142857144, number of tests: 5, total length: 115
[MASTER] 03:07:20.905 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:291.0952380952381 - branchDistance:0.0 - coverage:86.0952380952381 - ex: 0 - tex: 8
[MASTER] 03:07:20.905 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 291.0952380952381, number of tests: 6, total length: 83
[MASTER] 03:07:21.452 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:290.8452380952381 - branchDistance:0.0 - coverage:85.8452380952381 - ex: 0 - tex: 8
[MASTER] 03:07:21.452 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 290.8452380952381, number of tests: 5, total length: 78
[MASTER] 03:07:21.562 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:282.8330105566948 - branchDistance:0.0 - coverage:77.83301055669477 - ex: 0 - tex: 6
[MASTER] 03:07:21.562 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 282.8330105566948, number of tests: 4, total length: 95
[MASTER] 03:07:21.775 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:277.6060985797828 - branchDistance:0.0 - coverage:72.60609857978278 - ex: 0 - tex: 6
[MASTER] 03:07:21.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 277.6060985797828, number of tests: 6, total length: 84
[MASTER] 03:07:22.173 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:276.49967722336146 - branchDistance:0.0 - coverage:71.49967722336143 - ex: 0 - tex: 10
[MASTER] 03:07:22.173 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 276.49967722336146, number of tests: 5, total length: 113
[MASTER] 03:07:22.531 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:273.7973684210526 - branchDistance:0.0 - coverage:68.79736842105262 - ex: 0 - tex: 8
[MASTER] 03:07:22.531 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 273.7973684210526, number of tests: 6, total length: 112
[MASTER] 03:07:23.386 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:271.6060985797828 - branchDistance:0.0 - coverage:66.60609857978278 - ex: 0 - tex: 6
[MASTER] 03:07:23.386 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 271.6060985797828, number of tests: 6, total length: 93
[MASTER] 03:07:23.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.7973684210526 - branchDistance:0.0 - coverage:65.79736842105262 - ex: 0 - tex: 6
[MASTER] 03:07:23.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.7973684210526, number of tests: 6, total length: 101
[MASTER] 03:07:24.203 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.0473684210526 - branchDistance:0.0 - coverage:64.04736842105262 - ex: 0 - tex: 10
[MASTER] 03:07:24.203 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.0473684210526, number of tests: 8, total length: 144
[MASTER] 03:07:25.528 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.0473684210526 - branchDistance:0.0 - coverage:63.047368421052624 - ex: 0 - tex: 14
[MASTER] 03:07:25.529 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.0473684210526, number of tests: 9, total length: 183
[MASTER] 03:07:26.360 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:266.0473684210526 - branchDistance:0.0 - coverage:61.047368421052624 - ex: 0 - tex: 12
[MASTER] 03:07:26.360 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 266.0473684210526, number of tests: 8, total length: 188
[MASTER] 03:07:27.603 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.0473684210526 - branchDistance:0.0 - coverage:60.047368421052624 - ex: 0 - tex: 14
[MASTER] 03:07:27.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.0473684210526, number of tests: 8, total length: 189
[MASTER] 03:07:31.758 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:257.0473684210526 - branchDistance:0.0 - coverage:52.047368421052624 - ex: 0 - tex: 14
[MASTER] 03:07:31.758 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 257.0473684210526, number of tests: 8, total length: 154
[MASTER] 03:07:32.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:255.71403508493424 - branchDistance:0.0 - coverage:50.714035084934245 - ex: 0 - tex: 16
[MASTER] 03:07:32.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 255.71403508493424, number of tests: 9, total length: 183
[MASTER] 03:07:33.782 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:255.27276524366442 - branchDistance:0.0 - coverage:50.27276524366441 - ex: 0 - tex: 14
[MASTER] 03:07:33.782 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 255.27276524366442, number of tests: 8, total length: 152
[MASTER] 03:07:33.836 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:254.71403508493424 - branchDistance:0.0 - coverage:49.714035084934245 - ex: 0 - tex: 14
[MASTER] 03:07:33.836 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 254.71403508493424, number of tests: 8, total length: 156
[MASTER] 03:07:35.925 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:252.38070175160092 - branchDistance:0.0 - coverage:47.38070175160092 - ex: 0 - tex: 16
[MASTER] 03:07:35.925 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 252.38070175160092, number of tests: 9, total length: 162
[MASTER] 03:07:37.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.38070175253225 - branchDistance:0.0 - coverage:40.38070175253224 - ex: 0 - tex: 14
[MASTER] 03:07:37.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.38070175253225, number of tests: 9, total length: 162
[MASTER] 03:07:38.828 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:244.38070175160092 - branchDistance:0.0 - coverage:39.38070175160092 - ex: 0 - tex: 14
[MASTER] 03:07:38.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 244.38070175160092, number of tests: 10, total length: 186
[MASTER] 03:08:09.443 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:241.38070175253225 - branchDistance:0.0 - coverage:36.38070175253224 - ex: 0 - tex: 12
[MASTER] 03:08:09.444 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 241.38070175253225, number of tests: 9, total length: 103
[MASTER] 03:08:09.548 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:240.38070175253225 - branchDistance:0.0 - coverage:35.38070175253224 - ex: 0 - tex: 14
[MASTER] 03:08:09.548 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 240.38070175253225, number of tests: 9, total length: 106
[MASTER] 03:08:12.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:240.38070175146788 - branchDistance:0.0 - coverage:35.38070175146787 - ex: 0 - tex: 18
[MASTER] 03:08:12.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 240.38070175146788, number of tests: 11, total length: 134
[MASTER] 03:08:37.272 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:238.88070175146788 - branchDistance:0.0 - coverage:33.88070175146787 - ex: 0 - tex: 18
[MASTER] 03:08:37.272 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 238.88070175146788, number of tests: 10, total length: 108
[MASTER] 03:10:54.002 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.88070175146788 - branchDistance:0.0 - coverage:32.88070175146787 - ex: 0 - tex: 18
[MASTER] 03:10:54.002 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.88070175146788, number of tests: 11, total length: 106
[MASTER] 03:11:06.627 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:234.88070175146788 - branchDistance:0.0 - coverage:29.880701751467875 - ex: 0 - tex: 16
[MASTER] 03:11:06.627 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 234.88070175146788, number of tests: 10, total length: 101
[MASTER] 03:11:08.420 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:234.7897926605588 - branchDistance:0.0 - coverage:29.78979266055878 - ex: 0 - tex: 18
[MASTER] 03:11:08.420 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 234.7897926605588, number of tests: 12, total length: 144
[MASTER] 03:11:08.499 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:234.76959064035677 - branchDistance:0.0 - coverage:29.76959064035676 - ex: 0 - tex: 16
[MASTER] 03:11:08.499 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 234.76959064035677, number of tests: 11, total length: 107
[MASTER] 03:11:08.967 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.88070175253225 - branchDistance:0.0 - coverage:28.88070175253224 - ex: 0 - tex: 16
[MASTER] 03:11:08.967 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.88070175253225, number of tests: 11, total length: 105
[MASTER] 03:11:09.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.88070175146788 - branchDistance:0.0 - coverage:28.880701751467875 - ex: 0 - tex: 16
[MASTER] 03:11:09.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.88070175146788, number of tests: 11, total length: 107
[MASTER] 03:11:09.534 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.7897926605588 - branchDistance:0.0 - coverage:28.78979266055878 - ex: 0 - tex: 18
[MASTER] 03:11:09.534 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.7897926605588, number of tests: 12, total length: 127
[MASTER] 03:11:10.221 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.76959064035677 - branchDistance:0.0 - coverage:28.76959064035676 - ex: 0 - tex: 18
[MASTER] 03:11:10.221 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.76959064035677, number of tests: 12, total length: 129
[MASTER] 03:11:21.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.7897926605588 - branchDistance:0.0 - coverage:27.78979266055878 - ex: 0 - tex: 18
[MASTER] 03:11:21.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.7897926605588, number of tests: 11, total length: 106
[MASTER] 03:11:21.854 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.77543859357314 - branchDistance:0.0 - coverage:27.775438593573135 - ex: 0 - tex: 18
[MASTER] 03:11:21.854 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.77543859357314, number of tests: 11, total length: 106
[MASTER] 03:11:21.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.76959064035677 - branchDistance:0.0 - coverage:27.76959064035676 - ex: 0 - tex: 18
[MASTER] 03:11:21.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.76959064035677, number of tests: 11, total length: 106

* Search finished after 301s and 2410 generations, 1257328 statements, best individuals have fitness: 232.76959064035677
[MASTER] 03:12:19.802 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:12:19.805 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 80%
* Total number of goals: 97
* Number of covered goals: 78
* Generated 11 tests with total length 104
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                    233 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 03:12:20.223 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:12:20.247 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 79 
[MASTER] 03:12:20.325 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.326 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.326 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.326 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.327 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.327 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.327 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.327 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:12:20.328 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 03:12:20.329 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 03:12:20.329 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 03:12:20.330 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'ISO8601Utils_ESTest' to evosuite-tests
* Done!

* Computation finished
