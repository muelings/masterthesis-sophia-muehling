* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.general.DatasetUtilities
* Starting client
* Connecting to master process on port 13434
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_2_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.general.DatasetUtilities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 14:11:38.079 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 564
[MASTER] 14:12:02.439 [logback-2] ERROR TestCluster - Failed to check cache for java.util.List<E> : Type points to itself
[MASTER] 14:12:36.715 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 14:12:38.953 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/general/DatasetUtilities_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:12:39.001 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 14:12:39.717 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/general/DatasetUtilities_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:12:39.765 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 5157 | Tests with assertion: 1
* Generated 1 tests with total length 16
* GA-Budget:
[MASTER] 14:12:39.765 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         61 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 14:12:42.049 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:12:42.107 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 14:12:42.147 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 14:12:42.147 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:calculatePieDatasetTotal-182,, createConsolidatedPieDataset:MockIllegalArgumentException, iterateDomainBounds:TooManyResourcesException]
[MASTER] 14:12:42.147 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createConsolidatedPieDataset:MockIllegalArgumentException at 10
[MASTER] 14:12:42.147 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:calculatePieDatasetTotal-182,, createConsolidatedPieDataset:MockIllegalArgumentException, iterateDomainBounds:TooManyResourcesException]
[MASTER] 14:12:42.147 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: iterateDomainBounds:TooManyResourcesException at 9
[MASTER] 14:12:42.147 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [MockIllegalArgumentException:calculatePieDatasetTotal-182,, createConsolidatedPieDataset:MockIllegalArgumentException, iterateDomainBounds:TooManyResourcesException]
[MASTER] 14:12:42.513 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:12:42.517 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 564
* Number of covered goals: 20
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'DatasetUtilities_ESTest' to ..plots/Chart_2/RANDOM/2
* Done!

* Computation finished
[MASTER] 14:12:48.260 [main] WARN  CSVStatisticsBackend - Error while writing statistics: C:\Users\Sophia M�hling\Documents\Studium\TestGenAug\Masterarbeit\masterthesis-sophia-muehling\evosuite\..\plots\statistics.csv (Der Prozess kann nicht auf die Datei zugreifen, da sie von einem anderen Prozess verwendet wird)
