* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.SoundexUtils
* Starting client
* Connecting to master process on port 12924
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.SoundexUtils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 22:03:27.123 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 22:03:27.129 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 20
* Using seed 1590264193983
* Starting evolution
[MASTER] 22:03:31.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:46.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 22:03:31.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.0, number of tests: 6, total length: 182
[MASTER] 22:03:32.564 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:45.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:03:32.565 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.0, number of tests: 10, total length: 253
[MASTER] 22:04:26.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9997470275739944 - fitness:15.667903525046382 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:04:26.884 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.667903525046382, number of tests: 3, total length: 28
[MASTER] 22:04:29.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9994940551479887 - fitness:9.800890553053687 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:04:29.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.800890553053687, number of tests: 3, total length: 31
[MASTER] 22:04:31.455 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.99949609473419 - fitness:9.800886962656858 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:31.455 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.800886962656858, number of tests: 3, total length: 19
[MASTER] 22:04:31.827 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.999494055147989 - fitness:8.333951762523192 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:31.827 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333951762523192, number of tests: 3, total length: 22
[MASTER] 22:04:32.303 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9992410827219835 - fitness:7.2863958363452355 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:04:32.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.2863958363452355, number of tests: 3, total length: 24
[MASTER] 22:04:32.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9992410827219835 - fitness:6.500521805129503 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:32.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.500521805129503, number of tests: 3, total length: 23
[MASTER] 22:04:34.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.999941933610762 - fitness:5.888920431328525 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:34.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888920431328525, number of tests: 3, total length: 25
[MASTER] 22:04:34.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.998988110295977 - fitness:5.400445276526844 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:34.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.400445276526844, number of tests: 3, total length: 28
[MASTER] 22:04:34.824 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999241082721984 - fitness:5.400333948946289 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:34.824 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.400333948946289, number of tests: 3, total length: 28
[MASTER] 22:04:35.223 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.998988110295977 - fitness:5.000367993744106 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:35.223 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.000367993744106, number of tests: 3, total length: 27
[MASTER] 22:04:35.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.998988110295977 - fitness:4.666975881261596 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:35.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.666975881261596, number of tests: 3, total length: 37
[MASTER] 22:04:37.074 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.998735137869971 - fitness:4.384944729877005 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:37.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.384944729877005, number of tests: 3, total length: 29
[MASTER] 22:04:37.683 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.998482165443967 - fitness:3.933630184351229 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:37.683 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.933630184351229, number of tests: 3, total length: 35
[MASTER] 22:04:38.422 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.998482165443967 - fitness:3.588466403750279 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:38.423 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.588466403750279, number of tests: 4, total length: 46
[MASTER] 22:04:39.497 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.998482165443967 - fitness:3.4446505875077302 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:39.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4446505875077302, number of tests: 4, total length: 43
[MASTER] 22:04:41.488 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.99872431360196 - fitness:3.4446176980858807 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:41.488 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4446176980858807, number of tests: 3, total length: 37
[MASTER] 22:04:42.673 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.99999333144391 - fitness:3.4444453500511627 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:42.673 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4444453500511627, number of tests: 4, total length: 40
[MASTER] 22:04:42.701 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.998482165443967 - fitness:3.315974487689911 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:42.701 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.315974487689911, number of tests: 4, total length: 42
[MASTER] 22:04:43.392 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.998482165443967 - fitness:3.0953895454600215 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:43.392 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0953895454600215, number of tests: 4, total length: 48
[MASTER] 22:04:44.132 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.998482165443967 - fitness:3.0001379944802204 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:44.132 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0001379944802204, number of tests: 4, total length: 49
[MASTER] 22:04:46.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.998482165443967 - fitness:2.9131697337014506 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:46.268 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.9131697337014506, number of tests: 4, total length: 53
[MASTER] 22:04:46.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.998733707862407 - fitness:2.91314880892586 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:04:46.770 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.91314880892586, number of tests: 4, total length: 57
[MASTER] 22:04:47.608 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.998482165443967 - fitness:2.833449286361816 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:04:47.608 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.833449286361816, number of tests: 5, total length: 84
[MASTER] 22:04:48.345 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.998482165443967 - fitness:2.7601068620407005 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:04:48.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7601068620407005, number of tests: 5, total length: 106
[MASTER] 22:04:48.909 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.998229193017963 - fitness:2.692422959784375 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:04:48.909 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.692422959784375, number of tests: 5, total length: 91
[MASTER] 22:04:49.310 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.998482165443967 - fitness:2.6924064920406336 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:49.310 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6924064920406336, number of tests: 5, total length: 83
[MASTER] 22:04:50.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.999983846588744 - fitness:2.692308743713812 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:04:50.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.692308743713812, number of tests: 5, total length: 94
[MASTER] 22:04:50.240 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.99998618438322 - fitness:2.692308591549499 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:50.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.692308591549499, number of tests: 4, total length: 53
[MASTER] 22:04:50.767 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.998229193017963 - fitness:2.6297365166223154 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:50.767 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6297365166223154, number of tests: 4, total length: 56
[MASTER] 22:04:51.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.99797622059196 - fitness:2.571542159095016 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:51.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.571542159095016, number of tests: 4, total length: 59
[MASTER] 22:04:51.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.99990835882104 - fitness:2.5714337145727955 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:51.931 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5714337145727955, number of tests: 4, total length: 87
[MASTER] 22:04:52.644 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.999991610606294 - fitness:2.5714290422620327 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:52.644 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5714290422620327, number of tests: 4, total length: 58
[MASTER] 22:04:55.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.999992016358483 - fitness:2.571429019490213 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:04:55.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.571429019490213, number of tests: 3, total length: 53
[MASTER] 22:05:01.054 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.999992397525773 - fitness:2.5714289980981593 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:01.054 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5714289980981593, number of tests: 3, total length: 51
[MASTER] 22:05:03.181 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.999993751871727 - fitness:2.57142892208891 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:03.181 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.57142892208891, number of tests: 3, total length: 80
[MASTER] 22:05:08.547 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.999993829755233 - fitness:2.5714289177178946 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:08.547 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5714289177178946, number of tests: 3, total length: 62
[MASTER] 22:05:10.684 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.997077091706245 - fitness:2.5173943173943174 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:10.684 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5173943173943174, number of tests: 3, total length: 55
[MASTER] 22:05:12.220 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.99743957758653 - fitness:2.5173753490294244 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:12.220 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5173753490294244, number of tests: 3, total length: 52
[MASTER] 22:05:13.014 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.998538278823318 - fitness:2.517317858470534 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:13.014 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.517317858470534, number of tests: 3, total length: 52
[MASTER] 22:05:13.562 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.998720217592776 - fitness:2.517308338776493 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:13.562 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.517308338776493, number of tests: 3, total length: 50
[MASTER] 22:05:15.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.99998724882171 - fitness:2.517242046435305 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:15.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.517242046435305, number of tests: 2, total length: 48
[MASTER] 22:05:21.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.99999599185746 - fitness:2.5172415890110535 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:21.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5172415890110535, number of tests: 3, total length: 78
[MASTER] 22:05:28.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.998538278823318 - fitness:2.466738132072943 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:28.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.466738132072943, number of tests: 2, total length: 51
[MASTER] 22:05:29.424 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.998719423226447 - fitness:2.4667292753147687 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:29.424 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.4667292753147687, number of tests: 2, total length: 51
[MASTER] 22:05:29.563 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.998720179956997 - fitness:2.466729238315895 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:29.563 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.466729238315895, number of tests: 3, total length: 73
[MASTER] 22:05:30.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.99856347638714 - fitness:2.419420613910596 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:30.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.419420613910596, number of tests: 3, total length: 74
[MASTER] 22:05:31.371 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.999753510446894 - fitness:2.4193661244813454 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:31.371 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.4193661244813454, number of tests: 3, total length: 73
[MASTER] 22:05:32.087 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.99856347638714 - fitness:2.375061728395062 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:32.087 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.375061728395062, number of tests: 4, total length: 79
[MASTER] 22:05:33.026 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.999978302436908 - fitness:2.3750009323177963 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:33.026 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.3750009323177963, number of tests: 4, total length: 79
[MASTER] 22:05:33.785 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.999985930973843 - fitness:2.3750006045287337 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:33.785 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.3750006045287337, number of tests: 4, total length: 80
[MASTER] 22:05:34.046 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.99999374335721 - fitness:2.3750002688401723 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:34.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.3750002688401723, number of tests: 4, total length: 79
[MASTER] 22:05:34.683 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.998538278823318 - fitness:2.333392395390945 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:34.683 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.333392395390945, number of tests: 4, total length: 81
[MASTER] 22:05:36.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.999158453768572 - fitness:2.3333673360683873 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:36.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.3333673360683873, number of tests: 4, total length: 80
[MASTER] 22:05:36.280 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.9999936406237 - fitness:2.3333335902778796 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:36.280 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.3333335902778796, number of tests: 5, total length: 81
[MASTER] 22:05:36.695 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.99853827882332 - fitness:2.2941732858970085 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 22:05:36.696 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2941732858970085, number of tests: 5, total length: 121
[MASTER] 22:05:38.521 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.999261187520595 - fitness:2.294145768560117 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:38.521 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.294145768560117, number of tests: 5, total length: 89
[MASTER] 22:05:42.066 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.999264964399316 - fitness:2.294145624797256 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 22:05:42.066 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.294145624797256, number of tests: 5, total length: 72
[MASTER] 22:05:43.181 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.99977636122641 - fitness:2.294126159317269 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:43.181 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.294126159317269, number of tests: 4, total length: 58
[MASTER] 22:05:46.262 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.99999425946266 - fitness:2.2941178655568213 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:46.262 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2941178655568213, number of tests: 3, total length: 55
[MASTER] 22:05:49.055 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.983399833148496 - fitness:2.257739390964163 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:49.055 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.257739390964163, number of tests: 3, total length: 54
[MASTER] 22:05:50.301 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.98648134886904 - fitness:2.257628612641903 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:50.301 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.257628612641903, number of tests: 3, total length: 55
[MASTER] 22:05:50.394 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.99096905598591 - fitness:2.2574673176270013 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:50.394 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2574673176270013, number of tests: 3, total length: 54
[MASTER] 22:05:51.351 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.99857979762116 - fitness:2.257193870563589 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:51.351 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.257193870563589, number of tests: 3, total length: 56
[MASTER] 22:05:52.003 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.99926721108468 - fitness:2.2571691782753867 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:52.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2571691782753867, number of tests: 3, total length: 53
[MASTER] 22:05:53.082 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.99857979762116 - fitness:2.2222704408718807 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:53.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2222704408718807, number of tests: 3, total length: 55
[MASTER] 22:05:55.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.999996609069505 - fitness:2.2222223373464165 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:05:55.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2222223373464165, number of tests: 3, total length: 55
[MASTER] 22:06:18.755 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.999997949761465 - fitness:2.22222229182909 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:06:18.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.22222229182909, number of tests: 3, total length: 52
[MASTER] 22:06:25.157 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.999998067334545 - fitness:2.222222287837411 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 22:06:25.157 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.222222287837411, number of tests: 3, total length: 52
[MASTER] 22:08:28.168 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:08:28.171 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1339 generations, 1120868 statements, best individuals have fitness: 2.222222287837411
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 20
* Number of covered goals: 20
* Generated 3 tests with total length 52
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                      2 / 0           
[MASTER] 22:08:28.766 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:08:28.833 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 38 
[MASTER] 22:08:29.016 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 22:08:29.016 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 22:08:29.016 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 22:08:29.016 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 22:08:29.018 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 20
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'SoundexUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
