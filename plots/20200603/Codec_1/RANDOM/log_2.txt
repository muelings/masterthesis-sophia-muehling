* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.SoundexUtils
* Starting client
* Connecting to master process on port 18591
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.SoundexUtils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 20
[MASTER] 21:58:04.718 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:03:05.724 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 36730 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 22:03:05.810 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:03:05.811 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 191 seconds more than allowed.
[MASTER] 22:03:05.820 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 22:03:05.821 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
[MASTER] 22:03:05.853 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of goals: 20
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'SoundexUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
