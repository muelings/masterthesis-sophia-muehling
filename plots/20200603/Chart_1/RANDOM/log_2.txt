* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.renderer.category.AbstractCategoryItemRenderer
* Starting client
* Connecting to master process on port 21508
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_1_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.renderer.category.AbstractCategoryItemRenderer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 13:51:07.540 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 274
[MASTER] 13:51:12.638 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:51:15.767 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/renderer/category/AbstractCategoryItemRenderer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 13:51:18.506 [logback-1] WARN  TestUsageChecker - class org.jfree.chart.renderer.category.MinMaxCategoryRenderer$1 looks like an anonymous class, ignoring it (although reflection says false) MinMaxCategoryRenderer$1
[MASTER] 13:51:33.474 [logback-1] WARN  TestUsageChecker - class org.jfree.chart.renderer.category.MinMaxCategoryRenderer$2 looks like an anonymous class, ignoring it (although reflection says false) MinMaxCategoryRenderer$2
[MASTER] 13:51:57.540 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 13:52:07.255 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:52:08.559 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/renderer/category/AbstractCategoryItemRenderer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 13:52:55.160 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 13:54:35.881 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 13:54:37.258 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:54:37.879 [logback-2] ERROR JUnitAnalyzer - Ran out of time while checking tests
Generated test with 1 assertions.
[MASTER] 13:54:37.939 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:54:38.363 [logback-2] ERROR JUnitAnalyzer - Ran out of time while checking tests
[MASTER] 13:54:38.435 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:54:38.435 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 5561 | Tests with assertion: 1
* Generated 1 tests with total length 30
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        211 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 13:54:42.794 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:54:42.794 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 119 seconds more than allowed.
[MASTER] 13:54:42.856 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 22 
[MASTER] 13:54:42.868 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 13:54:42.868 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [getLegendItems:NullPointerException]
[MASTER] 13:54:42.868 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getLegendItems:NullPointerException at 18
[MASTER] 13:54:42.872 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [getLegendItems:NullPointerException]
[MASTER] 13:54:43.187 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 13:54:43.191 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 2%
* Total number of goals: 274
* Number of covered goals: 5
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'AbstractCategoryItemRenderer_ESTest' to ..plots/Chart_1/RANDOM/2
* Done!

* Computation finished
[MASTER] 13:54:53.671 [main] WARN  CSVStatisticsBackend - Error while writing statistics: C:\Users\Sophia M�hling\Documents\Studium\TestGenAug\Masterarbeit\masterthesis-sophia-muehling\evosuite\..\plots\statistics.csv (Der Prozess kann nicht auf die Datei zugreifen, da sie von einem anderen Prozess verwendet wird)
