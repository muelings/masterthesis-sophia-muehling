* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.changes.ChangeSetPerformer
* Starting client
* Connecting to master process on port 4963
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.changes.ChangeSetPerformer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 52
[MASTER] 00:25:18.470 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:25:34.666 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 00:25:34.801 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:25:35.863 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/changes/ChangeSetPerformer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:25:35.881 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:25:36.085 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/changes/ChangeSetPerformer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:25:36.100 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 00:25:36.100 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 5 | Tests with assertion: 1
* Generated 1 tests with total length 19
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                         17 / 300         
[MASTER] 00:25:36.522 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:25:36.534 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 00:25:36.546 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 00:25:36.546 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException:perform-128,, perform:NullPointerException]
[MASTER] 00:25:36.546 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: perform:NullPointerException at 12
[MASTER] 00:25:36.546 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException:perform-128,, perform:NullPointerException]
[MASTER] 00:25:36.755 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 10 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 00:25:36.758 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 8%
* Total number of goals: 52
* Number of covered goals: 4
* Generated 1 tests with total length 10
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'ChangeSetPerformer_ESTest' to evosuite-tests
* Done!

* Computation finished
