* EvoSuite 1.0.7-SNAPSHOT
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.general.DatasetUtilities
* Starting Client-0
* Connecting to master process on port 7657
* Analyzing classpath: 
  - ../../../defects4j_compiled/Chart_2_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.general.DatasetUtilities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 19:09:55.548 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 564
[MASTER] 19:10:52.977 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
* Computation finished
mv: das Verschieben von 'evosuite-tests' nach '../../../plots/Chart_2/RANDOM/300/1' ist nicht möglich: Datei oder Verzeichnis nicht gefunden
