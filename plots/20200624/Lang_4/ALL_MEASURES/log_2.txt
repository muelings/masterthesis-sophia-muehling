* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.text.translate.LookupTranslator
* Starting client
* Connecting to master process on port 17733
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.text.translate.LookupTranslator
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:33:13.380 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:33:13.388 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 14
* Using seed 1592375591203
* Starting evolution
[MASTER] 08:33:14.520 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:50.799999999068675 - branchDistance:0.0 - coverage:17.799999999068678 - ex: 0 - tex: 10
[MASTER] 08:33:14.520 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.799999999068675, number of tests: 6, total length: 280
[MASTER] 08:33:14.547 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:49.799999999068675 - branchDistance:0.0 - coverage:16.799999999068678 - ex: 0 - tex: 4
[MASTER] 08:33:14.547 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.799999999068675, number of tests: 4, total length: 77
[MASTER] 08:33:14.712 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:46.99999999906868 - branchDistance:0.0 - coverage:13.999999999068677 - ex: 0 - tex: 10
[MASTER] 08:33:14.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.99999999906868, number of tests: 6, total length: 289
[MASTER] 08:33:14.751 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:39.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 16
[MASTER] 08:33:14.751 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.0, number of tests: 8, total length: 340
[MASTER] 08:33:15.340 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:34.14285714285714 - branchDistance:0.0 - coverage:24.0 - ex: 0 - tex: 14
[MASTER] 08:33:15.340 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.14285714285714, number of tests: 7, total length: 409
[MASTER] 08:33:15.378 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:27.060606060606062 - branchDistance:0.0 - coverage:17.333333333333332 - ex: 0 - tex: 10
[MASTER] 08:33:15.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.060606060606062, number of tests: 5, total length: 359
[MASTER] 08:33:15.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.75 - fitness:18.53333333333333 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 18
[MASTER] 08:33:15.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.53333333333333, number of tests: 10, total length: 497
[MASTER] 08:33:15.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.75 - fitness:13.736842104331835 - branchDistance:0.0 - coverage:5.999999999068677 - ex: 0 - tex: 16
[MASTER] 08:33:15.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.736842104331835, number of tests: 9, total length: 407
[MASTER] 08:33:15.607 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:12.142857142857142 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 6
[MASTER] 08:33:15.607 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.142857142857142, number of tests: 4, total length: 166
[MASTER] 08:33:16.174 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:10.727272727272728 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 08:33:16.174 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.727272727272728, number of tests: 7, total length: 227
[MASTER] 08:33:16.397 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.75 - fitness:9.736842105263158 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 16
[MASTER] 08:33:16.397 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.736842105263158, number of tests: 8, total length: 416
[MASTER] 08:33:17.163 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.75 - fitness:8.736842105263158 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 08:33:17.163 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.736842105263158, number of tests: 6, total length: 233
[MASTER] 08:33:18.207 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.75 - fitness:7.7368421052631575 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 08:33:18.207 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.7368421052631575, number of tests: 8, total length: 388
[MASTER] 08:33:25.570 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.25 - fitness:6.12 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 08:33:25.570 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.12, number of tests: 8, total length: 530
[MASTER] 08:33:28.962 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.321428571428571 - fitness:6.062146892655368 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 08:33:28.962 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.062146892655368, number of tests: 6, total length: 308
[MASTER] 08:33:33.881 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.262605042016807 - fitness:5.372870582252734 - branchDistance:0.0 - coverage:0.5 - ex: 0 - tex: 6
[MASTER] 08:33:33.881 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.372870582252734, number of tests: 6, total length: 241
[MASTER] 08:33:40.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.262605042016807 - fitness:5.118116683725691 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 08:33:40.021 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.118116683725691, number of tests: 4, total length: 194
[MASTER] 08:33:41.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.262605042016807 - fitness:4.618116683725691 - branchDistance:0.0 - coverage:0.5 - ex: 0 - tex: 4
[MASTER] 08:33:41.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.618116683725691, number of tests: 4, total length: 188
[MASTER] 08:33:48.327 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.262605042016807 - fitness:4.118116683725691 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 08:33:48.327 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.118116683725691, number of tests: 4, total length: 168
[MASTER] 08:33:50.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.262605042016807 - fitness:3.8412609587763478 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:33:50.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8412609587763478, number of tests: 3, total length: 96
[MASTER] 08:33:52.954 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.080065359477125 - fitness:3.446470955652717 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:33:52.954 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.446470955652717, number of tests: 3, total length: 76
[MASTER] 08:34:00.821 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.55065359477124 - fitness:3.361509706981792 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:00.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.361509706981792, number of tests: 3, total length: 59
[MASTER] 08:34:01.880 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.582649806334016 - fitness:3.355946774470869 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:01.880 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.355946774470869, number of tests: 3, total length: 58
[MASTER] 08:34:04.611 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.594927536231884 - fitness:3.3538190927988913 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 08:34:04.612 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.3538190927988913, number of tests: 3, total length: 56
[MASTER] 08:34:08.991 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.594927536231884 - fitness:3.192542574847326 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 08:34:08.992 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.192542574847326, number of tests: 3, total length: 63
[MASTER] 08:34:09.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.70516524198748 - fitness:3.176106114648123 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:09.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.176106114648123, number of tests: 3, total length: 55
[MASTER] 08:34:11.637 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.70516524198748 - fitness:3.0375462153335753 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:11.637 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0375462153335753, number of tests: 3, total length: 67
[MASTER] 08:34:14.510 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.70516524198748 - fitness:2.9155751850673006 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:14.510 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.9155751850673006, number of tests: 3, total length: 53
[MASTER] 08:34:16.519 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.854766656095133 - fitness:2.69718356019301 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:16.519 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.69718356019301, number of tests: 3, total length: 53
[MASTER] 08:34:18.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.859675960551563 - fitness:2.69674177154124 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 08:34:18.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.69674177154124, number of tests: 3, total length: 53
[MASTER] 08:34:28.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.321214421899473 - fitness:2.3157237728715696 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 08:34:28.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.3157237728715696, number of tests: 3, total length: 54
[MASTER] 08:34:31.330 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.321214421899473 - fitness:2.2637624509954097 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:31.330 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2637624509954097, number of tests: 4, total length: 141
[MASTER] 08:34:33.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.323012467173342 - fitness:2.263672718302459 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:34:33.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.263672718302459, number of tests: 4, total length: 107
[MASTER] 08:35:00.036 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.323012467230242 - fitness:2.2636727182996195 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 08:35:00.036 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.2636727182996195, number of tests: 3, total length: 59
[MASTER] 08:35:11.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.323012467230242 - fitness:2.1711739340008385 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 08:35:11.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.1711739340008385, number of tests: 3, total length: 54
[MASTER] 08:35:15.842 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.324578716669414 - fitness:2.17110680211433 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 08:35:15.842 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.17110680211433, number of tests: 3, total length: 89
[MASTER] 08:35:21.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.324578716686144 - fitness:2.171106802113613 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 08:35:21.149 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.171106802113613, number of tests: 3, total length: 54
[MASTER] 08:35:26.604 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.324578716686144 - fitness:1.8155417971555117 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 08:35:26.605 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.8155417971555117, number of tests: 3, total length: 53
[MASTER] 08:35:32.426 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.324578716686144 - fitness:1.7635945639212343 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 08:35:32.426 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7635945639212343, number of tests: 3, total length: 61
[MASTER] 08:35:52.587 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.330781701878934 - fitness:1.7632851357140065 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 08:35:52.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7632851357140065, number of tests: 3, total length: 54
[MASTER] 08:35:54.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.330781702035686 - fitness:1.763285135706189 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 08:35:54.021 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.763285135706189, number of tests: 4, total length: 114
[MASTER] 08:35:57.510 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.330930345662594 - fitness:1.763277722662853 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 08:35:57.510 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.763277722662853, number of tests: 3, total length: 55
[MASTER] 08:36:31.325 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.3309303456816 - fitness:1.7632777226619052 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 08:36:31.325 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7632777226619052, number of tests: 3, total length: 64
[MASTER] 08:38:14.410 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 2397 generations, 2142240 statements, best individuals have fitness: 1.7632777226619052
[MASTER] 08:38:14.413 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 14
* Number of covered goals: 14
* Generated 3 tests with total length 54
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                      2 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 08:38:14.618 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:38:14.633 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 50 
[MASTER] 08:38:14.679 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 08:38:14.679 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:38:14.679 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:38:14.679 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 08:38:14.680 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:38:14.681 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 14
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'LookupTranslator_ESTest' to evosuite-tests
* Done!

* Computation finished
