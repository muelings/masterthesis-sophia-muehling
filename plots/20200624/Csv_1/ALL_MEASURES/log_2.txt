* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 14595
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:18:36.887 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:18:36.891 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 30
* Using seed 1592360315282
* Starting evolution
[MASTER] 04:18:38.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:111.0 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 8
[MASTER] 04:18:38.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.0, number of tests: 7, total length: 128
[MASTER] 04:18:38.453 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:99.36666666666667 - branchDistance:0.0 - coverage:36.36666666666667 - ex: 0 - tex: 14
[MASTER] 04:18:38.453 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.36666666666667, number of tests: 9, total length: 205
[MASTER] 04:18:39.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.43333333333334 - branchDistance:0.0 - coverage:34.43333333333334 - ex: 0 - tex: 12
[MASTER] 04:18:39.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.43333333333334, number of tests: 8, total length: 165
[MASTER] 04:18:40.892 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:85.58809523809524 - branchDistance:0.0 - coverage:22.58809523809524 - ex: 0 - tex: 12
[MASTER] 04:18:40.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.58809523809524, number of tests: 10, total length: 199
[MASTER] 04:18:47.036 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:84.65378908320085 - branchDistance:0.0 - coverage:21.65378908320085 - ex: 0 - tex: 14
[MASTER] 04:18:47.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.65378908320085, number of tests: 10, total length: 221
[MASTER] 04:18:47.887 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.82707339692634 - branchDistance:0.0 - coverage:19.827073396926338 - ex: 0 - tex: 12
[MASTER] 04:18:47.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.82707339692634, number of tests: 10, total length: 231
[MASTER] 04:18:48.017 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.58809523809524 - branchDistance:0.0 - coverage:19.58809523809524 - ex: 0 - tex: 12
[MASTER] 04:18:48.017 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.58809523809524, number of tests: 11, total length: 226
[MASTER] 04:18:52.077 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.18809523809524 - branchDistance:0.0 - coverage:19.18809523809524 - ex: 0 - tex: 12
[MASTER] 04:18:52.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.18809523809524, number of tests: 11, total length: 247
[MASTER] 04:18:54.791 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.92142857142858 - branchDistance:0.0 - coverage:18.92142857142857 - ex: 0 - tex: 12
[MASTER] 04:18:54.791 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.92142857142858, number of tests: 11, total length: 227
[MASTER] 04:19:07.759 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 10
[MASTER] 04:19:07.759 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 12, total length: 209
[MASTER] 04:23:37.950 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 224 generations, 184996 statements, best individuals have fitness: 81.91304347826087
[MASTER] 04:23:37.953 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 3 tests with total length 24
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     82 / 0           
[MASTER] 04:23:38.194 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:23:38.281 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 21 
[MASTER] 04:23:38.301 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 04:23:38.301 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 04:23:38.301 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 04:23:38.301 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 04:23:38.302 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
