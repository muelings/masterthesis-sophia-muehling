* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 11253
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 97
[MASTER] 03:13:13.699 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:13:13.842 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124, var0.size());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 03:13:13.843 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var0.toString());  // (Inspector) Original Value: 0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: 
[MASTER] 03:13:13.848 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 03:13:13.848 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 03:13:14.732 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:13:14.750 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124, var0.size());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 03:13:14.750 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var0.toString());  // (Inspector) Original Value: 0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: 
[MASTER] 03:13:14.751 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 03:13:14.751 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 03:13:14.972 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:13:14.992 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124, var0.size());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 03:13:14.992 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var0.toString());  // (Inspector) Original Value: 0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: 
[MASTER] 03:13:14.993 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Keeping 2 assertions.
[MASTER] 03:13:14.993 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 03:13:14.994 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 3 | Tests with assertion: 1
* Generated 1 tests with total length 4
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          1 / 300         
[MASTER] 03:13:15.556 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:13:15.572 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 03:13:15.586 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[size, toString]}
[MASTER] 03:13:15.587 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.InspectorAssertion@4bf7b4c9, org.evosuite.assertion.InspectorAssertion@930535fa] invalid assertion(s) to be removed
[MASTER] 03:13:15.588 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:13:15.588 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 03:13:15.590 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
