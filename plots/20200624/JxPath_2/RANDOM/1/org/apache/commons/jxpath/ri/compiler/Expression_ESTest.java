/*
 * This file was automatically generated by EvoSuite
 * Wed Jun 17 05:24:51 GMT 2020
 */

package org.apache.commons.jxpath.ri.compiler;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.jxpath.ri.compiler.CoreFunction;
import org.apache.commons.jxpath.ri.compiler.Expression;
import org.apache.commons.jxpath.ri.compiler.LocationPath;
import org.apache.commons.jxpath.ri.compiler.ProcessingInstructionTest;
import org.apache.commons.jxpath.ri.compiler.Step;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Expression_ESTest extends Expression_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Expression[] expressionArray0 = new Expression[1];
      Step[] stepArray0 = new Step[9];
      ProcessingInstructionTest processingInstructionTest0 = new ProcessingInstructionTest("org.apache.commons.jxpath.ri.compiler.Expression$PointerIterator");
      Step step0 = new Step(1, processingInstructionTest0, expressionArray0);
      stepArray0[0] = step0;
      Step step1 = new Step(758, processingInstructionTest0, expressionArray0);
      stepArray0[1] = step1;
      Step step2 = new Step(1, processingInstructionTest0, expressionArray0);
      stepArray0[2] = step2;
      Step step3 = new Step(0, processingInstructionTest0, expressionArray0);
      stepArray0[3] = step3;
      Step step4 = new Step((-1808), processingInstructionTest0, expressionArray0);
      stepArray0[4] = step4;
      Step step5 = new Step((-96), processingInstructionTest0, expressionArray0);
      stepArray0[5] = step5;
      Step step6 = new Step((-1809), processingInstructionTest0, expressionArray0);
      stepArray0[6] = step6;
      Step step7 = new Step(0, processingInstructionTest0, expressionArray0);
      stepArray0[7] = step7;
      Step step8 = new Step(1584, processingInstructionTest0, expressionArray0);
      stepArray0[8] = step8;
      LocationPath locationPath0 = new LocationPath(true, stepArray0);
      expressionArray0[0] = (Expression) locationPath0;
      CoreFunction coreFunction0 = new CoreFunction(1, expressionArray0);
      // Undeclared exception!
      try { 
        coreFunction0.computeContextDependent();
        fail("Expecting exception: StackOverflowError");
      
      } catch(StackOverflowError e) {
         //
         // no message in exception (getMessage() returned null)
         //
      }
  }
}
