* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.compiler.Expression
* Starting client
* Connecting to master process on port 19836
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.compiler.Expression
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:24:55.389 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:24:55.393 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 23
* Using seed 1592371492929
* Starting evolution
[MASTER] 07:24:56.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:79.0 - branchDistance:0.0 - coverage:38.0 - ex: 0 - tex: 6
[MASTER] 07:24:56.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.0, number of tests: 6, total length: 105
[MASTER] 07:24:57.035 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:75.0 - branchDistance:0.0 - coverage:34.0 - ex: 0 - tex: 8
[MASTER] 07:24:57.035 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.0, number of tests: 6, total length: 149
[MASTER] 07:24:57.135 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:67.0 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 10
[MASTER] 07:24:57.135 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.0, number of tests: 9, total length: 271
[MASTER] 07:25:00.238 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.0 - branchDistance:0.0 - coverage:25.0 - ex: 0 - tex: 16
[MASTER] 07:25:00.238 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0, number of tests: 9, total length: 209
[MASTER] 07:25:00.659 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:23.0 - ex: 0 - tex: 10
[MASTER] 07:25:00.659 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 8, total length: 192
[MASTER] 07:25:01.483 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:59.5 - branchDistance:0.0 - coverage:18.5 - ex: 0 - tex: 12
[MASTER] 07:25:01.484 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.5, number of tests: 7, total length: 179
[MASTER] 07:25:02.068 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:57.0 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 12
[MASTER] 07:25:02.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.0, number of tests: 7, total length: 162
[MASTER] 07:25:03.683 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:55.0 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 14
[MASTER] 07:25:03.683 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.0, number of tests: 7, total length: 175
[MASTER] 07:25:05.624 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:54.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 10
[MASTER] 07:25:05.624 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.0, number of tests: 8, total length: 189
[MASTER] 07:25:06.263 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:53.0 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 8
[MASTER] 07:25:06.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.0, number of tests: 7, total length: 138
[MASTER] 07:25:07.841 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:52.0 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 10
[MASTER] 07:25:07.841 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.0, number of tests: 8, total length: 173
[MASTER] 07:25:09.334 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:51.0 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 14
[MASTER] 07:25:09.334 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.0, number of tests: 8, total length: 199
* Computation finished
