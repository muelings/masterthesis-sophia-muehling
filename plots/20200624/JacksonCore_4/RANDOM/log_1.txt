* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 11807
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 06:13:04.745 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 141
[MASTER] 06:14:07.471 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:07.471 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:07.471 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:07.492 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:14:07.492 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 06:14:08.423 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:14:08.437 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:08.438 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:08.438 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:08.439 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:14:08.439 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
Generated test with 3 assertions.
[MASTER] 06:14:08.686 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:14:08.702 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:08.702 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:08.702 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(409601, var3.size());  // (Inspector) Original Value: 409601 | Regression Value: 262145
[MASTER] 06:14:08.703 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:14:08.704 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 06:14:08.704 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 3 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 20960 | Tests with assertion: 1
* Generated 1 tests with total length 31
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         63 / 300         
[MASTER] 06:14:09.254 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:14:09.268 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 24 
[MASTER] 06:14:09.280 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[size]}
[MASTER] 06:14:09.290 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:14:09.290 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 06:14:09.293 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 141
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
