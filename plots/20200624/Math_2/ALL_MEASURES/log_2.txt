* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 17114
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 01:48:38.554 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 01:48:38.558 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 38
* Using seed 1592351315294
* Starting evolution
[MASTER] 01:48:39.452 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:144.99685520563182 - branchDistance:0.0 - coverage:55.996855205631825 - ex: 0 - tex: 4
[MASTER] 01:48:39.452 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.99685520563182, number of tests: 3, total length: 56
[MASTER] 01:48:39.683 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:117.49556541019956 - branchDistance:0.0 - coverage:28.495565410199557 - ex: 0 - tex: 16
[MASTER] 01:48:39.683 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.49556541019956, number of tests: 8, total length: 204
[MASTER] 01:48:40.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:113.0 - branchDistance:0.0 - coverage:24.0 - ex: 0 - tex: 8
[MASTER] 01:48:40.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.0, number of tests: 5, total length: 152
[MASTER] 01:48:40.163 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:109.94444444444444 - branchDistance:0.0 - coverage:20.944444444444443 - ex: 0 - tex: 10
[MASTER] 01:48:40.163 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.94444444444444, number of tests: 8, total length: 144
[MASTER] 01:48:40.941 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:104.0 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 10
[MASTER] 01:48:40.941 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.0, number of tests: 9, total length: 209
[MASTER] 01:48:43.651 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:102.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 12
[MASTER] 01:48:43.651 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.0, number of tests: 9, total length: 186
[MASTER] 01:48:43.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:101.49497487437186 - branchDistance:0.0 - coverage:12.49497487437186 - ex: 0 - tex: 14
[MASTER] 01:48:43.895 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.49497487437186, number of tests: 8, total length: 207
[MASTER] 01:48:43.986 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.98290598290598 - branchDistance:0.0 - coverage:8.982905982905983 - ex: 0 - tex: 10
[MASTER] 01:48:43.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.98290598290598, number of tests: 7, total length: 198
[MASTER] 01:48:43.996 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.33333333333333 - branchDistance:0.0 - coverage:8.333333333333332 - ex: 0 - tex: 12
[MASTER] 01:48:43.996 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.33333333333333, number of tests: 8, total length: 165
[MASTER] 01:48:45.194 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:96.99556541019956 - branchDistance:0.0 - coverage:7.995565410199557 - ex: 0 - tex: 8
[MASTER] 01:48:45.194 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.99556541019956, number of tests: 8, total length: 142
[MASTER] 01:48:45.379 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.98290598290598 - branchDistance:0.0 - coverage:6.982905982905983 - ex: 0 - tex: 10
[MASTER] 01:48:45.379 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.98290598290598, number of tests: 8, total length: 235
[MASTER] 01:48:45.673 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.33333333333333 - branchDistance:0.0 - coverage:6.333333333333333 - ex: 0 - tex: 14
[MASTER] 01:48:45.673 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.33333333333333, number of tests: 9, total length: 227
[MASTER] 01:48:45.695 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 16
[MASTER] 01:48:45.695 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.0, number of tests: 10, total length: 216
[MASTER] 01:48:46.633 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:94.98290598290598 - branchDistance:0.0 - coverage:5.982905982905983 - ex: 0 - tex: 10
[MASTER] 01:48:46.633 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.98290598290598, number of tests: 9, total length: 236
[MASTER] 01:48:47.616 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:94.9375 - branchDistance:0.0 - coverage:5.9375 - ex: 0 - tex: 10
[MASTER] 01:48:47.616 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.9375, number of tests: 10, total length: 238
[MASTER] 01:48:47.657 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:94.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 14
[MASTER] 01:48:47.657 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.0, number of tests: 9, total length: 182
[MASTER] 01:48:48.745 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 12
[MASTER] 01:48:48.745 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.0, number of tests: 9, total length: 177
[MASTER] 01:48:49.402 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:92.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 16
[MASTER] 01:48:49.402 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.0, number of tests: 11, total length: 220
[MASTER] 01:48:50.524 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 18
[MASTER] 01:48:50.524 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0, number of tests: 11, total length: 222
[MASTER] 01:48:51.570 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:90.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 18
[MASTER] 01:48:51.570 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.0, number of tests: 12, total length: 242
[MASTER] 01:48:54.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 18
[MASTER] 01:48:54.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.0, number of tests: 13, total length: 243
[MASTER] 01:48:56.111 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:48.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 18
[MASTER] 01:48:56.111 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.0, number of tests: 12, total length: 239
[MASTER] 01:48:56.750 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:45.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 18
[MASTER] 01:48:56.750 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.0, number of tests: 12, total length: 251
[MASTER] 01:49:00.853 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:33.33333333333333 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 14
[MASTER] 01:49:00.854 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.33333333333333, number of tests: 12, total length: 226
[MASTER] 01:49:04.017 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:30.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 18
[MASTER] 01:49:04.017 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.333333333333332, number of tests: 12, total length: 240
[MASTER] 01:49:07.821 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:23.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 18
[MASTER] 01:49:07.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.0, number of tests: 12, total length: 241
[MASTER] 01:49:22.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:18.6 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 01:49:22.513 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.6, number of tests: 12, total length: 202
[MASTER] 01:49:47.987 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:15.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 16
[MASTER] 01:49:47.987 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.666666666666666, number of tests: 11, total length: 204
[MASTER] 01:49:59.127 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:14.571428571428571 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 12
[MASTER] 01:49:59.127 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.571428571428571, number of tests: 10, total length: 179
[MASTER] 01:49:59.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:13.571428571428571 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 01:49:59.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.571428571428571, number of tests: 10, total length: 177
[MASTER] 01:50:06.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:12.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 01:50:06.134 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.0, number of tests: 10, total length: 174
[MASTER] 01:50:21.433 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:10.777777777777777 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:50:21.433 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.777777777777777, number of tests: 10, total length: 154
[MASTER] 01:50:33.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.0 - fitness:9.8 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:50:33.050 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.8, number of tests: 10, total length: 145
[MASTER] 01:50:57.677 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.0 - fitness:9.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:50:57.677 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.0, number of tests: 10, total length: 118
[MASTER] 01:51:06.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.0 - fitness:8.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:51:06.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333333333333332, number of tests: 10, total length: 113
[MASTER] 01:51:18.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.0 - fitness:7.76923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:51:18.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.76923076923077, number of tests: 9, total length: 101
[MASTER] 01:51:21.470 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.0 - fitness:7.285714285714286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:51:21.470 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.285714285714286, number of tests: 9, total length: 102
[MASTER] 01:51:23.856 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.0 - fitness:6.866666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:51:23.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.866666666666666, number of tests: 9, total length: 105
[MASTER] 01:51:45.100 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.0 - fitness:6.5 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:51:45.100 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5, number of tests: 9, total length: 85
[MASTER] 01:51:55.633 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.0 - fitness:5.888888888888888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:51:55.633 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888888888888888, number of tests: 9, total length: 84
[MASTER] 01:52:12.979 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.0 - fitness:5.631578947368421 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:52:12.979 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.631578947368421, number of tests: 9, total length: 82
[MASTER] 01:52:24.472 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.0 - fitness:5.4 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:52:24.472 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.4, number of tests: 9, total length: 81
[MASTER] 01:52:33.152 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.0 - fitness:5.19047619047619 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:52:33.152 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.19047619047619, number of tests: 9, total length: 82
[MASTER] 01:52:39.558 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.0 - fitness:5.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:52:39.559 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.0, number of tests: 9, total length: 80
[MASTER] 01:52:41.539 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.0 - fitness:4.826086956521739 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:52:41.539 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.826086956521739, number of tests: 10, total length: 116
[MASTER] 01:53:35.534 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.0 - fitness:4.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:53:35.534 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.666666666666666, number of tests: 9, total length: 74
[MASTER] 01:53:39.613 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 645 generations, 356182 statements, best individuals have fitness: 4.666666666666666
[MASTER] 01:53:39.616 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 38
* Number of covered goals: 38
* Generated 9 tests with total length 74
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                      5 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 01:53:40.539 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:53:40.609 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 54 
[MASTER] 01:53:40.729 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 01:53:40.730 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 01:53:40.732 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 01:53:40.733 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 01:53:40.733 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 01:53:40.735 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 01:53:40.736 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 01:53:40.736 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 01:53:40.736 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 01:53:40.737 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 01:53:40.738 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 01:53:40.738 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 01:53:40.738 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 01:53:40.739 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 01:53:40.739 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 01:53:40.739 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
