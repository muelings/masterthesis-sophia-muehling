* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 13885
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 01:43:22.167 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 01:43:22.171 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 38
* Using seed 1592350998889
* Starting evolution
[MASTER] 01:43:23.184 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:124.99415204678363 - branchDistance:0.0 - coverage:35.994152046783626 - ex: 0 - tex: 2
[MASTER] 01:43:23.184 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.99415204678363, number of tests: 3, total length: 60
[MASTER] 01:43:23.490 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:115.5 - branchDistance:0.0 - coverage:26.5 - ex: 0 - tex: 8
[MASTER] 01:43:23.490 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.5, number of tests: 8, total length: 189
[MASTER] 01:43:23.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:112.0 - branchDistance:0.0 - coverage:23.0 - ex: 0 - tex: 16
[MASTER] 01:43:23.799 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.0, number of tests: 8, total length: 186
[MASTER] 01:43:24.997 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:102.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 12
[MASTER] 01:43:24.997 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.0, number of tests: 9, total length: 178
[MASTER] 01:43:26.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 10
[MASTER] 01:43:26.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.0, number of tests: 8, total length: 106
[MASTER] 01:43:27.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:51.5 - branchDistance:0.0 - coverage:6.5 - ex: 0 - tex: 14
[MASTER] 01:43:27.258 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.5, number of tests: 10, total length: 147
[MASTER] 01:43:28.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:49.5 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 12
[MASTER] 01:43:28.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.5, number of tests: 10, total length: 154
[MASTER] 01:43:28.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:48.5 - branchDistance:0.0 - coverage:3.5 - ex: 0 - tex: 14
[MASTER] 01:43:28.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.5, number of tests: 11, total length: 163
[MASTER] 01:43:28.769 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:48.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 12
[MASTER] 01:43:28.769 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.0, number of tests: 11, total length: 151
[MASTER] 01:43:29.379 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:47.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 16
[MASTER] 01:43:29.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.0, number of tests: 12, total length: 180
[MASTER] 01:43:31.293 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:46.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 18
[MASTER] 01:43:31.293 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.0, number of tests: 13, total length: 188
[MASTER] 01:43:32.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:32.33333333333333 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 10
[MASTER] 01:43:32.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.33333333333333, number of tests: 11, total length: 168
[MASTER] 01:43:33.261 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:31.333333333333332 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 01:43:33.261 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.333333333333332, number of tests: 11, total length: 150
[MASTER] 01:43:34.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:30.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:43:34.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.333333333333332, number of tests: 11, total length: 154
[MASTER] 01:43:39.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:24.5 - branchDistance:0.0 - coverage:1.5 - ex: 0 - tex: 10
[MASTER] 01:43:39.707 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.5, number of tests: 11, total length: 125
[MASTER] 01:43:40.712 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:23.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:43:40.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.0, number of tests: 11, total length: 131
[MASTER] 01:43:45.934 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:18.6 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:43:45.934 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.6, number of tests: 11, total length: 121
[MASTER] 01:43:50.374 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:17.0 - branchDistance:0.0 - coverage:1.3333333333333333 - ex: 0 - tex: 12
[MASTER] 01:43:50.374 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.0, number of tests: 10, total length: 122
[MASTER] 01:43:50.914 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:15.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 01:43:50.914 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.666666666666666, number of tests: 11, total length: 143
[MASTER] 01:43:52.693 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:13.571428571428571 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 01:43:52.693 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.571428571428571, number of tests: 10, total length: 114
[MASTER] 01:43:55.711 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:12.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:43:55.711 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.0, number of tests: 11, total length: 112
[MASTER] 01:43:57.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:10.777777777777777 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:43:57.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.777777777777777, number of tests: 11, total length: 117
[MASTER] 01:43:59.553 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.0 - fitness:9.8 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:43:59.553 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.8, number of tests: 11, total length: 116
[MASTER] 01:44:04.063 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.0 - fitness:9.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:44:04.063 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.0, number of tests: 11, total length: 123
[MASTER] 01:44:05.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.0 - fitness:8.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:05.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333333333333332, number of tests: 10, total length: 106
[MASTER] 01:44:08.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.0 - fitness:7.76923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:44:08.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.76923076923077, number of tests: 10, total length: 109
[MASTER] 01:44:14.611 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.0 - fitness:7.285714285714286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:44:14.611 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.285714285714286, number of tests: 10, total length: 117
[MASTER] 01:44:25.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.0 - fitness:6.866666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:44:25.549 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.866666666666666, number of tests: 9, total length: 117
[MASTER] 01:44:28.277 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.0 - fitness:6.5 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:28.277 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5, number of tests: 8, total length: 90
[MASTER] 01:44:34.642 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.0 - fitness:6.176470588235294 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:34.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.176470588235294, number of tests: 8, total length: 92
[MASTER] 01:44:47.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.0 - fitness:5.888888888888888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:47.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888888888888888, number of tests: 8, total length: 82
[MASTER] 01:44:49.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.0 - fitness:5.631578947368421 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:49.509 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.631578947368421, number of tests: 8, total length: 81
[MASTER] 01:44:53.187 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.0 - fitness:5.4 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:44:53.187 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.4, number of tests: 9, total length: 120
[MASTER] 01:45:02.954 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.0 - fitness:5.19047619047619 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:45:02.954 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.19047619047619, number of tests: 8, total length: 84
[MASTER] 01:45:12.398 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.0 - fitness:5.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:45:12.398 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.0, number of tests: 7, total length: 82
[MASTER] 01:45:22.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.0 - fitness:4.826086956521739 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:45:22.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.826086956521739, number of tests: 7, total length: 69
[MASTER] 01:45:27.887 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.0 - fitness:4.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:45:27.888 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.666666666666666, number of tests: 7, total length: 67
[MASTER] 01:45:55.652 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.0 - fitness:4.52 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:45:55.652 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.52, number of tests: 7, total length: 66
[MASTER] 01:46:08.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.0 - fitness:4.384615384615385 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:46:08.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.384615384615385, number of tests: 8, total length: 72
[MASTER] 01:46:16.544 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.0 - fitness:4.2592592592592595 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:46:16.544 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.2592592592592595, number of tests: 7, total length: 65
[MASTER] 01:46:19.256 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.0 - fitness:4.142857142857142 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:46:19.256 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.142857142857142, number of tests: 7, total length: 69
[MASTER] 01:47:06.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.0 - fitness:4.0344827586206895 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:47:06.576 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0344827586206895, number of tests: 7, total length: 64
[MASTER] 01:47:16.704 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.0 - fitness:3.933333333333333 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:47:16.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.933333333333333, number of tests: 7, total length: 64
[MASTER] 01:48:23.182 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1545 generations, 736796 statements, best individuals have fitness: 3.933333333333333
[MASTER] 01:48:23.184 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 38
* Number of covered goals: 38
* Generated 7 tests with total length 64
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                      4 / 0           
[MASTER] 01:48:24.030 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:48:24.050 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 50 
[MASTER] 01:48:24.110 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 01:48:24.111 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 01:48:24.112 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 01:48:24.113 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 01:48:24.114 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 01:48:24.115 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 01:48:24.117 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 01:48:24.117 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 01:48:24.117 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 01:48:24.118 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 01:48:24.118 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 01:48:24.119 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 01:48:24.119 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 01:48:24.120 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
