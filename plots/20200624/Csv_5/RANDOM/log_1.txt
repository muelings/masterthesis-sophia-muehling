* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 15945
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 128
[MASTER] 04:54:58.916 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:55:05.261 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 04:55:05.261 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var0.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 04:55:05.270 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 04:55:05.270 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:55:06.220 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:55:06.232 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 04:55:06.232 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var0.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 04:55:06.235 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 04:55:06.235 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 04:55:06.488 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:55:06.499 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 04:55:06.499 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var0.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 04:55:06.500 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 04:55:06.500 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
[MASTER] 04:55:06.501 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 167 | Tests with assertion: 1
* Generated 1 tests with total length 30
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          7 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:55:07.313 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:55:07.333 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 19 
[MASTER] 04:55:07.341 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[toString, size]}
[MASTER] 04:55:07.495 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
[MASTER] 04:55:07.498 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 2%
* Total number of goals: 128
* Number of covered goals: 3
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
