/*
 * This file was automatically generated by EvoSuite
 * Wed Jun 17 03:05:33 GMT 2020
 */

package org.apache.commons.csv;

import org.junit.Test;
import static org.junit.Assert.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayDeque;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class CSVPrinter_ESTest extends CSVPrinter_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      CSVFormat cSVFormat0 = CSVFormat.newFormat('`');
      ArrayDeque<Short> arrayDeque0 = new ArrayDeque<Short>();
      Charset charset0 = Charset.defaultCharset();
      ByteBuffer byteBuffer0 = ByteBuffer.allocate(87);
      CharBuffer charBuffer0 = charset0.decode(byteBuffer0);
      CharBuffer charBuffer1 = CharBuffer.wrap((CharSequence) charBuffer0);
      CSVPrinter cSVPrinter0 = new CSVPrinter(charBuffer1, cSVFormat0);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.nio.ReadOnlyBufferException : null
      cSVPrinter0.printRecord((Iterable<?>) arrayDeque0);
  }
}
