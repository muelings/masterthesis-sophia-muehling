* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.bind.TypeAdapters
* Starting client
* Connecting to master process on port 7093
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.bind.TypeAdapters
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 05:25:59.322 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 268
[MASTER] 05:26:00.207 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:26:01.147 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/google/gson/internal/bind/TypeAdapters_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:26:01.160 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 05:26:01.423 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/google/gson/internal/bind/TypeAdapters_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:26:01.431 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 05:26:01.431 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 4 | Tests with assertion: 1
* Generated 1 tests with total length 15
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 05:26:02.183 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:26:02.194 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 05:26:02.201 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:26:02.201 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [newFactory:CodeUnderTestException]
[MASTER] 05:26:02.201 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: newFactory:CodeUnderTestException at 4
[MASTER] 05:26:02.201 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [newFactory:CodeUnderTestException]
[MASTER] 05:26:02.298 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:26:02.301 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 268
* Number of covered goals: 8
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeAdapters_ESTest' to evosuite-tests
* Done!

* Computation finished
