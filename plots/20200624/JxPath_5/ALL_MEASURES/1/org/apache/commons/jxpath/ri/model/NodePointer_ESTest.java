/*
 * This file was automatically generated by EvoSuite
 * Wed Jun 17 05:49:10 GMT 2020
 */

package org.apache.commons.jxpath.ri.model;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.commons.jxpath.BasicVariables;
import org.apache.commons.jxpath.ri.QName;
import org.apache.commons.jxpath.ri.compiler.NodeNameTest;
import org.apache.commons.jxpath.ri.model.NodePointer;
import org.apache.commons.jxpath.ri.model.VariablePointer;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class NodePointer_ESTest extends NodePointer_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      QName qName0 = new QName("nde");
      VariablePointer variablePointer0 = new VariablePointer(qName0);
      NodeNameTest nodeNameTest0 = new NodeNameTest(qName0, "nde");
      NodePointer nodePointer0 = NodePointer.newChildNodePointer(variablePointer0, qName0, nodeNameTest0);
      BasicVariables basicVariables0 = new BasicVariables();
      VariablePointer variablePointer1 = new VariablePointer(basicVariables0, qName0);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     org.apache.commons.jxpath.JXPathException : Cannot compare pointers that do not belong to the same tree: '$nde' and '$nde'
      nodePointer0.compareTo(variablePointer1);
  }
}
