* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 12072
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 28
[MASTER] 07:01:27.680 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:01:29.733 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("wp9T3&bsol;&percnt;KQ&Hat; 3&rsqb;&lowbar;Lx&apos;O", var7);  // (Primitive) Original Value: wp9T3&bsol;&percnt;KQ&Hat; 3&rsqb;&lowbar;Lx&apos;O | Regression Value: wp9T3&bsol;&percnt;KQ&hat; 3&rsqb;&lowbar;Lx&apos;O
[MASTER] 07:01:29.735 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:01:29.735 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:01:30.641 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:01:30.651 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("wp9T3&bsol;&percnt;KQ&Hat; 3&rsqb;&lowbar;Lx&apos;O", var7);  // (Primitive) Original Value: wp9T3&bsol;&percnt;KQ&Hat; 3&rsqb;&lowbar;Lx&apos;O | Regression Value: wp9T3&bsol;&percnt;KQ&hat; 3&rsqb;&lowbar;Lx&apos;O
[MASTER] 07:01:30.652 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:01:30.652 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 07:01:30.881 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:01:30.890 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("wp9T3&bsol;&percnt;KQ&Hat; 3&rsqb;&lowbar;Lx&apos;O", var7);  // (Primitive) Original Value: wp9T3&bsol;&percnt;KQ&Hat; 3&rsqb;&lowbar;Lx&apos;O | Regression Value: wp9T3&bsol;&percnt;KQ&hat; 3&rsqb;&lowbar;Lx&apos;O
[MASTER] 07:01:30.891 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:01:30.891 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:01:30.891 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 252 | Tests with assertion: 1
* Generated 1 tests with total length 8
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          3 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 07:01:31.370 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:01:31.379 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 6 
[MASTER] 07:01:31.386 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 07:01:31.388 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@47f62da7] invalid assertion(s) to be removed
[MASTER] 07:01:31.389 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:01:31.389 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 07:01:31.391 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
