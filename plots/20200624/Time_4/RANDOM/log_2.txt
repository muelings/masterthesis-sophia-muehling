* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 2417
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 134
[MASTER] 09:24:29.369 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:27:43.630 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 09:27:44.838 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
Keeping 2 assertions.
[MASTER] 09:27:45.075 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 09:27:45.077 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 10557 | Tests with assertion: 1
* Generated 1 tests with total length 27
* GA-Budget:
	- MaxTime :                        195 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 09:27:46.609 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:27:46.609 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 84 seconds more than allowed.
[MASTER] 09:27:46.639 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 26 
[MASTER] 09:27:46.665 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 09:27:46.666 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [with:MockIllegalArgumentException]
[MASTER] 09:27:46.666 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: with:MockIllegalArgumentException at 25
[MASTER] 09:27:46.666 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [MockIllegalArgumentException:<init>-219,MockIllegalArgumentException:<init>-219, with:MockIllegalArgumentException]
[MASTER] 09:27:47.630 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 12 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:27:47.635 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 21%
* Total number of goals: 134
* Number of covered goals: 28
* Generated 1 tests with total length 12
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Partial_ESTest' to evosuite-tests
* Done!

* Computation finished
