* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 11498
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:37:12.599 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:37:12.603 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 53
* Using seed 1592372230399
* Starting evolution
[MASTER] 07:37:13.958 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.0 - branchDistance:0.0 - coverage:99.0 - ex: 0 - tex: 2
[MASTER] 07:37:13.958 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.0, number of tests: 1, total length: 32
[MASTER] 07:37:14.396 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:186.0 - branchDistance:0.0 - coverage:76.0 - ex: 0 - tex: 14
[MASTER] 07:37:14.396 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 186.0, number of tests: 7, total length: 200
[MASTER] 07:37:14.495 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:177.0 - branchDistance:0.0 - coverage:67.0 - ex: 0 - tex: 16
[MASTER] 07:37:14.495 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 177.0, number of tests: 9, total length: 218
[MASTER] 07:37:14.768 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:130.6020807483058 - branchDistance:0.0 - coverage:86.0 - ex: 0 - tex: 10
[MASTER] 07:37:14.769 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.6020807483058, number of tests: 5, total length: 135
[MASTER] 07:37:15.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:128.6020807483058 - branchDistance:0.0 - coverage:84.0 - ex: 0 - tex: 12
[MASTER] 07:37:15.904 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.6020807483058, number of tests: 6, total length: 150
[MASTER] 07:37:15.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:123.60208074830581 - branchDistance:0.0 - coverage:79.0 - ex: 0 - tex: 10
[MASTER] 07:37:15.917 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.60208074830581, number of tests: 6, total length: 134
[MASTER] 07:37:16.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:119.60208074830581 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 10
[MASTER] 07:37:16.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.60208074830581, number of tests: 5, total length: 109
[MASTER] 07:37:16.340 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:115.60208074830581 - branchDistance:0.0 - coverage:71.0 - ex: 0 - tex: 12
[MASTER] 07:37:16.340 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.60208074830581, number of tests: 6, total length: 129
[MASTER] 07:37:16.510 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:114.60208074830581 - branchDistance:0.0 - coverage:70.0 - ex: 0 - tex: 10
[MASTER] 07:37:16.510 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.60208074830581, number of tests: 7, total length: 127
[MASTER] 07:37:16.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:108.60208074830581 - branchDistance:0.0 - coverage:64.0 - ex: 0 - tex: 10
[MASTER] 07:37:16.569 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.60208074830581, number of tests: 5, total length: 130
[MASTER] 07:37:17.378 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:107.60208074830581 - branchDistance:0.0 - coverage:63.0 - ex: 0 - tex: 12
[MASTER] 07:37:17.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.60208074830581, number of tests: 7, total length: 151
[MASTER] 07:37:17.584 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:104.10208074830581 - branchDistance:0.0 - coverage:59.5 - ex: 0 - tex: 10
[MASTER] 07:37:17.584 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.10208074830581, number of tests: 5, total length: 109
[MASTER] 07:37:17.817 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:103.60208074830581 - branchDistance:0.0 - coverage:59.0 - ex: 0 - tex: 12
[MASTER] 07:37:17.817 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.60208074830581, number of tests: 7, total length: 163
[MASTER] 07:37:18.077 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:101.10208074830581 - branchDistance:0.0 - coverage:56.5 - ex: 0 - tex: 12
[MASTER] 07:37:18.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.10208074830581, number of tests: 7, total length: 151
[MASTER] 07:37:18.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:99.60208074830581 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 10
[MASTER] 07:37:18.137 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.60208074830581, number of tests: 7, total length: 176
[MASTER] 07:37:18.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:97.10208074830581 - branchDistance:0.0 - coverage:52.5 - ex: 0 - tex: 14
[MASTER] 07:37:18.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.10208074830581, number of tests: 7, total length: 157
[MASTER] 07:37:18.640 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:93.60208074830581 - branchDistance:0.0 - coverage:49.0 - ex: 0 - tex: 12
[MASTER] 07:37:18.640 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.60208074830581, number of tests: 8, total length: 190
[MASTER] 07:37:19.210 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:92.60208074830581 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 12
[MASTER] 07:37:19.211 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.60208074830581, number of tests: 8, total length: 161
[MASTER] 07:37:19.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:90.60208074830581 - branchDistance:0.0 - coverage:46.0 - ex: 0 - tex: 12
[MASTER] 07:37:19.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.60208074830581, number of tests: 7, total length: 173
[MASTER] 07:37:19.523 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:87.10208074830581 - branchDistance:0.0 - coverage:42.5 - ex: 0 - tex: 16
[MASTER] 07:37:19.523 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.10208074830581, number of tests: 9, total length: 211
[MASTER] 07:37:19.842 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:85.10208074830581 - branchDistance:0.0 - coverage:40.5 - ex: 0 - tex: 16
[MASTER] 07:37:19.842 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.10208074830581, number of tests: 9, total length: 217
[MASTER] 07:37:20.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:83.10208074830581 - branchDistance:0.0 - coverage:38.5 - ex: 0 - tex: 18
[MASTER] 07:37:20.904 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.10208074830581, number of tests: 11, total length: 239
[MASTER] 07:37:21.490 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:80.10208074830581 - branchDistance:0.0 - coverage:35.5 - ex: 0 - tex: 16
[MASTER] 07:37:21.490 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.10208074830581, number of tests: 10, total length: 221
[MASTER] 07:37:22.198 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:78.60208074830581 - branchDistance:0.0 - coverage:34.0 - ex: 0 - tex: 18
[MASTER] 07:37:22.198 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.60208074830581, number of tests: 12, total length: 266
[MASTER] 07:37:22.712 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:78.10208074830581 - branchDistance:0.0 - coverage:33.5 - ex: 0 - tex: 18
[MASTER] 07:37:22.713 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.10208074830581, number of tests: 11, total length: 224
[MASTER] 07:37:22.737 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:69.10208074830581 - branchDistance:0.0 - coverage:24.5 - ex: 0 - tex: 16
[MASTER] 07:37:22.737 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.10208074830581, number of tests: 10, total length: 213
[MASTER] 07:37:23.821 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:65.60208074830581 - branchDistance:0.0 - coverage:21.0 - ex: 0 - tex: 20
[MASTER] 07:37:23.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.60208074830581, number of tests: 12, total length: 267
[MASTER] 07:37:25.292 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:63.60208074830581 - branchDistance:0.0 - coverage:19.0 - ex: 0 - tex: 16
[MASTER] 07:37:25.293 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.60208074830581, number of tests: 11, total length: 254
[MASTER] 07:37:25.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:62.60208074830581 - branchDistance:0.0 - coverage:18.0 - ex: 0 - tex: 16
[MASTER] 07:37:25.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.60208074830581, number of tests: 11, total length: 234
[MASTER] 07:37:26.200 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:60.60208074830581 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 18
[MASTER] 07:37:26.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.60208074830581, number of tests: 11, total length: 246
[MASTER] 07:37:27.433 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:59.60208074830581 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 20
[MASTER] 07:37:27.433 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.60208074830581, number of tests: 11, total length: 249
[MASTER] 07:37:29.119 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:58.60208074830581 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 22
[MASTER] 07:37:29.119 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.60208074830581, number of tests: 12, total length: 242
[MASTER] 07:37:31.290 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998806967310905 - fitness:57.60208074830581 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 22
[MASTER] 07:37:31.290 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.60208074830581, number of tests: 12, total length: 246
[MASTER] 07:37:35.561 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999216423758033 - fitness:57.6013665997994 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 20
[MASTER] 07:37:35.562 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.6013665997994, number of tests: 11, total length: 220
[MASTER] 07:37:38.917 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999254954552228 - fitness:57.601299397985336 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 20
[MASTER] 07:37:38.917 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.601299397985336, number of tests: 11, total length: 230
[MASTER] 07:37:45.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999353420406052 - fitness:57.601127663976825 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 20
[MASTER] 07:37:45.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.601127663976825, number of tests: 10, total length: 214
[MASTER] 07:37:50.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999397953040337 - fitness:57.60104999518351 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 20
[MASTER] 07:37:50.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.60104999518351, number of tests: 10, total length: 215
[MASTER] 07:37:54.664 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999439021653764 - fitness:57.60097836818957 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 20
[MASTER] 07:37:54.664 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.60097836818957, number of tests: 10, total length: 205
[MASTER] 07:38:00.486 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999439021653764 - fitness:56.60097836818957 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 20
[MASTER] 07:38:00.486 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.60097836818957, number of tests: 11, total length: 231
[MASTER] 07:38:04.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499948234806916 - fitness:56.60090280366091 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 20
[MASTER] 07:38:04.134 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.60090280366091, number of tests: 11, total length: 235
[MASTER] 07:38:06.581 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499948234806916 - fitness:55.10090280366091 - branchDistance:0.0 - coverage:10.5 - ex: 0 - tex: 22
[MASTER] 07:38:06.581 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.10090280366091, number of tests: 12, total length: 257
[MASTER] 07:38:08.694 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499948234806916 - fitness:54.10090280366091 - branchDistance:0.0 - coverage:9.5 - ex: 0 - tex: 24
[MASTER] 07:38:08.694 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.10090280366091, number of tests: 12, total length: 265
[MASTER] 07:38:11.126 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499948234806916 - fitness:53.60090280366091 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 22
[MASTER] 07:38:11.127 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.60090280366091, number of tests: 13, total length: 264
[MASTER] 07:38:13.617 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999505684626793 - fitness:53.600862103056905 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 20
[MASTER] 07:38:13.617 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.600862103056905, number of tests: 13, total length: 247
[MASTER] 07:38:17.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999505684626793 - fitness:52.600862103056905 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 20
[MASTER] 07:38:17.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.600862103056905, number of tests: 12, total length: 233
[MASTER] 07:38:23.656 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:52.60082103043085 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 20
[MASTER] 07:38:23.656 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.60082103043085, number of tests: 10, total length: 220
[MASTER] 07:38:27.576 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:51.60082103043085 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 22
[MASTER] 07:38:27.576 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.60082103043085, number of tests: 11, total length: 230
[MASTER] 07:38:30.292 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:50.60082103043085 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 22
[MASTER] 07:38:30.293 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.60082103043085, number of tests: 11, total length: 236
[MASTER] 07:38:32.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:50.10082103043085 - branchDistance:0.0 - coverage:5.5 - ex: 0 - tex: 22
[MASTER] 07:38:32.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.10082103043085, number of tests: 12, total length: 274
[MASTER] 07:38:35.474 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:49.60082103043085 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 24
[MASTER] 07:38:35.474 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.60082103043085, number of tests: 13, total length: 295
[MASTER] 07:39:14.815 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:49.10082103043085 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 24
[MASTER] 07:39:14.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.10082103043085, number of tests: 12, total length: 228
[MASTER] 07:39:19.547 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:48.60082103043085 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 26
[MASTER] 07:39:19.547 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.60082103043085, number of tests: 13, total length: 249
[MASTER] 07:39:44.466 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999529234535354 - fitness:47.60082103043085 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 28
[MASTER] 07:39:44.466 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.60082103043085, number of tests: 14, total length: 258
[MASTER] 07:40:06.911 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4998869034155167 - fitness:35.14386350416855 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 28
[MASTER] 07:40:06.911 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.14386350416855, number of tests: 14, total length: 250
[MASTER] 07:42:13.626 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1553 generations, 1239066 statements, best individuals have fitness: 35.14386350416855
[MASTER] 07:42:13.630 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 92%
* Total number of goals: 53
* Number of covered goals: 49
* Generated 14 tests with total length 234
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     35 / 0           
	- RMIStoppingCondition
[MASTER] 07:42:13.981 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:42:14.222 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 199 
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.314 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:42:14.315 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 07:42:14.316 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 07:42:14.317 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 07:42:14.317 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
