* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 13422
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:31:03.441 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:31:03.445 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 53
* Using seed 1592371861299
* Starting evolution
[MASTER] 07:31:04.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.0 - branchDistance:0.0 - coverage:109.0 - ex: 0 - tex: 4
[MASTER] 07:31:04.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.0, number of tests: 3, total length: 80
[MASTER] 07:31:04.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:196.0 - branchDistance:0.0 - coverage:86.0 - ex: 0 - tex: 10
[MASTER] 07:31:04.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 196.0, number of tests: 6, total length: 143
[MASTER] 07:31:05.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:193.0 - branchDistance:0.0 - coverage:83.0 - ex: 0 - tex: 8
[MASTER] 07:31:05.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 193.0, number of tests: 4, total length: 87
[MASTER] 07:31:05.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:186.0 - branchDistance:0.0 - coverage:76.0 - ex: 0 - tex: 10
[MASTER] 07:31:05.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 186.0, number of tests: 5, total length: 129
[MASTER] 07:31:05.414 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:178.0 - branchDistance:0.0 - coverage:68.0 - ex: 0 - tex: 14
[MASTER] 07:31:05.414 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 178.0, number of tests: 9, total length: 201
[MASTER] 07:31:05.921 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:176.0 - branchDistance:0.0 - coverage:66.0 - ex: 0 - tex: 16
[MASTER] 07:31:05.921 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 176.0, number of tests: 9, total length: 155
[MASTER] 07:31:06.204 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:164.0 - branchDistance:0.0 - coverage:54.0 - ex: 0 - tex: 10
[MASTER] 07:31:06.205 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 164.0, number of tests: 8, total length: 144
[MASTER] 07:31:06.763 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:153.0 - branchDistance:0.0 - coverage:43.0 - ex: 0 - tex: 10
[MASTER] 07:31:06.763 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 153.0, number of tests: 5, total length: 148
[MASTER] 07:31:07.307 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:151.0 - branchDistance:0.0 - coverage:41.0 - ex: 0 - tex: 14
[MASTER] 07:31:07.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 151.0, number of tests: 7, total length: 175
[MASTER] 07:31:07.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:149.0 - branchDistance:0.0 - coverage:39.0 - ex: 0 - tex: 10
[MASTER] 07:31:07.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 149.0, number of tests: 6, total length: 136
[MASTER] 07:31:07.426 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:147.0 - branchDistance:0.0 - coverage:37.0 - ex: 0 - tex: 12
[MASTER] 07:31:07.426 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 147.0, number of tests: 6, total length: 147
[MASTER] 07:31:07.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:146.0 - branchDistance:0.0 - coverage:36.0 - ex: 0 - tex: 10
[MASTER] 07:31:07.509 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.0, number of tests: 7, total length: 175
[MASTER] 07:31:08.006 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:139.0 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 14
[MASTER] 07:31:08.006 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.0, number of tests: 7, total length: 178
[MASTER] 07:31:08.436 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:138.0 - branchDistance:0.0 - coverage:28.0 - ex: 0 - tex: 12
[MASTER] 07:31:08.436 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.0, number of tests: 6, total length: 144
[MASTER] 07:31:08.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:136.0 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 12
[MASTER] 07:31:08.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.0, number of tests: 7, total length: 169
[MASTER] 07:31:08.859 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:135.0 - branchDistance:0.0 - coverage:25.0 - ex: 0 - tex: 14
[MASTER] 07:31:08.859 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.0, number of tests: 7, total length: 190
[MASTER] 07:31:08.947 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:134.0 - branchDistance:0.0 - coverage:24.0 - ex: 0 - tex: 10
[MASTER] 07:31:08.947 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.0, number of tests: 6, total length: 156
[MASTER] 07:31:08.990 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:133.0 - branchDistance:0.0 - coverage:23.0 - ex: 0 - tex: 14
[MASTER] 07:31:08.990 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.0, number of tests: 7, total length: 151
[MASTER] 07:31:09.181 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:130.5 - branchDistance:0.0 - coverage:20.5 - ex: 0 - tex: 16
[MASTER] 07:31:09.181 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.5, number of tests: 8, total length: 158
[MASTER] 07:31:09.796 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:125.5 - branchDistance:0.0 - coverage:15.5 - ex: 0 - tex: 14
[MASTER] 07:31:09.796 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.5, number of tests: 8, total length: 185
[MASTER] 07:31:11.252 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:124.5 - branchDistance:0.0 - coverage:14.5 - ex: 0 - tex: 16
[MASTER] 07:31:11.252 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.5, number of tests: 9, total length: 189
[MASTER] 07:31:11.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499854184893555 - fitness:60.102543163789086 - branchDistance:0.0 - coverage:15.5 - ex: 0 - tex: 14
[MASTER] 07:31:11.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.102543163789086, number of tests: 8, total length: 178
[MASTER] 07:31:12.066 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499854184893555 - fitness:59.602543163789086 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 16
[MASTER] 07:31:12.067 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.602543163789086, number of tests: 9, total length: 203
[MASTER] 07:31:13.491 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998862343572241 - fitness:59.60198416310185 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 16
[MASTER] 07:31:13.491 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.60198416310185, number of tests: 9, total length: 208
[MASTER] 07:31:13.724 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499854184893555 - fitness:58.602543163789086 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 18
[MASTER] 07:31:13.724 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.602543163789086, number of tests: 10, total length: 220
[MASTER] 07:31:15.663 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998850310416187 - fitness:58.6020051508462 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 18
[MASTER] 07:31:15.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.6020051508462, number of tests: 10, total length: 217
[MASTER] 07:31:18.319 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999058557710412 - fitness:58.6016419371846 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 18
[MASTER] 07:31:18.319 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.6016419371846, number of tests: 10, total length: 208
[MASTER] 07:31:23.280 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999058557710412 - fitness:57.6016419371846 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 18
[MASTER] 07:31:23.280 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.6016419371846, number of tests: 10, total length: 195
[MASTER] 07:31:24.915 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999058557710412 - fitness:56.6016419371846 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 20
[MASTER] 07:31:24.915 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.6016419371846, number of tests: 11, total length: 232
[MASTER] 07:31:31.587 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999058557710412 - fitness:55.6016419371846 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 20
[MASTER] 07:31:31.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.6016419371846, number of tests: 11, total length: 244
[MASTER] 07:31:36.802 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999271243259 - fitness:55.601270988805965 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 20
[MASTER] 07:31:36.802 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.601270988805965, number of tests: 11, total length: 211
[MASTER] 07:31:45.849 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999271243259 - fitness:54.101270988805965 - branchDistance:0.0 - coverage:9.5 - ex: 0 - tex: 22
[MASTER] 07:31:45.849 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.101270988805965, number of tests: 12, total length: 204
[MASTER] 07:31:54.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999271243259 - fitness:53.601270988805965 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 24
[MASTER] 07:31:54.113 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.601270988805965, number of tests: 14, total length: 244
[MASTER] 07:32:02.520 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999271243259 - fitness:52.601270988805965 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 22
[MASTER] 07:32:02.520 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.601270988805965, number of tests: 12, total length: 187
[MASTER] 07:32:17.100 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:52.60091282137174 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 22
[MASTER] 07:32:17.100 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.60091282137174, number of tests: 12, total length: 172
[MASTER] 07:32:35.228 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:52.10091282137174 - branchDistance:0.0 - coverage:7.5 - ex: 0 - tex: 22
[MASTER] 07:32:35.229 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.10091282137174, number of tests: 12, total length: 159
[MASTER] 07:32:37.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:51.60091282137174 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 26
[MASTER] 07:32:37.180 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.60091282137174, number of tests: 14, total length: 189
[MASTER] 07:33:15.330 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:51.10091282137174 - branchDistance:0.0 - coverage:6.5 - ex: 0 - tex: 24
[MASTER] 07:33:15.330 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.10091282137174, number of tests: 14, total length: 196
[MASTER] 07:33:15.730 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:50.60091282137174 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 28
[MASTER] 07:33:15.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.60091282137174, number of tests: 15, total length: 212
[MASTER] 07:33:25.539 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:49.60091282137174 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 28
[MASTER] 07:33:25.539 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.60091282137174, number of tests: 15, total length: 201
[MASTER] 07:33:33.023 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:48.60091282137174 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 26
[MASTER] 07:33:33.023 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.60091282137174, number of tests: 15, total length: 193
[MASTER] 07:35:03.955 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999476604208102 - fitness:47.60091282137174 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 30
[MASTER] 07:35:03.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.60091282137174, number of tests: 17, total length: 223
[MASTER] 07:36:04.459 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1725 generations, 1185436 statements, best individuals have fitness: 47.60091282137174
[MASTER] 07:36:04.461 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 92%
* Total number of goals: 53
* Number of covered goals: 49
* Generated 15 tests with total length 177
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     48 / 0           
	- RMIStoppingCondition
[MASTER] 07:36:04.985 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:36:05.015 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 148 
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.109 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 07:36:05.110 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 07:36:05.111 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 07:36:05.111 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 07:36:05.111 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 07:36:05.111 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 07:36:05.112 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
