* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.plot.XYPlot
* Starting client
* Connecting to master process on port 2013
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_4_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.plot.XYPlot
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 22:16:17.426 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 1079
[MASTER] 22:16:26.125 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:16:27.548 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:16:27.603 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 22:16:28.125 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:16:28.194 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:16:28.195 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 91 | Tests with assertion: 1
* Generated 1 tests with total length 19
* GA-Budget:
	- MaxTime :                         10 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 22:16:31.615 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:16:31.671 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 22:16:31.714 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 22:16:31.714 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException]
[MASTER] 22:16:31.714 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NullPointerException at 7
[MASTER] 22:16:31.714 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException]
[MASTER] 22:16:32.261 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
[MASTER] 22:16:32.266 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 5%
* Total number of goals: 1079
* Number of covered goals: 59
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYPlot_ESTest' to evosuite-tests
* Done!

* Computation finished
