/*
 * This file was automatically generated by EvoSuite
 * Sun Jun 28 20:47:17 GMT 2020
 */

package org.jfree.chart.plot;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.util.Locale;
import java.util.SimpleTimeZone;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.TimeTableXYDataset;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYPlot_ESTest extends XYPlot_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      SimpleTimeZone simpleTimeZone0 = new SimpleTimeZone((-1883), "Tv5");
      Locale locale0 = new Locale("Tv5", "Tv5", "]}t8Q RG^FGUuR[");
      TimeTableXYDataset timeTableXYDataset0 = new TimeTableXYDataset(simpleTimeZone0, locale0);
      LogAxis logAxis0 = new LogAxis();
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NullPointerException : null
      XYPlot xYPlot0 = new XYPlot(timeTableXYDataset0, logAxis0, (ValueAxis) null, (XYItemRenderer) null);
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : The 'alpha' value must be in the range 0.0f to 1.0f
      ValueMarker valueMarker0 = null;
      try {
        valueMarker0 = new ValueMarker(0.05, logAxis0.DEFAULT_TICK_MARK_PAINT, xYPlot0.DEFAULT_CROSSHAIR_STROKE, logAxis0.DEFAULT_TICK_MARK_PAINT, logAxis0.DEFAULT_AXIS_LINE_STROKE, (-5.0F));
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // The 'alpha' value must be in the range 0.0f to 1.0f
         //
         verifyException("org.jfree.chart.plot.Marker", e);
         assertTrue(e.getMessage().equals("The 'alpha' value must be in the range 0.0f to 1.0f"));   
      }
  }
}
