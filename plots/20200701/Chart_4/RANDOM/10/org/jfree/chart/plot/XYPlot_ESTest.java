/*
 * This file was automatically generated by EvoSuite
 * Sun Jun 28 20:53:20 GMT 2020
 */

package org.jfree.chart.plot;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.Range;
import org.jfree.data.xy.DefaultIntervalXYDataset;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYPlot_ESTest extends XYPlot_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      DefaultIntervalXYDataset defaultIntervalXYDataset0 = new DefaultIntervalXYDataset();
      NumberAxis numberAxis0 = new NumberAxis((String) null);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NullPointerException : null
      XYPlot xYPlot0 = new XYPlot(defaultIntervalXYDataset0, numberAxis0, numberAxis0, (XYItemRenderer) null);
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Null 'range' argument.
      // Undeclared exception!
      try { 
        numberAxis0.setDefaultAutoRange((Range) null);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Null 'range' argument.
         //
         verifyException("org.jfree.chart.axis.ValueAxis", e);
         assertTrue(e.getMessage().equals("Null 'range' argument."));   
      }
  }
}
