/*
 * This file was automatically generated by EvoSuite
 * Sun Jun 28 20:10:39 GMT 2020
 */

package org.jfree.chart.plot;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.awt.image.IndexColorModel;
import java.util.Locale;
import java.util.TimeZone;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.evosuite.runtime.mock.java.util.MockDate;
import org.jfree.chart.axis.PeriodAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Quarter;
import org.jfree.data.xy.XYBarDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYPlot_ESTest extends XYPlot_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      XYSeriesCollection xYSeriesCollection0 = new XYSeriesCollection();
      XYBarDataset xYBarDataset0 = new XYBarDataset(xYSeriesCollection0, 0.001);
      MockDate mockDate0 = new MockDate(0L);
      Quarter quarter0 = new Quarter(mockDate0);
      TimeZone timeZone0 = TimeZone.getTimeZone("");
      Locale locale0 = new Locale("");
      PeriodAxis periodAxis0 = new PeriodAxis("", quarter0, quarter0, timeZone0, locale0);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NullPointerException : null
      XYPlot xYPlot0 = new XYPlot(xYBarDataset0, periodAxis0, periodAxis0, (XYItemRenderer) null);
      byte[] byteArray0 = new byte[2];
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     java.lang.ArrayIndexOutOfBoundsException : -1952257861
      IndexColorModel indexColorModel0 = null;
      try {
        indexColorModel0 = new IndexColorModel(4, 10, byteArray0, (-1952257861), false, 4);
        fail("Expecting exception: ArrayIndexOutOfBoundsException");
      
      } catch(ArrayIndexOutOfBoundsException e) {
         //
         // -1952257861
         //
         verifyException("java.awt.image.IndexColorModel", e);
         assertTrue(e.getMessage().equals("-1952257861"));   
      }
  }
}
