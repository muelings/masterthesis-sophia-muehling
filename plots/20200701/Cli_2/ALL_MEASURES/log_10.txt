* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.PosixParser
* Starting client
* Connecting to master process on port 20769
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.PosixParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:31:44.125 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:31:44.131 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 45
* Using seed 1593491501957
* Starting evolution
[MASTER] 06:31:45.017 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:187.0 - branchDistance:0.0 - coverage:86.0 - ex: 0 - tex: 14
[MASTER] 06:31:45.017 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 187.0, number of tests: 8, total length: 169
[MASTER] 06:31:45.637 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6597938144329896 - fitness:82.32394363623476 - branchDistance:0.0 - coverage:53.99999997426292 - ex: 0 - tex: 12
[MASTER] 06:31:45.637 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.32394363623476, number of tests: 7, total length: 142
[MASTER] 06:31:46.285 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:72.57142809272902 - branchDistance:0.0 - coverage:42.99999952130045 - ex: 0 - tex: 8
[MASTER] 06:31:46.285 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.57142809272902, number of tests: 5, total length: 115
[MASTER] 06:31:47.372 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.642857142857143 - fitness:69.45098039215686 - branchDistance:0.0 - coverage:41.0 - ex: 0 - tex: 6
[MASTER] 06:31:47.372 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.45098039215686, number of tests: 5, total length: 133
[MASTER] 06:31:47.721 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.642857142857143 - fitness:68.45098039215686 - branchDistance:0.0 - coverage:40.0 - ex: 0 - tex: 6
[MASTER] 06:31:47.721 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.45098039215686, number of tests: 6, total length: 122
[MASTER] 06:31:47.828 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.642857142857143 - fitness:65.45098039215686 - branchDistance:0.0 - coverage:37.0 - ex: 0 - tex: 8
[MASTER] 06:31:47.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.45098039215686, number of tests: 5, total length: 115
[MASTER] 06:31:48.426 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5813953488372094 - fitness:60.32207792207792 - branchDistance:0.0 - coverage:31.4 - ex: 0 - tex: 8
[MASTER] 06:31:48.426 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.32207792207792, number of tests: 5, total length: 127
[MASTER] 06:31:48.981 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.285714285714286 - fitness:47.809090909090905 - branchDistance:0.0 - coverage:30.9 - ex: 0 - tex: 6
[MASTER] 06:31:48.981 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.809090909090905, number of tests: 6, total length: 156
[MASTER] 06:31:50.381 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.291655664114074 - fitness:47.79406752985122 - branchDistance:0.0 - coverage:30.9 - ex: 0 - tex: 8
[MASTER] 06:31:50.381 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.79406752985122, number of tests: 6, total length: 136
[MASTER] 06:31:50.787 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.297402597402598 - fitness:47.77956279645287 - branchDistance:0.0 - coverage:30.9 - ex: 0 - tex: 4
[MASTER] 06:31:50.787 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.77956279645287, number of tests: 5, total length: 113
[MASTER] 06:31:50.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.304147465437788 - fitness:47.7625730994152 - branchDistance:0.0 - coverage:30.9 - ex: 0 - tex: 6
[MASTER] 06:31:50.962 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.7625730994152, number of tests: 5, total length: 116
[MASTER] 06:31:51.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.291655664114074 - fitness:47.39406752985121 - branchDistance:0.0 - coverage:30.5 - ex: 0 - tex: 8
[MASTER] 06:31:51.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.39406752985121, number of tests: 6, total length: 137
[MASTER] 06:31:51.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.304147465437788 - fitness:47.3625730994152 - branchDistance:0.0 - coverage:30.5 - ex: 0 - tex: 6
[MASTER] 06:31:51.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.3625730994152, number of tests: 5, total length: 116
[MASTER] 06:31:52.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.306578613157226 - fitness:47.356458173909544 - branchDistance:0.0 - coverage:30.5 - ex: 0 - tex: 4
[MASTER] 06:31:52.761 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.356458173909544, number of tests: 5, total length: 112
[MASTER] 06:32:03.730 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.306578613157226 - fitness:42.856458173909544 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 6
[MASTER] 06:32:03.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.856458173909544, number of tests: 6, total length: 143
[MASTER] 06:32:04.493 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.306578613157226 - fitness:42.356458173909544 - branchDistance:0.0 - coverage:26.0 - ex: 1 - tex: 7
[MASTER] 06:32:04.493 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.356458173909544, number of tests: 6, total length: 146
[MASTER] 06:32:06.242 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.307661290322581 - fitness:42.353736495557115 - branchDistance:0.0 - coverage:26.0 - ex: 1 - tex: 7
[MASTER] 06:32:06.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.353736495557115, number of tests: 6, total length: 160
[MASTER] 06:32:06.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.306578613157226 - fitness:41.356458173909544 - branchDistance:0.0 - coverage:25.0 - ex: 1 - tex: 7
[MASTER] 06:32:06.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.356458173909544, number of tests: 7, total length: 178
[MASTER] 06:32:07.014 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.307661290322581 - fitness:41.353736495557115 - branchDistance:0.0 - coverage:25.0 - ex: 1 - tex: 5
[MASTER] 06:32:07.014 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.353736495557115, number of tests: 6, total length: 162
[MASTER] 06:32:07.614 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.308319185059423 - fitness:41.35208310905372 - branchDistance:0.0 - coverage:25.0 - ex: 1 - tex: 5
[MASTER] 06:32:07.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.35208310905372, number of tests: 6, total length: 166
[MASTER] 06:32:09.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.308597285067873 - fitness:41.35138430641228 - branchDistance:0.0 - coverage:25.0 - ex: 1 - tex: 5
[MASTER] 06:32:09.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.35138430641228, number of tests: 6, total length: 150
[MASTER] 06:32:10.260 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.309496954658245 - fitness:41.34912406149446 - branchDistance:0.0 - coverage:25.0 - ex: 1 - tex: 7
[MASTER] 06:32:10.261 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.34912406149446, number of tests: 5, total length: 163
[MASTER] 06:32:10.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.969953842134293 - fitness:34.61580863867557 - branchDistance:0.0 - coverage:25.0 - ex: 1 - tex: 7
[MASTER] 06:32:10.551 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.61580863867557, number of tests: 6, total length: 165
[MASTER] 06:32:21.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.969953842134293 - fitness:22.9491419720089 - branchDistance:0.0 - coverage:13.333333333333334 - ex: 1 - tex: 5
[MASTER] 06:32:21.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.9491419720089, number of tests: 4, total length: 110
[MASTER] 06:32:22.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.969953842134293 - fitness:20.949141972008896 - branchDistance:0.0 - coverage:11.333333333333332 - ex: 1 - tex: 5
[MASTER] 06:32:22.185 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 20.949141972008896, number of tests: 4, total length: 109
[MASTER] 06:32:23.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.969953842134293 - fitness:19.615808638675567 - branchDistance:0.0 - coverage:10.0 - ex: 1 - tex: 3
[MASTER] 06:32:23.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.615808638675567, number of tests: 3, total length: 92
[MASTER] 06:32:24.593 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.969953842134293 - fitness:17.615808638675567 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 5
[MASTER] 06:32:24.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.615808638675567, number of tests: 5, total length: 172
[MASTER] 06:34:33.142 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.750127746550843 - fitness:15.279602300284072 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:33.142 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.279602300284072, number of tests: 4, total length: 80
[MASTER] 06:34:33.758 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.751644736842106 - fitness:15.278905117627383 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:33.758 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.278905117627383, number of tests: 4, total length: 82
[MASTER] 06:34:33.870 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.753523152142652 - fitness:15.278042028929004 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:33.870 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.278042028929004, number of tests: 4, total length: 82
[MASTER] 06:34:33.965 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.754225012071464 - fitness:15.277719596805865 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:33.965 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.277719596805865, number of tests: 4, total length: 82
[MASTER] 06:34:34.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.780002705993777 - fitness:15.265898625948608 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:34.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.265898625948608, number of tests: 4, total length: 81
[MASTER] 06:34:44.863 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.780002705993777 - fitness:13.555611037388676 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:44.863 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.555611037388676, number of tests: 4, total length: 82
[MASTER] 06:34:45.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.780222527815976 - fitness:13.555554853307378 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:45.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.555554853307378, number of tests: 4, total length: 84
[MASTER] 06:34:45.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.780684520732084 - fitness:13.55543677698263 - branchDistance:0.0 - coverage:8.0 - ex: 1 - tex: 1
[MASTER] 06:34:45.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.55543677698263, number of tests: 4, total length: 84
[MASTER] 06:34:56.785 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.780684520732084 - fitness:12.55543677698263 - branchDistance:0.0 - coverage:7.0 - ex: 1 - tex: 1
[MASTER] 06:34:56.785 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.55543677698263, number of tests: 5, total length: 97
[MASTER] 06:35:42.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.77491601343785 - fitness:12.05691148989184 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 3
[MASTER] 06:35:42.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.05691148989184, number of tests: 5, total length: 91
[MASTER] 06:35:42.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.77883336500095 - fitness:12.055909929296035 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 3
[MASTER] 06:35:42.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.055909929296035, number of tests: 5, total length: 93
[MASTER] 06:35:43.067 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.778947368421054 - fitness:12.055880787653006 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 5
[MASTER] 06:35:43.067 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.055880787653006, number of tests: 6, total length: 128
[MASTER] 06:35:43.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.787163029525033 - fitness:12.053781578025458 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 3
[MASTER] 06:35:43.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.053781578025458, number of tests: 5, total length: 90
[MASTER] 06:35:43.462 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.789473684210527 - fitness:12.053191489361701 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 3
[MASTER] 06:35:43.462 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.053191489361701, number of tests: 5, total length: 92
[MASTER] 06:36:00.539 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.790835913312694 - fitness:12.052843671587063 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 3
[MASTER] 06:36:00.539 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.052843671587063, number of tests: 5, total length: 90
[MASTER] 06:36:00.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.79153959665519 - fitness:12.052664018967993 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 3
[MASTER] 06:36:00.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.052664018967993, number of tests: 5, total length: 92
[MASTER] 06:36:00.876 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.79196413747302 - fitness:12.052555638511162 - branchDistance:0.0 - coverage:6.5 - ex: 1 - tex: 3
[MASTER] 06:36:00.877 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.052555638511162, number of tests: 5, total length: 92
[MASTER] 06:36:46.346 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 1888 generations, 1981955 statements, best individuals have fitness: 12.052555638511162
[MASTER] 06:36:46.350 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 89%
* Total number of goals: 45
* Number of covered goals: 40
* Generated 5 tests with total length 82
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                     12 / 0           
	- MaxTime :                        302 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 06:36:46.505 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:36:46.530 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 58 
[MASTER] 06:36:46.561 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 06:36:46.561 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parse:UnrecognizedOptionException]
[MASTER] 06:36:46.561 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:UnrecognizedOptionException at 21
[MASTER] 06:36:46.561 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 06:36:46.561 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnrecognizedOptionException,, parse:UnrecognizedOptionException]
[MASTER] 06:36:46.562 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 06:36:46.562 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 06:36:46.562 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 06:36:46.562 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 06:36:46.741 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 11 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 06:36:46.742 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 38%
* Total number of goals: 45
* Number of covered goals: 17
* Generated 1 tests with total length 11
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 13
* Writing JUnit test case 'PosixParser_ESTest' to evosuite-tests
* Done!

* Computation finished
