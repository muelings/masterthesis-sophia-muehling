* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.PosixParser
* Starting client
* Connecting to master process on port 21007
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.PosixParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:54:43.605 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:54:43.609 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 45
* Using seed 1593489281638
* Starting evolution
[MASTER] 05:54:44.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:176.99999997496033 - branchDistance:0.0 - coverage:75.99999997496033 - ex: 0 - tex: 6
[MASTER] 05:54:44.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 176.99999997496033, number of tests: 4, total length: 74
[MASTER] 05:54:44.827 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:170.99999995812775 - branchDistance:0.0 - coverage:69.99999995812775 - ex: 0 - tex: 4
[MASTER] 05:54:44.827 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 170.99999995812775, number of tests: 3, total length: 52
[MASTER] 05:54:44.948 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6608863198458574 - fitness:77.31578927922409 - branchDistance:0.0 - coverage:48.99999980553988 - ex: 0 - tex: 18
[MASTER] 05:54:44.948 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.31578927922409, number of tests: 10, total length: 302
[MASTER] 05:54:45.312 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.126909518213866 - fitness:73.00492779684885 - branchDistance:0.0 - coverage:52.499999994877726 - ex: 0 - tex: 4
[MASTER] 05:54:45.312 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.00492779684885, number of tests: 4, total length: 85
[MASTER] 05:54:45.913 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.64343163538874 - fitness:70.94665099442717 - branchDistance:0.0 - coverage:42.49999904446396 - ex: 0 - tex: 8
[MASTER] 05:54:45.913 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.94665099442717, number of tests: 9, total length: 229
[MASTER] 05:54:46.006 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.126909518213866 - fitness:66.00492755982731 - branchDistance:0.0 - coverage:45.49999975785619 - ex: 0 - tex: 4
[MASTER] 05:54:46.006 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.00492755982731, number of tests: 5, total length: 111
[MASTER] 05:54:46.221 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.126909518213866 - fitness:62.00492684643508 - branchDistance:0.0 - coverage:41.49999904446396 - ex: 0 - tex: 4
[MASTER] 05:54:46.221 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.00492684643508, number of tests: 5, total length: 136
[MASTER] 05:54:47.105 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.126909518213866 - fitness:58.00492780197112 - branchDistance:0.0 - coverage:37.5 - ex: 0 - tex: 6
[MASTER] 05:54:47.105 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.00492780197112, number of tests: 8, total length: 159
[MASTER] 05:54:47.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.126909518213866 - fitness:56.00492780197112 - branchDistance:0.0 - coverage:35.5 - ex: 0 - tex: 4
[MASTER] 05:54:47.600 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.00492780197112, number of tests: 6, total length: 140
[MASTER] 05:54:48.241 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.126909518213866 - fitness:54.00492780197112 - branchDistance:0.0 - coverage:33.5 - ex: 0 - tex: 4
[MASTER] 05:54:48.241 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.00492780197112, number of tests: 6, total length: 150
[MASTER] 05:54:48.836 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.126909518213866 - fitness:53.50492780197112 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 4
[MASTER] 05:54:48.836 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.50492780197112, number of tests: 6, total length: 143
[MASTER] 05:54:48.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:53.485174323884 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 6
[MASTER] 05:54:48.897 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.485174323884, number of tests: 7, total length: 169
[MASTER] 05:54:49.054 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:49.985174323884 - branchDistance:0.0 - coverage:29.5 - ex: 0 - tex: 4
[MASTER] 05:54:49.054 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.985174323884, number of tests: 6, total length: 152
[MASTER] 05:54:49.840 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:45.485174323884 - branchDistance:0.0 - coverage:25.0 - ex: 0 - tex: 4
[MASTER] 05:54:49.840 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.485174323884, number of tests: 7, total length: 172
[MASTER] 05:54:49.982 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:41.985174323884 - branchDistance:0.0 - coverage:21.5 - ex: 0 - tex: 4
[MASTER] 05:54:49.982 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.985174323884, number of tests: 7, total length: 168
[MASTER] 05:54:50.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:40.985174323884 - branchDistance:0.0 - coverage:20.5 - ex: 0 - tex: 6
[MASTER] 05:54:50.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.985174323884, number of tests: 8, total length: 197
[MASTER] 05:54:50.435 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:39.985174323884 - branchDistance:0.0 - coverage:19.5 - ex: 0 - tex: 4
[MASTER] 05:54:50.435 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.985174323884, number of tests: 7, total length: 171
[MASTER] 05:54:50.579 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:37.485174323884 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 6
[MASTER] 05:54:50.579 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 37.485174323884, number of tests: 9, total length: 227
[MASTER] 05:54:51.530 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:35.485174323884 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 4
[MASTER] 05:54:51.530 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.485174323884, number of tests: 9, total length: 213
[MASTER] 05:54:51.985 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:34.485174323884 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 4
[MASTER] 05:54:51.985 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.485174323884, number of tests: 9, total length: 211
[MASTER] 05:54:53.567 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:33.485174323884 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 4
[MASTER] 05:54:53.567 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.485174323884, number of tests: 9, total length: 223
[MASTER] 05:54:53.654 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:32.485174323884 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 4
[MASTER] 05:54:53.654 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.485174323884, number of tests: 8, total length: 197
[MASTER] 05:54:54.842 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.137351778656127 - fitness:32.46528178495865 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 4
[MASTER] 05:54:54.842 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.46528178495865, number of tests: 8, total length: 198
[MASTER] 05:54:55.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132107023411371 - fitness:29.485174323884003 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 4
[MASTER] 05:54:55.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.485174323884003, number of tests: 8, total length: 193
[MASTER] 05:54:55.587 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.137351778656127 - fitness:29.465281784958645 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 6
[MASTER] 05:54:55.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.465281784958645, number of tests: 9, total length: 216
[MASTER] 05:54:56.336 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.138054683998207 - fitness:29.462618860682195 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 6
[MASTER] 05:54:56.336 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.462618860682195, number of tests: 9, total length: 217
[MASTER] 05:54:56.674 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.137351778656127 - fitness:28.465281784958645 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 05:54:56.674 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.465281784958645, number of tests: 9, total length: 234
[MASTER] 05:54:57.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.138054683998207 - fitness:28.462618860682195 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 05:54:57.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.462618860682195, number of tests: 9, total length: 234
[MASTER] 05:54:57.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.140681251730823 - fitness:28.45267467542962 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 05:54:57.851 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.45267467542962, number of tests: 9, total length: 234
[MASTER] 05:55:00.762 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.142292490118577 - fitness:28.446579554189086 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:00.762 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.446579554189086, number of tests: 9, total length: 234
[MASTER] 05:55:03.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.143017316556621 - fitness:28.443838868299302 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:03.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.443838868299302, number of tests: 9, total length: 236
[MASTER] 05:55:04.671 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.143033596837945 - fitness:28.44377731879533 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:04.671 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.44377731879533, number of tests: 9, total length: 234
[MASTER] 05:55:05.422 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.14731954411144 - fitness:28.4275873380351 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:05.422 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.4275873380351, number of tests: 9, total length: 235
[MASTER] 05:55:08.635 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.310009853037094 - fitness:24.847835792501755 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:08.635 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.847835792501755, number of tests: 9, total length: 225
[MASTER] 05:55:09.163 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.3104110846614505 - fitness:24.84682814770457 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:09.163 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.84682814770457, number of tests: 9, total length: 224
[MASTER] 05:55:09.231 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.3145115753811405 - fitness:24.83653760171689 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:09.231 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.83653760171689, number of tests: 9, total length: 220
[MASTER] 05:55:10.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.314673913043478 - fitness:24.83613047594457 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:10.600 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.83613047594457, number of tests: 9, total length: 223
[MASTER] 05:55:11.106 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.314895806534603 - fitness:24.83557402428094 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:11.106 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.83557402428094, number of tests: 9, total length: 219
[MASTER] 05:55:12.454 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.315031055900621 - fitness:24.835234872924698 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:12.454 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.835234872924698, number of tests: 9, total length: 220
[MASTER] 05:55:14.710 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.31533180778032 - fitness:24.834480759475326 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:14.710 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.834480759475326, number of tests: 9, total length: 215
[MASTER] 05:55:15.186 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.315851043088321 - fitness:24.83317898376243 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:15.186 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.83317898376243, number of tests: 9, total length: 211
[MASTER] 05:55:16.022 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.316003700277521 - fitness:24.83279629738122 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:16.022 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.83279629738122, number of tests: 9, total length: 215
[MASTER] 05:55:17.974 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.316108339272986 - fitness:24.832533995373243 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:17.974 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.832533995373243, number of tests: 9, total length: 213
[MASTER] 05:55:20.123 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.324456938410426 - fitness:24.811634259483746 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:20.123 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.811634259483746, number of tests: 9, total length: 196
[MASTER] 05:55:21.536 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.323353835294544 - fitness:22.014387707027144 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 6
[MASTER] 05:55:21.536 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.014387707027144, number of tests: 9, total length: 200
[MASTER] 05:55:21.687 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.323353835294544 - fitness:21.014387707027144 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:21.687 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.014387707027144, number of tests: 10, total length: 215
[MASTER] 05:55:22.821 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.323386054988754 - fitness:21.01434119952461 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:22.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.01434119952461, number of tests: 9, total length: 184
[MASTER] 05:55:22.824 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.326298927247702 - fitness:21.010138102627007 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:55:22.824 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.010138102627007, number of tests: 10, total length: 208
[MASTER] 05:55:24.099 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.32632003533694 - fitness:21.010107655674958 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:24.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.010107655674958, number of tests: 9, total length: 198
[MASTER] 05:55:26.992 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.326533785195798 - fitness:19.683791490941598 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 6
[MASTER] 05:55:26.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.683791490941598, number of tests: 9, total length: 181
[MASTER] 05:55:27.662 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.326533785195798 - fitness:18.683791490941598 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:55:27.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.683791490941598, number of tests: 9, total length: 179
[MASTER] 05:55:34.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.326795380728978 - fitness:18.68354618380566 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 05:55:34.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.68354618380566, number of tests: 9, total length: 149
[MASTER] 05:55:34.888 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327166910691924 - fitness:18.6831978087299 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 05:55:34.888 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.6831978087299, number of tests: 9, total length: 148
[MASTER] 05:55:36.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327236140447212 - fitness:18.683132896355907 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 05:55:36.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.683132896355907, number of tests: 8, total length: 145
[MASTER] 05:55:38.128 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327236140447212 - fitness:17.683132896355907 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 05:55:38.128 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.683132896355907, number of tests: 9, total length: 167
[MASTER] 05:55:41.230 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327257793053414 - fitness:17.683112594251746 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:55:41.230 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.683112594251746, number of tests: 7, total length: 146
[MASTER] 05:55:44.233 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327489087104334 - fitness:17.682895731632133 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:44.233 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.682895731632133, number of tests: 7, total length: 134
[MASTER] 05:55:44.777 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327496474810921 - fitness:17.68288880503644 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:44.777 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.68288880503644, number of tests: 7, total length: 134
[MASTER] 05:55:50.455 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327609369516821 - fitness:17.682782957996263 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:50.455 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.682782957996263, number of tests: 6, total length: 120
[MASTER] 05:55:51.399 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327619299232339 - fitness:17.68277364827275 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:51.399 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.68277364827275, number of tests: 6, total length: 120
[MASTER] 05:55:54.682 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.327740303873806 - fitness:17.682660200362633 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:54.682 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.682660200362633, number of tests: 6, total length: 116
[MASTER] 05:55:55.494 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.327738615290231 - fitness:16.111787824246118 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:55.494 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.111787824246118, number of tests: 6, total length: 115
[MASTER] 05:55:56.312 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.327874274282092 - fitness:16.111698560117205 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:56.312 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.111698560117205, number of tests: 6, total length: 112
[MASTER] 05:55:57.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.329104504846175 - fitness:16.11088915344121 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:55:57.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11088915344121, number of tests: 6, total length: 112
[MASTER] 05:56:03.472 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.329246642668199 - fitness:16.110795646988436 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:03.472 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110795646988436, number of tests: 6, total length: 105
[MASTER] 05:56:04.082 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.329302917119874 - fitness:16.11075862700598 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:04.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11075862700598, number of tests: 6, total length: 106
[MASTER] 05:56:06.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.329321396034686 - fitness:16.110746470779944 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:06.569 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110746470779944, number of tests: 6, total length: 107
[MASTER] 05:56:09.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.329322879833052 - fitness:16.110745494675054 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:56:09.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110745494675054, number of tests: 7, total length: 137
[MASTER] 05:56:11.146 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.32937006231539 - fitness:16.110714456178837 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:56:11.146 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110714456178837, number of tests: 7, total length: 114
[MASTER] 05:56:11.823 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330052853164979 - fitness:16.11026531604292 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:11.823 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11026531604292, number of tests: 6, total length: 103
[MASTER] 05:56:14.983 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330132456973347 - fitness:16.11021295585877 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:14.983 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11021295585877, number of tests: 6, total length: 101
[MASTER] 05:56:18.630 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330139524053685 - fitness:16.11020830745018 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:18.630 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11020830745018, number of tests: 6, total length: 98
[MASTER] 05:56:19.583 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330172315116556 - fitness:16.110186739028933 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:19.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110186739028933, number of tests: 7, total length: 123
[MASTER] 05:56:20.078 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330190175632021 - fitness:16.110174991268877 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:20.078 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110174991268877, number of tests: 6, total length: 101
[MASTER] 05:56:20.361 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330233522836425 - fitness:16.11014647977212 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:20.361 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11014647977212, number of tests: 6, total length: 102
[MASTER] 05:56:24.694 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330238277830945 - fitness:16.1101433522006 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:24.695 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.1101433522006, number of tests: 6, total length: 101
[MASTER] 05:56:26.912 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330245230874283 - fitness:16.11013877887889 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:26.912 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11013877887889, number of tests: 6, total length: 101
[MASTER] 05:56:33.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330247502202646 - fitness:16.110137284927674 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 05:56:33.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110137284927674, number of tests: 6, total length: 100
[MASTER] 05:56:45.091 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330251977484899 - fitness:16.11013434134197 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:56:45.091 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11013434134197, number of tests: 7, total length: 126
[MASTER] 05:56:51.565 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330261730835018 - fitness:16.11012792615132 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:56:51.565 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11012792615132, number of tests: 6, total length: 100
[MASTER] 05:56:57.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330277079986807 - fitness:16.11011783038593 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:56:57.918 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11011783038593, number of tests: 6, total length: 99
[MASTER] 05:57:08.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330319768235096 - fitness:16.110089752709918 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:57:08.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.110089752709918, number of tests: 6, total length: 99
[MASTER] 05:57:42.193 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.330474433601047 - fitness:16.109988025075168 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:57:42.193 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.109988025075168, number of tests: 6, total length: 99
[MASTER] 05:57:47.291 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.330384366419455 - fitness:14.97817988988007 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:57:47.291 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.97817988988007, number of tests: 6, total length: 100
[MASTER] 05:57:59.378 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.330384724439092 - fitness:14.978179715542431 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:57:59.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.978179715542431, number of tests: 6, total length: 100
[MASTER] 05:58:02.108 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.330385080649787 - fitness:14.978179542085666 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:58:02.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.978179542085666, number of tests: 6, total length: 99
[MASTER] 05:58:06.990 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.330385435065214 - fitness:14.978179369503115 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:58:06.990 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.978179369503115, number of tests: 5, total length: 95
[MASTER] 05:58:12.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.330386138564286 - fitness:14.978179026934347 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:58:12.872 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.978179026934347, number of tests: 5, total length: 94
[MASTER] 05:58:19.875 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.33038683504299 - fitness:14.978178687784181 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:58:19.875 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.978178687784181, number of tests: 5, total length: 94
[MASTER] 05:58:28.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.330441155378747 - fitness:14.97815223660901 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 05:58:28.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.97815223660901, number of tests: 5, total length: 95
[MASTER] 05:58:36.514 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.330441155378747 - fitness:13.97815223660901 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:58:36.514 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.97815223660901, number of tests: 5, total length: 107
[MASTER] 05:58:40.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.329535627012895 - fitness:13.123872857387106 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:58:40.749 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.123872857387106, number of tests: 5, total length: 112
[MASTER] 05:58:44.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.330436494802669 - fitness:13.123535034218225 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 4
[MASTER] 05:58:44.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.123535034218225, number of tests: 5, total length: 112
[MASTER] 05:58:48.316 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.330451721351197 - fitness:13.123529324620907 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:58:48.316 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.123529324620907, number of tests: 5, total length: 106
[MASTER] 05:58:52.948 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.330452985752938 - fitness:13.123528850500492 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:58:52.948 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.123528850500492, number of tests: 5, total length: 107
[MASTER] 05:58:53.064 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.330486744480403 - fitness:13.123516191812184 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:58:53.064 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.123516191812184, number of tests: 5, total length: 111
[MASTER] 05:58:57.045 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.330482731895785 - fitness:12.455393699261172 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:58:57.045 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.455393699261172, number of tests: 5, total length: 106
[MASTER] 05:59:03.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.330484365565628 - fitness:12.4553932130595 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:59:03.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.4553932130595, number of tests: 5, total length: 107
[MASTER] 05:59:04.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.330485176728903 - fitness:12.455392971646653 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:59:04.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.455392971646653, number of tests: 6, total length: 143
[MASTER] 05:59:07.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.330496909582006 - fitness:12.455389479797812 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 05:59:07.691 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.455389479797812, number of tests: 5, total length: 106
[MASTER] 05:59:44.621 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 2132 generations, 1908098 statements, best individuals have fitness: 12.455389479797812
[MASTER] 05:59:44.623 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 91%
* Total number of goals: 45
* Number of covered goals: 41
* Generated 5 tests with total length 105
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ZeroFitness :                     12 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 05:59:44.774 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:59:44.802 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 81 
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 05:59:44.872 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 05:59:44.873 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 45
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'PosixParser_ESTest' to evosuite-tests
* Done!

* Computation finished
