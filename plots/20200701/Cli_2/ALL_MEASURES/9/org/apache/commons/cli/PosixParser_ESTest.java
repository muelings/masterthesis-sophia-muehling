/*
 * This file was automatically generated by EvoSuite
 * Tue Jun 30 04:31:26 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class PosixParser_ESTest extends PosixParser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      PosixParser posixParser0 = new PosixParser();
      Options options0 = new Options();
      String[] stringArray0 = new String[6];
      stringArray0[0] = "uM8h[Zq:@R{pk^";
      stringArray0[1] = ",V6";
      stringArray0[2] = "uM8h[Zq:@R{pk^";
      stringArray0[3] = "u1^+=(C?LM;%bRKl";
      stringArray0[4] = "-A8^DP.L6n{.x";
      stringArray0[5] = "org.apache.commons.cli.PosixParser";
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.apache.commons.cli.UnrecognizedOptionException : Unrecognized option: -A8^DP.L6n{.x
      try { 
        posixParser0.parse(options0, stringArray0, false);
        fail("Expecting exception: Exception");
      
      } catch(Exception e) {
         //
         // Unrecognized option: -A8^DP.L6n{.x
         //
         verifyException("org.apache.commons.cli.Parser", e);
         assertTrue(e.getMessage().equals("Unrecognized option: -A8^DP.L6n{.x"));   
      }
  }
}
