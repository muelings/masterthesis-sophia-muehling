/*
 * This file was automatically generated by EvoSuite
 * Tue Jun 30 04:26:10 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class PosixParser_ESTest extends PosixParser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      PosixParser posixParser0 = new PosixParser();
      Options options0 = new Options();
      String[] stringArray0 = new String[5];
      stringArray0[0] = "-4=Xnn475k}RWSc#?/-";
      stringArray0[1] = "-4=Xnn475k}RWSc#?/-";
      stringArray0[2] = "-4=Xnn475k}RWSc#?/-";
      stringArray0[3] = "-4=Xnn475k}RWSc#?/-";
      stringArray0[4] = "-4=Xnn475k}RWSc#?/-";
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.apache.commons.cli.UnrecognizedOptionException : Unrecognized option: -4=Xnn475k}RWSc#?/-
      // Modified Version:
      //     org.apache.commons.cli.UnrecognizedOptionException : Unrecognized option: -4
      try { 
        posixParser0.parse(options0, stringArray0);
        fail("Expecting exception: Exception");
      
      } catch(Exception e) {
         //
         // Unrecognized option: -4=Xnn475k}RWSc#?/-
         //
         verifyException("org.apache.commons.cli.Parser", e);
         assertTrue(e.getMessage().equals("Unrecognized option: -4=Xnn475k}RWSc#?/-"));   
      }
  }
}
