* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 10420
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 01:39:42.851 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 01:39:42.857 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 38
* Using seed 1593387577801
* Starting evolution
[MASTER] 01:39:44.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:113.0 - branchDistance:0.0 - coverage:24.0 - ex: 0 - tex: 16
[MASTER] 01:39:44.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.0, number of tests: 10, total length: 189
[MASTER] 01:39:45.235 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:61.92059873594828 - branchDistance:0.0 - coverage:16.920598735948282 - ex: 0 - tex: 18
[MASTER] 01:39:45.235 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.92059873594828, number of tests: 9, total length: 232
[MASTER] 01:39:50.273 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:56.96585012210012 - branchDistance:0.0 - coverage:11.965850122100122 - ex: 0 - tex: 16
[MASTER] 01:39:50.273 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.96585012210012, number of tests: 8, total length: 184
[MASTER] 01:39:50.306 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:56.77777777777778 - branchDistance:0.0 - coverage:11.777777777777779 - ex: 0 - tex: 14
[MASTER] 01:39:50.306 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.77777777777778, number of tests: 8, total length: 138
[MASTER] 01:39:50.763 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:53.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 12
[MASTER] 01:39:50.763 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.0, number of tests: 8, total length: 172
[MASTER] 01:39:51.407 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:52.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 12
[MASTER] 01:39:51.407 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.0, number of tests: 8, total length: 149
[MASTER] 01:39:51.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:51.96200396825397 - branchDistance:0.0 - coverage:6.962003968253969 - ex: 0 - tex: 14
[MASTER] 01:39:51.929 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.96200396825397, number of tests: 9, total length: 186
[MASTER] 01:39:51.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:35.88567810102764 - branchDistance:0.0 - coverage:12.885678101027645 - ex: 0 - tex: 14
[MASTER] 01:39:51.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.88567810102764, number of tests: 8, total length: 193
[MASTER] 01:39:53.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:32.92823129251701 - branchDistance:0.0 - coverage:9.928231292517008 - ex: 0 - tex: 14
[MASTER] 01:39:53.258 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.92823129251701, number of tests: 8, total length: 184
[MASTER] 01:39:53.364 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:30.77777777777778 - branchDistance:0.0 - coverage:7.777777777777778 - ex: 0 - tex: 12
[MASTER] 01:39:53.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.77777777777778, number of tests: 8, total length: 135
[MASTER] 01:39:53.591 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:29.995565410199557 - branchDistance:0.0 - coverage:6.995565410199557 - ex: 0 - tex: 10
[MASTER] 01:39:53.591 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.995565410199557, number of tests: 8, total length: 123
[MASTER] 01:39:53.953 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:28.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 14
[MASTER] 01:39:53.953 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.0, number of tests: 9, total length: 156
[MASTER] 01:39:56.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:26.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 12
[MASTER] 01:39:56.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.0, number of tests: 9, total length: 156
[MASTER] 01:39:58.654 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:25.99815668202765 - branchDistance:0.0 - coverage:2.9981566820276497 - ex: 0 - tex: 12
[MASTER] 01:39:58.654 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 25.99815668202765, number of tests: 9, total length: 157
[MASTER] 01:39:59.212 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:25.997245179063363 - branchDistance:0.0 - coverage:2.997245179063361 - ex: 0 - tex: 12
[MASTER] 01:39:59.212 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 25.997245179063363, number of tests: 9, total length: 159
[MASTER] 01:39:59.604 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:25.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 14
[MASTER] 01:39:59.604 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 25.0, number of tests: 10, total length: 167
[MASTER] 01:40:03.650 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:23.6 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 12
[MASTER] 01:40:03.650 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.6, number of tests: 9, total length: 141
[MASTER] 01:40:04.040 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:23.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 01:40:04.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.0, number of tests: 10, total length: 167
[MASTER] 01:40:04.305 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:20.6 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 14
[MASTER] 01:40:04.305 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 20.6, number of tests: 10, total length: 170
[MASTER] 01:40:07.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:18.6 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:40:07.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.6, number of tests: 10, total length: 162
[MASTER] 01:40:20.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:15.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:40:20.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.666666666666666, number of tests: 9, total length: 126
[MASTER] 01:40:28.648 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:13.571428571428571 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:40:28.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.571428571428571, number of tests: 8, total length: 119
[MASTER] 01:40:36.419 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:12.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:40:36.419 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.0, number of tests: 8, total length: 120
[MASTER] 01:40:44.980 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:10.777777777777777 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:40:44.980 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.777777777777777, number of tests: 8, total length: 111
[MASTER] 01:40:54.186 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.0 - fitness:9.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:40:54.186 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.0, number of tests: 8, total length: 107
[MASTER] 01:41:09.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.0 - fitness:8.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:41:09.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333333333333332, number of tests: 8, total length: 95
[MASTER] 01:41:47.116 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.0 - fitness:7.76923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:41:47.116 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.76923076923077, number of tests: 8, total length: 71
[MASTER] 01:42:00.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.0 - fitness:7.285714285714286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:42:00.905 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.285714285714286, number of tests: 8, total length: 72
[MASTER] 01:42:30.704 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.0 - fitness:6.866666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:42:30.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.866666666666666, number of tests: 8, total length: 67
[MASTER] 01:42:35.183 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.0 - fitness:6.5 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:42:35.183 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5, number of tests: 9, total length: 76
[MASTER] 01:42:42.542 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.0 - fitness:6.176470588235294 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:42:42.542 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.176470588235294, number of tests: 8, total length: 74
[MASTER] 01:42:48.971 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.0 - fitness:5.888888888888888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:42:48.971 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888888888888888, number of tests: 8, total length: 73
[MASTER] 01:42:51.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.0 - fitness:5.631578947368421 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:42:51.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.631578947368421, number of tests: 8, total length: 78
[MASTER] 01:42:59.498 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.0 - fitness:5.4 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:42:59.498 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.4, number of tests: 8, total length: 73
[MASTER] 01:43:06.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.0 - fitness:5.19047619047619 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:43:06.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.19047619047619, number of tests: 8, total length: 77
[MASTER] 01:43:38.477 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.0 - fitness:5.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:43:38.478 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.0, number of tests: 9, total length: 91
[MASTER] 01:43:52.079 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.0 - fitness:4.826086956521739 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:43:52.079 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.826086956521739, number of tests: 8, total length: 70
[MASTER] 01:44:00.915 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.0 - fitness:4.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:00.915 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.666666666666666, number of tests: 8, total length: 70
[MASTER] 01:44:12.746 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.0 - fitness:4.52 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:12.746 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.52, number of tests: 8, total length: 71
[MASTER] 01:44:31.220 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.0 - fitness:4.384615384615385 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 01:44:31.220 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.384615384615385, number of tests: 9, total length: 77
[MASTER] 01:44:34.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.0 - fitness:4.2592592592592595 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 01:44:34.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.2592592592592595, number of tests: 8, total length: 71

[MASTER] 01:44:43.884 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 753 generations, 401932 statements, best individuals have fitness: 4.2592592592592595
[MASTER] 01:44:43.888 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 38
* Number of covered goals: 38
* Generated 8 tests with total length 70
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                      4 / 0           
	- RMIStoppingCondition
	- MaxTime :                        302 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 01:44:44.924 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:44:44.979 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 52 
[MASTER] 01:44:45.060 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 01:44:45.061 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 01:44:45.062 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 01:44:45.064 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 01:44:45.065 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 01:44:45.066 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 01:44:45.066 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 01:44:45.066 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 01:44:45.069 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 01:44:45.069 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 01:44:45.070 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 01:44:45.071 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 01:44:45.073 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 01:44:45.073 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 01:44:45.075 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 5
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
