/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 29 23:30:26 GMT 2020
 */

package org.apache.commons.lang3.math;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.lang3.math.NumberUtils;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class NumberUtils_ESTest extends NumberUtils_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     java.lang.NumberFormatException : For input string: "Y7"
      // Modified Version:
      //     java.lang.NumberFormatException : For input string: "-Y7`%NW;cz7gM9}=c"
      try { 
        NumberUtils.createNumber("-#Y7`%NW;cz7gM9}=c");
        fail("Expecting exception: NumberFormatException");
      
      } catch(NumberFormatException e) {
         //
         // For input string: \"Y7\"
         //
         verifyException("java.lang.NumberFormatException", e);
         assertTrue(e.getMessage().equals("For input string: \"Y7\""));   
      }
  }
}
