* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.map.MultiValueMap
* Starting client
* Connecting to master process on port 7417
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_27_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.map.MultiValueMap
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:44:10.968 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:44:10.974 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 87
* Using seed 1593398647557
* Starting evolution
[MASTER] 04:45:13.427 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:340.0 - branchDistance:0.0 - coverage:165.0 - ex: 0 - tex: 4
[MASTER] 04:45:13.427 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 340.0, number of tests: 2, total length: 39
[MASTER] 04:45:14.199 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:316.0 - branchDistance:0.0 - coverage:141.0 - ex: 0 - tex: 4
[MASTER] 04:45:14.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 316.0, number of tests: 3, total length: 59
[MASTER] 04:45:14.281 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:310.0 - branchDistance:0.0 - coverage:135.0 - ex: 0 - tex: 8
[MASTER] 04:45:14.281 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 310.0, number of tests: 5, total length: 157
[MASTER] 04:45:14.430 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:279.0 - branchDistance:0.0 - coverage:104.0 - ex: 0 - tex: 10
[MASTER] 04:45:14.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 279.0, number of tests: 10, total length: 212
[MASTER] 04:45:14.575 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:273.33333333333337 - branchDistance:0.0 - coverage:98.33333333333334 - ex: 0 - tex: 10
[MASTER] 04:45:14.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 273.33333333333337, number of tests: 6, total length: 162
[MASTER] 04:45:15.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.33333333333337 - branchDistance:0.0 - coverage:94.33333333333334 - ex: 0 - tex: 8
[MASTER] 04:45:15.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.33333333333337, number of tests: 8, total length: 188
[MASTER] 04:45:15.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:259.3333333333333 - branchDistance:0.0 - coverage:84.33333333333333 - ex: 0 - tex: 8
[MASTER] 04:45:15.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 259.3333333333333, number of tests: 10, total length: 191
[MASTER] 04:45:17.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:247.33333333333331 - branchDistance:0.0 - coverage:72.33333333333333 - ex: 0 - tex: 8
[MASTER] 04:45:17.314 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 247.33333333333331, number of tests: 10, total length: 248
[MASTER] 04:45:27.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:246.33333333333331 - branchDistance:0.0 - coverage:71.33333333333333 - ex: 0 - tex: 10
[MASTER] 04:45:27.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 246.33333333333331, number of tests: 10, total length: 235
[MASTER] 04:45:31.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.66666666666666 - branchDistance:0.0 - coverage:62.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:45:31.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.66666666666666, number of tests: 11, total length: 279
[MASTER] 04:45:38.631 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:236.66666666666666 - branchDistance:0.0 - coverage:61.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:45:38.631 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 236.66666666666666, number of tests: 11, total length: 304
[MASTER] 04:45:41.161 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:235.66666666666666 - branchDistance:0.0 - coverage:60.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:45:41.161 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 235.66666666666666, number of tests: 12, total length: 298
[MASTER] 04:45:45.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.66666666666666 - branchDistance:0.0 - coverage:58.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:45:45.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.66666666666666, number of tests: 12, total length: 315
[MASTER] 04:45:52.550 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.16666666666666 - branchDistance:0.0 - coverage:57.16666666666666 - ex: 0 - tex: 14
[MASTER] 04:45:52.550 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.16666666666666, number of tests: 12, total length: 339
[MASTER] 04:46:05.607 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:231.16666666666666 - branchDistance:0.0 - coverage:56.16666666666666 - ex: 0 - tex: 12
[MASTER] 04:46:05.607 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 231.16666666666666, number of tests: 12, total length: 335
[MASTER] 04:46:07.964 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:228.66666666666666 - branchDistance:0.0 - coverage:53.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:46:07.964 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 228.66666666666666, number of tests: 13, total length: 339
[MASTER] 04:46:11.715 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:228.16666666666666 - branchDistance:0.0 - coverage:53.16666666666666 - ex: 0 - tex: 12
[MASTER] 04:46:11.715 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 228.16666666666666, number of tests: 12, total length: 341
[MASTER] 04:46:20.078 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.83333333333331 - branchDistance:0.0 - coverage:50.83333333333333 - ex: 0 - tex: 14
[MASTER] 04:46:20.078 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.83333333333331, number of tests: 12, total length: 340
[MASTER] 04:46:36.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.66666666666666 - branchDistance:0.0 - coverage:50.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:46:36.113 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.66666666666666, number of tests: 12, total length: 338
[MASTER] 04:46:37.296 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:224.83333333333331 - branchDistance:0.0 - coverage:49.83333333333333 - ex: 0 - tex: 14
[MASTER] 04:46:37.296 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 224.83333333333331, number of tests: 13, total length: 368
[MASTER] 04:46:41.389 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.83333333333331 - branchDistance:0.0 - coverage:48.83333333333333 - ex: 0 - tex: 14
[MASTER] 04:46:41.389 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.83333333333331, number of tests: 14, total length: 374
[MASTER] 04:46:58.075 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:222.83333333333331 - branchDistance:0.0 - coverage:47.83333333333333 - ex: 0 - tex: 12
[MASTER] 04:46:58.075 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 222.83333333333331, number of tests: 13, total length: 367
[MASTER] 04:47:21.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:222.66666666666666 - branchDistance:0.0 - coverage:47.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:47:21.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 222.66666666666666, number of tests: 14, total length: 381
[MASTER] 04:47:22.660 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:221.83333333333331 - branchDistance:0.0 - coverage:46.83333333333333 - ex: 0 - tex: 12
[MASTER] 04:47:22.660 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 221.83333333333331, number of tests: 14, total length: 403
[MASTER] 04:47:25.518 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:221.66666666666666 - branchDistance:0.0 - coverage:46.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:47:25.518 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 221.66666666666666, number of tests: 13, total length: 385
[MASTER] 04:47:32.967 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:221.33333333333331 - branchDistance:0.0 - coverage:46.33333333333333 - ex: 0 - tex: 12
[MASTER] 04:47:32.967 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 221.33333333333331, number of tests: 13, total length: 380
[MASTER] 04:47:38.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:220.83333333333331 - branchDistance:0.0 - coverage:45.83333333333333 - ex: 0 - tex: 10
[MASTER] 04:47:38.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 220.83333333333331, number of tests: 13, total length: 399
[MASTER] 04:47:39.958 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:220.33333333333331 - branchDistance:0.0 - coverage:45.33333333333333 - ex: 0 - tex: 12
[MASTER] 04:47:39.958 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 220.33333333333331, number of tests: 14, total length: 414
[MASTER] 04:47:51.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.83333333333331 - branchDistance:0.0 - coverage:44.83333333333333 - ex: 0 - tex: 14
[MASTER] 04:47:51.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.83333333333331, number of tests: 15, total length: 442
[MASTER] 04:48:02.596 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:218.66666666666666 - branchDistance:0.0 - coverage:43.66666666666666 - ex: 0 - tex: 16
[MASTER] 04:48:02.596 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 218.66666666666666, number of tests: 16, total length: 467
[MASTER] 04:49:12.092 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 65 generations, 85116 statements, best individuals have fitness: 218.66666666666666
[MASTER] 04:49:12.101 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 72%
* Total number of goals: 61
* Number of covered goals: 44
* Generated 14 tests with total length 387
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                    219 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 04:49:12.647 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:49:12.704 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 345 
[MASTER] 04:49:12.857 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 04:49:12.857 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 04:49:12.857 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 04:49:12.857 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:49:12.857 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
* Coverage of criterion REGRESSION: 0%
[MASTER] 04:49:12.858 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
* Total number of goals: 81
* Number of covered goals: 0
[MASTER] 04:49:12.859 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 04:49:12.859 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
* Generated 0 tests with total length 0
[MASTER] 04:49:12.859 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
* Resulting test suite's coverage: 0%
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
* Compiling and checking tests
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 04:49:12.860 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 04:49:12.861 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Writing JUnit test case 'MultiValueMap_ESTest' to evosuite-tests
* Done!

* Computation finished
