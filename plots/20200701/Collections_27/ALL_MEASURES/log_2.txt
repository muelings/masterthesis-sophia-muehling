* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.map.MultiValueMap
* Starting client
* Connecting to master process on port 14592
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_27_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.map.MultiValueMap
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:22:08.728 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:22:08.735 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 87
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593393725046
* Starting evolution
[MASTER] 03:23:06.486 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:349.0 - branchDistance:0.0 - coverage:174.0 - ex: 0 - tex: 10
[MASTER] 03:23:06.487 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 349.0, number of tests: 5, total length: 143
[MASTER] 03:23:06.745 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:287.33333333333337 - branchDistance:0.0 - coverage:112.33333333333334 - ex: 0 - tex: 12
[MASTER] 03:23:06.745 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 287.33333333333337, number of tests: 10, total length: 139
[MASTER] 03:23:06.854 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.33333333333337 - branchDistance:0.0 - coverage:95.33333333333334 - ex: 0 - tex: 10
[MASTER] 03:23:06.854 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.33333333333337, number of tests: 9, total length: 210
[MASTER] 03:23:08.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:256.33333333333337 - branchDistance:0.0 - coverage:81.33333333333334 - ex: 0 - tex: 18
[MASTER] 03:23:08.479 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 256.33333333333337, number of tests: 10, total length: 285
[MASTER] 03:23:16.341 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:154.83333333333334 - branchDistance:0.0 - coverage:81.33333333333334 - ex: 0 - tex: 18
[MASTER] 03:23:16.341 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.83333333333334, number of tests: 10, total length: 285
[MASTER] 03:23:25.550 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:150.33333333333334 - branchDistance:0.0 - coverage:76.83333333333334 - ex: 0 - tex: 10
[MASTER] 03:23:25.550 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 150.33333333333334, number of tests: 8, total length: 238
[MASTER] 03:23:32.397 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:146.66666666666666 - branchDistance:0.0 - coverage:73.16666666666666 - ex: 0 - tex: 8
[MASTER] 03:23:32.397 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.66666666666666, number of tests: 8, total length: 214
[MASTER] 03:23:47.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:145.33333333333334 - branchDistance:0.0 - coverage:71.83333333333334 - ex: 0 - tex: 12
[MASTER] 03:23:47.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 145.33333333333334, number of tests: 9, total length: 272
[MASTER] 03:23:55.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:144.83333333333334 - branchDistance:0.0 - coverage:71.33333333333334 - ex: 0 - tex: 10
[MASTER] 03:23:55.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.83333333333334, number of tests: 9, total length: 227
[MASTER] 03:23:59.000 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:143.66666666666666 - branchDistance:0.0 - coverage:70.16666666666666 - ex: 0 - tex: 6
[MASTER] 03:23:59.000 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.66666666666666, number of tests: 8, total length: 196
[MASTER] 03:24:06.257 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:143.16666666666666 - branchDistance:0.0 - coverage:69.66666666666666 - ex: 0 - tex: 6
[MASTER] 03:24:06.257 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.16666666666666, number of tests: 8, total length: 184
[MASTER] 03:24:06.436 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:142.16666666666666 - branchDistance:0.0 - coverage:68.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:24:06.436 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.16666666666666, number of tests: 9, total length: 221
[MASTER] 03:24:08.396 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:141.16666666666666 - branchDistance:0.0 - coverage:67.66666666666666 - ex: 0 - tex: 8
[MASTER] 03:24:08.396 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.16666666666666, number of tests: 9, total length: 224
[MASTER] 03:24:09.617 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:140.66666666666666 - branchDistance:0.0 - coverage:67.16666666666666 - ex: 0 - tex: 16
[MASTER] 03:24:09.617 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.66666666666666, number of tests: 10, total length: 317
[MASTER] 03:24:17.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:139.66666666666666 - branchDistance:0.0 - coverage:66.16666666666666 - ex: 0 - tex: 14
[MASTER] 03:24:17.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.66666666666666, number of tests: 10, total length: 313
[MASTER] 03:24:19.533 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:138.16666666666666 - branchDistance:0.0 - coverage:64.66666666666666 - ex: 0 - tex: 6
[MASTER] 03:24:19.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.16666666666666, number of tests: 10, total length: 201
[MASTER] 03:24:22.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:137.66666666666666 - branchDistance:0.0 - coverage:64.16666666666666 - ex: 0 - tex: 8
[MASTER] 03:24:22.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.66666666666666, number of tests: 10, total length: 227
[MASTER] 03:24:26.592 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:136.66666666666666 - branchDistance:0.0 - coverage:63.16666666666666 - ex: 0 - tex: 12
[MASTER] 03:24:26.592 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.66666666666666, number of tests: 10, total length: 261
[MASTER] 03:24:37.912 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:135.66666666666666 - branchDistance:0.0 - coverage:62.16666666666666 - ex: 0 - tex: 10
[MASTER] 03:24:37.912 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.66666666666666, number of tests: 10, total length: 255
[MASTER] 03:24:44.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:134.66666666666666 - branchDistance:0.0 - coverage:61.16666666666666 - ex: 0 - tex: 10
[MASTER] 03:24:44.134 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.66666666666666, number of tests: 11, total length: 258
[MASTER] 03:24:47.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:133.66666666666666 - branchDistance:0.0 - coverage:60.16666666666666 - ex: 0 - tex: 10
[MASTER] 03:24:47.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.66666666666666, number of tests: 12, total length: 265
[MASTER] 03:24:51.475 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:133.16666666666666 - branchDistance:0.0 - coverage:59.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:24:51.475 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.16666666666666, number of tests: 10, total length: 256
[MASTER] 03:25:04.235 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:132.16666666666666 - branchDistance:0.0 - coverage:58.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:25:04.235 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.16666666666666, number of tests: 11, total length: 280
[MASTER] 03:25:20.777 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:131.16666666666666 - branchDistance:0.0 - coverage:57.66666666666666 - ex: 0 - tex: 8
[MASTER] 03:25:20.777 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.16666666666666, number of tests: 11, total length: 250
[MASTER] 03:25:40.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:130.16666666666666 - branchDistance:0.0 - coverage:56.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:25:40.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.16666666666666, number of tests: 11, total length: 256
[MASTER] 03:25:56.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:129.16666666666666 - branchDistance:0.0 - coverage:55.66666666666666 - ex: 0 - tex: 8
[MASTER] 03:25:56.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.16666666666666, number of tests: 12, total length: 274
[MASTER] 03:26:17.956 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:127.16666666666666 - branchDistance:0.0 - coverage:53.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:26:17.957 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.16666666666666, number of tests: 12, total length: 284
[MASTER] 03:26:32.369 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:126.16666666666666 - branchDistance:0.0 - coverage:52.66666666666666 - ex: 0 - tex: 12
[MASTER] 03:26:32.369 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.16666666666666, number of tests: 13, total length: 309
[MASTER] 03:26:41.057 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.16666666666666 - branchDistance:0.0 - coverage:51.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:26:41.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.16666666666666, number of tests: 13, total length: 318
[MASTER] 03:26:41.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.16666666666666 - branchDistance:0.0 - coverage:50.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:26:41.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.16666666666666, number of tests: 12, total length: 294
[MASTER] 03:26:53.392 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:123.83333333333333 - branchDistance:0.0 - coverage:50.33333333333333 - ex: 0 - tex: 12
[MASTER] 03:26:53.392 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.83333333333333, number of tests: 13, total length: 316
[MASTER] 03:26:58.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:123.16666666666666 - branchDistance:0.0 - coverage:49.66666666666666 - ex: 0 - tex: 12
[MASTER] 03:26:58.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.16666666666666, number of tests: 13, total length: 318
[MASTER] 03:27:07.663 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.16666666666666 - branchDistance:0.0 - coverage:46.66666666666666 - ex: 0 - tex: 10
[MASTER] 03:27:07.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.16666666666666, number of tests: 12, total length: 292
[MASTER] 03:27:10.024 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 61 generations, 73604 statements, best individuals have fitness: 120.16666666666666
[MASTER] 03:27:10.028 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 70%
* Total number of goals: 61
* Number of covered goals: 43
* Generated 12 tests with total length 292
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ZeroFitness :                    120 / 0           
[MASTER] 03:27:10.634 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:27:10.690 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 252 
* Going to analyze the coverage criteria
[MASTER] 03:27:10.852 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 03:27:10.852 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 03:27:10.853 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 03:27:10.853 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 03:27:10.853 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 03:27:10.853 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:27:10.853 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:27:10.853 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 03:27:10.854 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 03:27:10.857 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 81
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiValueMap_ESTest' to evosuite-tests
* Done!

* Computation finished
