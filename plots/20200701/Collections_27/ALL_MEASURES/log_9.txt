* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.map.MultiValueMap
* Starting client
* Connecting to master process on port 15942
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_27_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.map.MultiValueMap
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:33:54.407 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:33:54.414 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 87
* Using seed 1593398030973
* Starting evolution
[MASTER] 04:34:56.415 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:312.3333333333333 - branchDistance:0.0 - coverage:137.33333333333331 - ex: 0 - tex: 4
[MASTER] 04:34:56.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 312.3333333333333, number of tests: 2, total length: 46
[MASTER] 04:34:56.464 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:311.0 - branchDistance:0.0 - coverage:136.0 - ex: 0 - tex: 2
[MASTER] 04:34:56.464 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 311.0, number of tests: 1, total length: 23
[MASTER] 04:34:57.230 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:302.0 - branchDistance:0.0 - coverage:127.0 - ex: 0 - tex: 6
[MASTER] 04:34:57.230 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 302.0, number of tests: 3, total length: 82
[MASTER] 04:34:57.263 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:300.3333333333333 - branchDistance:0.0 - coverage:125.33333333333331 - ex: 0 - tex: 4
[MASTER] 04:34:57.263 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 300.3333333333333, number of tests: 3, total length: 78
[MASTER] 04:34:57.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:281.33333333333337 - branchDistance:0.0 - coverage:106.33333333333334 - ex: 0 - tex: 8
[MASTER] 04:34:57.918 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 281.33333333333337, number of tests: 6, total length: 82
[MASTER] 04:34:57.958 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:279.33333333333337 - branchDistance:0.0 - coverage:104.33333333333334 - ex: 0 - tex: 6
[MASTER] 04:34:57.958 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 279.33333333333337, number of tests: 4, total length: 144
[MASTER] 04:34:58.354 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:273.3333333333333 - branchDistance:0.0 - coverage:98.33333333333333 - ex: 0 - tex: 12
[MASTER] 04:34:58.354 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 273.3333333333333, number of tests: 10, total length: 222
[MASTER] 04:35:06.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.33333333333337 - branchDistance:0.0 - coverage:93.33333333333334 - ex: 0 - tex: 12
[MASTER] 04:35:06.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.33333333333337, number of tests: 10, total length: 239
[MASTER] 04:35:06.554 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:265.33333333333337 - branchDistance:0.0 - coverage:90.33333333333334 - ex: 0 - tex: 12
[MASTER] 04:35:06.554 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 265.33333333333337, number of tests: 8, total length: 205
[MASTER] 04:35:07.726 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:263.33333333333337 - branchDistance:0.0 - coverage:88.33333333333334 - ex: 0 - tex: 8
[MASTER] 04:35:07.726 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 263.33333333333337, number of tests: 6, total length: 197
[MASTER] 04:35:08.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:261.3333333333333 - branchDistance:0.0 - coverage:86.33333333333333 - ex: 0 - tex: 10
[MASTER] 04:35:08.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 261.3333333333333, number of tests: 10, total length: 210
[MASTER] 04:35:08.383 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:256.3333333333333 - branchDistance:0.0 - coverage:81.33333333333333 - ex: 0 - tex: 10
[MASTER] 04:35:08.383 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 256.3333333333333, number of tests: 8, total length: 179
[MASTER] 04:35:09.523 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:251.33333333333331 - branchDistance:0.0 - coverage:76.33333333333333 - ex: 0 - tex: 10
[MASTER] 04:35:09.523 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 251.33333333333331, number of tests: 8, total length: 187
[MASTER] 04:35:17.654 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:249.33333333333331 - branchDistance:0.0 - coverage:74.33333333333333 - ex: 0 - tex: 10
[MASTER] 04:35:17.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 249.33333333333331, number of tests: 8, total length: 195
[MASTER] 04:35:19.842 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:246.33333333333331 - branchDistance:0.0 - coverage:71.33333333333333 - ex: 0 - tex: 10
[MASTER] 04:35:19.842 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 246.33333333333331, number of tests: 9, total length: 199
[MASTER] 04:35:27.522 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.33333333333331 - branchDistance:0.0 - coverage:70.33333333333333 - ex: 0 - tex: 10
[MASTER] 04:35:27.522 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.33333333333331, number of tests: 8, total length: 189
[MASTER] 04:35:30.538 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:239.66666666666666 - branchDistance:0.0 - coverage:64.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:35:30.538 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 239.66666666666666, number of tests: 9, total length: 203
[MASTER] 04:35:44.183 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:238.66666666666666 - branchDistance:0.0 - coverage:63.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:35:44.184 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 238.66666666666666, number of tests: 8, total length: 203
[MASTER] 04:35:57.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.66666666666666 - branchDistance:0.0 - coverage:62.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:35:57.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.66666666666666, number of tests: 9, total length: 223
[MASTER] 04:35:58.995 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:234.66666666666666 - branchDistance:0.0 - coverage:59.66666666666666 - ex: 0 - tex: 10
[MASTER] 04:35:58.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 234.66666666666666, number of tests: 9, total length: 208
[MASTER] 04:36:01.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.66666666666666 - branchDistance:0.0 - coverage:58.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:36:01.406 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.66666666666666, number of tests: 9, total length: 227
[MASTER] 04:36:09.604 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.66666666666666 - branchDistance:0.0 - coverage:57.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:36:09.604 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.66666666666666, number of tests: 9, total length: 239
[MASTER] 04:36:13.207 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:231.66666666666666 - branchDistance:0.0 - coverage:56.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:36:13.207 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 231.66666666666666, number of tests: 8, total length: 218
[MASTER] 04:36:15.657 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:230.66666666666666 - branchDistance:0.0 - coverage:55.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:36:15.657 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 230.66666666666666, number of tests: 10, total length: 238
[MASTER] 04:36:19.818 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:230.16666666666666 - branchDistance:0.0 - coverage:55.16666666666666 - ex: 0 - tex: 12
[MASTER] 04:36:19.819 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 230.16666666666666, number of tests: 9, total length: 228
[MASTER] 04:36:22.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:226.66666666666666 - branchDistance:0.0 - coverage:51.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:36:22.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 226.66666666666666, number of tests: 11, total length: 262
[MASTER] 04:36:37.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.66666666666666 - branchDistance:0.0 - coverage:50.66666666666666 - ex: 0 - tex: 16
[MASTER] 04:36:37.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.66666666666666, number of tests: 11, total length: 284
[MASTER] 04:36:43.468 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.16666666666666 - branchDistance:0.0 - coverage:48.16666666666666 - ex: 0 - tex: 14
[MASTER] 04:36:43.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.16666666666666, number of tests: 11, total length: 252
[MASTER] 04:36:56.668 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.0 - branchDistance:0.0 - coverage:44.0 - ex: 0 - tex: 12
[MASTER] 04:36:56.669 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.0, number of tests: 11, total length: 249
[MASTER] 04:37:01.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:218.0 - branchDistance:0.0 - coverage:43.0 - ex: 0 - tex: 12
[MASTER] 04:37:01.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 218.0, number of tests: 11, total length: 255
[MASTER] 04:37:26.492 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:216.99999999906868 - branchDistance:0.0 - coverage:41.99999999906868 - ex: 0 - tex: 12
[MASTER] 04:37:26.492 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 216.99999999906868, number of tests: 12, total length: 265
[MASTER] 04:37:35.155 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:215.99999999906868 - branchDistance:0.0 - coverage:40.99999999906868 - ex: 0 - tex: 12
[MASTER] 04:37:35.155 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 215.99999999906868, number of tests: 12, total length: 280
[MASTER] 04:38:10.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:214.99999999906868 - branchDistance:0.0 - coverage:39.99999999906868 - ex: 0 - tex: 12
[MASTER] 04:38:10.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 214.99999999906868, number of tests: 11, total length: 262
[MASTER] 04:38:38.133 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:212.0 - branchDistance:0.0 - coverage:37.0 - ex: 0 - tex: 12
[MASTER] 04:38:38.133 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 212.0, number of tests: 11, total length: 265

[MASTER] 04:38:57.501 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 303s and 69 generations, 69776 statements, best individuals have fitness: 212.0
[MASTER] 04:38:57.507 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 77%
* Total number of goals: 61
* Number of covered goals: 47
* Generated 11 tests with total length 253
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                    212 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        303 / 300          Finished!
[MASTER] 04:38:57.992 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:38:58.033 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 223 
[MASTER] 04:38:58.133 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 04:38:58.134 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:38:58.135 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 81
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'MultiValueMap_ESTest' to evosuite-tests
* Done!

* Computation finished
