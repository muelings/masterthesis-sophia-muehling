* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.CommandLine
* Starting client
* Connecting to master process on port 14250
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_1_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.CommandLine
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 32
[MASTER] 22:16:19.142 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:16:19.577 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:16:20.787 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:16:20.806 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 22:16:21.143 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:16:21.158 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:16:21.159 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 4 | Tests with assertion: 1
* Generated 1 tests with total length 13
* GA-Budget:
	- MaxTime :                          2 / 300         
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 22:16:21.593 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:16:21.610 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 22:16:21.624 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 22:16:21.625 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException:resolveOption-165,, hasOption:NullPointerException]
[MASTER] 22:16:21.625 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: hasOption:NullPointerException at 8
[MASTER] 22:16:21.625 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException:resolveOption-165,, hasOption:NullPointerException]
[MASTER] 22:16:21.783 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:16:21.788 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 32
* Number of covered goals: 1
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CommandLine_ESTest' to evosuite-tests
* Done!

* Computation finished
* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.CommandLine
* Starting client
* Connecting to master process on port 6722
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_1_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.CommandLine
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 32
[MASTER] 04:56:54.246 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:56:54.848 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 04:56:55.749 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:56:55.756 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 04:56:55.950 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/CommandLine_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:56:55.956 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 04:56:55.956 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 30 | Tests with assertion: 1
* Generated 1 tests with total length 5
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          1 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 04:56:56.289 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:56:56.295 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 04:56:56.300 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 04:56:56.301 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException:resolveOption-165,, hasOption:NullPointerException]
[MASTER] 04:56:56.301 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: hasOption:NullPointerException at 3
[MASTER] 04:56:56.301 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException:resolveOption-165,, hasOption:NullPointerException]
[MASTER] 04:56:56.338 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
[MASTER] 04:56:56.339 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 32
* Number of covered goals: 1
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CommandLine_ESTest' to evosuite-tests
* Done!

* Computation finished
