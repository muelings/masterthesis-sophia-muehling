/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 29 20:28:49 GMT 2020
 */

package org.jsoup.parser;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jsoup.parser.Parser;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Parser_ESTest extends Parser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.StringIndexOutOfBoundsException : String index out of range: 15
      Parser.parse("~fL<RQBk#2.Z}=", "3^");
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.StringIndexOutOfBoundsException : String index out of range: 13
      Parser.parseBodyFragment("~fL<QCk#2Z}=", "~fL<QCk#2Z}=");
  }

  @Test(timeout = 4000)
  public void test2()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.StringIndexOutOfBoundsException : String index out of range: 14
      Parser.parseBodyFragmentRelaxed("~fL<QCk#2.Z}=", "M");
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Object must not be null
      // Undeclared exception!
      try { 
        Parser.parseBodyFragment((String) null, "");
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Object must not be null
         //
         verifyException("org.jsoup.helper.Validate", e);
         assertTrue(e.getMessage().equals("Object must not be null"));   
      }
  }
}
