/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 29 20:23:18 GMT 2020
 */

package org.jsoup.parser;

import org.junit.Test;
import static org.junit.Assert.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jsoup.parser.Parser;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Parser_ESTest extends Parser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.StringIndexOutOfBoundsException : String index out of range: 19
      Parser.parseBodyFragment("I1YU<PbIj/=&?7Axfe|", "8n");
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.StringIndexOutOfBoundsException : String index out of range: 11
      Parser.parse("lYYm<Lu=_cD", "Em^~[|(:Ta");
  }

  @Test(timeout = 4000)
  public void test2()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.StringIndexOutOfBoundsException : String index out of range: 13
      Parser.parseBodyFragmentRelaxed("<1}dE<g0@=ehF", "erlq");
  }
}
