* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting client
* Connecting to master process on port 9766
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 100
[MASTER] 22:28:54.860 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:28:58.711 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:29:00.182 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:29:00.198 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 22:29:00.514 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 22:29:00.528 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 22:29:00.528 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 455 | Tests with assertion: 1
* Generated 1 tests with total length 33
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          5 / 300         
[MASTER] 22:29:01.357 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:29:01.373 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 14 
[MASTER] 22:29:01.382 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 22:29:01.382 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parse:StringIndexOutOfBoundsException]
[MASTER] 22:29:01.382 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parse:StringIndexOutOfBoundsException at 6
[MASTER] 22:29:01.382 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:StringIndexOutOfBoundsException]
[MASTER] 22:29:01.538 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:29:01.544 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 45%
* Total number of goals: 100
* Number of covered goals: 45
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
