* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 10091
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 128
[MASTER] 12:57:06.849 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:57:07.894 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.remaining());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.position());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.length());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.remaining());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.position());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.length());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(43, var1.position());  // (Inspector) Original Value: 43 | Regression Value: 47
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1504, var1.length());  // (Inspector) Original Value: 1504 | Regression Value: 1500
[MASTER] 12:57:07.895 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1504, var1.remaining());  // (Inspector) Original Value: 1504 | Regression Value: 1500
[MASTER] 12:57:07.931 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:57:07.931 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 9
[MASTER] 12:57:08.960 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.remaining());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.position());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.length());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.remaining());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.position());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.length());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(43, var1.position());  // (Inspector) Original Value: 43 | Regression Value: 47
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1504, var1.length());  // (Inspector) Original Value: 1504 | Regression Value: 1500
[MASTER] 12:57:08.998 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1504, var1.remaining());  // (Inspector) Original Value: 1504 | Regression Value: 1500
[MASTER] 12:57:09.000 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:57:09.000 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 9
Generated test with 9 assertions.
[MASTER] 12:57:09.278 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.remaining());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.position());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.length());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.remaining());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.position());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1547, var1.length());  // (Inspector) Original Value: 1547 | Regression Value: 1543
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(43, var1.position());  // (Inspector) Original Value: 43 | Regression Value: 47
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1504, var1.length());  // (Inspector) Original Value: 1504 | Regression Value: 1500
[MASTER] 12:57:09.299 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1504, var1.remaining());  // (Inspector) Original Value: 1504 | Regression Value: 1500
[MASTER] 12:57:09.301 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:57:09.301 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 9
[MASTER] 12:57:09.301 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 9 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 15 | Tests with assertion: 1
* Generated 1 tests with total length 17
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:57:10.034 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:57:10.053 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 12 
[MASTER] 12:57:10.072 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[remaining, position, length]}
[MASTER] 12:57:10.086 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 12:57:10.086 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 12:57:10.090 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 128
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
