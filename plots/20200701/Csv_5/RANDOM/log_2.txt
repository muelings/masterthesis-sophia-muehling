* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 20352
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 128
[MASTER] 12:46:15.843 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:46:25.517 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 12:46:25.517 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 12:46:25.517 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 12:46:25.517 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnullnull
[MASTER] 12:46:25.526 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:46:25.526 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 12:46:26.807 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:46:26.819 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 12:46:26.819 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 12:46:26.820 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 12:46:26.820 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnullnull
[MASTER] 12:46:26.822 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:46:26.822 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
Generated test with 4 assertions.
[MASTER] 12:46:27.081 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:46:27.093 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 12:46:27.093 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 12:46:27.093 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 12:46:27.093 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var0.toString());  // (Inspector) Original Value:  | Regression Value: nullnullnull
[MASTER] 12:46:27.095 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 12:46:27.095 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
Keeping 4 assertions.
[MASTER] 12:46:27.096 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 384 | Tests with assertion: 1
* Generated 1 tests with total length 29
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                         11 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:46:27.785 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:46:27.798 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 21 
[MASTER] 12:46:27.815 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[toString]}
[MASTER] 12:46:27.826 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 12:46:27.826 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 12:46:27.829 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 128
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 18
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
