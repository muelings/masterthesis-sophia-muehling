* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 5955
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 128
[MASTER] 13:24:07.449 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:24:10.396 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 8
[MASTER] 13:24:10.396 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 13:24:10.396 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(87, var1.size());  // (Inspector) Original Value: 87 | Regression Value: 103
[MASTER] 13:24:10.396 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(175, var1.size());  // (Inspector) Original Value: 175 | Regression Value: 195
[MASTER] 13:24:10.396 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(262, var1.size());  // (Inspector) Original Value: 262 | Regression Value: 290
[MASTER] 13:24:10.418 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 13:24:10.418 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 5
[MASTER] 13:24:11.520 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 13:24:11.542 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 8
[MASTER] 13:24:11.542 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 13:24:11.542 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(87, var1.size());  // (Inspector) Original Value: 87 | Regression Value: 103
[MASTER] 13:24:11.542 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(175, var1.size());  // (Inspector) Original Value: 175 | Regression Value: 195
[MASTER] 13:24:11.542 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(262, var1.size());  // (Inspector) Original Value: 262 | Regression Value: 290
[MASTER] 13:24:11.545 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 13:24:11.545 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 5
[MASTER] 13:24:23.477 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(132, var0.size());  // (Inspector) Original Value: 132 | Regression Value: 144
[MASTER] 13:24:23.478 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 13:24:23.478 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:24:23.726 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 13:24:23.731 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(132, var0.size());  // (Inspector) Original Value: 132 | Regression Value: 144
Generated test with 1 assertions.
[MASTER] 13:24:23.732 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 13:24:23.732 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:24:23.941 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 13:24:23.947 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(132, var0.size());  // (Inspector) Original Value: 132 | Regression Value: 144
Keeping 1 assertions.
[MASTER] 13:24:23.948 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
*** Random test generation finished.
[MASTER] 13:24:23.948 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 13:24:23.948 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 977 | Tests with assertion: 1
* Generated 1 tests with total length 9
* GA-Budget:
	- MaxTime :                         16 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 13:24:24.472 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:24:24.481 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 13:24:24.486 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[size]}
[MASTER] 13:24:24.489 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.InspectorAssertion@c4bde6ec] invalid assertion(s) to be removed
[MASTER] 13:24:24.489 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 13:24:24.490 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 13:24:24.492 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 128
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 38
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
