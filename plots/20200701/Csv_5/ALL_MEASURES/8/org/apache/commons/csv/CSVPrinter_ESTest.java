/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 29 11:24:02 GMT 2020
 */

package org.apache.commons.csv;

import org.junit.Test;
import static org.junit.Assert.*;
import java.nio.CharBuffer;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class CSVPrinter_ESTest extends CSVPrinter_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      char[] charArray0 = new char[2];
      CharBuffer charBuffer0 = CharBuffer.wrap(charArray0);
      CSVFormat cSVFormat0 = CSVFormat.newFormat('r');
      CSVPrinter cSVPrinter0 = new CSVPrinter(charBuffer0, cSVFormat0);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.nio.BufferOverflowException : null
      cSVPrinter0.println();
  }
}
