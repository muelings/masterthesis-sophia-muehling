* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 17098
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 12:41:04.893 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 12:41:04.900 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 128
* Using seed 1593427262276
* Starting evolution
[MASTER] 12:41:11.882 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:471.7217520182871 - branchDistance:0.0 - coverage:190.72175201828708 - ex: 0 - tex: 20
[MASTER] 12:41:11.882 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 471.7217520182871, number of tests: 10, total length: 300
[MASTER] 12:41:13.200 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:414.27401038947744 - branchDistance:0.0 - coverage:133.27401038947744 - ex: 0 - tex: 6
[MASTER] 12:41:13.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 414.27401038947744, number of tests: 4, total length: 111
[MASTER] 12:41:13.465 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:242.19295052264368 - branchDistance:0.0 - coverage:175.22436413520913 - ex: 0 - tex: 20
[MASTER] 12:41:13.465 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 242.19295052264368, number of tests: 10, total length: 271
[MASTER] 12:41:26.494 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:233.1010672870889 - branchDistance:0.0 - coverage:166.13248089965435 - ex: 0 - tex: 14
[MASTER] 12:41:26.494 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.1010672870889, number of tests: 8, total length: 219
[MASTER] 12:41:27.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:209.9545674576475 - branchDistance:0.0 - coverage:142.98598107021294 - ex: 0 - tex: 18
[MASTER] 12:41:27.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.9545674576475, number of tests: 9, total length: 249
[MASTER] 12:41:28.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:208.1828533171508 - branchDistance:0.0 - coverage:141.21426692971625 - ex: 0 - tex: 16
[MASTER] 12:41:28.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 208.1828533171508, number of tests: 9, total length: 253
[MASTER] 12:41:28.438 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:206.53325927410097 - branchDistance:0.0 - coverage:139.56467288666641 - ex: 0 - tex: 16
[MASTER] 12:41:28.438 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 206.53325927410097, number of tests: 9, total length: 220
[MASTER] 12:41:28.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:194.70145949493278 - branchDistance:0.0 - coverage:127.73287310749822 - ex: 0 - tex: 18
[MASTER] 12:41:28.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 194.70145949493278, number of tests: 9, total length: 241
[MASTER] 12:41:28.914 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:193.68710542794713 - branchDistance:0.0 - coverage:126.71851904051258 - ex: 0 - tex: 18
[MASTER] 12:41:28.914 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 193.68710542794713, number of tests: 9, total length: 244
[MASTER] 12:41:29.867 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:185.75607094518853 - branchDistance:0.0 - coverage:118.78748455775397 - ex: 0 - tex: 20
[MASTER] 12:41:29.867 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 185.75607094518853, number of tests: 10, total length: 253
[MASTER] 12:41:30.400 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:184.75607094518853 - branchDistance:0.0 - coverage:117.78748455775397 - ex: 0 - tex: 20
[MASTER] 12:41:30.400 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 184.75607094518853, number of tests: 10, total length: 258
[MASTER] 12:41:31.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:180.6083516816031 - branchDistance:0.0 - coverage:113.63976529416854 - ex: 0 - tex: 20
[MASTER] 12:41:31.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 180.6083516816031, number of tests: 10, total length: 262
[MASTER] 12:41:33.162 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:177.75607094518853 - branchDistance:0.0 - coverage:110.78748455775397 - ex: 0 - tex: 16
[MASTER] 12:41:33.162 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 177.75607094518853, number of tests: 10, total length: 257
[MASTER] 12:41:33.723 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:176.6083516816031 - branchDistance:0.0 - coverage:109.63976529416854 - ex: 0 - tex: 20
[MASTER] 12:41:33.723 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 176.6083516816031, number of tests: 10, total length: 284
[MASTER] 12:41:34.083 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:173.87303466305968 - branchDistance:0.0 - coverage:106.90444827562513 - ex: 0 - tex: 16
[MASTER] 12:41:34.083 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 173.87303466305968, number of tests: 10, total length: 281
[MASTER] 12:41:34.384 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:173.6083516816031 - branchDistance:0.0 - coverage:106.63976529416854 - ex: 0 - tex: 18
[MASTER] 12:41:34.384 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 173.6083516816031, number of tests: 10, total length: 259
[MASTER] 12:41:35.181 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:173.53255302902426 - branchDistance:0.0 - coverage:106.5639666415897 - ex: 0 - tex: 20
[MASTER] 12:41:35.181 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 173.53255302902426, number of tests: 10, total length: 302
[MASTER] 12:41:35.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:172.8658863623576 - branchDistance:0.0 - coverage:105.89729997492304 - ex: 0 - tex: 20
[MASTER] 12:41:35.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 172.8658863623576, number of tests: 12, total length: 341
[MASTER] 12:41:35.938 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:172.53255302902426 - branchDistance:0.0 - coverage:105.5639666415897 - ex: 0 - tex: 18
[MASTER] 12:41:35.938 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 172.53255302902426, number of tests: 10, total length: 282
[MASTER] 12:41:36.129 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:171.53255302902426 - branchDistance:0.0 - coverage:104.5639666415897 - ex: 0 - tex: 24
[MASTER] 12:41:36.129 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 171.53255302902426, number of tests: 13, total length: 402
[MASTER] 12:41:38.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:170.6083516816031 - branchDistance:0.0 - coverage:103.63976529416854 - ex: 0 - tex: 24
[MASTER] 12:41:38.348 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 170.6083516816031, number of tests: 13, total length: 383
[MASTER] 12:41:39.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:163.7681852129323 - branchDistance:0.0 - coverage:96.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:39.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 163.7681852129323, number of tests: 12, total length: 352
[MASTER] 12:41:40.603 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.3594771241830066 - fitness:162.02748488246925 - branchDistance:0.0 - coverage:96.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:40.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 162.02748488246925, number of tests: 12, total length: 357
[MASTER] 12:41:41.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.3594771241830066 - fitness:161.02748488246925 - branchDistance:0.0 - coverage:95.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:41.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 161.02748488246925, number of tests: 12, total length: 367
[MASTER] 12:41:42.816 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.2444444444444445 - fitness:158.7681852129323 - branchDistance:0.0 - coverage:91.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:42.816 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 158.7681852129323, number of tests: 12, total length: 355
[MASTER] 12:41:43.515 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.359477124183007 - fitness:149.04350126452215 - branchDistance:0.0 - coverage:95.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:43.515 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 149.04350126452215, number of tests: 13, total length: 368
[MASTER] 12:41:45.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.359477124183007 - fitness:143.04350126452215 - branchDistance:0.0 - coverage:89.79959882549775 - ex: 0 - tex: 20
[MASTER] 12:41:45.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.04350126452215, number of tests: 12, total length: 344
[MASTER] 12:41:46.852 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.359477124183007 - fitness:142.04350126452215 - branchDistance:0.0 - coverage:88.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:46.852 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.04350126452215, number of tests: 13, total length: 382
[MASTER] 12:41:47.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.359477124183007 - fitness:140.828375803915 - branchDistance:0.0 - coverage:95.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:47.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.828375803915, number of tests: 13, total length: 381
[MASTER] 12:41:47.677 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.359477124183007 - fitness:133.828375803915 - branchDistance:0.0 - coverage:88.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:47.677 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.828375803915, number of tests: 13, total length: 386
[MASTER] 12:41:51.936 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.359477124183007 - fitness:127.84577999778905 - branchDistance:0.0 - coverage:88.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:51.936 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.84577999778905, number of tests: 13, total length: 379
[MASTER] 12:41:53.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.359477124183007 - fitness:123.29451673011073 - branchDistance:0.0 - coverage:88.79959882549775 - ex: 0 - tex: 22
[MASTER] 12:41:53.707 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.29451673011073, number of tests: 13, total length: 368
[MASTER] 12:41:55.743 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.359477124183007 - fitness:121.29451673011073 - branchDistance:0.0 - coverage:86.79959882549775 - ex: 0 - tex: 24
[MASTER] 12:41:55.743 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.29451673011073, number of tests: 15, total length: 425
[MASTER] 12:42:00.323 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.359477124183007 - fitness:119.36831326622853 - branchDistance:0.0 - coverage:84.87339536161555 - ex: 0 - tex: 22
[MASTER] 12:42:00.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.36831326622853, number of tests: 15, total length: 422
[MASTER] 12:42:03.031 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:119.19452315363313 - branchDistance:0.0 - coverage:84.87339536161555 - ex: 0 - tex: 20
[MASTER] 12:42:03.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.19452315363313, number of tests: 14, total length: 392
[MASTER] 12:42:03.512 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.42602495543672 - fitness:119.10377403730837 - branchDistance:0.0 - coverage:84.87339536161555 - ex: 0 - tex: 22
[MASTER] 12:42:03.512 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.10377403730837, number of tests: 15, total length: 412
[MASTER] 12:42:04.378 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.359477124183007 - fitness:105.18135674448939 - branchDistance:0.0 - coverage:70.68643883987642 - ex: 0 - tex: 18
[MASTER] 12:42:04.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.18135674448939, number of tests: 14, total length: 368
[MASTER] 12:42:06.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:105.00756663189398 - branchDistance:0.0 - coverage:70.68643883987642 - ex: 0 - tex: 20
[MASTER] 12:42:06.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.00756663189398, number of tests: 14, total length: 375
[MASTER] 12:42:06.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.359477124183007 - fitness:104.18135674448939 - branchDistance:0.0 - coverage:69.68643883987642 - ex: 0 - tex: 20
[MASTER] 12:42:06.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.18135674448939, number of tests: 15, total length: 404
[MASTER] 12:42:08.742 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:104.00756663189398 - branchDistance:0.0 - coverage:69.68643883987642 - ex: 0 - tex: 22
[MASTER] 12:42:08.742 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.00756663189398, number of tests: 15, total length: 405
[MASTER] 12:42:09.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.359477124183007 - fitness:103.18135674448939 - branchDistance:0.0 - coverage:68.68643883987642 - ex: 0 - tex: 20
[MASTER] 12:42:09.259 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.18135674448939, number of tests: 15, total length: 423
[MASTER] 12:42:10.755 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.359477124183007 - fitness:102.18135674448939 - branchDistance:0.0 - coverage:67.68643883987642 - ex: 0 - tex: 18
[MASTER] 12:42:10.755 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.18135674448939, number of tests: 15, total length: 405
[MASTER] 12:42:11.967 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:102.00756663189398 - branchDistance:0.0 - coverage:67.68643883987642 - ex: 0 - tex: 18
[MASTER] 12:42:11.967 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.00756663189398, number of tests: 14, total length: 382
[MASTER] 12:42:13.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:101.00756663189398 - branchDistance:0.0 - coverage:66.68643883987642 - ex: 0 - tex: 20
[MASTER] 12:42:13.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.00756663189398, number of tests: 15, total length: 406
[MASTER] 12:42:14.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:100.72686487750804 - branchDistance:0.0 - coverage:66.40573708549046 - ex: 0 - tex: 18
[MASTER] 12:42:14.706 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.72686487750804, number of tests: 14, total length: 380
[MASTER] 12:42:16.149 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:100.00756663189398 - branchDistance:0.0 - coverage:65.68643883987642 - ex: 0 - tex: 20
[MASTER] 12:42:16.150 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.00756663189398, number of tests: 15, total length: 410
[MASTER] 12:42:16.958 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:99.72686487750804 - branchDistance:0.0 - coverage:65.40573708549046 - ex: 0 - tex: 20
[MASTER] 12:42:16.958 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.72686487750804, number of tests: 15, total length: 408
[MASTER] 12:42:17.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:98.72686487750804 - branchDistance:0.0 - coverage:64.40573708549046 - ex: 0 - tex: 20
[MASTER] 12:42:17.086 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.72686487750804, number of tests: 15, total length: 397
[MASTER] 12:42:18.153 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:93.4035844542276 - branchDistance:0.0 - coverage:59.082456662210035 - ex: 0 - tex: 18
[MASTER] 12:42:18.153 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.4035844542276, number of tests: 14, total length: 393
[MASTER] 12:42:18.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:92.4035844542276 - branchDistance:0.0 - coverage:58.082456662210035 - ex: 0 - tex: 20
[MASTER] 12:42:18.691 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.4035844542276, number of tests: 15, total length: 406
[MASTER] 12:42:21.750 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:91.4035844542276 - branchDistance:0.0 - coverage:57.082456662210035 - ex: 0 - tex: 18
[MASTER] 12:42:21.750 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.4035844542276, number of tests: 16, total length: 422
[MASTER] 12:42:27.314 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:90.4035844542276 - branchDistance:0.0 - coverage:56.082456662210035 - ex: 0 - tex: 16
[MASTER] 12:42:27.314 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.4035844542276, number of tests: 15, total length: 382
[MASTER] 12:42:33.157 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:89.06678190492505 - branchDistance:0.0 - coverage:54.74565411290749 - ex: 0 - tex: 18
[MASTER] 12:42:33.157 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.06678190492505, number of tests: 16, total length: 410
[MASTER] 12:42:42.524 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:87.35249619063934 - branchDistance:0.0 - coverage:53.03136839862177 - ex: 0 - tex: 18
[MASTER] 12:42:42.524 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.35249619063934, number of tests: 15, total length: 389
[MASTER] 12:42:51.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:87.30796636669776 - branchDistance:0.0 - coverage:52.986838574680185 - ex: 0 - tex: 14
[MASTER] 12:42:51.114 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.30796636669776, number of tests: 15, total length: 384
[MASTER] 12:42:54.298 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:86.98978454851594 - branchDistance:0.0 - coverage:52.66865675649837 - ex: 0 - tex: 14
[MASTER] 12:42:54.298 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.98978454851594, number of tests: 14, total length: 388
[MASTER] 12:42:56.857 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:85.30796636669776 - branchDistance:0.0 - coverage:50.986838574680185 - ex: 0 - tex: 14
[MASTER] 12:42:56.857 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.30796636669776, number of tests: 14, total length: 390
[MASTER] 12:42:59.103 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:84.98978454851594 - branchDistance:0.0 - coverage:50.66865675649837 - ex: 0 - tex: 14
[MASTER] 12:42:59.103 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.98978454851594, number of tests: 14, total length: 391
[MASTER] 12:43:03.251 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:84.98978454851593 - branchDistance:0.0 - coverage:50.668656756498365 - ex: 0 - tex: 12
[MASTER] 12:43:03.251 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.98978454851593, number of tests: 14, total length: 374
[MASTER] 12:43:03.611 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:84.94525472457435 - branchDistance:0.0 - coverage:50.624126932556784 - ex: 0 - tex: 14
[MASTER] 12:43:03.611 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.94525472457435, number of tests: 14, total length: 396
[MASTER] 12:43:05.535 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.403076923076923 - fitness:84.8982823411658 - branchDistance:0.0 - coverage:50.577154549148226 - ex: 0 - tex: 16
[MASTER] 12:43:05.535 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.8982823411658, number of tests: 14, total length: 398
[MASTER] 12:43:14.578 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.440185830429733 - fitness:84.75177956634927 - branchDistance:0.0 - coverage:50.577154549148226 - ex: 0 - tex: 14
[MASTER] 12:43:14.578 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.75177956634927, number of tests: 14, total length: 371
[MASTER] 12:43:15.166 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.403076923076924 - fitness:81.35464146014299 - branchDistance:0.0 - coverage:50.577154549148226 - ex: 0 - tex: 12
[MASTER] 12:43:15.166 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.35464146014299, number of tests: 14, total length: 371
[MASTER] 12:43:19.887 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.403076923076924 - fitness:81.34879350692661 - branchDistance:0.0 - coverage:50.57130659593185 - ex: 0 - tex: 12
[MASTER] 12:43:19.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.34879350692661, number of tests: 14, total length: 365
[MASTER] 12:43:20.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.42602495543672 - fitness:81.2821469848517 - branchDistance:0.0 - coverage:50.577154549148226 - ex: 0 - tex: 12
[MASTER] 12:43:20.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.2821469848517, number of tests: 14, total length: 365
[MASTER] 12:43:21.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.42602495543672 - fitness:81.27629903163533 - branchDistance:0.0 - coverage:50.57130659593185 - ex: 0 - tex: 12
[MASTER] 12:43:21.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.27629903163533, number of tests: 14, total length: 365
[MASTER] 12:43:22.138 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.22602495543672 - fitness:76.51919780234775 - branchDistance:0.0 - coverage:50.577154549148226 - ex: 0 - tex: 14
[MASTER] 12:43:22.138 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.51919780234775, number of tests: 14, total length: 365
[MASTER] 12:43:22.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.22602495543672 - fitness:76.51334984913137 - branchDistance:0.0 - coverage:50.57130659593185 - ex: 0 - tex: 14
[MASTER] 12:43:22.749 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.51334984913137, number of tests: 14, total length: 370
[MASTER] 12:43:32.328 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.240185830429732 - fitness:76.48192677820468 - branchDistance:0.0 - coverage:50.57130659593185 - ex: 0 - tex: 8
[MASTER] 12:43:32.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.48192677820468, number of tests: 13, total length: 344
[MASTER] 12:43:32.603 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.26612365790448 - fitness:76.42457543108686 - branchDistance:0.0 - coverage:50.57130659593185 - ex: 0 - tex: 8
[MASTER] 12:43:32.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.42457543108686, number of tests: 14, total length: 348
[MASTER] 12:43:32.610 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.22602495543672 - fitness:75.51334984913137 - branchDistance:0.0 - coverage:49.571306595931844 - ex: 0 - tex: 8
[MASTER] 12:43:32.610 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.51334984913137, number of tests: 14, total length: 348
[MASTER] 12:43:33.474 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.240185830429732 - fitness:75.48192677820468 - branchDistance:0.0 - coverage:49.571306595931844 - ex: 0 - tex: 8
[MASTER] 12:43:33.474 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.48192677820468, number of tests: 13, total length: 346
[MASTER] 12:43:33.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.22602495543672 - fitness:74.47327195430358 - branchDistance:0.0 - coverage:50.57130659593185 - ex: 0 - tex: 8
[MASTER] 12:43:33.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.47327195430358, number of tests: 14, total length: 349
[MASTER] 12:43:35.897 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.22602495543672 - fitness:73.47327195430357 - branchDistance:0.0 - coverage:49.571306595931844 - ex: 0 - tex: 8
[MASTER] 12:43:35.897 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.47327195430357, number of tests: 13, total length: 351
[MASTER] 12:43:49.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.02602495543672 - fitness:72.06673601188237 - branchDistance:0.0 - coverage:49.571306595931844 - ex: 0 - tex: 8
[MASTER] 12:43:49.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.06673601188237, number of tests: 13, total length: 349
[MASTER] 12:44:21.721 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.02602495543672 - fitness:70.65718316635392 - branchDistance:0.0 - coverage:48.16175375040339 - ex: 0 - tex: 6
[MASTER] 12:44:21.721 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.65718316635392, number of tests: 13, total length: 310
[MASTER] 12:44:23.166 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.05674531155475 - fitness:70.60660798618373 - branchDistance:0.0 - coverage:48.16175375040339 - ex: 0 - tex: 6
[MASTER] 12:44:23.166 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.60660798618373, number of tests: 13, total length: 311
[MASTER] 12:44:23.620 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.02602495543672 - fitness:69.12464422647284 - branchDistance:0.0 - coverage:48.16175375040339 - ex: 0 - tex: 8
[MASTER] 12:44:23.620 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.12464422647284, number of tests: 14, total length: 318
[MASTER] 12:44:23.925 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.02602495543672 - fitness:68.62680418860651 - branchDistance:0.0 - coverage:46.131374772655995 - ex: 0 - tex: 8
[MASTER] 12:44:23.925 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.62680418860651, number of tests: 14, total length: 346
[MASTER] 12:44:24.534 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.05674531155475 - fitness:68.57622900843634 - branchDistance:0.0 - coverage:46.131374772655995 - ex: 0 - tex: 6
[MASTER] 12:44:24.534 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.57622900843634, number of tests: 14, total length: 338
[MASTER] 12:44:25.522 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.02602495543672 - fitness:68.09426524872543 - branchDistance:0.0 - coverage:47.131374772655995 - ex: 0 - tex: 8
[MASTER] 12:44:25.522 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.09426524872543, number of tests: 14, total length: 365
[MASTER] 12:44:25.713 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.02602495543672 - fitness:67.09426524872543 - branchDistance:0.0 - coverage:46.131374772655995 - ex: 0 - tex: 8
[MASTER] 12:44:25.713 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.09426524872543, number of tests: 14, total length: 328
[MASTER] 12:44:30.527 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.02602495543672 - fitness:66.76093191539209 - branchDistance:0.0 - coverage:45.79804143932266 - ex: 0 - tex: 6
[MASTER] 12:44:30.527 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.76093191539209, number of tests: 15, total length: 351
[MASTER] 12:44:33.282 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.074437197559437 - fitness:66.7408479889202 - branchDistance:0.0 - coverage:45.84662443527408 - ex: 0 - tex: 8
[MASTER] 12:44:33.282 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.7408479889202, number of tests: 15, total length: 342
[MASTER] 12:44:33.612 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.074437197559437 - fitness:66.69226499296877 - branchDistance:0.0 - coverage:45.79804143932266 - ex: 0 - tex: 6
[MASTER] 12:44:33.612 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.69226499296877, number of tests: 15, total length: 349
[MASTER] 12:44:42.090 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.085939186186758 - fitness:66.67602020232763 - branchDistance:0.0 - coverage:45.79804143932266 - ex: 0 - tex: 8
[MASTER] 12:44:42.090 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.67602020232763, number of tests: 16, total length: 349
[MASTER] 12:44:55.825 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.087100048074355 - fitness:66.67438213715668 - branchDistance:0.0 - coverage:45.79804143932266 - ex: 0 - tex: 4
[MASTER] 12:44:55.825 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.67438213715668, number of tests: 15, total length: 283
[MASTER] 12:44:56.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.143082043329615 - fitness:66.5957064137727 - branchDistance:0.0 - coverage:45.79804143932266 - ex: 0 - tex: 4
[MASTER] 12:44:56.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.5957064137727, number of tests: 15, total length: 271
[MASTER] 12:45:36.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.143082043329615 - fitness:66.59372426194007 - branchDistance:0.0 - coverage:45.796059287490024 - ex: 0 - tex: 4
[MASTER] 12:45:36.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.59372426194007, number of tests: 12, total length: 225
[MASTER] 12:45:43.529 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.174828075075647 - fitness:66.5493852902469 - branchDistance:0.0 - coverage:45.796059287490024 - ex: 0 - tex: 4
[MASTER] 12:45:43.529 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.5493852902469, number of tests: 11, total length: 215
[MASTER] 12:45:49.524 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.174828075075647 - fitness:66.54286355111647 - branchDistance:0.0 - coverage:45.78953754835959 - ex: 0 - tex: 4
[MASTER] 12:45:49.524 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.54286355111647, number of tests: 11, total length: 218
[MASTER] 12:45:59.187 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.174828075075647 - fitness:66.53253757218584 - branchDistance:0.0 - coverage:45.77921156942896 - ex: 0 - tex: 6
[MASTER] 12:45:59.187 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.53253757218584, number of tests: 11, total length: 214
[MASTER] 12:46:10.692 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 306s and 629 generations, 776283 statements, best individuals have fitness: 66.53253757218584
[MASTER] 12:46:10.697 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 80%
* Total number of goals: 128
* Number of covered goals: 102
* Generated 11 tests with total length 205
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     67 / 0           
	- MaxTime :                        306 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:46:11.064 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:46:11.118 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 170 
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 12:46:11.319 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 12:46:11.320 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 12:46:11.320 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 128
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 217
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
