* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 21400
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 02:30:58.027 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 02:30:58.032 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 23
* Using seed 1593390655223
* Starting evolution
[MASTER] 02:30:59.345 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:57.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 2
[MASTER] 02:30:59.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.0, number of tests: 4, total length: 95
[MASTER] 02:30:59.561 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:54.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 12
[MASTER] 02:30:59.561 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.0, number of tests: 8, total length: 171
[MASTER] 02:30:59.616 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.875 - fitness:12.666666666666666 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 02:30:59.616 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.666666666666666, number of tests: 5, total length: 153
[MASTER] 02:31:01.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.888888888888889 - fitness:12.636363636363635 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 02:31:01.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.636363636363635, number of tests: 5, total length: 150
[MASTER] 02:31:02.004 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.875 - fitness:11.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 02:31:02.004 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.666666666666666, number of tests: 6, total length: 137
[MASTER] 02:31:02.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.923076923076923 - fitness:9.51111111111111 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 02:31:02.270 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.51111111111111, number of tests: 5, total length: 136
[MASTER] 02:31:02.814 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.96 - fitness:9.471264367816092 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 02:31:02.814 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.471264367816092, number of tests: 5, total length: 135
[MASTER] 02:31:02.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.947368421052632 - fitness:8.543046357615895 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 02:31:02.904 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.543046357615895, number of tests: 5, total length: 140
[MASTER] 02:31:03.162 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.923076923076923 - fitness:8.51111111111111 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:31:03.162 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.51111111111111, number of tests: 6, total length: 165
[MASTER] 02:31:03.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.947368421052632 - fitness:8.484848484848484 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:31:03.754 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.484848484848484, number of tests: 6, total length: 166
[MASTER] 02:31:04.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.986301369863014 - fitness:8.44313725490196 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:31:04.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.44313725490196, number of tests: 6, total length: 166
[MASTER] 02:31:04.905 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.947368421052632 - fitness:7.811764705882353 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 02:31:04.905 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.811764705882353, number of tests: 5, total length: 142
[MASTER] 02:31:05.552 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.96 - fitness:7.532663316582914 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:05.552 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.532663316582914, number of tests: 5, total length: 157
[MASTER] 02:31:07.073 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9 - fitness:6.842696629213483 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:07.073 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.842696629213483, number of tests: 5, total length: 154
[MASTER] 02:31:07.226 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.96 - fitness:6.803571428571428 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:31:07.226 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.803571428571428, number of tests: 6, total length: 188
[MASTER] 02:31:09.010 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.979591836734693 - fitness:6.210633946830266 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:09.010 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.210633946830266, number of tests: 6, total length: 204
[MASTER] 02:31:09.546 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.991735537190083 - fitness:6.204301075268817 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:09.546 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.204301075268817, number of tests: 5, total length: 162
[MASTER] 02:31:09.891 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.976744186046512 - fitness:5.737288135593221 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:31:09.891 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.737288135593221, number of tests: 6, total length: 198
[MASTER] 02:31:11.626 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.979591836734693 - fitness:5.736059479553903 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:31:11.627 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.736059479553903, number of tests: 6, total length: 188
[MASTER] 02:31:12.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.99056603773585 - fitness:5.731330472103004 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:12.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.731330472103004, number of tests: 5, total length: 159
[MASTER] 02:31:12.347 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.979591836734693 - fitness:5.3407155025553665 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:12.347 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.3407155025553665, number of tests: 7, total length: 215
[MASTER] 02:31:16.182 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.979591836734693 - fitness:5.006289308176101 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:16.183 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.006289308176101, number of tests: 6, total length: 137
[MASTER] 02:31:16.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.989690721649485 - fitness:5.003174603174603 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:16.820 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.003174603174603, number of tests: 6, total length: 135
[MASTER] 02:31:17.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.991735537190083 - fitness:5.002544529262086 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:17.930 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.002544529262086, number of tests: 6, total length: 121
[MASTER] 02:31:18.790 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.979591836734693 - fitness:4.719708029197081 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:18.790 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.719708029197081, number of tests: 6, total length: 122
[MASTER] 02:31:19.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.99056603773585 - fitness:4.716790289952798 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:19.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.716790289952798, number of tests: 6, total length: 124
[MASTER] 02:31:20.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.988235294117647 - fitness:4.469387755102041 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:31:20.872 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.469387755102041, number of tests: 7, total length: 174
[MASTER] 02:31:21.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.989010989010989 - fitness:4.469208211143695 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:21.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.469208211143695, number of tests: 6, total length: 124
[MASTER] 02:31:22.554 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.99056603773585 - fitness:4.468848332284455 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:22.554 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.468848332284455, number of tests: 7, total length: 155
[MASTER] 02:31:23.627 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.986301369863014 - fitness:4.0612903225806445 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:23.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0612903225806445, number of tests: 6, total length: 122
[MASTER] 02:31:23.659 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.991735537190083 - fitness:4.060311284046692 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:23.659 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.060311284046692, number of tests: 6, total length: 130
[MASTER] 02:31:25.448 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.99056603773585 - fitness:3.8904037755637124 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:25.448 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8904037755637124, number of tests: 7, total length: 161
[MASTER] 02:31:25.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.99173553719008 - fitness:3.8902158934313276 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:25.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8902158934313276, number of tests: 6, total length: 116
[MASTER] 02:31:26.248 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.99585062240664 - fitness:3.8895549919299057 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:26.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8895549919299057, number of tests: 6, total length: 116
[MASTER] 02:31:27.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.99173553719008 - fitness:3.738033072236728 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:27.904 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.738033072236728, number of tests: 6, total length: 116
[MASTER] 02:31:31.724 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.98901098901099 - fitness:3.6014293567894446 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:31.724 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.6014293567894446, number of tests: 6, total length: 100
[MASTER] 02:31:32.246 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.99056603773585 - fitness:3.6012269938650308 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:32.246 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.6012269938650308, number of tests: 6, total length: 99
[MASTER] 02:31:32.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.99173553719008 - fitness:3.601074824307565 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:32.749 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.601074824307565, number of tests: 6, total length: 99
[MASTER] 02:31:37.935 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.99056603773585 - fitness:3.4773033707865166 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:37.935 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4773033707865166, number of tests: 4, total length: 87
[MASTER] 02:31:38.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.99173553719008 - fitness:3.4771653543307086 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:38.678 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4771653543307086, number of tests: 4, total length: 85
[MASTER] 02:31:39.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.99173553719008 - fitness:3.364524614806464 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:39.931 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.364524614806464, number of tests: 4, total length: 84
[MASTER] 02:31:40.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.99173553719008 - fitness:3.2616822429906542 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:40.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2616822429906542, number of tests: 4, total length: 84
[MASTER] 02:31:47.711 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.99526066350711 - fitness:3.2613355317394888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:31:47.711 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2613355317394888, number of tests: 4, total length: 78
[MASTER] 02:31:48.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.99585062240664 - fitness:3.261277517141826 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:48.851 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.261277517141826, number of tests: 4, total length: 84
[MASTER] 02:31:50.191 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.997624703087887 - fitness:3.261103077876472 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:50.191 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.261103077876472, number of tests: 4, total length: 80
[MASTER] 02:31:50.688 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.994475138121548 - fitness:3.167165553764679 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:50.688 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.167165553764679, number of tests: 4, total length: 75
[MASTER] 02:31:52.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.99526066350711 - fitness:3.1670946079399567 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:52.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1670946079399567, number of tests: 4, total length: 75
[MASTER] 02:31:52.859 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.99585062240664 - fitness:3.1670413280304337 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:31:52.860 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1670413280304337, number of tests: 4, total length: 75
[MASTER] 02:32:01.603 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.994475138121548 - fitness:3.0804597701149428 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:01.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0804597701149428, number of tests: 4, total length: 75
[MASTER] 02:32:01.663 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.99526066350711 - fitness:3.080394387561623 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:32:01.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.080394387561623, number of tests: 4, total length: 71
[MASTER] 02:32:01.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.99585062240664 - fitness:3.0803452855245683 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:01.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0803452855245683, number of tests: 4, total length: 72
[MASTER] 02:32:02.707 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.997506234413965 - fitness:3.0802075019952118 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:32:02.707 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0802075019952118, number of tests: 4, total length: 69
[MASTER] 02:32:12.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.997506234413965 - fitness:3.0001918465227817 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:32:12.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0001918465227817, number of tests: 4, total length: 64
[MASTER] 02:32:26.236 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.99857346647646 - fitness:3.0001097393689986 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:26.236 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0001097393689986, number of tests: 5, total length: 100
[MASTER] 02:32:27.934 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.998751560549312 - fitness:3.000096038415366 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:27.934 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.000096038415366, number of tests: 5, total length: 100
[MASTER] 02:32:39.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.997150997150996 - fitness:2.926129168425496 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:39.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926129168425496, number of tests: 5, total length: 100
[MASTER] 02:32:43.772 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.997506234413965 - fitness:2.926103824127101 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:43.773 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926103824127101, number of tests: 5, total length: 100
[MASTER] 02:32:45.828 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.998336106489184 - fitness:2.9260446197460865 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:45.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.9260446197460865, number of tests: 5, total length: 100
[MASTER] 02:32:50.211 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.99857346647646 - fitness:2.926027686780091 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:50.211 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926027686780091, number of tests: 5, total length: 101
[MASTER] 02:32:53.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.998751560549312 - fitness:2.926014981966152 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:32:53.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926014981966152, number of tests: 5, total length: 98
[MASTER] 02:33:35.835 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.998751560549312 - fitness:2.8572256654924866 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:33:35.835 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8572256654924866, number of tests: 5, total length: 67
[MASTER] 02:33:58.088 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.998751560549312 - fitness:2.793180644050284 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:33:58.088 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793180644050284, number of tests: 5, total length: 65
[MASTER] 02:35:38.696 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.998751560549312 - fitness:2.7334054683923594 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:35:38.696 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7334054683923594, number of tests: 5, total length: 61
[MASTER] 02:35:59.137 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 462 generations, 456360 statements, best individuals have fitness: 2.7334054683923594
[MASTER] 02:35:59.141 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 23
* Number of covered goals: 23
* Generated 4 tests with total length 58
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                      3 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 02:35:59.543 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:35:59.567 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 51 
[MASTER] 02:35:59.619 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 02:35:59.619 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 02:35:59.619 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 02:35:59.619 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 02:35:59.619 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 02:35:59.619 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 02:35:59.619 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 02:35:59.620 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
