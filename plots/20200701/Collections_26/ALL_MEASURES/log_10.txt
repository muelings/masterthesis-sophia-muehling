* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 6048
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:01:37.541 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:01:37.546 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 23
* Using seed 1593392494211
* Starting evolution
[MASTER] 03:01:38.867 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:105.0 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 2
[MASTER] 03:01:38.868 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.0, number of tests: 1, total length: 6
[MASTER] 03:01:39.000 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:59.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 03:01:39.000 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.0, number of tests: 4, total length: 61
[MASTER] 03:01:39.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9411764705882355 - fitness:11.523809523809524 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 03:01:39.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.523809523809524, number of tests: 10, total length: 260
[MASTER] 03:01:40.519 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.954545454545455 - fitness:10.477124183006534 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 4
[MASTER] 03:01:40.519 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.477124183006534, number of tests: 8, total length: 209
[MASTER] 03:01:41.046 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.954545454545455 - fitness:9.477124183006534 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 03:01:41.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.477124183006534, number of tests: 8, total length: 254
[MASTER] 03:01:42.358 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.954545454545455 - fitness:8.477124183006534 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 03:01:42.358 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.477124183006534, number of tests: 9, total length: 297
[MASTER] 03:01:43.371 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.954545454545455 - fitness:7.5371428571428565 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 03:01:43.371 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.5371428571428565, number of tests: 9, total length: 198
[MASTER] 03:01:44.693 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.947368421052632 - fitness:6.811764705882353 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:44.693 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.811764705882353, number of tests: 8, total length: 170
[MASTER] 03:01:45.698 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.96 - fitness:6.803571428571428 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 03:01:45.698 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.803571428571428, number of tests: 8, total length: 156
[MASTER] 03:01:45.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.989690721649485 - fitness:6.205366357069144 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:45.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.205366357069144, number of tests: 8, total length: 155
[MASTER] 03:01:46.846 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.988235294117647 - fitness:5.337585868498528 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:46.846 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.337585868498528, number of tests: 8, total length: 144
[MASTER] 03:01:47.982 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.989690721649485 - fitness:5.337059329320722 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:01:47.982 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.337059329320722, number of tests: 8, total length: 137
[MASTER] 03:01:48.570 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.994082840236686 - fitness:5.335471139615195 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:01:48.570 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.335471139615195, number of tests: 8, total length: 138
[MASTER] 03:01:50.238 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.988235294117647 - fitness:5.003623188405797 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:01:50.238 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.003623188405797, number of tests: 8, total length: 148
[MASTER] 03:01:50.666 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.994082840236686 - fitness:5.001821493624772 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 03:01:50.666 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.001821493624772, number of tests: 8, total length: 123
[MASTER] 03:01:52.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.988235294117647 - fitness:4.717409587888982 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:01:52.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.717409587888982, number of tests: 8, total length: 143
[MASTER] 03:01:52.671 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.989690721649485 - fitness:4.4690508940852816 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:52.671 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.4690508940852816, number of tests: 8, total length: 145
[MASTER] 03:01:53.960 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.989690721649485 - fitness:4.252095422308189 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:53.960 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.252095422308189, number of tests: 8, total length: 147
[MASTER] 03:01:55.630 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.988235294117647 - fitness:4.0609418282548475 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:55.630 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0609418282548475, number of tests: 8, total length: 150
[MASTER] 03:01:57.026 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.989690721649485 - fitness:4.060679611650485 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:57.026 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.060679611650485, number of tests: 8, total length: 136
[MASTER] 03:01:59.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.989690721649485 - fitness:3.8905444126074498 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:01:59.268 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8905444126074498, number of tests: 8, total length: 140
[MASTER] 03:02:01.253 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.989690721649485 - fitness:3.7383279044516833 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:02:01.253 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.7383279044516833, number of tests: 8, total length: 142
[MASTER] 03:02:03.231 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.99526066350711 - fitness:3.7375249500998002 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 03:02:03.231 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.7375249500998002, number of tests: 8, total length: 126
[MASTER] 03:02:03.500 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.989690721649485 - fitness:3.601340897369778 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:03.500 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.601340897369778, number of tests: 8, total length: 131
[MASTER] 03:02:05.975 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.986301369863014 - fitness:3.4778067885117494 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:02:05.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4778067885117494, number of tests: 8, total length: 99
[MASTER] 03:02:06.261 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.986301369863014 - fitness:3.365109034267913 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:02:06.261 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.365109034267913, number of tests: 8, total length: 106
[MASTER] 03:02:07.189 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.989690721649485 - fitness:3.261883408071749 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:07.189 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.261883408071749, number of tests: 8, total length: 100
[MASTER] 03:02:08.980 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.989690721649485 - fitness:3.1675977653631286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:08.980 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1675977653631286, number of tests: 8, total length: 92
[MASTER] 03:02:11.310 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.988235294117647 - fitness:3.080979284369115 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:11.310 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.080979284369115, number of tests: 8, total length: 140
[MASTER] 03:02:11.874 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.989690721649485 - fitness:3.080858085808581 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:11.874 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.080858085808581, number of tests: 8, total length: 119
[MASTER] 03:02:13.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.988235294117647 - fitness:3.000905387052965 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:13.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.000905387052965, number of tests: 7, total length: 128
[MASTER] 03:02:16.061 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.989690721649485 - fitness:3.0007933359777867 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:16.061 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0007933359777867, number of tests: 8, total length: 163
[MASTER] 03:02:16.586 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.994082840236686 - fitness:2.926348092941692 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:02:16.586 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926348092941692, number of tests: 8, total length: 142
[MASTER] 03:02:16.660 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.988235294117647 - fitness:2.8579234972677594 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:16.660 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8579234972677594, number of tests: 8, total length: 162
[MASTER] 03:02:17.898 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.989690721649485 - fitness:2.8578268876611417 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:17.898 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8578268876611417, number of tests: 8, total length: 162
[MASTER] 03:02:21.304 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.986301369863014 - fitness:2.7939508506616253 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:02:21.304 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7939508506616253, number of tests: 8, total length: 127
[MASTER] 03:02:22.581 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.988235294117647 - fitness:2.793831168831169 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:22.581 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793831168831169, number of tests: 8, total length: 126
[MASTER] 03:02:22.848 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.989690721649485 - fitness:2.793741109530583 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 8
[MASTER] 03:02:22.848 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793741109530583, number of tests: 8, total length: 124
[MASTER] 03:02:24.970 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.99481865284974 - fitness:2.793423874195854 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 03:02:24.970 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793423874195854, number of tests: 8, total length: 120
[MASTER] 03:02:29.520 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.996677740863788 - fitness:2.793308890925756 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:29.520 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793308890925756, number of tests: 8, total length: 112
[MASTER] 03:02:30.689 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.997624703087887 - fitness:2.7932503276539973 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:30.689 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7932503276539973, number of tests: 8, total length: 116
[MASTER] 03:02:36.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.997920997921 - fitness:2.793232004588471 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:36.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793232004588471, number of tests: 7, total length: 106
[MASTER] 03:02:38.871 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.998918918918918 - fitness:2.793170295257978 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:38.871 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793170295257978, number of tests: 7, total length: 106
[MASTER] 03:02:43.654 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.999053926206244 - fitness:2.793161947018139 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:02:43.654 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793161947018139, number of tests: 7, total length: 105
[MASTER] 03:03:04.666 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.99924299772899 - fitness:2.7931502558212387 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 03:03:04.666 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7931502558212387, number of tests: 6, total length: 95
[MASTER] 03:06:38.809 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 270 generations, 265802 statements, best individuals have fitness: 2.7931502558212387
[MASTER] 03:06:38.812 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 23
* Number of covered goals: 23
* Generated 6 tests with total length 71
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                      3 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 03:06:39.235 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:06:39.262 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 61 
[MASTER] 03:06:39.329 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 03:06:39.329 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 03:06:39.329 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:06:39.329 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:06:39.329 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:06:39.330 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:06:39.330 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:06:39.330 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:06:39.330 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 03:06:39.331 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
