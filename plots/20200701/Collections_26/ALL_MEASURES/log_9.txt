* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 6057
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 02:51:23.811 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 02:51:23.817 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 23
* Using seed 1593391880972
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 02:51:25.123 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:60.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 02:51:25.123 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.0, number of tests: 6, total length: 171
[MASTER] 02:51:25.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.875 - fitness:22.419354838709676 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 10
[MASTER] 02:51:25.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.419354838709676, number of tests: 7, total length: 138
[MASTER] 02:51:25.722 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.8888888888888888 - fitness:22.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 8
[MASTER] 02:51:25.722 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.0, number of tests: 8, total length: 236
[MASTER] 02:51:26.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.9846153846153847 - fitness:18.05019305019305 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 10
[MASTER] 02:51:26.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.05019305019305, number of tests: 9, total length: 223
[MASTER] 02:51:26.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.9473684210526314 - fitness:13.51063829787234 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 8
[MASTER] 02:51:26.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.51063829787234, number of tests: 8, total length: 220
[MASTER] 02:51:26.717 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.975609756097561 - fitness:9.519877675840977 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 6
[MASTER] 02:51:26.718 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.519877675840977, number of tests: 8, total length: 287
[MASTER] 02:51:28.322 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.975609756097561 - fitness:8.519877675840977 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 02:51:28.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.519877675840977, number of tests: 8, total length: 206
[MASTER] 02:51:29.313 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.975609756097561 - fitness:7.7934782608695645 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 02:51:29.313 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.7934782608695645, number of tests: 8, total length: 211
[MASTER] 02:51:30.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.975609756097561 - fitness:7.519877675840978 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:51:30.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.519877675840978, number of tests: 7, total length: 197
[MASTER] 02:51:31.597 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.967741935483871 - fitness:6.798561151079136 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:31.597 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.798561151079136, number of tests: 7, total length: 186
[MASTER] 02:51:31.603 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.975609756097561 - fitness:6.7934782608695645 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:31.603 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.7934782608695645, number of tests: 7, total length: 179
[MASTER] 02:51:33.298 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.967741935483872 - fitness:6.216828478964401 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:51:33.298 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.216828478964401, number of tests: 7, total length: 196
[MASTER] 02:51:33.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.975609756097562 - fitness:6.212713936430317 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:51:33.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.212713936430317, number of tests: 7, total length: 189
[MASTER] 02:51:34.352 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.967741935483872 - fitness:5.741176470588235 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 02:51:34.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.741176470588235, number of tests: 7, total length: 203
[MASTER] 02:51:35.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.975609756097562 - fitness:5.737777777777778 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:35.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.737777777777778, number of tests: 7, total length: 186
[MASTER] 02:51:37.627 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.987654320987655 - fitness:5.732584269662921 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:37.627 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.732584269662921, number of tests: 7, total length: 165
[MASTER] 02:51:37.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.967741935483872 - fitness:5.345013477088949 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:37.799 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.345013477088949, number of tests: 7, total length: 170
[MASTER] 02:51:38.368 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.985915492957746 - fitness:5.338425381903643 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:38.369 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.338425381903643, number of tests: 7, total length: 173
[MASTER] 02:51:39.076 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.990291262135923 - fitness:5.002989536621824 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:39.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.002989536621824, number of tests: 7, total length: 171
[MASTER] 02:51:39.676 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.990291262135923 - fitness:4.716863289382373 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:39.676 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.716863289382373, number of tests: 7, total length: 173
[MASTER] 02:51:39.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.991666666666667 - fitness:4.716497915425849 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:39.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.716497915425849, number of tests: 7, total length: 173
[MASTER] 02:51:40.843 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.992700729927007 - fitness:4.71622326551904 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:40.843 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.71622326551904, number of tests: 7, total length: 156
[MASTER] 02:51:41.345 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.985915492957746 - fitness:4.469924812030076 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:41.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.469924812030076, number of tests: 7, total length: 175
[MASTER] 02:51:41.624 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.992700729927007 - fitness:4.25148334094021 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:41.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.25148334094021, number of tests: 7, total length: 155
[MASTER] 02:51:52.846 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.987654320987655 - fitness:4.061046511627907 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:52.846 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.061046511627907, number of tests: 6, total length: 101
[MASTER] 02:51:55.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.985915492957748 - fitness:3.8911511354737662 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:51:55.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8911511354737662, number of tests: 6, total length: 100
[MASTER] 02:51:57.035 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.987654320987655 - fitness:3.8908716540837336 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:51:57.035 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.8908716540837336, number of tests: 6, total length: 92
[MASTER] 02:52:00.941 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.987654320987655 - fitness:3.7386215864759427 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:00.941 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.7386215864759427, number of tests: 5, total length: 75
[MASTER] 02:52:01.468 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.985915492957748 - fitness:3.6018322762508808 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:01.468 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.6018322762508808, number of tests: 5, total length: 76
[MASTER] 02:52:02.725 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.987654320987655 - fitness:3.601605929586164 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:02.725 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.601605929586164, number of tests: 5, total length: 75
[MASTER] 02:52:03.814 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.987654320987655 - fitness:3.4776470588235293 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:03.814 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4776470588235293, number of tests: 4, total length: 71
[MASTER] 02:52:07.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.987654320987655 - fitness:3.364963503649635 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:52:07.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.364963503649635, number of tests: 4, total length: 82
[MASTER] 02:52:09.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.983606557377048 - fitness:3.2624821683309557 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:09.569 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2624821683309557, number of tests: 4, total length: 69
[MASTER] 02:52:10.444 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.987654320987655 - fitness:3.2620837808807734 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:10.444 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2620837808807734, number of tests: 4, total length: 71
[MASTER] 02:52:11.428 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.985915492957748 - fitness:3.16793893129771 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 02:52:11.428 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.16793893129771, number of tests: 4, total length: 74
[MASTER] 02:52:12.364 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.987654320987655 - fitness:3.1677817807514153 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:12.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1677817807514153, number of tests: 4, total length: 65
[MASTER] 02:52:13.304 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.99290780141844 - fitness:3.1673071238545667 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:13.304 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.1673071238545667, number of tests: 4, total length: 64
[MASTER] 02:52:14.591 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.987654320987655 - fitness:3.0810276679841895 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:14.591 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0810276679841895, number of tests: 4, total length: 64
[MASTER] 02:52:18.800 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.987654320987655 - fitness:3.0009501187648455 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:18.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0009501187648455, number of tests: 4, total length: 60
[MASTER] 02:52:22.038 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.99290780141844 - fitness:3.0005457025920874 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:22.038 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.0005457025920874, number of tests: 4, total length: 56
[MASTER] 02:52:25.833 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.993788819875775 - fitness:3.000477897252091 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:25.834 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.000477897252091, number of tests: 4, total length: 55
[MASTER] 02:52:28.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.99173553719008 - fitness:2.926515615431721 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:28.021 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926515615431721, number of tests: 4, total length: 56
[MASTER] 02:52:30.913 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.99290780141844 - fitness:2.926431949553337 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:30.914 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926431949553337, number of tests: 4, total length: 55
[MASTER] 02:52:34.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.993788819875775 - fitness:2.926369075011505 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:34.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.926369075011505, number of tests: 4, total length: 54
[MASTER] 02:52:34.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.993788819875775 - fitness:2.8575549145773245 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:34.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8575549145773245, number of tests: 4, total length: 54
[MASTER] 02:52:46.018 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.993788819875775 - fitness:2.793487574978578 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:52:46.018 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.793487574978578, number of tests: 4, total length: 61
[MASTER] 02:53:15.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.99290780141844 - fitness:2.7337432017025303 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:53:15.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7337432017025303, number of tests: 4, total length: 53
[MASTER] 02:53:24.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.993788819875775 - fitness:2.733692275833506 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:53:24.302 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.733692275833506, number of tests: 4, total length: 52
[MASTER] 02:53:37.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.99526066350711 - fitness:2.7336072049296885 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:53:37.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7336072049296885, number of tests: 4, total length: 52
[MASTER] 02:53:47.448 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.99585062240664 - fitness:2.7335731083137365 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:53:47.448 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7335731083137365, number of tests: 4, total length: 52
[MASTER] 02:54:09.877 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.99604743083004 - fitness:2.7335617340888128 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:54:09.877 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7335617340888128, number of tests: 4, total length: 70
[MASTER] 02:54:27.487 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.996539792387544 - fitness:2.7335332795016725 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 02:54:27.487 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7335332795016725, number of tests: 5, total length: 57
[MASTER] 02:56:24.832 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 842 generations, 653702 statements, best individuals have fitness: 2.7335332795016725
[MASTER] 02:56:24.836 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 23
* Number of covered goals: 23
* Generated 4 tests with total length 52
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                      3 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 02:56:25.180 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:56:25.205 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 47 
[MASTER] 02:56:25.258 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 02:56:25.258 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 02:56:25.258 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 02:56:25.258 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 02:56:25.258 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 02:56:25.258 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 02:56:25.260 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
