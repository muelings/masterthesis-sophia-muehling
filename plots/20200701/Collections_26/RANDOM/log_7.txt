* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.keyvalue.MultiKey
* Starting client
* Connecting to master process on port 13110
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_26_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.keyvalue.MultiKey
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 23
[MASTER] 02:25:51.430 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:27:15.517 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:27:16.897 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/collections4/keyvalue/MultiKey_1_tmp__ESTest.class' should be in target project, but could not be found!
*** Random test generation finished.
[MASTER] 02:30:52.437 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 36015 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 02:30:52.478 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:30:52.478 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 183 seconds more than allowed.
[MASTER] 02:30:52.487 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 02:30:52.488 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:30:52.491 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 23
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'MultiKey_ESTest' to evosuite-tests
* Done!

* Computation finished
