/*
 * This file was automatically generated by EvoSuite
 * Tue Jun 30 05:35:56 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Parser_ESTest extends Parser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      BasicParser basicParser0 = new BasicParser();
      Options options0 = new Options();
      Option option0 = new Option("o", "e", true, "o");
      option0.setRequired(true);
      options0.addOption(option0);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.apache.commons.cli.MissingOptionException : Missing required option: o
      // Modified Version:
      //     org.apache.commons.cli.MissingOptionException : o
      try { 
        basicParser0.parse(options0, (String[]) null);
        fail("Expecting exception: Exception");
      
      } catch(Exception e) {
         //
         // Missing required option: o
         //
         verifyException("org.apache.commons.cli.Parser", e);
         assertTrue(e.getMessage().equals("Missing required option: o"));   
      }
  }
}
