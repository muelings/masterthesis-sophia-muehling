/*
 * This file was automatically generated by EvoSuite
 * Tue Jun 30 06:20:12 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Properties;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.evosuite.runtime.testdata.EvoSuiteFile;
import org.evosuite.runtime.testdata.FileSystemHandling;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Parser_ESTest extends Parser_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      FileSystemHandling.setPermissions((EvoSuiteFile) null, false, true, false);
      FileSystemHandling.shouldAllThrowIOExceptions();
      GnuParser gnuParser0 = new GnuParser();
      Options options0 = new Options();
      String[] stringArray0 = new String[5];
      stringArray0[0] = "  dy)HwlS@?";
      stringArray0[1] = " ";
      stringArray0[2] = "\"Y}<zK{B8Hq/=/vc}";
      stringArray0[3] = "M";
      stringArray0[4] = "[1LlbJ";
      Properties properties0 = new Properties();
      byte[] byteArray0 = new byte[5];
      byteArray0[0] = (byte) (-83);
      byteArray0[1] = (byte)1;
      byteArray0[2] = (byte) (-1);
      byteArray0[3] = (byte)19;
      byteArray0[4] = (byte) (-71);
      ByteArrayInputStream byteArrayInputStream0 = new ByteArrayInputStream(byteArray0, (byte) (-71), 1609);
      DataInputStream dataInputStream0 = new DataInputStream(byteArrayInputStream0);
      Integer integer0 = new Integer((-2021168327));
      properties0.put(dataInputStream0, integer0);
      Object object0 = new Object();
      Object object1 = new Object();
      properties0.put(object0, object1);
      Object object2 = new Object();
      properties0.put("  dy)HwlS@?", object2);
      // Undeclared exception!
      try { 
        gnuParser0.parse(options0, stringArray0, properties0);
        fail("Expecting exception: ClassCastException");
      
      } catch(ClassCastException e) {
         //
         // java.lang.Object cannot be cast to java.lang.String
         //
         verifyException("java.util.Properties", e);
         assertTrue(e.getMessage().equals("java.lang.Object cannot be cast to java.lang.String"));   
      }
  }
}
