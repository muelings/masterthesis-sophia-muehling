* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 21886
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 74
[MASTER] 07:29:11.877 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:29:57.116 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 07:30:13.554 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:30:14.929 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:30:14.934 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 07:30:15.219 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:30:15.224 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:30:15.225 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 11754 | Tests with assertion: 1
* Generated 1 tests with total length 10
* GA-Budget:
	- MaxTime :                         63 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 07:30:15.739 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:30:15.749 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 8 
[MASTER] 07:30:15.753 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 07:30:15.753 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 07:30:15.753 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 7
[MASTER] 07:30:15.753 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 07:30:15.819 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 07:30:15.824 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 14%
* Total number of goals: 74
* Number of covered goals: 10
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
