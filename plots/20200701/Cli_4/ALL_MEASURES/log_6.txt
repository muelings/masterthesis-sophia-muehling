* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 13825
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:57:51.277 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:57:51.283 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 74
* Using seed 1593496668861
* Starting evolution
[MASTER] 07:57:52.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:262.99999981277597 - branchDistance:0.0 - coverage:105.99999981277595 - ex: 0 - tex: 8
[MASTER] 07:57:52.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 262.99999981277597, number of tests: 7, total length: 150
[MASTER] 07:57:52.428 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:260.99999990747074 - branchDistance:0.0 - coverage:103.99999990747074 - ex: 0 - tex: 10
[MASTER] 07:57:52.428 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 260.99999990747074, number of tests: 8, total length: 199
[MASTER] 07:57:53.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:251.9999999550119 - branchDistance:0.0 - coverage:94.9999999550119 - ex: 0 - tex: 10
[MASTER] 07:57:53.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 251.9999999550119, number of tests: 8, total length: 217
[MASTER] 07:57:53.209 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:246.99999998144568 - branchDistance:0.0 - coverage:89.9999999814457 - ex: 0 - tex: 16
[MASTER] 07:57:53.209 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 246.99999998144568, number of tests: 9, total length: 233
[MASTER] 07:57:53.746 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:243.99999997943786 - branchDistance:0.0 - coverage:86.99999997943786 - ex: 0 - tex: 10
[MASTER] 07:57:53.747 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.99999997943786, number of tests: 8, total length: 223
[MASTER] 07:57:55.154 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:243.99999986004707 - branchDistance:0.0 - coverage:86.99999986004707 - ex: 0 - tex: 6
[MASTER] 07:57:55.154 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.99999986004707, number of tests: 7, total length: 153
[MASTER] 07:57:55.253 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:241.99999999813735 - branchDistance:0.0 - coverage:84.99999999813735 - ex: 0 - tex: 10
[MASTER] 07:57:55.253 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 241.99999999813735, number of tests: 8, total length: 185
[MASTER] 07:57:55.336 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:240.99999997943786 - branchDistance:0.0 - coverage:83.99999997943786 - ex: 0 - tex: 12
[MASTER] 07:57:55.336 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 240.99999997943786, number of tests: 8, total length: 226
[MASTER] 07:57:55.367 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.99999999813735 - branchDistance:0.0 - coverage:80.99999999813735 - ex: 0 - tex: 12
[MASTER] 07:57:55.367 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.99999999813735, number of tests: 8, total length: 204
[MASTER] 07:57:55.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.99999997943786 - branchDistance:0.0 - coverage:80.99999997943786 - ex: 0 - tex: 12
[MASTER] 07:57:55.509 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.99999997943786, number of tests: 8, total length: 235
[MASTER] 07:57:56.060 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:234.99999997943786 - branchDistance:0.0 - coverage:77.99999997943786 - ex: 0 - tex: 14
[MASTER] 07:57:56.060 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 234.99999997943786, number of tests: 8, total length: 245
[MASTER] 07:57:56.397 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.99999997943786 - branchDistance:0.0 - coverage:76.99999997943786 - ex: 0 - tex: 10
[MASTER] 07:57:56.397 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.99999997943786, number of tests: 8, total length: 228
[MASTER] 07:57:56.624 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:230.99999997943786 - branchDistance:0.0 - coverage:73.99999997943786 - ex: 0 - tex: 12
[MASTER] 07:57:56.624 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 230.99999997943786, number of tests: 8, total length: 239
[MASTER] 07:57:57.548 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:229.99999999813735 - branchDistance:0.0 - coverage:72.99999999813735 - ex: 0 - tex: 12
[MASTER] 07:57:57.548 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 229.99999999813735, number of tests: 10, total length: 259
[MASTER] 07:57:57.736 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:228.99999997943786 - branchDistance:0.0 - coverage:71.99999997943786 - ex: 0 - tex: 14
[MASTER] 07:57:57.736 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 228.99999997943786, number of tests: 8, total length: 227
[MASTER] 07:57:57.743 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:227.99999997943786 - branchDistance:0.0 - coverage:70.99999997943786 - ex: 0 - tex: 14
[MASTER] 07:57:57.743 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 227.99999997943786, number of tests: 9, total length: 268
[MASTER] 07:57:58.423 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:226.99999997943786 - branchDistance:0.0 - coverage:69.99999997943786 - ex: 0 - tex: 16
[MASTER] 07:57:58.423 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 226.99999997943786, number of tests: 9, total length: 230
[MASTER] 07:57:58.839 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:220.99999997943786 - branchDistance:0.0 - coverage:63.99999997943786 - ex: 0 - tex: 16
[MASTER] 07:57:58.839 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 220.99999997943786, number of tests: 9, total length: 228
[MASTER] 07:57:59.953 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.99999997943786 - branchDistance:0.0 - coverage:62.99999997943786 - ex: 0 - tex: 16
[MASTER] 07:57:59.954 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.99999997943786, number of tests: 9, total length: 241
[MASTER] 07:58:01.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:218.99999997943786 - branchDistance:0.0 - coverage:61.99999997943786 - ex: 0 - tex: 16
[MASTER] 07:58:01.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 218.99999997943786, number of tests: 9, total length: 252
[MASTER] 07:58:01.699 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:217.99999997943786 - branchDistance:0.0 - coverage:60.99999997943786 - ex: 0 - tex: 18
[MASTER] 07:58:01.699 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 217.99999997943786, number of tests: 10, total length: 263
[MASTER] 07:58:02.038 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:217.9999999723624 - branchDistance:0.0 - coverage:60.999999972362374 - ex: 0 - tex: 20
[MASTER] 07:58:02.038 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 217.9999999723624, number of tests: 11, total length: 301
[MASTER] 07:58:03.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:217.99999989217355 - branchDistance:0.0 - coverage:60.99999989217355 - ex: 0 - tex: 16
[MASTER] 07:58:03.050 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 217.99999989217355, number of tests: 11, total length: 294
[MASTER] 07:58:04.310 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:216.9999999723624 - branchDistance:0.0 - coverage:59.999999972362374 - ex: 0 - tex: 20
[MASTER] 07:58:04.310 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 216.9999999723624, number of tests: 12, total length: 346
[MASTER] 07:58:05.017 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.99999997236237 - branchDistance:0.0 - coverage:59.999999972362374 - ex: 0 - tex: 18
[MASTER] 07:58:05.017 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.99999997236237, number of tests: 12, total length: 351
[MASTER] 07:58:06.105 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.99999975971883 - branchDistance:0.0 - coverage:59.99999975971883 - ex: 0 - tex: 20
[MASTER] 07:58:06.105 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.99999975971883, number of tests: 13, total length: 372
[MASTER] 07:58:09.802 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.99999975971883 - branchDistance:0.0 - coverage:58.99999975971883 - ex: 0 - tex: 16
[MASTER] 07:58:09.802 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.99999975971883, number of tests: 12, total length: 359
[MASTER] 07:58:11.011 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:122.99999975971883 - branchDistance:0.0 - coverage:56.99999975971883 - ex: 0 - tex: 18
[MASTER] 07:58:11.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.99999975971883, number of tests: 12, total length: 363
[MASTER] 07:58:12.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:121.99999975971883 - branchDistance:0.0 - coverage:55.99999975971883 - ex: 0 - tex: 18
[MASTER] 07:58:12.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.99999975971883, number of tests: 13, total length: 400
[MASTER] 07:58:13.969 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:118.0 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 20
[MASTER] 07:58:13.969 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.0, number of tests: 14, total length: 418
[MASTER] 07:58:15.973 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:117.0 - branchDistance:0.0 - coverage:51.0 - ex: 0 - tex: 18
[MASTER] 07:58:15.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.0, number of tests: 13, total length: 382
[MASTER] 07:58:17.496 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:116.0 - branchDistance:0.0 - coverage:50.0 - ex: 0 - tex: 20
[MASTER] 07:58:17.496 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.0, number of tests: 14, total length: 400
[MASTER] 07:58:19.621 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:115.0 - branchDistance:0.0 - coverage:49.0 - ex: 0 - tex: 18
[MASTER] 07:58:19.621 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.0, number of tests: 13, total length: 366
[MASTER] 07:59:03.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:112.0 - branchDistance:0.0 - coverage:46.0 - ex: 0 - tex: 16
[MASTER] 07:59:03.509 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.0, number of tests: 10, total length: 217
[MASTER] 07:59:14.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:111.99999990276993 - branchDistance:0.0 - coverage:45.999999902769936 - ex: 0 - tex: 18
[MASTER] 07:59:14.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.99999990276993, number of tests: 11, total length: 232
[MASTER] 07:59:30.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:110.99999990276993 - branchDistance:0.0 - coverage:44.999999902769936 - ex: 0 - tex: 14
[MASTER] 07:59:30.022 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.99999990276993, number of tests: 11, total length: 192
[MASTER] 08:01:15.033 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:110.99999968024603 - branchDistance:0.0 - coverage:44.999999680246034 - ex: 0 - tex: 14
[MASTER] 08:01:15.033 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.99999968024603, number of tests: 10, total length: 121
[MASTER] 08:01:34.354 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:109.0 - branchDistance:0.0 - coverage:43.0 - ex: 0 - tex: 14
[MASTER] 08:01:34.354 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.0, number of tests: 10, total length: 124
[MASTER] 08:02:52.314 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1422 generations, 1369544 statements, best individuals have fitness: 109.0
[MASTER] 08:02:52.318 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 69%
* Total number of goals: 74
* Number of covered goals: 51
* Generated 10 tests with total length 115
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                    109 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 08:02:52.601 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:02:52.635 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 92 
[MASTER] 08:02:52.699 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 1.0
[MASTER] 08:02:52.699 [logback-1] WARN  RegressionSuiteMinimizer - Test1, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 08:02:52.699 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 7
[MASTER] 08:02:52.699 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 1.0
[MASTER] 08:02:52.699 [logback-1] WARN  RegressionSuiteMinimizer - Test2, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 08:02:52.699 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 7
[MASTER] 08:02:52.699 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 7
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:02:52.703 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 08:02:52.704 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 08:02:52.704 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 08:02:52.704 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 08:02:52.704 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 08:02:52.704 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 08:02:52.704 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 08:02:52.704 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 08:02:52.752 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:02:52.753 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 14%
* Total number of goals: 74
* Number of covered goals: 10
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 13
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
