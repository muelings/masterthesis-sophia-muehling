* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 16573
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:08:43.359 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:08:43.365 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 74
* Using seed 1593497321031
* Starting evolution
[MASTER] 08:08:44.364 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:276.9999999829568 - branchDistance:0.0 - coverage:119.9999999829568 - ex: 0 - tex: 10
[MASTER] 08:08:44.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 276.9999999829568, number of tests: 6, total length: 171
[MASTER] 08:08:44.459 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.9999997564592 - branchDistance:0.0 - coverage:111.9999997564592 - ex: 0 - tex: 8
[MASTER] 08:08:44.459 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.9999997564592, number of tests: 6, total length: 159
[MASTER] 08:08:44.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:266.99999986003553 - branchDistance:0.0 - coverage:109.99999986003554 - ex: 0 - tex: 10
[MASTER] 08:08:44.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 266.99999986003553, number of tests: 8, total length: 201
[MASTER] 08:08:45.245 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.99999903938462 - branchDistance:0.0 - coverage:88.99999903938462 - ex: 0 - tex: 14
[MASTER] 08:08:45.245 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.99999903938462, number of tests: 10, total length: 214
[MASTER] 08:08:48.018 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:243.99999993527308 - branchDistance:0.0 - coverage:86.99999993527308 - ex: 0 - tex: 10
[MASTER] 08:08:48.018 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.99999993527308, number of tests: 8, total length: 163
[MASTER] 08:08:48.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:243.9999990407816 - branchDistance:0.0 - coverage:86.9999990407816 - ex: 0 - tex: 10
[MASTER] 08:08:48.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.9999990407816, number of tests: 9, total length: 204
[MASTER] 08:08:49.103 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:243.99999902244173 - branchDistance:0.0 - coverage:86.99999902244171 - ex: 0 - tex: 10
[MASTER] 08:08:49.103 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.99999902244173, number of tests: 9, total length: 207
[MASTER] 08:08:49.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:242.99999993485034 - branchDistance:0.0 - coverage:85.99999993485035 - ex: 0 - tex: 10
[MASTER] 08:08:49.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 242.99999993485034, number of tests: 9, total length: 207
[MASTER] 08:08:49.705 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:241.99999993527308 - branchDistance:0.0 - coverage:84.99999993527308 - ex: 0 - tex: 14
[MASTER] 08:08:49.705 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 241.99999993527308, number of tests: 9, total length: 204
[MASTER] 08:08:49.770 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:235.9999997578991 - branchDistance:0.0 - coverage:78.99999975789912 - ex: 0 - tex: 12
[MASTER] 08:08:49.770 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 235.9999997578991, number of tests: 9, total length: 218
[MASTER] 08:08:50.387 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.99999998302923 - branchDistance:0.0 - coverage:75.99999998302923 - ex: 0 - tex: 8
[MASTER] 08:08:50.387 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.99999998302923, number of tests: 8, total length: 192
[MASTER] 08:08:51.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:230.9999997446107 - branchDistance:0.0 - coverage:73.9999997446107 - ex: 0 - tex: 10
[MASTER] 08:08:51.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 230.9999997446107, number of tests: 10, total length: 240
[MASTER] 08:08:51.794 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:229.9999997446107 - branchDistance:0.0 - coverage:72.9999997446107 - ex: 0 - tex: 16
[MASTER] 08:08:51.794 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 229.9999997446107, number of tests: 9, total length: 231
[MASTER] 08:08:51.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:228.9999997446107 - branchDistance:0.0 - coverage:71.9999997446107 - ex: 0 - tex: 14
[MASTER] 08:08:51.929 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 228.9999997446107, number of tests: 9, total length: 230
[MASTER] 08:08:52.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:227.9999997446107 - branchDistance:0.0 - coverage:70.9999997446107 - ex: 0 - tex: 10
[MASTER] 08:08:52.132 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 227.9999997446107, number of tests: 9, total length: 226
[MASTER] 08:08:52.722 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:226.9999997446107 - branchDistance:0.0 - coverage:69.9999997446107 - ex: 0 - tex: 12
[MASTER] 08:08:52.722 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 226.9999997446107, number of tests: 10, total length: 241
[MASTER] 08:08:53.938 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.9999997446107 - branchDistance:0.0 - coverage:68.9999997446107 - ex: 0 - tex: 16
[MASTER] 08:08:53.938 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.9999997446107, number of tests: 11, total length: 277
[MASTER] 08:08:54.667 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.99999998489187 - branchDistance:0.0 - coverage:66.99999998489187 - ex: 0 - tex: 12
[MASTER] 08:08:54.667 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.99999998489187, number of tests: 9, total length: 263
[MASTER] 08:08:56.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:222.99999998489187 - branchDistance:0.0 - coverage:65.99999998489187 - ex: 0 - tex: 16
[MASTER] 08:08:56.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 222.99999998489187, number of tests: 11, total length: 289
[MASTER] 08:08:57.745 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:222.99999993014194 - branchDistance:0.0 - coverage:65.99999993014193 - ex: 0 - tex: 14
[MASTER] 08:08:57.745 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 222.99999993014194, number of tests: 10, total length: 259
[MASTER] 08:09:01.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:220.0 - branchDistance:0.0 - coverage:63.0 - ex: 0 - tex: 14
[MASTER] 08:09:01.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 220.0, number of tests: 10, total length: 284
[MASTER] 08:09:04.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.0 - branchDistance:0.0 - coverage:62.0 - ex: 0 - tex: 20
[MASTER] 08:09:04.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.0, number of tests: 12, total length: 317
[MASTER] 08:09:10.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:213.0 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 14
[MASTER] 08:09:10.050 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 213.0, number of tests: 9, total length: 224
[MASTER] 08:09:12.612 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.0 - branchDistance:0.0 - coverage:59.0 - ex: 0 - tex: 16
[MASTER] 08:09:12.612 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.0, number of tests: 9, total length: 227
[MASTER] 08:09:13.774 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:123.0 - branchDistance:0.0 - coverage:57.0 - ex: 0 - tex: 18
[MASTER] 08:09:13.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.0, number of tests: 10, total length: 266
[MASTER] 08:09:39.377 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:122.0 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 20
[MASTER] 08:09:39.377 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.0, number of tests: 11, total length: 270
[MASTER] 08:09:44.565 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:121.5 - branchDistance:0.0 - coverage:55.5 - ex: 0 - tex: 22
[MASTER] 08:09:44.565 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.5, number of tests: 12, total length: 287
[MASTER] 08:10:08.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:121.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 22
[MASTER] 08:10:08.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 12, total length: 259
[MASTER] 08:10:11.800 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.0 - branchDistance:0.0 - coverage:54.0 - ex: 0 - tex: 22
[MASTER] 08:10:11.800 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.0, number of tests: 12, total length: 258
[MASTER] 08:10:21.173 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:118.0 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 20
[MASTER] 08:10:21.173 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.0, number of tests: 12, total length: 243
[MASTER] 08:10:21.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:117.0 - branchDistance:0.0 - coverage:51.0 - ex: 0 - tex: 20
[MASTER] 08:10:21.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.0, number of tests: 12, total length: 245
[MASTER] 08:10:24.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:116.0 - branchDistance:0.0 - coverage:50.0 - ex: 0 - tex: 22
[MASTER] 08:10:24.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.0, number of tests: 13, total length: 259
[MASTER] 08:10:27.795 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:113.0 - branchDistance:0.0 - coverage:47.0 - ex: 0 - tex: 22
[MASTER] 08:10:27.795 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.0, number of tests: 13, total length: 259
[MASTER] 08:10:33.430 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:112.0 - branchDistance:0.0 - coverage:46.0 - ex: 0 - tex: 20
[MASTER] 08:10:33.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.0, number of tests: 13, total length: 254
[MASTER] 08:10:44.423 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:111.9999999186645 - branchDistance:0.0 - coverage:45.9999999186645 - ex: 0 - tex: 24
[MASTER] 08:10:44.423 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.9999999186645, number of tests: 13, total length: 242
[MASTER] 08:10:57.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:104.9999999186645 - branchDistance:0.0 - coverage:38.9999999186645 - ex: 0 - tex: 26
[MASTER] 08:10:57.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.9999999186645, number of tests: 14, total length: 270
[MASTER] 08:10:58.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:101.9999999186645 - branchDistance:0.0 - coverage:35.9999999186645 - ex: 0 - tex: 28
[MASTER] 08:10:58.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.9999999186645, number of tests: 15, total length: 307
[MASTER] 08:11:04.528 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:100.9999999186645 - branchDistance:0.0 - coverage:34.9999999186645 - ex: 0 - tex: 28
[MASTER] 08:11:04.528 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.9999999186645, number of tests: 15, total length: 293
[MASTER] 08:11:16.668 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:99.9999999186645 - branchDistance:0.0 - coverage:33.9999999186645 - ex: 0 - tex: 22
[MASTER] 08:11:16.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.9999999186645, number of tests: 15, total length: 274
[MASTER] 08:11:33.080 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:98.0 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 22
[MASTER] 08:11:33.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.0, number of tests: 14, total length: 266
[MASTER] 08:11:36.928 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:97.0 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 22
[MASTER] 08:11:36.928 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.0, number of tests: 14, total length: 246

[MASTER] 08:13:44.380 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 834 generations, 827766 statements, best individuals have fitness: 97.0
[MASTER] 08:13:44.383 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 77%
* Total number of goals: 74
* Number of covered goals: 57
* Generated 12 tests with total length 169
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     97 / 0           
[MASTER] 08:13:44.598 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:13:44.631 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 133 
[MASTER] 08:13:44.694 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.695 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.695 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 1.0
[MASTER] 08:13:44.695 [logback-1] WARN  RegressionSuiteMinimizer - Test2, uniqueExceptions: []
[MASTER] 08:13:44.695 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 8
[MASTER] 08:13:44.695 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 1.0
[MASTER] 08:13:44.695 [logback-1] WARN  RegressionSuiteMinimizer - Test3, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 08:13:44.695 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 9
[MASTER] 08:13:44.696 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 9
[MASTER] 08:13:44.699 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.699 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.699 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.699 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 08:13:44.700 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 08:13:44.701 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 08:13:44.701 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 08:13:44.701 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 08:13:44.701 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 08:13:44.746 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
* Going to analyze the coverage criteria
[MASTER] 08:13:44.747 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 26%
* Total number of goals: 74
* Number of covered goals: 19
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 8
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
