* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 20604
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:52:28.750 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:52:28.756 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 74
* Using seed 1593496346625
[Progress:>                             0%] [Cov:>                                  0%]* Starting evolution
[MASTER] 07:52:29.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:269.99999881379756 - branchDistance:0.0 - coverage:112.99999881379755 - ex: 0 - tex: 2
[MASTER] 07:52:29.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 269.99999881379756, number of tests: 3, total length: 70
[MASTER] 07:52:29.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:260.9999998579867 - branchDistance:0.0 - coverage:103.9999998579867 - ex: 0 - tex: 12
[MASTER] 07:52:29.792 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 260.9999998579867, number of tests: 6, total length: 163
[MASTER] 07:52:30.501 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:256.9999998186677 - branchDistance:0.0 - coverage:99.99999981866769 - ex: 0 - tex: 18
[MASTER] 07:52:30.501 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 256.9999998186677, number of tests: 9, total length: 265
[MASTER] 07:52:31.025 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:248.99999984629585 - branchDistance:0.0 - coverage:91.99999984629585 - ex: 0 - tex: 8
[MASTER] 07:52:31.025 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 248.99999984629585, number of tests: 4, total length: 143
[MASTER] 07:52:31.815 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:248.99999970324475 - branchDistance:0.0 - coverage:91.99999970324475 - ex: 0 - tex: 10
[MASTER] 07:52:31.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 248.99999970324475, number of tests: 6, total length: 172
[MASTER] 07:52:31.889 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.9999998354779 - branchDistance:0.0 - coverage:88.9999998354779 - ex: 0 - tex: 16
[MASTER] 07:52:31.889 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.9999998354779, number of tests: 9, total length: 257
[MASTER] 07:52:32.920 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:243.9999998843726 - branchDistance:0.0 - coverage:86.99999988437261 - ex: 0 - tex: 14
[MASTER] 07:52:32.920 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 243.9999998843726, number of tests: 7, total length: 236
[MASTER] 07:52:33.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:242.99999987706542 - branchDistance:0.0 - coverage:85.99999987706543 - ex: 0 - tex: 6
[MASTER] 07:52:33.050 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 242.99999987706542, number of tests: 7, total length: 153
[MASTER] 07:52:33.358 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:159.9999998203402 - branchDistance:0.0 - coverage:93.99999982034021 - ex: 0 - tex: 16
[MASTER] 07:52:33.358 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 159.9999998203402, number of tests: 9, total length: 299
[MASTER] 07:52:33.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:158.9999998203402 - branchDistance:0.0 - coverage:92.99999982034021 - ex: 0 - tex: 16
[MASTER] 07:52:33.931 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 158.9999998203402, number of tests: 9, total length: 300
[MASTER] 07:52:34.408 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:156.9999998839185 - branchDistance:0.0 - coverage:90.99999988391849 - ex: 0 - tex: 18
[MASTER] 07:52:34.408 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 156.9999998839185, number of tests: 10, total length: 290
[MASTER] 07:52:34.538 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:154.99999986192773 - branchDistance:0.0 - coverage:88.99999986192775 - ex: 0 - tex: 12
[MASTER] 07:52:34.538 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.99999986192773, number of tests: 9, total length: 274
[MASTER] 07:52:34.558 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:153.9999998203402 - branchDistance:0.0 - coverage:87.99999982034021 - ex: 0 - tex: 16
[MASTER] 07:52:34.559 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 153.9999998203402, number of tests: 9, total length: 292
[MASTER] 07:52:35.242 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:152.9999998354779 - branchDistance:0.0 - coverage:86.9999998354779 - ex: 0 - tex: 16
[MASTER] 07:52:35.242 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 152.9999998354779, number of tests: 9, total length: 263
[MASTER] 07:52:35.406 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:151.9999998203402 - branchDistance:0.0 - coverage:85.99999982034021 - ex: 0 - tex: 14
[MASTER] 07:52:35.406 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 151.9999998203402, number of tests: 8, total length: 257
[MASTER] 07:52:36.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:151.99999976211836 - branchDistance:0.0 - coverage:85.99999976211835 - ex: 0 - tex: 16
[MASTER] 07:52:36.298 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 151.99999976211836, number of tests: 9, total length: 300
[MASTER] 07:52:36.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:149.9999998203402 - branchDistance:0.0 - coverage:83.99999982034021 - ex: 0 - tex: 20
[MASTER] 07:52:36.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 149.9999998203402, number of tests: 11, total length: 326
[MASTER] 07:52:36.505 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:148.9999998203402 - branchDistance:0.0 - coverage:82.99999982034021 - ex: 0 - tex: 22
[MASTER] 07:52:36.505 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 148.9999998203402, number of tests: 12, total length: 377
[MASTER] 07:52:37.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:143.9999999229267 - branchDistance:0.0 - coverage:77.99999992292669 - ex: 0 - tex: 16
[MASTER] 07:52:37.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.9999999229267, number of tests: 9, total length: 278
[MASTER] 07:52:37.470 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:143.99999975786773 - branchDistance:0.0 - coverage:77.99999975786771 - ex: 0 - tex: 18
[MASTER] 07:52:37.470 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.99999975786773, number of tests: 10, total length: 307
[MASTER] 07:52:37.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:142.99999998114856 - branchDistance:0.0 - coverage:76.99999998114855 - ex: 0 - tex: 20
[MASTER] 07:52:37.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.99999998114856, number of tests: 11, total length: 334
[MASTER] 07:52:38.118 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:141.99999998114856 - branchDistance:0.0 - coverage:75.99999998114855 - ex: 0 - tex: 24
[MASTER] 07:52:38.118 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.99999998114856, number of tests: 13, total length: 385
[MASTER] 07:52:38.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:140.9999999229267 - branchDistance:0.0 - coverage:74.99999992292669 - ex: 0 - tex: 22
[MASTER] 07:52:38.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.9999999229267, number of tests: 12, total length: 364
[MASTER] 07:52:38.859 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:140.99999975786773 - branchDistance:0.0 - coverage:74.99999975786771 - ex: 0 - tex: 22
[MASTER] 07:52:38.859 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.99999975786773, number of tests: 12, total length: 354
[MASTER] 07:52:39.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:137.99999998114856 - branchDistance:0.0 - coverage:71.99999998114856 - ex: 0 - tex: 22
[MASTER] 07:52:39.615 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.99999998114856, number of tests: 11, total length: 346
[MASTER] 07:52:40.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:135.99999998114856 - branchDistance:0.0 - coverage:69.99999998114855 - ex: 0 - tex: 22
[MASTER] 07:52:40.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.99999998114856, number of tests: 11, total length: 318
[MASTER] 07:52:40.484 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:133.99999998114856 - branchDistance:0.0 - coverage:67.99999998114855 - ex: 0 - tex: 22
[MASTER] 07:52:40.484 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.99999998114856, number of tests: 11, total length: 318
[MASTER] 07:52:41.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:132.99999975786773 - branchDistance:0.0 - coverage:66.99999975786771 - ex: 0 - tex: 24
[MASTER] 07:52:41.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.99999975786773, number of tests: 12, total length: 343
[MASTER] 07:52:41.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:131.9999999981489 - branchDistance:0.0 - coverage:65.99999999814888 - ex: 0 - tex: 22
[MASTER] 07:52:41.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.9999999981489, number of tests: 11, total length: 328
[MASTER] 07:52:42.535 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:131.99999999814096 - branchDistance:0.0 - coverage:65.99999999814096 - ex: 0 - tex: 22
[MASTER] 07:52:42.535 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.99999999814096, number of tests: 12, total length: 347
[MASTER] 07:52:42.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:130.9999999981489 - branchDistance:0.0 - coverage:64.99999999814888 - ex: 0 - tex: 24
[MASTER] 07:52:42.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.9999999981489, number of tests: 12, total length: 363
[MASTER] 07:52:43.428 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:129.9999999981489 - branchDistance:0.0 - coverage:63.99999999814888 - ex: 0 - tex: 24
[MASTER] 07:52:43.428 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.9999999981489, number of tests: 12, total length: 347
[MASTER] 07:52:44.521 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:127.9999999981489 - branchDistance:0.0 - coverage:61.99999999814889 - ex: 0 - tex: 26
[MASTER] 07:52:44.521 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.9999999981489, number of tests: 13, total length: 378
[MASTER] 07:52:45.711 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:126.9999999981489 - branchDistance:0.0 - coverage:60.99999999814889 - ex: 0 - tex: 24
[MASTER] 07:52:45.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.9999999981489, number of tests: 13, total length: 353
[MASTER] 07:52:47.564 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:126.9999999247778 - branchDistance:0.0 - coverage:60.999999924777796 - ex: 0 - tex: 24
[MASTER] 07:52:47.565 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.9999999247778, number of tests: 13, total length: 365
[MASTER] 07:52:47.986 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.9999999981489 - branchDistance:0.0 - coverage:59.99999999814889 - ex: 0 - tex: 24
[MASTER] 07:52:47.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.9999999981489, number of tests: 14, total length: 376
[MASTER] 07:52:48.780 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.9999999247778 - branchDistance:0.0 - coverage:59.999999924777796 - ex: 0 - tex: 24
[MASTER] 07:52:48.780 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.9999999247778, number of tests: 14, total length: 392
[MASTER] 07:52:49.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.9999999981489 - branchDistance:0.0 - coverage:58.99999999814889 - ex: 0 - tex: 24
[MASTER] 07:52:49.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.9999999981489, number of tests: 14, total length: 365
[MASTER] 07:52:49.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.9999999247778 - branchDistance:0.0 - coverage:58.999999924777796 - ex: 0 - tex: 24
[MASTER] 07:52:49.489 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.9999999247778, number of tests: 15, total length: 402
[MASTER] 07:52:49.829 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.0 - branchDistance:0.0 - coverage:58.0 - ex: 0 - tex: 24
[MASTER] 07:52:49.829 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.0, number of tests: 15, total length: 411
[MASTER] 07:52:51.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:122.0 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 26
[MASTER] 07:52:51.114 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.0, number of tests: 15, total length: 399
[MASTER] 07:52:59.481 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:121.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 22
[MASTER] 07:52:59.481 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 15, total length: 333
[MASTER] 07:53:15.575 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.5 - branchDistance:0.0 - coverage:54.5 - ex: 0 - tex: 20
[MASTER] 07:53:15.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.5, number of tests: 16, total length: 306
[MASTER] 07:54:02.078 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:102.49999999906868 - branchDistance:0.0 - coverage:36.49999999906868 - ex: 0 - tex: 16
[MASTER] 07:54:02.078 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.49999999906868, number of tests: 12, total length: 218
[MASTER] 07:54:11.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:99.5 - branchDistance:0.0 - coverage:33.5 - ex: 0 - tex: 14
[MASTER] 07:54:11.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.5, number of tests: 12, total length: 210
[MASTER] 07:54:15.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:98.5 - branchDistance:0.0 - coverage:32.5 - ex: 0 - tex: 12
[MASTER] 07:54:15.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.5, number of tests: 12, total length: 200
[MASTER] 07:57:25.621 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:98.0 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 14
[MASTER] 07:57:25.621 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.0, number of tests: 10, total length: 130
[MASTER] 07:57:29.773 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1360 generations, 1268608 statements, best individuals have fitness: 98.0
[MASTER] 07:57:29.775 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 76%
* Total number of goals: 74
* Number of covered goals: 56
* Generated 10 tests with total length 130
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     98 / 0           
[MASTER] 07:57:29.952 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:57:29.978 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 104 
[MASTER] 07:57:30.022 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 07:57:30.024 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 1.0
[MASTER] 07:57:30.024 [logback-1] WARN  RegressionSuiteMinimizer - Test1, uniqueExceptions: []
[MASTER] 07:57:30.024 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 10
[MASTER] 07:57:30.024 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 1.0
[MASTER] 07:57:30.024 [logback-1] WARN  RegressionSuiteMinimizer - Test2, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 07:57:30.024 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 10
[MASTER] 07:57:30.024 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 10
[MASTER] 07:57:30.027 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 07:57:30.027 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 07:57:30.027 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 07:57:30.027 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 07:57:30.027 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 07:57:30.027 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:57:30.027 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:57:30.028 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:57:30.028 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 07:57:30.028 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 07:57:30.028 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 07:57:30.028 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 07:57:30.028 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 07:57:30.028 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 07:57:30.095 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 07:57:30.096 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 16%
* Total number of goals: 74
* Number of covered goals: 12
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 12
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
