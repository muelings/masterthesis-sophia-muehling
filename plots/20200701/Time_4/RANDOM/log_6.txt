* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 12850
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:36:04.986 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 134
[MASTER] 04:38:32.143 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 04:38:32.975 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 04:38:33.132 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 04:38:33.132 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 13934 | Tests with assertion: 1
* Generated 1 tests with total length 13
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                        148 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:38:33.998 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:38:33.999 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 32 seconds more than allowed.
[MASTER] 04:38:34.011 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 04:38:34.022 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 04:38:34.023 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [getField:ArrayIndexOutOfBoundsException]
[MASTER] 04:38:34.023 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getField:ArrayIndexOutOfBoundsException at 8
[MASTER] 04:38:34.023 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [getField:ArrayIndexOutOfBoundsException]
[MASTER] 04:38:34.178 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:38:34.181 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 5%
* Total number of goals: 134
* Number of covered goals: 7
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'Partial_ESTest' to evosuite-tests
* Done!

* Computation finished
