* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 7260
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:33:30.773 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 134
[MASTER] 04:33:52.520 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:33:53.942 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:33:53.959 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 04:33:54.224 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:33:54.240 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:33:54.240 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 1735 | Tests with assertion: 1
* Generated 1 tests with total length 11
* GA-Budget:
	- MaxTime :                         23 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 04:33:55.067 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:33:55.090 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 04:33:55.106 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 04:33:55.106 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [with:MockIllegalArgumentException, MockIllegalArgumentException:<init>-224,MockIllegalArgumentException:<init>-224]
[MASTER] 04:33:55.106 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: with:MockIllegalArgumentException at 8
[MASTER] 04:33:55.106 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [with:MockIllegalArgumentException, MockIllegalArgumentException:<init>-224,MockIllegalArgumentException:<init>-224]
[MASTER] 04:33:55.321 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
[MASTER] 04:33:55.324 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 17%
* Total number of goals: 134
* Number of covered goals: 23
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Partial_ESTest' to evosuite-tests
* Done!

* Computation finished
