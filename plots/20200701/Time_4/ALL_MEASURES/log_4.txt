* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 14402
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:35:17.110 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:35:17.116 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 134
* Using seed 1593484512704
* Starting evolution
[MASTER] 04:35:19.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:575.0 - branchDistance:0.0 - coverage:284.0 - ex: 0 - tex: 6
[MASTER] 04:35:19.180 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 575.0, number of tests: 3, total length: 71
[MASTER] 04:35:19.404 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:541.0 - branchDistance:0.0 - coverage:250.0 - ex: 0 - tex: 14
[MASTER] 04:35:19.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 541.0, number of tests: 8, total length: 193
[MASTER] 04:35:19.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:514.6 - branchDistance:0.0 - coverage:223.6 - ex: 0 - tex: 14
[MASTER] 04:35:19.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 514.6, number of tests: 7, total length: 161
[MASTER] 04:35:21.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:462.0 - branchDistance:0.0 - coverage:171.0 - ex: 0 - tex: 16
[MASTER] 04:35:21.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 462.0, number of tests: 8, total length: 261
* Computation finished
