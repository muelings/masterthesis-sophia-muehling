* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 16910
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:34:03.256 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:34:03.261 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 134
* Using seed 1593484439038
* Starting evolution
[MASTER] 04:34:05.087 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:563.0 - branchDistance:0.0 - coverage:272.0 - ex: 0 - tex: 6
[MASTER] 04:34:05.087 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 563.0, number of tests: 4, total length: 80
[MASTER] 04:34:05.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:483.7142857142857 - branchDistance:0.0 - coverage:192.71428571428572 - ex: 0 - tex: 12
[MASTER] 04:34:05.761 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 483.7142857142857, number of tests: 6, total length: 176
[MASTER] 04:34:07.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:483.0 - branchDistance:0.0 - coverage:192.0 - ex: 0 - tex: 14
[MASTER] 04:34:07.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 483.0, number of tests: 8, total length: 209
[MASTER] 04:34:09.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:473.66666666666663 - branchDistance:0.0 - coverage:182.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:34:09.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 473.66666666666663, number of tests: 9, total length: 208
* Computation finished
