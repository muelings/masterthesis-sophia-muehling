* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 18585
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:33:11.491 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:33:11.496 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 134
* Using seed 1593484386718
* Starting evolution
[MASTER] 04:33:13.313 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:501.7142857142857 - branchDistance:0.0 - coverage:210.71428571428572 - ex: 0 - tex: 14
[MASTER] 04:33:13.314 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 501.7142857142857, number of tests: 7, total length: 167
[MASTER] 04:33:14.026 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:464.5 - branchDistance:0.0 - coverage:173.5 - ex: 0 - tex: 20
[MASTER] 04:33:14.026 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 464.5, number of tests: 10, total length: 306
[MASTER] 04:33:17.853 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:443.5 - branchDistance:0.0 - coverage:152.5 - ex: 0 - tex: 16
[MASTER] 04:33:17.853 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 443.5, number of tests: 8, total length: 215
[MASTER] 04:33:19.194 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:437.5 - branchDistance:0.0 - coverage:146.5 - ex: 0 - tex: 18
[MASTER] 04:33:19.194 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 437.5, number of tests: 9, total length: 225
[MASTER] 04:33:19.574 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:429.5 - branchDistance:0.0 - coverage:138.5 - ex: 0 - tex: 16
[MASTER] 04:33:19.574 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 429.5, number of tests: 8, total length: 229
[MASTER] 04:33:19.913 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:418.3571428571429 - branchDistance:0.0 - coverage:127.35714285714286 - ex: 0 - tex: 18
[MASTER] 04:33:19.913 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 418.3571428571429, number of tests: 9, total length: 246
[MASTER] 04:33:22.716 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:413.8571428571429 - branchDistance:0.0 - coverage:122.85714285714286 - ex: 0 - tex: 20
[MASTER] 04:33:22.716 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 413.8571428571429, number of tests: 10, total length: 283
[MASTER] 04:33:22.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:413.3571428571429 - branchDistance:0.0 - coverage:122.35714285714286 - ex: 0 - tex: 20
[MASTER] 04:33:22.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 413.3571428571429, number of tests: 10, total length: 278
* Computation finished
