* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 6325
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:35:54.220 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:35:54.226 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 134
* Using seed 1593484550299
* Starting evolution
[MASTER] 04:35:55.830 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:545.6 - branchDistance:0.0 - coverage:254.60000000000002 - ex: 0 - tex: 14
[MASTER] 04:35:55.830 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 545.6, number of tests: 7, total length: 171
[MASTER] 04:35:56.199 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:541.0 - branchDistance:0.0 - coverage:250.0 - ex: 0 - tex: 12
[MASTER] 04:35:56.199 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 541.0, number of tests: 7, total length: 186
[MASTER] 04:35:57.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:518.0 - branchDistance:0.0 - coverage:227.0 - ex: 0 - tex: 14
[MASTER] 04:35:57.115 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 518.0, number of tests: 7, total length: 204
[MASTER] 04:35:57.892 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:512.3333333333334 - branchDistance:0.0 - coverage:221.33333333333334 - ex: 0 - tex: 20
[MASTER] 04:35:57.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 512.3333333333334, number of tests: 10, total length: 205
[MASTER] 04:35:58.486 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:493.0 - branchDistance:0.0 - coverage:202.0 - ex: 0 - tex: 16
[MASTER] 04:35:58.486 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 493.0, number of tests: 8, total length: 192
* Computation finished
