* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 6812
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:00:55.035 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 151
[MASTER] 04:02:01.607 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var14);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:02:01.607 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var14, var12);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:02:01.627 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 04:02:01.627 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:02:02.856 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/MutableDateTime_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:02:02.871 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var14);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:02:02.871 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var14, var12);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:02:02.873 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 04:02:02.873 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:03:35.631 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var18, var15);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:03:35.631 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var15, var18);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:03:35.633 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 04:03:35.633 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:03:35.848 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var18, var15);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:03:35.848 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var15, var18);  // (Same) Original Value: false | Regression Value: true
[MASTER] 04:03:35.849 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 04:03:35.849 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:05:56.055 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 26407 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 04:05:56.084 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:05:56.085 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 186 seconds more than allowed.
[MASTER] 04:05:56.090 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 04:05:56.090 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:05:56.093 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 151
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 18
* Writing JUnit test case 'MutableDateTime_ESTest' to evosuite-tests
* Done!

* Computation finished
