* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 8224
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:10:47.112 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:10:47.119 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 151
* Using seed 1593483043098
* Starting evolution
[MASTER] 04:10:48.870 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:586.0 - branchDistance:0.0 - coverage:277.0 - ex: 0 - tex: 8
[MASTER] 04:10:48.870 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 586.0, number of tests: 4, total length: 108
[MASTER] 04:10:49.636 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:559.1 - branchDistance:0.0 - coverage:250.10000000000002 - ex: 0 - tex: 16
[MASTER] 04:10:49.636 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 559.1, number of tests: 10, total length: 181
[MASTER] 04:10:50.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:547.1 - branchDistance:0.0 - coverage:238.10000000000002 - ex: 0 - tex: 6
[MASTER] 04:10:50.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 547.1, number of tests: 5, total length: 97
[MASTER] 04:10:50.163 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:534.1 - branchDistance:0.0 - coverage:225.10000000000002 - ex: 0 - tex: 16
[MASTER] 04:10:50.163 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 534.1, number of tests: 9, total length: 116
[MASTER] 04:10:50.686 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:505.3333333333333 - branchDistance:0.0 - coverage:196.33333333333331 - ex: 0 - tex: 12
[MASTER] 04:10:50.686 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 505.3333333333333, number of tests: 8, total length: 176
* Computation finished
