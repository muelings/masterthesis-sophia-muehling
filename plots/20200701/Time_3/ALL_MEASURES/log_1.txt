* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 15398
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:06:03.062 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:06:03.068 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 151
* Using seed 1593482758896
* Starting evolution
[MASTER] 04:06:05.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:579.55 - branchDistance:0.0 - coverage:270.55 - ex: 0 - tex: 10
[MASTER] 04:06:05.348 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 579.55, number of tests: 6, total length: 137
[MASTER] 04:06:06.118 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:537.1 - branchDistance:0.0 - coverage:228.10000000000002 - ex: 0 - tex: 8
[MASTER] 04:06:06.118 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 537.1, number of tests: 5, total length: 111
[MASTER] 04:06:07.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:530.4333333333334 - branchDistance:0.0 - coverage:221.43333333333334 - ex: 0 - tex: 12
[MASTER] 04:06:07.514 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 530.4333333333334, number of tests: 7, total length: 172
[MASTER] 04:06:08.536 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:515.1 - branchDistance:0.0 - coverage:206.1 - ex: 0 - tex: 12
[MASTER] 04:06:08.536 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 515.1, number of tests: 6, total length: 156
* Computation finished
