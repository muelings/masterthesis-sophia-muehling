* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.javascript.jscomp.InlineObjectLiterals
* Starting client
* Connecting to master process on port 17219
* Analyzing classpath: 
  - ../defects4j_compiled/Closure_5_fixed/build/classes
* Finished analyzing classpath
* Generating tests for class com.google.javascript.jscomp.InlineObjectLiterals
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 00:42:54.016 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 00:42:54.022 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 126
* Using seed 1593384169847
* Starting evolution
[MASTER] 00:42:54.156 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:275.0 - branchDistance:0.0 - coverage:137.0 - ex: 0 - tex: 0
[MASTER] 00:42:54.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 275.0, number of tests: 9, total length: 0
[MASTER] 00:47:55.031 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 130226 generations, 0 statements, best individuals have fitness: 275.0
[MASTER] 00:47:55.035 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 2
* Number of covered goals: 0
* Generated 5 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                    275 / 0           
[MASTER] 00:47:55.761 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:47:55.800 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 00:47:55.812 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 00:47:55.813 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 00:47:55.813 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 00:47:55.813 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 00:47:55.813 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 00:47:55.813 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 00:47:55.814 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 2
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'InlineObjectLiterals_ESTest' to evosuite-tests
* Done!

* Computation finished
