* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 16995
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:18:18.886 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:18:18.892 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 53
* Using seed 1593465496400
* Starting evolution
[MASTER] 23:18:20.560 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:219.0 - branchDistance:0.0 - coverage:109.0 - ex: 0 - tex: 14
[MASTER] 23:18:20.560 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 219.0, number of tests: 8, total length: 205
[MASTER] 23:18:20.665 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:213.0 - branchDistance:0.0 - coverage:103.0 - ex: 0 - tex: 18
[MASTER] 23:18:20.665 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 213.0, number of tests: 10, total length: 214
[MASTER] 23:18:21.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:191.0 - branchDistance:0.0 - coverage:81.0 - ex: 0 - tex: 8
[MASTER] 23:18:21.134 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 191.0, number of tests: 5, total length: 95
[MASTER] 23:18:21.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:180.0 - branchDistance:0.0 - coverage:70.0 - ex: 0 - tex: 10
[MASTER] 23:18:21.363 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 180.0, number of tests: 6, total length: 113
[MASTER] 23:18:21.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:176.0 - branchDistance:0.0 - coverage:66.0 - ex: 0 - tex: 8
[MASTER] 23:18:21.615 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 176.0, number of tests: 6, total length: 109
[MASTER] 23:18:22.291 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:175.0 - branchDistance:0.0 - coverage:65.0 - ex: 0 - tex: 6
[MASTER] 23:18:22.292 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 175.0, number of tests: 4, total length: 104
[MASTER] 23:18:22.584 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:174.0 - branchDistance:0.0 - coverage:64.0 - ex: 0 - tex: 10
[MASTER] 23:18:22.585 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 174.0, number of tests: 7, total length: 158
[MASTER] 23:18:22.997 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:168.0 - branchDistance:0.0 - coverage:58.0 - ex: 0 - tex: 10
[MASTER] 23:18:22.997 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 168.0, number of tests: 6, total length: 110
[MASTER] 23:18:23.040 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:165.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 18
[MASTER] 23:18:23.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 165.0, number of tests: 11, total length: 295
[MASTER] 23:18:23.282 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:157.0 - branchDistance:0.0 - coverage:47.0 - ex: 0 - tex: 14
[MASTER] 23:18:23.282 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 157.0, number of tests: 9, total length: 226
[MASTER] 23:18:23.823 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:155.0 - branchDistance:0.0 - coverage:45.0 - ex: 0 - tex: 12
[MASTER] 23:18:23.824 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 155.0, number of tests: 9, total length: 175
[MASTER] 23:18:23.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:149.0 - branchDistance:0.0 - coverage:39.0 - ex: 0 - tex: 12
[MASTER] 23:18:23.895 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 149.0, number of tests: 8, total length: 183
[MASTER] 23:18:25.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:146.0 - branchDistance:0.0 - coverage:36.0 - ex: 0 - tex: 14
[MASTER] 23:18:25.270 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.0, number of tests: 9, total length: 216
[MASTER] 23:18:26.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:142.0 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 14
[MASTER] 23:18:26.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.0, number of tests: 9, total length: 193
[MASTER] 23:18:27.149 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:141.0 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 14
[MASTER] 23:18:27.149 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.0, number of tests: 9, total length: 200
[MASTER] 23:18:27.750 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:139.0 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 16
[MASTER] 23:18:27.750 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.0, number of tests: 10, total length: 231
[MASTER] 23:18:28.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:137.0 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 14
[MASTER] 23:18:28.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.0, number of tests: 10, total length: 226
[MASTER] 23:18:28.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:136.0 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 14
[MASTER] 23:18:28.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.0, number of tests: 10, total length: 227
[MASTER] 23:18:29.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:133.5 - branchDistance:0.0 - coverage:23.5 - ex: 0 - tex: 16
[MASTER] 23:18:29.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.5, number of tests: 10, total length: 209
[MASTER] 23:18:29.886 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:132.0 - branchDistance:0.0 - coverage:22.0 - ex: 0 - tex: 16
[MASTER] 23:18:29.886 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.0, number of tests: 10, total length: 203
[MASTER] 23:18:30.387 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:131.0 - branchDistance:0.0 - coverage:21.0 - ex: 0 - tex: 18
[MASTER] 23:18:30.387 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.0, number of tests: 11, total length: 233
[MASTER] 23:18:30.756 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:130.5 - branchDistance:0.0 - coverage:20.5 - ex: 0 - tex: 16
[MASTER] 23:18:30.756 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.5, number of tests: 11, total length: 232
[MASTER] 23:18:31.361 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:129.0 - branchDistance:0.0 - coverage:19.0 - ex: 0 - tex: 16
[MASTER] 23:18:31.362 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.0, number of tests: 10, total length: 199
[MASTER] 23:18:31.657 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:127.0 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 18
[MASTER] 23:18:31.657 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.0, number of tests: 11, total length: 217
[MASTER] 23:18:32.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:126.0 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 16
[MASTER] 23:18:32.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.0, number of tests: 10, total length: 206
[MASTER] 23:18:33.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:125.5 - branchDistance:0.0 - coverage:15.5 - ex: 0 - tex: 16
[MASTER] 23:18:33.268 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.5, number of tests: 10, total length: 206
[MASTER] 23:18:33.876 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:60.60153229774373 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 22
[MASTER] 23:18:33.876 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.60153229774373, number of tests: 12, total length: 238
[MASTER] 23:18:34.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:60.10153229774373 - branchDistance:0.0 - coverage:15.5 - ex: 0 - tex: 18
[MASTER] 23:18:34.903 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.10153229774373, number of tests: 10, total length: 210
[MASTER] 23:18:35.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:59.10153229774373 - branchDistance:0.0 - coverage:14.5 - ex: 0 - tex: 20
[MASTER] 23:18:35.008 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.10153229774373, number of tests: 11, total length: 241
[MASTER] 23:18:35.044 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:58.10153229774373 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 22
[MASTER] 23:18:35.044 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.10153229774373, number of tests: 12, total length: 278
[MASTER] 23:18:35.901 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:57.60153229774373 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 22
[MASTER] 23:18:35.901 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.60153229774373, number of tests: 12, total length: 259
[MASTER] 23:18:43.189 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:56.10153229774373 - branchDistance:0.0 - coverage:11.5 - ex: 0 - tex: 24
[MASTER] 23:18:43.189 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.10153229774373, number of tests: 13, total length: 287
[MASTER] 23:18:45.347 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:55.10153229774373 - branchDistance:0.0 - coverage:10.5 - ex: 0 - tex: 24
[MASTER] 23:18:45.347 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.10153229774373, number of tests: 12, total length: 281
[MASTER] 23:18:53.995 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:54.60153229774373 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 22
[MASTER] 23:18:53.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.60153229774373, number of tests: 12, total length: 256
[MASTER] 23:18:55.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:54.10153229774373 - branchDistance:0.0 - coverage:9.5 - ex: 0 - tex: 22
[MASTER] 23:18:55.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.10153229774373, number of tests: 12, total length: 252
[MASTER] 23:18:56.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:53.60153229774373 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 24
[MASTER] 23:18:56.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.60153229774373, number of tests: 13, total length: 282
[MASTER] 23:18:58.790 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999121419785626 - fitness:52.60153229774373 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 26
[MASTER] 23:18:58.790 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.60153229774373, number of tests: 13, total length: 289
[MASTER] 23:19:06.157 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999463461744822 - fitness:52.6009357427995 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 24
[MASTER] 23:19:06.157 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.6009357427995, number of tests: 13, total length: 276
[MASTER] 23:19:31.775 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999463461744822 - fitness:52.1009357427995 - branchDistance:0.0 - coverage:7.5 - ex: 0 - tex: 24
[MASTER] 23:19:31.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.1009357427995, number of tests: 12, total length: 219
[MASTER] 23:19:40.455 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999463461744822 - fitness:51.6009357427995 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 26
[MASTER] 23:19:40.455 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.6009357427995, number of tests: 13, total length: 234
[MASTER] 23:19:46.097 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999463461744822 - fitness:50.6009357427995 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 28
[MASTER] 23:19:46.097 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.6009357427995, number of tests: 14, total length: 239
[MASTER] 23:20:24.669 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999633672796542 - fitness:50.600638884004454 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 28
[MASTER] 23:20:24.669 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.600638884004454, number of tests: 14, total length: 206
[MASTER] 23:21:46.035 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999633672796542 - fitness:50.100638884004454 - branchDistance:0.0 - coverage:5.5 - ex: 0 - tex: 24
[MASTER] 23:21:46.035 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.100638884004454, number of tests: 13, total length: 157
[MASTER] 23:21:47.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999633672796542 - fitness:49.600638884004454 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 26
[MASTER] 23:21:47.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.600638884004454, number of tests: 14, total length: 159
[MASTER] 23:22:27.355 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999633672796542 - fitness:49.100638884004454 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 28
[MASTER] 23:22:27.355 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.100638884004454, number of tests: 15, total length: 196
[MASTER] 23:22:31.882 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999633672796542 - fitness:48.600638884004454 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 28
[MASTER] 23:22:31.882 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.600638884004454, number of tests: 16, total length: 197
[MASTER] 23:23:19.911 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1290 generations, 881338 statements, best individuals have fitness: 48.600638884004454
[MASTER] 23:23:19.915 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 91%
* Total number of goals: 53
* Number of covered goals: 48
* Generated 14 tests with total length 174
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     49 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 23:23:20.332 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:23:20.370 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 151 
[MASTER] 23:23:20.484 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.484 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.484 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 23:23:20.485 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:23:20.486 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 23:23:20.487 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 23:23:20.487 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 23:23:20.487 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 23:23:20.488 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
