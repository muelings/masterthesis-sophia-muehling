* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 5405
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:50:09.894 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:50:09.898 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 53
* Using seed 1593467407219
* Starting evolution
[MASTER] 23:50:11.604 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:190.0 - branchDistance:0.0 - coverage:80.0 - ex: 0 - tex: 18
[MASTER] 23:50:11.604 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 190.0, number of tests: 10, total length: 267
[MASTER] 23:50:12.039 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:181.0 - branchDistance:0.0 - coverage:71.0 - ex: 0 - tex: 18
[MASTER] 23:50:12.039 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 181.0, number of tests: 10, total length: 206
[MASTER] 23:50:13.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:173.0 - branchDistance:0.0 - coverage:63.0 - ex: 0 - tex: 16
[MASTER] 23:50:13.022 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 173.0, number of tests: 10, total length: 226
[MASTER] 23:50:13.430 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:167.0 - branchDistance:0.0 - coverage:57.0 - ex: 0 - tex: 18
[MASTER] 23:50:13.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 167.0, number of tests: 11, total length: 234
[MASTER] 23:50:14.216 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:165.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 16
[MASTER] 23:50:14.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 165.0, number of tests: 11, total length: 236
[MASTER] 23:50:14.234 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:159.0 - branchDistance:0.0 - coverage:49.0 - ex: 0 - tex: 18
[MASTER] 23:50:14.234 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 159.0, number of tests: 11, total length: 229
[MASTER] 23:50:14.974 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:158.0 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 18
[MASTER] 23:50:14.974 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 158.0, number of tests: 10, total length: 221
[MASTER] 23:50:15.256 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:157.0 - branchDistance:0.0 - coverage:47.0 - ex: 0 - tex: 16
[MASTER] 23:50:15.257 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 157.0, number of tests: 10, total length: 173
[MASTER] 23:50:15.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:151.0 - branchDistance:0.0 - coverage:41.0 - ex: 0 - tex: 20
[MASTER] 23:50:15.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 151.0, number of tests: 11, total length: 233
[MASTER] 23:50:15.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:144.0 - branchDistance:0.0 - coverage:34.0 - ex: 0 - tex: 20
[MASTER] 23:50:15.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.0, number of tests: 11, total length: 239
[MASTER] 23:50:16.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:142.0 - branchDistance:0.0 - coverage:32.0 - ex: 0 - tex: 20
[MASTER] 23:50:16.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.0, number of tests: 12, total length: 254
[MASTER] 23:50:17.364 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:141.0 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 22
[MASTER] 23:50:17.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.0, number of tests: 11, total length: 255
[MASTER] 23:50:17.652 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:139.0 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 16
[MASTER] 23:50:17.652 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.0, number of tests: 11, total length: 174
[MASTER] 23:50:17.664 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:137.5 - branchDistance:0.0 - coverage:27.5 - ex: 0 - tex: 20
[MASTER] 23:50:17.664 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.5, number of tests: 12, total length: 244
[MASTER] 23:50:17.786 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:137.0 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 22
[MASTER] 23:50:17.786 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.0, number of tests: 11, total length: 241
[MASTER] 23:50:18.106 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:136.0 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 18
[MASTER] 23:50:18.106 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.0, number of tests: 12, total length: 225
[MASTER] 23:50:18.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:133.0 - branchDistance:0.0 - coverage:23.0 - ex: 0 - tex: 24
[MASTER] 23:50:18.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.0, number of tests: 12, total length: 271
[MASTER] 23:50:18.526 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:132.0 - branchDistance:0.0 - coverage:22.0 - ex: 0 - tex: 22
[MASTER] 23:50:18.526 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.0, number of tests: 12, total length: 245
[MASTER] 23:50:18.704 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:129.5 - branchDistance:0.0 - coverage:19.5 - ex: 0 - tex: 20
[MASTER] 23:50:18.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.5, number of tests: 12, total length: 248
[MASTER] 23:50:19.773 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:128.0 - branchDistance:0.0 - coverage:18.0 - ex: 0 - tex: 22
[MASTER] 23:50:19.774 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.0, number of tests: 12, total length: 245
[MASTER] 23:50:19.844 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:127.5 - branchDistance:0.0 - coverage:17.5 - ex: 0 - tex: 22
[MASTER] 23:50:19.844 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.5, number of tests: 12, total length: 251
[MASTER] 23:50:20.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:126.0 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 24
[MASTER] 23:50:20.600 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.0, number of tests: 13, total length: 265
[MASTER] 23:50:20.650 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:125.5 - branchDistance:0.0 - coverage:15.5 - ex: 0 - tex: 22
[MASTER] 23:50:20.650 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.5, number of tests: 12, total length: 261
[MASTER] 23:50:20.978 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:125.0 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 24
[MASTER] 23:50:20.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.0, number of tests: 13, total length: 265
[MASTER] 23:50:21.859 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:123.0 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 24
[MASTER] 23:50:21.860 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.0, number of tests: 13, total length: 278
[MASTER] 23:50:28.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:57.60179690075833 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 24
[MASTER] 23:50:28.168 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.60179690075833, number of tests: 13, total length: 253
[MASTER] 23:50:33.968 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:56.60179690075833 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 26
[MASTER] 23:50:33.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.60179690075833, number of tests: 15, total length: 291
[MASTER] 23:50:34.772 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:55.60179690075833 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 26
[MASTER] 23:50:34.772 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.60179690075833, number of tests: 15, total length: 297
[MASTER] 23:50:35.227 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:55.10179690075833 - branchDistance:0.0 - coverage:10.5 - ex: 0 - tex: 24
[MASTER] 23:50:35.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.10179690075833, number of tests: 14, total length: 254
[MASTER] 23:50:38.018 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:54.60179690075833 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 26
[MASTER] 23:50:38.019 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.60179690075833, number of tests: 15, total length: 277
[MASTER] 23:50:41.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:53.60179690075833 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 26
[MASTER] 23:50:41.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.60179690075833, number of tests: 16, total length: 288
[MASTER] 23:50:55.523 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:52.60179690075833 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 22
[MASTER] 23:50:55.523 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.60179690075833, number of tests: 14, total length: 233
[MASTER] 23:51:04.679 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:51.60179690075833 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 22
[MASTER] 23:51:04.679 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.60179690075833, number of tests: 13, total length: 218
[MASTER] 23:51:21.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4998969709458065 - fitness:50.60179690075833 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 24
[MASTER] 23:51:21.903 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.60179690075833, number of tests: 14, total length: 203
[MASTER] 23:51:26.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999514515972425 - fitness:50.60084670058647 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 24
[MASTER] 23:51:26.646 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.60084670058647, number of tests: 14, total length: 200
[MASTER] 23:51:27.963 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999514515972425 - fitness:49.60084670058647 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 26
[MASTER] 23:51:27.963 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.60084670058647, number of tests: 15, total length: 230
[MASTER] 23:52:32.717 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999514515972425 - fitness:49.10084670058647 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 26
[MASTER] 23:52:32.717 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.10084670058647, number of tests: 14, total length: 194
[MASTER] 23:52:41.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999514515972425 - fitness:48.60084670058647 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 28
[MASTER] 23:52:41.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.60084670058647, number of tests: 15, total length: 185

[MASTER] 23:55:10.914 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 1633 generations, 1088010 statements, best individuals have fitness: 48.60084670058647
[MASTER] 23:55:10.918 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 91%
* Total number of goals: 53
* Number of covered goals: 48
* Generated 15 tests with total length 173
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     49 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 23:55:11.249 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:55:11.275 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 142 
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.617 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 23:55:11.618 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 23:55:11.619 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 23:55:11.620 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
