* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 14883
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 53
[MASTER] 00:08:06.576 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:09:04.133 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 00:09:05.520 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/beans/NullPropertyPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:09:05.528 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 00:09:05.825 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/beans/NullPropertyPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 00:09:05.831 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 00:09:05.832 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 9535 | Tests with assertion: 1
* Generated 1 tests with total length 13
* GA-Budget:
	- MaxTime :                         59 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 00:09:06.426 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:09:06.435 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 10 
[MASTER] 00:09:06.442 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 00:09:06.442 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 00:09:06.442 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 00:09:06.445 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
