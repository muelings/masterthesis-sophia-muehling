* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 4681
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 23:23:25.021 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 53
[MASTER] 23:24:30.594 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 23:24:31.994 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/beans/NullPropertyPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 23:24:32.051 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 23:24:32.392 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/beans/NullPropertyPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 23:24:32.434 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 23:24:32.435 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 10579 | Tests with assertion: 1
* Generated 1 tests with total length 32
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                         67 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 23:24:33.200 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:24:33.213 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 22 
[MASTER] 23:24:33.249 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 23:24:33.249 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [JXPathAbstractFactoryException:createBadFactoryException-234,, createPath:JXPathAbstractFactoryException]
[MASTER] 23:24:33.249 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createPath:JXPathAbstractFactoryException at 21
[MASTER] 23:24:33.249 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [JXPathAbstractFactoryException:createBadFactoryException-234,, createPath:JXPathAbstractFactoryException]
[MASTER] 23:24:34.887 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:24:34.891 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 23%
* Total number of goals: 53
* Number of covered goals: 12
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
