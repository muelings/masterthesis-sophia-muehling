* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 2136
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 21:50:57.255 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 21:50:57.260 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 28
* Using seed 1593460253729
* Starting evolution
[MASTER] 21:50:58.741 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.98058252427185 - branchDistance:0.0 - coverage:32.980582524271846 - ex: 0 - tex: 18
[MASTER] 21:50:58.741 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.98058252427185, number of tests: 10, total length: 229
[MASTER] 21:50:59.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9999999999123552 - fitness:54.00000000058429 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 12
[MASTER] 21:50:59.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.00000000058429, number of tests: 8, total length: 177
[MASTER] 21:50:59.419 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:29.25 - branchDistance:0.0 - coverage:17.0 - ex: 0 - tex: 16
[MASTER] 21:50:59.419 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.25, number of tests: 8, total length: 177
[MASTER] 21:51:01.807 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:26.583128658069498 - branchDistance:0.0 - coverage:14.333128658069498 - ex: 0 - tex: 16
[MASTER] 21:51:01.808 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.583128658069498, number of tests: 8, total length: 200
[MASTER] 21:51:01.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:23.230582524271846 - branchDistance:0.0 - coverage:10.980582524271846 - ex: 0 - tex: 16
[MASTER] 21:51:01.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.230582524271846, number of tests: 9, total length: 195
[MASTER] 21:51:02.425 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:20.25 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 16
[MASTER] 21:51:02.425 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 20.25, number of tests: 9, total length: 188
[MASTER] 21:51:04.451 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:20.24996948335317 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 18
[MASTER] 21:51:04.451 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 20.24996948335317, number of tests: 10, total length: 208
[MASTER] 21:51:05.423 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:18.473653693879484 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 22
[MASTER] 21:51:05.423 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.473653693879484, number of tests: 12, total length: 216
[MASTER] 21:51:15.825 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:17.181787665171353 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 14
[MASTER] 21:51:15.825 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.181787665171353, number of tests: 9, total length: 138
[MASTER] 21:51:19.223 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:16.199969483353172 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:19.224 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.199969483353172, number of tests: 7, total length: 88
[MASTER] 21:51:20.097 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:15.428540911924596 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:20.097 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.428540911924596, number of tests: 7, total length: 88
[MASTER] 21:51:22.557 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.333333333333334 - fitness:15.294087130411992 - branchDistance:0.0 - coverage:8.99996948335317 - ex: 0 - tex: 6
[MASTER] 21:51:22.557 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.294087130411992, number of tests: 6, total length: 79
[MASTER] 21:51:22.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.333333333333334 - fitness:14.806421096256395 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:22.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.806421096256395, number of tests: 6, total length: 79
[MASTER] 21:51:23.060 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.333333333333334 - fitness:14.294087130411992 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:23.061 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.294087130411992, number of tests: 6, total length: 79
[MASTER] 21:51:23.756 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.666666666666666 - fitness:13.829756717395723 - branchDistance:0.0 - coverage:8.99996948335317 - ex: 0 - tex: 6
[MASTER] 21:51:23.756 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.829756717395723, number of tests: 6, total length: 83
[MASTER] 21:51:24.077 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.583333333333334 - fitness:13.417147397463598 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:24.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.417147397463598, number of tests: 6, total length: 81
[MASTER] 21:51:24.458 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.666666666666666 - fitness:12.829756717395723 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:24.458 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.829756717395723, number of tests: 6, total length: 86
[MASTER] 21:51:25.028 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.416666666666668 - fitness:12.444945559908193 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:51:25.028 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.444945559908193, number of tests: 6, total length: 93
[MASTER] 21:51:26.067 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.166666666666668 - fitness:12.130404265961864 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:26.067 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.130404265961864, number of tests: 6, total length: 71
[MASTER] 21:51:26.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.416665580768896 - fitness:12.09009841153673 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:26.333 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.09009841153673, number of tests: 7, total length: 127
[MASTER] 21:51:28.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.386363636363637 - fitness:11.943113296062199 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:51:28.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.943113296062199, number of tests: 6, total length: 74
[MASTER] 21:51:28.558 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.416655280387133 - fitness:11.938746632499525 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:28.558 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.938746632499525, number of tests: 6, total length: 74
[MASTER] 21:51:29.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.356060606060606 - fitness:11.809476400699745 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:29.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.809476400699745, number of tests: 6, total length: 81
[MASTER] 21:51:30.402 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.356060606060606 - fitness:11.683805471153914 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:30.402 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.683805471153914, number of tests: 6, total length: 75
[MASTER] 21:51:32.023 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.356060606060606 - fitness:11.568895853771593 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:32.023 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.568895853771593, number of tests: 5, total length: 76
[MASTER] 21:51:33.159 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.41664640426861 - fitness:11.56224928056028 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:33.159 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.56224928056028, number of tests: 5, total length: 75
[MASTER] 21:51:33.856 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.416665502327803 - fitness:11.562247190827597 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:51:33.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.562247190827597, number of tests: 5, total length: 79
[MASTER] 21:51:35.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.356060606060606 - fitness:11.463422049449592 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:51:35.363 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.463422049449592, number of tests: 5, total length: 70
[MASTER] 21:51:36.623 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.515151515151516 - fitness:11.447435490769733 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:36.623 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.447435490769733, number of tests: 4, total length: 68
[MASTER] 21:51:36.984 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.356060606060606 - fitness:11.366267660825532 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:36.984 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.366267660825532, number of tests: 4, total length: 66
[MASTER] 21:51:37.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.515151515151516 - fitness:11.351513426346044 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:51:37.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.351513426346044, number of tests: 4, total length: 73
[MASTER] 21:51:39.735 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.575508105881063 - fitness:11.34596393052519 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:39.735 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.34596393052519, number of tests: 4, total length: 66
[MASTER] 21:51:39.768 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.575756334841856 - fitness:11.345941161158919 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:51:39.768 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.345941161158919, number of tests: 4, total length: 101
[MASTER] 21:51:41.036 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.575756412025743 - fitness:11.34594115407912 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:41.036 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.34594115407912, number of tests: 3, total length: 61
[MASTER] 21:51:43.908 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.575683482474133 - fitness:11.257672471107679 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:43.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.257672471107679, number of tests: 3, total length: 60
[MASTER] 21:51:47.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.515151515151516 - fitness:11.180586223441274 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:47.260 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.180586223441274, number of tests: 3, total length: 58
[MASTER] 21:51:49.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.566188197767147 - fitness:11.176548978840325 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:49.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.176548978840325, number of tests: 3, total length: 62
[MASTER] 21:51:49.408 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.575756414544642 - fitness:11.17579375080113 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:49.408 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.17579375080113, number of tests: 3, total length: 57
[MASTER] 21:51:50.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.515151515151516 - fitness:11.104114010452001 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:50.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.104114010452001, number of tests: 3, total length: 55
[MASTER] 21:51:52.159 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.575225802293073 - fitness:11.09969042397248 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:52.159 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.09969042397248, number of tests: 3, total length: 52
[MASTER] 21:51:52.531 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.575756401685545 - fitness:11.099651436005203 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:52.531 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.099651436005203, number of tests: 3, total length: 61
[MASTER] 21:51:54.414 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.515151515151516 - fitness:11.032823692798754 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:54.414 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.032823692798754, number of tests: 3, total length: 49
[MASTER] 21:51:56.626 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.356060606060606 - fitness:10.976510536510144 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:56.626 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.976510536510144, number of tests: 3, total length: 58
[MASTER] 21:51:57.139 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.515151515151516 - fitness:10.966205828934102 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:57.139 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.966205828934102, number of tests: 3, total length: 47
[MASTER] 21:51:58.394 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.575476083850468 - fitness:10.962326499003733 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:51:58.394 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.962326499003733, number of tests: 3, total length: 50
[MASTER] 21:51:58.898 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.575661941463117 - fitness:10.962314570595092 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:58.898 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.962314570595092, number of tests: 3, total length: 46
[MASTER] 21:51:59.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.563335215509127 - fitness:10.90090927917418 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:51:59.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.90090927917418, number of tests: 3, total length: 46
[MASTER] 21:52:00.070 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.572473503507986 - fitness:10.90035907409377 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:00.070 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.90035907409377, number of tests: 3, total length: 46
[MASTER] 21:52:00.617 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.5756928445539 - fitness:10.900165317388483 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:00.617 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.900165317388483, number of tests: 3, total length: 45
[MASTER] 21:52:01.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.5757564015532 - fitness:10.900161492595064 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:01.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.900161492595064, number of tests: 3, total length: 46
[MASTER] 21:52:02.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.563335215509127 - fitness:10.842532587006737 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:52:02.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.842532587006737, number of tests: 3, total length: 64
[MASTER] 21:52:04.742 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.574234346511574 - fitness:10.84191607714703 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:52:04.743 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.84191607714703, number of tests: 3, total length: 62
[MASTER] 21:52:07.106 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.57557240771758 - fitness:10.841840418084164 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:07.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.841840418084164, number of tests: 3, total length: 55
[MASTER] 21:52:07.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.575755464594273 - fitness:10.841830067836387 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:52:07.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.841830067836387, number of tests: 4, total length: 57
[MASTER] 21:52:07.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.575756388820295 - fitness:10.841830015579873 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:07.569 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.841830015579873, number of tests: 3, total length: 53
[MASTER] 21:52:14.267 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.575756388831564 - fitness:10.841830015579236 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:14.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.841830015579236, number of tests: 3, total length: 53
[MASTER] 21:52:27.233 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.35606060606061 - fitness:10.798743046832614 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:27.233 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.798743046832614, number of tests: 2, total length: 54
[MASTER] 21:52:30.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.41043613707165 - fitness:10.795815540370816 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:52:30.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.795815540370816, number of tests: 2, total length: 53
[MASTER] 21:52:34.124 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.41666550517319 - fitness:10.795480767706525 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:52:34.124 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.795480767706525, number of tests: 2, total length: 67
[MASTER] 21:52:49.670 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.43939393939394 - fitness:10.794275634290994 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 4
[MASTER] 21:52:49.670 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.794275634290994, number of tests: 2, total length: 65
[MASTER] 21:52:49.935 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.43939393939394 - fitness:10.794260375967578 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:52:49.935 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.794260375967578, number of tests: 2, total length: 54
[MASTER] 21:52:55.705 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.53463203463203 - fitness:10.789164603730114 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:52:55.705 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.789164603730114, number of tests: 2, total length: 50
[MASTER] 21:52:57.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.59482968192382 - fitness:10.78597385274161 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 2
[MASTER] 21:52:57.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.78597385274161, number of tests: 2, total length: 57
[MASTER] 21:52:57.650 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.59521773713914 - fitness:10.785937964625154 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:52:57.650 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.785937964625154, number of tests: 2, total length: 50
[MASTER] 21:52:57.974 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.59984395724428 - fitness:10.785692062194602 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:52:57.975 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.785692062194602, number of tests: 2, total length: 54
[MASTER] 21:53:10.430 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.59998895473097 - fitness:10.78568435608237 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:53:10.430 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.78568435608237, number of tests: 2, total length: 46
[MASTER] 21:53:10.697 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.59999688877205 - fitness:10.785683934417595 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:53:10.697 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.785683934417595, number of tests: 2, total length: 49
[MASTER] 21:53:11.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.599998817162266 - fitness:10.785683831930857 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:53:11.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.785683831930857, number of tests: 3, total length: 76
[MASTER] 21:53:11.523 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.59999883550928 - fitness:10.785683830955781 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:53:11.523 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.785683830955781, number of tests: 2, total length: 49
[MASTER] 21:53:24.233 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.53939393939394 - fitness:10.737116351224726 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:53:24.233 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.737116351224726, number of tests: 2, total length: 50
[MASTER] 21:53:26.328 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.599998809819226 - fitness:10.734073589246156 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:53:26.328 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.734073589246156, number of tests: 2, total length: 50
[MASTER] 21:53:37.985 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.59999883001594 - fitness:10.734073588233926 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:53:37.985 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.734073588233926, number of tests: 2, total length: 50
[MASTER] 21:53:49.621 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.59999883238747 - fitness:10.734073588115066 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:53:49.621 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.734073588115066, number of tests: 2, total length: 49
[MASTER] 21:54:01.811 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.6309509824302 - fitness:10.73253895396029 - branchDistance:0.0 - coverage:7.999984741676585 - ex: 0 - tex: 0
[MASTER] 21:54:01.811 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.73253895396029, number of tests: 2, total length: 48
[MASTER] 21:54:01.997 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.6309509824302 - fitness:10.732523695636875 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:01.997 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.732523695636875, number of tests: 2, total length: 62
[MASTER] 21:54:06.887 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.63095117909936 - fitness:10.7325236857977 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:06.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.7325236857977, number of tests: 2, total length: 49
[MASTER] 21:54:13.209 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.64880826589805 - fitness:10.731630772813824 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:54:13.209 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.731630772813824, number of tests: 2, total length: 52
[MASTER] 21:54:17.500 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.58820346320346 - fitness:10.685921404529443 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:17.500 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.685921404529443, number of tests: 2, total length: 51
[MASTER] 21:54:22.272 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.64880826548017 - fitness:10.683055199800522 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:54:22.272 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.683055199800522, number of tests: 2, total length: 56
[MASTER] 21:54:23.144 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.64880836411561 - fitness:10.68305519514365 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:54:23.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.68305519514365, number of tests: 2, total length: 58
[MASTER] 21:54:23.624 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.64880759319019 - fitness:10.637130524607585 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:54:23.624 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.637130524607585, number of tests: 2, total length: 64
[MASTER] 21:54:25.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.64880814457622 - fitness:10.637130499976287 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:54:25.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.637130499976287, number of tests: 2, total length: 63
[MASTER] 21:54:31.548 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.631110408765274 - fitness:10.594394929433665 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:31.548 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.594394929433665, number of tests: 2, total length: 51
[MASTER] 21:54:35.573 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.64878710655721 - fitness:10.59364632160515 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:35.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.59364632160515, number of tests: 2, total length: 60
[MASTER] 21:54:36.414 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.64880814770408 - fitness:10.593645430931844 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:36.414 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.593645430931844, number of tests: 2, total length: 51
[MASTER] 21:54:37.538 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.64880824751873 - fitness:10.593645426706685 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:37.538 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.593645426706685, number of tests: 2, total length: 51
[MASTER] 21:54:48.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.64880833611664 - fitness:10.59364542295633 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:48.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.59364542295633, number of tests: 2, total length: 50
[MASTER] 21:54:56.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.631110408765274 - fitness:10.553121835566845 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:56.268 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.553121835566845, number of tests: 2, total length: 51
[MASTER] 21:54:57.960 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.64880826535349 - fitness:10.552410624307745 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:57.960 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.552410624307745, number of tests: 2, total length: 51
[MASTER] 21:54:58.919 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.648808270225054 - fitness:10.552410624112063 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:54:58.919 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.552410624112063, number of tests: 2, total length: 51
[MASTER] 21:55:20.996 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.648808273660286 - fitness:10.552410623974078 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:55:20.996 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.552410623974078, number of tests: 2, total length: 51
[MASTER] 21:55:22.327 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.64880831692036 - fitness:10.552410622236412 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:55:22.327 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.552410622236412, number of tests: 2, total length: 51
[MASTER] 21:55:40.696 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.64880833233548 - fitness:10.55241062161722 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:55:40.696 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.55241062161722, number of tests: 2, total length: 50
[MASTER] 21:55:58.279 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 754 generations, 644384 statements, best individuals have fitness: 10.55241062161722
[MASTER] 21:55:58.284 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 82%
* Total number of goals: 28
* Number of covered goals: 23
* Generated 2 tests with total length 50
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     11 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 21:55:58.681 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:55:58.725 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 44 
[MASTER] 21:55:58.899 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 21:55:58.899 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:55:58.899 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 21:55:58.900 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
