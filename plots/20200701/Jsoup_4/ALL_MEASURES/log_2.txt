* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 17211
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 21:18:29.521 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 21:18:29.527 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 28
* Using seed 1593458305797
* Starting evolution
[MASTER] 21:18:30.876 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:121.0 - branchDistance:0.0 - coverage:60.0 - ex: 0 - tex: 20
[MASTER] 21:18:30.876 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 10, total length: 207
[MASTER] 21:18:31.119 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.0 - branchDistance:0.0 - coverage:34.0 - ex: 0 - tex: 16
[MASTER] 21:18:31.119 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.0, number of tests: 9, total length: 225
[MASTER] 21:18:31.237 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.0 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 18
[MASTER] 21:18:31.237 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0, number of tests: 10, total length: 171
[MASTER] 21:18:32.044 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:75.98055200576238 - branchDistance:0.0 - coverage:14.98055200576237 - ex: 0 - tex: 12
[MASTER] 21:18:32.044 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.98055200576238, number of tests: 6, total length: 98
[MASTER] 21:18:33.499 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:74.99996948149052 - branchDistance:0.0 - coverage:13.999969481490524 - ex: 0 - tex: 14
[MASTER] 21:18:33.500 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.99996948149052, number of tests: 8, total length: 167
[MASTER] 21:18:33.704 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:55.0 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 8
[MASTER] 21:18:33.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.0, number of tests: 7, total length: 124
[MASTER] 21:18:34.440 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:36.999969481490524 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 12
[MASTER] 21:18:34.440 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.999969481490524, number of tests: 7, total length: 98
[MASTER] 21:18:37.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.5 - fitness:30.142826624347666 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 12
[MASTER] 21:18:37.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.142826624347666, number of tests: 7, total length: 103
[MASTER] 21:18:38.083 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:22.47365369201684 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 12
[MASTER] 21:18:38.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.47365369201684, number of tests: 6, total length: 120
[MASTER] 21:18:39.444 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:21.181787663308707 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 14
[MASTER] 21:18:39.444 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.181787663308707, number of tests: 7, total length: 102
[MASTER] 21:18:40.990 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:20.199969481490527 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 8
[MASTER] 21:18:40.990 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 20.199969481490527, number of tests: 6, total length: 85
[MASTER] 21:18:42.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:19.42854091006195 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 8
[MASTER] 21:18:42.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.42854091006195, number of tests: 6, total length: 80
[MASTER] 21:18:42.971 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.333333333333334 - fitness:18.806421094393748 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 6
[MASTER] 21:18:42.971 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.806421094393748, number of tests: 6, total length: 76
[MASTER] 21:18:43.380 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.166666666666666 - fitness:17.55693150680698 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 6
[MASTER] 21:18:43.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.55693150680698, number of tests: 5, total length: 78
[MASTER] 21:18:44.575 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.166666666666666 - fitness:17.235263599137582 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 6
[MASTER] 21:18:44.575 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.235263599137582, number of tests: 6, total length: 100
[MASTER] 21:18:46.028 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.166666666666666 - fitness:16.95601343753448 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 4
[MASTER] 21:18:46.028 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.95601343753448, number of tests: 5, total length: 71
[MASTER] 21:18:46.327 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.166666666666668 - fitness:16.495115112558487 - branchDistance:0.0 - coverage:11.999969481490524 - ex: 0 - tex: 6
[MASTER] 21:18:46.327 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.495115112558487, number of tests: 6, total length: 75
[MASTER] 21:18:47.260 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.5 - fitness:12.636333119716806 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:18:47.260 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.636333119716806, number of tests: 6, total length: 85
[MASTER] 21:18:47.550 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.166666666666668 - fitness:12.49511511442113 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:18:47.550 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.49511511442113, number of tests: 6, total length: 82
[MASTER] 21:18:48.120 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.5 - fitness:12.428540911924598 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:18:48.121 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.428540911924598, number of tests: 6, total length: 86
[MASTER] 21:18:48.510 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.5 - fitness:12.243212726596413 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:18:48.511 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.243212726596413, number of tests: 6, total length: 84
[MASTER] 21:18:48.981 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.166666666666668 - fitness:12.130404265961864 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:18:48.982 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.130404265961864, number of tests: 7, total length: 115
[MASTER] 21:18:51.349 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.5 - fitness:12.076892560276246 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:18:51.350 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.076892560276246, number of tests: 4, total length: 81
[MASTER] 21:18:54.081 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.5 - fitness:11.790667157771773 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:18:54.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.790667157771773, number of tests: 4, total length: 74
[MASTER] 21:18:56.361 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.5 - fitness:11.666636150019837 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:18:56.361 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.666636150019837, number of tests: 4, total length: 68
[MASTER] 21:18:58.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.5 - fitness:11.55316097271487 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:18:58.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.55316097271487, number of tests: 4, total length: 65
[MASTER] 21:19:00.031 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.5 - fitness:11.448949075189903 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:19:00.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.448949075189903, number of tests: 4, total length: 66
[MASTER] 21:19:01.186 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.5 - fitness:11.352910659823758 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:19:01.186 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.352910659823758, number of tests: 4, total length: 68
[MASTER] 21:19:02.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.575756154109573 - fitness:11.345941177736838 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:19:02.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.345941177736838, number of tests: 4, total length: 67
[MASTER] 21:19:02.860 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.575757575757574 - fitness:11.345941047334211 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:02.860 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.345941047334211, number of tests: 4, total length: 64
[MASTER] 21:19:04.193 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.5 - fitness:11.264120426749395 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:19:04.193 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.264120426749395, number of tests: 4, total length: 70
[MASTER] 21:19:04.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.575757575757574 - fitness:11.25766617662569 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:04.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.25766617662569, number of tests: 4, total length: 63
[MASTER] 21:19:05.169 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.575757575757574 - fitness:11.175793659177344 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:05.169 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.175793659177344, number of tests: 4, total length: 63
[MASTER] 21:19:06.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.575757575757574 - fitness:11.099651349737051 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:19:06.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.099651349737051, number of tests: 5, total length: 104
[MASTER] 21:19:07.937 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.575757575757574 - fitness:11.028658007943333 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:19:07.937 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.028658007943333, number of tests: 6, total length: 126
[MASTER] 21:19:10.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.616666666666667 - fitness:11.025855808620474 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 6
[MASTER] 21:19:10.721 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 11.025855808620474, number of tests: 5, total length: 112
[MASTER] 21:19:13.356 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.575757575757574 - fitness:10.962308432808076 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 8
[MASTER] 21:19:13.356 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.962308432808076, number of tests: 5, total length: 87
[MASTER] 21:19:14.866 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.616666666666667 - fitness:10.95968641312998 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:14.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.95968641312998, number of tests: 4, total length: 77
[MASTER] 21:19:17.043 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.616666666666667 - fitness:10.897702746400086 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:17.043 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.897702746400086, number of tests: 4, total length: 77
[MASTER] 21:19:20.313 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.616666666666667 - fitness:10.839519815494201 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:20.313 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.839519815494201, number of tests: 4, total length: 64
[MASTER] 21:19:24.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.61666666666667 - fitness:10.784798437245088 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:24.180 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.784798437245088, number of tests: 4, total length: 61
[MASTER] 21:19:26.597 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.61666666666667 - fitness:10.733238621533236 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:26.597 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.733238621533236, number of tests: 4, total length: 61
[MASTER] 21:19:27.331 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.61666666666667 - fitness:10.684574069221208 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:19:27.331 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.684574069221208, number of tests: 5, total length: 96
[MASTER] 21:19:29.342 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.525757575757574 - fitness:10.642645884398508 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:29.342 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.642645884398508, number of tests: 4, total length: 61
[MASTER] 21:19:29.747 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.61666666666667 - fitness:10.6385675716554 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:19:29.747 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.6385675716554, number of tests: 3, total length: 55
[MASTER] 21:19:38.385 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.10769230769231 - fitness:10.616884906238742 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:19:38.385 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.616884906238742, number of tests: 3, total length: 50
[MASTER] 21:19:53.812 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.74492174492175 - fitness:10.589587297518396 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:19:53.812 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.589587297518396, number of tests: 2, total length: 45
[MASTER] 21:19:59.371 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 37.74492174492175 - fitness:10.548559525553063 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:19:59.371 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.548559525553063, number of tests: 2, total length: 43
[MASTER] 21:20:01.242 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.74492174492175 - fitness:10.509596307757882 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:20:01.242 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.509596307757882, number of tests: 2, total length: 44
[MASTER] 21:20:05.254 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.75238095238095 - fitness:10.509313038695765 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:20:05.254 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.509313038695765, number of tests: 2, total length: 44
[MASTER] 21:20:09.380 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.75821709155043 - fitness:10.509091480998064 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:20:09.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.509091480998064, number of tests: 2, total length: 44
[MASTER] 21:20:12.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 38.761216976170246 - fitness:10.50897762150848 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:20:12.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.50897762150848, number of tests: 3, total length: 90
[MASTER] 21:20:34.373 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 39.761216976170246 - fitness:10.4719569373852 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:20:34.373 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.4719569373852, number of tests: 2, total length: 43
[MASTER] 21:20:38.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 45.341303556256825 - fitness:10.294710791766342 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:20:38.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.294710791766342, number of tests: 2, total length: 51
[MASTER] 21:20:43.101 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 45.39620800140517 - fitness:10.293178617991561 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:20:43.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.293178617991561, number of tests: 3, total length: 69
[MASTER] 21:20:46.531 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 45.39875107470926 - fitness:10.293107738376253 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 4
[MASTER] 21:20:46.531 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.293107738376253, number of tests: 4, total length: 97
[MASTER] 21:20:49.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 46.39875107470926 - fitness:10.265825621744428 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:20:49.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.265825621744428, number of tests: 3, total length: 69
[MASTER] 21:21:03.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 46.42388763884091 - fitness:10.26515466717726 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:21:03.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.26515466717726, number of tests: 2, total length: 59
[MASTER] 21:21:07.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 46.426430712145 - fitness:10.265086826131299 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:21:07.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.265086826131299, number of tests: 2, total length: 45
[MASTER] 21:21:31.837 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 47.426430712145 - fitness:10.238962304373786 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:21:31.837 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.238962304373786, number of tests: 2, total length: 50
[MASTER] 21:21:36.223 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 49.926430712145 - fitness:10.178139623317271 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:21:36.223 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.178139623317271, number of tests: 2, total length: 47
[MASTER] 21:21:42.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 49.981335157293344 - fitness:10.176870790760699 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:21:42.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.176870790760699, number of tests: 2, total length: 45
[MASTER] 21:21:57.045 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 51.912839193326995 - fitness:10.13390976920286 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:21:57.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.13390976920286, number of tests: 2, total length: 47
[MASTER] 21:22:03.119 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 51.95365551985761 - fitness:10.133035735549866 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:22:03.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.133035735549866, number of tests: 2, total length: 47
[MASTER] 21:22:13.667 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 52.95365551985761 - fitness:10.112035003694363 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:22:13.667 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.112035003694363, number of tests: 3, total length: 57
[MASTER] 21:22:51.068 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 52.988494427362184 - fitness:10.111317385094083 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:22:51.068 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.111317385094083, number of tests: 2, total length: 48
[MASTER] 21:22:52.609 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 53.95365551985761 - fitness:10.091798578841074 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 0
[MASTER] 21:22:52.609 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.091798578841074, number of tests: 2, total length: 49
[MASTER] 21:23:05.478 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 53.975633541835634 - fitness:10.091362090122146 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:23:05.478 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.091362090122146, number of tests: 3, total length: 86
[MASTER] 21:23:24.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 54.0049604836978 - fitness:10.090780193375997 - branchDistance:0.0 - coverage:7.999969483353169 - ex: 0 - tex: 2
[MASTER] 21:23:24.903 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.090780193375997, number of tests: 3, total length: 83

[MASTER] 21:23:30.576 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 661 generations, 520248 statements, best individuals have fitness: 10.090780193375997
[MASTER] 21:23:30.580 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 82%
* Total number of goals: 28
* Number of covered goals: 23
* Generated 3 tests with total length 59
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     10 / 0           
[MASTER] 21:23:31.033 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:23:31.083 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 53 
[MASTER] 21:23:31.186 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 21:23:31.186 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:23:31.186 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 21:23:31.186 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 21:23:31.187 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 28
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
