* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.nodes.Entities
* Starting client
* Connecting to master process on port 15597
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.nodes.Entities
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 28
[MASTER] 21:13:00.383 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:13:01.943 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("O&gt;C7&rcub;&equals;V&Hat;kpNV1", var9);  // (Primitive) Original Value: O&gt;C7&rcub;&equals;V&Hat;kpNV1 | Regression Value: O&gt;C7&rcub;&equals;V&hat;kpNV1
[MASTER] 21:13:01.947 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 21:13:01.947 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 21:13:03.465 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 21:13:03.485 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("O&gt;C7&rcub;&equals;V&Hat;kpNV1", var9);  // (Primitive) Original Value: O&gt;C7&rcub;&equals;V&Hat;kpNV1 | Regression Value: O&gt;C7&rcub;&equals;V&hat;kpNV1
Generated test with 1 assertions.
[MASTER] 21:13:03.487 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 21:13:03.487 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 21:13:03.903 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/nodes/Entities_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 21:13:03.926 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("O&gt;C7&rcub;&equals;V&Hat;kpNV1", var9);  // (Primitive) Original Value: O&gt;C7&rcub;&equals;V&Hat;kpNV1 | Regression Value: O&gt;C7&rcub;&equals;V&hat;kpNV1
[MASTER] 21:13:03.928 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 21:13:03.928 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 21:13:03.928 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 42 | Tests with assertion: 1
* Generated 1 tests with total length 10
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          3 / 300         
[MASTER] 21:13:04.697 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:13:04.726 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 21:13:04.741 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 21:13:04.792 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@118c4642] invalid assertion(s) to be removed
[MASTER] 21:13:04.866 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.PrimitiveAssertion@1eb3c2bb] invalid assertion(s) to be removed
[MASTER] 21:13:04.912 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
[MASTER] 21:13:04.917 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 32%
* Total number of goals: 28
* Number of covered goals: 9
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Entities_ESTest' to evosuite-tests
* Done!

* Computation finished
