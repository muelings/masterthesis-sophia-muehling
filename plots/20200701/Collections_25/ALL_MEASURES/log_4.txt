* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.IteratorUtils
* Starting client
* Connecting to master process on port 11995
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_25_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.IteratorUtils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 00:32:31.653 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 00:32:31.659 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 170
* Using seed 1593383546901
* Starting evolution
[MASTER] 00:35:22.558 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:727.0 - branchDistance:0.0 - coverage:336.0 - ex: 0 - tex: 20
[MASTER] 00:35:22.558 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 727.0, number of tests: 10, total length: 326
[MASTER] 00:35:23.193 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:715.0 - branchDistance:0.0 - coverage:324.0 - ex: 0 - tex: 12
[MASTER] 00:35:23.193 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 715.0, number of tests: 8, total length: 215
[MASTER] 00:35:23.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:712.0 - branchDistance:0.0 - coverage:321.0 - ex: 0 - tex: 12
[MASTER] 00:35:23.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 712.0, number of tests: 8, total length: 276
[MASTER] 00:35:23.746 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:708.0 - branchDistance:0.0 - coverage:317.0 - ex: 0 - tex: 14
[MASTER] 00:35:23.746 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 708.0, number of tests: 7, total length: 267
[MASTER] 00:35:25.605 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:691.0 - branchDistance:0.0 - coverage:300.0 - ex: 0 - tex: 18
[MASTER] 00:35:25.605 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 691.0, number of tests: 9, total length: 298
[MASTER] 00:35:28.925 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:679.0 - branchDistance:0.0 - coverage:288.0 - ex: 0 - tex: 16
[MASTER] 00:35:28.925 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 679.0, number of tests: 10, total length: 299
[MASTER] 00:35:34.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:673.0 - branchDistance:0.0 - coverage:282.0 - ex: 0 - tex: 12
[MASTER] 00:35:34.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 673.0, number of tests: 7, total length: 232
[MASTER] 00:35:38.358 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:667.0 - branchDistance:0.0 - coverage:276.0 - ex: 0 - tex: 12
[MASTER] 00:35:38.358 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 667.0, number of tests: 7, total length: 220
[MASTER] 00:35:39.028 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:666.0 - branchDistance:0.0 - coverage:275.0 - ex: 0 - tex: 18
[MASTER] 00:35:39.028 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 666.0, number of tests: 9, total length: 376
[MASTER] 00:35:40.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:660.0 - branchDistance:0.0 - coverage:269.0 - ex: 0 - tex: 18
[MASTER] 00:35:40.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 660.0, number of tests: 10, total length: 284
[MASTER] 00:35:47.662 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:640.0 - branchDistance:0.0 - coverage:249.0 - ex: 0 - tex: 16
[MASTER] 00:35:47.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 640.0, number of tests: 9, total length: 315
[MASTER] 00:35:56.620 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:637.3181818181818 - branchDistance:0.0 - coverage:246.3181818181818 - ex: 0 - tex: 16
[MASTER] 00:35:56.620 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 637.3181818181818, number of tests: 9, total length: 351
[MASTER] 00:36:05.349 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:631.0 - branchDistance:0.0 - coverage:240.0 - ex: 0 - tex: 16
[MASTER] 00:36:05.349 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 631.0, number of tests: 10, total length: 336
[MASTER] 00:36:12.823 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:629.0 - branchDistance:0.0 - coverage:238.0 - ex: 0 - tex: 16
[MASTER] 00:36:12.823 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 629.0, number of tests: 9, total length: 200
[MASTER] 00:36:13.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:401.5 - branchDistance:0.0 - coverage:238.0 - ex: 0 - tex: 16
[MASTER] 00:36:13.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 401.5, number of tests: 9, total length: 204
[MASTER] 00:36:23.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:398.8181818181818 - branchDistance:0.0 - coverage:235.3181818181818 - ex: 0 - tex: 16
[MASTER] 00:36:23.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 398.8181818181818, number of tests: 9, total length: 240
[MASTER] 00:36:38.675 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:396.8181818181818 - branchDistance:0.0 - coverage:233.3181818181818 - ex: 0 - tex: 18
[MASTER] 00:36:38.675 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 396.8181818181818, number of tests: 10, total length: 246
[MASTER] 00:36:41.577 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:395.8181818181818 - branchDistance:0.0 - coverage:232.3181818181818 - ex: 0 - tex: 18
[MASTER] 00:36:41.577 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 395.8181818181818, number of tests: 11, total length: 246
[MASTER] 00:36:50.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:389.8181818181818 - branchDistance:0.0 - coverage:226.3181818181818 - ex: 0 - tex: 18
[MASTER] 00:36:50.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 389.8181818181818, number of tests: 11, total length: 250
[MASTER] 00:36:56.837 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:388.8181818181818 - branchDistance:0.0 - coverage:225.3181818181818 - ex: 0 - tex: 18
[MASTER] 00:36:56.837 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 388.8181818181818, number of tests: 12, total length: 298
[MASTER] 00:36:57.847 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:387.8181818181818 - branchDistance:0.0 - coverage:224.3181818181818 - ex: 0 - tex: 18
[MASTER] 00:36:57.847 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 387.8181818181818, number of tests: 11, total length: 246
[MASTER] 00:37:01.835 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:381.8181818181818 - branchDistance:0.0 - coverage:218.3181818181818 - ex: 0 - tex: 18
[MASTER] 00:37:01.835 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 381.8181818181818, number of tests: 11, total length: 253
[MASTER] 00:37:12.440 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:376.8181818181818 - branchDistance:0.0 - coverage:213.3181818181818 - ex: 0 - tex: 22
[MASTER] 00:37:12.440 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 376.8181818181818, number of tests: 13, total length: 340
[MASTER] 00:37:29.502 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:371.8181818181818 - branchDistance:0.0 - coverage:208.3181818181818 - ex: 0 - tex: 24
[MASTER] 00:37:29.502 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 371.8181818181818, number of tests: 14, total length: 349
[MASTER] 00:37:32.623 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.499047619047619 - fitness:369.377633037694 - branchDistance:0.0 - coverage:212.3181818181818 - ex: 0 - tex: 20
[MASTER] 00:37:32.624 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 369.377633037694, number of tests: 12, total length: 284
[MASTER] 00:37:32.750 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 31 generations, 37214 statements, best individuals have fitness: 369.377633037694
[MASTER] 00:37:32.754 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 38%
* Total number of goals: 170
* Number of covered goals: 64
* Generated 12 tests with total length 284
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                    369 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 00:37:33.394 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:37:33.434 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 226 
[MASTER] 00:37:33.578 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.578 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.579 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.579 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.579 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.579 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.579 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.580 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.580 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.580 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 170
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
* Number of covered goals: 0
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
* Generated 0 tests with total length 0
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
* Resulting test suite's coverage: 0%
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
* Compiling and checking tests
[MASTER] 00:37:33.581 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 00:37:33.582 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 00:37:33.584 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Writing JUnit test case 'IteratorUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
