* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.collections4.IteratorUtils
* Starting client
* Connecting to master process on port 14995
* Analyzing classpath: 
  - ../defects4j_compiled/Collections_25_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.collections4.IteratorUtils
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 00:15:13.456 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 00:15:13.464 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 170
* Using seed 1593382508731
* Starting evolution
[MASTER] 00:18:00.740 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:724.0 - branchDistance:0.0 - coverage:333.0 - ex: 0 - tex: 14
[MASTER] 00:18:00.740 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 724.0, number of tests: 9, total length: 201
[MASTER] 00:18:00.833 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:723.0 - branchDistance:0.0 - coverage:332.0 - ex: 0 - tex: 8
[MASTER] 00:18:00.833 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 723.0, number of tests: 4, total length: 90
[MASTER] 00:18:31.487 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:704.0 - branchDistance:0.0 - coverage:313.0 - ex: 0 - tex: 16
[MASTER] 00:18:31.487 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 704.0, number of tests: 10, total length: 196
[MASTER] 00:18:31.848 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:686.0 - branchDistance:0.0 - coverage:295.0 - ex: 0 - tex: 16
[MASTER] 00:18:31.848 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 686.0, number of tests: 10, total length: 219
[MASTER] 00:18:58.074 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999909989378746 - fitness:499.0005616682989 - branchDistance:0.0 - coverage:342.0 - ex: 0 - tex: 6
[MASTER] 00:18:58.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 499.0005616682989, number of tests: 5, total length: 134
[MASTER] 00:19:12.154 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999909989378746 - fitness:491.0005616682989 - branchDistance:0.0 - coverage:334.0 - ex: 0 - tex: 8
[MASTER] 00:19:12.154 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 491.0005616682989, number of tests: 5, total length: 116
[MASTER] 00:19:27.099 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999988944681397 - fitness:470.0000689852186 - branchDistance:0.0 - coverage:313.0 - ex: 0 - tex: 14
[MASTER] 00:19:27.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 470.0000689852186, number of tests: 10, total length: 276
[MASTER] 00:19:42.400 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:467.4555555555555 - branchDistance:0.0 - coverage:303.9555555555555 - ex: 0 - tex: 14
[MASTER] 00:19:42.400 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 467.4555555555555, number of tests: 8, total length: 199
[MASTER] 00:19:52.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:465.4555555555555 - branchDistance:0.0 - coverage:301.9555555555555 - ex: 0 - tex: 14
[MASTER] 00:19:52.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 465.4555555555555, number of tests: 9, total length: 246
[MASTER] 00:19:59.564 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999988944681397 - fitness:456.0000689852186 - branchDistance:0.0 - coverage:299.0 - ex: 0 - tex: 10
[MASTER] 00:19:59.564 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 456.0000689852186, number of tests: 6, total length: 129
[MASTER] 00:20:06.330 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:445.5 - branchDistance:0.0 - coverage:282.0 - ex: 0 - tex: 14
[MASTER] 00:20:06.330 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 445.5, number of tests: 8, total length: 219
[MASTER] 00:20:14.179 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4999988944681397 - fitness:436.0000689852186 - branchDistance:0.0 - coverage:279.0 - ex: 0 - tex: 12
[MASTER] 00:20:14.179 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 436.0000689852186, number of tests: 9, total length: 245

* Search finished after 303s and 10 generations, 14472 statements, best individuals have fitness: 436.0000689852186
[MASTER] 00:20:16.962 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:20:16.967 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 25%
* Total number of goals: 170
* Number of covered goals: 43
* Generated 9 tests with total length 245
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        304 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                    436 / 0           
[MASTER] 00:20:17.600 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:20:17.643 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 199 
[MASTER] 00:20:17.742 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 00:20:17.742 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 00:20:17.743 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 00:20:17.745 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 170
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'IteratorUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
