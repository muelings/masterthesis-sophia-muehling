* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.TypeHandler
* Starting client
* Connecting to master process on port 2509
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_3_fixed/target/classes/
[MASTER] 06:42:04.893 [logback-1] WARN  CheapPurityAnalyzer - org.apache.commons.lang.math.NumberUtils was not found in the inheritance tree. Using DEFAULT value for cheap-purity analysis
[MASTER] 06:42:04.894 [logback-1] WARN  InheritanceTree - Class not in inheritance graph: org.apache.commons.lang.math.NumberUtils
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.TypeHandler
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 06:42:04.977 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 31
[MASTER] 06:42:05.126 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 06:42:06.340 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:42:06.363 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 06:42:06.601 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:42:06.610 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 06:42:06.610 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 3 | Tests with assertion: 1
* Generated 1 tests with total length 7
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          1 / 300         
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 06:42:06.910 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:42:06.924 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 5 
[MASTER] 06:42:06.935 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 06:42:06.936 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createValue:ClassCastException, createNumber:NoClassDefFoundError, ClassCastException:createValue-47,]
[MASTER] 06:42:06.936 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createValue:ClassCastException at 4
[MASTER] 06:42:06.936 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createValue:ClassCastException, createNumber:NoClassDefFoundError, ClassCastException:createValue-47,]
[MASTER] 06:42:06.936 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: createNumber:NoClassDefFoundError at 1
[MASTER] 06:42:06.936 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createValue:ClassCastException, createNumber:NoClassDefFoundError, ClassCastException:createValue-47,]
[MASTER] 06:42:07.002 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 06:42:07.004 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 10%
* Total number of goals: 31
* Number of covered goals: 3
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeHandler_ESTest' to evosuite-tests
* Done!

* Computation finished
