/*
 * This file was automatically generated by EvoSuite
 * Tue Jun 30 05:13:24 GMT 2020
 */

package org.apache.commons.cli;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.cli.TypeHandler;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class TypeHandler_ESTest extends TypeHandler_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Object object0 = TypeHandler.createObject("org.apache.commons.cli.PatternOptionBuilder");
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NoClassDefFoundError : org/apache/commons/lang/math/NumberUtils
      TypeHandler.createValue("/SpcXRCF8_w(t9u", ((PatternOptionBuilder) object0).NUMBER_VALUE);
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NoClassDefFoundError : org/apache/commons/lang/math/NumberUtils
      TypeHandler.createNumber("a6Z~:cA8O^q|HzB");
  }
}
