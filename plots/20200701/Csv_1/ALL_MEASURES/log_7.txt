* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 11048
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:27:07.881 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:27:07.886 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 30
* Using seed 1593415625701
* Starting evolution
[MASTER] 09:27:09.154 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:103.76145833333334 - branchDistance:0.0 - coverage:40.76145833333334 - ex: 0 - tex: 8
[MASTER] 09:27:09.154 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.76145833333334, number of tests: 5, total length: 120
[MASTER] 09:27:09.540 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:88.66525459991442 - branchDistance:0.0 - coverage:25.66525459991442 - ex: 0 - tex: 8
[MASTER] 09:27:09.540 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.66525459991442, number of tests: 5, total length: 117
[MASTER] 09:27:10.448 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:87.2547619047619 - branchDistance:0.0 - coverage:24.254761904761903 - ex: 0 - tex: 10
[MASTER] 09:27:10.448 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.2547619047619, number of tests: 8, total length: 155
[MASTER] 09:27:15.455 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:86.56525459991443 - branchDistance:0.0 - coverage:23.565254599914418 - ex: 0 - tex: 2
[MASTER] 09:27:15.455 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.56525459991443, number of tests: 2, total length: 52
[MASTER] 09:27:18.611 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.16525459991442 - branchDistance:0.0 - coverage:19.16525459991442 - ex: 0 - tex: 12
[MASTER] 09:27:18.611 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.16525459991442, number of tests: 8, total length: 226
[MASTER] 09:27:23.088 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.08809523809524 - branchDistance:0.0 - coverage:19.08809523809524 - ex: 0 - tex: 14
[MASTER] 09:27:23.088 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.08809523809524, number of tests: 10, total length: 236
[MASTER] 09:27:25.063 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.99858793324775 - branchDistance:0.0 - coverage:18.99858793324775 - ex: 0 - tex: 12
[MASTER] 09:27:25.063 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.99858793324775, number of tests: 8, total length: 229
[MASTER] 09:27:29.245 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.92142857142858 - branchDistance:0.0 - coverage:18.92142857142857 - ex: 0 - tex: 12
[MASTER] 09:27:29.245 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.92142857142858, number of tests: 10, total length: 226
[MASTER] 09:28:47.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 0
[MASTER] 09:28:47.851 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 4, total length: 78
[MASTER] 09:32:08.900 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 195 generations, 169968 statements, best individuals have fitness: 81.91304347826087
[MASTER] 09:32:08.903 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 3 tests with total length 21
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     82 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 09:32:09.175 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:32:09.284 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 18 
[MASTER] 09:32:09.304 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:32:09.304 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:32:09.304 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:32:09.304 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 09:32:09.305 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
