* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 14197
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 08:26:00.790 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:26:00.795 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
* Using seed 1593411958364
* Starting evolution
[MASTER] 08:26:02.424 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:99.0 - branchDistance:0.0 - coverage:36.0 - ex: 0 - tex: 18
[MASTER] 08:26:02.424 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.0, number of tests: 10, total length: 219
[MASTER] 08:26:03.998 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.50313101367868 - branchDistance:0.0 - coverage:26.50313101367868 - ex: 0 - tex: 12
[MASTER] 08:26:03.998 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.50313101367868, number of tests: 8, total length: 186
[MASTER] 08:26:10.391 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.05411140583554 - branchDistance:0.0 - coverage:26.054111405835542 - ex: 0 - tex: 4
[MASTER] 08:26:10.391 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.05411140583554, number of tests: 5, total length: 77
[MASTER] 08:26:15.463 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:88.89316239316238 - branchDistance:0.0 - coverage:25.89316239316239 - ex: 0 - tex: 8
[MASTER] 08:26:15.463 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.89316239316238, number of tests: 8, total length: 163
[MASTER] 08:26:16.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:87.62077807250222 - branchDistance:0.0 - coverage:24.620778072502212 - ex: 0 - tex: 16
[MASTER] 08:26:16.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.62077807250222, number of tests: 9, total length: 240
[MASTER] 08:26:16.313 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.65982905982906 - branchDistance:0.0 - coverage:19.65982905982906 - ex: 0 - tex: 10
[MASTER] 08:26:16.313 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.65982905982906, number of tests: 7, total length: 152
[MASTER] 08:26:19.640 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.40982905982906 - branchDistance:0.0 - coverage:19.40982905982906 - ex: 0 - tex: 12
[MASTER] 08:26:19.640 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.40982905982906, number of tests: 7, total length: 148
[MASTER] 08:26:20.760 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.22077807250221 - branchDistance:0.0 - coverage:19.22077807250221 - ex: 0 - tex: 10
[MASTER] 08:26:20.760 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.22077807250221, number of tests: 8, total length: 189
[MASTER] 08:26:22.298 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.15982905982906 - branchDistance:0.0 - coverage:19.15982905982906 - ex: 0 - tex: 8
[MASTER] 08:26:22.298 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.15982905982906, number of tests: 7, total length: 146
[MASTER] 08:26:23.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.07971014492753 - branchDistance:0.0 - coverage:19.079710144927535 - ex: 0 - tex: 6
[MASTER] 08:26:23.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.07971014492753, number of tests: 6, total length: 120
[MASTER] 08:27:01.561 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 2
[MASTER] 08:27:01.561 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 5, total length: 90
[MASTER] 08:31:01.834 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 183 generations, 139536 statements, best individuals have fitness: 81.91304347826087
[MASTER] 08:31:01.837 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 4 tests with total length 29
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     82 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 08:31:02.236 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:31:02.363 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 24 
[MASTER] 08:31:02.394 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:31:02.394 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:31:02.394 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 08:31:02.394 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 08:31:02.394 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 08:31:02.395 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
