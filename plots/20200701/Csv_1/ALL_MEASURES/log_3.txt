* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 14764
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:46:24.889 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:46:24.895 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 30
* Using seed 1593413182382
* Starting evolution
[MASTER] 08:46:26.343 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:104.0 - branchDistance:0.0 - coverage:41.0 - ex: 0 - tex: 10
[MASTER] 08:46:26.343 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.0, number of tests: 7, total length: 208
[MASTER] 08:46:27.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:103.95944444444444 - branchDistance:0.0 - coverage:40.95944444444444 - ex: 0 - tex: 14
[MASTER] 08:46:27.720 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.95944444444444, number of tests: 8, total length: 165
[MASTER] 08:46:28.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:97.76666666666667 - branchDistance:0.0 - coverage:34.766666666666666 - ex: 0 - tex: 10
[MASTER] 08:46:28.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.76666666666667, number of tests: 9, total length: 179
[MASTER] 08:46:30.924 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:94.82909090909091 - branchDistance:0.0 - coverage:31.829090909090908 - ex: 0 - tex: 14
[MASTER] 08:46:30.924 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.82909090909091, number of tests: 8, total length: 179
[MASTER] 08:46:35.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.69824561403509 - branchDistance:0.0 - coverage:26.698245614035088 - ex: 0 - tex: 8
[MASTER] 08:46:35.646 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.69824561403509, number of tests: 7, total length: 183
[MASTER] 08:46:39.882 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.44824561403509 - branchDistance:0.0 - coverage:26.448245614035088 - ex: 0 - tex: 14
[MASTER] 08:46:39.882 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.44824561403509, number of tests: 9, total length: 231
[MASTER] 08:46:41.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:85.0 - branchDistance:0.0 - coverage:22.0 - ex: 0 - tex: 8
[MASTER] 08:46:41.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.0, number of tests: 5, total length: 125
[MASTER] 08:46:42.470 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:84.69824561403509 - branchDistance:0.0 - coverage:21.698245614035088 - ex: 0 - tex: 6
[MASTER] 08:46:42.471 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.69824561403509, number of tests: 7, total length: 132
[MASTER] 08:46:42.841 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.65689223057645 - branchDistance:0.0 - coverage:20.656892230576442 - ex: 0 - tex: 12
[MASTER] 08:46:42.841 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.65689223057645, number of tests: 9, total length: 194
[MASTER] 08:46:47.182 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.44824561403509 - branchDistance:0.0 - coverage:20.448245614035088 - ex: 0 - tex: 8
[MASTER] 08:46:47.182 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.44824561403509, number of tests: 8, total length: 136
[MASTER] 08:46:50.219 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.3652380952381 - branchDistance:0.0 - coverage:20.365238095238094 - ex: 0 - tex: 8
[MASTER] 08:46:50.219 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.3652380952381, number of tests: 9, total length: 160
[MASTER] 08:46:51.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.99022556390977 - branchDistance:0.0 - coverage:19.990225563909775 - ex: 0 - tex: 16
[MASTER] 08:46:51.050 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.99022556390977, number of tests: 10, total length: 257
[MASTER] 08:46:51.321 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.65689223057645 - branchDistance:0.0 - coverage:19.656892230576442 - ex: 0 - tex: 12
[MASTER] 08:46:51.321 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.65689223057645, number of tests: 9, total length: 194
[MASTER] 08:46:53.195 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.6152380952381 - branchDistance:0.0 - coverage:19.615238095238094 - ex: 0 - tex: 10
[MASTER] 08:46:53.195 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.6152380952381, number of tests: 10, total length: 201
[MASTER] 08:46:55.948 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.99022556390977 - branchDistance:0.0 - coverage:18.990225563909775 - ex: 0 - tex: 12
[MASTER] 08:46:55.948 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.99022556390977, number of tests: 10, total length: 217
[MASTER] 08:46:58.162 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.94857142857143 - branchDistance:0.0 - coverage:18.948571428571427 - ex: 0 - tex: 14
[MASTER] 08:46:58.162 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.94857142857143, number of tests: 10, total length: 215
[MASTER] 08:47:05.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.93612040133779 - branchDistance:0.0 - coverage:18.936120401337792 - ex: 0 - tex: 12
[MASTER] 08:47:05.143 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.93612040133779, number of tests: 10, total length: 218
[MASTER] 08:47:09.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 12
[MASTER] 08:47:09.720 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 10, total length: 222

* Search finished after 302s and 209 generations, 151394 statements, best individuals have fitness: 81.91304347826087
[MASTER] 08:51:26.058 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:51:26.064 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 5 tests with total length 32
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     82 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 08:51:26.490 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:51:26.638 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 25 
[MASTER] 08:51:26.699 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:51:26.699 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:51:26.699 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 08:51:26.699 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 08:51:26.699 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 08:51:26.699 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:51:26.701 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
