/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 29 03:54:51 GMT 2020
 */

package org.apache.commons.collections4.trie;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.util.AbstractMap;
import org.apache.commons.collections4.trie.AbstractPatriciaTrie;
import org.apache.commons.collections4.trie.PatriciaTrie;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class AbstractPatriciaTrie_ESTest extends AbstractPatriciaTrie_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      PatriciaTrie<AbstractMap.SimpleEntry<Integer, Object>> patriciaTrie0 = new PatriciaTrie<AbstractMap.SimpleEntry<Integer, Object>>();
      Integer integer0 = new Integer(474);
      Integer integer1 = new Integer(0);
      AbstractMap.SimpleImmutableEntry<Integer, Object> abstractMap_SimpleImmutableEntry0 = new AbstractMap.SimpleImmutableEntry<Integer, Object>(integer0, integer0);
      AbstractMap.SimpleImmutableEntry<Integer, Object> abstractMap_SimpleImmutableEntry1 = new AbstractMap.SimpleImmutableEntry<Integer, Object>(abstractMap_SimpleImmutableEntry0);
      AbstractMap.SimpleEntry<Integer, Object> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<Integer, Object>(abstractMap_SimpleImmutableEntry1);
      patriciaTrie0.put("  ", abstractMap_SimpleEntry0);
      AbstractMap.SimpleEntry<Integer, Object> abstractMap_SimpleEntry1 = new AbstractMap.SimpleEntry<Integer, Object>(integer0, integer1);
      abstractMap_SimpleEntry1.setValue(integer0);
      abstractMap_SimpleEntry1.setValue(patriciaTrie0);
      AbstractPatriciaTrie.TrieEntry<String, AbstractMap.SimpleEntry<Integer, Object>> abstractPatriciaTrie_TrieEntry0 = new AbstractPatriciaTrie.TrieEntry<String, AbstractMap.SimpleEntry<Integer, Object>>("  ", abstractMap_SimpleEntry1, 474);
      abstractPatriciaTrie_TrieEntry0.key = "}\n";
      abstractPatriciaTrie_TrieEntry0.value = abstractMap_SimpleEntry1;
      patriciaTrie0.nextEntry(abstractPatriciaTrie_TrieEntry0);
      PatriciaTrie<String> patriciaTrie1 = new PatriciaTrie<String>();
      Integer integer2 = new Integer(2);
      // Undeclared exception!
      try { 
        patriciaTrie1.containsKey(integer2);
        fail("Expecting exception: ClassCastException");
      
      } catch(ClassCastException e) {
         //
         // java.lang.Integer cannot be cast to java.lang.String
         //
         verifyException("org.apache.commons.collections4.trie.analyzer.StringKeyAnalyzer", e);
         assertTrue(e.getMessage().equals("java.lang.Integer cannot be cast to java.lang.String"));   
      }
  }
}
