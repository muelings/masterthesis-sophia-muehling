/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 29 02:54:08 GMT 2020
 */

package org.apache.commons.collections4.trie;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.apache.commons.collections4.trie.PatriciaTrie;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class AbstractPatriciaTrie_ESTest extends AbstractPatriciaTrie_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      PatriciaTrie<Object> patriciaTrie0 = new PatriciaTrie<Object>();
      Integer integer0 = new Integer((-3618));
      // Undeclared exception!
      try { 
        patriciaTrie0.remove((Object) integer0);
        fail("Expecting exception: ClassCastException");
      
      } catch(ClassCastException e) {
         //
         // no message in exception (getMessage() returned null)
         //
      }
  }
}
