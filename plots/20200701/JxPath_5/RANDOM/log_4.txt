* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.NodePointer
* Starting client
* Connecting to master process on port 16608
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.NodePointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 158
[MASTER] 00:32:37.941 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:33:17.784 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 00:33:19.025 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 2 assertions.
[MASTER] 00:33:19.036 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 00:33:19.286 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 00:33:19.296 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 00:33:19.296 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 9324 | Tests with assertion: 1
* Generated 1 tests with total length 10
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                         41 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 00:33:19.874 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:33:19.886 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 00:33:19.892 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 00:33:19.892 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [compareTo:JXPathException, remove:UnsupportedOperationException, UnsupportedOperationException,]
[MASTER] 00:33:19.892 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: remove:UnsupportedOperationException at 8
[MASTER] 00:33:19.892 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [compareTo:JXPathException, remove:UnsupportedOperationException, UnsupportedOperationException,]
[MASTER] 00:33:19.892 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: compareTo:JXPathException at 7
[MASTER] 00:33:19.892 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [compareTo:JXPathException, remove:UnsupportedOperationException, UnsupportedOperationException,]
[MASTER] 00:33:19.960 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 00:33:19.965 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 10%
* Total number of goals: 158
* Number of covered goals: 16
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NodePointer_ESTest' to evosuite-tests
* Done!

* Computation finished
