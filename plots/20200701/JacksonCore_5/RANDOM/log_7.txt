* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.JsonPointer
* Starting client
* Connecting to master process on port 17704
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.JsonPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 19:08:01.013 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 70
[MASTER] 19:08:08.056 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 19:08:08.996 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 19:08:09.001 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 19:08:09.208 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 19:08:09.211 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
[MASTER] 19:08:09.212 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 2049 | Tests with assertion: 1
* Generated 1 tests with total length 5
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          8 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 19:08:09.697 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 19:08:09.707 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 19:08:09.712 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 19:08:09.712 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:compile-96,, valueOf:MockIllegalArgumentException, _parseTail:NumberFormatException]
[MASTER] 19:08:09.712 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: valueOf:MockIllegalArgumentException at 1
[MASTER] 19:08:09.712 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:compile-96,, valueOf:MockIllegalArgumentException, _parseTail:NumberFormatException]
[MASTER] 19:08:09.712 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: _parseTail:NumberFormatException at 0
[MASTER] 19:08:09.712 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [MockIllegalArgumentException:compile-96,, valueOf:MockIllegalArgumentException, _parseTail:NumberFormatException]
[MASTER] 19:08:09.725 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
[MASTER] 19:08:09.727 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 21%
* Total number of goals: 70
* Number of covered goals: 15
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'JsonPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
