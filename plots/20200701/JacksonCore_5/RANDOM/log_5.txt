* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.JsonPointer
* Starting client
* Connecting to master process on port 13720
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.JsonPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 18:57:33.359 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:57:33.723 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 18:57:34.781 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 18:57:34.790 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 18:57:35.056 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 18:57:35.064 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 18:57:35.065 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 11 | Tests with assertion: 1
* Generated 1 tests with total length 5
* GA-Budget:
	- MaxTime :                          1 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 18:57:35.562 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:57:35.574 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 18:57:35.581 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 18:57:35.581 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IndexOutOfBoundsException:_parseQuotedTail-239,, _parseTail:NumberFormatException, _parseQuotedTail:IndexOutOfBoundsException]
[MASTER] 18:57:35.581 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: _parseQuotedTail:IndexOutOfBoundsException at 1
[MASTER] 18:57:35.581 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IndexOutOfBoundsException:_parseQuotedTail-239,, _parseTail:NumberFormatException, _parseQuotedTail:IndexOutOfBoundsException]
[MASTER] 18:57:35.581 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: _parseTail:NumberFormatException at 0
[MASTER] 18:57:35.581 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [IndexOutOfBoundsException:_parseQuotedTail-239,, _parseTail:NumberFormatException, _parseQuotedTail:IndexOutOfBoundsException]
[MASTER] 18:57:35.605 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 18:57:35.608 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 17%
* Total number of goals: 70
* Number of covered goals: 12
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'JsonPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
