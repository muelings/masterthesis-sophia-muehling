* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting client
* Connecting to master process on port 19353
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:00:33.863 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:00:33.868 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 59
* Using seed 1593410431173
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 08:00:35.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:127.5 - branchDistance:0.0 - coverage:83.75 - ex: 0 - tex: 14
[MASTER] 08:00:35.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.5, number of tests: 7, total length: 202
[MASTER] 08:00:45.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:126.75 - branchDistance:0.0 - coverage:83.0 - ex: 0 - tex: 16
[MASTER] 08:00:45.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.75, number of tests: 8, total length: 168
[MASTER] 08:00:57.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:122.25 - branchDistance:0.0 - coverage:78.5 - ex: 0 - tex: 10
[MASTER] 08:00:57.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.25, number of tests: 5, total length: 130
[MASTER] 08:01:17.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:109.49333333240202 - branchDistance:0.0 - coverage:81.133333332402 - ex: 0 - tex: 20
[MASTER] 08:01:17.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.49333333240202, number of tests: 10, total length: 227
[MASTER] 08:01:28.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:99.15999999800431 - branchDistance:0.0 - coverage:70.79999999800431 - ex: 0 - tex: 16
[MASTER] 08:01:28.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.15999999800431, number of tests: 9, total length: 290
[MASTER] 08:01:38.974 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:95.35999999800433 - branchDistance:0.0 - coverage:66.99999999800431 - ex: 0 - tex: 18
[MASTER] 08:01:38.974 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.35999999800433, number of tests: 10, total length: 313
[MASTER] 08:01:39.024 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:95.13777777684646 - branchDistance:0.0 - coverage:66.77777777684645 - ex: 0 - tex: 14
[MASTER] 08:01:39.024 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.13777777684646, number of tests: 8, total length: 206
[MASTER] 08:01:49.614 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:94.13777777684646 - branchDistance:0.0 - coverage:65.77777777684645 - ex: 0 - tex: 14
[MASTER] 08:01:49.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.13777777684646, number of tests: 8, total length: 213
[MASTER] 08:02:00.201 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:93.24888888795758 - branchDistance:0.0 - coverage:64.88888888795756 - ex: 0 - tex: 16
[MASTER] 08:02:00.201 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.24888888795758, number of tests: 10, total length: 226
[MASTER] 08:02:00.382 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:91.35999999906869 - branchDistance:0.0 - coverage:62.99999999906868 - ex: 0 - tex: 16
[MASTER] 08:02:00.382 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.35999999906869, number of tests: 8, total length: 217
[MASTER] 08:02:00.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:91.31238095144963 - branchDistance:0.0 - coverage:62.952380951449626 - ex: 0 - tex: 16
[MASTER] 08:02:00.412 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.31238095144963, number of tests: 8, total length: 251
[MASTER] 08:02:01.543 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:90.04888888795757 - branchDistance:0.0 - coverage:61.68888888795757 - ex: 0 - tex: 16
[MASTER] 08:02:01.543 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.04888888795757, number of tests: 9, total length: 230
[MASTER] 08:02:12.142 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:89.49999999800431 - branchDistance:0.0 - coverage:65.99999999800431 - ex: 0 - tex: 16
[MASTER] 08:02:12.142 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.49999999800431, number of tests: 10, total length: 271
[MASTER] 08:02:13.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:88.60769230676098 - branchDistance:0.0 - coverage:61.299999999068675 - ex: 0 - tex: 12
[MASTER] 08:02:13.185 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.60769230676098, number of tests: 7, total length: 219
[MASTER] 08:02:13.238 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:87.34615384522252 - branchDistance:0.0 - coverage:63.84615384522252 - ex: 0 - tex: 14
[MASTER] 08:02:13.238 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.34615384522252, number of tests: 8, total length: 157
[MASTER] 08:02:13.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:85.49999999906868 - branchDistance:0.0 - coverage:61.99999999906868 - ex: 0 - tex: 16
[MASTER] 08:02:13.503 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.49999999906868, number of tests: 8, total length: 278
[MASTER] 08:02:13.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:85.29999999906867 - branchDistance:0.0 - coverage:61.799999999068675 - ex: 0 - tex: 18
[MASTER] 08:02:13.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.29999999906867, number of tests: 9, total length: 214
[MASTER] 08:02:13.990 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:84.29999999906867 - branchDistance:0.0 - coverage:60.799999999068675 - ex: 0 - tex: 14
[MASTER] 08:02:13.990 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.29999999906867, number of tests: 8, total length: 219
[MASTER] 08:02:14.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:83.68888888795757 - branchDistance:0.0 - coverage:60.18888888795757 - ex: 0 - tex: 12
[MASTER] 08:02:14.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.68888888795757, number of tests: 8, total length: 218
[MASTER] 08:02:25.003 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:83.55555555462423 - branchDistance:0.0 - coverage:60.055555554624235 - ex: 0 - tex: 18
[MASTER] 08:02:25.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.55555555462423, number of tests: 10, total length: 231
[MASTER] 08:02:46.071 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:83.3 - branchDistance:0.0 - coverage:59.8 - ex: 0 - tex: 14
[MASTER] 08:02:46.071 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.3, number of tests: 8, total length: 205
[MASTER] 08:02:56.207 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:82.6888888888889 - branchDistance:0.0 - coverage:59.18888888888889 - ex: 0 - tex: 14
[MASTER] 08:02:56.207 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.6888888888889, number of tests: 9, total length: 236
[MASTER] 08:02:56.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:82.55555555555556 - branchDistance:0.0 - coverage:59.05555555555556 - ex: 0 - tex: 16
[MASTER] 08:02:56.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.55555555555556, number of tests: 9, total length: 219
[MASTER] 08:03:08.580 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:81.55555555555556 - branchDistance:0.0 - coverage:58.05555555555556 - ex: 0 - tex: 16
[MASTER] 08:03:08.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.55555555555556, number of tests: 9, total length: 278
[MASTER] 08:03:31.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:81.38888888888889 - branchDistance:0.0 - coverage:57.888888888888886 - ex: 0 - tex: 14
[MASTER] 08:03:31.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.38888888888889, number of tests: 9, total length: 248
[MASTER] 08:03:53.018 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:80.88888888888889 - branchDistance:0.0 - coverage:57.388888888888886 - ex: 0 - tex: 12
[MASTER] 08:03:53.019 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.88888888888889, number of tests: 9, total length: 229
[MASTER] 08:04:04.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:79.6161616161616 - branchDistance:0.0 - coverage:57.888888888888886 - ex: 0 - tex: 16
[MASTER] 08:04:04.421 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.6161616161616, number of tests: 10, total length: 273
[MASTER] 08:04:15.605 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:76.38888888888889 - branchDistance:0.0 - coverage:52.888888888888886 - ex: 0 - tex: 16
[MASTER] 08:04:15.605 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.38888888888889, number of tests: 9, total length: 236
[MASTER] 08:04:26.320 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:74.6161616161616 - branchDistance:0.0 - coverage:52.888888888888886 - ex: 0 - tex: 16
[MASTER] 08:04:26.320 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.6161616161616, number of tests: 9, total length: 277
[MASTER] 08:04:38.935 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:74.1161616161616 - branchDistance:0.0 - coverage:52.388888888888886 - ex: 0 - tex: 16
[MASTER] 08:04:38.935 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.1161616161616, number of tests: 9, total length: 273
[MASTER] 08:05:31.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:73.83923853923854 - branchDistance:0.0 - coverage:52.11196581196581 - ex: 0 - tex: 18
[MASTER] 08:05:31.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.83923853923854, number of tests: 10, total length: 310
[MASTER] 08:05:42.736 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 309s and 53 generations, 48894 statements, best individuals have fitness: 73.83923853923854
[MASTER] 08:05:42.742 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 46%
* Total number of goals: 59
* Number of covered goals: 27
* Generated 9 tests with total length 268
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     74 / 0           
	- MaxTime :                        309 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 08:05:43.068 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:05:43.120 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 223 
[MASTER] 08:05:43.239 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 08:05:43.239 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 08:05:43.239 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 08:05:43.239 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 08:05:43.239 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 08:05:43.239 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 08:05:43.240 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 1.0
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Test8, uniqueExceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: read:ArrayIndexOutOfBoundsException at 13
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 08:05:43.241 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 08:05:43.353 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:05:43.354 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 59
* Number of covered goals: 2
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
