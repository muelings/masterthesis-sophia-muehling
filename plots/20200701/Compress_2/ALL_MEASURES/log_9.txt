* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting client
* Connecting to master process on port 12047
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:10:05.347 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:10:05.353 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 59
* Using seed 1593411002773
* Starting evolution
[MASTER] 08:10:06.260 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:141.75 - branchDistance:0.0 - coverage:98.0 - ex: 0 - tex: 2
[MASTER] 08:10:06.260 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.75, number of tests: 1, total length: 32
[MASTER] 08:10:06.845 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:137.75 - branchDistance:0.0 - coverage:94.0 - ex: 0 - tex: 10
[MASTER] 08:10:06.845 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.75, number of tests: 5, total length: 102
[MASTER] 08:10:06.908 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:125.46428571428571 - branchDistance:0.0 - coverage:81.71428571428571 - ex: 0 - tex: 18
[MASTER] 08:10:06.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.46428571428571, number of tests: 9, total length: 202
[MASTER] 08:10:38.525 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:116.25 - branchDistance:0.0 - coverage:72.5 - ex: 0 - tex: 18
[MASTER] 08:10:38.525 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.25, number of tests: 10, total length: 192
[MASTER] 08:11:01.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:112.19117647058823 - branchDistance:0.0 - coverage:68.44117647058823 - ex: 0 - tex: 16
[MASTER] 08:11:01.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.19117647058823, number of tests: 10, total length: 194
[MASTER] 08:11:22.411 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:111.96428571428572 - branchDistance:0.0 - coverage:68.21428571428572 - ex: 0 - tex: 12
[MASTER] 08:11:22.411 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.96428571428572, number of tests: 9, total length: 158
[MASTER] 08:11:33.476 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:104.19117647058823 - branchDistance:0.0 - coverage:60.44117647058823 - ex: 0 - tex: 14
[MASTER] 08:11:33.476 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.19117647058823, number of tests: 9, total length: 225
[MASTER] 08:11:43.989 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:89.36000000000001 - branchDistance:0.0 - coverage:61.0 - ex: 0 - tex: 14
[MASTER] 08:11:43.989 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.36000000000001, number of tests: 10, total length: 198
[MASTER] 08:11:44.308 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:89.30117647058825 - branchDistance:0.0 - coverage:60.94117647058823 - ex: 0 - tex: 14
[MASTER] 08:11:44.308 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.30117647058825, number of tests: 10, total length: 231
[MASTER] 08:11:45.498 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:89.03450980392157 - branchDistance:0.0 - coverage:60.674509803921566 - ex: 0 - tex: 16
[MASTER] 08:11:45.498 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.03450980392157, number of tests: 11, total length: 255
[MASTER] 08:12:06.379 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:88.80117647058825 - branchDistance:0.0 - coverage:60.44117647058823 - ex: 0 - tex: 14
[MASTER] 08:12:06.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.80117647058825, number of tests: 11, total length: 268
[MASTER] 08:12:38.092 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:86.09333333333333 - branchDistance:0.0 - coverage:57.733333333333334 - ex: 0 - tex: 18
[MASTER] 08:12:38.092 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.09333333333333, number of tests: 10, total length: 203
[MASTER] 08:12:59.699 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:86.03450980392157 - branchDistance:0.0 - coverage:57.674509803921566 - ex: 0 - tex: 18
[MASTER] 08:12:59.699 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.03450980392157, number of tests: 10, total length: 204
[MASTER] 08:13:00.310 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:86.01925925925926 - branchDistance:0.0 - coverage:57.65925925925926 - ex: 0 - tex: 18
[MASTER] 08:13:00.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.01925925925926, number of tests: 11, total length: 198
[MASTER] 08:13:10.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:85.80117647058825 - branchDistance:0.0 - coverage:57.44117647058823 - ex: 0 - tex: 16
[MASTER] 08:13:10.646 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.80117647058825, number of tests: 10, total length: 207
[MASTER] 08:13:10.773 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:85.53450980392157 - branchDistance:0.0 - coverage:57.174509803921566 - ex: 0 - tex: 18
[MASTER] 08:13:10.773 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.53450980392157, number of tests: 11, total length: 224
[MASTER] 08:13:21.813 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:85.26925925925926 - branchDistance:0.0 - coverage:56.90925925925926 - ex: 0 - tex: 18
[MASTER] 08:13:21.813 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.26925925925926, number of tests: 11, total length: 195
[MASTER] 08:13:32.007 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:85.05117647058825 - branchDistance:0.0 - coverage:56.69117647058823 - ex: 0 - tex: 16
[MASTER] 08:13:32.007 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.05117647058825, number of tests: 10, total length: 201
[MASTER] 08:13:32.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:78.63653198653199 - branchDistance:0.0 - coverage:56.90925925925926 - ex: 0 - tex: 20
[MASTER] 08:13:32.022 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.63653198653199, number of tests: 12, total length: 202
[MASTER] 08:14:13.810 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.398809523809524 - fitness:77.04032375740994 - branchDistance:0.0 - coverage:58.22450980392157 - ex: 0 - tex: 18
[MASTER] 08:14:13.810 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.04032375740994, number of tests: 11, total length: 221
[MASTER] 08:14:14.016 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.518315018315018 - fitness:75.79702549184888 - branchDistance:0.0 - coverage:57.3078431372549 - ex: 0 - tex: 14
[MASTER] 08:14:14.016 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.79702549184888, number of tests: 10, total length: 188
[MASTER] 08:14:14.102 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.398809523809524 - fitness:75.37365709074327 - branchDistance:0.0 - coverage:56.5578431372549 - ex: 0 - tex: 16
[MASTER] 08:14:14.102 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.37365709074327, number of tests: 10, total length: 200
[MASTER] 08:14:14.493 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.518315018315018 - fitness:75.29702549184888 - branchDistance:0.0 - coverage:57.3078431372549 - ex: 1 - tex: 15
[MASTER] 08:14:14.493 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.29702549184888, number of tests: 10, total length: 187
[MASTER] 08:14:15.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.398809523809524 - fitness:75.24032375740994 - branchDistance:0.0 - coverage:56.424509803921566 - ex: 0 - tex: 18
[MASTER] 08:14:15.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.24032375740994, number of tests: 11, total length: 234
[MASTER] 08:14:15.582 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.166666666666666 - fitness:73.67766295707472 - branchDistance:0.0 - coverage:54.19117647058823 - ex: 0 - tex: 14
[MASTER] 08:14:15.582 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.67766295707472, number of tests: 10, total length: 201
[MASTER] 08:14:46.562 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.166666666666666 - fitness:73.17766295707472 - branchDistance:0.0 - coverage:54.19117647058823 - ex: 1 - tex: 15
[MASTER] 08:14:46.562 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.17766295707472, number of tests: 10, total length: 203
[MASTER] 08:14:57.467 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.398809523809524 - fitness:72.87365709074327 - branchDistance:0.0 - coverage:54.0578431372549 - ex: 0 - tex: 16
[MASTER] 08:14:57.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.87365709074327, number of tests: 11, total length: 230
[MASTER] 08:15:07.540 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 43 generations, 40554 statements, best individuals have fitness: 72.87365709074327
[MASTER] 08:15:07.543 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 44%
* Total number of goals: 59
* Number of covered goals: 26
* Generated 11 tests with total length 230
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        302 / 300          Finished!
	- RMIStoppingCondition
	- ZeroFitness :                     73 / 0           
[MASTER] 08:15:07.888 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:15:07.942 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 158 
[MASTER] 08:15:08.034 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 08:15:08.034 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 08:15:08.034 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 08:15:08.034 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 1.0
[MASTER] 08:15:08.035 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: [getNextArEntry:MockIOException, MockIOException:getNextArEntry-119,MockIOException:getNextArEntry-119]
[MASTER] 08:15:08.035 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: getNextArEntry:MockIOException at 12
[MASTER] 08:15:08.035 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 08:15:08.035 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: [getNextArEntry:MockIOException, MockIOException:getNextArEntry-119,MockIOException:getNextArEntry-119]
[MASTER] 08:15:08.035 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: getNextArEntry:MockIOException at 12
[MASTER] 08:15:08.036 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 12
[MASTER] 08:15:08.047 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 08:15:08.047 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 1.0
[MASTER] 08:15:08.047 [logback-1] WARN  RegressionSuiteMinimizer - Test8, uniqueExceptions: [getNextArEntry:MockIOException, MockIOException:getNextArEntry-119,MockIOException:getNextArEntry-119]
[MASTER] 08:15:08.047 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: getNextArEntry:MockIOException at 12
[MASTER] 08:15:08.047 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 12
[MASTER] 08:15:08.054 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 08:15:08.054 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [getNextArEntry:MockIOException, MockIOException:getNextArEntry-119,MockIOException:getNextArEntry-119]
[MASTER] 08:15:08.054 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 08:15:08.054 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 08:15:08.054 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 08:15:08.054 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 08:15:08.054 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 08:15:08.055 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 08:15:08.055 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 08:15:08.055 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 08:15:08.055 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 08:15:08.055 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 08:15:08.180 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:15:08.182 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 20%
* Total number of goals: 59
* Number of covered goals: 12
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
