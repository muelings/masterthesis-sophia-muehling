* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting client
* Connecting to master process on port 10030
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 59
[MASTER] 07:34:43.297 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:34:45.307 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:34:46.611 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/ar/ArArchiveInputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:34:46.622 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 07:34:46.960 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/ar/ArArchiveInputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:34:46.975 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:34:46.976 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 105 | Tests with assertion: 1
* Generated 1 tests with total length 10
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          3 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 07:34:47.650 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:34:47.668 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 07:34:47.677 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 07:34:47.677 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 07:34:47.677 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: read:ArrayIndexOutOfBoundsException at 6
[MASTER] 07:34:47.677 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [read:ArrayIndexOutOfBoundsException]
[MASTER] 07:34:47.758 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
[MASTER] 07:34:47.764 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 3%
* Total number of goals: 59
* Number of covered goals: 2
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
