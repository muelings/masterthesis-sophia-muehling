/*
 * This file was automatically generated by EvoSuite
 * Tue Jun 30 01:19:53 GMT 2020
 */

package org.mockito.internal.util;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;
import org.mockito.internal.util.Timer;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Timer_ESTest extends Timer_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     java.lang.NoClassDefFoundError : Could not initialize class org.mockito.internal.exceptions.stacktrace.StackTraceFilter
      Timer timer0 = null;
      try {
        timer0 = new Timer((-119L));
        fail("Expecting exception: NoClassDefFoundError");
      
      } catch(NoClassDefFoundError e) {
         //
         // Could not initialize class org.mockito.internal.exceptions.stacktrace.StackTraceFilter
         //
         verifyException("org.mockito.internal.exceptions.stacktrace.ConditionalStackTraceFilter", e);
         assertTrue(e.getMessage().equals("Could not initialize class org.mockito.internal.exceptions.stacktrace.StackTraceFilter"));   
      }
  }
}
