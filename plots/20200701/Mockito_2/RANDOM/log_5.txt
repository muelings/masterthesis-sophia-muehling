* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.mockito.internal.util.Timer
* Starting client
* Connecting to master process on port 16807
* Analyzing classpath: 
  - ../defects4j_compiled/Mockito_2_fixed/build/classes/main/
* Finished analyzing classpath
* Generating tests for class org.mockito.internal.util.Timer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 10
[MASTER] 03:25:10.889 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:25:11.277 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 03:25:12.372 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/mockito/internal/util/Timer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:25:12.378 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 03:25:12.607 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/mockito/internal/util/Timer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:25:12.612 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 03:25:12.612 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 2 | Tests with assertion: 1
* Generated 1 tests with total length 2
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          1 / 300         
	- RMIStoppingCondition
[MASTER] 03:25:12.830 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:25:12.838 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 1 
[MASTER] 03:25:12.842 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 03:25:12.843 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NoClassDefFoundError, NoClassDefFoundError:validateInput-32,]
[MASTER] 03:25:12.843 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: NoClassDefFoundError at 0
[MASTER] 03:25:12.843 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NoClassDefFoundError, NoClassDefFoundError:validateInput-32,]
[MASTER] 03:25:12.852 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 03:25:12.854 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 20%
* Total number of goals: 10
* Number of covered goals: 2
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Timer_ESTest' to evosuite-tests
* Done!

* Computation finished
