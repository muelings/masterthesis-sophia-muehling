* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 15430
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:50:36.960 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:50:36.965 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593420634759
* Starting evolution
[MASTER] 10:50:38.141 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.0 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 4
[MASTER] 10:50:38.141 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0, number of tests: 5, total length: 129
[MASTER] 10:50:38.582 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:90.33333333333333 - branchDistance:0.0 - coverage:29.333333333333332 - ex: 0 - tex: 0
[MASTER] 10:50:38.582 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.33333333333333, number of tests: 2, total length: 55
[MASTER] 10:50:38.608 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.99999999901694 - branchDistance:0.0 - coverage:28.999999999016936 - ex: 0 - tex: 0
[MASTER] 10:50:38.608 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.99999999901694, number of tests: 4, total length: 85
[MASTER] 10:50:38.641 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:88.0 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 2
[MASTER] 10:50:38.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.0, number of tests: 4, total length: 81
[MASTER] 10:50:38.759 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:73.6 - branchDistance:0.0 - coverage:12.6 - ex: 0 - tex: 2
[MASTER] 10:50:38.759 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.6, number of tests: 4, total length: 117
[MASTER] 10:50:38.999 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:73.33333333333333 - branchDistance:0.0 - coverage:12.333333333333332 - ex: 0 - tex: 4
[MASTER] 10:50:38.999 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.33333333333333, number of tests: 6, total length: 148
[MASTER] 10:50:39.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:71.0 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 6
[MASTER] 10:50:39.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.0, number of tests: 8, total length: 181
[MASTER] 10:50:39.924 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:69.6 - branchDistance:0.0 - coverage:8.6 - ex: 0 - tex: 8
[MASTER] 10:50:39.924 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.6, number of tests: 8, total length: 181
[MASTER] 10:50:40.677 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.5 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 8
[MASTER] 10:50:40.677 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.5, number of tests: 8, total length: 230
[MASTER] 10:50:41.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 8
[MASTER] 10:50:41.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 6, total length: 184
[MASTER] 10:50:42.029 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.5 - branchDistance:0.0 - coverage:2.5 - ex: 0 - tex: 10
[MASTER] 10:50:42.029 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.5, number of tests: 8, total length: 247
[MASTER] 10:50:44.686 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 8
[MASTER] 10:50:44.686 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 8, total length: 235
[MASTER] 10:50:46.485 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.5 - branchDistance:0.0 - coverage:1.5 - ex: 0 - tex: 8
[MASTER] 10:50:46.485 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.5, number of tests: 8, total length: 225
[MASTER] 10:50:47.634 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.333333333333336 - branchDistance:0.0 - coverage:1.3333333333333333 - ex: 0 - tex: 8
[MASTER] 10:50:47.634 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.333333333333336, number of tests: 9, total length: 240
[MASTER] 10:50:48.307 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 10:50:48.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 9, total length: 246
[MASTER] 10:50:57.080 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 10:50:57.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 8, total length: 191
[MASTER] 10:55:37.988 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1688 generations, 1316362 statements, best individuals have fitness: 61.0
[MASTER] 10:55:37.991 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 6 tests with total length 55
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     61 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 10:55:38.153 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:55:38.177 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 38 
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 10:55:38.300 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 10:55:38.302 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 25
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
