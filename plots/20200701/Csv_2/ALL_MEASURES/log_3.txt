* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 17531
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:13:19.748 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:13:19.753 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593418397793
* Starting evolution
[MASTER] 10:13:21.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.93333333333334 - branchDistance:0.0 - coverage:22.933333333333334 - ex: 0 - tex: 4
[MASTER] 10:13:21.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.93333333333334, number of tests: 4, total length: 124
[MASTER] 10:13:21.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.5 - branchDistance:0.0 - coverage:11.5 - ex: 0 - tex: 6
[MASTER] 10:13:21.615 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.5, number of tests: 9, total length: 210
[MASTER] 10:13:21.780 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:71.0 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 4
[MASTER] 10:13:21.780 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.0, number of tests: 9, total length: 168
[MASTER] 10:13:22.598 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:70.99999999860302 - branchDistance:0.0 - coverage:9.999999998603016 - ex: 0 - tex: 8
[MASTER] 10:13:22.598 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.99999999860302, number of tests: 9, total length: 269
[MASTER] 10:13:22.969 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:69.98913043478261 - branchDistance:0.0 - coverage:8.98913043478261 - ex: 0 - tex: 8
[MASTER] 10:13:22.969 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.98913043478261, number of tests: 8, total length: 158
[MASTER] 10:13:23.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:69.33333333333333 - branchDistance:0.0 - coverage:8.333333333333332 - ex: 0 - tex: 0
[MASTER] 10:13:23.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.33333333333333, number of tests: 6, total length: 106
[MASTER] 10:13:23.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:68.6 - branchDistance:0.0 - coverage:7.6 - ex: 0 - tex: 14
[MASTER] 10:13:23.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.6, number of tests: 9, total length: 225
[MASTER] 10:13:24.003 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 4
[MASTER] 10:13:24.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0, number of tests: 9, total length: 133
[MASTER] 10:13:24.593 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 8
[MASTER] 10:13:24.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0, number of tests: 9, total length: 201
[MASTER] 10:13:25.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 0
[MASTER] 10:13:25.363 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 7, total length: 156
[MASTER] 10:13:26.121 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 4
[MASTER] 10:13:26.121 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 10, total length: 201
[MASTER] 10:13:26.125 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 2
[MASTER] 10:13:26.126 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 7, total length: 148
[MASTER] 10:13:30.714 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 10:13:30.714 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 7, total length: 116
[MASTER] 10:18:20.771 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1829 generations, 1383764 statements, best individuals have fitness: 61.0
[MASTER] 10:18:20.774 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 5 tests with total length 50
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     61 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 10:18:20.936 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:18:20.957 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 37 
[MASTER] 10:18:21.025 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 10:18:21.025 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 10:18:21.025 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 10:18:21.025 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 10:18:21.026 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 10:18:21.026 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 10:18:21.026 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 10:18:21.027 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 25
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
