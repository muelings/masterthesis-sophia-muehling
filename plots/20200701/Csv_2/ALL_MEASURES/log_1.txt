* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 21846
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:02:52.074 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:02:52.079 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593417770217
* Starting evolution
[MASTER] 10:02:53.785 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.33333333333333 - branchDistance:0.0 - coverage:11.333333333333332 - ex: 0 - tex: 8
[MASTER] 10:02:53.785 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.33333333333333, number of tests: 10, total length: 252
[MASTER] 10:02:53.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:67.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 6
[MASTER] 10:02:53.976 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.0, number of tests: 9, total length: 224
[MASTER] 10:02:55.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:66.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 6
[MASTER] 10:02:55.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.0, number of tests: 7, total length: 193
[MASTER] 10:02:56.702 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 4
[MASTER] 10:02:56.703 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0, number of tests: 9, total length: 221
[MASTER] 10:02:56.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.333333333333336 - branchDistance:0.0 - coverage:2.333333333333333 - ex: 0 - tex: 4
[MASTER] 10:02:56.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.333333333333336, number of tests: 10, total length: 248
[MASTER] 10:02:59.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.333333333333336 - branchDistance:0.0 - coverage:1.3333333333333333 - ex: 0 - tex: 4
[MASTER] 10:02:59.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.333333333333336, number of tests: 9, total length: 198
[MASTER] 10:03:01.547 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 10:03:01.547 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 10, total length: 200
[MASTER] 10:03:02.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 10:03:02.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 8, total length: 191

[MASTER] 10:07:53.093 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 2083 generations, 1559384 statements, best individuals have fitness: 61.0
[MASTER] 10:07:53.096 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 5 tests with total length 46
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     61 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 10:07:53.256 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:07:53.276 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 34 
[MASTER] 10:07:53.413 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 10:07:53.414 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 10:07:53.414 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 10:07:53.414 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 10:07:53.414 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 10:07:53.414 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 10:07:53.414 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 10:07:53.414 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 10:07:53.415 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 25
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
