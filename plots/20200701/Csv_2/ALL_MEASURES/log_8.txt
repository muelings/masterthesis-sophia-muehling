* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 11060
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:39:40.309 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:39:40.314 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593419978380
* Starting evolution
[MASTER] 10:39:41.978 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:90.0 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 6
[MASTER] 10:39:41.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.0, number of tests: 5, total length: 117
[MASTER] 10:39:42.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:80.0 - branchDistance:0.0 - coverage:19.0 - ex: 0 - tex: 12
[MASTER] 10:39:42.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.0, number of tests: 10, total length: 231
[MASTER] 10:39:42.221 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:75.33333333333333 - branchDistance:0.0 - coverage:14.333333333333332 - ex: 0 - tex: 6
[MASTER] 10:39:42.221 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.33333333333333, number of tests: 9, total length: 168
[MASTER] 10:39:42.373 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:71.0 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 6
[MASTER] 10:39:42.373 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.0, number of tests: 10, total length: 208
[MASTER] 10:39:42.543 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:68.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 8
[MASTER] 10:39:42.543 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.0, number of tests: 10, total length: 230
[MASTER] 10:39:42.619 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 10
[MASTER] 10:39:42.619 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0, number of tests: 10, total length: 260
[MASTER] 10:39:44.948 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 10
[MASTER] 10:39:44.948 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 11, total length: 295
[MASTER] 10:39:46.215 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 4
[MASTER] 10:39:46.215 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 9, total length: 239
[MASTER] 10:39:53.232 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 10:39:53.232 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 9, total length: 191
[MASTER] 10:44:41.329 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 2035 generations, 1514646 statements, best individuals have fitness: 61.0
[MASTER] 10:44:41.332 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 5 tests with total length 54
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     61 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 10:44:41.437 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:44:41.461 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 40 
[MASTER] 10:44:41.557 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 1.0
[MASTER] 10:44:41.557 [logback-1] WARN  RegressionSuiteMinimizer - Test1, uniqueExceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 10:44:41.557 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: get:MockIllegalArgumentException at 8
[MASTER] 10:44:41.558 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 10:44:41.558 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 10:44:41.558 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [get:MockIllegalArgumentException, MockIllegalArgumentException:get-88,MockIllegalArgumentException:get-88]
[MASTER] 10:44:41.558 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 10:44:41.558 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 10:44:41.558 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 10:44:41.558 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 10:44:41.673 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 10:44:41.675 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 40%
* Total number of goals: 25
* Number of covered goals: 10
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
