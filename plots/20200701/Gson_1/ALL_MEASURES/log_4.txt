* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.TypeInfoFactory
* Starting client
* Connecting to master process on port 14999
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.TypeInfoFactory
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 14:10:28.955 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 14:10:28.960 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 46
* Using seed 1593432626679
* Starting evolution
[MASTER] 14:10:30.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.875 - fitness:85.2 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 14:10:30.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.2, number of tests: 6, total length: 125
[MASTER] 14:10:30.602 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.875 - fitness:83.77777777777777 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 14:10:30.602 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.77777777777777, number of tests: 6, total length: 124
[MASTER] 14:10:30.691 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.833333333333334 - fitness:81.83050847457628 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 14:10:30.691 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.83050847457628, number of tests: 7, total length: 124
[MASTER] 14:10:30.759 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.88888888888889 - fitness:80.4766355140187 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 14:10:30.759 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.4766355140187, number of tests: 10, total length: 221
[MASTER] 14:10:32.834 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.875 - fitness:79.98058252427184 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 14:10:32.834 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.98058252427184, number of tests: 7, total length: 184
[MASTER] 14:10:32.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.88888888888889 - fitness:79.97413793103448 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 22
[MASTER] 14:10:32.903 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.97413793103448, number of tests: 11, total length: 269
[MASTER] 14:10:33.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.875 - fitness:79.54954954954955 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 14:10:33.977 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.54954954954955, number of tests: 10, total length: 203
[MASTER] 14:10:34.403 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.88888888888889 - fitness:79.544 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 14:10:34.403 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.544, number of tests: 10, total length: 203
[MASTER] 14:10:34.945 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.88888888888889 - fitness:79.17164179104478 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 14:10:34.945 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.17164179104478, number of tests: 10, total length: 207
[MASTER] 14:10:36.138 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.88888888888889 - fitness:78.84615384615384 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 18
[MASTER] 14:10:36.138 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.84615384615384, number of tests: 9, total length: 189
[MASTER] 14:10:37.526 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.875 - fitness:78.56296296296296 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 14:10:37.527 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.56296296296296, number of tests: 10, total length: 207
[MASTER] 14:10:38.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.857142857142858 - fitness:78.312 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 16
[MASTER] 14:10:38.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.312, number of tests: 8, total length: 151
[MASTER] 14:10:40.025 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.88888888888889 - fitness:78.30434782608695 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 16
[MASTER] 14:10:40.025 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.30434782608695, number of tests: 8, total length: 151
[MASTER] 14:10:41.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.875 - fitness:78.0794701986755 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 16
[MASTER] 14:10:41.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.0794701986755, number of tests: 8, total length: 150
[MASTER] 14:10:42.000 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.875 - fitness:77.87421383647799 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 14:10:42.000 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.87421383647799, number of tests: 7, total length: 129
[MASTER] 14:10:42.960 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.88888888888889 - fitness:77.87150837988827 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 14:10:42.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.87150837988827, number of tests: 7, total length: 100
[MASTER] 14:10:47.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88888888888889 - fitness:77.68617021276596 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:10:47.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68617021276596, number of tests: 4, total length: 53
[MASTER] 14:10:48.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.88888888888889 - fitness:77.36407766990291 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:10:48.185 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.36407766990291, number of tests: 4, total length: 43
[MASTER] 14:10:48.844 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.857142857142858 - fitness:77.22754491017965 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:10:48.844 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.22754491017965, number of tests: 4, total length: 44
[MASTER] 14:10:49.048 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.88888888888889 - fitness:77.09375 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:10:49.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.09375, number of tests: 4, total length: 47
[MASTER] 14:10:49.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.857142857142858 - fitness:76.97790055248619 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:10:49.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.97790055248619, number of tests: 4, total length: 46
[MASTER] 14:10:49.849 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.88888888888889 - fitness:76.97424892703863 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:10:49.849 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.97424892703863, number of tests: 4, total length: 53
[MASTER] 14:10:50.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.875 - fitness:76.86511627906977 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:10:50.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.86511627906977, number of tests: 4, total length: 48
[MASTER] 14:10:50.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.88888888888889 - fitness:76.86363636363636 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:10:50.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.86363636363636, number of tests: 4, total length: 74
[MASTER] 14:10:50.983 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.857142857142858 - fitness:76.76410256410256 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:10:50.984 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.76410256410256, number of tests: 4, total length: 49
[MASTER] 14:10:51.249 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.88888888888889 - fitness:76.7609561752988 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 14:10:51.249 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.7609561752988, number of tests: 6, total length: 101
[MASTER] 14:10:51.303 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.88888888888889 - fitness:76.57620817843866 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:10:51.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.57620817843866, number of tests: 4, total length: 76
[MASTER] 14:10:51.868 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.88888888888889 - fitness:76.49280575539568 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:10:51.868 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.49280575539568, number of tests: 5, total length: 100
[MASTER] 14:10:52.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.88888888888889 - fitness:76.41463414634147 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:10:52.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.41463414634147, number of tests: 4, total length: 77
[MASTER] 14:10:52.320 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.88888888888889 - fitness:76.34121621621621 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:10:52.320 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.34121621621621, number of tests: 5, total length: 92
[MASTER] 14:10:55.708 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.888888888888886 - fitness:76.27213114754099 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:10:55.708 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.27213114754099, number of tests: 2, total length: 43
[MASTER] 14:10:56.736 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.875 - fitness:76.2078853046595 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:10:56.736 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.2078853046595, number of tests: 2, total length: 43
[MASTER] 14:10:57.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.888888888888886 - fitness:76.20700636942675 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:10:57.549 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.20700636942675, number of tests: 2, total length: 43
[MASTER] 14:10:58.390 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.888888888888886 - fitness:76.14551083591331 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:10:58.390 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14551083591331, number of tests: 2, total length: 49
[MASTER] 14:11:01.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.875 - fitness:76.08813559322034 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:11:01.792 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08813559322034, number of tests: 2, total length: 43
[MASTER] 14:11:02.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.888888888888886 - fitness:76.08734939759036 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:11:02.258 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08734939759036, number of tests: 2, total length: 43
[MASTER] 14:11:30.039 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.875 - fitness:76.03300330033004 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:11:30.039 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.03300330033004, number of tests: 3, total length: 62
[MASTER] 14:11:30.782 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.888888888888886 - fitness:76.03225806451613 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:11:30.782 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.03225806451613, number of tests: 4, total length: 57
[MASTER] 14:15:29.970 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 3204 generations, 1538560 statements, best individuals have fitness: 76.03225806451613
[MASTER] 14:15:29.973 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 46
* Number of covered goals: 2
* Generated 3 tests with total length 45
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     76 / 0           
[MASTER] 14:15:30.233 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:15:30.248 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 42 
[MASTER] 14:15:30.272 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 14:15:30.272 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 14:15:30.272 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 14:15:30.272 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 14:15:30.272 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 14:15:30.272 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 14:15:30.273 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 46
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'TypeInfoFactory_ESTest' to evosuite-tests
* Done!

* Computation finished
