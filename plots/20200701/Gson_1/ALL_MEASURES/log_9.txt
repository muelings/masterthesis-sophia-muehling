* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.TypeInfoFactory
* Starting client
* Connecting to master process on port 9612
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.TypeInfoFactory
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 15:01:25.845 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 15:01:25.849 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 46
* Using seed 1593435683633
* Starting evolution
[MASTER] 15:01:27.234 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.875 - fitness:83.77777777777777 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 15:01:27.234 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.77777777777777, number of tests: 10, total length: 178
[MASTER] 15:01:27.385 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.888888888888889 - fitness:83.7605633802817 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 15:01:27.385 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.7605633802817, number of tests: 3, total length: 76
[MASTER] 15:01:28.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.857142857142858 - fitness:81.81159420289855 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 18
[MASTER] 15:01:28.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.81159420289855, number of tests: 9, total length: 247
[MASTER] 15:01:28.481 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.875 - fitness:81.79746835443038 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:28.481 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.79746835443038, number of tests: 6, total length: 116
[MASTER] 15:01:29.735 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.88888888888889 - fitness:81.78651685393258 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 16
[MASTER] 15:01:29.735 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.78651685393258, number of tests: 8, total length: 173
[MASTER] 15:01:30.496 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.875 - fitness:81.08045977011494 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:30.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.08045977011494, number of tests: 5, total length: 114
[MASTER] 15:01:32.164 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.88888888888889 - fitness:81.07142857142857 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:32.164 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.07142857142857, number of tests: 5, total length: 91
[MASTER] 15:01:32.951 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.875 - fitness:80.48421052631579 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:32.951 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.48421052631579, number of tests: 6, total length: 103
[MASTER] 15:01:33.220 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.88888888888889 - fitness:79.97413793103448 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:33.220 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.97413793103448, number of tests: 6, total length: 142
[MASTER] 15:01:34.192 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.833333333333334 - fitness:79.56626506024097 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:34.192 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.56626506024097, number of tests: 6, total length: 122
[MASTER] 15:01:35.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.857142857142858 - fitness:79.55670103092784 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:35.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.55670103092784, number of tests: 6, total length: 101
[MASTER] 15:01:35.138 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.88888888888889 - fitness:79.544 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:35.138 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.544, number of tests: 6, total length: 110
[MASTER] 15:01:35.399 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.875 - fitness:79.17647058823529 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:35.400 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.17647058823529, number of tests: 7, total length: 122
[MASTER] 15:01:35.785 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.875 - fitness:78.85039370078741 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:35.785 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.85039370078741, number of tests: 7, total length: 121
[MASTER] 15:01:36.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.88888888888889 - fitness:78.84615384615384 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:36.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.84615384615384, number of tests: 8, total length: 135
[MASTER] 15:01:38.082 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.875 - fitness:78.56296296296296 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:38.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.56296296296296, number of tests: 8, total length: 107
[MASTER] 15:01:38.212 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.88888888888889 - fitness:78.30434782608695 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:38.212 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.30434782608695, number of tests: 8, total length: 109
[MASTER] 15:01:38.461 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.88888888888889 - fitness:77.87150837988827 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:38.461 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.87150837988827, number of tests: 8, total length: 111
[MASTER] 15:01:39.640 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.875 - fitness:77.68862275449102 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 16
[MASTER] 15:01:39.640 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68862275449102, number of tests: 9, total length: 135
[MASTER] 15:01:40.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88888888888889 - fitness:77.68617021276596 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:40.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68617021276596, number of tests: 8, total length: 91
[MASTER] 15:01:40.593 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88888888888889 - fitness:77.51776649746193 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:40.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.51776649746193, number of tests: 8, total length: 129
[MASTER] 15:01:41.444 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.88888888888889 - fitness:77.36407766990291 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:41.444 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.36407766990291, number of tests: 8, total length: 80
[MASTER] 15:01:42.252 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.88888888888889 - fitness:77.09375 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:42.252 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.09375, number of tests: 9, total length: 92
[MASTER] 15:01:42.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.88888888888889 - fitness:76.97424892703863 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:42.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.97424892703863, number of tests: 7, total length: 107
[MASTER] 15:01:43.161 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.875 - fitness:76.86511627906977 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:43.161 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.86511627906977, number of tests: 8, total length: 120
[MASTER] 15:01:43.952 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.88888888888889 - fitness:76.7609561752988 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:43.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.7609561752988, number of tests: 8, total length: 114
[MASTER] 15:01:44.262 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.875 - fitness:76.66666666666667 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:44.262 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.66666666666667, number of tests: 8, total length: 115
[MASTER] 15:01:44.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.88888888888889 - fitness:76.66538461538461 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 14
[MASTER] 15:01:44.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.66538461538461, number of tests: 9, total length: 150
[MASTER] 15:01:45.128 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.88888888888889 - fitness:76.57620817843866 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:45.128 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.57620817843866, number of tests: 8, total length: 117
[MASTER] 15:01:45.668 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.875 - fitness:76.49392712550608 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:45.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.49392712550608, number of tests: 7, total length: 113
[MASTER] 15:01:46.401 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.88888888888889 - fitness:76.41463414634147 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:46.401 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.41463414634147, number of tests: 7, total length: 82
[MASTER] 15:01:47.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.88888888888889 - fitness:76.34121621621621 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:47.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.34121621621621, number of tests: 7, total length: 82
[MASTER] 15:01:48.068 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.875 - fitness:76.2730627306273 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 15:01:48.068 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.2730627306273, number of tests: 8, total length: 154
[MASTER] 15:01:48.641 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.888888888888886 - fitness:76.20700636942675 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:48.641 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.20700636942675, number of tests: 7, total length: 123
[MASTER] 15:01:48.773 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.888888888888886 - fitness:76.14551083591331 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 15:01:48.773 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14551083591331, number of tests: 6, total length: 114
[MASTER] 15:01:56.098 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.875 - fitness:76.08813559322034 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 15:01:56.098 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08813559322034, number of tests: 2, total length: 43
[MASTER] 15:01:56.203 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.888888888888886 - fitness:76.08734939759036 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 15:01:56.203 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08734939759036, number of tests: 2, total length: 43
[MASTER] 15:04:09.443 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.888888888888886 - fitness:76.03225806451613 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 15:04:09.443 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.03225806451613, number of tests: 3, total length: 76
[MASTER] 15:06:26.866 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 2529 generations, 1319092 statements, best individuals have fitness: 76.03225806451613
[MASTER] 15:06:26.869 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 46
* Number of covered goals: 2
* Generated 3 tests with total length 45
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     76 / 0           
[MASTER] 15:06:27.058 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:06:27.075 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 43 
[MASTER] 15:06:27.101 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 15:06:27.101 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 15:06:27.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 15:06:27.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 15:06:27.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 15:06:27.101 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 15:06:27.102 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 46
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'TypeInfoFactory_ESTest' to evosuite-tests
* Done!

* Computation finished
