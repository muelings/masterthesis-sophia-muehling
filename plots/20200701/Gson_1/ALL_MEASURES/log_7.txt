* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.TypeInfoFactory
* Starting client
* Connecting to master process on port 16306
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.TypeInfoFactory
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 14:41:03.085 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 14:41:03.091 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 46
* Using seed 1593434460745
* Starting evolution
[MASTER] 14:41:04.534 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.888888888888889 - fitness:89.0754716981132 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 16
[MASTER] 14:41:04.534 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.0754716981132, number of tests: 8, total length: 129
[MASTER] 14:41:04.651 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.875 - fitness:85.2 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 12
[MASTER] 14:41:04.651 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.2, number of tests: 6, total length: 145
[MASTER] 14:41:04.805 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.857142857142857 - fitness:84.69354838709677 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 10
[MASTER] 14:41:04.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.69354838709677, number of tests: 5, total length: 159
[MASTER] 14:41:04.813 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.888888888888889 - fitness:84.6625 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 2
[MASTER] 14:41:04.813 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.6625, number of tests: 1, total length: 39
[MASTER] 14:41:04.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.857142857142858 - fitness:83.09210526315789 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 14
[MASTER] 14:41:04.895 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.09210526315789, number of tests: 7, total length: 143
[MASTER] 14:41:05.007 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.888888888888889 - fitness:82.6625 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 14:41:05.007 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.6625, number of tests: 10, total length: 170
[MASTER] 14:41:05.346 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.875 - fitness:81.79746835443038 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 14:41:05.347 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.79746835443038, number of tests: 5, total length: 121
[MASTER] 14:41:05.418 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.88888888888889 - fitness:81.07142857142857 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 18
[MASTER] 14:41:05.418 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.07142857142857, number of tests: 9, total length: 195
[MASTER] 14:41:07.075 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.88888888888889 - fitness:80.4766355140187 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 14:41:07.075 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.4766355140187, number of tests: 5, total length: 149
[MASTER] 14:41:07.573 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.857142857142858 - fitness:79.9888888888889 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 14:41:07.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.9888888888889, number of tests: 5, total length: 134
[MASTER] 14:41:07.832 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.88888888888889 - fitness:79.544 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 14:41:07.832 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.544, number of tests: 5, total length: 109
[MASTER] 14:41:09.974 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.88888888888889 - fitness:79.17164179104478 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 10
[MASTER] 14:41:09.974 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.17164179104478, number of tests: 5, total length: 110
[MASTER] 14:41:11.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.857142857142858 - fitness:78.85585585585585 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:41:11.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.85585585585585, number of tests: 4, total length: 90
[MASTER] 14:41:11.620 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.857142857142858 - fitness:78.312 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:41:11.620 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.312, number of tests: 4, total length: 91
[MASTER] 14:41:12.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.88888888888889 - fitness:78.30434782608695 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:41:12.134 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.30434782608695, number of tests: 4, total length: 92
[MASTER] 14:41:12.321 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.88888888888889 - fitness:78.0764705882353 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:41:12.321 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.0764705882353, number of tests: 4, total length: 75
[MASTER] 14:41:12.550 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.88888888888889 - fitness:77.87150837988827 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:41:12.550 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.87150837988827, number of tests: 4, total length: 71
[MASTER] 14:41:12.873 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.857142857142858 - fitness:77.6917808219178 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:41:12.873 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.6917808219178, number of tests: 4, total length: 99
[MASTER] 14:41:13.073 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88888888888889 - fitness:77.68617021276596 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:41:13.073 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.68617021276596, number of tests: 4, total length: 73
[MASTER] 14:41:13.940 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88888888888889 - fitness:77.51776649746193 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:41:13.941 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.51776649746193, number of tests: 4, total length: 75
[MASTER] 14:41:14.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.875 - fitness:77.36612021857924 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:14.185 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.36612021857924, number of tests: 2, total length: 58
[MASTER] 14:41:14.298 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.88888888888889 - fitness:77.36407766990291 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:41:14.298 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.36407766990291, number of tests: 4, total length: 88
[MASTER] 14:41:14.952 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.875 - fitness:77.22513089005236 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 8
[MASTER] 14:41:14.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.22513089005236, number of tests: 4, total length: 75
[MASTER] 14:41:15.272 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.88888888888889 - fitness:77.22325581395349 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:15.272 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.22325581395349, number of tests: 2, total length: 58
[MASTER] 14:41:16.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.875 - fitness:77.09547738693468 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:41:16.334 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.09547738693468, number of tests: 3, total length: 60
[MASTER] 14:41:16.842 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.88888888888889 - fitness:77.09375 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:16.843 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.09375, number of tests: 2, total length: 47
[MASTER] 14:41:18.128 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.88888888888889 - fitness:76.97424892703863 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:18.128 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.97424892703863, number of tests: 2, total length: 43
[MASTER] 14:41:19.539 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.88888888888889 - fitness:76.86363636363636 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:19.539 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.86363636363636, number of tests: 2, total length: 43
[MASTER] 14:41:19.973 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.875 - fitness:76.76233183856502 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:19.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.76233183856502, number of tests: 2, total length: 43
[MASTER] 14:41:20.265 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.857142857142858 - fitness:76.57894736842105 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:41:20.265 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.57894736842105, number of tests: 3, total length: 51
[MASTER] 14:41:20.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.857142857142858 - fitness:76.49537037037037 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:20.549 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.49537037037037, number of tests: 2, total length: 49
[MASTER] 14:41:20.695 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.88888888888889 - fitness:76.49280575539568 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:20.695 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.49280575539568, number of tests: 2, total length: 42
[MASTER] 14:41:21.138 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.88888888888889 - fitness:76.41463414634147 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:21.138 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.41463414634147, number of tests: 2, total length: 41
[MASTER] 14:41:21.280 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.875 - fitness:76.34220532319392 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:21.280 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.34220532319392, number of tests: 2, total length: 42
[MASTER] 14:41:21.403 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.888888888888886 - fitness:76.27213114754099 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 6
[MASTER] 14:41:21.403 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.27213114754099, number of tests: 3, total length: 71
[MASTER] 14:41:21.841 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 33.888888888888886 - fitness:76.20700636942675 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:21.841 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.20700636942675, number of tests: 2, total length: 43
[MASTER] 14:41:25.995 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 34.888888888888886 - fitness:76.14551083591331 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:25.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14551083591331, number of tests: 2, total length: 50
[MASTER] 14:41:29.915 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.875 - fitness:76.08813559322034 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:29.915 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08813559322034, number of tests: 2, total length: 43
[MASTER] 14:41:30.593 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 35.888888888888886 - fitness:76.08734939759036 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:30.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.08734939759036, number of tests: 2, total length: 45
[MASTER] 14:41:59.216 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.875 - fitness:76.03300330033004 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:41:59.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.03300330033004, number of tests: 3, total length: 93
[MASTER] 14:42:01.030 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 36.888888888888886 - fitness:76.03225806451613 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 4
[MASTER] 14:42:01.030 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.03225806451613, number of tests: 3, total length: 47
[MASTER] 14:46:04.108 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 2819 generations, 1360810 statements, best individuals have fitness: 76.03225806451613
[MASTER] 14:46:04.111 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 46
* Number of covered goals: 2
* Generated 3 tests with total length 45
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     76 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 14:46:04.300 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:46:04.320 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 43 
[MASTER] 14:46:04.348 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 14:46:04.348 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 14:46:04.348 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 14:46:04.348 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 14:46:04.348 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 14:46:04.348 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:46:04.349 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 46
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeInfoFactory_ESTest' to evosuite-tests
* Done!

* Computation finished
