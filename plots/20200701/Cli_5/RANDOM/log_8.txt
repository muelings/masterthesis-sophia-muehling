* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Util
* Starting client
* Connecting to master process on port 17245
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_5_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Util
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 11
[MASTER] 09:01:49.651 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:01:49.770 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:01:50.783 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 1 assertions.
[MASTER] 09:01:50.810 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:01:51.036 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 09:01:51.056 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:01:51.056 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 5 | Tests with assertion: 1
* Generated 1 tests with total length 38
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          1 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 09:01:51.309 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:01:51.332 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 24 
[MASTER] 09:01:51.344 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 09:01:51.345 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 09:01:51.345 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: stripLeadingHyphens:NullPointerException at 4
[MASTER] 09:01:51.345 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 09:01:51.630 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
[MASTER] 09:01:51.633 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 9%
* Total number of goals: 11
* Number of covered goals: 1
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Util_ESTest' to evosuite-tests
* Done!

* Computation finished
