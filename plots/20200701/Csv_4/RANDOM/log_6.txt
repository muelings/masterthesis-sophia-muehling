* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVParser
* Starting client
* Connecting to master process on port 21419
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVParser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 12:14:29.131 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 70
[MASTER] 12:14:29.702 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 12:14:30.903 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:14:30.938 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 12:14:31.290 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 12:14:31.312 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 12:14:31.312 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 12 | Tests with assertion: 1
* Generated 1 tests with total length 22
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          2 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:14:31.781 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:14:31.804 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 14 
[MASTER] 12:14:31.823 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 12:14:31.824 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [getHeaderMap:NullPointerException]
[MASTER] 12:14:31.824 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getHeaderMap:NullPointerException at 10
[MASTER] 12:14:31.824 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [getHeaderMap:NullPointerException]
[MASTER] 12:14:32.106 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 12:14:32.111 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 70
* Number of covered goals: 3
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CSVParser_ESTest' to evosuite-tests
* Done!

* Computation finished
