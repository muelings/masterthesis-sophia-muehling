* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 10417
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 181
[MASTER] 03:48:31.481 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:48:34.235 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var35);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 03:48:34.235 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var35 == var18);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 03:48:34.250 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 03:48:34.250 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 03:48:35.372 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:48:35.392 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var35);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 03:48:35.392 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var35 == var18);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 03:48:35.400 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 03:48:35.400 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 03:48:35.701 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:48:35.718 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var35);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 03:48:35.718 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var35 == var18);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 03:48:35.721 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Keeping 2 assertions.
[MASTER] 03:48:35.721 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 03:48:35.722 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 213 | Tests with assertion: 1
* Generated 1 tests with total length 42
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          4 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 03:48:36.512 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:48:36.530 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 24 
[MASTER] 03:48:36.551 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[], EqualsAssertion=[]}
[MASTER] 03:48:36.943 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 03:48:36.953 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 13%
* Total number of goals: 181
* Number of covered goals: 24
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
