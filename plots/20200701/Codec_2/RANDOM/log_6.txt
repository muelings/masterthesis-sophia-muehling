* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 12883
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 181
[MASTER] 03:53:49.392 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:53:53.275 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var41);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 03:53:53.284 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 03:53:53.284 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 03:53:54.488 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:53:54.506 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var41);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 03:53:54.508 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 03:53:54.508 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 03:53:54.893 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 03:53:54.906 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var41);  // (Primitive) Original Value: 0 | Regression Value: 2
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 380 | Tests with assertion: 1
* Generated 1 tests with total length 44
* GA-Budget:
[MASTER] 03:53:54.909 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 03:53:54.909 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 03:53:54.909 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- MaxTime :                          5 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 03:53:55.701 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:53:55.716 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 29 
[MASTER] 03:53:55.727 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 03:53:55.728 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 03:53:56.015 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
[MASTER] 03:53:56.019 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 12%
* Total number of goals: 181
* Number of covered goals: 22
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
