* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.DoubleMetaphone
* Starting client
* Connecting to master process on port 2367
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.DoubleMetaphone
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 504
[MASTER] 04:40:54.067 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:41:48.583 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:42:07.136 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:42:07.618 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 04:42:08.770 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/language/DoubleMetaphone_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:43:33.884 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:43:55.873 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:45:15.152 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
*** Random test generation finished.
[MASTER] 04:46:01.681 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
*=*=*=* Total tests: 28093 | Tests with assertion: 0
[MASTER] 04:46:01.682 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 0 tests with total length 0
* GA-Budget:
	- MaxTime :                        307 / 300          Finished!
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:46:01.724 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:46:01.725 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 190 seconds more than allowed.
[MASTER] 04:46:01.732 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 04:46:01.732 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 04:46:01.734 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 504
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'DoubleMetaphone_ESTest' to evosuite-tests
* Done!

* Computation finished
