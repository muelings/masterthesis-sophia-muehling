* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.language.DoubleMetaphone
* Starting client
* Connecting to master process on port 5389
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.language.DoubleMetaphone
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:20:23.856 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 504
[MASTER] 04:21:40.220 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:22:26.452 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:22:57.221 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:23:21.268 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:23:37.868 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:24:22.616 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 04:24:49.013 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
*** Random test generation finished.
*=*=*=* Total tests: 23122 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
[MASTER] 04:25:26.137 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- MaxTime :                        302 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 04:25:26.181 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:25:26.182 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 185 seconds more than allowed.
[MASTER] 04:25:26.187 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 04:25:26.187 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 04:25:26.189 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 504
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'DoubleMetaphone_ESTest' to evosuite-tests
* Done!

* Computation finished
