* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Starting client
* Connecting to master process on port 21063
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonDatabind_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 20:57:35.151 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 20:57:35.157 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 288
* Using seed 1593457050426
* Starting evolution
[MASTER] 20:57:35.273 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1305.0 - branchDistance:0.0 - coverage:652.0 - ex: 0 - tex: 0
[MASTER] 20:57:35.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1305.0, number of tests: 1, total length: 0
[MASTER] 21:02:36.175 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 67486 generations, 0 statements, best individuals have fitness: 1305.0
[MASTER] 21:02:36.180 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 10 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ZeroFitness :                  1,305 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 21:02:36.773 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 21:02:36.803 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 21:02:36.831 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 21:02:36.832 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 21:02:36.833 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'AnnotatedClass_ESTest' to evosuite-tests
* Done!

* Computation finished
