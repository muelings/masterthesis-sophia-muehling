* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 8638
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 02:55:09.873 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 02:55:09.882 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 183
* Using seed 1593392105448
* Starting evolution
[MASTER] 02:55:11.970 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:731.5 - branchDistance:0.0 - coverage:302.5 - ex: 0 - tex: 0
[MASTER] 02:55:11.970 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 731.5, number of tests: 1, total length: 20
[MASTER] 02:55:12.110 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:566.9999999981374 - branchDistance:0.0 - coverage:137.99999999813735 - ex: 0 - tex: 2
[MASTER] 02:55:12.110 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 566.9999999981374, number of tests: 8, total length: 145
[MASTER] 02:55:12.936 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:530.9999999981374 - branchDistance:0.0 - coverage:101.99999999813735 - ex: 0 - tex: 6
[MASTER] 02:55:12.937 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 530.9999999981374, number of tests: 8, total length: 195
[MASTER] 02:55:13.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:527.9999999979619 - branchDistance:0.0 - coverage:98.99999999796191 - ex: 0 - tex: 2
[MASTER] 02:55:13.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 527.9999999979619, number of tests: 7, total length: 144
[MASTER] 02:55:13.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:231.59999999431892 - branchDistance:0.0 - coverage:144.99999999431893 - ex: 0 - tex: 4
[MASTER] 02:55:13.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 231.59999999431892, number of tests: 7, total length: 118
[MASTER] 02:55:13.622 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:178.59999051152374 - branchDistance:0.0 - coverage:91.99999051152375 - ex: 0 - tex: 4
[MASTER] 02:55:13.622 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 178.59999051152374, number of tests: 10, total length: 215
[MASTER] 02:55:14.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:142.59999999219318 - branchDistance:0.0 - coverage:55.99999999219316 - ex: 0 - tex: 2
[MASTER] 02:55:14.729 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.59999999219318, number of tests: 10, total length: 232
[MASTER] 02:55:20.953 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:136.37777776997095 - branchDistance:0.0 - coverage:49.77777776997094 - ex: 0 - tex: 0
[MASTER] 02:55:20.953 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.37777776997095, number of tests: 10, total length: 247
[MASTER] 02:55:25.209 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:136.37777776903897 - branchDistance:0.0 - coverage:49.77777776903896 - ex: 0 - tex: 0
[MASTER] 02:55:25.209 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.37777776903897, number of tests: 10, total length: 245
[MASTER] 02:55:25.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:134.3777777681083 - branchDistance:0.0 - coverage:47.777777768108294 - ex: 0 - tex: 0
[MASTER] 02:55:25.931 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.3777777681083, number of tests: 10, total length: 247
[MASTER] 02:55:26.408 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:133.37777777458174 - branchDistance:0.0 - coverage:46.77777777458174 - ex: 0 - tex: 0
[MASTER] 02:55:26.408 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.37777777458174, number of tests: 10, total length: 245
[MASTER] 02:55:26.765 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:133.3777777681083 - branchDistance:0.0 - coverage:46.777777768108294 - ex: 0 - tex: 0
[MASTER] 02:55:26.765 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.3777777681083, number of tests: 10, total length: 251
[MASTER] 02:55:27.498 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:131.93333333013732 - branchDistance:0.0 - coverage:45.33333333013729 - ex: 0 - tex: 0
[MASTER] 02:55:27.499 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.93333333013732, number of tests: 10, total length: 253
[MASTER] 02:55:28.909 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:131.3777777681083 - branchDistance:0.0 - coverage:44.777777768108294 - ex: 0 - tex: 0
[MASTER] 02:55:28.909 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.3777777681083, number of tests: 12, total length: 271
[MASTER] 02:55:29.426 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:129.93333332818153 - branchDistance:0.0 - coverage:43.333333328181524 - ex: 0 - tex: 0
[MASTER] 02:55:29.426 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.93333332818153, number of tests: 11, total length: 301
[MASTER] 02:55:30.428 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:129.37777777262596 - branchDistance:0.0 - coverage:42.77777777262597 - ex: 0 - tex: 0
[MASTER] 02:55:30.429 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.37777777262596, number of tests: 10, total length: 238
[MASTER] 02:55:30.642 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:126.9333333265044 - branchDistance:0.0 - coverage:40.33333332650438 - ex: 0 - tex: 0
[MASTER] 02:55:30.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.9333333265044, number of tests: 12, total length: 334
[MASTER] 02:55:32.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:125.9333333265044 - branchDistance:0.0 - coverage:39.33333332650438 - ex: 0 - tex: 0
[MASTER] 02:55:32.754 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.9333333265044, number of tests: 13, total length: 352
[MASTER] 02:55:35.051 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:123.93333332818153 - branchDistance:0.0 - coverage:37.333333328181524 - ex: 0 - tex: 0
[MASTER] 02:55:35.051 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.93333332818153, number of tests: 12, total length: 331
[MASTER] 02:55:35.951 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:121.37777777262598 - branchDistance:0.0 - coverage:34.77777777262597 - ex: 0 - tex: 0
[MASTER] 02:55:35.951 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.37777777262598, number of tests: 11, total length: 273
[MASTER] 02:55:37.873 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:92.83333333718183 - branchDistance:0.0 - coverage:38.3333333276057 - ex: 0 - tex: 0
[MASTER] 02:55:37.873 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.83333333718183, number of tests: 13, total length: 355
[MASTER] 02:55:40.293 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:88.83333333718183 - branchDistance:0.0 - coverage:34.3333333276057 - ex: 0 - tex: 0
[MASTER] 02:55:40.293 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.83333333718183, number of tests: 13, total length: 358
[MASTER] 02:55:43.128 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:87.33333333718183 - branchDistance:0.0 - coverage:32.8333333276057 - ex: 0 - tex: 0
[MASTER] 02:55:43.128 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.33333333718183, number of tests: 13, total length: 367
[MASTER] 02:55:45.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.962962962962963 - fitness:86.58217053748385 - branchDistance:0.0 - coverage:31.833333328181524 - ex: 0 - tex: 0
[MASTER] 02:55:45.513 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.58217053748385, number of tests: 14, total length: 371
[MASTER] 02:55:46.694 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:84.83333333718183 - branchDistance:0.0 - coverage:30.3333333276057 - ex: 0 - tex: 0
[MASTER] 02:55:46.695 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.83333333718183, number of tests: 13, total length: 359
[MASTER] 02:55:50.438 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:83.83333333815972 - branchDistance:0.0 - coverage:29.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:55:50.438 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.83333333815972, number of tests: 15, total length: 425
[MASTER] 02:55:51.671 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:82.83333333815972 - branchDistance:0.0 - coverage:28.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:55:51.671 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.83333333815972, number of tests: 14, total length: 391
[MASTER] 02:55:54.950 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:81.83333333815972 - branchDistance:0.0 - coverage:27.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:55:54.950 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.83333333815972, number of tests: 15, total length: 411
[MASTER] 02:55:57.097 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:81.83333333815743 - branchDistance:0.0 - coverage:27.333333328581308 - ex: 0 - tex: 0
[MASTER] 02:55:57.097 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.83333333815743, number of tests: 15, total length: 417
[MASTER] 02:55:57.266 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:80.83333333815972 - branchDistance:0.0 - coverage:26.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:55:57.266 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.83333333815972, number of tests: 15, total length: 419
[MASTER] 02:55:58.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:79.33333333815972 - branchDistance:0.0 - coverage:24.83333332858359 - ex: 0 - tex: 0
[MASTER] 02:55:58.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.33333333815972, number of tests: 15, total length: 429
[MASTER] 02:56:00.009 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:78.83333333815972 - branchDistance:0.0 - coverage:24.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:56:00.009 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.83333333815972, number of tests: 15, total length: 418
[MASTER] 02:56:04.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:76.8333333391376 - branchDistance:0.0 - coverage:22.333333329561476 - ex: 0 - tex: 0
[MASTER] 02:56:04.008 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.8333333391376, number of tests: 16, total length: 453
[MASTER] 02:56:05.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:75.83333333815972 - branchDistance:0.0 - coverage:21.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:56:05.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.83333333815972, number of tests: 17, total length: 486
[MASTER] 02:56:07.428 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:74.50000000482638 - branchDistance:0.0 - coverage:19.999999995250256 - ex: 0 - tex: 2
[MASTER] 02:56:07.429 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.50000000482638, number of tests: 17, total length: 488
[MASTER] 02:56:10.655 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:65.38658777265486 - branchDistance:0.0 - coverage:21.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:56:10.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.38658777265486, number of tests: 17, total length: 487
[MASTER] 02:56:11.593 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:64.38658777265486 - branchDistance:0.0 - coverage:20.33333332858359 - ex: 0 - tex: 0
[MASTER] 02:56:11.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.38658777265486, number of tests: 18, total length: 512
[MASTER] 02:56:13.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:63.05325443929023 - branchDistance:0.0 - coverage:18.99999999521895 - ex: 0 - tex: 2
[MASTER] 02:56:13.503 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.05325443929023, number of tests: 18, total length: 526
[MASTER] 02:56:14.779 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:62.386587773632755 - branchDistance:0.0 - coverage:18.333333329561476 - ex: 0 - tex: 0
[MASTER] 02:56:14.779 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.386587773632755, number of tests: 18, total length: 536
[MASTER] 02:56:16.129 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:62.38658777265487 - branchDistance:0.0 - coverage:18.333333328583592 - ex: 0 - tex: 0
[MASTER] 02:56:16.129 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.38658777265487, number of tests: 18, total length: 517
[MASTER] 02:56:17.323 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:62.053254439321535 - branchDistance:0.0 - coverage:17.999999995250256 - ex: 0 - tex: 2
[MASTER] 02:56:17.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.053254439321535, number of tests: 18, total length: 517
[MASTER] 02:56:19.772 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:61.38658777057237 - branchDistance:0.0 - coverage:17.33333332650109 - ex: 0 - tex: 0
[MASTER] 02:56:19.772 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.38658777057237, number of tests: 20, total length: 554
[MASTER] 02:56:19.779 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:60.053254440299426 - branchDistance:0.0 - coverage:15.999999996228144 - ex: 0 - tex: 2
[MASTER] 02:56:19.779 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.053254440299426, number of tests: 20, total length: 528
[MASTER] 02:56:22.116 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:59.053254440299426 - branchDistance:0.0 - coverage:14.999999996228144 - ex: 0 - tex: 2
[MASTER] 02:56:22.116 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.053254440299426, number of tests: 18, total length: 521
[MASTER] 02:56:27.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:57.05325444127731 - branchDistance:0.0 - coverage:12.999999997206032 - ex: 0 - tex: 2
[MASTER] 02:56:27.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.05325444127731, number of tests: 19, total length: 522
[MASTER] 02:56:32.533 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:55.55325444127731 - branchDistance:0.0 - coverage:11.499999997206032 - ex: 0 - tex: 2
[MASTER] 02:56:32.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.55325444127731, number of tests: 19, total length: 535
[MASTER] 02:56:36.063 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:55.05325444407128 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 2
[MASTER] 02:56:36.063 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.05325444407128, number of tests: 20, total length: 556
[MASTER] 02:56:37.736 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:54.05325444407128 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 2
[MASTER] 02:56:37.736 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.05325444407128, number of tests: 20, total length: 577
[MASTER] 02:56:40.985 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:53.55325444127731 - branchDistance:0.0 - coverage:9.499999997206032 - ex: 0 - tex: 2
[MASTER] 02:56:40.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.55325444127731, number of tests: 20, total length: 551
[MASTER] 02:56:42.956 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:52.55325444407128 - branchDistance:0.0 - coverage:8.5 - ex: 0 - tex: 2
[MASTER] 02:56:42.956 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.55325444407128, number of tests: 20, total length: 571
[MASTER] 02:56:49.120 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:52.05325444407128 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 2
[MASTER] 02:56:49.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.05325444407128, number of tests: 22, total length: 607
[MASTER] 02:56:51.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:51.05325444407128 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 02:56:51.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.05325444407128, number of tests: 22, total length: 609
[MASTER] 02:57:01.473 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:50.05325444407128 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 02:57:01.473 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.05325444407128, number of tests: 23, total length: 629
[MASTER] 02:57:03.681 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:49.05325444407128 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 2
[MASTER] 02:57:03.681 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.05325444407128, number of tests: 21, total length: 583
[MASTER] 02:57:04.753 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:48.05325444407128 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 2
[MASTER] 02:57:04.753 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.05325444407128, number of tests: 21, total length: 585
[MASTER] 02:57:38.071 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:47.05325444407128 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 2
[MASTER] 02:57:38.071 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.05325444407128, number of tests: 21, total length: 527
[MASTER] 02:59:06.418 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.941176469156293 - fitness:46.05325444407128 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 2
[MASTER] 02:59:06.418 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.05325444407128, number of tests: 21, total length: 471
[MASTER] 02:59:38.486 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.941176469156293 - fitness:42.11827957501213 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 2
[MASTER] 02:59:38.486 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.11827957501213, number of tests: 21, total length: 430
[MASTER] 03:00:11.008 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 272 generations, 429034 statements, best individuals have fitness: 42.11827957501213
[MASTER] 03:00:11.014 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 99%
* Total number of goals: 183
* Number of covered goals: 181
* Generated 20 tests with total length 411
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                     42 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        302 / 300          Finished!
[MASTER] 03:00:12.151 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:00:12.267 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 343 
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 03:00:12.683 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 19: no assertions
[MASTER] 03:00:12.686 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
[MASTER] 03:00:12.687 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
