* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 4541
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:00:32.475 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:00:32.487 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 183
* Using seed 1593392428229
* Starting evolution
[MASTER] 03:00:34.767 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:240.59856733151827 - branchDistance:0.0 - coverage:153.99856733151825 - ex: 0 - tex: 0
[MASTER] 03:00:34.768 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 240.59856733151827, number of tests: 7, total length: 91
[MASTER] 03:00:37.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:227.59295773201563 - branchDistance:0.0 - coverage:140.9929577320156 - ex: 0 - tex: 0
[MASTER] 03:00:37.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 227.59295773201563, number of tests: 7, total length: 103
[MASTER] 03:00:37.868 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:197.53103447278227 - branchDistance:0.0 - coverage:110.93103447278226 - ex: 0 - tex: 6
[MASTER] 03:00:37.868 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 197.53103447278227, number of tests: 8, total length: 153
[MASTER] 03:00:37.942 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:194.0999999933386 - branchDistance:0.0 - coverage:107.49999999333859 - ex: 0 - tex: 4
[MASTER] 03:00:37.942 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 194.0999999933386, number of tests: 6, total length: 147
[MASTER] 03:00:38.616 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:183.66666640869374 - branchDistance:0.0 - coverage:111.3333330753604 - ex: 0 - tex: 4
[MASTER] 03:00:38.616 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 183.66666640869374, number of tests: 8, total length: 197
[MASTER] 03:00:38.835 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:168.59999999813738 - branchDistance:0.0 - coverage:81.99999999813735 - ex: 0 - tex: 2
[MASTER] 03:00:38.835 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 168.59999999813738, number of tests: 9, total length: 165
[MASTER] 03:00:39.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:157.9333293536152 - branchDistance:0.0 - coverage:71.33332935361518 - ex: 0 - tex: 0
[MASTER] 03:00:39.073 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 157.9333293536152, number of tests: 8, total length: 192
[MASTER] 03:00:40.928 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:156.33333307437582 - branchDistance:0.0 - coverage:83.99999974104249 - ex: 0 - tex: 0
[MASTER] 03:00:40.928 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 156.33333307437582, number of tests: 8, total length: 204
[MASTER] 03:00:42.131 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:136.33333307536037 - branchDistance:0.0 - coverage:63.99999974202706 - ex: 0 - tex: 4
[MASTER] 03:00:42.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.33333307536037, number of tests: 9, total length: 257
[MASTER] 03:00:43.260 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:131.8300915842745 - branchDistance:0.0 - coverage:59.496758250941156 - ex: 0 - tex: 0
[MASTER] 03:00:43.260 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.8300915842745, number of tests: 9, total length: 228
[MASTER] 03:00:45.865 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:129.8300915842745 - branchDistance:0.0 - coverage:57.496758250941156 - ex: 0 - tex: 0
[MASTER] 03:00:45.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.8300915842745, number of tests: 9, total length: 236
[MASTER] 03:00:46.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:128.33332975380742 - branchDistance:0.0 - coverage:55.9999964204741 - ex: 0 - tex: 2
[MASTER] 03:00:46.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.33332975380742, number of tests: 9, total length: 260
[MASTER] 03:00:47.620 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:127.83009158427448 - branchDistance:0.0 - coverage:55.496758250941156 - ex: 0 - tex: 0
[MASTER] 03:00:47.620 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.83009158427448, number of tests: 9, total length: 260
[MASTER] 03:00:47.703 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:126.33332975473925 - branchDistance:0.0 - coverage:53.999996421405925 - ex: 0 - tex: 2
[MASTER] 03:00:47.703 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.33332975473925, number of tests: 9, total length: 277
[MASTER] 03:00:48.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:123.3333297557196 - branchDistance:0.0 - coverage:50.99999642238627 - ex: 0 - tex: 2
[MASTER] 03:00:48.259 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.3333297557196, number of tests: 11, total length: 339
[MASTER] 03:00:49.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:122.33332975195592 - branchDistance:0.0 - coverage:49.99999641862259 - ex: 0 - tex: 4
[MASTER] 03:00:49.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.33332975195592, number of tests: 10, total length: 277
[MASTER] 03:00:49.173 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:117.33332975473925 - branchDistance:0.0 - coverage:44.999996421405925 - ex: 0 - tex: 2
[MASTER] 03:00:49.174 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.33332975473925, number of tests: 11, total length: 314
[MASTER] 03:00:54.692 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:115.33332975473925 - branchDistance:0.0 - coverage:42.999996421405925 - ex: 0 - tex: 4
[MASTER] 03:00:54.692 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.33332975473925, number of tests: 12, total length: 334
[MASTER] 03:00:57.009 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:114.3333330714949 - branchDistance:0.0 - coverage:41.99999973816157 - ex: 0 - tex: 2
[MASTER] 03:00:57.009 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.3333330714949, number of tests: 11, total length: 298
[MASTER] 03:00:58.036 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:113.33333307242182 - branchDistance:0.0 - coverage:40.999999739088494 - ex: 0 - tex: 4
[MASTER] 03:00:58.036 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.33333307242182, number of tests: 12, total length: 336
[MASTER] 03:00:59.188 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:111.33332975473925 - branchDistance:0.0 - coverage:38.999996421405925 - ex: 0 - tex: 2
[MASTER] 03:00:59.188 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.33332975473925, number of tests: 12, total length: 324
[MASTER] 03:01:02.499 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:110.33332975473925 - branchDistance:0.0 - coverage:37.999996421405925 - ex: 0 - tex: 2
[MASTER] 03:01:02.500 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.33332975473925, number of tests: 12, total length: 328
[MASTER] 03:01:03.312 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:109.33332975473925 - branchDistance:0.0 - coverage:36.999996421405925 - ex: 0 - tex: 2
[MASTER] 03:01:03.312 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.33332975473925, number of tests: 12, total length: 354
[MASTER] 03:01:07.606 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:108.33333332853702 - branchDistance:0.0 - coverage:35.999999995203694 - ex: 0 - tex: 2
[MASTER] 03:01:07.606 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.33333332853702, number of tests: 12, total length: 337
[MASTER] 03:01:08.779 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:105.33332975473925 - branchDistance:0.0 - coverage:32.999996421405925 - ex: 0 - tex: 2
[MASTER] 03:01:08.779 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.33332975473925, number of tests: 13, total length: 380
[MASTER] 03:01:11.594 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:104.33332975473925 - branchDistance:0.0 - coverage:31.999996421405925 - ex: 0 - tex: 2
[MASTER] 03:01:11.594 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.33332975473925, number of tests: 13, total length: 379
[MASTER] 03:01:17.463 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:102.3333297557196 - branchDistance:0.0 - coverage:29.999996422386268 - ex: 0 - tex: 2
[MASTER] 03:01:17.463 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.3333297557196, number of tests: 13, total length: 381
[MASTER] 03:01:21.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:102.33332975571959 - branchDistance:0.0 - coverage:29.999996422386264 - ex: 0 - tex: 2
[MASTER] 03:01:21.976 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.33332975571959, number of tests: 13, total length: 381
[MASTER] 03:01:22.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:101.3333297557196 - branchDistance:0.0 - coverage:28.999996422386268 - ex: 0 - tex: 2
[MASTER] 03:01:22.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.3333297557196, number of tests: 14, total length: 397
[MASTER] 03:01:22.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:99.33332975571959 - branchDistance:0.0 - coverage:26.999996422386264 - ex: 0 - tex: 2
[MASTER] 03:01:22.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.33332975571959, number of tests: 14, total length: 402
[MASTER] 03:01:27.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:98.3333297557196 - branchDistance:0.0 - coverage:25.999996422386268 - ex: 0 - tex: 4
[MASTER] 03:01:27.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.3333297557196, number of tests: 15, total length: 435
[MASTER] 03:01:29.919 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:98.33332975571959 - branchDistance:0.0 - coverage:25.999996422386264 - ex: 0 - tex: 2
[MASTER] 03:01:29.919 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.33332975571959, number of tests: 15, total length: 436
[MASTER] 03:01:32.142 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:97.3333297557196 - branchDistance:0.0 - coverage:24.999996422386268 - ex: 0 - tex: 4
[MASTER] 03:01:32.142 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.3333297557196, number of tests: 16, total length: 476
[MASTER] 03:01:33.041 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:97.33332975571959 - branchDistance:0.0 - coverage:24.999996422386264 - ex: 0 - tex: 2
[MASTER] 03:01:33.041 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.33332975571959, number of tests: 15, total length: 445
[MASTER] 03:01:35.699 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:96.3333297557196 - branchDistance:0.0 - coverage:23.999996422386268 - ex: 0 - tex: 4
[MASTER] 03:01:35.699 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.3333297557196, number of tests: 16, total length: 461
[MASTER] 03:01:38.484 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:96.33332975571959 - branchDistance:0.0 - coverage:23.999996422386264 - ex: 0 - tex: 4
[MASTER] 03:01:38.484 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.33332975571959, number of tests: 15, total length: 433
[MASTER] 03:01:40.936 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:94.33332975669992 - branchDistance:0.0 - coverage:21.999996423366603 - ex: 0 - tex: 4
[MASTER] 03:01:40.936 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.33332975669992, number of tests: 16, total length: 460
[MASTER] 03:01:51.802 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:93.33332975669992 - branchDistance:0.0 - coverage:20.999996423366603 - ex: 0 - tex: 6
[MASTER] 03:01:51.803 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.33332975669992, number of tests: 17, total length: 490
[MASTER] 03:01:56.727 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:91.33332975669992 - branchDistance:0.0 - coverage:18.999996423366603 - ex: 0 - tex: 4
[MASTER] 03:01:56.727 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.33332975669992, number of tests: 17, total length: 465
[MASTER] 03:02:00.068 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:90.33332975669992 - branchDistance:0.0 - coverage:17.999996423366603 - ex: 0 - tex: 2
[MASTER] 03:02:00.068 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.33332975669992, number of tests: 17, total length: 459
[MASTER] 03:02:04.775 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:89.33332975669992 - branchDistance:0.0 - coverage:16.999996423366603 - ex: 0 - tex: 4
[MASTER] 03:02:04.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.33332975669992, number of tests: 17, total length: 503
[MASTER] 03:02:05.944 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:80.14285356622375 - branchDistance:0.0 - coverage:17.999996423366603 - ex: 0 - tex: 2
[MASTER] 03:02:05.944 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.14285356622375, number of tests: 17, total length: 451
[MASTER] 03:02:07.515 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:79.14285356622375 - branchDistance:0.0 - coverage:16.999996423366603 - ex: 0 - tex: 2
[MASTER] 03:02:07.515 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.14285356622375, number of tests: 16, total length: 428
[MASTER] 03:02:10.364 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:78.14285356622375 - branchDistance:0.0 - coverage:15.999996423366603 - ex: 0 - tex: 2
[MASTER] 03:02:10.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.14285356622375, number of tests: 17, total length: 436
[MASTER] 03:02:11.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:78.14285195728165 - branchDistance:0.0 - coverage:15.999994814424515 - ex: 0 - tex: 2
[MASTER] 03:02:11.761 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.14285195728165, number of tests: 18, total length: 464
[MASTER] 03:02:13.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:76.14285195728165 - branchDistance:0.0 - coverage:13.999994814424515 - ex: 0 - tex: 2
[MASTER] 03:02:13.646 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.14285195728165, number of tests: 18, total length: 521
[MASTER] 03:02:17.632 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:76.1383828689486 - branchDistance:0.0 - coverage:13.995525726091461 - ex: 0 - tex: 4
[MASTER] 03:02:17.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.1383828689486, number of tests: 18, total length: 530
[MASTER] 03:02:17.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:74.14285195825954 - branchDistance:0.0 - coverage:11.999994815402403 - ex: 0 - tex: 2
[MASTER] 03:02:17.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.14285195825954, number of tests: 18, total length: 521
[MASTER] 03:02:21.747 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:73.14285195825954 - branchDistance:0.0 - coverage:10.999994815402403 - ex: 0 - tex: 4
[MASTER] 03:02:21.747 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.14285195825954, number of tests: 19, total length: 533
[MASTER] 03:02:28.698 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:72.14285714285714 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 2
[MASTER] 03:02:28.698 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.14285714285714, number of tests: 19, total length: 531
[MASTER] 03:02:28.823 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:72.14285195825954 - branchDistance:0.0 - coverage:9.999994815402403 - ex: 0 - tex: 6
[MASTER] 03:02:28.823 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.14285195825954, number of tests: 20, total length: 569
[MASTER] 03:02:36.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:71.14285195825954 - branchDistance:0.0 - coverage:8.999994815402403 - ex: 0 - tex: 2
[MASTER] 03:02:36.911 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.14285195825954, number of tests: 20, total length: 540
[MASTER] 03:02:43.095 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:70.14285195825954 - branchDistance:0.0 - coverage:7.9999948154024025 - ex: 0 - tex: 2
[MASTER] 03:02:43.095 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.14285195825954, number of tests: 20, total length: 540
[MASTER] 03:02:51.483 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:69.14285195825954 - branchDistance:0.0 - coverage:6.9999948154024025 - ex: 0 - tex: 2
[MASTER] 03:02:51.483 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.14285195825954, number of tests: 20, total length: 537
[MASTER] 03:03:00.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:68.14285714285714 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 2
[MASTER] 03:03:00.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.14285714285714, number of tests: 20, total length: 536
[MASTER] 03:03:05.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:67.14285714285714 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 2
[MASTER] 03:03:05.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.14285714285714, number of tests: 20, total length: 524
[MASTER] 03:03:28.589 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:66.14285714285714 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 4
[MASTER] 03:03:28.589 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.14285714285714, number of tests: 21, total length: 529
[MASTER] 03:05:20.716 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:65.14285714285714 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 4
[MASTER] 03:05:20.717 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.14285714285714, number of tests: 18, total length: 415
[MASTER] 03:05:33.631 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 262 generations, 416612 statements, best individuals have fitness: 65.14285714285714
[MASTER] 03:05:33.635 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 98%
* Total number of goals: 183
* Number of covered goals: 180
* Generated 19 tests with total length 404
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     65 / 0           
	- MaxTime :                        302 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 03:05:34.842 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:05:34.986 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 323 
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 03:05:35.402 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 03:05:35.403 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 03:05:35.404 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 03:05:35.404 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
