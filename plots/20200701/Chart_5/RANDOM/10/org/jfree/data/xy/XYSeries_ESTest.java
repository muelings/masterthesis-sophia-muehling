/*
 * This file was automatically generated by EvoSuite
 * Sun Jun 28 21:49:57 GMT 2020
 */

package org.jfree.data.xy;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jfree.data.time.Minute;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYSeries_ESTest extends XYSeries_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Minute minute0 = new Minute();
      XYSeries xYSeries0 = new XYSeries(minute0, true);
      xYSeries0.add((Number) 59, (Number) 0, true);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.IndexOutOfBoundsException : Index: -1, Size: 1
      xYSeries0.addOrUpdate((double) 59, (double) 0);
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Null 'item' argument.
      // Undeclared exception!
      try { 
        xYSeries0.add((XYDataItem) null, true);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Null 'item' argument.
         //
         verifyException("org.jfree.data.xy.XYSeries", e);
         assertTrue(e.getMessage().equals("Null 'item' argument."));   
      }
  }
}
