* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.xy.XYSeries
* Starting client
* Connecting to master process on port 8373
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_5_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.xy.XYSeries
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 103
[MASTER] 23:27:07.019 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:27:15.351 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 23:27:16.604 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 2 assertions.
[MASTER] 23:27:16.619 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 23:27:17.014 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 23:27:17.031 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 23:27:17.031 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 602 | Tests with assertion: 1
* Generated 1 tests with total length 18
* GA-Budget:
	- MaxTime :                         10 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 23:27:17.779 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:27:17.805 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 15 
[MASTER] 23:27:17.815 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 23:27:17.816 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [ArrayIndexOutOfBoundsException:remove-419,, addOrUpdate:IndexOutOfBoundsException, remove:ArrayIndexOutOfBoundsException]
[MASTER] 23:27:17.816 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: remove:ArrayIndexOutOfBoundsException at 14
[MASTER] 23:27:17.816 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [ArrayIndexOutOfBoundsException:remove-419,, addOrUpdate:IndexOutOfBoundsException, remove:ArrayIndexOutOfBoundsException]
[MASTER] 23:27:17.816 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: addOrUpdate:IndexOutOfBoundsException at 8
[MASTER] 23:27:17.816 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [ArrayIndexOutOfBoundsException:remove-419,, addOrUpdate:IndexOutOfBoundsException, remove:ArrayIndexOutOfBoundsException]
[MASTER] 23:27:17.975 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
[MASTER] 23:27:17.979 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 19%
* Total number of goals: 103
* Number of covered goals: 20
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYSeries_ESTest' to evosuite-tests
* Done!

* Computation finished
