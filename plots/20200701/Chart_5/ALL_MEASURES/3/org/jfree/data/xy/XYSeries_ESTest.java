/*
 * This file was automatically generated by EvoSuite
 * Sun Jun 28 21:15:44 GMT 2020
 */

package org.jfree.data.xy;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.evosuite.runtime.mock.java.util.MockDate;
import org.jfree.data.time.Hour;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYSeries_ESTest extends XYSeries_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      MockDate mockDate0 = new MockDate(1296L);
      Hour hour0 = new Hour(mockDate0);
      XYSeries xYSeries0 = new XYSeries(hour0, true);
      xYSeries0.add(Double.NEGATIVE_INFINITY, 830.699072);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.ArrayIndexOutOfBoundsException : null
      xYSeries0.addOrUpdate(Double.NEGATIVE_INFINITY, (double) (-1));
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Null 'item' argument.
      // Undeclared exception!
      try { 
        xYSeries0.add((XYDataItem) null, true);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Null 'item' argument.
         //
         verifyException("org.jfree.data.xy.XYSeries", e);
         assertTrue(e.getMessage().equals("Null 'item' argument."));   
      }
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      MockDate mockDate0 = new MockDate(1311L);
      Hour hour0 = new Hour(mockDate0);
      XYSeries xYSeries0 = new XYSeries(hour0, true);
      xYSeries0.add((double) 0, (Number) 23);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.IndexOutOfBoundsException : Index: -1, Size: 2
      xYSeries0.addOrUpdate(0.0, 0.0);
  }
}
