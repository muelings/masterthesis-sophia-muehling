* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 11312
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:47:45.047 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:47:45.055 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
* Using seed 1593406061782
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 06:47:46.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:333.8464285714286 - branchDistance:0.0 - coverage:118.84642857142858 - ex: 0 - tex: 20
[MASTER] 06:47:46.259 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 333.8464285714286, number of tests: 10, total length: 253
[MASTER] 06:47:47.209 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:330.3464285714286 - branchDistance:0.0 - coverage:115.34642857142858 - ex: 0 - tex: 14
[MASTER] 06:47:47.209 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 330.3464285714286, number of tests: 7, total length: 135
[MASTER] 06:47:47.276 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:286.5642857142857 - branchDistance:0.0 - coverage:178.56428571428572 - ex: 0 - tex: 16
[MASTER] 06:47:47.276 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 286.5642857142857, number of tests: 8, total length: 206
[MASTER] 06:47:48.055 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:216.49021164021164 - branchDistance:0.0 - coverage:154.06428571428572 - ex: 0 - tex: 12
[MASTER] 06:47:48.055 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 216.49021164021164, number of tests: 6, total length: 143
[MASTER] 06:47:49.340 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.4838709677419355 - fitness:161.2723544973545 - branchDistance:0.0 - coverage:98.84642857142858 - ex: 0 - tex: 12
[MASTER] 06:47:49.340 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 161.2723544973545, number of tests: 6, total length: 150
[MASTER] 06:47:49.655 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:147.3264491711617 - branchDistance:0.0 - coverage:107.34642857142858 - ex: 0 - tex: 16
[MASTER] 06:47:49.655 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 147.3264491711617, number of tests: 9, total length: 233
[MASTER] 06:47:49.972 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:124.4419253616379 - branchDistance:0.0 - coverage:84.46190476190476 - ex: 0 - tex: 14
[MASTER] 06:47:49.972 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.4419253616379, number of tests: 8, total length: 248
[MASTER] 06:47:51.224 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:122.60859202830457 - branchDistance:0.0 - coverage:82.62857142857143 - ex: 0 - tex: 12
[MASTER] 06:47:51.224 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.60859202830457, number of tests: 7, total length: 157
[MASTER] 06:47:52.145 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:120.4419253616379 - branchDistance:0.0 - coverage:80.46190476190476 - ex: 0 - tex: 16
[MASTER] 06:47:52.145 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.4419253616379, number of tests: 8, total length: 226
[MASTER] 06:47:52.271 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:116.4419253616379 - branchDistance:0.0 - coverage:76.46190476190476 - ex: 0 - tex: 12
[MASTER] 06:47:52.271 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.4419253616379, number of tests: 7, total length: 134
[MASTER] 06:47:53.200 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:115.4419253616379 - branchDistance:0.0 - coverage:75.46190476190476 - ex: 0 - tex: 12
[MASTER] 06:47:53.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.4419253616379, number of tests: 8, total length: 149
[MASTER] 06:47:54.383 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:112.4419253616379 - branchDistance:0.0 - coverage:72.46190476190476 - ex: 0 - tex: 16
[MASTER] 06:47:54.383 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.4419253616379, number of tests: 9, total length: 169
[MASTER] 06:47:55.486 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:110.4419253616379 - branchDistance:0.0 - coverage:70.46190476190476 - ex: 0 - tex: 16
[MASTER] 06:47:55.486 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.4419253616379, number of tests: 9, total length: 171
[MASTER] 06:47:56.738 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:109.31097298068552 - branchDistance:0.0 - coverage:69.33095238095238 - ex: 0 - tex: 16
[MASTER] 06:47:56.738 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.31097298068552, number of tests: 10, total length: 176
[MASTER] 06:47:58.099 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:108.31097298068552 - branchDistance:0.0 - coverage:68.33095238095238 - ex: 0 - tex: 14
[MASTER] 06:47:58.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.31097298068552, number of tests: 9, total length: 138
[MASTER] 06:47:58.317 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:107.31097298068552 - branchDistance:0.0 - coverage:67.33095238095238 - ex: 0 - tex: 16
[MASTER] 06:47:58.318 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.31097298068552, number of tests: 10, total length: 178
[MASTER] 06:48:11.514 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:106.31097298068552 - branchDistance:0.0 - coverage:66.33095238095238 - ex: 0 - tex: 16
[MASTER] 06:48:11.514 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.31097298068552, number of tests: 10, total length: 179
[MASTER] 06:48:13.237 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:105.31097298068552 - branchDistance:0.0 - coverage:65.33095238095238 - ex: 0 - tex: 14
[MASTER] 06:48:13.237 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.31097298068552, number of tests: 9, total length: 151
[MASTER] 06:48:14.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:100.74549679020933 - branchDistance:0.0 - coverage:60.76547619047619 - ex: 0 - tex: 18
[MASTER] 06:48:14.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.74549679020933, number of tests: 10, total length: 181
[MASTER] 06:48:14.722 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:99.74549679020933 - branchDistance:0.0 - coverage:59.76547619047619 - ex: 0 - tex: 18
[MASTER] 06:48:14.722 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.74549679020933, number of tests: 10, total length: 182
[MASTER] 06:48:14.888 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:94.18002059973314 - branchDistance:0.0 - coverage:54.2 - ex: 0 - tex: 16
[MASTER] 06:48:14.888 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.18002059973314, number of tests: 10, total length: 181
[MASTER] 06:48:15.106 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:93.18002059973314 - branchDistance:0.0 - coverage:53.2 - ex: 0 - tex: 14
[MASTER] 06:48:15.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.18002059973314, number of tests: 9, total length: 187
[MASTER] 06:48:16.844 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:90.18002059973314 - branchDistance:0.0 - coverage:50.2 - ex: 0 - tex: 16
[MASTER] 06:48:16.844 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.18002059973314, number of tests: 10, total length: 177
[MASTER] 06:48:21.410 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:89.18002059973314 - branchDistance:0.0 - coverage:49.2 - ex: 0 - tex: 22
[MASTER] 06:48:21.410 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.18002059973314, number of tests: 11, total length: 236
[MASTER] 06:48:22.609 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:89.08002059973313 - branchDistance:0.0 - coverage:49.1 - ex: 0 - tex: 18
[MASTER] 06:48:22.609 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.08002059973313, number of tests: 11, total length: 198
[MASTER] 06:48:23.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:88.78002059973315 - branchDistance:0.0 - coverage:48.800000000000004 - ex: 0 - tex: 12
[MASTER] 06:48:23.720 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.78002059973315, number of tests: 9, total length: 152
[MASTER] 06:48:26.240 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:87.78002059973315 - branchDistance:0.0 - coverage:47.800000000000004 - ex: 0 - tex: 14
[MASTER] 06:48:26.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.78002059973315, number of tests: 9, total length: 189
[MASTER] 06:48:26.980 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.489991967871486 - fitness:86.78002059973315 - branchDistance:0.0 - coverage:46.800000000000004 - ex: 0 - tex: 14
[MASTER] 06:48:26.980 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.78002059973315, number of tests: 9, total length: 170
[MASTER] 06:48:28.195 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:70.68652051133766 - branchDistance:0.0 - coverage:49.2 - ex: 0 - tex: 16
[MASTER] 06:48:28.195 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.68652051133766, number of tests: 10, total length: 229
[MASTER] 06:48:31.926 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:69.68652051133766 - branchDistance:0.0 - coverage:48.2 - ex: 0 - tex: 18
[MASTER] 06:48:31.926 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.68652051133766, number of tests: 11, total length: 252
[MASTER] 06:48:34.205 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:67.58652051133765 - branchDistance:0.0 - coverage:46.1 - ex: 0 - tex: 16
[MASTER] 06:48:34.205 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.58652051133765, number of tests: 12, total length: 250
[MASTER] 06:48:36.497 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:67.28652051133767 - branchDistance:0.0 - coverage:45.800000000000004 - ex: 0 - tex: 16
[MASTER] 06:48:36.497 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.28652051133767, number of tests: 11, total length: 209
[MASTER] 06:48:39.886 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:66.58652051133765 - branchDistance:0.0 - coverage:45.1 - ex: 0 - tex: 16
[MASTER] 06:48:39.886 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.58652051133765, number of tests: 11, total length: 204
[MASTER] 06:48:40.797 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:65.18652051133766 - branchDistance:0.0 - coverage:43.7 - ex: 0 - tex: 16
[MASTER] 06:48:40.797 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.18652051133766, number of tests: 12, total length: 214
[MASTER] 06:48:44.737 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:62.994474618853275 - branchDistance:0.0 - coverage:44.800000000000004 - ex: 0 - tex: 20
[MASTER] 06:48:44.737 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.994474618853275, number of tests: 11, total length: 219
[MASTER] 06:48:48.871 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:61.994474618853275 - branchDistance:0.0 - coverage:43.800000000000004 - ex: 0 - tex: 18
[MASTER] 06:48:48.871 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.994474618853275, number of tests: 11, total length: 227
[MASTER] 06:48:50.440 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:61.003187178004325 - branchDistance:0.0 - coverage:39.516666666666666 - ex: 0 - tex: 18
[MASTER] 06:48:50.440 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.003187178004325, number of tests: 12, total length: 231
[MASTER] 06:48:51.614 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:60.89447461885327 - branchDistance:0.0 - coverage:42.7 - ex: 0 - tex: 18
[MASTER] 06:48:51.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.89447461885327, number of tests: 12, total length: 232
[MASTER] 06:48:52.572 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.445892941242416 - fitness:60.003187178004325 - branchDistance:0.0 - coverage:38.516666666666666 - ex: 0 - tex: 18
[MASTER] 06:48:52.572 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.003187178004325, number of tests: 12, total length: 231
[MASTER] 06:48:53.784 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:59.89447461885327 - branchDistance:0.0 - coverage:41.7 - ex: 0 - tex: 18
[MASTER] 06:48:53.784 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.89447461885327, number of tests: 12, total length: 218
[MASTER] 06:48:54.096 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:57.711141285519936 - branchDistance:0.0 - coverage:39.516666666666666 - ex: 0 - tex: 16
[MASTER] 06:48:54.096 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.711141285519936, number of tests: 12, total length: 239
[MASTER] 06:48:55.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:57.04447461885327 - branchDistance:0.0 - coverage:38.85 - ex: 0 - tex: 18
[MASTER] 06:48:55.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.04447461885327, number of tests: 12, total length: 254
[MASTER] 06:49:08.445 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:56.711141285519936 - branchDistance:0.0 - coverage:38.516666666666666 - ex: 0 - tex: 18
[MASTER] 06:49:08.445 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.711141285519936, number of tests: 12, total length: 250
[MASTER] 06:49:16.647 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:56.211141285519936 - branchDistance:0.0 - coverage:38.516666666666666 - ex: 1 - tex: 17
[MASTER] 06:49:16.647 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.211141285519936, number of tests: 12, total length: 210
[MASTER] 06:49:18.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:55.711141285519936 - branchDistance:0.0 - coverage:37.516666666666666 - ex: 0 - tex: 20
[MASTER] 06:49:18.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.711141285519936, number of tests: 13, total length: 217
[MASTER] 06:49:37.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:55.211141285519936 - branchDistance:0.0 - coverage:37.516666666666666 - ex: 1 - tex: 19
[MASTER] 06:49:37.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.211141285519936, number of tests: 13, total length: 217
[MASTER] 06:49:53.415 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:55.04447461885327 - branchDistance:0.0 - coverage:37.516666666666666 - ex: 2 - tex: 20
[MASTER] 06:49:53.415 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.04447461885327, number of tests: 14, total length: 243
[MASTER] 06:49:55.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:52.711141285519936 - branchDistance:0.0 - coverage:34.516666666666666 - ex: 0 - tex: 20
[MASTER] 06:49:55.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.711141285519936, number of tests: 13, total length: 242
[MASTER] 06:50:07.644 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:52.211141285519936 - branchDistance:0.0 - coverage:34.516666666666666 - ex: 1 - tex: 19
[MASTER] 06:50:07.645 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.211141285519936, number of tests: 13, total length: 220
[MASTER] 06:50:09.290 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:52.04447461885327 - branchDistance:0.0 - coverage:34.516666666666666 - ex: 2 - tex: 20
[MASTER] 06:50:09.291 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.04447461885327, number of tests: 14, total length: 241
[MASTER] 06:50:22.694 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:52.04447461776673 - branchDistance:0.0 - coverage:34.516666665580125 - ex: 2 - tex: 22
[MASTER] 06:50:22.694 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.04447461776673, number of tests: 15, total length: 253
[MASTER] 06:50:22.880 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:51.961141285519936 - branchDistance:0.0 - coverage:34.516666666666666 - ex: 3 - tex: 21
[MASTER] 06:50:22.880 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.961141285519936, number of tests: 15, total length: 246
[MASTER] 06:50:22.921 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:51.211141285519936 - branchDistance:0.0 - coverage:33.516666666666666 - ex: 1 - tex: 21
[MASTER] 06:50:22.921 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.211141285519936, number of tests: 14, total length: 253
[MASTER] 06:50:23.216 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:51.04447461885327 - branchDistance:0.0 - coverage:33.516666666666666 - ex: 2 - tex: 20
[MASTER] 06:50:23.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.04447461885327, number of tests: 15, total length: 235
[MASTER] 06:50:24.853 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:49.44447461885327 - branchDistance:0.0 - coverage:32.0 - ex: 3 - tex: 21
[MASTER] 06:50:24.853 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.44447461885327, number of tests: 14, total length: 225
[MASTER] 06:50:29.292 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:47.19447461885327 - branchDistance:0.0 - coverage:29.666666666666664 - ex: 2 - tex: 22
[MASTER] 06:50:29.292 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.19447461885327, number of tests: 14, total length: 249
[MASTER] 06:50:34.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:46.19447461885327 - branchDistance:0.0 - coverage:28.666666666666664 - ex: 2 - tex: 24
[MASTER] 06:50:34.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.19447461885327, number of tests: 15, total length: 260
[MASTER] 06:50:37.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:45.19447461885327 - branchDistance:0.0 - coverage:27.666666666666664 - ex: 2 - tex: 24
[MASTER] 06:50:37.977 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.19447461885327, number of tests: 15, total length: 285
[MASTER] 06:50:43.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:43.19447461885327 - branchDistance:0.0 - coverage:25.666666666666664 - ex: 2 - tex: 22
[MASTER] 06:50:43.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 43.19447461885327, number of tests: 15, total length: 263
[MASTER] 06:51:04.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:42.19447461885327 - branchDistance:0.0 - coverage:24.666666666666664 - ex: 2 - tex: 24
[MASTER] 06:51:04.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.19447461885327, number of tests: 16, total length: 261
[MASTER] 06:51:09.162 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:41.19447461885327 - branchDistance:0.0 - coverage:23.666666666666664 - ex: 2 - tex: 28
[MASTER] 06:51:09.162 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.19447461885327, number of tests: 17, total length: 289
[MASTER] 06:51:24.258 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:40.19447461885327 - branchDistance:0.0 - coverage:22.666666666666664 - ex: 2 - tex: 26
[MASTER] 06:51:24.258 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.19447461885327, number of tests: 17, total length: 287
[MASTER] 06:51:26.806 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:38.19447461885327 - branchDistance:0.0 - coverage:20.666666666666664 - ex: 2 - tex: 22
[MASTER] 06:51:26.806 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.19447461885327, number of tests: 17, total length: 302
[MASTER] 06:51:29.002 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:38.111141285519935 - branchDistance:0.0 - coverage:20.666666666666664 - ex: 3 - tex: 25
[MASTER] 06:51:29.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.111141285519935, number of tests: 18, total length: 320
[MASTER] 06:51:30.295 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:37.19447461885327 - branchDistance:0.0 - coverage:19.666666666666664 - ex: 2 - tex: 24
[MASTER] 06:51:30.295 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 37.19447461885327, number of tests: 17, total length: 296
[MASTER] 06:51:33.108 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:37.111141285519935 - branchDistance:0.0 - coverage:19.666666666666664 - ex: 3 - tex: 25
[MASTER] 06:51:33.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 37.111141285519935, number of tests: 18, total length: 315
[MASTER] 06:51:35.442 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:36.111141285519935 - branchDistance:0.0 - coverage:18.666666666666664 - ex: 3 - tex: 25
[MASTER] 06:51:35.442 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.111141285519935, number of tests: 17, total length: 302
[MASTER] 06:51:38.515 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:35.19447461885327 - branchDistance:0.0 - coverage:17.666666666666664 - ex: 2 - tex: 28
[MASTER] 06:51:38.515 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.19447461885327, number of tests: 18, total length: 323
[MASTER] 06:51:40.844 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:35.111141285519935 - branchDistance:0.0 - coverage:17.666666666666664 - ex: 3 - tex: 27
[MASTER] 06:51:40.844 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.111141285519935, number of tests: 18, total length: 320
[MASTER] 06:52:00.274 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:34.19447461885327 - branchDistance:0.0 - coverage:16.666666666666664 - ex: 2 - tex: 30
[MASTER] 06:52:00.274 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.19447461885327, number of tests: 19, total length: 368
[MASTER] 06:52:00.644 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:34.111141285519935 - branchDistance:0.0 - coverage:16.666666666666664 - ex: 3 - tex: 27
[MASTER] 06:52:00.644 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.111141285519935, number of tests: 19, total length: 339
[MASTER] 06:52:38.031 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.445858611193323 - fitness:33.111141285519935 - branchDistance:0.0 - coverage:15.666666666666666 - ex: 3 - tex: 31
[MASTER] 06:52:38.031 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.111141285519935, number of tests: 19, total length: 341
[MASTER] 06:52:48.735 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 304s and 216 generations, 169947 statements, best individuals have fitness: 33.111141285519935
[MASTER] 06:52:48.740 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 87%
* Total number of goals: 97
* Number of covered goals: 84
* Generated 19 tests with total length 341
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     33 / 0           
	- MaxTime :                        304 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 06:52:49.417 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:52:49.611 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 250 
[MASTER] 06:52:49.895 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.895 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 1.0
[MASTER] 06:52:49.895 [logback-1] WARN  RegressionSuiteMinimizer - Test1, uniqueExceptions: [close:NullPointerException, NullPointerException:write-412,]
[MASTER] 06:52:49.895 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:NullPointerException at 4
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.896 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 1.0
[MASTER] 06:52:49.897 [logback-1] WARN  RegressionSuiteMinimizer - Test11, uniqueExceptions: [close:NullPointerException, IOException:writeHeader-176,, close:IOException, NullPointerException:write-412,]
[MASTER] 06:52:49.897 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:IOException at 10
[MASTER] 06:52:49.897 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 1.0
[MASTER] 06:52:49.897 [logback-1] WARN  RegressionSuiteMinimizer - Test12, uniqueExceptions: [close:NullPointerException, IOException:writeHeader-176,, close:IOException, NullPointerException:write-412,]
[MASTER] 06:52:49.897 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:IOException at 5
[MASTER] 06:52:49.897 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 5
[MASTER] 06:52:49.901 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 1.0
[MASTER] 06:52:49.901 [logback-1] WARN  RegressionSuiteMinimizer - Test13, uniqueExceptions: [close:NullPointerException, IOException:writeHeader-176,, close:IOException, NullPointerException:write-412,]
[MASTER] 06:52:49.901 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:IOException at 12
[MASTER] 06:52:49.901 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 12
[MASTER] 06:52:49.911 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.913 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.913 [logback-1] WARN  RegressionSuiteMinimizer - Test16 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.914 [logback-1] WARN  RegressionSuiteMinimizer - Test17 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.914 [logback-1] WARN  RegressionSuiteMinimizer - Test18 - Difference in number of exceptions: 0.0
[MASTER] 06:52:49.914 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [close:NullPointerException, IOException:writeHeader-176,, close:IOException, NullPointerException:write-412,]
[MASTER] 06:52:49.914 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:52:49.914 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 06:52:49.915 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 06:52:49.916 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 06:52:49.916 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 06:52:49.916 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 06:52:50.233 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
[MASTER] 06:52:50.234 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 30%
* Total number of goals: 97
* Number of covered goals: 29
* Generated 2 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 164
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
