* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 12753
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 97
[MASTER] 06:15:00.644 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:15:01.949 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 06:15:03.181 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 1 assertions.
[MASTER] 06:15:03.202 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 06:15:03.590 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 87 | Tests with assertion: 1
[MASTER] 06:15:03.597 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
* Generated 1 tests with total length 5
[MASTER] 06:15:03.599 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- MaxTime :                          3 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 06:15:04.436 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:15:04.452 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 06:15:04.462 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 06:15:04.462 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [close:NullPointerException, NullPointerException:writeHeader-176,]
[MASTER] 06:15:04.462 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:NullPointerException at 1
[MASTER] 06:15:04.462 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [close:NullPointerException, NullPointerException:writeHeader-176,]
[MASTER] 06:15:04.492 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 06:15:04.496 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 25%
* Total number of goals: 97
* Number of covered goals: 24
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 8
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
