* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 5126
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 06:04:13.861 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
[MASTER] 06:04:15.266 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 06:04:16.639 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 1 assertions.
[MASTER] 06:04:16.652 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 06:04:17.055 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 62 | Tests with assertion: 1
[MASTER] 06:04:17.073 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
* Generated 1 tests with total length 4
[MASTER] 06:04:17.073 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          3 / 300         
[MASTER] 06:04:17.930 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:04:17.953 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 3 
[MASTER] 06:04:17.963 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 06:04:17.965 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [close:NullPointerException, NullPointerException:writeHeader-176,]
[MASTER] 06:04:17.965 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:NullPointerException at 2
[MASTER] 06:04:17.965 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [close:NullPointerException, NullPointerException:writeHeader-176,]
[MASTER] 06:04:18.026 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 06:04:18.031 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 25%
* Total number of goals: 97
* Number of covered goals: 24
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
