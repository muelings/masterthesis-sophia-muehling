* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 4833
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 17:39:31.635 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 141
[MASTER] 17:41:04.779 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:41:05.871 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:41:05.880 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 17:41:06.135 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 0 assertions.
[MASTER] 17:41:06.164 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:41:06.406 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:41:21.837 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:41:22.054 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_7_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:42:52.268 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:42:52.402 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
Keeping 0 assertions.
*** Random test generation finished.
[MASTER] 17:44:32.637 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 95266 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 17:44:32.674 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:44:32.675 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 182 seconds more than allowed.
[MASTER] 17:44:32.681 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 17:44:32.681 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 17:44:32.684 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 141
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
