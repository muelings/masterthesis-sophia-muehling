/*
 * This file was automatically generated by EvoSuite
 * Mon Jun 29 16:22:26 GMT 2020
 */

package com.fasterxml.jackson.core.util;

import org.junit.Test;
import static org.junit.Assert.*;
import com.fasterxml.jackson.core.util.BufferRecycler;
import com.fasterxml.jackson.core.util.TextBuffer;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class TextBuffer_ESTest extends TextBuffer_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      BufferRecycler bufferRecycler0 = new BufferRecycler();
      TextBuffer textBuffer0 = new TextBuffer(bufferRecycler0);
      textBuffer0.append('?');
      //  // Unstable assertion: assertEquals(409601, textBuffer0.size()); // (Inspector) Original Value: 409601 | Regression Value: 262145
  }
}
