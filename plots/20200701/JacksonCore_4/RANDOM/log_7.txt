* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 4282
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 17:57:36.191 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 141
[MASTER] 17:57:53.143 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671745, var3.size());  // (Inspector) Original Value: 671745 | Regression Value: 524289
[MASTER] 17:57:53.144 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671745, var3.size());  // (Inspector) Original Value: 671745 | Regression Value: 524289
[MASTER] 17:57:53.144 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671745, var3.size());  // (Inspector) Original Value: 671745 | Regression Value: 524289
[MASTER] 17:57:53.144 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671747, var3.size());  // (Inspector) Original Value: 671747 | Regression Value: 524291
[MASTER] 17:57:53.163 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 17:57:53.163 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 17:57:54.255 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:57:54.273 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671745, var3.size());  // (Inspector) Original Value: 671745 | Regression Value: 524289
[MASTER] 17:57:54.273 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671745, var3.size());  // (Inspector) Original Value: 671745 | Regression Value: 524289
[MASTER] 17:57:54.273 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671745, var3.size());  // (Inspector) Original Value: 671745 | Regression Value: 524289
[MASTER] 17:57:54.273 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(671747, var3.size());  // (Inspector) Original Value: 671747 | Regression Value: 524291
[MASTER] 17:57:54.275 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 17:57:54.275 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 17:59:02.428 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:59:02.733 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:59:02.788 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:59:03.041 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_5_tmp__ESTest.class' should be in target project, but could not be found!
*** Random test generation finished.
[MASTER] 18:02:37.203 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 98813 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 18:02:37.233 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 18:02:37.233 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 183 seconds more than allowed.
[MASTER] 18:02:37.238 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 18:02:37.238 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 18:02:37.240 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 141
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
