* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 17979
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 141
[MASTER] 16:58:37.954 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:00:09.818 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 17:00:10.928 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 17:01:31.595 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 17:03:38.957 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 92715 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 17:03:38.993 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 17:03:38.994 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 183 seconds more than allowed.
[MASTER] 17:03:39.002 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 17:03:39.002 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 17:03:39.006 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 141
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
