* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Period
* Starting client
* Connecting to master process on port 9899
* Analyzing classpath: 
  - ../defects4j_compiled/Time_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Period
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:48:07.479 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:48:07.485 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 131
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593485283473
* Starting evolution
[MASTER] 04:48:09.662 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:574.0 - branchDistance:0.0 - coverage:277.0 - ex: 0 - tex: 2
[MASTER] 04:48:09.662 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 574.0, number of tests: 1, total length: 18
[MASTER] 04:48:09.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:561.0 - branchDistance:0.0 - coverage:264.0 - ex: 0 - tex: 4
[MASTER] 04:48:09.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 561.0, number of tests: 2, total length: 43
[MASTER] 04:48:10.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:546.9930555555555 - branchDistance:0.0 - coverage:249.99305555555554 - ex: 0 - tex: 12
[MASTER] 04:48:10.333 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 546.9930555555555, number of tests: 7, total length: 163
[MASTER] 04:48:10.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:506.8461538461538 - branchDistance:0.0 - coverage:209.84615384615384 - ex: 0 - tex: 20
[MASTER] 04:48:10.678 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 506.8461538461538, number of tests: 10, total length: 246
[MASTER] 04:48:11.026 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:499.0 - branchDistance:0.0 - coverage:202.0 - ex: 0 - tex: 16
[MASTER] 04:48:11.026 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 499.0, number of tests: 9, total length: 227
[MASTER] 04:48:11.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:491.0 - branchDistance:0.0 - coverage:194.0 - ex: 0 - tex: 14
[MASTER] 04:48:11.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 491.0, number of tests: 9, total length: 192
[MASTER] 04:48:11.487 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:477.0 - branchDistance:0.0 - coverage:180.0 - ex: 0 - tex: 12
[MASTER] 04:48:11.487 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 477.0, number of tests: 9, total length: 168
[MASTER] 04:48:11.553 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:469.5 - branchDistance:0.0 - coverage:172.5 - ex: 0 - tex: 6
[MASTER] 04:48:11.553 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 469.5, number of tests: 5, total length: 106
[MASTER] 04:48:12.061 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:452.0 - branchDistance:0.0 - coverage:155.0 - ex: 0 - tex: 14
[MASTER] 04:48:12.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 452.0, number of tests: 10, total length: 182
* Computation finished
