* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Period
* Starting client
* Connecting to master process on port 3903
* Analyzing classpath: 
  - ../defects4j_compiled/Time_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Period
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:51:09.849 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:51:09.854 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 131
* Using seed 1593485465790
* Starting evolution
[MASTER] 04:51:12.374 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:545.0 - branchDistance:0.0 - coverage:248.0 - ex: 0 - tex: 12
[MASTER] 04:51:12.374 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 545.0, number of tests: 8, total length: 152
[MASTER] 04:51:12.517 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:511.0 - branchDistance:0.0 - coverage:214.0 - ex: 0 - tex: 8
[MASTER] 04:51:12.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 511.0, number of tests: 8, total length: 159
[MASTER] 04:51:12.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:492.3333333333333 - branchDistance:0.0 - coverage:195.33333333333331 - ex: 0 - tex: 10
[MASTER] 04:51:12.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 492.3333333333333, number of tests: 6, total length: 124
[MASTER] 04:51:12.800 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:453.3333333333333 - branchDistance:0.0 - coverage:156.33333333333331 - ex: 0 - tex: 10
[MASTER] 04:51:12.800 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 453.3333333333333, number of tests: 9, total length: 184
[MASTER] 04:51:13.199 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:450.9939577039275 - branchDistance:0.0 - coverage:153.9939577039275 - ex: 0 - tex: 10
[MASTER] 04:51:13.199 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 450.9939577039275, number of tests: 7, total length: 179
* Computation finished
