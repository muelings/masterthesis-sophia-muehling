/*
 * This file was automatically generated by EvoSuite
 * Tue Jun 30 02:50:39 GMT 2020
 */

package org.joda.time;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.joda.time.Days;
import org.joda.time.Period;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Period_ESTest extends Period_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Period period0 = new Period((-2009), 931, 2970, 1274, (-2009), 510, (-969), (-204));
      Period period1 = period0.normalizedStandard();
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Cannot convert period to duration as years is not precise in the period P-1931Y-5M3140WT15H13M50.796S
      // Modified Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Cannot convert period to duration as years is not precise in the period P-1932Y7M3140WT15H13M50.796S
      // Undeclared exception!
      try { 
        Days.standardDaysIn(period1);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Cannot convert period to duration as years is not precise in the period P-1931Y-5M3140WT15H13M50.796S
         //
         verifyException("org.joda.time.base.BaseSingleFieldPeriod", e);
         assertTrue(e.getMessage().equals("Cannot convert period to duration as years is not precise in the period P-1931Y-5M3140WT15H13M50.796S"));   
      }
  }
}
