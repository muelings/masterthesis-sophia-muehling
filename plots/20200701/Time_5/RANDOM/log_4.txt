* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Period
* Starting client
* Connecting to master process on port 4915
* Analyzing classpath: 
  - ../defects4j_compiled/Time_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Period
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 131
[MASTER] 04:48:45.838 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:48:47.988 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:48:49.213 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:48:49.231 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 04:48:49.503 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:48:49.522 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
[MASTER] 04:48:49.523 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 57 | Tests with assertion: 1
* Generated 1 tests with total length 16
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          3 / 300         
[MASTER] 04:48:50.254 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:48:50.268 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 12 
[MASTER] 04:48:50.280 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 04:48:50.280 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 04:48:50.281 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: normalizedStandard:UnsupportedOperationException at 11
[MASTER] 04:48:50.281 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 04:48:50.436 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
[MASTER] 04:48:50.439 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 11%
* Total number of goals: 131
* Number of covered goals: 14
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Period_ESTest' to evosuite-tests
* Done!

* Computation finished
