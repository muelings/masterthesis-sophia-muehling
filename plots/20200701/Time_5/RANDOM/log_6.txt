* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Period
* Starting client
* Connecting to master process on port 4653
* Analyzing classpath: 
  - ../defects4j_compiled/Time_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Period
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:49:37.335 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 131
[MASTER] 04:49:40.395 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:49:41.607 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:49:41.615 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 04:49:41.840 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:49:41.846 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 04:49:41.846 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 124 | Tests with assertion: 1
* Generated 1 tests with total length 8
* GA-Budget:
	- MaxTime :                          4 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:49:42.515 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:49:42.524 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 6 
[MASTER] 04:49:42.529 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 04:49:42.529 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 04:49:42.530 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: normalizedStandard:UnsupportedOperationException at 5
[MASTER] 04:49:42.530 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 04:49:42.565 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:49:42.568 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 11%
* Total number of goals: 131
* Number of covered goals: 15
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Period_ESTest' to evosuite-tests
* Done!

* Computation finished
