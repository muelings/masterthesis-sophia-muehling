* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVParser
* Starting client
* Connecting to master process on port 12054
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVParser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 15:52:16.617 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:52:17.009 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 15:52:18.202 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:52:18.248 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 15:52:18.561 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVParser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 15:52:18.601 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 15:52:18.602 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 1 | Tests with assertion: 1
* Generated 1 tests with total length 14
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
[MASTER] 15:52:19.182 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:52:19.216 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 15:52:19.259 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 15:52:19.260 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IllegalCharsetNameException,, forName:IllegalCharsetNameException]
[MASTER] 15:52:19.260 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: forName:IllegalCharsetNameException at 12
[MASTER] 15:52:19.260 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [IllegalCharsetNameException,, forName:IllegalCharsetNameException, getHeaderMap:NullPointerException]
[MASTER] 15:52:19.260 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getHeaderMap:NullPointerException at 9
[MASTER] 15:52:19.260 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [IllegalCharsetNameException,, forName:IllegalCharsetNameException, getHeaderMap:NullPointerException]
[MASTER] 15:52:19.708 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
[MASTER] 15:52:19.712 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 70
* Number of covered goals: 4
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CSVParser_ESTest' to evosuite-tests
* Done!

* Computation finished
