* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 11181
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 03:47:45.400 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 03:47:45.404 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 53
* Using seed 1593136062503
* Starting evolution
[MASTER] 03:47:46.751 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:215.0 - branchDistance:0.0 - coverage:105.0 - ex: 0 - tex: 0
[MASTER] 03:47:46.751 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 215.0, number of tests: 1, total length: 5
[MASTER] 03:47:47.127 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:206.0 - branchDistance:0.0 - coverage:96.0 - ex: 0 - tex: 10
[MASTER] 03:47:47.127 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 206.0, number of tests: 5, total length: 149
[MASTER] 03:47:47.166 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:182.0 - branchDistance:0.0 - coverage:72.0 - ex: 0 - tex: 6
[MASTER] 03:47:47.166 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 182.0, number of tests: 3, total length: 45
[MASTER] 03:47:48.062 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:147.6 - branchDistance:0.0 - coverage:103.0 - ex: 0 - tex: 14
[MASTER] 03:47:48.062 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 147.6, number of tests: 7, total length: 163
[MASTER] 03:47:49.340 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:120.6 - branchDistance:0.0 - coverage:76.0 - ex: 0 - tex: 12
[MASTER] 03:47:49.340 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.6, number of tests: 6, total length: 132
[MASTER] 03:47:49.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:110.6 - branchDistance:0.0 - coverage:66.0 - ex: 0 - tex: 12
[MASTER] 03:47:49.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.6, number of tests: 8, total length: 183
[MASTER] 03:47:50.669 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:110.1 - branchDistance:0.0 - coverage:65.5 - ex: 0 - tex: 12
[MASTER] 03:47:50.669 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.1, number of tests: 7, total length: 161
[MASTER] 03:47:50.856 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:109.6 - branchDistance:0.0 - coverage:65.0 - ex: 0 - tex: 10
[MASTER] 03:47:50.856 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.6, number of tests: 7, total length: 142
[MASTER] 03:47:51.120 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:102.6 - branchDistance:0.0 - coverage:58.0 - ex: 0 - tex: 12
[MASTER] 03:47:51.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.6, number of tests: 8, total length: 161
[MASTER] 03:47:51.273 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:101.6 - branchDistance:0.0 - coverage:57.0 - ex: 0 - tex: 14
[MASTER] 03:47:51.273 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.6, number of tests: 10, total length: 203
[MASTER] 03:47:51.590 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:97.6 - branchDistance:0.0 - coverage:53.0 - ex: 0 - tex: 14
[MASTER] 03:47:51.590 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.6, number of tests: 8, total length: 174
[MASTER] 03:47:52.490 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:96.6 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 16
[MASTER] 03:47:52.490 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.6, number of tests: 10, total length: 206
[MASTER] 03:47:52.514 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:94.6 - branchDistance:0.0 - coverage:50.0 - ex: 0 - tex: 14
[MASTER] 03:47:52.514 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.6, number of tests: 11, total length: 229
[MASTER] 03:47:52.598 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:92.6 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 16
[MASTER] 03:47:52.598 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.6, number of tests: 10, total length: 229
[MASTER] 03:47:53.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:90.6 - branchDistance:0.0 - coverage:46.0 - ex: 0 - tex: 14
[MASTER] 03:47:53.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.6, number of tests: 11, total length: 223
[MASTER] 03:47:53.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:81.6 - branchDistance:0.0 - coverage:37.0 - ex: 0 - tex: 16
[MASTER] 03:47:53.895 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.6, number of tests: 10, total length: 229
[MASTER] 03:47:54.684 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:75.6 - branchDistance:0.0 - coverage:31.0 - ex: 0 - tex: 16
[MASTER] 03:47:54.685 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.6, number of tests: 11, total length: 258
[MASTER] 03:47:55.478 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:74.6 - branchDistance:0.0 - coverage:30.0 - ex: 0 - tex: 20
[MASTER] 03:47:55.478 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.6, number of tests: 12, total length: 258
[MASTER] 03:47:56.005 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:73.6 - branchDistance:0.0 - coverage:29.0 - ex: 0 - tex: 20
[MASTER] 03:47:56.006 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.6, number of tests: 12, total length: 260
[MASTER] 03:47:56.120 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:69.6 - branchDistance:0.0 - coverage:25.0 - ex: 0 - tex: 22
[MASTER] 03:47:56.121 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.6, number of tests: 13, total length: 291
[MASTER] 03:47:56.197 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:67.1 - branchDistance:0.0 - coverage:22.5 - ex: 0 - tex: 18
[MASTER] 03:47:56.197 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.1, number of tests: 12, total length: 262
[MASTER] 03:47:56.483 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:65.1 - branchDistance:0.0 - coverage:20.5 - ex: 0 - tex: 18
[MASTER] 03:47:56.484 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.1, number of tests: 12, total length: 287
[MASTER] 03:47:57.123 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:63.1 - branchDistance:0.0 - coverage:18.5 - ex: 0 - tex: 18
[MASTER] 03:47:57.124 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.1, number of tests: 12, total length: 272
[MASTER] 03:47:58.474 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:62.1 - branchDistance:0.0 - coverage:17.5 - ex: 0 - tex: 18
[MASTER] 03:47:58.474 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.1, number of tests: 13, total length: 295
[MASTER] 03:47:59.082 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:61.1 - branchDistance:0.0 - coverage:16.5 - ex: 0 - tex: 16
[MASTER] 03:47:59.082 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.1, number of tests: 12, total length: 263
[MASTER] 03:48:00.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:60.1 - branchDistance:0.0 - coverage:15.5 - ex: 0 - tex: 18
[MASTER] 03:48:00.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.1, number of tests: 12, total length: 277
[MASTER] 03:48:00.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:59.1 - branchDistance:0.0 - coverage:14.5 - ex: 0 - tex: 18
[MASTER] 03:48:00.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.1, number of tests: 12, total length: 253
[MASTER] 03:48:01.120 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:58.1 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 16
[MASTER] 03:48:01.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.1, number of tests: 12, total length: 261
[MASTER] 03:48:01.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:57.1 - branchDistance:0.0 - coverage:12.5 - ex: 0 - tex: 18
[MASTER] 03:48:01.334 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.1, number of tests: 12, total length: 257
[MASTER] 03:48:02.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:56.1 - branchDistance:0.0 - coverage:11.5 - ex: 0 - tex: 18
[MASTER] 03:48:02.810 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.1, number of tests: 12, total length: 250
[MASTER] 03:48:05.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.5 - fitness:55.6 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 18
[MASTER] 03:48:05.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.6, number of tests: 12, total length: 246
[MASTER] 03:48:21.642 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:48.33333333333333 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 16
[MASTER] 03:48:21.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.33333333333333, number of tests: 11, total length: 211
[MASTER] 03:48:38.589 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:47.33333333333333 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 16
[MASTER] 03:48:38.589 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.33333333333333, number of tests: 10, total length: 192
[MASTER] 03:48:41.014 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:46.33333333333333 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 16
[MASTER] 03:48:41.014 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.33333333333333, number of tests: 10, total length: 196
[MASTER] 03:48:49.204 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:45.83333333333333 - branchDistance:0.0 - coverage:8.5 - ex: 0 - tex: 18
[MASTER] 03:48:49.204 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.83333333333333, number of tests: 11, total length: 220
[MASTER] 03:48:52.672 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:45.33333333333333 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 20
[MASTER] 03:48:52.672 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.33333333333333, number of tests: 12, total length: 240
[MASTER] 03:49:11.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:44.33333333333333 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 18
[MASTER] 03:49:11.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 44.33333333333333, number of tests: 12, total length: 206
[MASTER] 03:49:33.382 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:43.33333333333333 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 20
[MASTER] 03:49:33.382 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 43.33333333333333, number of tests: 13, total length: 216
[MASTER] 03:49:41.063 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:42.33333333333333 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 20
[MASTER] 03:49:41.063 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.33333333333333, number of tests: 13, total length: 204
[MASTER] 03:49:57.464 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:41.83333333333333 - branchDistance:0.0 - coverage:4.5 - ex: 0 - tex: 20
[MASTER] 03:49:57.464 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.83333333333333, number of tests: 13, total length: 190
[MASTER] 03:50:08.634 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:41.33333333333333 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 22
[MASTER] 03:50:08.634 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.33333333333333, number of tests: 14, total length: 206
[MASTER] 03:51:30.227 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:40.83333333333333 - branchDistance:0.0 - coverage:3.5 - ex: 0 - tex: 22
[MASTER] 03:51:30.227 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.83333333333333, number of tests: 14, total length: 184
[MASTER] 03:51:33.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:40.33333333333333 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 24
[MASTER] 03:51:33.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.33333333333333, number of tests: 15, total length: 195
[MASTER] 03:52:46.427 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1233 generations, 909538 statements, best individuals have fitness: 40.33333333333333
[MASTER] 03:52:46.431 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 92%
* Total number of goals: 53
* Number of covered goals: 49
* Generated 15 tests with total length 184
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     40 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 03:52:46.860 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:52:46.892 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 155 
[MASTER] 03:52:46.999 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 03:52:46.999 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 03:52:47.000 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 03:52:47.001 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 03:52:47.001 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 03:52:47.001 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 03:52:47.001 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 03:52:47.001 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 03:52:47.001 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 03:52:47.001 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 03:52:47.002 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
* Going to analyze the coverage criteria
[MASTER] 03:52:47.002 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
* Coverage analysis for criterion REGRESSION
[MASTER] 03:52:47.002 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 03:52:47.002 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 03:52:47.002 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 03:52:47.002 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 03:52:47.003 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 03:52:47.004 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 53
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
