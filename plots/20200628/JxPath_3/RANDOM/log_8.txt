* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Starting client
* Connecting to master process on port 4374
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 53
[MASTER] 03:44:48.231 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:47:35.456 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 03:47:36.664 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 03:47:36.879 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 03:47:36.879 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 28312 | Tests with assertion: 1
* Generated 1 tests with total length 19
* GA-Budget:
	- MaxTime :                        168 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 03:47:37.560 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:47:37.560 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 51 seconds more than allowed.
[MASTER] 03:47:37.569 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 16 
[MASTER] 03:47:37.608 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 03:47:37.609 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [JXPathAbstractFactoryException:createBadFactoryException-234,, createPath:JXPathAbstractFactoryException]
[MASTER] 03:47:37.609 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createPath:JXPathAbstractFactoryException at 15
[MASTER] 03:47:37.609 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [JXPathAbstractFactoryException:createBadFactoryException-234,, createPath:JXPathAbstractFactoryException]
[MASTER] 03:47:38.584 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
* Going to analyze the coverage criteria
[MASTER] 03:47:38.587 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 13%
* Total number of goals: 53
* Number of covered goals: 7
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NullPropertyPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
