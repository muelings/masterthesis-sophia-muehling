* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.util.TextBuffer
* Starting client
* Connecting to master process on port 12094
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.util.TextBuffer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 22:15:21.918 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 141
[MASTER] 22:15:59.653 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:15:59.653 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:15:59.653 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:15:59.653 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:15:59.674 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 22:15:59.674 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 22:16:00.750 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:16:00.760 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:16:00.760 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:16:00.760 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:16:00.760 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(327680, var1.size());  // (Inspector) Original Value: 327680 | Regression Value: 262145
[MASTER] 22:16:00.762 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 22:16:00.762 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 22:17:00.272 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:17:00.561 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:17:05.837 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:17:06.144 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:17:06.148 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 22:17:06.339 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_7_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 0 assertions.
[MASTER] 22:17:06.376 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:17:06.593 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/util/TextBuffer_9_tmp__ESTest.class' should be in target project, but could not be found!
*** Random test generation finished.
[MASTER] 22:20:22.923 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 83177 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 22:20:22.957 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:20:22.958 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 183 seconds more than allowed.
[MASTER] 22:20:22.962 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 22:20:22.962 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 22:20:22.964 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 141
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TextBuffer_ESTest' to evosuite-tests
* Done!

* Computation finished
