* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 4087
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 05:18:08.329 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 38
[MASTER] 05:18:08.928 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(107.62116788321167, var3.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 107.62116788321167 | Regression Value: 107.62116788321168
[MASTER] 05:18:08.929 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(107.62116788321167, var3.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 107.62116788321167 | Regression Value: 107.62116788321168
[MASTER] 05:18:08.930 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 05:18:08.930 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 05:18:09.986 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/distribution/HypergeometricDistribution_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 2 assertions.
[MASTER] 05:18:09.995 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(107.62116788321167, var3.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 107.62116788321167 | Regression Value: 107.62116788321168
[MASTER] 05:18:09.995 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(107.62116788321167, var3.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 107.62116788321167 | Regression Value: 107.62116788321168
[MASTER] 05:18:09.996 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 05:18:09.996 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 05:18:10.307 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/distribution/HypergeometricDistribution_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:18:10.320 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(107.62116788321167, var3.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 107.62116788321167 | Regression Value: 107.62116788321168
Keeping 2 assertions.
[MASTER] 05:18:10.320 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(107.62116788321167, var3.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 107.62116788321167 | Regression Value: 107.62116788321168
[MASTER] 05:18:10.320 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 05:18:10.320 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 05:18:10.321 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 19 | Tests with assertion: 1
* Generated 1 tests with total length 5
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 05:18:10.841 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:18:10.858 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 05:18:10.868 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[getNumericalMean]}
[MASTER] 05:18:10.875 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:18:10.875 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 05:18:10.879 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
