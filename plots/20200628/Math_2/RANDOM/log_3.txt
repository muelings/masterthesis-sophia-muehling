* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 14987
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 38
[MASTER] 05:28:47.984 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:28:48.803 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(21.02883588465646, var22.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 21.02883588465646 | Regression Value: 21.028835884656463
[MASTER] 05:28:48.803 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(21.02883588465646, var22.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 21.02883588465646 | Regression Value: 21.028835884656463
[MASTER] 05:28:48.811 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 05:28:48.811 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 05:28:49.954 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/distribution/HypergeometricDistribution_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:28:49.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(21.02883588465646, var22.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 21.02883588465646 | Regression Value: 21.028835884656463
[MASTER] 05:28:49.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(21.02883588465646, var22.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 21.02883588465646 | Regression Value: 21.028835884656463
[MASTER] 05:28:49.975 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 05:28:49.975 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 05:28:50.357 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/distribution/HypergeometricDistribution_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:28:50.373 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(21.02883588465646, var22.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 21.02883588465646 | Regression Value: 21.028835884656463
[MASTER] 05:28:50.373 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(21.02883588465646, var22.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 21.02883588465646 | Regression Value: 21.028835884656463
[MASTER] 05:28:50.375 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Keeping 2 assertions.
[MASTER] 05:28:50.375 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 05:28:50.376 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 24 | Tests with assertion: 1
* Generated 1 tests with total length 24
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
	- RMIStoppingCondition
[MASTER] 05:28:50.992 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:28:51.016 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 05:28:51.032 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[getNumericalMean]}
[MASTER] 05:28:51.039 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:28:51.039 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 05:28:51.041 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
