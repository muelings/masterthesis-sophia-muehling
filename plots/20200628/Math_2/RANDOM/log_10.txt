* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 17212
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 38
[MASTER] 06:06:05.174 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:06:07.146 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1.9999999999999998, var2.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 1.9999999999999998 | Regression Value: 2.0
[MASTER] 06:06:07.146 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1.9999999999999998, var2.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 1.9999999999999998 | Regression Value: 2.0
[MASTER] 06:06:07.148 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:06:07.148 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 06:06:08.187 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/distribution/HypergeometricDistribution_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:06:08.194 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1.9999999999999998, var2.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 1.9999999999999998 | Regression Value: 2.0
[MASTER] 06:06:08.194 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1.9999999999999998, var2.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 1.9999999999999998 | Regression Value: 2.0
[MASTER] 06:06:08.195 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:06:08.195 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 06:06:08.555 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/distribution/HypergeometricDistribution_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:06:08.563 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1.9999999999999998, var2.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 1.9999999999999998 | Regression Value: 2.0
[MASTER] 06:06:08.563 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(1.9999999999999998, var2.getNumericalMean(), 0.01D);  // (Inspector) Original Value: 1.9999999999999998 | Regression Value: 2.0
[MASTER] 06:06:08.564 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:06:08.564 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
*** Random test generation finished.
[MASTER] 06:06:08.565 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 134 | Tests with assertion: 1
* Generated 1 tests with total length 4
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          3 / 300         
[MASTER] 06:06:09.090 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:06:09.102 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 06:06:09.110 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[getNumericalMean]}
[MASTER] 06:06:09.114 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:06:09.114 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 06:06:09.117 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
