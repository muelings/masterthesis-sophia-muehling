/*
 * This file was automatically generated by EvoSuite
 * Thu Jun 25 03:23:32 GMT 2020
 */

package org.apache.commons.math3.distribution;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class HypergeometricDistribution_ESTest extends HypergeometricDistribution_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      HypergeometricDistribution hypergeometricDistribution0 = new HypergeometricDistribution(387, 36, 36);
      double double0 = hypergeometricDistribution0.getNumericalMean();
      assertEquals(3.3488372093023253, double0, 0.01); // (Primitive) Original Value: 3.3488372093023253 | Regression Value: 3.3488372093023258
  }
}
