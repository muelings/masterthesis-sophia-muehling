* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 2799
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 38
* Using seed 1593056690048
* Starting evolution
[MASTER] 05:44:54.305 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:44:54.311 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:44:55.476 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:126.0 - branchDistance:0.0 - coverage:37.0 - ex: 0 - tex: 14
[MASTER] 05:44:55.476 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.0, number of tests: 8, total length: 203
[MASTER] 05:44:55.684 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:125.0 - branchDistance:0.0 - coverage:36.0 - ex: 0 - tex: 16
[MASTER] 05:44:55.684 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.0, number of tests: 8, total length: 209
[MASTER] 05:44:56.038 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:117.98803985654189 - branchDistance:0.0 - coverage:28.988039856541896 - ex: 0 - tex: 6
[MASTER] 05:44:56.038 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.98803985654189, number of tests: 4, total length: 83
[MASTER] 05:44:56.381 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:111.99196787148594 - branchDistance:0.0 - coverage:22.991967871485944 - ex: 0 - tex: 6
[MASTER] 05:44:56.381 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.99196787148594, number of tests: 4, total length: 78
[MASTER] 05:44:56.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:104.98870056497175 - branchDistance:0.0 - coverage:15.98870056497175 - ex: 0 - tex: 12
[MASTER] 05:44:56.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.98870056497175, number of tests: 9, total length: 174
[MASTER] 05:44:58.666 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:104.9875 - branchDistance:0.0 - coverage:15.9875 - ex: 0 - tex: 10
[MASTER] 05:44:58.666 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.9875, number of tests: 6, total length: 109
[MASTER] 05:44:59.565 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:102.6 - branchDistance:0.0 - coverage:13.6 - ex: 0 - tex: 14
[MASTER] 05:44:59.565 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.6, number of tests: 10, total length: 196
[MASTER] 05:45:00.071 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:101.93333333333334 - branchDistance:0.0 - coverage:12.933333333333334 - ex: 0 - tex: 14
[MASTER] 05:45:00.071 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.93333333333334, number of tests: 11, total length: 209
[MASTER] 05:45:01.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:96.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 14
[MASTER] 05:45:01.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.0, number of tests: 11, total length: 200
[MASTER] 05:45:02.156 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.99029891112627 - branchDistance:0.0 - coverage:6.990298911126267 - ex: 0 - tex: 4
[MASTER] 05:45:02.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.99029891112627, number of tests: 7, total length: 185
[MASTER] 05:45:02.431 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 12
[MASTER] 05:45:02.431 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.0, number of tests: 10, total length: 193
[MASTER] 05:45:02.540 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.93333333333334 - branchDistance:0.0 - coverage:4.933333333333334 - ex: 0 - tex: 14
[MASTER] 05:45:02.540 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.93333333333334, number of tests: 11, total length: 225
[MASTER] 05:45:03.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:93.6 - branchDistance:0.0 - coverage:4.6 - ex: 0 - tex: 12
[MASTER] 05:45:03.720 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.6, number of tests: 10, total length: 181
[MASTER] 05:45:03.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 8
[MASTER] 05:45:03.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0, number of tests: 8, total length: 172
[MASTER] 05:45:09.541 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:90.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 12
[MASTER] 05:45:09.541 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.0, number of tests: 9, total length: 192
[MASTER] 05:45:10.110 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:48.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 6
[MASTER] 05:45:10.110 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.0, number of tests: 10, total length: 191
[MASTER] 05:45:11.860 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:47.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 8
[MASTER] 05:45:11.860 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.0, number of tests: 10, total length: 191
[MASTER] 05:45:12.573 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:46.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 10
[MASTER] 05:45:12.573 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.0, number of tests: 11, total length: 234
[MASTER] 05:45:13.281 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:24.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 05:45:13.281 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.0, number of tests: 10, total length: 202
[MASTER] 05:45:15.814 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:23.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:45:15.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.0, number of tests: 11, total length: 211
[MASTER] 05:45:50.853 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:19.6 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 05:45:50.853 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 19.6, number of tests: 10, total length: 154
[MASTER] 05:45:53.190 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:18.6 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:45:53.190 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.6, number of tests: 10, total length: 158
[MASTER] 05:45:55.985 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:15.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:45:55.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.666666666666666, number of tests: 10, total length: 161
[MASTER] 05:46:07.651 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:14.904761904761905 - branchDistance:0.0 - coverage:1.3333333333333333 - ex: 0 - tex: 8
[MASTER] 05:46:07.651 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.904761904761905, number of tests: 8, total length: 147
[MASTER] 05:46:08.828 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:13.571428571428571 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:46:08.828 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.571428571428571, number of tests: 9, total length: 155
[MASTER] 05:46:28.621 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:12.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:46:28.621 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.0, number of tests: 8, total length: 118
[MASTER] 05:46:34.815 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:10.777777777777777 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:46:34.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.777777777777777, number of tests: 8, total length: 119
[MASTER] 05:46:53.192 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.0 - fitness:9.8 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:46:53.192 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.8, number of tests: 8, total length: 114
[MASTER] 05:47:45.120 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.0 - fitness:9.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:47:45.120 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.0, number of tests: 8, total length: 97
[MASTER] 05:47:52.566 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.0 - fitness:8.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:47:52.566 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333333333333332, number of tests: 8, total length: 92
[MASTER] 05:48:31.414 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.0 - fitness:7.76923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:48:31.414 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.76923076923077, number of tests: 8, total length: 77
[MASTER] 05:48:39.886 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.0 - fitness:7.285714285714286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:48:39.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.285714285714286, number of tests: 8, total length: 76
[MASTER] 05:48:42.544 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.0 - fitness:6.866666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:48:42.544 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.866666666666666, number of tests: 8, total length: 78
[MASTER] 05:48:52.402 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.0 - fitness:6.5 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:48:52.402 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5, number of tests: 8, total length: 76
[MASTER] 05:48:55.554 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.0 - fitness:6.176470588235294 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:48:55.554 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.176470588235294, number of tests: 8, total length: 77
[MASTER] 05:49:01.294 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.0 - fitness:5.888888888888888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:49:01.294 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888888888888888, number of tests: 8, total length: 76
[MASTER] 05:49:17.900 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.0 - fitness:5.631578947368421 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:49:17.900 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.631578947368421, number of tests: 8, total length: 71
[MASTER] 05:49:21.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.0 - fitness:5.4 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:49:21.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.4, number of tests: 8, total length: 74
[MASTER] 05:49:27.506 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.0 - fitness:5.19047619047619 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:49:27.506 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.19047619047619, number of tests: 8, total length: 74
[MASTER] 05:49:55.351 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 545 generations, 336480 statements, best individuals have fitness: 5.19047619047619
[MASTER] 05:49:55.354 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 38
* Number of covered goals: 38
* Generated 8 tests with total length 67
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                      5 / 0           
	- MaxTime :                        302 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 05:49:56.368 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:49:56.407 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 47 
[MASTER] 05:49:56.489 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 05:49:56.490 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 05:49:56.491 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 05:49:56.493 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 05:49:56.494 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 05:49:56.495 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:49:56.497 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 05:49:56.497 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 05:49:56.498 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 05:49:56.498 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 05:49:56.498 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 05:49:56.499 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 05:49:56.500 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 05:49:56.500 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 05:49:56.501 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
