* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 20017
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:23:39.200 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:23:39.208 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 38
* Using seed 1593055414868
* Starting evolution
[MASTER] 05:23:40.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:171.0 - branchDistance:0.0 - coverage:82.0 - ex: 0 - tex: 2
[MASTER] 05:23:40.130 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 171.0, number of tests: 1, total length: 24
[MASTER] 05:23:40.618 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:129.984126984127 - branchDistance:0.0 - coverage:40.98412698412699 - ex: 0 - tex: 16
[MASTER] 05:23:40.618 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.984126984127, number of tests: 9, total length: 205
[MASTER] 05:23:41.002 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:110.99672667757774 - branchDistance:0.0 - coverage:21.99672667757774 - ex: 0 - tex: 14
[MASTER] 05:23:41.002 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.99672667757774, number of tests: 9, total length: 175
[MASTER] 05:23:41.894 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:110.33333333333333 - branchDistance:0.0 - coverage:21.333333333333332 - ex: 0 - tex: 16
[MASTER] 05:23:41.894 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.33333333333333, number of tests: 9, total length: 192
[MASTER] 05:23:42.071 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:48.96116504854369 - branchDistance:0.0 - coverage:25.96116504854369 - ex: 0 - tex: 4
[MASTER] 05:23:42.071 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.96116504854369, number of tests: 4, total length: 61
[MASTER] 05:23:46.559 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:42.99665551839465 - branchDistance:0.0 - coverage:19.996655518394647 - ex: 0 - tex: 8
[MASTER] 05:23:46.559 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.99665551839465, number of tests: 6, total length: 105
[MASTER] 05:23:46.764 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:41.96116504854369 - branchDistance:0.0 - coverage:18.96116504854369 - ex: 0 - tex: 2
[MASTER] 05:23:46.764 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 41.96116504854369, number of tests: 4, total length: 63
[MASTER] 05:23:48.011 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:39.33333333333333 - branchDistance:0.0 - coverage:16.333333333333332 - ex: 0 - tex: 8
[MASTER] 05:23:48.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.33333333333333, number of tests: 6, total length: 124
[MASTER] 05:23:48.025 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:37.166666666666664 - branchDistance:0.0 - coverage:14.166666666666666 - ex: 0 - tex: 4
[MASTER] 05:23:48.025 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 37.166666666666664, number of tests: 5, total length: 70
[MASTER] 05:23:48.972 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:33.33333333333333 - branchDistance:0.0 - coverage:10.333333333333332 - ex: 0 - tex: 4
[MASTER] 05:23:48.972 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.33333333333333, number of tests: 5, total length: 102
[MASTER] 05:23:49.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:32.33333333333333 - branchDistance:0.0 - coverage:9.333333333333332 - ex: 0 - tex: 6
[MASTER] 05:23:49.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.33333333333333, number of tests: 6, total length: 105
[MASTER] 05:23:49.892 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:31.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:23:49.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.0, number of tests: 6, total length: 112
[MASTER] 05:23:52.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:30.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 05:23:52.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.0, number of tests: 6, total length: 99
[MASTER] 05:23:52.674 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:29.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 6
[MASTER] 05:23:52.674 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.0, number of tests: 6, total length: 111
[MASTER] 05:23:55.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:28.996969696969696 - branchDistance:0.0 - coverage:5.996969696969697 - ex: 0 - tex: 2
[MASTER] 05:23:55.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.996969696969696, number of tests: 6, total length: 123
[MASTER] 05:23:55.391 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:28.0 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 6
[MASTER] 05:23:55.391 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.0, number of tests: 7, total length: 116
[MASTER] 05:23:55.829 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:26.6 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:23:55.829 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.6, number of tests: 6, total length: 115
[MASTER] 05:23:56.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:24.6 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 4
[MASTER] 05:23:56.197 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.6, number of tests: 6, total length: 105
[MASTER] 05:23:56.647 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:23.666666666666664 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 05:23:56.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.666666666666664, number of tests: 6, total length: 115
[MASTER] 05:23:57.706 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:23.6 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 8
[MASTER] 05:23:57.706 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.6, number of tests: 7, total length: 133
[MASTER] 05:23:57.712 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:22.666666666666664 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 8
[MASTER] 05:23:57.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.666666666666664, number of tests: 7, total length: 146
[MASTER] 05:23:57.754 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:21.57142857142857 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 05:23:57.754 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.57142857142857, number of tests: 7, total length: 143
[MASTER] 05:23:58.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:18.57142857142857 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 6
[MASTER] 05:23:58.720 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.57142857142857, number of tests: 7, total length: 112
[MASTER] 05:24:00.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:17.57142857142857 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 8
[MASTER] 05:24:00.514 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.57142857142857, number of tests: 8, total length: 134
[MASTER] 05:24:05.200 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:16.57142857142857 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 10
[MASTER] 05:24:05.200 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.57142857142857, number of tests: 9, total length: 134
[MASTER] 05:24:07.802 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:16.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 10
[MASTER] 05:24:07.802 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.0, number of tests: 9, total length: 141
[MASTER] 05:24:09.312 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:15.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 10
[MASTER] 05:24:09.312 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.0, number of tests: 9, total length: 134
[MASTER] 05:24:10.254 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:14.777777777777777 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 8
[MASTER] 05:24:10.255 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.777777777777777, number of tests: 9, total length: 125
[MASTER] 05:24:10.835 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:13.777777777777777 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 12
[MASTER] 05:24:10.835 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.777777777777777, number of tests: 10, total length: 171
[MASTER] 05:24:13.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:10.777777777777777 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:24:13.509 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.777777777777777, number of tests: 10, total length: 160
[MASTER] 05:24:20.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.0 - fitness:9.8 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:24:20.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.8, number of tests: 10, total length: 145
[MASTER] 05:24:32.718 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.0 - fitness:9.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:24:32.718 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.0, number of tests: 10, total length: 125
[MASTER] 05:24:42.805 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.0 - fitness:8.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:24:42.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333333333333332, number of tests: 10, total length: 118
[MASTER] 05:24:52.322 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.0 - fitness:7.76923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:24:52.322 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.76923076923077, number of tests: 10, total length: 111
[MASTER] 05:24:54.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.0 - fitness:7.285714285714286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:24:54.333 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.285714285714286, number of tests: 10, total length: 111
[MASTER] 05:25:01.888 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.0 - fitness:6.866666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:25:01.888 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.866666666666666, number of tests: 10, total length: 107
[MASTER] 05:25:06.968 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.0 - fitness:6.5 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 05:25:06.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5, number of tests: 12, total length: 139
[MASTER] 05:25:12.273 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.0 - fitness:6.176470588235294 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:25:12.273 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.176470588235294, number of tests: 10, total length: 109
[MASTER] 05:25:17.189 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.0 - fitness:5.888888888888888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:25:17.190 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888888888888888, number of tests: 10, total length: 113
[MASTER] 05:25:37.891 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.0 - fitness:5.631578947368421 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:25:37.891 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.631578947368421, number of tests: 10, total length: 96
[MASTER] 05:25:40.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.0 - fitness:5.4 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:25:40.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.4, number of tests: 10, total length: 94
[MASTER] 05:25:43.160 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.0 - fitness:5.19047619047619 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:25:43.160 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.19047619047619, number of tests: 10, total length: 95
[MASTER] 05:25:52.454 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.0 - fitness:5.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:25:52.454 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.0, number of tests: 10, total length: 88
[MASTER] 05:26:10.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.0 - fitness:4.826086956521739 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:26:10.729 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.826086956521739, number of tests: 10, total length: 80
[MASTER] 05:26:16.360 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.0 - fitness:4.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:26:16.360 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.666666666666666, number of tests: 10, total length: 80
[MASTER] 05:26:41.308 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.0 - fitness:4.52 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:26:41.308 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.52, number of tests: 8, total length: 71
[MASTER] 05:26:43.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.0 - fitness:4.384615384615385 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:26:43.297 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.384615384615385, number of tests: 8, total length: 70
[MASTER] 05:26:45.960 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.0 - fitness:4.2592592592592595 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:26:45.960 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.2592592592592595, number of tests: 9, total length: 97
[MASTER] 05:26:58.932 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.0 - fitness:4.142857142857142 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:26:58.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.142857142857142, number of tests: 8, total length: 70
[MASTER] 05:27:25.184 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.0 - fitness:4.0344827586206895 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:27:25.184 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0344827586206895, number of tests: 8, total length: 73
[MASTER] 05:28:40.245 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1113 generations, 561618 statements, best individuals have fitness: 4.0344827586206895
[MASTER] 05:28:40.248 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 38
* Number of covered goals: 38
* Generated 7 tests with total length 67
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                      4 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 05:28:41.161 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:28:41.189 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 50 
[MASTER] 05:28:41.255 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 05:28:41.256 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 05:28:41.257 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 05:28:41.258 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 05:28:41.259 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 05:28:41.259 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:28:41.260 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 05:28:41.261 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 05:28:41.263 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 05:28:41.264 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 05:28:41.264 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 05:28:41.265 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 05:28:41.265 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 05:28:41.266 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
