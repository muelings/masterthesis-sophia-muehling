* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.distribution.HypergeometricDistribution
* Starting client
* Connecting to master process on port 20652
* Analyzing classpath: 
  - ../defects4j_compiled/Math_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.distribution.HypergeometricDistribution
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 05:34:15.091 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 05:34:15.102 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 38
* Using seed 1593056051046
* Starting evolution
[MASTER] 05:34:16.722 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:109.96405020247636 - branchDistance:0.0 - coverage:20.964050202476365 - ex: 0 - tex: 14
[MASTER] 05:34:16.722 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.96405020247636, number of tests: 9, total length: 188
[MASTER] 05:34:17.262 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:106.99283154121864 - branchDistance:0.0 - coverage:17.99283154121864 - ex: 0 - tex: 14
[MASTER] 05:34:17.262 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.99283154121864, number of tests: 9, total length: 219
[MASTER] 05:34:18.734 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:105.99554565701558 - branchDistance:0.0 - coverage:16.99554565701559 - ex: 0 - tex: 16
[MASTER] 05:34:18.734 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.99554565701558, number of tests: 9, total length: 251
[MASTER] 05:34:21.511 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:104.0 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 18
[MASTER] 05:34:21.511 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.0, number of tests: 10, total length: 268
[MASTER] 05:34:21.582 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:103.32280701754385 - branchDistance:0.0 - coverage:14.322807017543859 - ex: 0 - tex: 14
[MASTER] 05:34:21.582 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.32280701754385, number of tests: 8, total length: 157
[MASTER] 05:34:22.352 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:57.33333333333333 - branchDistance:0.0 - coverage:12.333333333333332 - ex: 0 - tex: 14
[MASTER] 05:34:22.352 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.33333333333333, number of tests: 8, total length: 170
[MASTER] 05:34:22.630 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:53.33333333333333 - branchDistance:0.0 - coverage:8.333333333333332 - ex: 0 - tex: 12
[MASTER] 05:34:22.630 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.33333333333333, number of tests: 8, total length: 205
[MASTER] 05:34:25.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:53.0 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 18
[MASTER] 05:34:25.302 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.0, number of tests: 11, total length: 266
[MASTER] 05:34:25.908 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:50.333333333333336 - branchDistance:0.0 - coverage:5.333333333333333 - ex: 0 - tex: 12
[MASTER] 05:34:25.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.333333333333336, number of tests: 9, total length: 208
[MASTER] 05:34:28.035 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:49.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 14
[MASTER] 05:34:28.035 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.0, number of tests: 9, total length: 177
[MASTER] 05:34:28.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:48.666666666666664 - branchDistance:0.0 - coverage:3.666666666666666 - ex: 0 - tex: 12
[MASTER] 05:34:28.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.666666666666664, number of tests: 9, total length: 183
[MASTER] 05:34:30.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:47.333333333333336 - branchDistance:0.0 - coverage:2.333333333333333 - ex: 0 - tex: 12
[MASTER] 05:34:30.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.333333333333336, number of tests: 10, total length: 199
[MASTER] 05:34:30.985 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:46.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 14
[MASTER] 05:34:30.985 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.0, number of tests: 10, total length: 207
[MASTER] 05:34:34.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:45.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:34:34.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.0, number of tests: 9, total length: 164
[MASTER] 05:34:37.538 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.0 - fitness:30.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 16
[MASTER] 05:34:37.538 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.333333333333332, number of tests: 11, total length: 218
[MASTER] 05:34:41.702 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.0 - fitness:23.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:34:41.703 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 23.0, number of tests: 9, total length: 156
[MASTER] 05:34:43.283 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:15.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:34:43.283 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 15.666666666666666, number of tests: 9, total length: 159
[MASTER] 05:34:46.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.0 - fitness:13.571428571428571 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:34:46.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.571428571428571, number of tests: 9, total length: 153
[MASTER] 05:34:46.888 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.0 - fitness:12.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:34:46.889 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.0, number of tests: 9, total length: 159
[MASTER] 05:34:50.100 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.0 - fitness:10.777777777777777 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:34:50.100 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 10.777777777777777, number of tests: 9, total length: 141
[MASTER] 05:35:02.391 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.0 - fitness:9.8 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:35:02.391 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.8, number of tests: 8, total length: 110
[MASTER] 05:35:15.667 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.0 - fitness:9.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:35:15.667 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.0, number of tests: 8, total length: 91
[MASTER] 05:35:20.634 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.0 - fitness:8.333333333333332 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:35:20.634 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.333333333333332, number of tests: 8, total length: 93
[MASTER] 05:35:29.513 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.0 - fitness:7.76923076923077 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:35:29.513 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.76923076923077, number of tests: 8, total length: 84
[MASTER] 05:35:36.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.0 - fitness:7.285714285714286 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:35:36.348 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.285714285714286, number of tests: 8, total length: 81
[MASTER] 05:35:48.955 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 14.0 - fitness:6.866666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:35:48.956 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.866666666666666, number of tests: 9, total length: 96
[MASTER] 05:35:49.538 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.0 - fitness:6.5 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:35:49.538 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.5, number of tests: 9, total length: 101
[MASTER] 05:35:53.803 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.0 - fitness:6.176470588235294 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:35:53.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.176470588235294, number of tests: 9, total length: 101
[MASTER] 05:35:55.424 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.0 - fitness:5.888888888888888 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:35:55.424 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.888888888888888, number of tests: 9, total length: 102
[MASTER] 05:36:00.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.0 - fitness:5.631578947368421 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:36:00.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.631578947368421, number of tests: 9, total length: 88
[MASTER] 05:36:06.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.0 - fitness:5.4 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:36:06.615 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.4, number of tests: 9, total length: 125
[MASTER] 05:36:08.545 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.0 - fitness:5.19047619047619 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:36:08.546 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.19047619047619, number of tests: 9, total length: 125
[MASTER] 05:36:10.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.0 - fitness:5.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:36:10.851 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.0, number of tests: 9, total length: 128
[MASTER] 05:36:44.726 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.0 - fitness:4.826086956521739 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:36:44.726 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.826086956521739, number of tests: 8, total length: 77
[MASTER] 05:36:47.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.0 - fitness:4.666666666666666 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:36:47.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.666666666666666, number of tests: 9, total length: 81
[MASTER] 05:37:00.748 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.0 - fitness:4.52 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:37:00.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.52, number of tests: 8, total length: 74
[MASTER] 05:37:06.552 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.0 - fitness:4.384615384615385 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:37:06.552 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.384615384615385, number of tests: 8, total length: 73
[MASTER] 05:37:37.598 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 26.0 - fitness:4.2592592592592595 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:37:37.598 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.2592592592592595, number of tests: 8, total length: 71
[MASTER] 05:37:42.974 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 27.0 - fitness:4.142857142857142 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:37:42.974 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.142857142857142, number of tests: 8, total length: 72
[MASTER] 05:38:07.306 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 28.0 - fitness:4.0344827586206895 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:38:07.306 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.0344827586206895, number of tests: 8, total length: 69
[MASTER] 05:38:23.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 29.0 - fitness:3.933333333333333 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:38:23.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.933333333333333, number of tests: 8, total length: 70
[MASTER] 05:38:35.770 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 30.0 - fitness:3.838709677419355 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:38:35.770 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.838709677419355, number of tests: 9, total length: 80
[MASTER] 05:38:37.367 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 31.0 - fitness:3.75 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 05:38:37.367 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.75, number of tests: 8, total length: 71
[MASTER] 05:38:49.187 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 32.0 - fitness:3.666666666666667 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 12
[MASTER] 05:38:49.187 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.666666666666667, number of tests: 9, total length: 110
[MASTER] 05:39:16.111 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1234 generations, 637542 statements, best individuals have fitness: 3.666666666666667
[MASTER] 05:39:16.114 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 38
* Number of covered goals: 38
* Generated 8 tests with total length 69
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                      4 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 05:39:17.064 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:39:17.086 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 53 
[MASTER] 05:39:17.145 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 05:39:17.146 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 05:39:17.148 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 05:39:17.151 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 05:39:17.152 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 05:39:17.152 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 05:39:17.153 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 05:39:17.154 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 05:39:17.156 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 05:39:17.156 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 05:39:17.156 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 05:39:17.157 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 05:39:17.158 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 05:39:17.158 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 05:39:17.159 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 38
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'HypergeometricDistribution_ESTest' to evosuite-tests
* Done!

* Computation finished
