* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting client
* Connecting to master process on port 8491
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 02:33:30.888 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 100
[MASTER] 02:33:32.493 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:33:33.617 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:33:33.628 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 02:33:33.948 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:33:33.956 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 02:33:33.956 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 116 | Tests with assertion: 1
* Generated 1 tests with total length 9
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          3 / 300         
[MASTER] 02:33:34.620 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:33:34.633 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 02:33:34.641 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 02:33:34.641 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException]
[MASTER] 02:33:34.641 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragmentRelaxed:StringIndexOutOfBoundsException at 1
[MASTER] 02:33:34.641 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parseBodyFragmentRelaxed:StringIndexOutOfBoundsException]
[MASTER] 02:33:34.711 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
[MASTER] 02:33:34.716 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 38%
* Total number of goals: 100
* Number of covered goals: 38
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
