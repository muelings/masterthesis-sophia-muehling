* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jsoup.parser.Parser
* Starting client
* Connecting to master process on port 17822
* Analyzing classpath: 
  - ../defects4j_compiled/Jsoup_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.jsoup.parser.Parser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 100
[MASTER] 02:12:00.698 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:12:02.657 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 02:12:03.765 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:12:03.777 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 02:12:04.090 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jsoup/parser/Parser_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:12:04.098 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 02:12:04.099 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 198 | Tests with assertion: 1
* Generated 1 tests with total length 13
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          3 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 02:12:04.694 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:12:04.711 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 6 
[MASTER] 02:12:04.719 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 02:12:04.719 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragment:MockIllegalArgumentException, parseBodyFragment:StringIndexOutOfBoundsException, MockIllegalArgumentException,]
[MASTER] 02:12:04.719 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parseBodyFragment:MockIllegalArgumentException at 5
[MASTER] 02:12:04.719 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [parseBodyFragment:MockIllegalArgumentException, parseBodyFragment:StringIndexOutOfBoundsException, MockIllegalArgumentException,]
[MASTER] 02:12:04.719 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: parseBodyFragment:StringIndexOutOfBoundsException at 0
[MASTER] 02:12:04.719 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parseBodyFragment:MockIllegalArgumentException, parseBodyFragment:StringIndexOutOfBoundsException, MockIllegalArgumentException,]
[MASTER] 02:12:04.781 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 02:12:04.786 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 38%
* Total number of goals: 100
* Number of covered goals: 38
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
