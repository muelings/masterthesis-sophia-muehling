* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 12677
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 16:51:18.612 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 128
[MASTER] 16:51:52.208 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 16:51:54.259 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 16:51:54.259 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 16:51:54.262 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:51:54.262 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 16:51:55.641 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 16:51:55.650 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 16:51:55.650 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 16:51:55.652 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:51:55.652 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 16:51:56.047 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 16:51:56.059 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 16:51:56.059 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var1.size());  // (Inspector) Original Value: 0 | Regression Value: 4
[MASTER] 16:51:56.061 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:51:56.061 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 16:51:56.061 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 843 | Tests with assertion: 1
* Generated 1 tests with total length 11
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         37 / 300         
[MASTER] 16:51:56.693 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 16:51:56.702 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 16:51:56.706 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[toString, size]}
[MASTER] 16:51:56.761 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 16:51:56.765 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 2%
* Total number of goals: 128
* Number of covered goals: 3
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 36
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
