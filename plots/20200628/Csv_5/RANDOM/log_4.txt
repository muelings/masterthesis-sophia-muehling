* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVPrinter
* Starting client
* Connecting to master process on port 9988
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVPrinter
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 16:45:55.124 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 128
[MASTER] 16:46:01.383 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 16:46:01.383 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 16:46:01.383 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 16:46:01.396 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:46:01.396 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 16:46:02.751 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 16:46:02.781 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 16:46:02.781 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 16:46:02.781 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 16:46:02.785 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:46:02.785 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
Generated test with 3 assertions.
[MASTER] 16:46:03.136 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/CSVPrinter_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 16:46:03.160 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: null
[MASTER] 16:46:03.160 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 16:46:03.160 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("", var1.toString());  // (Inspector) Original Value:  | Regression Value: nullnull
[MASTER] 16:46:03.164 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 16:46:03.164 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 16:46:03.164 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 3 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 98 | Tests with assertion: 1
* Generated 1 tests with total length 24
* GA-Budget:
	- MaxTime :                          8 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 16:46:04.271 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 16:46:04.287 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 19 
[MASTER] 16:46:04.301 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[toString]}
[MASTER] 16:46:04.314 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 16:46:04.314 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 16:46:04.314 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 16:46:04.317 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 128
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 6
* Writing JUnit test case 'CSVPrinter_ESTest' to evosuite-tests
* Done!

* Computation finished
