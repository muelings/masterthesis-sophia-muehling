* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.javascript.jscomp.InlineObjectLiterals
* Starting client
* Connecting to master process on port 15398
* Analyzing classpath: 
  - ../defects4j_compiled/Closure_5_fixed/build/classes
* Finished analyzing classpath
* Generating tests for class com.google.javascript.jscomp.InlineObjectLiterals
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 126
[MASTER] 03:45:36.961 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
[MASTER] 03:50:37.963 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 243430 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
[MASTER] 03:50:38.029 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 03:50:38.030 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 185 seconds more than allowed.
[MASTER] 03:50:38.035 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 03:50:38.036 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 03:50:38.038 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 126
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'InlineObjectLiterals_ESTest' to evosuite-tests
* Done!

* Computation finished
