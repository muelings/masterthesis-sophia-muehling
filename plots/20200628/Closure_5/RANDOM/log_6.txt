* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.javascript.jscomp.InlineObjectLiterals
* Starting client
* Connecting to master process on port 5630
* Analyzing classpath: 
  - ../defects4j_compiled/Closure_5_fixed/build/classes
* Finished analyzing classpath
* Generating tests for class com.google.javascript.jscomp.InlineObjectLiterals
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:26:44.259 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 126
[MASTER] 04:31:45.260 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 252079 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 04:31:45.302 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:31:45.302 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 185 seconds more than allowed.
[MASTER] 04:31:45.307 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 04:31:45.308 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:31:45.310 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 126
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'InlineObjectLiterals_ESTest' to evosuite-tests
* Done!

* Computation finished
