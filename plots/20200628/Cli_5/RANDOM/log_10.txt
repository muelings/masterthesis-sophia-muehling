* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Util
* Starting client
* Connecting to master process on port 16854
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_5_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Util
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 11
[MASTER] 12:38:15.477 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:38:15.909 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 12:38:17.391 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:38:17.407 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 12:38:17.819 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:38:17.838 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 12:38:17.838 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 10 | Tests with assertion: 1
* Generated 1 tests with total length 14
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          2 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:38:18.196 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:38:18.218 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 12:38:18.244 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 12:38:18.244 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 12:38:18.244 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: stripLeadingHyphens:NullPointerException at 1
[MASTER] 12:38:18.244 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 12:38:18.349 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
[MASTER] 12:38:18.353 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 9%
* Total number of goals: 11
* Number of covered goals: 1
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Util_ESTest' to evosuite-tests
* Done!

* Computation finished
