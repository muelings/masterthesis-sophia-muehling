* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Util
* Starting client
* Connecting to master process on port 15246
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_5_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Util
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 11
[MASTER] 12:27:44.524 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:27:44.863 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 12:27:46.384 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:27:46.411 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 12:27:46.733 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/Util_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 12:27:46.765 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 12:27:46.765 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 6 | Tests with assertion: 1
* Generated 1 tests with total length 34
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          2 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 12:27:47.108 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:27:47.130 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 24 
[MASTER] 12:27:47.149 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 12:27:47.149 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 12:27:47.149 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: stripLeadingHyphens:NullPointerException at 18
[MASTER] 12:27:47.149 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [stripLeadingHyphens:NullPointerException]
[MASTER] 12:27:47.518 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 12:27:47.523 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 9%
* Total number of goals: 11
* Number of covered goals: 1
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Util_ESTest' to evosuite-tests
* Done!

* Computation finished
