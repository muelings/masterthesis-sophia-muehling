* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 4368
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 06:54:27.185 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 183
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var12.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var12.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var17, var12);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var17);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var12.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var12.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:54:28.398 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:28.406 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:54:28.406 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 12
[MASTER] 06:54:29.567 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:54:29.590 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var12.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var12.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var17, var12);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var17);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var12.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var12.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:54:29.591 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:29.593 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:54:29.593 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 12
Generated test with 12 assertions.
[MASTER] 06:54:29.935 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var12.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var12.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var12);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var17, var12);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.971 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var17);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var12, var9);  // (Same) Original Value: false | Regression Value: true
Keeping 14 assertions.
[MASTER] 06:54:29.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
*** Random test generation finished.
[MASTER] 06:54:29.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var12.isNaN());  // (Inspector) Original Value: false | Regression Value: true
*=*=*=* Total tests: 9 | Tests with assertion: 1
* Generated 1 tests with total length 18
[MASTER] 06:54:29.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var12.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
* GA-Budget:
[MASTER] 06:54:29.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var12.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:54:29.972 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var12);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:54:29.974 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:54:29.974 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 14
[MASTER] 06:54:29.974 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 06:54:30.725 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:54:30.748 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 17 
[MASTER] 06:54:30.772 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {SameAssertion=[], InspectorAssertion=[getReal, isNaN, isInfinite, getImaginary]}
[MASTER] 06:54:30.792 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:54:30.792 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 06:54:30.793 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
