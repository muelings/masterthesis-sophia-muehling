* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 21123
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 06:43:44.469 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 183
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var25, var24);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.342 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var32.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var32.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var35.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:45.343 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var14.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:45.356 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:43:45.356 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 31
[MASTER] 06:43:46.538 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var25, var24);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:46.608 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var32.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var32.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var35.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:46.609 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var14.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:46.615 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:43:46.615 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 31
Generated test with 31 assertions.
[MASTER] 06:43:47.051 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var25, var24);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var4.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var7.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var14.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var16.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var24.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var24.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var24.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var4.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var7.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var14.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var16.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var25, var24);  // (Same) Original Value: false | Regression Value: true
Keeping 68 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 4 | Tests with assertion: 1
* Generated 1 tests with total length 37
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                          2 / 300         
	- ShutdownTestWriter :               0 / 0           
[MASTER] 06:43:47.147 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var32.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var4.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var7.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var14.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var16.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var32.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var4.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var7.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var14.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var16.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var32));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var35.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.148 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var14.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var4.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var7.equals((java.lang.Object)var24));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var16.equals((java.lang.Object)var24));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var32));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var16));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var4));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var24.equals((java.lang.Object)var7));  // (Comp) Original Value: false | Regression Value: true
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var24.equals((java.lang.Object)var14));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:43:47.149 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var24, var25);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:43:47.158 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:43:47.158 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 68
[MASTER] 06:43:47.158 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:43:48.003 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:43:48.065 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 29 
[MASTER] 06:43:48.149 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {SameAssertion=[], InspectorAssertion=[isNaN, getReal, getImaginary, isInfinite], EqualsAssertion=[]}
[MASTER] 06:43:48.205 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:43:48.205 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 06:43:48.212 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
