* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 7597
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 06:22:11.688 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 183
[MASTER] 06:22:13.425 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var11, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:22:13.426 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var11.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:22:13.426 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var11.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:22:13.426 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var11.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:22:13.426 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var11.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:22:13.431 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:22:13.431 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 5
[MASTER] 06:22:14.535 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:22:14.557 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var11, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:22:14.557 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var11.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:22:14.557 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var11.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:22:14.557 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var11.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:22:14.557 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var11.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:22:14.560 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Generated test with 5 assertions.
[MASTER] 06:22:14.560 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 5
[MASTER] 06:22:14.847 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:22:14.869 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var11, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:22:14.869 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var11.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:22:14.869 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var11.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:22:14.869 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var11.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:22:14.869 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var11.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:22:14.869 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var11);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:22:14.871 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Keeping 6 assertions.
[MASTER] 06:22:14.871 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 6
[MASTER] 06:22:14.871 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 29 | Tests with assertion: 1
* Generated 1 tests with total length 17
* GA-Budget:
	- MaxTime :                          3 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 06:22:15.590 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:22:15.616 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 06:22:15.631 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {SameAssertion=[], InspectorAssertion=[isNaN, getReal, getImaginary, isInfinite]}
[MASTER] 06:22:15.889 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
[MASTER] 06:22:15.892 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 183
* Number of covered goals: 11
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
