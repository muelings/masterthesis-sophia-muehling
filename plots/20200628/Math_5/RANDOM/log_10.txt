* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 8277
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 183
[MASTER] 06:59:46.583 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var4, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var5.equals((java.lang.Object)var2));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var4, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var2.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var7.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var7, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var7, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var7.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var7.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var7.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var7.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var2.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var2.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:47.794 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:47.805 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:59:47.805 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 67
[MASTER] 06:59:48.925 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var4, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var5.equals((java.lang.Object)var2));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var4, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var2.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var7.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var7, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var7, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var7.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var7.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var7.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var7.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var2.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var2.equals((java.lang.Object)var5));  // (Comp) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var2, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var3, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var4);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var3);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var9, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var2);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var7);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var9);  // (Same) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var2.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var2.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:48.948 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var2.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:48.954 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:59:48.954 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 67
[MASTER] 06:59:49.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var4.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:49.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var4.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:49.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var4.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:49.793 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var4.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:49.795 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:59:49.795 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 06:59:50.079 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:59:50.086 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var4.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:50.086 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var4.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:50.086 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var4.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:50.086 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var4.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:50.087 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:59:50.087 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
Generated test with 4 assertions.
[MASTER] 06:59:50.368 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/math3/complex/Complex_5_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 4 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 55 | Tests with assertion: 1
* Generated 1 tests with total length 7
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          4 / 300         
[MASTER] 06:59:50.379 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var4.getImaginary(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 06:59:50.379 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(true, var4.isInfinite());  // (Inspector) Original Value: true | Regression Value: false
[MASTER] 06:59:50.379 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(false, var4.isNaN());  // (Inspector) Original Value: false | Regression Value: true
[MASTER] 06:59:50.379 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(Double.POSITIVE_INFINITY, var4.getReal(), 0.01D);  // (Inspector) Original Value: Infinity | Regression Value: NaN
[MASTER] 06:59:50.379 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 06:59:50.380 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 06:59:50.380 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:59:51.090 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:59:51.101 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 6 
[MASTER] 06:59:51.111 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[getImaginary, isInfinite, isNaN, getReal]}
[MASTER] 06:59:51.163 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
[MASTER] 06:59:51.167 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 2%
* Total number of goals: 183
* Number of covered goals: 3
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
