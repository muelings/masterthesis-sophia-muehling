* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 15064
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:54:37.265 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:54:37.276 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 183
* Using seed 1593060872852
* Starting evolution
[MASTER] 06:54:39.191 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:548.9999999961791 - branchDistance:0.0 - coverage:119.99999999617913 - ex: 0 - tex: 2
[MASTER] 06:54:39.191 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 548.9999999961791, number of tests: 7, total length: 118
[MASTER] 06:54:39.381 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:147.5999977560182 - branchDistance:0.0 - coverage:60.9999977560182 - ex: 0 - tex: 2
[MASTER] 06:54:39.381 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 147.5999977560182, number of tests: 9, total length: 202
[MASTER] 06:54:40.561 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:146.59999999621562 - branchDistance:0.0 - coverage:59.99999999621559 - ex: 0 - tex: 4
[MASTER] 06:54:40.561 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.59999999621562, number of tests: 10, total length: 246
[MASTER] 06:54:44.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:146.5999999942598 - branchDistance:0.0 - coverage:59.99999999425981 - ex: 0 - tex: 4
[MASTER] 06:54:44.149 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 146.5999999942598, number of tests: 11, total length: 256
[MASTER] 06:54:46.154 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:144.59999999532968 - branchDistance:0.0 - coverage:57.99999999532968 - ex: 0 - tex: 6
[MASTER] 06:54:46.154 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.59999999532968, number of tests: 10, total length: 270
[MASTER] 06:54:46.597 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:133.59999999279057 - branchDistance:0.0 - coverage:46.99999999279056 - ex: 0 - tex: 4
[MASTER] 06:54:46.597 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.59999999279057, number of tests: 10, total length: 263
[MASTER] 06:54:50.762 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:127.09999999568669 - branchDistance:0.0 - coverage:40.499999995686686 - ex: 0 - tex: 4
[MASTER] 06:54:50.763 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.09999999568669, number of tests: 10, total length: 270
[MASTER] 06:54:51.636 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:126.09999999568669 - branchDistance:0.0 - coverage:39.499999995686686 - ex: 0 - tex: 4
[MASTER] 06:54:51.636 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.09999999568669, number of tests: 11, total length: 309
[MASTER] 06:54:51.951 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:122.33333332755421 - branchDistance:0.0 - coverage:49.99999999422089 - ex: 0 - tex: 6
[MASTER] 06:54:51.951 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.33333332755421, number of tests: 10, total length: 287
[MASTER] 06:54:54.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:117.33333332902001 - branchDistance:0.0 - coverage:44.999999995686686 - ex: 0 - tex: 4
[MASTER] 06:54:54.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.33333332902001, number of tests: 10, total length: 255
[MASTER] 06:54:54.584 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:117.3333333220153 - branchDistance:0.0 - coverage:44.99999998868198 - ex: 0 - tex: 2
[MASTER] 06:54:54.584 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.3333333220153, number of tests: 10, total length: 239
[MASTER] 06:54:55.492 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:115.33333333049035 - branchDistance:0.0 - coverage:42.99999999715702 - ex: 0 - tex: 4
[MASTER] 06:54:55.492 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.33333333049035, number of tests: 10, total length: 253
[MASTER] 06:54:55.801 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:114.83333333048681 - branchDistance:0.0 - coverage:42.49999999715348 - ex: 0 - tex: 4
[MASTER] 06:54:55.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.83333333048681, number of tests: 11, total length: 274
[MASTER] 06:54:56.236 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:114.33333332480927 - branchDistance:0.0 - coverage:41.999999991475946 - ex: 0 - tex: 2
[MASTER] 06:54:56.236 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.33333332480927, number of tests: 11, total length: 282
[MASTER] 06:54:57.762 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:111.83333333049035 - branchDistance:0.0 - coverage:39.49999999715702 - ex: 0 - tex: 4
[MASTER] 06:54:57.763 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.83333333049035, number of tests: 11, total length: 271
[MASTER] 06:54:59.445 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:111.83333332853456 - branchDistance:0.0 - coverage:39.499999995201236 - ex: 0 - tex: 4
[MASTER] 06:54:59.445 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.83333332853456, number of tests: 11, total length: 273
[MASTER] 06:55:00.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:111.3333333286277 - branchDistance:0.0 - coverage:38.999999995294374 - ex: 0 - tex: 4
[MASTER] 06:55:00.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.3333333286277, number of tests: 11, total length: 290
[MASTER] 06:55:00.549 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:110.83333333049035 - branchDistance:0.0 - coverage:38.49999999715702 - ex: 0 - tex: 4
[MASTER] 06:55:00.550 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.83333333049035, number of tests: 11, total length: 275
[MASTER] 06:55:01.638 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:108.83333333049035 - branchDistance:0.0 - coverage:36.49999999715702 - ex: 0 - tex: 6
[MASTER] 06:55:01.638 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.83333333049035, number of tests: 11, total length: 280
[MASTER] 06:55:02.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:108.3333333286277 - branchDistance:0.0 - coverage:35.999999995294374 - ex: 0 - tex: 4
[MASTER] 06:55:02.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.3333333286277, number of tests: 12, total length: 328
[MASTER] 06:55:04.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:104.33333332853456 - branchDistance:0.0 - coverage:31.99999999520124 - ex: 0 - tex: 2
[MASTER] 06:55:04.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.33333332853456, number of tests: 12, total length: 319
[MASTER] 06:55:07.366 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:98.6428571462747 - branchDistance:0.0 - coverage:36.49999999090996 - ex: 0 - tex: 2
[MASTER] 06:55:07.366 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.6428571462747, number of tests: 11, total length: 289
[MASTER] 06:55:09.596 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:96.64285715056596 - branchDistance:0.0 - coverage:34.499999995201236 - ex: 0 - tex: 2
[MASTER] 06:55:09.596 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.64285715056596, number of tests: 12, total length: 318
[MASTER] 06:55:11.955 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:95.64285715056596 - branchDistance:0.0 - coverage:33.499999995201236 - ex: 0 - tex: 2
[MASTER] 06:55:11.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.64285715056596, number of tests: 13, total length: 338
[MASTER] 06:55:12.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:94.64285715056596 - branchDistance:0.0 - coverage:32.499999995201236 - ex: 0 - tex: 2
[MASTER] 06:55:12.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.64285715056596, number of tests: 13, total length: 350
[MASTER] 06:55:13.308 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:93.64285715154386 - branchDistance:0.0 - coverage:31.499999996179127 - ex: 0 - tex: 2
[MASTER] 06:55:13.308 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.64285715154386, number of tests: 13, total length: 351
[MASTER] 06:55:14.708 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:92.64285715154386 - branchDistance:0.0 - coverage:30.499999996179127 - ex: 0 - tex: 2
[MASTER] 06:55:14.708 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.64285715154386, number of tests: 14, total length: 372
[MASTER] 06:55:15.543 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9999999985680565 - fitness:92.14285715056597 - branchDistance:0.0 - coverage:29.99999999520124 - ex: 0 - tex: 2
[MASTER] 06:55:15.544 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.14285715056597, number of tests: 13, total length: 345
[MASTER] 06:55:15.593 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:91.0000000004861 - branchDistance:0.0 - coverage:36.49999999090996 - ex: 0 - tex: 2
[MASTER] 06:55:15.593 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.0000000004861, number of tests: 12, total length: 320
[MASTER] 06:55:17.239 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:89.00000000477736 - branchDistance:0.0 - coverage:34.499999995201236 - ex: 0 - tex: 2
[MASTER] 06:55:17.239 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.00000000477736, number of tests: 13, total length: 322
[MASTER] 06:55:17.822 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:88.00000000477736 - branchDistance:0.0 - coverage:33.499999995201236 - ex: 0 - tex: 2
[MASTER] 06:55:17.823 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.00000000477736, number of tests: 13, total length: 339
[MASTER] 06:55:18.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:86.00000000389261 - branchDistance:0.0 - coverage:31.499999994316482 - ex: 0 - tex: 2
[MASTER] 06:55:18.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.00000000389261, number of tests: 14, total length: 393
[MASTER] 06:55:19.909 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:84.50000000291472 - branchDistance:0.0 - coverage:29.999999993338594 - ex: 0 - tex: 2
[MASTER] 06:55:19.909 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.50000000291472, number of tests: 14, total length: 387
[MASTER] 06:55:20.475 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:83.50000000477736 - branchDistance:0.0 - coverage:28.99999999520124 - ex: 0 - tex: 2
[MASTER] 06:55:20.475 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.50000000477736, number of tests: 13, total length: 350
[MASTER] 06:55:20.656 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:82.50000000291472 - branchDistance:0.0 - coverage:27.999999993338594 - ex: 0 - tex: 4
[MASTER] 06:55:20.656 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.50000000291472, number of tests: 14, total length: 388
[MASTER] 06:55:21.712 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:82.00000000575525 - branchDistance:0.0 - coverage:27.499999996179127 - ex: 0 - tex: 2
[MASTER] 06:55:21.712 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.00000000575525, number of tests: 14, total length: 383
[MASTER] 06:55:21.951 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:81.50000000344606 - branchDistance:0.0 - coverage:26.999999993869938 - ex: 0 - tex: 2
[MASTER] 06:55:21.951 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.50000000344606, number of tests: 14, total length: 371
[MASTER] 06:55:25.323 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:80.50000000291472 - branchDistance:0.0 - coverage:25.999999993338594 - ex: 0 - tex: 4
[MASTER] 06:55:25.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.50000000291472, number of tests: 14, total length: 390
[MASTER] 06:55:28.670 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:80.50000000158342 - branchDistance:0.0 - coverage:25.999999992007293 - ex: 0 - tex: 2
[MASTER] 06:55:28.670 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.50000000158342, number of tests: 15, total length: 410
[MASTER] 06:55:28.901 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:78.50000000389505 - branchDistance:0.0 - coverage:23.999999994318934 - ex: 0 - tex: 6
[MASTER] 06:55:28.902 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.50000000389505, number of tests: 15, total length: 408
[MASTER] 06:55:34.537 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:77.5000000057577 - branchDistance:0.0 - coverage:22.99999999618158 - ex: 0 - tex: 2
[MASTER] 06:55:34.537 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.5000000057577, number of tests: 14, total length: 388
[MASTER] 06:55:35.012 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:76.5000000057577 - branchDistance:0.0 - coverage:21.99999999618158 - ex: 0 - tex: 6
[MASTER] 06:55:35.012 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.5000000057577, number of tests: 15, total length: 410
[MASTER] 06:55:36.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:76.50000000477736 - branchDistance:0.0 - coverage:21.99999999520124 - ex: 0 - tex: 4
[MASTER] 06:55:36.270 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.50000000477736, number of tests: 15, total length: 417
[MASTER] 06:55:38.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:76.50000000389505 - branchDistance:0.0 - coverage:21.999999994318934 - ex: 0 - tex: 6
[MASTER] 06:55:38.113 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.50000000389505, number of tests: 15, total length: 409
[MASTER] 06:55:38.408 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:75.5000000057577 - branchDistance:0.0 - coverage:20.99999999618158 - ex: 0 - tex: 6
[MASTER] 06:55:38.408 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.5000000057577, number of tests: 15, total length: 404
[MASTER] 06:55:38.597 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:72.55554994265005 - branchDistance:0.0 - coverage:23.999994379528186 - ex: 0 - tex: 4
[MASTER] 06:55:38.597 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.55554994265005, number of tests: 14, total length: 391
[MASTER] 06:55:39.368 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:70.5555555574408 - branchDistance:0.0 - coverage:21.999999994318934 - ex: 0 - tex: 6
[MASTER] 06:55:39.368 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.5555555574408, number of tests: 15, total length: 408
[MASTER] 06:55:41.586 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:69.55555555930344 - branchDistance:0.0 - coverage:20.99999999618158 - ex: 0 - tex: 6
[MASTER] 06:55:41.586 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.55555555930344, number of tests: 15, total length: 405
[MASTER] 06:55:41.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:68.55555555841869 - branchDistance:0.0 - coverage:19.99999999529682 - ex: 0 - tex: 4
[MASTER] 06:55:41.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.55555555841869, number of tests: 15, total length: 405
[MASTER] 06:55:43.635 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:68.55555555743834 - branchDistance:0.0 - coverage:19.999999994316482 - ex: 0 - tex: 6
[MASTER] 06:55:43.635 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.55555555743834, number of tests: 15, total length: 423
[MASTER] 06:55:43.703 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:67.55554994460827 - branchDistance:0.0 - coverage:18.99999438148641 - ex: 0 - tex: 2
[MASTER] 06:55:43.703 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.55554994460827, number of tests: 15, total length: 410
[MASTER] 06:55:43.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:66.55555555930098 - branchDistance:0.0 - coverage:17.999999996179127 - ex: 0 - tex: 8
[MASTER] 06:55:43.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.55555555930098, number of tests: 16, total length: 462
[MASTER] 06:55:44.269 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:65.55555556028133 - branchDistance:0.0 - coverage:16.999999997159467 - ex: 0 - tex: 4
[MASTER] 06:55:44.269 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.55555556028133, number of tests: 15, total length: 391
[MASTER] 06:55:46.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:65.55555556025169 - branchDistance:0.0 - coverage:16.999999997129827 - ex: 0 - tex: 4
[MASTER] 06:55:46.014 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.55555556025169, number of tests: 16, total length: 413
[MASTER] 06:55:47.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:61.55555556025169 - branchDistance:0.0 - coverage:12.999999997129827 - ex: 0 - tex: 4
[MASTER] 06:55:47.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.55555556025169, number of tests: 17, total length: 448
[MASTER] 06:55:52.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:61.55555555913082 - branchDistance:0.0 - coverage:12.999999996008958 - ex: 0 - tex: 4
[MASTER] 06:55:52.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.55555555913082, number of tests: 18, total length: 485
[MASTER] 06:55:54.564 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:60.55555556025169 - branchDistance:0.0 - coverage:11.999999997129827 - ex: 0 - tex: 4
[MASTER] 06:55:54.564 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.55555556025169, number of tests: 18, total length: 485
[MASTER] 06:55:55.051 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:60.55555556010587 - branchDistance:0.0 - coverage:11.99999999698401 - ex: 0 - tex: 4
[MASTER] 06:55:55.051 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.55555556010587, number of tests: 17, total length: 459
[MASTER] 06:55:56.672 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:60.55555556010342 - branchDistance:0.0 - coverage:11.99999999698156 - ex: 0 - tex: 4
[MASTER] 06:55:56.672 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.55555556010342, number of tests: 19, total length: 516
[MASTER] 06:55:57.972 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:60.55555555913082 - branchDistance:0.0 - coverage:11.999999996008958 - ex: 0 - tex: 4
[MASTER] 06:55:57.972 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.55555555913082, number of tests: 19, total length: 516
[MASTER] 06:56:00.465 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:59.55555556010587 - branchDistance:0.0 - coverage:10.999999996984009 - ex: 0 - tex: 4
[MASTER] 06:56:00.465 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.55555556010587, number of tests: 18, total length: 476
[MASTER] 06:56:03.358 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:59.55555556010342 - branchDistance:0.0 - coverage:10.99999999698156 - ex: 0 - tex: 4
[MASTER] 06:56:03.358 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.55555556010342, number of tests: 19, total length: 510
[MASTER] 06:56:03.399 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:59.555555558964244 - branchDistance:0.0 - coverage:10.999999995842376 - ex: 0 - tex: 4
[MASTER] 06:56:03.399 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.555555558964244, number of tests: 18, total length: 475
[MASTER] 06:56:03.584 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:57.555555561083764 - branchDistance:0.0 - coverage:8.999999997961899 - ex: 0 - tex: 2
[MASTER] 06:56:03.584 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.555555561083764, number of tests: 19, total length: 508
[MASTER] 06:56:04.010 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:55.80000000311027 - branchDistance:0.0 - coverage:11.99999999698156 - ex: 0 - tex: 4
[MASTER] 06:56:04.010 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.80000000311027, number of tests: 19, total length: 518
[MASTER] 06:56:05.295 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:55.555555561083764 - branchDistance:0.0 - coverage:6.999999997961899 - ex: 0 - tex: 2
[MASTER] 06:56:05.295 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.555555561083764, number of tests: 20, total length: 535
[MASTER] 06:56:08.524 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:54.80000000311027 - branchDistance:0.0 - coverage:10.999999996981558 - ex: 0 - tex: 2
[MASTER] 06:56:08.524 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.80000000311027, number of tests: 19, total length: 511
[MASTER] 06:56:09.410 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:51.79999780420577 - branchDistance:0.0 - coverage:7.999997798077061 - ex: 0 - tex: 4
[MASTER] 06:56:09.410 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.79999780420577, number of tests: 19, total length: 517
[MASTER] 06:56:10.079 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:50.80000000409061 - branchDistance:0.0 - coverage:6.999999997961899 - ex: 0 - tex: 4
[MASTER] 06:56:10.079 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.80000000409061, number of tests: 20, total length: 539
[MASTER] 06:56:12.938 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.80000000409061 - branchDistance:0.0 - coverage:5.999999997961899 - ex: 0 - tex: 2
[MASTER] 06:56:12.938 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.80000000409061, number of tests: 21, total length: 573
[MASTER] 06:56:23.766 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.79999993071485 - branchDistance:0.0 - coverage:5.999999924586137 - ex: 0 - tex: 4
[MASTER] 06:56:23.766 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.79999993071485, number of tests: 21, total length: 560
[MASTER] 06:56:36.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.799999882380575 - branchDistance:0.0 - coverage:5.999999876251859 - ex: 0 - tex: 4
[MASTER] 06:56:36.396 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.799999882380575, number of tests: 21, total length: 580
[MASTER] 06:56:38.318 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.799999238764705 - branchDistance:0.0 - coverage:5.999999232635991 - ex: 0 - tex: 6
[MASTER] 06:56:38.318 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.799999238764705, number of tests: 22, total length: 577
[MASTER] 06:56:38.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.79999756718463 - branchDistance:0.0 - coverage:5.999997561055917 - ex: 0 - tex: 2
[MASTER] 06:56:38.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.79999756718463, number of tests: 20, total length: 523
[MASTER] 06:56:43.812 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.79999755405751 - branchDistance:0.0 - coverage:5.999997547928797 - ex: 0 - tex: 2
[MASTER] 06:56:43.812 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.79999755405751, number of tests: 20, total length: 514
[MASTER] 06:56:50.159 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:49.73939394552265 - branchDistance:0.0 - coverage:5.9393939393939394 - ex: 0 - tex: 2
[MASTER] 06:56:50.159 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.73939394552265, number of tests: 20, total length: 500
[MASTER] 06:56:56.505 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:48.73939394552265 - branchDistance:0.0 - coverage:4.9393939393939394 - ex: 0 - tex: 2
[MASTER] 06:56:56.505 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.73939394552265, number of tests: 20, total length: 491
[MASTER] 06:57:27.445 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:47.80000000612871 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 2
[MASTER] 06:57:27.445 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.80000000612871, number of tests: 20, total length: 437
[MASTER] 06:57:27.519 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:47.73939394552265 - branchDistance:0.0 - coverage:3.9393939393939394 - ex: 0 - tex: 2
[MASTER] 06:57:27.519 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.73939394552265, number of tests: 20, total length: 465
[MASTER] 06:57:29.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:46.80000000612871 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 2
[MASTER] 06:57:29.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.80000000612871, number of tests: 22, total length: 507
[MASTER] 06:57:39.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:45.80000000612871 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 4
[MASTER] 06:57:39.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.80000000612871, number of tests: 21, total length: 462
[MASTER] 06:59:05.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.999999998568057 - fitness:44.80000000612871 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 2
[MASTER] 06:59:05.297 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 44.80000000612871, number of tests: 16, total length: 339

[MASTER] 06:59:38.346 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 433 generations, 622780 statements, best individuals have fitness: 44.80000000612871
[MASTER] 06:59:38.350 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 99%
* Total number of goals: 183
* Number of covered goals: 182
* Generated 16 tests with total length 279
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     45 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
[MASTER] 06:59:39.274 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:59:39.308 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 232 
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
* Going to analyze the coverage criteria
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 06:59:39.472 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 06:59:39.473 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
* Coverage analysis for criterion REGRESSION
[MASTER] 06:59:39.473 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
[MASTER] 06:59:39.474 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
