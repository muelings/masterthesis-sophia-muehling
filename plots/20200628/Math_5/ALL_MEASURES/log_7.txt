* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 12207
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:43:54.472 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:43:54.481 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 183
* Using seed 1593060230396
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 06:43:56.261 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:554.9999999958376 - branchDistance:0.0 - coverage:125.99999999583761 - ex: 0 - tex: 0
[MASTER] 06:43:56.261 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 554.9999999958376, number of tests: 6, total length: 117
[MASTER] 06:43:56.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:247.59999999431892 - branchDistance:0.0 - coverage:160.99999999431893 - ex: 0 - tex: 0
[MASTER] 06:43:56.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 247.59999999431892, number of tests: 4, total length: 66
[MASTER] 06:43:57.268 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:217.5999999930001 - branchDistance:0.0 - coverage:130.99999999300007 - ex: 0 - tex: 8
[MASTER] 06:43:57.268 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 217.5999999930001, number of tests: 9, total length: 155
[MASTER] 06:43:57.784 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:199.59999999329114 - branchDistance:0.0 - coverage:112.99999999329111 - ex: 0 - tex: 0
[MASTER] 06:43:57.784 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 199.59999999329114, number of tests: 8, total length: 116
[MASTER] 06:43:58.005 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:154.93332957596698 - branchDistance:0.0 - coverage:68.33332957596699 - ex: 0 - tex: 4
[MASTER] 06:43:58.005 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.93332957596698, number of tests: 10, total length: 267
[MASTER] 06:44:00.673 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:148.93332957596698 - branchDistance:0.0 - coverage:62.333329575966985 - ex: 0 - tex: 4
[MASTER] 06:44:00.673 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 148.93332957596698, number of tests: 10, total length: 243
[MASTER] 06:44:02.548 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:141.26666291027823 - branchDistance:0.0 - coverage:54.66666291027821 - ex: 0 - tex: 2
[MASTER] 06:44:02.548 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.26666291027823, number of tests: 9, total length: 239
[MASTER] 06:44:03.441 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:136.93332957508468 - branchDistance:0.0 - coverage:50.33332957508468 - ex: 0 - tex: 2
[MASTER] 06:44:03.441 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.93332957508468, number of tests: 10, total length: 256
[MASTER] 06:44:04.511 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:134.93333332951246 - branchDistance:0.0 - coverage:48.333333329512456 - ex: 0 - tex: 0
[MASTER] 06:44:04.511 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.93333332951246, number of tests: 10, total length: 225
[MASTER] 06:44:08.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:125.93332957694489 - branchDistance:0.0 - coverage:39.33332957694488 - ex: 0 - tex: 2
[MASTER] 06:44:08.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.93332957694489, number of tests: 11, total length: 317
[MASTER] 06:44:09.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:122.66666666284578 - branchDistance:0.0 - coverage:50.333333329512456 - ex: 0 - tex: 0
[MASTER] 06:44:09.302 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.66666666284578, number of tests: 10, total length: 199
[MASTER] 06:44:11.257 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:121.99999624361155 - branchDistance:0.0 - coverage:49.66666291027821 - ex: 0 - tex: 2
[MASTER] 06:44:11.257 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.99999624361155, number of tests: 10, total length: 266
[MASTER] 06:44:11.640 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:120.1666629102782 - branchDistance:0.0 - coverage:47.83332957694488 - ex: 0 - tex: 2
[MASTER] 06:44:11.640 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.1666629102782, number of tests: 11, total length: 293
[MASTER] 06:44:13.484 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:119.6666629102782 - branchDistance:0.0 - coverage:47.33332957694488 - ex: 0 - tex: 2
[MASTER] 06:44:13.484 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.6666629102782, number of tests: 10, total length: 271
[MASTER] 06:44:15.347 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:119.1666629102782 - branchDistance:0.0 - coverage:46.83332957694488 - ex: 0 - tex: 2
[MASTER] 06:44:15.347 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.1666629102782, number of tests: 11, total length: 292
[MASTER] 06:44:15.800 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:112.99999624361153 - branchDistance:0.0 - coverage:40.666662910278205 - ex: 0 - tex: 0
[MASTER] 06:44:15.800 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.99999624361153, number of tests: 12, total length: 264
[MASTER] 06:44:16.156 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:112.6666666628276 - branchDistance:0.0 - coverage:40.33333332949427 - ex: 0 - tex: 2
[MASTER] 06:44:16.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.6666666628276, number of tests: 11, total length: 280
[MASTER] 06:44:17.581 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:108.49999624361153 - branchDistance:0.0 - coverage:36.166662910278205 - ex: 0 - tex: 0
[MASTER] 06:44:17.582 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.49999624361153, number of tests: 11, total length: 275
[MASTER] 06:44:18.642 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:106.99999624361153 - branchDistance:0.0 - coverage:34.666662910278205 - ex: 0 - tex: 0
[MASTER] 06:44:18.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.99999624361153, number of tests: 12, total length: 297
[MASTER] 06:44:19.461 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:105.49999624361153 - branchDistance:0.0 - coverage:33.166662910278205 - ex: 0 - tex: 0
[MASTER] 06:44:19.461 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.49999624361153, number of tests: 13, total length: 338
[MASTER] 06:44:21.438 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:104.49999624361155 - branchDistance:0.0 - coverage:32.16666291027821 - ex: 0 - tex: 0
[MASTER] 06:44:21.438 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.49999624361155, number of tests: 13, total length: 336
[MASTER] 06:44:24.991 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:103.49999624361155 - branchDistance:0.0 - coverage:31.166662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:24.991 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.49999624361155, number of tests: 13, total length: 353
[MASTER] 06:44:27.361 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:103.1666629102782 - branchDistance:0.0 - coverage:30.833329576944877 - ex: 0 - tex: 4
[MASTER] 06:44:27.362 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.1666629102782, number of tests: 14, total length: 364
[MASTER] 06:44:27.462 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:102.49999624361155 - branchDistance:0.0 - coverage:30.166662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:27.462 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.49999624361155, number of tests: 14, total length: 352
[MASTER] 06:44:28.517 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:102.1666629102782 - branchDistance:0.0 - coverage:29.833329576944877 - ex: 0 - tex: 2
[MASTER] 06:44:28.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.1666629102782, number of tests: 14, total length: 361
[MASTER] 06:44:29.337 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:101.99999624361155 - branchDistance:0.0 - coverage:29.666662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:29.337 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.99999624361155, number of tests: 14, total length: 394
[MASTER] 06:44:29.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:100.99999624361155 - branchDistance:0.0 - coverage:28.666662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:29.615 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.99999624361155, number of tests: 14, total length: 372
[MASTER] 06:44:30.935 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:100.49999624361155 - branchDistance:0.0 - coverage:28.166662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:30.935 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.49999624361155, number of tests: 15, total length: 365
[MASTER] 06:44:31.452 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:99.99999624361155 - branchDistance:0.0 - coverage:27.666662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:31.452 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.99999624361155, number of tests: 15, total length: 395
[MASTER] 06:44:31.635 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:98.99999624361155 - branchDistance:0.0 - coverage:26.666662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:31.635 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.99999624361155, number of tests: 14, total length: 371
[MASTER] 06:44:34.019 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:98.49999624361155 - branchDistance:0.0 - coverage:26.166662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:34.019 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.49999624361155, number of tests: 15, total length: 353
[MASTER] 06:44:34.415 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:97.99999624361155 - branchDistance:0.0 - coverage:25.666662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:34.415 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.99999624361155, number of tests: 14, total length: 372
[MASTER] 06:44:35.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:97.49999624361155 - branchDistance:0.0 - coverage:25.166662910278212 - ex: 0 - tex: 0
[MASTER] 06:44:35.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.49999624361155, number of tests: 15, total length: 378
[MASTER] 06:44:35.857 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:97.1666629102782 - branchDistance:0.0 - coverage:24.833329576944877 - ex: 0 - tex: 2
[MASTER] 06:44:35.857 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.1666629102782, number of tests: 15, total length: 376
[MASTER] 06:44:35.908 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:96.6666629102782 - branchDistance:0.0 - coverage:24.333329576944877 - ex: 0 - tex: 0
[MASTER] 06:44:35.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.6666629102782, number of tests: 16, total length: 385
[MASTER] 06:44:41.435 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:94.1666629102782 - branchDistance:0.0 - coverage:21.833329576944877 - ex: 0 - tex: 2
[MASTER] 06:44:41.435 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.1666629102782, number of tests: 16, total length: 409
[MASTER] 06:44:45.475 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:93.83332957694488 - branchDistance:0.0 - coverage:21.499996243611545 - ex: 0 - tex: 2
[MASTER] 06:44:45.475 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.83332957694488, number of tests: 16, total length: 408
[MASTER] 06:44:46.212 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:92.6666629102782 - branchDistance:0.0 - coverage:20.333329576944877 - ex: 0 - tex: 2
[MASTER] 06:44:46.212 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.6666629102782, number of tests: 15, total length: 385
[MASTER] 06:44:47.012 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:92.1666629102782 - branchDistance:0.0 - coverage:19.833329576944877 - ex: 0 - tex: 2
[MASTER] 06:44:47.012 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.1666629102782, number of tests: 16, total length: 451
[MASTER] 06:44:50.044 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:91.66666291028065 - branchDistance:0.0 - coverage:19.333329576947328 - ex: 0 - tex: 2
[MASTER] 06:44:50.044 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.66666291028065, number of tests: 15, total length: 392
[MASTER] 06:44:50.679 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:90.6666629102782 - branchDistance:0.0 - coverage:18.333329576944877 - ex: 0 - tex: 2
[MASTER] 06:44:50.679 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.6666629102782, number of tests: 16, total length: 412
[MASTER] 06:44:51.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:90.33332957694488 - branchDistance:0.0 - coverage:17.999996243611545 - ex: 0 - tex: 2
[MASTER] 06:44:51.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.33332957694488, number of tests: 16, total length: 436
[MASTER] 06:44:52.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:89.33332957694488 - branchDistance:0.0 - coverage:16.999996243611545 - ex: 0 - tex: 2
[MASTER] 06:44:52.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.33332957694488, number of tests: 17, total length: 455
[MASTER] 06:44:55.548 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:88.3333333313751 - branchDistance:0.0 - coverage:15.999999998041773 - ex: 0 - tex: 2
[MASTER] 06:44:55.548 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.3333333313751, number of tests: 17, total length: 440
[MASTER] 06:44:57.439 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:88.33332957694488 - branchDistance:0.0 - coverage:15.999996243611545 - ex: 0 - tex: 2
[MASTER] 06:44:57.439 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.33332957694488, number of tests: 17, total length: 456
[MASTER] 06:44:57.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:87.3333333313751 - branchDistance:0.0 - coverage:14.999999998041773 - ex: 0 - tex: 2
[MASTER] 06:44:57.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.3333333313751, number of tests: 17, total length: 438
[MASTER] 06:44:58.103 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:87.33332957694488 - branchDistance:0.0 - coverage:14.999996243611545 - ex: 0 - tex: 2
[MASTER] 06:44:58.103 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.33332957694488, number of tests: 18, total length: 486
[MASTER] 06:44:58.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:86.33332957694488 - branchDistance:0.0 - coverage:13.999996243611545 - ex: 0 - tex: 2
[MASTER] 06:44:58.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.33332957694488, number of tests: 18, total length: 495
[MASTER] 06:45:04.291 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:85.33332957694488 - branchDistance:0.0 - coverage:12.999996243611545 - ex: 0 - tex: 2
[MASTER] 06:45:04.291 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.33332957694488, number of tests: 18, total length: 496
[MASTER] 06:45:07.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:84.33332957792277 - branchDistance:0.0 - coverage:11.999996244589433 - ex: 0 - tex: 2
[MASTER] 06:45:07.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.33332957792277, number of tests: 19, total length: 458
[MASTER] 06:45:10.438 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:84.33332957694488 - branchDistance:0.0 - coverage:11.999996243611545 - ex: 0 - tex: 4
[MASTER] 06:45:10.438 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.33332957694488, number of tests: 18, total length: 496
[MASTER] 06:45:10.727 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:83.33332957792277 - branchDistance:0.0 - coverage:10.999996244589433 - ex: 0 - tex: 2
[MASTER] 06:45:10.727 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.33332957792277, number of tests: 18, total length: 498
[MASTER] 06:45:15.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:82.33332957792277 - branchDistance:0.0 - coverage:9.999996244589433 - ex: 0 - tex: 2
[MASTER] 06:45:15.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.33332957792277, number of tests: 18, total length: 499
[MASTER] 06:45:18.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:81.33332957792277 - branchDistance:0.0 - coverage:8.999996244589433 - ex: 0 - tex: 2
[MASTER] 06:45:18.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.33332957792277, number of tests: 18, total length: 500
[MASTER] 06:45:23.807 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:80.3333295789031 - branchDistance:0.0 - coverage:7.999996245569772 - ex: 0 - tex: 2
[MASTER] 06:45:23.807 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.3333295789031, number of tests: 19, total length: 506
[MASTER] 06:45:24.916 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:80.33332957792277 - branchDistance:0.0 - coverage:7.999996244589433 - ex: 0 - tex: 2
[MASTER] 06:45:24.916 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.33332957792277, number of tests: 19, total length: 507
[MASTER] 06:45:25.495 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.0 - fitness:79.3333295789031 - branchDistance:0.0 - coverage:6.999996245569772 - ex: 0 - tex: 2
[MASTER] 06:45:25.496 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.3333295789031, number of tests: 19, total length: 505
[MASTER] 06:45:30.347 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:63.49999625416556 - branchDistance:0.0 - coverage:8.999996244589433 - ex: 0 - tex: 2
[MASTER] 06:45:30.347 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.49999625416556, number of tests: 19, total length: 518
[MASTER] 06:45:38.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:62.4999962551459 - branchDistance:0.0 - coverage:7.999996245569772 - ex: 0 - tex: 2
[MASTER] 06:45:38.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.4999962551459, number of tests: 20, total length: 535
[MASTER] 06:45:40.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.9999999985680565 - fitness:61.4999962551459 - branchDistance:0.0 - coverage:6.999996245569772 - ex: 0 - tex: 2
[MASTER] 06:45:40.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.4999962551459, number of tests: 19, total length: 493
[MASTER] 06:45:43.241 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:55.55555180869163 - branchDistance:0.0 - coverage:6.999996245569772 - ex: 0 - tex: 2
[MASTER] 06:45:43.241 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.55555180869163, number of tests: 19, total length: 491
[MASTER] 06:45:52.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:54.55555180869163 - branchDistance:0.0 - coverage:5.999996245569772 - ex: 0 - tex: 2
[MASTER] 06:45:52.601 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.55555180869163, number of tests: 20, total length: 506
[MASTER] 06:46:04.029 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:53.55555180869163 - branchDistance:0.0 - coverage:4.999996245569772 - ex: 0 - tex: 2
[MASTER] 06:46:04.029 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.55555180869163, number of tests: 19, total length: 489
[MASTER] 06:46:16.251 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:53.5555486388252 - branchDistance:0.0 - coverage:4.999993075703335 - ex: 0 - tex: 2
[MASTER] 06:46:16.251 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.5555486388252, number of tests: 20, total length: 486
[MASTER] 06:46:16.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:53.555546929779894 - branchDistance:0.0 - coverage:4.999991366658033 - ex: 0 - tex: 2
[MASTER] 06:46:16.412 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.555546929779894, number of tests: 19, total length: 483
[MASTER] 06:46:16.665 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:52.55555180869164 - branchDistance:0.0 - coverage:3.9999962455697724 - ex: 0 - tex: 2
[MASTER] 06:46:16.665 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.55555180869164, number of tests: 19, total length: 475
[MASTER] 06:46:37.328 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:52.555546483974396 - branchDistance:0.0 - coverage:3.999990920852532 - ex: 0 - tex: 2
[MASTER] 06:46:37.328 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.555546483974396, number of tests: 18, total length: 460
[MASTER] 06:46:55.527 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.9999999985680565 - fitness:52.55554638641052 - branchDistance:0.0 - coverage:3.999990823288658 - ex: 0 - tex: 2
[MASTER] 06:46:55.527 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.55554638641052, number of tests: 18, total length: 425
[MASTER] 06:47:10.849 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.941176469156293 - fitness:44.11827039830079 - branchDistance:0.0 - coverage:3.999990823288658 - ex: 0 - tex: 2
[MASTER] 06:47:10.849 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 44.11827039830079, number of tests: 18, total length: 401
[MASTER] 06:47:27.790 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.941176469156293 - fitness:43.11827039830079 - branchDistance:0.0 - coverage:2.999990823288658 - ex: 0 - tex: 2
[MASTER] 06:47:27.790 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 43.11827039830079, number of tests: 18, total length: 420
[MASTER] 06:47:34.787 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.941176469156293 - fitness:43.11827029930567 - branchDistance:0.0 - coverage:2.9999907242935393 - ex: 0 - tex: 2
[MASTER] 06:47:34.787 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 43.11827029930567, number of tests: 18, total length: 417
[MASTER] 06:47:39.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.941176469156293 - fitness:42.11827957501213 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 2
[MASTER] 06:47:39.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 42.11827957501213, number of tests: 19, total length: 434
[MASTER] 06:48:55.514 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:48:55.518 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 322 generations, 493050 statements, best individuals have fitness: 42.11827957501213
* Coverage of criterion REGRESSION: 99%
* Total number of goals: 183
* Number of covered goals: 181
* Generated 19 tests with total length 348
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                     42 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        302 / 300          Finished!
[MASTER] 06:48:56.675 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:48:56.761 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 300 
[MASTER] 06:48:57.118 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 06:48:57.118 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:48:57.118 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 06:48:57.118 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 06:48:57.118 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 06:48:57.118 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 06:48:57.119 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 06:48:57.119 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 06:48:57.119 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 06:48:57.119 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 06:48:57.119 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 06:48:57.120 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 06:48:57.121 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
