* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.math3.complex.Complex
* Starting client
* Connecting to master process on port 20962
* Analyzing classpath: 
  - ../defects4j_compiled/Math_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.math3.complex.Complex
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:11:41.168 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:11:41.176 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 183
* Using seed 1593058296467
* Starting evolution
[MASTER] 06:11:43.046 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:559.9999999981374 - branchDistance:0.0 - coverage:130.99999999813735 - ex: 0 - tex: 0
[MASTER] 06:11:43.046 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 559.9999999981374, number of tests: 5, total length: 113
[MASTER] 06:11:43.396 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:189.59399398737807 - branchDistance:0.0 - coverage:102.99399398737806 - ex: 0 - tex: 0
[MASTER] 06:11:43.396 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 189.59399398737807, number of tests: 6, total length: 145
[MASTER] 06:11:45.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:187.5999986857246 - branchDistance:0.0 - coverage:100.99999868572459 - ex: 0 - tex: 0
[MASTER] 06:11:45.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 187.5999986857246, number of tests: 7, total length: 139
[MASTER] 06:11:48.015 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:176.9285600587105 - branchDistance:0.0 - coverage:90.32856005871048 - ex: 0 - tex: 0
[MASTER] 06:11:48.015 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 176.9285600587105, number of tests: 6, total length: 132
[MASTER] 06:11:48.538 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:163.37777771888335 - branchDistance:0.0 - coverage:76.77777771888336 - ex: 0 - tex: 2
[MASTER] 06:11:48.538 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 163.37777771888335, number of tests: 10, total length: 197
[MASTER] 06:11:49.815 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:156.59999996999073 - branchDistance:0.0 - coverage:69.99999996999074 - ex: 0 - tex: 2
[MASTER] 06:11:49.815 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 156.59999996999073, number of tests: 8, total length: 236
[MASTER] 06:11:53.231 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:156.5999999681281 - branchDistance:0.0 - coverage:69.9999999681281 - ex: 0 - tex: 2
[MASTER] 06:11:53.232 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 156.5999999681281, number of tests: 8, total length: 237
[MASTER] 06:11:54.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:154.5999999942258 - branchDistance:0.0 - coverage:67.99999999422579 - ex: 0 - tex: 0
[MASTER] 06:11:54.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.5999999942258, number of tests: 7, total length: 204
[MASTER] 06:11:54.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:145.54871252348624 - branchDistance:0.0 - coverage:58.94871252348622 - ex: 0 - tex: 0
[MASTER] 06:11:54.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 145.54871252348624, number of tests: 8, total length: 231
[MASTER] 06:11:54.276 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:144.59702820277147 - branchDistance:0.0 - coverage:57.99702820277146 - ex: 0 - tex: 2
[MASTER] 06:11:54.276 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.59702820277147, number of tests: 8, total length: 232
[MASTER] 06:11:54.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:142.59702822513896 - branchDistance:0.0 - coverage:55.99702822513896 - ex: 0 - tex: 0
[MASTER] 06:11:54.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.59702822513896, number of tests: 8, total length: 226
[MASTER] 06:11:54.951 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:141.59702822513896 - branchDistance:0.0 - coverage:54.99702822513896 - ex: 0 - tex: 0
[MASTER] 06:11:54.951 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.59702822513896, number of tests: 8, total length: 224
[MASTER] 06:11:55.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:140.59702820277147 - branchDistance:0.0 - coverage:53.99702820277145 - ex: 0 - tex: 2
[MASTER] 06:11:55.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.59702820277147, number of tests: 8, total length: 227
[MASTER] 06:11:55.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:138.59378881321683 - branchDistance:0.0 - coverage:51.99378881321682 - ex: 0 - tex: 0
[MASTER] 06:11:55.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.59378881321683, number of tests: 10, total length: 294
[MASTER] 06:11:56.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:137.54871252348624 - branchDistance:0.0 - coverage:50.94871252348622 - ex: 0 - tex: 0
[MASTER] 06:11:56.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.54871252348624, number of tests: 9, total length: 264
[MASTER] 06:11:57.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:131.93333332667194 - branchDistance:0.0 - coverage:45.33333332667193 - ex: 0 - tex: 2
[MASTER] 06:11:57.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.93333332667194, number of tests: 9, total length: 270
[MASTER] 06:11:58.636 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:131.93333332297195 - branchDistance:0.0 - coverage:45.33333332297194 - ex: 0 - tex: 2
[MASTER] 06:11:58.636 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.93333332297195, number of tests: 9, total length: 268
[MASTER] 06:12:00.589 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:128.93333332851265 - branchDistance:0.0 - coverage:42.33333332851263 - ex: 0 - tex: 0
[MASTER] 06:12:00.590 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.93333332851265, number of tests: 9, total length: 266
[MASTER] 06:12:01.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:128.5999926871491 - branchDistance:0.0 - coverage:41.99999268714908 - ex: 0 - tex: 2
[MASTER] 06:12:01.929 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.5999926871491, number of tests: 10, total length: 255
[MASTER] 06:12:06.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:127.93332601954943 - branchDistance:0.0 - coverage:41.33332601954942 - ex: 0 - tex: 0
[MASTER] 06:12:06.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.93332601954943, number of tests: 10, total length: 286
[MASTER] 06:12:06.775 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:127.5487179439192 - branchDistance:0.0 - coverage:40.948717943919185 - ex: 0 - tex: 0
[MASTER] 06:12:06.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.5487179439192, number of tests: 10, total length: 294
[MASTER] 06:12:07.117 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:126.5999926871491 - branchDistance:0.0 - coverage:39.99999268714908 - ex: 0 - tex: 4
[MASTER] 06:12:07.118 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.5999926871491, number of tests: 11, total length: 277
[MASTER] 06:12:07.147 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:126.59702822700162 - branchDistance:0.0 - coverage:39.99702822700161 - ex: 0 - tex: 2
[MASTER] 06:12:07.147 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.59702822700162, number of tests: 10, total length: 318
[MASTER] 06:12:07.278 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:125.93332602048243 - branchDistance:0.0 - coverage:39.33332602048242 - ex: 0 - tex: 2
[MASTER] 06:12:07.278 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.93332602048243, number of tests: 10, total length: 295
[MASTER] 06:12:07.336 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:124.59999268812922 - branchDistance:0.0 - coverage:37.99999268812922 - ex: 0 - tex: 2
[MASTER] 06:12:07.336 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.59999268812922, number of tests: 10, total length: 283
[MASTER] 06:12:09.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:124.5999926871491 - branchDistance:0.0 - coverage:37.99999268714908 - ex: 0 - tex: 2
[MASTER] 06:12:09.180 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.5999926871491, number of tests: 10, total length: 281
[MASTER] 06:12:10.411 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:123.5999926871491 - branchDistance:0.0 - coverage:36.99999268714908 - ex: 0 - tex: 4
[MASTER] 06:12:10.411 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.5999926871491, number of tests: 10, total length: 302
[MASTER] 06:12:11.312 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:122.59999268812922 - branchDistance:0.0 - coverage:35.99999268812922 - ex: 0 - tex: 2
[MASTER] 06:12:11.312 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.59999268812922, number of tests: 12, total length: 305
[MASTER] 06:12:11.614 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:120.93332602048243 - branchDistance:0.0 - coverage:34.33332602048242 - ex: 0 - tex: 2
[MASTER] 06:12:11.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.93332602048243, number of tests: 11, total length: 335
[MASTER] 06:12:12.723 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:119.59999268812922 - branchDistance:0.0 - coverage:32.99999268812922 - ex: 0 - tex: 2
[MASTER] 06:12:12.723 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.59999268812922, number of tests: 12, total length: 321
[MASTER] 06:12:13.232 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:118.59999268812922 - branchDistance:0.0 - coverage:31.99999268812922 - ex: 0 - tex: 2
[MASTER] 06:12:13.232 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.59999268812922, number of tests: 12, total length: 325
[MASTER] 06:12:14.488 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:117.59999268812922 - branchDistance:0.0 - coverage:30.99999268812922 - ex: 0 - tex: 2
[MASTER] 06:12:14.488 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 117.59999268812922, number of tests: 12, total length: 322
[MASTER] 06:12:14.790 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:116.59999268812922 - branchDistance:0.0 - coverage:29.99999268812922 - ex: 0 - tex: 2
[MASTER] 06:12:14.790 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.59999268812922, number of tests: 12, total length: 322
[MASTER] 06:12:16.973 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.0 - fitness:115.59999268812922 - branchDistance:0.0 - coverage:28.99999268812922 - ex: 0 - tex: 2
[MASTER] 06:12:16.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.59999268812922, number of tests: 13, total length: 360
[MASTER] 06:12:19.748 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:89.66100963825741 - branchDistance:0.0 - coverage:26.999992689104857 - ex: 0 - tex: 2
[MASTER] 06:12:19.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.66100963825741, number of tests: 13, total length: 358
[MASTER] 06:12:21.234 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:88.66100963825741 - branchDistance:0.0 - coverage:25.999992689104857 - ex: 0 - tex: 2
[MASTER] 06:12:21.235 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.66100963825741, number of tests: 13, total length: 365
[MASTER] 06:12:23.727 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:87.66100963825741 - branchDistance:0.0 - coverage:24.999992689104857 - ex: 0 - tex: 2
[MASTER] 06:12:23.727 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.66100963825741, number of tests: 15, total length: 416
[MASTER] 06:12:25.833 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:85.66100963825741 - branchDistance:0.0 - coverage:22.999992689104857 - ex: 0 - tex: 4
[MASTER] 06:12:25.833 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.66100963825741, number of tests: 15, total length: 407
[MASTER] 06:12:28.410 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:84.66100963825741 - branchDistance:0.0 - coverage:21.999992689104857 - ex: 0 - tex: 4
[MASTER] 06:12:28.410 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.66100963825741, number of tests: 15, total length: 414
[MASTER] 06:12:32.794 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:83.66100963825741 - branchDistance:0.0 - coverage:20.999992689104857 - ex: 0 - tex: 6
[MASTER] 06:12:32.794 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.66100963825741, number of tests: 16, total length: 449
[MASTER] 06:12:33.420 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:81.66100963825741 - branchDistance:0.0 - coverage:18.999992689104857 - ex: 0 - tex: 4
[MASTER] 06:12:33.420 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.66100963825741, number of tests: 15, total length: 413
[MASTER] 06:12:39.319 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:80.66100963825741 - branchDistance:0.0 - coverage:17.999992689104857 - ex: 0 - tex: 4
[MASTER] 06:12:39.319 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.66100963825741, number of tests: 16, total length: 441
[MASTER] 06:12:40.119 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:79.66100963923775 - branchDistance:0.0 - coverage:16.999992690085197 - ex: 0 - tex: 2
[MASTER] 06:12:40.119 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.66100963923775, number of tests: 15, total length: 430
[MASTER] 06:12:41.402 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:79.1610169481722 - branchDistance:0.0 - coverage:16.49999999901966 - ex: 0 - tex: 4
[MASTER] 06:12:41.402 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.1610169481722, number of tests: 16, total length: 448
[MASTER] 06:12:42.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:77.66101694915255 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 4
[MASTER] 06:12:42.600 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.66101694915255, number of tests: 16, total length: 440
[MASTER] 06:12:48.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:76.66101694915255 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 4
[MASTER] 06:12:48.733 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.66101694915255, number of tests: 17, total length: 450
[MASTER] 06:12:54.042 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:75.66101694915255 - branchDistance:0.0 - coverage:13.0 - ex: 0 - tex: 2
[MASTER] 06:12:54.042 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.66101694915255, number of tests: 17, total length: 439
[MASTER] 06:12:55.899 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:74.66101694915255 - branchDistance:0.0 - coverage:12.0 - ex: 0 - tex: 2
[MASTER] 06:12:55.899 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.66101694915255, number of tests: 17, total length: 447
[MASTER] 06:13:00.632 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:73.66101694915255 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 2
[MASTER] 06:13:00.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.66101694915255, number of tests: 17, total length: 475
[MASTER] 06:13:12.404 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:72.66101694915255 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 2
[MASTER] 06:13:12.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.66101694915255, number of tests: 17, total length: 470
[MASTER] 06:13:18.700 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.9411764705882355 - fitness:71.66101694915255 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 2
[MASTER] 06:13:18.700 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.66101694915255, number of tests: 17, total length: 460
[MASTER] 06:13:25.263 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.947368421052632 - fitness:57.83529411764705 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 2
[MASTER] 06:13:25.263 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.83529411764705, number of tests: 18, total length: 495
[MASTER] 06:13:34.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:53.026455026455025 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 4
[MASTER] 06:13:34.792 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.026455026455025, number of tests: 20, total length: 516
[MASTER] 06:13:42.434 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:52.026455026455025 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 06:13:42.434 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.026455026455025, number of tests: 21, total length: 531
[MASTER] 06:14:00.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:51.026455026455025 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 2
[MASTER] 06:14:00.509 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.026455026455025, number of tests: 21, total length: 522
[MASTER] 06:14:04.426 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:49.026455026455025 - branchDistance:0.0 - coverage:5.0 - ex: 0 - tex: 2
[MASTER] 06:14:04.426 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.026455026455025, number of tests: 20, total length: 468
[MASTER] 06:14:08.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:48.026455026455025 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 2
[MASTER] 06:14:08.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.026455026455025, number of tests: 21, total length: 515
[MASTER] 06:14:13.622 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:47.026455026455025 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 2
[MASTER] 06:14:13.622 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.026455026455025, number of tests: 21, total length: 499
[MASTER] 06:14:20.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:46.026455026455025 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 2
[MASTER] 06:14:20.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.026455026455025, number of tests: 21, total length: 500
[MASTER] 06:14:52.007 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.947368421052632 - fitness:45.026455026455025 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 2
[MASTER] 06:14:52.007 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.026455026455025, number of tests: 22, total length: 476
[MASTER] 06:15:24.875 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.942595151362895 - fitness:35.06910206141543 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 2
[MASTER] 06:15:24.875 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.06910206141543, number of tests: 22, total length: 419
[MASTER] 06:15:51.676 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.942595151362895 - fitness:32.697298125174555 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 2
[MASTER] 06:15:51.676 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.697298125174555, number of tests: 21, total length: 381
[MASTER] 06:16:42.316 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 419 generations, 640202 statements, best individuals have fitness: 32.697298125174555
[MASTER] 06:16:42.319 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 99%
* Total number of goals: 183
* Number of covered goals: 182
* Generated 21 tests with total length 332
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        302 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     33 / 0           
[MASTER] 06:16:43.334 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:16:43.416 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 280 
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 06:16:43.671 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 19: no assertions
[MASTER] 06:16:43.672 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 20: no assertions
[MASTER] 06:16:43.673 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 06:16:43.673 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 183
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Complex_ESTest' to evosuite-tests
* Done!

* Computation finished
