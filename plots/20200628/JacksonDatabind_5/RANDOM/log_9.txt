* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Starting client
* Connecting to master process on port 7776
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonDatabind_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 00:42:22.078 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 288
[MASTER] 00:47:23.084 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 253854 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 00:47:23.126 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 00:47:23.127 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 184 seconds more than allowed.
[MASTER] 00:47:23.131 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 00:47:23.132 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 00:47:23.134 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'AnnotatedClass_ESTest' to evosuite-tests
* Done!

* Computation finished
