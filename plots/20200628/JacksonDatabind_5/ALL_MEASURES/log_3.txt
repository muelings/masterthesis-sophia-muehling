* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Starting client
* Connecting to master process on port 18697
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonDatabind_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:45:56.221 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:45:56.229 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 288
* Using seed 1593121551590
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 23:45:56.347 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1305.0 - branchDistance:0.0 - coverage:652.0 - ex: 0 - tex: 0
[MASTER] 23:45:56.347 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1305.0, number of tests: 8, total length: 0
[MASTER] 23:50:57.241 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 75101 generations, 0 statements, best individuals have fitness: 1305.0
[MASTER] 23:50:57.246 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 10 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                  1,305 / 0           
[MASTER] 23:50:57.802 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:50:57.825 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 23:50:57.848 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 23:50:57.849 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 23:50:57.850 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'AnnotatedClass_ESTest' to evosuite-tests
* Done!

* Computation finished
