* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Starting client
* Connecting to master process on port 19579
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonDatabind_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.databind.introspect.AnnotatedClass
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 23:25:23.781 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 23:25:23.793 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 288
[Progress:>                             0%] [Cov:>                                  0%]* Using seed 1593120319705
* Starting evolution
[MASTER] 23:25:23.922 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:1305.0 - branchDistance:0.0 - coverage:652.0 - ex: 0 - tex: 0
[MASTER] 23:25:23.922 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1305.0, number of tests: 8, total length: 0
[MASTER] 23:30:24.805 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 103546 generations, 0 statements, best individuals have fitness: 1305.0
[MASTER] 23:30:24.811 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 6 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                  1,305 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 23:30:25.499 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 23:30:25.521 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 23:30:25.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 23:30:25.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 23:30:25.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 23:30:25.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 23:30:25.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 23:30:25.536 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 23:30:25.536 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 23:30:25.537 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 288
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'AnnotatedClass_ESTest' to evosuite-tests
* Done!

* Computation finished
