* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 9165
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 151
[MASTER] 08:26:30.296 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:30:52.530 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 08:30:52.530 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 08:30:52.534 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 08:30:52.534 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 08:30:53.503 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 08:30:53.503 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 08:30:53.505 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 08:30:53.505 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 08:30:53.717 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var8, var10);  // (Same) Original Value: false | Regression Value: true
[MASTER] 08:30:53.717 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertNotSame(var10, var8);  // (Same) Original Value: false | Regression Value: true
[MASTER] 08:30:53.719 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 08:30:53.719 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Keeping 2 assertions.
[MASTER] 08:30:53.720 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 18515 | Tests with assertion: 1
* Generated 1 tests with total length 14
* GA-Budget:
	- MaxTime :                        263 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 08:30:54.696 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:30:54.696 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 149 seconds more than allowed.
[MASTER] 08:30:54.715 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 14 
[MASTER] 08:30:54.735 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {SameAssertion=[]}
[MASTER] 08:30:55.179 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 9 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:30:55.182 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 1%
* Total number of goals: 151
* Number of covered goals: 1
* Generated 1 tests with total length 9
* Resulting test suite's coverage: 0%
* Compiling and checking tests
[MASTER] 08:30:56.559 [logback-1] WARN  JUnitAnalyzer - Found unstable test named test0 -> class java.lang.AssertionError: expected not same
[MASTER] 08:30:56.560 [logback-1] WARN  JUnitAnalyzer - Failing test:
 BuddhistChronology buddhistChronology0 = BuddhistChronology.getInstance();
DateTimeZone dateTimeZone0 = buddhistChronology0.getZone();
MutableDateTime mutableDateTime0 = new MutableDateTime(dateTimeZone0);
MutableInterval mutableInterval0 = new MutableInterval(mutableDateTime0, mutableDateTime0);
Days days0 = Days.daysIn(mutableInterval0);
DurationFieldType durationFieldType0 = DurationFieldType.HALFDAYS_TYPE;
UnsupportedDurationField unsupportedDurationField0 = UnsupportedDurationField.getInstance(durationFieldType0);
DurationFieldType durationFieldType1 = unsupportedDurationField0.getType();
days0.get(durationFieldType1);
assertNotSame(durationFieldType0, durationFieldType1); // (Same) Original Value: false | Regression Value: true


* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'MutableDateTime_ESTest' to evosuite-tests
* Done!

* Computation finished
