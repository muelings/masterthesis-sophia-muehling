* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.MutableDateTime
* Starting client
* Connecting to master process on port 16309
* Analyzing classpath: 
  - ../defects4j_compiled/Time_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.MutableDateTime
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:26:13.411 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:26:13.422 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 151
* Using seed 1593152768229
* Starting evolution
[MASTER] 08:26:15.943 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:615.0 - branchDistance:0.0 - coverage:306.0 - ex: 0 - tex: 4
[MASTER] 08:26:15.944 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 615.0, number of tests: 2, total length: 62
[MASTER] 08:26:16.158 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:562.1 - branchDistance:0.0 - coverage:253.10000000000002 - ex: 0 - tex: 8
[MASTER] 08:26:16.158 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 562.1, number of tests: 5, total length: 50
[MASTER] 08:26:17.599 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:545.1 - branchDistance:0.0 - coverage:236.10000000000002 - ex: 0 - tex: 16
[MASTER] 08:26:17.599 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 545.1, number of tests: 9, total length: 152
[MASTER] 08:26:17.777 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:520.1 - branchDistance:0.0 - coverage:211.1 - ex: 0 - tex: 8
[MASTER] 08:26:17.777 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 520.1, number of tests: 4, total length: 104
[MASTER] 08:26:18.741 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:463.0 - branchDistance:0.0 - coverage:308.0 - ex: 0 - tex: 16
[MASTER] 08:26:18.741 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 463.0, number of tests: 9, total length: 223
* Computation finished
