* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 8637
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 10:39:31.472 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 97
[MASTER] 10:39:31.699 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124L, var2.length());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 10:39:31.732 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 10:39:31.732 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:39:33.063 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 1 assertions.
[MASTER] 10:39:33.095 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124L, var2.length());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 10:39:33.096 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 10:39:33.096 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:39:33.454 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:39:33.475 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124L, var2.length());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 10:39:33.476 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 10:39:33.476 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:39:33.476 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 5 | Tests with assertion: 1
* Generated 1 tests with total length 6
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          2 / 300         
[MASTER] 10:39:34.270 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:39:34.287 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 4 
[MASTER] 10:39:34.305 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {InspectorAssertion=[length]}
[MASTER] 10:39:34.307 [logback-1] WARN  RegressionSuiteMinimizer - [org.evosuite.assertion.InspectorAssertion@dc10f00a] invalid assertion(s) to be removed
[MASTER] 10:39:34.308 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 10:39:34.308 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 10:39:34.310 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 97
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
