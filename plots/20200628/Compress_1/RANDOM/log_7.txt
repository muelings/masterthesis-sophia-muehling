* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 19672
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 97
[MASTER] 10:23:08.609 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:23:10.034 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(125, var1.size());  // (Inspector) Original Value: 125 | Regression Value: 1
[MASTER] 10:23:10.034 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var1.toString());  // (Inspector) Original Value: s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: s
[MASTER] 10:23:10.034 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(125, var1.size());  // (Inspector) Original Value: 125 | Regression Value: 1
[MASTER] 10:23:10.035 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var1.toString());  // (Inspector) Original Value: s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: s
[MASTER] 10:23:10.042 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 10:23:10.043 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 10:23:11.192 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:23:11.227 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(125, var1.size());  // (Inspector) Original Value: 125 | Regression Value: 1
[MASTER] 10:23:11.227 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var1.toString());  // (Inspector) Original Value: s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: s
[MASTER] 10:23:11.227 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(125, var1.size());  // (Inspector) Original Value: 125 | Regression Value: 1
[MASTER] 10:23:11.227 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals("s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!\u0000\u0000\u0000\u0000", var1.toString());  // (Inspector) Original Value: s0707010000000000000000000000000000000000000001ffffffff00000000000000000000000000000000000000000000000b00000000TRAILER!!!     | Regression Value: s
[MASTER] 10:23:11.229 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 10:23:11.229 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 4
[MASTER] 10:23:11.756 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124L, var1.length());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 10:23:11.759 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 10:23:11.759 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:23:12.116 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:23:12.143 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(124L, var1.length());  // (Inspector) Original Value: 124 | Regression Value: 0
[MASTER] 10:23:12.145 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 10:23:12.145 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:23:12.988 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:23:13.247 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:23:13.256 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 10:23:13.493 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/compress/archivers/cpio/CpioArchiveOutputStream_7_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:23:13.499 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:23:13.499 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 257 | Tests with assertion: 1
* Generated 1 tests with total length 3
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                          5 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 10:23:14.245 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:23:14.263 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 10:23:14.267 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 10:23:14.267 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [close:NullPointerException, NullPointerException:writeHeader-176,]
[MASTER] 10:23:14.267 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:NullPointerException at 1
[MASTER] 10:23:14.268 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [close:NullPointerException, NullPointerException:writeHeader-176,]
[MASTER] 10:23:14.288 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
[MASTER] 10:23:14.291 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 25%
* Total number of goals: 97
* Number of covered goals: 24
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 20
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
