* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Starting client
* Connecting to master process on port 12723
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 09:51:09.070 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:51:09.083 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 97
* Using seed 1593071466412
* Starting evolution
[MASTER] 09:51:09.849 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:402.3420634920635 - branchDistance:0.0 - coverage:187.3420634920635 - ex: 0 - tex: 12
[MASTER] 09:51:09.849 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 402.3420634920635, number of tests: 6, total length: 92
[MASTER] 09:51:09.893 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:362.5642857142857 - branchDistance:0.0 - coverage:147.56428571428572 - ex: 0 - tex: 14
[MASTER] 09:51:09.893 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 362.5642857142857, number of tests: 7, total length: 169
[MASTER] 09:51:10.006 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:164.59406628887717 - branchDistance:0.0 - coverage:115.78214285714286 - ex: 0 - tex: 4
[MASTER] 09:51:10.006 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 164.59406628887717, number of tests: 2, total length: 52
[MASTER] 09:51:11.651 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:154.1583520031629 - branchDistance:0.0 - coverage:105.34642857142858 - ex: 0 - tex: 14
[MASTER] 09:51:11.651 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.1583520031629, number of tests: 7, total length: 168
[MASTER] 09:51:12.002 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:152.6583520031629 - branchDistance:0.0 - coverage:103.84642857142858 - ex: 0 - tex: 10
[MASTER] 09:51:12.003 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 152.6583520031629, number of tests: 5, total length: 162
[MASTER] 09:51:12.377 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:144.6583520031629 - branchDistance:0.0 - coverage:95.84642857142858 - ex: 0 - tex: 8
[MASTER] 09:51:12.377 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 144.6583520031629, number of tests: 4, total length: 137
[MASTER] 09:51:22.910 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:141.94049486030576 - branchDistance:0.0 - coverage:93.12857142857143 - ex: 0 - tex: 16
[MASTER] 09:51:22.910 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.94049486030576, number of tests: 9, total length: 184
[MASTER] 09:51:23.176 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:139.24287581268672 - branchDistance:0.0 - coverage:90.43095238095239 - ex: 0 - tex: 8
[MASTER] 09:51:23.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.24287581268672, number of tests: 5, total length: 120
[MASTER] 09:51:23.460 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:125.94049486030576 - branchDistance:0.0 - coverage:77.12857142857143 - ex: 0 - tex: 18
[MASTER] 09:51:23.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.94049486030576, number of tests: 9, total length: 215
[MASTER] 09:51:24.188 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:116.64525676506764 - branchDistance:0.0 - coverage:67.83333333333333 - ex: 0 - tex: 18
[MASTER] 09:51:24.188 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.64525676506764, number of tests: 9, total length: 253
[MASTER] 09:51:25.372 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:115.64525676506764 - branchDistance:0.0 - coverage:66.83333333333333 - ex: 0 - tex: 16
[MASTER] 09:51:25.372 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.64525676506764, number of tests: 8, total length: 217
[MASTER] 09:51:25.830 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:114.64525676506764 - branchDistance:0.0 - coverage:65.83333333333333 - ex: 0 - tex: 16
[MASTER] 09:51:25.830 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.64525676506764, number of tests: 8, total length: 212
[MASTER] 09:51:26.271 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:114.31192343173433 - branchDistance:0.0 - coverage:65.5 - ex: 0 - tex: 18
[MASTER] 09:51:26.271 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.31192343173433, number of tests: 9, total length: 248
[MASTER] 09:51:26.317 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:109.64525676506764 - branchDistance:0.0 - coverage:60.83333333333333 - ex: 0 - tex: 20
[MASTER] 09:51:26.317 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.64525676506764, number of tests: 10, total length: 255
[MASTER] 09:51:26.467 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:108.64525676506764 - branchDistance:0.0 - coverage:59.83333333333333 - ex: 0 - tex: 18
[MASTER] 09:51:26.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.64525676506764, number of tests: 9, total length: 239
[MASTER] 09:51:27.004 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:107.31192343173433 - branchDistance:0.0 - coverage:58.5 - ex: 0 - tex: 20
[MASTER] 09:51:27.004 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.31192343173433, number of tests: 10, total length: 240
[MASTER] 09:51:27.450 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:106.14525676506764 - branchDistance:0.0 - coverage:57.33333333333333 - ex: 0 - tex: 20
[MASTER] 09:51:27.450 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.14525676506764, number of tests: 10, total length: 266
[MASTER] 09:51:27.725 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:104.97859009840099 - branchDistance:0.0 - coverage:56.16666666666667 - ex: 0 - tex: 22
[MASTER] 09:51:27.725 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.97859009840099, number of tests: 11, total length: 257
[MASTER] 09:51:28.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:101.47859009840099 - branchDistance:0.0 - coverage:52.666666666666664 - ex: 0 - tex: 20
[MASTER] 09:51:28.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.47859009840099, number of tests: 10, total length: 272
[MASTER] 09:51:29.201 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:100.47859009840099 - branchDistance:0.0 - coverage:51.666666666666664 - ex: 0 - tex: 20
[MASTER] 09:51:29.202 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.47859009840099, number of tests: 10, total length: 277
[MASTER] 09:51:29.813 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:99.47859009840099 - branchDistance:0.0 - coverage:50.666666666666664 - ex: 0 - tex: 20
[MASTER] 09:51:29.813 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.47859009840099, number of tests: 10, total length: 271
[MASTER] 09:51:30.451 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:98.97859009840099 - branchDistance:0.0 - coverage:50.666666666666664 - ex: 1 - tex: 19
[MASTER] 09:51:30.451 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.97859009840099, number of tests: 10, total length: 235
[MASTER] 09:51:31.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:97.97859009840099 - branchDistance:0.0 - coverage:49.666666666666664 - ex: 1 - tex: 19
[MASTER] 09:51:31.302 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.97859009840099, number of tests: 10, total length: 204
[MASTER] 09:51:33.974 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.4758709677419355 - fitness:96.97859009840099 - branchDistance:0.0 - coverage:48.666666666666664 - ex: 1 - tex: 21
[MASTER] 09:51:33.974 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.97859009840099, number of tests: 11, total length: 222
[MASTER] 09:51:34.295 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:82.98076272641632 - branchDistance:0.0 - coverage:46.166666666666664 - ex: 0 - tex: 18
[MASTER] 09:51:34.295 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.98076272641632, number of tests: 10, total length: 212
[MASTER] 09:51:35.098 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:81.98076272641632 - branchDistance:0.0 - coverage:45.166666666666664 - ex: 0 - tex: 18
[MASTER] 09:51:35.098 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.98076272641632, number of tests: 10, total length: 227
[MASTER] 09:51:35.983 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:81.48076272641632 - branchDistance:0.0 - coverage:44.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:51:35.983 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.48076272641632, number of tests: 10, total length: 203
[MASTER] 09:51:36.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:80.48076272641632 - branchDistance:0.0 - coverage:43.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:51:36.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.48076272641632, number of tests: 10, total length: 192
[MASTER] 09:51:37.545 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:79.48076272641633 - branchDistance:0.0 - coverage:42.66666666666667 - ex: 0 - tex: 20
[MASTER] 09:51:37.545 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.48076272641633, number of tests: 11, total length: 255
[MASTER] 09:51:47.906 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:77.48076272641632 - branchDistance:0.0 - coverage:40.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:51:47.906 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.48076272641632, number of tests: 10, total length: 201
[MASTER] 09:51:48.284 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:76.48076272641632 - branchDistance:0.0 - coverage:39.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:51:48.284 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.48076272641632, number of tests: 10, total length: 196
[MASTER] 09:51:59.356 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:75.48076272641633 - branchDistance:0.0 - coverage:38.66666666666667 - ex: 0 - tex: 20
[MASTER] 09:51:59.356 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.48076272641633, number of tests: 11, total length: 225
[MASTER] 09:52:09.906 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:75.48076272641632 - branchDistance:0.0 - coverage:38.666666666666664 - ex: 0 - tex: 16
[MASTER] 09:52:09.906 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.48076272641632, number of tests: 10, total length: 200
[MASTER] 09:52:10.519 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:74.48076272641633 - branchDistance:0.0 - coverage:37.66666666666667 - ex: 0 - tex: 16
[MASTER] 09:52:10.519 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.48076272641633, number of tests: 10, total length: 192
[MASTER] 09:52:11.533 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:74.48076272641632 - branchDistance:0.0 - coverage:37.666666666666664 - ex: 0 - tex: 16
[MASTER] 09:52:11.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.48076272641632, number of tests: 10, total length: 178
[MASTER] 09:52:21.809 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:73.48076272641633 - branchDistance:0.0 - coverage:36.66666666666667 - ex: 0 - tex: 18
[MASTER] 09:52:21.809 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.48076272641633, number of tests: 11, total length: 224
[MASTER] 09:52:32.674 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:72.48076272641632 - branchDistance:0.0 - coverage:35.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:52:32.674 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.48076272641632, number of tests: 11, total length: 200
[MASTER] 09:52:43.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.975300888314417 - fitness:71.48076272641632 - branchDistance:0.0 - coverage:34.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:52:43.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.48076272641632, number of tests: 11, total length: 214
[MASTER] 09:52:44.517 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.748878923766771 - fitness:70.37563676633465 - branchDistance:0.0 - coverage:37.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:52:44.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.37563676633465, number of tests: 11, total length: 210
[MASTER] 09:52:46.395 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.748878923766771 - fitness:69.37563676633465 - branchDistance:0.0 - coverage:36.666666666666664 - ex: 0 - tex: 20
[MASTER] 09:52:46.395 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.37563676633465, number of tests: 12, total length: 244
[MASTER] 09:53:02.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.748878923766771 - fitness:68.37563676633465 - branchDistance:0.0 - coverage:35.666666666666664 - ex: 0 - tex: 18
[MASTER] 09:53:02.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.37563676633465, number of tests: 12, total length: 237
[MASTER] 09:53:07.660 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.748878923766771 - fitness:67.37563676633465 - branchDistance:0.0 - coverage:34.666666666666664 - ex: 0 - tex: 20
[MASTER] 09:53:07.660 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.37563676633465, number of tests: 12, total length: 230
[MASTER] 09:53:08.311 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.799259024261822 - fitness:63.10517067509771 - branchDistance:0.0 - coverage:34.666666666666664 - ex: 0 - tex: 20
[MASTER] 09:53:08.311 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.10517067509771, number of tests: 12, total length: 232
[MASTER] 09:53:09.733 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.799259024261822 - fitness:62.10517067509771 - branchDistance:0.0 - coverage:34.166666666666664 - ex: 1 - tex: 19
[MASTER] 09:53:09.734 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.10517067509771, number of tests: 12, total length: 221
[MASTER] 09:53:20.167 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.832811420350669 - fitness:59.89451325093168 - branchDistance:0.0 - coverage:34.666666666666664 - ex: 0 - tex: 20
[MASTER] 09:53:20.167 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.89451325093168, number of tests: 12, total length: 232
[MASTER] 09:53:20.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.832811420350669 - fitness:58.89451325093168 - branchDistance:0.0 - coverage:34.166666666666664 - ex: 1 - tex: 17
[MASTER] 09:53:20.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 58.89451325093168, number of tests: 12, total length: 217
[MASTER] 09:53:22.223 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.832789751372686 - fitness:57.8945726877222 - branchDistance:0.0 - coverage:33.166666666666664 - ex: 1 - tex: 21
[MASTER] 09:53:22.223 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.8945726877222, number of tests: 12, total length: 222
[MASTER] 09:53:23.145 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.832811420350669 - fitness:57.89451325093168 - branchDistance:0.0 - coverage:33.166666666666664 - ex: 1 - tex: 21
[MASTER] 09:53:23.145 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.89451325093168, number of tests: 13, total length: 237
[MASTER] 09:53:23.710 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.832789751372686 - fitness:57.64457268772222 - branchDistance:0.0 - coverage:32.41666666666667 - ex: 0 - tex: 22
[MASTER] 09:53:23.710 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.64457268772222, number of tests: 12, total length: 225
[MASTER] 09:53:24.042 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.832811420350669 - fitness:57.644513250931695 - branchDistance:0.0 - coverage:32.41666666666667 - ex: 0 - tex: 20
[MASTER] 09:53:24.042 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.644513250931695, number of tests: 12, total length: 213
[MASTER] 09:53:37.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.832811420350669 - fitness:56.64451325093169 - branchDistance:0.0 - coverage:31.416666666666668 - ex: 0 - tex: 22
[MASTER] 09:53:37.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.64451325093169, number of tests: 13, total length: 226
[MASTER] 09:53:47.996 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:55.12766817403018 - branchDistance:0.0 - coverage:32.41666666666667 - ex: 0 - tex: 20
[MASTER] 09:53:47.997 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.12766817403018, number of tests: 12, total length: 214
[MASTER] 09:54:00.004 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:54.12766817403018 - branchDistance:0.0 - coverage:31.416666666666668 - ex: 0 - tex: 22
[MASTER] 09:54:00.004 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.12766817403018, number of tests: 13, total length: 227
[MASTER] 09:54:02.104 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:53.12766817403018 - branchDistance:0.0 - coverage:30.416666666666668 - ex: 0 - tex: 24
[MASTER] 09:54:02.104 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.12766817403018, number of tests: 14, total length: 233
[MASTER] 09:54:06.685 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:52.12766817403018 - branchDistance:0.0 - coverage:29.416666666666668 - ex: 0 - tex: 24
[MASTER] 09:54:06.685 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.12766817403018, number of tests: 13, total length: 274
[MASTER] 09:54:18.404 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:51.62766817403018 - branchDistance:0.0 - coverage:29.416666666666668 - ex: 1 - tex: 23
[MASTER] 09:54:18.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.62766817403018, number of tests: 14, total length: 254
[MASTER] 09:54:20.651 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:49.12766817403018 - branchDistance:0.0 - coverage:26.416666666666668 - ex: 0 - tex: 26
[MASTER] 09:54:20.651 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.12766817403018, number of tests: 14, total length: 283
[MASTER] 09:54:22.188 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:48.62766817403018 - branchDistance:0.0 - coverage:26.416666666666668 - ex: 1 - tex: 25
[MASTER] 09:54:22.189 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.62766817403018, number of tests: 14, total length: 284
[MASTER] 09:54:23.217 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:48.12766817403018 - branchDistance:0.0 - coverage:25.416666666666668 - ex: 0 - tex: 26
[MASTER] 09:54:23.217 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.12766817403018, number of tests: 14, total length: 282
[MASTER] 09:54:23.867 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:47.62766817403018 - branchDistance:0.0 - coverage:25.416666666666668 - ex: 1 - tex: 25
[MASTER] 09:54:23.867 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.62766817403018, number of tests: 14, total length: 280
[MASTER] 09:54:25.796 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.85675395616456 - fitness:47.12766817403018 - branchDistance:0.0 - coverage:24.416666666666668 - ex: 0 - tex: 28
[MASTER] 09:54:25.796 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.12766817403018, number of tests: 15, total length: 322
[MASTER] 09:54:27.139 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:46.097055435903634 - branchDistance:0.0 - coverage:25.416666666666668 - ex: 0 - tex: 26
[MASTER] 09:54:27.139 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.097055435903634, number of tests: 14, total length: 284
[MASTER] 09:54:28.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:45.097055435903634 - branchDistance:0.0 - coverage:24.416666666666668 - ex: 0 - tex: 28
[MASTER] 09:54:28.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.097055435903634, number of tests: 15, total length: 324
[MASTER] 09:54:44.737 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:44.597055435903634 - branchDistance:0.0 - coverage:24.416666666666668 - ex: 1 - tex: 27
[MASTER] 09:54:44.737 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 44.597055435903634, number of tests: 15, total length: 327
[MASTER] 09:54:46.020 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:40.18038876923697 - branchDistance:0.0 - coverage:20.0 - ex: 1 - tex: 27
[MASTER] 09:54:46.020 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.18038876923697, number of tests: 16, total length: 343
[MASTER] 09:54:49.660 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:40.013722102570306 - branchDistance:0.0 - coverage:20.0 - ex: 2 - tex: 26
[MASTER] 09:54:49.660 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.013722102570306, number of tests: 16, total length: 340
[MASTER] 09:55:14.012 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:39.93038876923697 - branchDistance:0.0 - coverage:20.0 - ex: 3 - tex: 27
[MASTER] 09:55:14.012 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.93038876923697, number of tests: 16, total length: 342
[MASTER] 09:55:27.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:38.59705543590363 - branchDistance:0.0 - coverage:18.666666666666664 - ex: 3 - tex: 29
[MASTER] 09:55:27.800 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.59705543590363, number of tests: 17, total length: 379
[MASTER] 09:55:31.547 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:37.59705543590363 - branchDistance:0.0 - coverage:17.666666666666664 - ex: 3 - tex: 31
[MASTER] 09:55:31.547 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 37.59705543590363, number of tests: 18, total length: 385
[MASTER] 09:55:33.251 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.887870537320115 - fitness:36.91820860205201 - branchDistance:0.0 - coverage:18.666666666666664 - ex: 3 - tex: 29
[MASTER] 09:55:33.251 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.91820860205201, number of tests: 18, total length: 378
[MASTER] 09:55:33.281 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:36.84705543590363 - branchDistance:0.0 - coverage:16.666666666666664 - ex: 1 - tex: 35
[MASTER] 09:55:33.281 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.84705543590363, number of tests: 19, total length: 436
[MASTER] 09:55:33.433 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.873768933594956 - fitness:35.597055435903634 - branchDistance:0.0 - coverage:15.666666666666666 - ex: 3 - tex: 35
[MASTER] 09:55:33.433 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.597055435903634, number of tests: 20, total length: 432
[MASTER] 09:55:55.142 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.88791025080843 - fitness:35.00148179832087 - branchDistance:0.0 - coverage:16.666666666666664 - ex: 2 - tex: 36
[MASTER] 09:55:55.142 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.00148179832087, number of tests: 20, total length: 433
[MASTER] 09:55:55.672 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.88791025080843 - fitness:34.00148179832087 - branchDistance:0.0 - coverage:15.666666666666666 - ex: 2 - tex: 36
[MASTER] 09:55:55.672 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.00148179832087, number of tests: 20, total length: 435

* Search finished after 302s and 248 generations, 215353 statements, best individuals have fitness: 34.00148179832087
[MASTER] 09:56:10.108 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:56:10.113 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 87%
* Total number of goals: 97
* Number of covered goals: 84
* Generated 20 tests with total length 416
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     34 / 0           
[MASTER] 09:56:10.430 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:56:10.502 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 328 
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 1.0
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test4, uniqueExceptions: [close:MockIOException, MockIOException,]
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:MockIOException at 8
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 1.0
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: [close:MockIOException, finish:MockIOException, MockIOException,]
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: finish:MockIOException at 12
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: [close:MockIOException, finish:MockIOException, MockIOException,]
[MASTER] 09:56:10.633 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:MockIOException at 3
[MASTER] 09:56:10.634 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 3
[MASTER] 09:56:10.639 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 09:56:10.639 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: [close:MockIOException, finish:MockIOException, MockIOException,]
[MASTER] 09:56:10.639 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:MockIOException at 4
[MASTER] 09:56:10.639 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 4
[MASTER] 09:56:10.645 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 09:56:10.645 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: [close:MockIOException, finish:MockIOException, MockIOException,]
[MASTER] 09:56:10.645 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: close:MockIOException at 4
[MASTER] 09:56:10.645 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 4
[MASTER] 09:56:10.650 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.650 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.651 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.652 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.652 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.653 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.653 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Test16 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Test17 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Test18 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Test19 - Difference in number of exceptions: 0.0
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [close:MockIOException, finish:MockIOException, MockIOException,]
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:56:10.654 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 09:56:10.655 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 19: no assertions
[MASTER] 09:56:10.908 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:56:10.909 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 41%
* Total number of goals: 97
* Number of covered goals: 40
* Generated 2 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 291
* Writing JUnit test case 'CpioArchiveOutputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
