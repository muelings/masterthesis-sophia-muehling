* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.fasterxml.jackson.core.JsonPointer
* Starting client
* Connecting to master process on port 7949
* Analyzing classpath: 
  - ../defects4j_compiled/JacksonCore_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.fasterxml.jackson.core.JsonPointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 22:47:09.495 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:47:14.392 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:47:15.584 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:47:15.596 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 22:47:15.860 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'com/fasterxml/jackson/core/JsonPointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 22:47:15.866 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 22:47:15.867 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 862 | Tests with assertion: 1
* Generated 1 tests with total length 4
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          6 / 300         
	- RMIStoppingCondition
[MASTER] 22:47:16.393 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 22:47:16.409 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 2 
[MASTER] 22:47:16.415 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 22:47:16.415 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NumberFormatException]
[MASTER] 22:47:16.415 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NumberFormatException at 1
[MASTER] 22:47:16.415 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NumberFormatException]
[MASTER] 22:47:16.432 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 22:47:16.437 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 10%
* Total number of goals: 70
* Number of covered goals: 7
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'JsonPointer_ESTest' to evosuite-tests
* Done!

* Computation finished
