* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.Lexer
* Starting client
* Connecting to master process on port 17277
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.Lexer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 70
[MASTER] 14:54:16.419 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:54:16.773 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var8);  // (Primitive) Original Value: -1 | Regression Value: 56
[MASTER] 14:54:16.794 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:54:16.794 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:54:17.939 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/Lexer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:54:17.955 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var8);  // (Primitive) Original Value: -1 | Regression Value: 56
[MASTER] 14:54:17.956 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:54:17.957 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:54:18.262 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var21);  // (Primitive) Original Value: -1 | Regression Value: 119
[MASTER] 14:54:18.266 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:54:18.266 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:54:18.610 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/Lexer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:54:18.639 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var21);  // (Primitive) Original Value: -1 | Regression Value: 119
[MASTER] 14:54:18.648 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
Generated test with 1 assertions.
[MASTER] 14:54:18.648 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 14:54:18.968 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/csv/Lexer_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 14:54:18.988 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals((-1), var21);  // (Primitive) Original Value: -1 | Regression Value: 119
[MASTER] 14:54:18.991 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 14:54:18.991 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 14:54:18.991 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 13 | Tests with assertion: 1
* Generated 1 tests with total length 22
* GA-Budget:
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 14:54:19.529 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:54:19.546 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 16 
[MASTER] 14:54:19.559 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 14:54:19.883 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:54:19.888 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 31%
* Total number of goals: 70
* Number of covered goals: 22
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Lexer_ESTest' to evosuite-tests
* Done!

* Computation finished
