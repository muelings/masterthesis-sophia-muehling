* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.Lexer
* Starting client
* Connecting to master process on port 18863
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.Lexer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 15:26:01.957 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 15:26:01.962 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 70
* Using seed 1593091559680
* Starting evolution
[MASTER] 15:26:03.502 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:301.0 - branchDistance:0.0 - coverage:146.0 - ex: 0 - tex: 2
[MASTER] 15:26:03.502 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 301.0, number of tests: 1, total length: 34
[MASTER] 15:26:04.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:297.0 - branchDistance:0.0 - coverage:142.0 - ex: 0 - tex: 8
[MASTER] 15:26:04.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 297.0, number of tests: 5, total length: 135
[MASTER] 15:26:04.482 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.026171391148605 - fitness:104.44592332904867 - branchDistance:0.0 - coverage:90.64051793516366 - ex: 0 - tex: 16
[MASTER] 15:26:04.482 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.44592332904867, number of tests: 10, total length: 224
[MASTER] 15:26:05.372 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.975609756097561 - fitness:104.15487702039388 - branchDistance:0.0 - coverage:51.400778659738144 - ex: 0 - tex: 6
[MASTER] 15:26:05.372 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.15487702039388, number of tests: 7, total length: 180
[MASTER] 15:26:08.316 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.991150442477876 - fitness:103.89881319430236 - branchDistance:0.0 - coverage:51.41360609371065 - ex: 0 - tex: 12
[MASTER] 15:26:08.316 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.89881319430236, number of tests: 7, total length: 144
[MASTER] 15:26:20.057 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.026171391148605 - fitness:102.44244506817911 - branchDistance:0.0 - coverage:88.6370396742941 - ex: 0 - tex: 16
[MASTER] 15:26:20.057 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.44244506817911, number of tests: 11, total length: 249
[MASTER] 15:26:20.334 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.991150442477876 - fitness:101.84881319430237 - branchDistance:0.0 - coverage:49.363606093710644 - ex: 0 - tex: 12
[MASTER] 15:26:20.334 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.84881319430237, number of tests: 7, total length: 137
[MASTER] 15:26:21.074 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.9901960784313726 - fitness:100.3680757020222 - branchDistance:0.0 - coverage:47.866436357759895 - ex: 0 - tex: 6
[MASTER] 15:26:21.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.3680757020222, number of tests: 6, total length: 170
[MASTER] 15:26:21.213 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.026171391148605 - fitness:68.20146359805793 - branchDistance:0.0 - coverage:54.39605820417291 - ex: 0 - tex: 14
[MASTER] 15:26:21.213 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.20146359805793, number of tests: 9, total length: 195
[MASTER] 15:26:23.945 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.617406312212006 - fitness:62.944881553514776 - branchDistance:0.0 - coverage:49.73952046825369 - ex: 0 - tex: 16
[MASTER] 15:26:23.945 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.944881553514776, number of tests: 10, total length: 224
[MASTER] 15:26:24.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.026171391148605 - fitness:61.522734731214356 - branchDistance:0.0 - coverage:47.71732933732934 - ex: 0 - tex: 12
[MASTER] 15:26:24.884 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.522734731214356, number of tests: 8, total length: 153
[MASTER] 15:26:25.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.026171391148605 - fitness:60.43190556038519 - branchDistance:0.0 - coverage:46.62650016650017 - ex: 0 - tex: 12
[MASTER] 15:26:25.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.43190556038519, number of tests: 8, total length: 190
[MASTER] 15:26:28.101 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.026171391148605 - fitness:60.431905560385175 - branchDistance:0.0 - coverage:46.626500166500165 - ex: 0 - tex: 10
[MASTER] 15:26:28.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 60.431905560385175, number of tests: 8, total length: 191
[MASTER] 15:26:31.449 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.026171391148605 - fitness:59.58245077259802 - branchDistance:0.0 - coverage:45.77704537871301 - ex: 0 - tex: 14
[MASTER] 15:26:31.449 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 59.58245077259802, number of tests: 9, total length: 207
[MASTER] 15:26:31.477 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.617406312212006 - fitness:57.599343769243774 - branchDistance:0.0 - coverage:44.393982683982685 - ex: 0 - tex: 10
[MASTER] 15:26:31.477 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.599343769243774, number of tests: 8, total length: 186
[MASTER] 15:26:32.666 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.728279837198148 - fitness:57.55393247302039 - branchDistance:0.0 - coverage:44.4548898710548 - ex: 0 - tex: 12
[MASTER] 15:26:32.666 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.55393247302039, number of tests: 9, total length: 198
[MASTER] 15:26:32.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.728279837198148 - fitness:56.92052112344411 - branchDistance:0.0 - coverage:43.821478521478525 - ex: 0 - tex: 12
[MASTER] 15:26:32.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.92052112344411, number of tests: 8, total length: 169
[MASTER] 15:26:33.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.617406312212006 - fitness:54.69648662638663 - branchDistance:0.0 - coverage:41.49112554112554 - ex: 0 - tex: 10
[MASTER] 15:26:33.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.69648662638663, number of tests: 8, total length: 205
[MASTER] 15:26:35.263 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.617406312212006 - fitness:53.64517294007294 - branchDistance:0.0 - coverage:40.43981185481185 - ex: 0 - tex: 12
[MASTER] 15:26:35.263 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.64517294007294, number of tests: 8, total length: 187
[MASTER] 15:26:35.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:52.94972433234241 - branchDistance:0.0 - coverage:44.38661505161505 - ex: 0 - tex: 10
[MASTER] 15:26:35.932 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.94972433234241, number of tests: 8, total length: 213
[MASTER] 15:26:39.020 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.617406312212006 - fitness:52.64886757876758 - branchDistance:0.0 - coverage:39.44350649350649 - ex: 0 - tex: 10
[MASTER] 15:26:39.020 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.64886757876758, number of tests: 9, total length: 220
[MASTER] 15:26:39.760 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:52.33258147519956 - branchDistance:0.0 - coverage:43.7694721944722 - ex: 0 - tex: 12
[MASTER] 15:26:39.760 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.33258147519956, number of tests: 8, total length: 229
[MASTER] 15:26:40.611 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:51.59392326187515 - branchDistance:0.0 - coverage:43.03081398114779 - ex: 0 - tex: 14
[MASTER] 15:26:40.611 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 51.59392326187515, number of tests: 8, total length: 203
[MASTER] 15:26:42.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:50.67137767899575 - branchDistance:0.0 - coverage:42.10826839826839 - ex: 0 - tex: 12
[MASTER] 15:26:42.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.67137767899575, number of tests: 9, total length: 242
[MASTER] 15:26:42.740 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:50.10006399268207 - branchDistance:0.0 - coverage:41.53695471195471 - ex: 0 - tex: 12
[MASTER] 15:26:42.741 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.10006399268207, number of tests: 8, total length: 211
[MASTER] 15:26:43.396 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:49.033397326015404 - branchDistance:0.0 - coverage:40.470288045288044 - ex: 0 - tex: 14
[MASTER] 15:26:43.397 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 49.033397326015404, number of tests: 8, total length: 207
[MASTER] 15:26:44.622 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:48.33994910756718 - branchDistance:0.0 - coverage:39.77683982683982 - ex: 0 - tex: 12
[MASTER] 15:26:44.622 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 48.33994910756718, number of tests: 9, total length: 231
[MASTER] 15:26:46.676 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:47.25661577423385 - branchDistance:0.0 - coverage:38.69350649350649 - ex: 0 - tex: 10
[MASTER] 15:26:46.676 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.25661577423385, number of tests: 8, total length: 222
[MASTER] 15:26:48.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:46.87566339328147 - branchDistance:0.0 - coverage:38.31255411255411 - ex: 0 - tex: 10
[MASTER] 15:26:48.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.87566339328147, number of tests: 9, total length: 262
[MASTER] 15:27:02.060 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:46.80899672661481 - branchDistance:0.0 - coverage:38.24588744588745 - ex: 0 - tex: 8
[MASTER] 15:27:02.060 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.80899672661481, number of tests: 9, total length: 254
[MASTER] 15:27:02.522 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:46.7089967266148 - branchDistance:0.0 - coverage:38.14588744588744 - ex: 0 - tex: 8
[MASTER] 15:27:02.522 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 46.7089967266148, number of tests: 9, total length: 251
[MASTER] 15:27:05.719 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:45.92328244090052 - branchDistance:0.0 - coverage:37.360173160173154 - ex: 0 - tex: 8
[MASTER] 15:27:05.719 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.92328244090052, number of tests: 9, total length: 246
[MASTER] 15:27:07.949 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:45.75661577423385 - branchDistance:0.0 - coverage:37.19350649350649 - ex: 0 - tex: 8
[MASTER] 15:27:07.949 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 45.75661577423385, number of tests: 9, total length: 248
[MASTER] 15:27:11.533 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:40.72328244090052 - branchDistance:0.0 - coverage:32.16017316017316 - ex: 0 - tex: 8
[MASTER] 15:27:11.533 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.72328244090052, number of tests: 9, total length: 252
[MASTER] 15:27:12.887 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.369483965338716 - fitness:40.72050214357061 - branchDistance:0.0 - coverage:32.16017316017316 - ex: 0 - tex: 8
[MASTER] 15:27:12.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.72050214357061, number of tests: 9, total length: 255
[MASTER] 15:27:14.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:40.55661577423385 - branchDistance:0.0 - coverage:31.993506493506487 - ex: 0 - tex: 8
[MASTER] 15:27:14.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.55661577423385, number of tests: 9, total length: 254
[MASTER] 15:27:18.759 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:40.22328244090052 - branchDistance:0.0 - coverage:31.660173160173155 - ex: 0 - tex: 8
[MASTER] 15:27:18.759 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.22328244090052, number of tests: 9, total length: 256
[MASTER] 15:27:22.438 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:40.00899672661481 - branchDistance:0.0 - coverage:31.445887445887443 - ex: 0 - tex: 8
[MASTER] 15:27:22.438 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 40.00899672661481, number of tests: 10, total length: 263
[MASTER] 15:27:30.566 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:39.97982589744397 - branchDistance:0.0 - coverage:31.416716616716613 - ex: 0 - tex: 8
[MASTER] 15:27:30.567 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 39.97982589744397, number of tests: 10, total length: 264
[MASTER] 15:27:35.099 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:37.83367205129014 - branchDistance:0.0 - coverage:29.27056277056277 - ex: 0 - tex: 8
[MASTER] 15:27:35.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 37.83367205129014, number of tests: 10, total length: 258
[MASTER] 15:27:44.553 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:36.500338717956794 - branchDistance:0.0 - coverage:27.937229437229433 - ex: 0 - tex: 8
[MASTER] 15:27:44.553 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.500338717956794, number of tests: 10, total length: 260
[MASTER] 15:27:48.985 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:36.167005384623465 - branchDistance:0.0 - coverage:27.603896103896105 - ex: 0 - tex: 8
[MASTER] 15:27:48.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 36.167005384623465, number of tests: 10, total length: 260
[MASTER] 15:27:50.263 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:35.83367205129014 - branchDistance:0.0 - coverage:27.27056277056277 - ex: 0 - tex: 8
[MASTER] 15:27:50.263 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 35.83367205129014, number of tests: 11, total length: 268
[MASTER] 15:27:53.323 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:34.500338717956794 - branchDistance:0.0 - coverage:25.937229437229433 - ex: 0 - tex: 8
[MASTER] 15:27:53.323 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.500338717956794, number of tests: 10, total length: 234
[MASTER] 15:27:57.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:34.33367205129014 - branchDistance:0.0 - coverage:25.77056277056277 - ex: 0 - tex: 10
[MASTER] 15:27:57.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 34.33367205129014, number of tests: 12, total length: 325
[MASTER] 15:27:58.152 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:33.500338717956794 - branchDistance:0.0 - coverage:24.937229437229433 - ex: 0 - tex: 8
[MASTER] 15:27:58.152 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.500338717956794, number of tests: 11, total length: 275
[MASTER] 15:28:03.215 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:33.33367205129014 - branchDistance:0.0 - coverage:24.77056277056277 - ex: 0 - tex: 8
[MASTER] 15:28:03.215 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.33367205129014, number of tests: 12, total length: 331
[MASTER] 15:28:03.822 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:33.167005384623465 - branchDistance:0.0 - coverage:24.603896103896105 - ex: 0 - tex: 8
[MASTER] 15:28:03.822 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.167005384623465, number of tests: 11, total length: 278
[MASTER] 15:28:08.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:33.000338717956794 - branchDistance:0.0 - coverage:24.437229437229433 - ex: 0 - tex: 8
[MASTER] 15:28:08.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.000338717956794, number of tests: 11, total length: 279
[MASTER] 15:28:10.766 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:32.667005384623465 - branchDistance:0.0 - coverage:24.103896103896105 - ex: 0 - tex: 8
[MASTER] 15:28:10.766 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.667005384623465, number of tests: 11, total length: 267
[MASTER] 15:28:16.074 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:32.5003387179568 - branchDistance:0.0 - coverage:23.937229437229437 - ex: 0 - tex: 10
[MASTER] 15:28:16.075 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.5003387179568, number of tests: 13, total length: 358
[MASTER] 15:28:25.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:32.45271967033776 - branchDistance:0.0 - coverage:23.88961038961039 - ex: 0 - tex: 8
[MASTER] 15:28:25.820 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 32.45271967033776, number of tests: 11, total length: 295
[MASTER] 15:28:33.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:31.833672051290133 - branchDistance:0.0 - coverage:23.27056277056277 - ex: 0 - tex: 8
[MASTER] 15:28:33.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.833672051290133, number of tests: 11, total length: 291
[MASTER] 15:28:34.632 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:31.786053003671086 - branchDistance:0.0 - coverage:23.22294372294372 - ex: 0 - tex: 8
[MASTER] 15:28:34.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.786053003671086, number of tests: 11, total length: 296
[MASTER] 15:28:41.987 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:31.71938633700442 - branchDistance:0.0 - coverage:23.156277056277055 - ex: 0 - tex: 8
[MASTER] 15:28:41.987 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.71938633700442, number of tests: 12, total length: 298
[MASTER] 15:28:43.101 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:31.40581582114512 - branchDistance:0.0 - coverage:23.22294372294372 - ex: 0 - tex: 10
[MASTER] 15:28:43.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.40581582114512, number of tests: 11, total length: 295
[MASTER] 15:28:48.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:31.405100622718702 - branchDistance:0.0 - coverage:22.84199134199134 - ex: 0 - tex: 10
[MASTER] 15:28:48.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.405100622718702, number of tests: 12, total length: 329
[MASTER] 15:28:51.222 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:31.33914915447845 - branchDistance:0.0 - coverage:23.156277056277055 - ex: 0 - tex: 8
[MASTER] 15:28:51.222 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.33914915447845, number of tests: 12, total length: 298
[MASTER] 15:28:55.166 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:31.024863440192732 - branchDistance:0.0 - coverage:22.84199134199134 - ex: 0 - tex: 10
[MASTER] 15:28:55.166 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 31.024863440192732, number of tests: 12, total length: 328
[MASTER] 15:28:59.966 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:30.201842477355296 - branchDistance:0.0 - coverage:21.638733196627932 - ex: 0 - tex: 10
[MASTER] 15:28:59.967 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.201842477355296, number of tests: 13, total length: 305
[MASTER] 15:29:05.959 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:29.888271961495995 - branchDistance:0.0 - coverage:21.705399863294602 - ex: 0 - tex: 8
[MASTER] 15:29:05.959 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.888271961495995, number of tests: 12, total length: 321
[MASTER] 15:29:08.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:29.887556763069583 - branchDistance:0.0 - coverage:21.32444748234222 - ex: 0 - tex: 12
[MASTER] 15:29:08.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.887556763069583, number of tests: 13, total length: 330
[MASTER] 15:29:10.239 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:29.507319580543616 - branchDistance:0.0 - coverage:21.32444748234222 - ex: 0 - tex: 12
[MASTER] 15:29:10.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 29.507319580543616, number of tests: 12, total length: 343
[MASTER] 15:29:10.556 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.361995878127708 - fitness:28.868509144021967 - branchDistance:0.0 - coverage:20.305399863294603 - ex: 0 - tex: 8
[MASTER] 15:29:10.556 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.868509144021967, number of tests: 14, total length: 335
[MASTER] 15:29:16.172 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.43989171665216 - fitness:28.83968635242203 - branchDistance:0.0 - coverage:20.305399863294603 - ex: 0 - tex: 12
[MASTER] 15:29:16.172 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.83968635242203, number of tests: 15, total length: 373
[MASTER] 15:29:16.241 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:28.488271961495997 - branchDistance:0.0 - coverage:20.305399863294603 - ex: 0 - tex: 8
[MASTER] 15:29:16.241 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.488271961495997, number of tests: 13, total length: 318
[MASTER] 15:29:23.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:28.33914915447845 - branchDistance:0.0 - coverage:20.156277056277055 - ex: 0 - tex: 10
[MASTER] 15:29:23.904 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.33914915447845, number of tests: 14, total length: 351
[MASTER] 15:29:30.624 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:28.172482487811784 - branchDistance:0.0 - coverage:19.98961038961039 - ex: 0 - tex: 10
[MASTER] 15:29:30.624 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 28.172482487811784, number of tests: 14, total length: 352
[MASTER] 15:29:38.875 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:27.572482487811783 - branchDistance:0.0 - coverage:19.38961038961039 - ex: 0 - tex: 10
[MASTER] 15:29:38.875 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.572482487811783, number of tests: 14, total length: 356
[MASTER] 15:29:52.981 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:27.239149154478454 - branchDistance:0.0 - coverage:19.056277056277057 - ex: 0 - tex: 8
[MASTER] 15:29:52.981 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.239149154478454, number of tests: 14, total length: 345
[MASTER] 15:30:43.071 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:27.12486344019274 - branchDistance:0.0 - coverage:18.941991341991343 - ex: 0 - tex: 8
[MASTER] 15:30:43.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.12486344019274, number of tests: 13, total length: 288
[MASTER] 15:30:43.399 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:26.239149154478454 - branchDistance:0.0 - coverage:18.056277056277057 - ex: 0 - tex: 8
[MASTER] 15:30:43.399 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.239149154478454, number of tests: 14, total length: 301
[MASTER] 15:30:50.084 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:26.12486344019274 - branchDistance:0.0 - coverage:17.941991341991343 - ex: 0 - tex: 8
[MASTER] 15:30:50.084 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.12486344019274, number of tests: 13, total length: 303
[MASTER] 15:30:51.373 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:25.52486344019274 - branchDistance:0.0 - coverage:17.341991341991346 - ex: 0 - tex: 8
[MASTER] 15:30:51.373 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 25.52486344019274, number of tests: 14, total length: 321
[MASTER] 15:31:02.473 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.50523262574307 - fitness:25.22636343312673 - branchDistance:0.0 - coverage:17.941991341991343 - ex: 0 - tex: 8
[MASTER] 15:31:02.473 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 25.22636343312673, number of tests: 14, total length: 294
[MASTER] 15:31:02.798 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.43989171665216 - fitness:24.52486344019274 - branchDistance:0.0 - coverage:16.341991341991346 - ex: 0 - tex: 10
[MASTER] 15:31:02.798 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.52486344019274, number of tests: 15, total length: 332
[MASTER] 15:31:03.080 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 155 generations, 199445 statements, best individuals have fitness: 24.52486344019274
[MASTER] 15:31:03.085 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 81%
* Total number of goals: 70
* Number of covered goals: 57
* Generated 15 tests with total length 332
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :                     25 / 0           
[MASTER] 15:31:04.021 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 15:31:04.406 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 259 
[MASTER] 15:31:04.577 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 15:31:04.577 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 15:31:04.578 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 15:31:04.578 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 15:31:04.579 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 15:31:04.580 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 70
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Lexer_ESTest' to evosuite-tests
* Done!

* Computation finished
