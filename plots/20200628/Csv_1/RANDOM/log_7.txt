* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 7661
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 13:10:21.202 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
*** Random test generation finished.
[MASTER] 13:15:22.216 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 77884 | Tests with assertion: 0
* Generated 0 tests with total length 0
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 13:15:22.253 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:15:22.253 [logback-1] WARN  TimeController - Phase INITIALIZATION lasted too long, 183 seconds more than allowed.
[MASTER] 13:15:22.260 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 13:15:22.260 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 13:15:22.262 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
