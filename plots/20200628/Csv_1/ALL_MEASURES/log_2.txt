* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 9171
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 12:24:28.828 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 12:24:28.832 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 30
* Using seed 1593080666713
* Starting evolution
[MASTER] 12:24:29.952 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:101.33333333333334 - branchDistance:0.0 - coverage:38.333333333333336 - ex: 0 - tex: 4
[MASTER] 12:24:29.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.33333333333334, number of tests: 3, total length: 90
[MASTER] 12:24:30.895 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:98.0 - branchDistance:0.0 - coverage:35.0 - ex: 0 - tex: 6
[MASTER] 12:24:30.895 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.0, number of tests: 5, total length: 97
[MASTER] 12:24:32.207 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:84.25378908320084 - branchDistance:0.0 - coverage:21.253789083200846 - ex: 0 - tex: 12
[MASTER] 12:24:32.207 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.25378908320084, number of tests: 8, total length: 193
[MASTER] 12:24:39.098 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.45378908320085 - branchDistance:0.0 - coverage:20.45378908320085 - ex: 0 - tex: 6
[MASTER] 12:24:39.098 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.45378908320085, number of tests: 6, total length: 134
[MASTER] 12:24:44.108 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.65378908320085 - branchDistance:0.0 - coverage:19.65378908320085 - ex: 0 - tex: 10
[MASTER] 12:24:44.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.65378908320085, number of tests: 8, total length: 197
[MASTER] 12:24:44.626 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.54444444444445 - branchDistance:0.0 - coverage:19.544444444444444 - ex: 0 - tex: 10
[MASTER] 12:24:44.626 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.54444444444445, number of tests: 6, total length: 185
[MASTER] 12:24:49.502 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.54267797208973 - branchDistance:0.0 - coverage:19.542677972089738 - ex: 0 - tex: 8
[MASTER] 12:24:49.502 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.54267797208973, number of tests: 8, total length: 215
[MASTER] 12:24:51.205 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.43156686097862 - branchDistance:0.0 - coverage:19.431566860978624 - ex: 0 - tex: 10
[MASTER] 12:24:51.205 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.43156686097862, number of tests: 8, total length: 219
[MASTER] 12:24:54.574 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.35748792270532 - branchDistance:0.0 - coverage:19.357487922705314 - ex: 0 - tex: 12
[MASTER] 12:24:54.574 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.35748792270532, number of tests: 7, total length: 220
[MASTER] 12:25:01.989 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.26776556776557 - branchDistance:0.0 - coverage:19.26776556776557 - ex: 0 - tex: 6
[MASTER] 12:25:01.989 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.26776556776557, number of tests: 8, total length: 175
[MASTER] 12:25:02.934 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.98712241653418 - branchDistance:0.0 - coverage:18.98712241653418 - ex: 0 - tex: 8
[MASTER] 12:25:02.934 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.98712241653418, number of tests: 8, total length: 209
[MASTER] 12:25:12.843 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 12
[MASTER] 12:25:12.843 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 7, total length: 202

* Search finished after 301s and 141 generations, 135274 statements, best individuals have fitness: 81.91304347826087
[MASTER] 12:29:29.884 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:29:29.892 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 3 tests with total length 21
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     82 / 0           
[MASTER] 12:29:30.290 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 12:29:30.425 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 17 
[MASTER] 12:29:30.444 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 12:29:30.444 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 12:29:30.445 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 12:29:30.445 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
[MASTER] 12:29:30.445 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
