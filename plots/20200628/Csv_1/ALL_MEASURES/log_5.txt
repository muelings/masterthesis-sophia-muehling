* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 17529
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 12:55:02.932 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 12:55:02.938 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 30
* Using seed 1593082500672
* Starting evolution
[MASTER] 12:55:04.598 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.95674538227729 - branchDistance:0.0 - coverage:32.9567453822773 - ex: 0 - tex: 14
[MASTER] 12:55:04.598 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.95674538227729, number of tests: 10, total length: 202
[MASTER] 12:55:07.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:92.94615384615385 - branchDistance:0.0 - coverage:29.946153846153845 - ex: 0 - tex: 4
[MASTER] 12:55:07.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.94615384615385, number of tests: 5, total length: 111
[MASTER] 12:55:10.876 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.69705407586764 - branchDistance:0.0 - coverage:28.697054075867634 - ex: 0 - tex: 6
[MASTER] 12:55:10.876 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.69705407586764, number of tests: 6, total length: 129
[MASTER] 12:55:14.111 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:87.01001011122347 - branchDistance:0.0 - coverage:24.01001011122346 - ex: 0 - tex: 10
[MASTER] 12:55:14.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.01001011122347, number of tests: 9, total length: 131
[MASTER] 12:55:20.198 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:85.9494040506174 - branchDistance:0.0 - coverage:22.9494040506174 - ex: 0 - tex: 8
[MASTER] 12:55:20.198 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.9494040506174, number of tests: 7, total length: 147
[MASTER] 12:55:20.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:83.67667677789012 - branchDistance:0.0 - coverage:20.676676777890126 - ex: 0 - tex: 10
[MASTER] 12:55:20.333 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.67667677789012, number of tests: 8, total length: 130
[MASTER] 12:55:21.210 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.61282051282052 - branchDistance:0.0 - coverage:19.612820512820512 - ex: 0 - tex: 6
[MASTER] 12:55:21.210 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.61282051282052, number of tests: 5, total length: 134
[MASTER] 12:55:24.787 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.48095238095237 - branchDistance:0.0 - coverage:19.48095238095238 - ex: 0 - tex: 2
[MASTER] 12:55:24.787 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.48095238095237, number of tests: 5, total length: 113
[MASTER] 12:55:33.411 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.43769776322968 - branchDistance:0.0 - coverage:19.43769776322968 - ex: 0 - tex: 2
[MASTER] 12:55:33.411 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.43769776322968, number of tests: 5, total length: 113
[MASTER] 12:55:41.170 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.34500768049155 - branchDistance:0.0 - coverage:19.345007680491552 - ex: 0 - tex: 2
[MASTER] 12:55:41.170 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.34500768049155, number of tests: 6, total length: 127
[MASTER] 12:55:49.410 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.3100432900433 - branchDistance:0.0 - coverage:19.31004329004329 - ex: 0 - tex: 2
[MASTER] 12:55:49.410 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.3100432900433, number of tests: 5, total length: 107
[MASTER] 12:56:15.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.92909090909092 - branchDistance:0.0 - coverage:18.92909090909091 - ex: 0 - tex: 6
[MASTER] 12:56:15.503 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.92909090909092, number of tests: 6, total length: 112
[MASTER] 12:57:29.112 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 0
[MASTER] 12:57:29.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 3, total length: 54
[MASTER] 13:00:04.081 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 155 generations, 145236 statements, best individuals have fitness: 81.91304347826087
[MASTER] 13:00:04.084 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 3 tests with total length 23
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     82 / 0           
[MASTER] 13:00:04.408 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:00:04.531 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 19 
[MASTER] 13:00:04.566 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 13:00:04.566 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 13:00:04.566 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 13:00:04.566 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 13:00:04.567 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
