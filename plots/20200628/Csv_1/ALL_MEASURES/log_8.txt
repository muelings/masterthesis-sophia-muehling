* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.ExtendedBufferedReader
* Starting client
* Connecting to master process on port 6304
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.ExtendedBufferedReader
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 13:25:38.937 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 13:25:38.941 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 30
* Using seed 1593084336799
* Starting evolution
[MASTER] 13:25:40.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:103.0 - branchDistance:0.0 - coverage:40.0 - ex: 0 - tex: 6
[MASTER] 13:25:40.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.0, number of tests: 3, total length: 90
[MASTER] 13:25:41.381 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:95.76666666666667 - branchDistance:0.0 - coverage:32.766666666666666 - ex: 0 - tex: 8
[MASTER] 13:25:41.381 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.76666666666667, number of tests: 4, total length: 104
[MASTER] 13:25:42.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:91.9 - branchDistance:0.0 - coverage:28.9 - ex: 0 - tex: 12
[MASTER] 13:25:42.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.9, number of tests: 8, total length: 147
[MASTER] 13:25:46.715 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:90.47744107744109 - branchDistance:0.0 - coverage:27.477441077441078 - ex: 0 - tex: 12
[MASTER] 13:25:46.715 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.47744107744109, number of tests: 9, total length: 183
[MASTER] 13:25:53.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:89.81304347826087 - branchDistance:0.0 - coverage:26.81304347826087 - ex: 0 - tex: 8
[MASTER] 13:25:53.503 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.81304347826087, number of tests: 8, total length: 156
[MASTER] 13:25:54.036 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:84.24410774410774 - branchDistance:0.0 - coverage:21.244107744107744 - ex: 0 - tex: 10
[MASTER] 13:25:54.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.24410774410774, number of tests: 9, total length: 182
[MASTER] 13:25:56.080 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.62592592592593 - branchDistance:0.0 - coverage:19.625925925925927 - ex: 0 - tex: 12
[MASTER] 13:25:56.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.62592592592593, number of tests: 10, total length: 212
[MASTER] 13:25:57.011 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.44410774410774 - branchDistance:0.0 - coverage:19.444107744107743 - ex: 0 - tex: 6
[MASTER] 13:25:57.011 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.44410774410774, number of tests: 9, total length: 192
[MASTER] 13:25:58.708 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.39789196310936 - branchDistance:0.0 - coverage:19.397891963109355 - ex: 0 - tex: 6
[MASTER] 13:25:58.708 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.39789196310936, number of tests: 8, total length: 172
[MASTER] 13:26:01.748 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.95925925925926 - branchDistance:0.0 - coverage:18.95925925925926 - ex: 0 - tex: 12
[MASTER] 13:26:01.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.95925925925926, number of tests: 8, total length: 213
[MASTER] 13:26:03.383 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:81.91304347826087 - branchDistance:0.0 - coverage:18.91304347826087 - ex: 0 - tex: 8
[MASTER] 13:26:03.384 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.91304347826087, number of tests: 8, total length: 192
[MASTER] 13:30:40.174 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 302s and 148 generations, 124824 statements, best individuals have fitness: 81.91304347826087
[MASTER] 13:30:40.180 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 60%
* Total number of goals: 30
* Number of covered goals: 18
* Generated 3 tests with total length 24
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                     82 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 13:30:40.502 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 13:30:40.640 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 21 
[MASTER] 13:30:40.681 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 13:30:40.681 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 13:30:40.681 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 13:30:40.681 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 13:30:40.682 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 30
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 4
* Writing JUnit test case 'ExtendedBufferedReader_ESTest' to evosuite-tests
* Done!

* Computation finished
