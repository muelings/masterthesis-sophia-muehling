* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting client
* Connecting to master process on port 21635
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 05:39:40.299 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 371
[MASTER] 05:40:12.917 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:40:14.131 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:40:14.135 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 05:40:14.450 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:40:14.454 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:40:14.454 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 8585 | Tests with assertion: 1
* Generated 1 tests with total length 2
* GA-Budget:
	- MaxTime :                         34 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 05:40:15.650 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:40:15.660 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 1 
[MASTER] 05:40:15.666 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:40:15.666 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 05:40:15.666 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createNumber:NumberFormatException at 0
[MASTER] 05:40:15.666 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 05:40:15.677 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
[MASTER] 05:40:15.680 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 371
* Number of covered goals: 13
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NumberUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
