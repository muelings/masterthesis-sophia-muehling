* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.math.NumberUtils
* Starting client
* Connecting to master process on port 13009
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_1_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.math.NumberUtils
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 05:50:51.847 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 371
[MASTER] 05:51:19.703 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:51:20.870 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:51:20.881 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 05:51:21.220 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/math/NumberUtils_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 05:51:21.226 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 05:51:21.226 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 7253 | Tests with assertion: 1
* Generated 1 tests with total length 20
* GA-Budget:
	- MaxTime :                         29 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 05:51:22.390 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 05:51:22.403 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 14 
[MASTER] 05:51:22.414 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 05:51:22.414 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 05:51:22.414 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createNumber:NumberFormatException at 13
[MASTER] 05:51:22.414 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createNumber:NumberFormatException, NumberFormatException:createBigInteger-745,NumberFormatException:createBigInteger-745]
[MASTER] 05:51:22.505 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
[MASTER] 05:51:22.508 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 4%
* Total number of goals: 371
* Number of covered goals: 16
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NumberUtils_ESTest' to evosuite-tests
* Done!

* Computation finished
