* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 21755
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 08:43:42.489 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 08:43:42.496 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 134
* Using seed 1593153817250
* Starting evolution
[MASTER] 08:43:44.851 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:559.0 - branchDistance:0.0 - coverage:268.0 - ex: 0 - tex: 10
[MASTER] 08:43:44.852 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 559.0, number of tests: 5, total length: 105
[MASTER] 08:43:45.883 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:539.0 - branchDistance:0.0 - coverage:248.0 - ex: 0 - tex: 20
[MASTER] 08:43:45.883 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 539.0, number of tests: 10, total length: 237
[MASTER] 08:43:46.065 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:496.0 - branchDistance:0.0 - coverage:205.0 - ex: 0 - tex: 8
[MASTER] 08:43:46.065 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 496.0, number of tests: 5, total length: 126
[MASTER] 08:43:50.178 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:494.0 - branchDistance:0.0 - coverage:203.0 - ex: 0 - tex: 8
[MASTER] 08:43:50.178 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 494.0, number of tests: 5, total length: 116
[MASTER] 08:43:50.232 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:492.0 - branchDistance:0.0 - coverage:201.0 - ex: 0 - tex: 10
[MASTER] 08:43:50.232 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 492.0, number of tests: 6, total length: 146
* Computation finished
