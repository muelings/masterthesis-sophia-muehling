* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 9811
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 134
[MASTER] 08:50:26.613 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:51:46.353 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 08:51:47.585 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 08:51:47.591 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 08:51:47.931 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 08:51:47.938 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 08:51:47.939 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 6022 | Tests with assertion: 1
* Generated 1 tests with total length 18
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                         81 / 300         
[MASTER] 08:51:48.880 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:51:48.890 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 18 
[MASTER] 08:51:48.896 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 08:51:48.896 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [MockIllegalArgumentException:<init>-219,, with:MockIllegalArgumentException]
[MASTER] 08:51:48.896 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: with:MockIllegalArgumentException at 17
[MASTER] 08:51:48.896 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [MockIllegalArgumentException:<init>-219,, with:MockIllegalArgumentException]
[MASTER] 08:51:49.031 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 11 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:51:49.034 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 22%
* Total number of goals: 134
* Number of covered goals: 29
* Generated 1 tests with total length 11
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Partial_ESTest' to evosuite-tests
* Done!

* Computation finished
