* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Partial
* Starting client
* Connecting to master process on port 12172
* Analyzing classpath: 
  - ../defects4j_compiled/Time_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Partial
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 08:58:35.802 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 134
[MASTER] 08:59:37.376 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 08:59:38.620 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 08:59:38.638 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 08:59:38.950 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Partial_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 08:59:38.964 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 08:59:38.964 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 4498 | Tests with assertion: 1
* Generated 1 tests with total length 10
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                         63 / 300         
[MASTER] 08:59:39.936 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 08:59:39.961 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 7 
[MASTER] 08:59:39.978 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 08:59:39.978 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [with:MockIllegalArgumentException, MockIllegalArgumentException:<init>-224,MockIllegalArgumentException:<init>-224]
[MASTER] 08:59:39.978 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: with:MockIllegalArgumentException at 6
[MASTER] 08:59:39.979 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [with:MockIllegalArgumentException, MockIllegalArgumentException:<init>-224,MockIllegalArgumentException:<init>-224]
[MASTER] 08:59:40.171 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 08:59:40.178 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 17%
* Total number of goals: 134
* Number of covered goals: 23
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Partial_ESTest' to evosuite-tests
* Done!

* Computation finished
