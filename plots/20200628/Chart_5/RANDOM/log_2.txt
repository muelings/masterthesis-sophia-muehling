* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.data.xy.XYSeries
* Starting client
* Connecting to master process on port 3862
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_5_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.data.xy.XYSeries
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 103
[MASTER] 02:44:01.136 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:44:02.052 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:44:03.722 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:44:04.070 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 02:44:04.455 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 02:44:04.469 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 02:44:04.717 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/data/xy/XYSeries_5_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 02:44:04.731 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 02:44:04.732 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 40 | Tests with assertion: 1
* Generated 1 tests with total length 14
* GA-Budget:
	- MaxTime :                          3 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 02:44:05.274 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 02:44:05.283 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 02:44:05.291 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 02:44:05.291 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [addOrUpdate:IndexOutOfBoundsException]
[MASTER] 02:44:05.291 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: addOrUpdate:IndexOutOfBoundsException at 10
[MASTER] 02:44:05.291 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [addOrUpdate:IndexOutOfBoundsException]
[MASTER] 02:44:05.419 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
[MASTER] 02:44:05.422 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 17%
* Total number of goals: 103
* Number of covered goals: 18
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYSeries_ESTest' to evosuite-tests
* Done!

* Computation finished
