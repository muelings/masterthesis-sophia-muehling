/*
 * This file was automatically generated by EvoSuite
 * Thu Jun 25 01:06:00 GMT 2020
 */

package org.jfree.data.xy;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jfree.data.time.Hour;
import org.jfree.data.xy.XYSeries;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYSeries_ESTest extends XYSeries_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Hour hour0 = new Hour();
      XYSeries xYSeries0 = new XYSeries(hour0);
      xYSeries0.addOrUpdate((double) 0, (-169.53183061034));
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.IndexOutOfBoundsException : Index: -1, Size: 3
      xYSeries0.addOrUpdate((double) 0, (double) 1787);
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     java.lang.IndexOutOfBoundsException : Index: 0, Size: 0
      // Undeclared exception!
      try { 
        xYSeries0.setMaximumItemCount((-232));
        fail("Expecting exception: IndexOutOfBoundsException");
      
      } catch(IndexOutOfBoundsException e) {
         //
         // Index: 0, Size: 0
         //
         verifyException("java.util.ArrayList", e);
         assertTrue(e.getMessage().equals("Index: 0, Size: 0"));   
      }
  }
}
