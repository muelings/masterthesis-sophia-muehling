* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.NodePointer
* Starting client
* Connecting to master process on port 19125
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.NodePointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:39:31.999 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 158
[MASTER] 04:40:57.067 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 04:40:58.696 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:40:58.705 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 04:40:59.013 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:40:59.019 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
[MASTER] 04:40:59.020 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 14201 | Tests with assertion: 1
* Generated 1 tests with total length 22
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
	- MaxTime :                         87 / 300         
[MASTER] 04:40:59.777 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:40:59.788 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 13 
[MASTER] 04:40:59.809 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 04:40:59.898 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
[MASTER] 04:40:59.901 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 1%
* Total number of goals: 158
* Number of covered goals: 2
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NodePointer_ESTest' to evosuite-tests
* Done!

* Computation finished
