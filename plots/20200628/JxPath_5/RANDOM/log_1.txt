* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.NodePointer
* Starting client
* Connecting to master process on port 2955
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.NodePointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:09:36.855 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 158
[MASTER] 04:10:36.985 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:10:38.527 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 2 assertions.
[MASTER] 04:10:38.542 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:10:38.915 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 04:10:38.933 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:10:38.934 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*** Random test generation finished.
*=*=*=* Total tests: 10775 | Tests with assertion: 1
* Generated 1 tests with total length 18
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                         62 / 300         
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:10:39.689 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:10:39.705 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 15 
[MASTER] 04:10:39.717 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 04:10:39.717 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [compareTo:JXPathException, isCollection:JXPathException, JXPathException,]
[MASTER] 04:10:39.717 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: isCollection:JXPathException at 14
[MASTER] 04:10:39.717 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [compareTo:JXPathException, isCollection:JXPathException, JXPathException,]
[MASTER] 04:10:39.717 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: compareTo:JXPathException at 13
[MASTER] 04:10:39.717 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [compareTo:JXPathException, isCollection:JXPathException, JXPathException,]
[MASTER] 04:10:39.884 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
[MASTER] 04:10:39.887 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 14%
* Total number of goals: 158
* Number of covered goals: 22
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NodePointer_ESTest' to evosuite-tests
* Done!

* Computation finished
