* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.NodePointer
* Starting client
* Connecting to master process on port 4092
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.NodePointer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 04:58:16.695 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 158
[MASTER] 04:58:37.709 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 04:58:39.101 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:58:39.110 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 04:58:39.392 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/jxpath/ri/model/NodePointer_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 04:58:39.400 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 04:58:39.401 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 2735 | Tests with assertion: 1
* Generated 1 tests with total length 18
* GA-Budget:
	- MaxTime :                         22 / 300         
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 04:58:40.068 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:58:40.078 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 04:58:40.084 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 04:58:40.084 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [compareTo:JXPathException]
[MASTER] 04:58:40.084 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: compareTo:JXPathException at 6
[MASTER] 04:58:40.084 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [compareTo:JXPathException]
[MASTER] 04:58:40.169 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 04:58:40.173 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 12%
* Total number of goals: 158
* Number of covered goals: 19
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NodePointer_ESTest' to evosuite-tests
* Done!

* Computation finished
