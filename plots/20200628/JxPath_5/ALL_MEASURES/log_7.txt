* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.NodePointer
* Starting client
* Connecting to master process on port 19358
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.NodePointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:50:02.617 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:50:02.626 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 158
* Using seed 1593139799484
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 04:50:04.585 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:717.0 - branchDistance:0.0 - coverage:358.0 - ex: 0 - tex: 16
[MASTER] 04:50:04.585 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 717.0, number of tests: 8, total length: 178
[MASTER] 04:50:04.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:686.3333333333334 - branchDistance:0.0 - coverage:327.33333333333337 - ex: 0 - tex: 16
[MASTER] 04:50:04.872 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 686.3333333333334, number of tests: 9, total length: 217
[MASTER] 04:50:04.945 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:682.3333333333334 - branchDistance:0.0 - coverage:323.33333333333337 - ex: 0 - tex: 16
[MASTER] 04:50:04.945 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 682.3333333333334, number of tests: 9, total length: 259
[MASTER] 04:50:05.460 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:681.3333333333334 - branchDistance:0.0 - coverage:322.33333333333337 - ex: 0 - tex: 14
[MASTER] 04:50:05.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 681.3333333333334, number of tests: 7, total length: 154
[MASTER] 04:50:05.730 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:672.3333333333334 - branchDistance:0.0 - coverage:313.33333333333337 - ex: 0 - tex: 12
[MASTER] 04:50:05.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 672.3333333333334, number of tests: 7, total length: 155
[MASTER] 04:50:05.887 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:665.3333333333334 - branchDistance:0.0 - coverage:306.33333333333337 - ex: 0 - tex: 20
[MASTER] 04:50:05.887 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 665.3333333333334, number of tests: 10, total length: 239
[MASTER] 04:50:06.344 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:653.6666666666666 - branchDistance:0.0 - coverage:294.66666666666663 - ex: 0 - tex: 12
[MASTER] 04:50:06.344 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 653.6666666666666, number of tests: 8, total length: 180
[MASTER] 04:50:06.902 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:650.6666666666666 - branchDistance:0.0 - coverage:291.66666666666663 - ex: 0 - tex: 16
[MASTER] 04:50:06.902 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 650.6666666666666, number of tests: 9, total length: 209
[MASTER] 04:50:07.346 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:643.6666666666666 - branchDistance:0.0 - coverage:284.66666666666663 - ex: 0 - tex: 12
[MASTER] 04:50:07.346 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 643.6666666666666, number of tests: 8, total length: 159
[MASTER] 04:50:08.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:642.6666666666666 - branchDistance:0.0 - coverage:283.66666666666663 - ex: 0 - tex: 16
[MASTER] 04:50:08.114 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 642.6666666666666, number of tests: 9, total length: 218
[MASTER] 04:50:08.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:637.6666666666666 - branchDistance:0.0 - coverage:278.66666666666663 - ex: 0 - tex: 18
[MASTER] 04:50:08.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 637.6666666666666, number of tests: 9, total length: 275
[MASTER] 04:50:08.516 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:634.6666666666666 - branchDistance:0.0 - coverage:275.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:50:08.516 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 634.6666666666666, number of tests: 9, total length: 191
[MASTER] 04:50:08.660 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:624.6666666666666 - branchDistance:0.0 - coverage:265.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:50:08.660 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 624.6666666666666, number of tests: 9, total length: 241
[MASTER] 04:50:09.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:612.6666666666666 - branchDistance:0.0 - coverage:253.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:50:09.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 612.6666666666666, number of tests: 9, total length: 195
[MASTER] 04:50:10.279 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:606.6666666666666 - branchDistance:0.0 - coverage:247.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:50:10.279 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 606.6666666666666, number of tests: 10, total length: 201
[MASTER] 04:50:10.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:604.6666666666666 - branchDistance:0.0 - coverage:245.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:50:10.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 604.6666666666666, number of tests: 9, total length: 177
[MASTER] 04:50:11.999 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:602.6666666666666 - branchDistance:0.0 - coverage:243.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:50:11.999 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 602.6666666666666, number of tests: 9, total length: 199
[MASTER] 04:50:12.812 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:598.6666666666666 - branchDistance:0.0 - coverage:239.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:50:12.812 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 598.6666666666666, number of tests: 9, total length: 204
[MASTER] 04:50:13.110 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:596.6666666666666 - branchDistance:0.0 - coverage:237.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:50:13.110 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 596.6666666666666, number of tests: 10, total length: 195
[MASTER] 04:50:13.303 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:595.6666666666666 - branchDistance:0.0 - coverage:236.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:50:13.303 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 595.6666666666666, number of tests: 9, total length: 185
[MASTER] 04:50:13.559 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:594.6666666666666 - branchDistance:0.0 - coverage:235.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:50:13.559 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 594.6666666666666, number of tests: 10, total length: 237
[MASTER] 04:50:14.343 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:593.6666666666666 - branchDistance:0.0 - coverage:234.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:50:14.343 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 593.6666666666666, number of tests: 10, total length: 192
[MASTER] 04:50:14.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:589.6666666666666 - branchDistance:0.0 - coverage:230.66666666666666 - ex: 0 - tex: 14
[MASTER] 04:50:14.503 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 589.6666666666666, number of tests: 10, total length: 184
[MASTER] 04:50:15.111 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:589.6666666657353 - branchDistance:0.0 - coverage:230.66666666573533 - ex: 0 - tex: 16
[MASTER] 04:50:15.112 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 589.6666666657353, number of tests: 11, total length: 240
[MASTER] 04:50:15.159 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:587.6666666666666 - branchDistance:0.0 - coverage:228.66666666666666 - ex: 0 - tex: 16
[MASTER] 04:50:15.159 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 587.6666666666666, number of tests: 10, total length: 228
[MASTER] 04:50:15.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:585.6666666666666 - branchDistance:0.0 - coverage:226.66666666666666 - ex: 0 - tex: 12
[MASTER] 04:50:15.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 585.6666666666666, number of tests: 10, total length: 197
[MASTER] 04:50:15.493 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:584.6666666657353 - branchDistance:0.0 - coverage:225.66666666573533 - ex: 0 - tex: 18
[MASTER] 04:50:15.493 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 584.6666666657353, number of tests: 10, total length: 235
[MASTER] 04:50:16.083 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:578.6666666657353 - branchDistance:0.0 - coverage:219.66666666573533 - ex: 0 - tex: 18
[MASTER] 04:50:16.083 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 578.6666666657353, number of tests: 12, total length: 251
[MASTER] 04:50:17.233 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:571.6666666657353 - branchDistance:0.0 - coverage:212.66666666573533 - ex: 0 - tex: 20
[MASTER] 04:50:17.233 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 571.6666666657353, number of tests: 11, total length: 248
[MASTER] 04:50:17.919 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:569.6666666657353 - branchDistance:0.0 - coverage:210.66666666573533 - ex: 0 - tex: 20
[MASTER] 04:50:17.919 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 569.6666666657353, number of tests: 12, total length: 258
[MASTER] 04:50:18.920 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:561.6666666657353 - branchDistance:0.0 - coverage:202.66666666573533 - ex: 0 - tex: 20
[MASTER] 04:50:18.920 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 561.6666666657353, number of tests: 12, total length: 285
[MASTER] 04:50:19.148 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:560.6666666657353 - branchDistance:0.0 - coverage:201.66666666573533 - ex: 0 - tex: 22
[MASTER] 04:50:19.148 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 560.6666666657353, number of tests: 12, total length: 254
[MASTER] 04:50:19.270 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:558.6666666657353 - branchDistance:0.0 - coverage:199.66666666573533 - ex: 0 - tex: 22
[MASTER] 04:50:19.270 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 558.6666666657353, number of tests: 12, total length: 258
[MASTER] 04:50:20.412 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:553.6666666657353 - branchDistance:0.0 - coverage:194.66666666573533 - ex: 0 - tex: 22
[MASTER] 04:50:20.412 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 553.6666666657353, number of tests: 12, total length: 269
[MASTER] 04:50:21.041 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:547.6666666657353 - branchDistance:0.0 - coverage:188.66666666573533 - ex: 0 - tex: 22
[MASTER] 04:50:21.041 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 547.6666666657353, number of tests: 12, total length: 272
[MASTER] 04:50:22.724 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:543.6666666657353 - branchDistance:0.0 - coverage:184.66666666573533 - ex: 0 - tex: 22
[MASTER] 04:50:22.724 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 543.6666666657353, number of tests: 13, total length: 300
[MASTER] 04:50:24.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:541.6666666657353 - branchDistance:0.0 - coverage:182.66666666573533 - ex: 0 - tex: 26
[MASTER] 04:50:24.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 541.6666666657353, number of tests: 13, total length: 316
[MASTER] 04:50:25.560 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:539.6666666657353 - branchDistance:0.0 - coverage:180.66666666573533 - ex: 0 - tex: 26
[MASTER] 04:50:25.560 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 539.6666666657353, number of tests: 14, total length: 342
[MASTER] 04:50:26.211 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:536.6666666666666 - branchDistance:0.0 - coverage:177.66666666666666 - ex: 0 - tex: 26
[MASTER] 04:50:26.211 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 536.6666666666666, number of tests: 14, total length: 342
[MASTER] 04:50:27.040 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:532.6666666657353 - branchDistance:0.0 - coverage:173.66666666573533 - ex: 0 - tex: 22
[MASTER] 04:50:27.041 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 532.6666666657353, number of tests: 14, total length: 332
[MASTER] 04:50:28.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:530.6666666657353 - branchDistance:0.0 - coverage:171.66666666573533 - ex: 0 - tex: 22
[MASTER] 04:50:28.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 530.6666666657353, number of tests: 13, total length: 314
[MASTER] 04:50:28.176 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:529.6666666666667 - branchDistance:0.0 - coverage:170.66666666666669 - ex: 0 - tex: 22
[MASTER] 04:50:28.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 529.6666666666667, number of tests: 14, total length: 337
[MASTER] 04:50:29.015 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:527.6666666666667 - branchDistance:0.0 - coverage:168.66666666666669 - ex: 0 - tex: 26
[MASTER] 04:50:29.015 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 527.6666666666667, number of tests: 14, total length: 366
[MASTER] 04:50:29.788 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:526.6666666657353 - branchDistance:0.0 - coverage:167.66666666573533 - ex: 0 - tex: 24
[MASTER] 04:50:29.789 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 526.6666666657353, number of tests: 14, total length: 334
[MASTER] 04:50:30.099 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:525.6666666666667 - branchDistance:0.0 - coverage:166.66666666666669 - ex: 0 - tex: 26
[MASTER] 04:50:30.099 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 525.6666666666667, number of tests: 14, total length: 359
[MASTER] 04:50:31.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:519.6666666666667 - branchDistance:0.0 - coverage:160.66666666666669 - ex: 0 - tex: 28
[MASTER] 04:50:31.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 519.6666666666667, number of tests: 15, total length: 401
[MASTER] 04:50:33.349 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:518.6666666666667 - branchDistance:0.0 - coverage:159.66666666666669 - ex: 0 - tex: 30
[MASTER] 04:50:33.349 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 518.6666666666667, number of tests: 15, total length: 397
[MASTER] 04:50:33.517 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:517.6666666666666 - branchDistance:0.0 - coverage:158.66666666666666 - ex: 0 - tex: 30
[MASTER] 04:50:33.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 517.6666666666666, number of tests: 16, total length: 434
[MASTER] 04:50:33.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:515.6666666666667 - branchDistance:0.0 - coverage:156.66666666666669 - ex: 0 - tex: 30
[MASTER] 04:50:33.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 515.6666666666667, number of tests: 15, total length: 404
[MASTER] 04:50:35.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:513.6666666666667 - branchDistance:0.0 - coverage:154.66666666666669 - ex: 0 - tex: 30
[MASTER] 04:50:35.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 513.6666666666667, number of tests: 15, total length: 425
[MASTER] 04:50:35.853 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:512.6666666666666 - branchDistance:0.0 - coverage:153.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:35.853 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 512.6666666666666, number of tests: 16, total length: 395
[MASTER] 04:50:36.590 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:511.66666666666663 - branchDistance:0.0 - coverage:152.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:36.590 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 511.66666666666663, number of tests: 16, total length: 427
[MASTER] 04:50:37.006 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:510.66666666666663 - branchDistance:0.0 - coverage:151.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:37.006 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 510.66666666666663, number of tests: 16, total length: 409
[MASTER] 04:50:38.615 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:509.66666666666663 - branchDistance:0.0 - coverage:150.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:38.615 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 509.66666666666663, number of tests: 16, total length: 438
[MASTER] 04:50:41.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:508.66666666666663 - branchDistance:0.0 - coverage:149.66666666666666 - ex: 0 - tex: 30
[MASTER] 04:50:41.348 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 508.66666666666663, number of tests: 16, total length: 404
[MASTER] 04:50:42.934 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:507.66666666666663 - branchDistance:0.0 - coverage:148.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:42.934 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 507.66666666666663, number of tests: 16, total length: 394
[MASTER] 04:50:46.854 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:482.6666666666667 - branchDistance:0.0 - coverage:123.66666666666667 - ex: 0 - tex: 30
[MASTER] 04:50:46.854 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 482.6666666666667, number of tests: 16, total length: 384
[MASTER] 04:50:48.130 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:480.6666666666667 - branchDistance:0.0 - coverage:121.66666666666667 - ex: 0 - tex: 32
[MASTER] 04:50:48.131 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 480.6666666666667, number of tests: 17, total length: 413
[MASTER] 04:50:51.333 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:479.6666666666667 - branchDistance:0.0 - coverage:120.66666666666667 - ex: 0 - tex: 30
[MASTER] 04:50:51.333 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 479.6666666666667, number of tests: 18, total length: 399
[MASTER] 04:50:52.580 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:478.6666666666667 - branchDistance:0.0 - coverage:119.66666666666667 - ex: 0 - tex: 34
[MASTER] 04:50:52.580 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 478.6666666666667, number of tests: 18, total length: 417
[MASTER] 04:50:53.971 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:477.6666666666667 - branchDistance:0.0 - coverage:118.66666666666667 - ex: 0 - tex: 34
[MASTER] 04:50:53.971 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 477.6666666666667, number of tests: 18, total length: 432
[MASTER] 04:50:55.153 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:476.66666666666663 - branchDistance:0.0 - coverage:117.66666666666666 - ex: 0 - tex: 34
[MASTER] 04:50:55.153 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 476.66666666666663, number of tests: 18, total length: 417
[MASTER] 04:50:57.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:475.66666666666663 - branchDistance:0.0 - coverage:116.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:57.021 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 475.66666666666663, number of tests: 18, total length: 414
[MASTER] 04:50:57.737 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:474.6666666666667 - branchDistance:0.0 - coverage:115.66666666666667 - ex: 0 - tex: 32
[MASTER] 04:50:57.738 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 474.6666666666667, number of tests: 19, total length: 422
[MASTER] 04:50:58.874 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:473.66666666666663 - branchDistance:0.0 - coverage:114.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:58.874 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 473.66666666666663, number of tests: 19, total length: 422
[MASTER] 04:50:59.641 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:472.66666666666663 - branchDistance:0.0 - coverage:113.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:50:59.641 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 472.66666666666663, number of tests: 19, total length: 423
[MASTER] 04:51:02.540 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:471.66666666666663 - branchDistance:0.0 - coverage:112.66666666666666 - ex: 0 - tex: 32
[MASTER] 04:51:02.540 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 471.66666666666663, number of tests: 19, total length: 430
[MASTER] 04:51:05.307 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:470.6666666657353 - branchDistance:0.0 - coverage:111.66666666573533 - ex: 0 - tex: 32
[MASTER] 04:51:05.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 470.6666666657353, number of tests: 19, total length: 432
[MASTER] 04:51:07.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:469.6666666657353 - branchDistance:0.0 - coverage:110.66666666573533 - ex: 0 - tex: 32
[MASTER] 04:51:07.626 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 469.6666666657353, number of tests: 20, total length: 450
[MASTER] 04:51:09.203 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:465.66666666573536 - branchDistance:0.0 - coverage:106.66666666573535 - ex: 0 - tex: 34
[MASTER] 04:51:09.203 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 465.66666666573536, number of tests: 19, total length: 444
[MASTER] 04:51:09.980 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:464.66666666573536 - branchDistance:0.0 - coverage:105.66666666573535 - ex: 0 - tex: 32
[MASTER] 04:51:09.980 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 464.66666666573536, number of tests: 20, total length: 444
[MASTER] 04:51:10.748 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:464.6666666657353 - branchDistance:0.0 - coverage:105.66666666573533 - ex: 0 - tex: 32
[MASTER] 04:51:10.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 464.6666666657353, number of tests: 21, total length: 444
[MASTER] 04:51:12.448 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:463.66666666573536 - branchDistance:0.0 - coverage:104.66666666573535 - ex: 0 - tex: 34
[MASTER] 04:51:12.448 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 463.66666666573536, number of tests: 21, total length: 469
[MASTER] 04:51:13.917 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:463.6666666657353 - branchDistance:0.0 - coverage:104.66666666573533 - ex: 0 - tex: 34
[MASTER] 04:51:13.917 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 463.6666666657353, number of tests: 22, total length: 474
[MASTER] 04:51:15.249 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:462.66666666573536 - branchDistance:0.0 - coverage:103.66666666573535 - ex: 0 - tex: 36
[MASTER] 04:51:15.249 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 462.66666666573536, number of tests: 21, total length: 474
[MASTER] 04:51:17.950 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:456.66666666573536 - branchDistance:0.0 - coverage:97.66666666573535 - ex: 0 - tex: 36
[MASTER] 04:51:17.950 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 456.66666666573536, number of tests: 22, total length: 488
[MASTER] 04:51:20.693 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:456.6666666657353 - branchDistance:0.0 - coverage:97.66666666573533 - ex: 0 - tex: 38
[MASTER] 04:51:20.693 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 456.6666666657353, number of tests: 23, total length: 492
[MASTER] 04:51:22.213 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:455.6666666657353 - branchDistance:0.0 - coverage:96.66666666573533 - ex: 0 - tex: 38
[MASTER] 04:51:22.213 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 455.6666666657353, number of tests: 23, total length: 517
[MASTER] 04:51:26.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:454.66666666666663 - branchDistance:0.0 - coverage:95.66666666666666 - ex: 0 - tex: 36
[MASTER] 04:51:26.180 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 454.66666666666663, number of tests: 23, total length: 517
[MASTER] 04:51:27.495 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:454.6666666657353 - branchDistance:0.0 - coverage:95.66666666573533 - ex: 0 - tex: 40
[MASTER] 04:51:27.495 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 454.6666666657353, number of tests: 24, total length: 551
[MASTER] 04:51:28.418 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:453.66666666666663 - branchDistance:0.0 - coverage:94.66666666666666 - ex: 0 - tex: 38
[MASTER] 04:51:28.418 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 453.66666666666663, number of tests: 24, total length: 554
[MASTER] 04:51:36.955 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:445.66666666666663 - branchDistance:0.0 - coverage:86.66666666666666 - ex: 0 - tex: 34
[MASTER] 04:51:36.955 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 445.66666666666663, number of tests: 23, total length: 502
[MASTER] 04:51:49.647 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:442.66666666666663 - branchDistance:0.0 - coverage:83.66666666666666 - ex: 0 - tex: 36
[MASTER] 04:51:49.647 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 442.66666666666663, number of tests: 24, total length: 494
[MASTER] 04:51:52.521 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:441.66666666666663 - branchDistance:0.0 - coverage:82.66666666666666 - ex: 0 - tex: 38
[MASTER] 04:51:52.521 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 441.66666666666663, number of tests: 25, total length: 524
[MASTER] 04:52:08.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:439.6666666656115 - branchDistance:0.0 - coverage:80.66666666561153 - ex: 0 - tex: 36
[MASTER] 04:52:08.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 439.6666666656115, number of tests: 24, total length: 503
[MASTER] 04:52:22.668 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:438.6666666656115 - branchDistance:0.0 - coverage:79.66666666561152 - ex: 0 - tex: 34
[MASTER] 04:52:22.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 438.6666666656115, number of tests: 25, total length: 522
[MASTER] 04:54:26.241 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:435.6666666656115 - branchDistance:0.0 - coverage:76.66666666561152 - ex: 0 - tex: 22
[MASTER] 04:54:26.241 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 435.6666666656115, number of tests: 25, total length: 361
[MASTER] 04:54:28.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:434.6666666656115 - branchDistance:0.0 - coverage:75.66666666561152 - ex: 0 - tex: 24
[MASTER] 04:54:28.884 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 434.6666666656115, number of tests: 26, total length: 389
[MASTER] 04:54:39.972 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:433.6666666656115 - branchDistance:0.0 - coverage:74.66666666561152 - ex: 0 - tex: 24
[MASTER] 04:54:39.972 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 433.6666666656115, number of tests: 27, total length: 395
[MASTER] 04:55:02.416 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:433.66666666561105 - branchDistance:0.0 - coverage:74.66666666561107 - ex: 0 - tex: 24
[MASTER] 04:55:02.416 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 433.66666666561105, number of tests: 27, total length: 382
[MASTER] 04:55:03.676 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 737 generations, 536324 statements, best individuals have fitness: 433.66666666561105
[MASTER] 04:55:03.680 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 71%
* Total number of goals: 158
* Number of covered goals: 112
* Generated 27 tests with total length 381
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :                    434 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 04:55:04.424 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 04:55:04.473 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 333 
[MASTER] 04:55:04.631 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.631 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.631 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.631 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.631 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.631 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Test23 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Test25 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Test26 - Difference in number of exceptions: 0.0
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 04:55:04.632 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 19: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 20: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 21: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 22: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 23: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 24: no assertions
* Going to analyze the coverage criteria
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 25: no assertions
[MASTER] 04:55:04.633 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 26: no assertions
[MASTER] 04:55:04.634 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 04:55:04.634 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 158
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'NodePointer_ESTest' to evosuite-tests
* Done!

* Computation finished
