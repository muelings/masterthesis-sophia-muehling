* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.jxpath.ri.model.NodePointer
* Starting client
* Connecting to master process on port 9388
* Analyzing classpath: 
  - ../defects4j_compiled/JxPath_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.jxpath.ri.model.NodePointer
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 04:55:59.705 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 04:55:59.714 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 158
* Using seed 1593140156398
* Starting evolution
[MASTER] 04:56:00.871 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:715.0 - branchDistance:0.0 - coverage:356.0 - ex: 0 - tex: 2
[MASTER] 04:56:00.871 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 715.0, number of tests: 1, total length: 40
[MASTER] 04:56:01.338 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:687.3333333333334 - branchDistance:0.0 - coverage:328.33333333333337 - ex: 0 - tex: 6
[MASTER] 04:56:01.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 687.3333333333334, number of tests: 3, total length: 105
[MASTER] 04:56:01.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:676.3333333333334 - branchDistance:0.0 - coverage:317.33333333333337 - ex: 0 - tex: 20
[MASTER] 04:56:01.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 676.3333333333334, number of tests: 10, total length: 235
[MASTER] 04:56:01.903 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:666.6666666666666 - branchDistance:0.0 - coverage:307.66666666666663 - ex: 0 - tex: 16
[MASTER] 04:56:01.903 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 666.6666666666666, number of tests: 8, total length: 215
[MASTER] 04:56:02.374 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:666.3333333333334 - branchDistance:0.0 - coverage:307.33333333333337 - ex: 0 - tex: 4
[MASTER] 04:56:02.374 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 666.3333333333334, number of tests: 2, total length: 75
[MASTER] 04:56:02.697 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:665.6666666666666 - branchDistance:0.0 - coverage:306.66666666666663 - ex: 0 - tex: 10
[MASTER] 04:56:02.697 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 665.6666666666666, number of tests: 7, total length: 128
[MASTER] 04:56:03.241 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:665.3333333333334 - branchDistance:0.0 - coverage:306.33333333333337 - ex: 0 - tex: 6
[MASTER] 04:56:03.241 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 665.3333333333334, number of tests: 3, total length: 101
[MASTER] 04:56:03.447 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:661.3333333333334 - branchDistance:0.0 - coverage:302.33333333333337 - ex: 0 - tex: 4
[MASTER] 04:56:03.447 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 661.3333333333334, number of tests: 2, total length: 54
[MASTER] 04:56:03.491 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:659.6666666666666 - branchDistance:0.0 - coverage:300.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:56:03.491 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 659.6666666666666, number of tests: 7, total length: 172
[MASTER] 04:56:03.523 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:653.6666666666666 - branchDistance:0.0 - coverage:294.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:56:03.523 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 653.6666666666666, number of tests: 7, total length: 118
[MASTER] 04:56:04.467 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:633.6666666666666 - branchDistance:0.0 - coverage:274.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:56:04.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 633.6666666666666, number of tests: 7, total length: 189
[MASTER] 04:56:04.972 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:632.6666666666666 - branchDistance:0.0 - coverage:273.66666666666663 - ex: 0 - tex: 10
[MASTER] 04:56:04.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 632.6666666666666, number of tests: 5, total length: 92
[MASTER] 04:56:05.238 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:627.6666666666666 - branchDistance:0.0 - coverage:268.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:56:05.239 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 627.6666666666666, number of tests: 7, total length: 142
[MASTER] 04:56:05.991 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:626.6666666666666 - branchDistance:0.0 - coverage:267.66666666666663 - ex: 0 - tex: 10
[MASTER] 04:56:05.991 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 626.6666666666666, number of tests: 5, total length: 97
[MASTER] 04:56:06.199 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:625.6666666666666 - branchDistance:0.0 - coverage:266.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:56:06.199 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 625.6666666666666, number of tests: 7, total length: 159
[MASTER] 04:56:06.372 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:622.6666666666666 - branchDistance:0.0 - coverage:263.66666666666663 - ex: 0 - tex: 14
[MASTER] 04:56:06.372 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 622.6666666666666, number of tests: 7, total length: 131
[MASTER] 04:56:06.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:617.6666666666666 - branchDistance:0.0 - coverage:258.66666666666663 - ex: 0 - tex: 16
[MASTER] 04:56:06.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 617.6666666666666, number of tests: 8, total length: 187
[MASTER] 04:56:06.929 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:615.6666666666666 - branchDistance:0.0 - coverage:256.66666666666663 - ex: 0 - tex: 16
[MASTER] 04:56:06.929 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 615.6666666666666, number of tests: 8, total length: 177
[MASTER] 04:56:07.507 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:610.6666666666666 - branchDistance:0.0 - coverage:251.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:07.507 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 610.6666666666666, number of tests: 9, total length: 215
[MASTER] 04:56:08.066 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:609.6666666666666 - branchDistance:0.0 - coverage:250.66666666666666 - ex: 0 - tex: 16
[MASTER] 04:56:08.066 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 609.6666666666666, number of tests: 8, total length: 179
[MASTER] 04:56:08.508 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:605.6666666666666 - branchDistance:0.0 - coverage:246.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:08.508 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 605.6666666666666, number of tests: 9, total length: 187
[MASTER] 04:56:08.821 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:604.6666666666666 - branchDistance:0.0 - coverage:245.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:08.821 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 604.6666666666666, number of tests: 9, total length: 218
[MASTER] 04:56:09.530 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:596.6666666666666 - branchDistance:0.0 - coverage:237.66666666666666 - ex: 0 - tex: 16
[MASTER] 04:56:09.530 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 596.6666666666666, number of tests: 8, total length: 173
[MASTER] 04:56:10.102 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:595.6666666666666 - branchDistance:0.0 - coverage:236.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:10.103 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 595.6666666666666, number of tests: 9, total length: 198
[MASTER] 04:56:10.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:594.6666666666666 - branchDistance:0.0 - coverage:235.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:10.175 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 594.6666666666666, number of tests: 9, total length: 181
[MASTER] 04:56:10.186 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:591.6666666666666 - branchDistance:0.0 - coverage:232.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:10.186 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 591.6666666666666, number of tests: 9, total length: 211
[MASTER] 04:56:10.865 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:586.6666666666666 - branchDistance:0.0 - coverage:227.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:10.865 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 586.6666666666666, number of tests: 9, total length: 205
[MASTER] 04:56:12.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:585.6666666666666 - branchDistance:0.0 - coverage:226.66666666666666 - ex: 0 - tex: 16
[MASTER] 04:56:12.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 585.6666666666666, number of tests: 9, total length: 180
[MASTER] 04:56:12.687 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:581.6666666666666 - branchDistance:0.0 - coverage:222.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:12.687 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 581.6666666666666, number of tests: 9, total length: 208
[MASTER] 04:56:12.713 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:578.6666666666666 - branchDistance:0.0 - coverage:219.66666666666666 - ex: 0 - tex: 20
[MASTER] 04:56:12.713 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 578.6666666666666, number of tests: 10, total length: 238
[MASTER] 04:56:14.318 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:576.6666666666666 - branchDistance:0.0 - coverage:217.66666666666666 - ex: 0 - tex: 20
[MASTER] 04:56:14.318 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 576.6666666666666, number of tests: 10, total length: 238
[MASTER] 04:56:14.411 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:560.6666666666666 - branchDistance:0.0 - coverage:201.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:14.411 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 560.6666666666666, number of tests: 10, total length: 211
[MASTER] 04:56:14.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:558.6666666666666 - branchDistance:0.0 - coverage:199.66666666666666 - ex: 0 - tex: 20
[MASTER] 04:56:14.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 558.6666666666666, number of tests: 11, total length: 249
[MASTER] 04:56:15.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:556.6666666666666 - branchDistance:0.0 - coverage:197.66666666666666 - ex: 0 - tex: 20
[MASTER] 04:56:15.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 556.6666666666666, number of tests: 11, total length: 242
[MASTER] 04:56:15.973 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:554.6666666666666 - branchDistance:0.0 - coverage:195.66666666666666 - ex: 0 - tex: 22
[MASTER] 04:56:15.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 554.6666666666666, number of tests: 11, total length: 243
[MASTER] 04:56:17.021 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:553.6666666666666 - branchDistance:0.0 - coverage:194.66666666666666 - ex: 0 - tex: 18
[MASTER] 04:56:17.022 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 553.6666666666666, number of tests: 10, total length: 198
[MASTER] 04:56:17.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:552.6666666666666 - branchDistance:0.0 - coverage:193.66666666666666 - ex: 0 - tex: 24
[MASTER] 04:56:17.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 552.6666666666666, number of tests: 12, total length: 255
[MASTER] 04:56:17.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:551.6666666666666 - branchDistance:0.0 - coverage:192.66666666666666 - ex: 0 - tex: 20
[MASTER] 04:56:17.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 551.6666666666666, number of tests: 10, total length: 197
[MASTER] 04:56:17.810 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:541.1406926406926 - branchDistance:0.0 - coverage:182.14069264069263 - ex: 0 - tex: 22
[MASTER] 04:56:17.810 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 541.1406926406926, number of tests: 11, total length: 239
[MASTER] 04:56:18.071 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:538.1406926406926 - branchDistance:0.0 - coverage:179.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:18.071 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 538.1406926406926, number of tests: 12, total length: 270
[MASTER] 04:56:18.736 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:537.1406926406926 - branchDistance:0.0 - coverage:178.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:18.736 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 537.1406926406926, number of tests: 13, total length: 281
[MASTER] 04:56:19.327 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:536.1406926406926 - branchDistance:0.0 - coverage:177.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:19.327 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 536.1406926406926, number of tests: 12, total length: 266
[MASTER] 04:56:19.414 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:534.1406926406926 - branchDistance:0.0 - coverage:175.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:19.414 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 534.1406926406926, number of tests: 13, total length: 298
[MASTER] 04:56:19.679 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:532.1406926406926 - branchDistance:0.0 - coverage:173.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:19.679 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 532.1406926406926, number of tests: 14, total length: 303
[MASTER] 04:56:20.141 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:530.1406926406926 - branchDistance:0.0 - coverage:171.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:20.141 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 530.1406926406926, number of tests: 13, total length: 280
[MASTER] 04:56:21.350 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:529.1406926406926 - branchDistance:0.0 - coverage:170.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:21.350 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 529.1406926406926, number of tests: 14, total length: 311
[MASTER] 04:56:22.346 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:528.1406926406926 - branchDistance:0.0 - coverage:169.14069264069263 - ex: 0 - tex: 28
[MASTER] 04:56:22.346 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 528.1406926406926, number of tests: 14, total length: 345
[MASTER] 04:56:23.628 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:527.1406926406926 - branchDistance:0.0 - coverage:168.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:23.628 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 527.1406926406926, number of tests: 14, total length: 308
[MASTER] 04:56:24.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:526.1406926406926 - branchDistance:0.0 - coverage:167.14069264069263 - ex: 0 - tex: 28
[MASTER] 04:56:24.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 526.1406926406926, number of tests: 14, total length: 337
[MASTER] 04:56:25.300 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:525.1406926406926 - branchDistance:0.0 - coverage:166.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:25.300 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 525.1406926406926, number of tests: 14, total length: 309
[MASTER] 04:56:25.698 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:524.1406926406926 - branchDistance:0.0 - coverage:165.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:25.698 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 524.1406926406926, number of tests: 15, total length: 328
[MASTER] 04:56:26.827 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:523.1406926406926 - branchDistance:0.0 - coverage:164.14069264069263 - ex: 0 - tex: 28
[MASTER] 04:56:26.827 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 523.1406926406926, number of tests: 15, total length: 347
[MASTER] 04:56:26.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:522.1406926406926 - branchDistance:0.0 - coverage:163.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:26.976 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 522.1406926406926, number of tests: 15, total length: 331
[MASTER] 04:56:27.590 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:520.1406926406926 - branchDistance:0.0 - coverage:161.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:27.590 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 520.1406926406926, number of tests: 14, total length: 301
[MASTER] 04:56:28.579 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:519.1406926406926 - branchDistance:0.0 - coverage:160.14069264069263 - ex: 0 - tex: 24
[MASTER] 04:56:28.579 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 519.1406926406926, number of tests: 15, total length: 320
[MASTER] 04:56:28.874 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:518.1406926406926 - branchDistance:0.0 - coverage:159.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:28.874 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 518.1406926406926, number of tests: 15, total length: 336
[MASTER] 04:56:28.905 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:517.1406926406926 - branchDistance:0.0 - coverage:158.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:28.905 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 517.1406926406926, number of tests: 15, total length: 330
[MASTER] 04:56:30.057 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:516.1406926406926 - branchDistance:0.0 - coverage:157.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:30.057 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 516.1406926406926, number of tests: 15, total length: 329
[MASTER] 04:56:30.648 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:515.1406926406926 - branchDistance:0.0 - coverage:156.14069264069263 - ex: 0 - tex: 28
[MASTER] 04:56:30.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 515.1406926406926, number of tests: 15, total length: 331
[MASTER] 04:56:32.026 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:514.1406926406926 - branchDistance:0.0 - coverage:155.14069264069263 - ex: 0 - tex: 26
[MASTER] 04:56:32.027 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 514.1406926406926, number of tests: 15, total length: 331
[MASTER] 04:56:32.180 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:513.1406926406926 - branchDistance:0.0 - coverage:154.14069264069263 - ex: 0 - tex: 28
[MASTER] 04:56:32.181 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 513.1406926406926, number of tests: 16, total length: 341
[MASTER] 04:56:32.760 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:510.14069264069263 - branchDistance:0.0 - coverage:151.14069264069263 - ex: 0 - tex: 28
[MASTER] 04:56:32.760 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 510.14069264069263, number of tests: 15, total length: 339
[MASTER] 04:56:35.278 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:510.08186911128087 - branchDistance:0.0 - coverage:151.08186911128087 - ex: 0 - tex: 32
[MASTER] 04:56:35.279 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 510.08186911128087, number of tests: 17, total length: 390
[MASTER] 04:56:35.474 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:509.14069264069263 - branchDistance:0.0 - coverage:150.14069264069263 - ex: 0 - tex: 30
[MASTER] 04:56:35.474 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 509.14069264069263, number of tests: 16, total length: 343
[MASTER] 04:56:35.885 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:508.08186911128087 - branchDistance:0.0 - coverage:149.08186911128087 - ex: 0 - tex: 30
[MASTER] 04:56:35.885 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 508.08186911128087, number of tests: 16, total length: 359
[MASTER] 04:56:36.968 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:507.08186911128087 - branchDistance:0.0 - coverage:148.08186911128087 - ex: 0 - tex: 32
[MASTER] 04:56:36.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 507.08186911128087, number of tests: 17, total length: 371
[MASTER] 04:56:38.290 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:506.08186911128087 - branchDistance:0.0 - coverage:147.08186911128087 - ex: 0 - tex: 32
[MASTER] 04:56:38.290 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 506.08186911128087, number of tests: 17, total length: 395
[MASTER] 04:56:38.378 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:505.08186911128087 - branchDistance:0.0 - coverage:146.08186911128087 - ex: 0 - tex: 34
[MASTER] 04:56:38.378 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 505.08186911128087, number of tests: 18, total length: 397
[MASTER] 04:56:39.380 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:504.08186911128087 - branchDistance:0.0 - coverage:145.08186911128087 - ex: 0 - tex: 34
[MASTER] 04:56:39.380 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 504.08186911128087, number of tests: 19, total length: 435
[MASTER] 04:56:41.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:503.08186911128087 - branchDistance:0.0 - coverage:144.08186911128087 - ex: 0 - tex: 38
[MASTER] 04:56:41.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 503.08186911128087, number of tests: 20, total length: 425
[MASTER] 04:56:41.886 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:501.08186911128087 - branchDistance:0.0 - coverage:142.08186911128087 - ex: 0 - tex: 38
[MASTER] 04:56:41.886 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 501.08186911128087, number of tests: 20, total length: 470
[MASTER] 04:56:45.205 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:500.08186911128087 - branchDistance:0.0 - coverage:141.08186911128087 - ex: 0 - tex: 38
[MASTER] 04:56:45.205 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 500.08186911128087, number of tests: 20, total length: 467
[MASTER] 04:56:46.963 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:499.6078431372549 - branchDistance:0.0 - coverage:140.60784313725492 - ex: 0 - tex: 40
[MASTER] 04:56:46.963 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 499.6078431372549, number of tests: 21, total length: 495
[MASTER] 04:56:47.001 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:499.08186911128087 - branchDistance:0.0 - coverage:140.08186911128087 - ex: 0 - tex: 40
[MASTER] 04:56:47.001 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 499.08186911128087, number of tests: 21, total length: 462
[MASTER] 04:56:47.327 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:497.6078431372549 - branchDistance:0.0 - coverage:138.60784313725492 - ex: 0 - tex: 40
[MASTER] 04:56:47.327 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 497.6078431372549, number of tests: 21, total length: 486
[MASTER] 04:56:50.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:497.08186911128087 - branchDistance:0.0 - coverage:138.08186911128087 - ex: 0 - tex: 40
[MASTER] 04:56:50.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 497.08186911128087, number of tests: 21, total length: 469
[MASTER] 04:56:50.656 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:491.6078431372549 - branchDistance:0.0 - coverage:132.60784313725492 - ex: 0 - tex: 40
[MASTER] 04:56:50.656 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 491.6078431372549, number of tests: 21, total length: 477
[MASTER] 04:56:51.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:490.6078431372549 - branchDistance:0.0 - coverage:131.60784313725492 - ex: 0 - tex: 40
[MASTER] 04:56:51.248 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 490.6078431372549, number of tests: 21, total length: 482
[MASTER] 04:56:52.837 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:488.6078431372549 - branchDistance:0.0 - coverage:129.60784313725492 - ex: 0 - tex: 40
[MASTER] 04:56:52.837 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 488.6078431372549, number of tests: 22, total length: 488
[MASTER] 04:56:54.670 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:486.6078431372549 - branchDistance:0.0 - coverage:127.6078431372549 - ex: 0 - tex: 40
[MASTER] 04:56:54.670 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 486.6078431372549, number of tests: 21, total length: 466
[MASTER] 04:56:58.343 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:486.5 - branchDistance:0.0 - coverage:127.5 - ex: 0 - tex: 40
[MASTER] 04:56:58.343 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 486.5, number of tests: 21, total length: 456
[MASTER] 04:56:59.396 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:485.6078431372549 - branchDistance:0.0 - coverage:126.6078431372549 - ex: 0 - tex: 42
[MASTER] 04:56:59.396 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 485.6078431372549, number of tests: 22, total length: 473
[MASTER] 04:57:00.768 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:485.5 - branchDistance:0.0 - coverage:126.5 - ex: 0 - tex: 42
[MASTER] 04:57:00.768 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 485.5, number of tests: 22, total length: 472
[MASTER] 04:57:08.304 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:484.5 - branchDistance:0.0 - coverage:125.5 - ex: 0 - tex: 38
[MASTER] 04:57:08.304 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 484.5, number of tests: 22, total length: 469
[MASTER] 04:57:09.632 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:481.5 - branchDistance:0.0 - coverage:122.5 - ex: 0 - tex: 38
[MASTER] 04:57:09.632 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 481.5, number of tests: 23, total length: 497
[MASTER] 04:57:12.725 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:480.5 - branchDistance:0.0 - coverage:121.5 - ex: 0 - tex: 40
[MASTER] 04:57:12.725 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 480.5, number of tests: 23, total length: 486
[MASTER] 04:57:15.498 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:479.5 - branchDistance:0.0 - coverage:120.5 - ex: 0 - tex: 40
[MASTER] 04:57:15.498 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 479.5, number of tests: 23, total length: 514
[MASTER] 04:57:18.833 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:478.5 - branchDistance:0.0 - coverage:119.5 - ex: 0 - tex: 38
[MASTER] 04:57:18.833 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 478.5, number of tests: 23, total length: 466
[MASTER] 04:57:20.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:477.5 - branchDistance:0.0 - coverage:118.5 - ex: 0 - tex: 40
[MASTER] 04:57:20.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 477.5, number of tests: 23, total length: 512
[MASTER] 04:57:21.890 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:476.5 - branchDistance:0.0 - coverage:117.5 - ex: 0 - tex: 42
[MASTER] 04:57:21.890 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 476.5, number of tests: 24, total length: 549
[MASTER] 04:57:23.665 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:475.5 - branchDistance:0.0 - coverage:116.5 - ex: 0 - tex: 44
[MASTER] 04:57:23.665 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 475.5, number of tests: 25, total length: 560
[MASTER] 04:57:31.000 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:473.5 - branchDistance:0.0 - coverage:114.5 - ex: 0 - tex: 36
[MASTER] 04:57:31.000 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 473.5, number of tests: 23, total length: 501
[MASTER] 04:57:34.467 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:473.4848484848485 - branchDistance:0.0 - coverage:114.48484848484848 - ex: 0 - tex: 38
[MASTER] 04:57:34.467 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 473.4848484848485, number of tests: 23, total length: 480
[MASTER] 04:57:34.995 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:472.5 - branchDistance:0.0 - coverage:113.5 - ex: 0 - tex: 36
[MASTER] 04:57:34.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 472.5, number of tests: 23, total length: 468
[MASTER] 04:57:37.729 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:472.4848484848485 - branchDistance:0.0 - coverage:113.48484848484848 - ex: 0 - tex: 36
[MASTER] 04:57:37.729 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 472.4848484848485, number of tests: 23, total length: 499
[MASTER] 04:57:42.995 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:472.4848484839172 - branchDistance:0.0 - coverage:113.48484848391716 - ex: 0 - tex: 40
[MASTER] 04:57:42.995 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 472.4848484839172, number of tests: 24, total length: 532
[MASTER] 04:57:55.756 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:472.48484848387136 - branchDistance:0.0 - coverage:113.48484848387135 - ex: 0 - tex: 32
[MASTER] 04:57:55.756 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 472.48484848387136, number of tests: 22, total length: 463
[MASTER] 04:57:56.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:471.4848484839172 - branchDistance:0.0 - coverage:112.48484848391716 - ex: 0 - tex: 34
[MASTER] 04:57:56.264 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 471.4848484839172, number of tests: 23, total length: 482
* Computation finished
