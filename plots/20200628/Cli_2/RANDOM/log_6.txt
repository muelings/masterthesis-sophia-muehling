* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.PosixParser
* Starting client
* Connecting to master process on port 3005
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.PosixParser
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 09:32:15.217 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 45
[MASTER] 09:32:16.884 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:32:17.988 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/PosixParser_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:32:17.996 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 09:32:18.284 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/PosixParser_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
[MASTER] 09:32:18.294 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:32:18.294 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 107 | Tests with assertion: 1
* Generated 1 tests with total length 19
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          3 / 300         
[MASTER] 09:32:18.806 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:32:18.824 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 09:32:18.849 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 09:32:18.850 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnrecognizedOptionException,UnrecognizedOptionException, parse:UnrecognizedOptionException]
[MASTER] 09:32:18.850 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:UnrecognizedOptionException at 10
[MASTER] 09:32:18.850 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnrecognizedOptionException,UnrecognizedOptionException, parse:UnrecognizedOptionException]
[MASTER] 09:32:19.057 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 11 
[MASTER] 09:32:19.063 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 38%
* Total number of goals: 45
* Number of covered goals: 17
* Generated 1 tests with total length 11
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'PosixParser_ESTest' to evosuite-tests
* Done!

* Computation finished
