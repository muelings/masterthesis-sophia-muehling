* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.PosixParser
* Starting client
* Connecting to master process on port 19080
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_2_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.PosixParser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 45
* Using seed 1593155156163
* Starting evolution
[MASTER] 09:05:58.558 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 09:05:58.562 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:05:59.703 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4705882352941178 - fitness:106.47619044955411 - branchDistance:0.0 - coverage:64.99999997336364 - ex: 0 - tex: 20
[MASTER] 09:05:59.704 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.47619044955411, number of tests: 10, total length: 254
[MASTER] 09:05:59.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4545454545454546 - fitness:95.74074070818833 - branchDistance:0.0 - coverage:53.99999996744759 - ex: 0 - tex: 12
[MASTER] 09:05:59.793 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.74074070818833, number of tests: 8, total length: 202
[MASTER] 09:06:00.008 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.625 - fitness:94.461206781373 - branchDistance:0.0 - coverage:65.87499988482126 - ex: 0 - tex: 6
[MASTER] 09:06:00.008 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 94.461206781373, number of tests: 4, total length: 95
[MASTER] 09:06:00.206 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6153846153846154 - fitness:93.65957443381515 - branchDistance:0.0 - coverage:64.99999996573004 - ex: 0 - tex: 10
[MASTER] 09:06:00.206 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.65957443381515, number of tests: 8, total length: 156
[MASTER] 09:06:00.578 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4814814814814814 - fitness:91.29850745896127 - branchDistance:0.0 - coverage:49.99999999627471 - ex: 0 - tex: 12
[MASTER] 09:06:00.578 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.29850745896127, number of tests: 7, total length: 151
[MASTER] 09:06:00.689 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:80.36842102599522 - branchDistance:0.0 - coverage:51.99999997336364 - ex: 0 - tex: 8
[MASTER] 09:06:00.689 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.36842102599522, number of tests: 6, total length: 188
[MASTER] 09:06:01.468 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:80.36842099575358 - branchDistance:0.0 - coverage:51.99999994312199 - ex: 0 - tex: 10
[MASTER] 09:06:01.468 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.36842099575358, number of tests: 8, total length: 203
[MASTER] 09:06:01.731 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6530612244897958 - fitness:73.04096738710828 - branchDistance:0.0 - coverage:44.66666571113063 - ex: 0 - tex: 4
[MASTER] 09:06:01.731 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.04096738710828, number of tests: 4, total length: 99
[MASTER] 09:06:01.883 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.625 - fitness:70.58620678872526 - branchDistance:0.0 - coverage:41.99999989217355 - ex: 0 - tex: 6
[MASTER] 09:06:01.883 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.58620678872526, number of tests: 6, total length: 119
[MASTER] 09:06:03.401 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:70.36842094480514 - branchDistance:0.0 - coverage:41.99999989217355 - ex: 0 - tex: 6
[MASTER] 09:06:03.401 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.36842094480514, number of tests: 6, total length: 175
[MASTER] 09:06:04.271 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6545454545454543 - fitness:70.36318397177554 - branchDistance:0.0 - coverage:41.99999989217355 - ex: 0 - tex: 8
[MASTER] 09:06:04.271 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.36318397177554, number of tests: 9, total length: 207
[MASTER] 09:06:04.891 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6582278481012658 - fitness:70.33564003058186 - branchDistance:0.0 - coverage:41.99999989217355 - ex: 0 - tex: 4
[MASTER] 09:06:04.892 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.33564003058186, number of tests: 6, total length: 134
[MASTER] 09:06:05.102 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:64.36842105263159 - branchDistance:0.0 - coverage:36.0 - ex: 0 - tex: 6
[MASTER] 09:06:05.102 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.36842105263159, number of tests: 8, total length: 188
[MASTER] 09:06:06.398 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:56.36842105263158 - branchDistance:0.0 - coverage:28.0 - ex: 0 - tex: 6
[MASTER] 09:06:06.398 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.36842105263158, number of tests: 7, total length: 187
[MASTER] 09:06:08.063 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.657142857142857 - fitness:56.34375 - branchDistance:0.0 - coverage:28.0 - ex: 0 - tex: 6
[MASTER] 09:06:08.063 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.34375, number of tests: 7, total length: 175
[MASTER] 09:06:08.165 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6486486486486487 - fitness:55.407407407407405 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 8
[MASTER] 09:06:08.165 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.407407407407405, number of tests: 7, total length: 173
[MASTER] 09:06:08.190 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:55.36842105263158 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 10
[MASTER] 09:06:08.190 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.36842105263158, number of tests: 8, total length: 177
[MASTER] 09:06:09.931 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6545454545454543 - fitness:55.36318407960199 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 10
[MASTER] 09:06:09.931 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.36318407960199, number of tests: 8, total length: 171
[MASTER] 09:06:10.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:54.36842105263158 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 6
[MASTER] 09:06:10.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.36842105263158, number of tests: 9, total length: 196
[MASTER] 09:06:11.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:50.36842105263158 - branchDistance:0.0 - coverage:22.0 - ex: 0 - tex: 10
[MASTER] 09:06:11.014 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.36842105263158, number of tests: 8, total length: 170
[MASTER] 09:06:11.946 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6545454545454543 - fitness:50.36318407960199 - branchDistance:0.0 - coverage:22.0 - ex: 0 - tex: 4
[MASTER] 09:06:11.946 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 50.36318407960199, number of tests: 7, total length: 144
[MASTER] 09:06:12.408 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6538461538461537 - fitness:47.36842105263158 - branchDistance:0.0 - coverage:19.0 - ex: 0 - tex: 6
[MASTER] 09:06:12.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.36842105263158, number of tests: 7, total length: 140
[MASTER] 09:06:12.947 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6545454545454543 - fitness:47.36318407960199 - branchDistance:0.0 - coverage:19.0 - ex: 0 - tex: 4
[MASTER] 09:06:12.947 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 47.36318407960199, number of tests: 7, total length: 143
[MASTER] 09:06:13.267 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.128260869565217 - fitness:38.99978804578211 - branchDistance:0.0 - coverage:18.5 - ex: 0 - tex: 6
[MASTER] 09:06:13.268 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 38.99978804578211, number of tests: 7, total length: 154
[MASTER] 09:06:14.242 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.128260869565217 - fitness:33.99978804578211 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 6
[MASTER] 09:06:14.242 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.99978804578211, number of tests: 7, total length: 148
[MASTER] 09:06:15.385 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.132423724094037 - fitness:33.98397197420635 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 6
[MASTER] 09:06:15.385 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 33.98397197420635, number of tests: 7, total length: 159
[MASTER] 09:06:16.245 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.290322580645161 - fitness:30.397435897435898 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 6
[MASTER] 09:06:16.246 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.397435897435898, number of tests: 7, total length: 162
[MASTER] 09:06:18.344 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.297556805103417 - fitness:30.37917395504269 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 6
[MASTER] 09:06:18.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.37917395504269, number of tests: 7, total length: 165
[MASTER] 09:06:18.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.303056027164686 - fitness:30.365319865319865 - branchDistance:0.0 - coverage:13.5 - ex: 0 - tex: 6
[MASTER] 09:06:18.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 30.365319865319865, number of tests: 7, total length: 165
[MASTER] 09:06:18.422 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.290322580645161 - fitness:27.897435897435898 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 4
[MASTER] 09:06:18.422 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.897435897435898, number of tests: 6, total length: 147
[MASTER] 09:06:19.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.297556805103417 - fitness:27.87917395504269 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 4
[MASTER] 09:06:19.114 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.87917395504269, number of tests: 6, total length: 148
[MASTER] 09:06:19.160 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.302898010876853 - fitness:27.865717615520815 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 6
[MASTER] 09:06:19.160 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 27.865717615520815, number of tests: 6, total length: 154
[MASTER] 09:06:19.234 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.297556805103417 - fitness:26.87917395504269 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 2
[MASTER] 09:06:19.234 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.87917395504269, number of tests: 7, total length: 172
[MASTER] 09:06:20.400 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.305538648813147 - fitness:26.859073359073356 - branchDistance:0.0 - coverage:10.0 - ex: 0 - tex: 2
[MASTER] 09:06:20.400 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.859073359073356, number of tests: 7, total length: 176
[MASTER] 09:06:20.813 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.239755884917175 - fitness:26.026267989381026 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 2
[MASTER] 09:06:20.813 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 26.026267989381026, number of tests: 8, total length: 207
[MASTER] 09:06:20.991 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.302898010876853 - fitness:24.865717615520815 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 09:06:20.991 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 24.865717615520815, number of tests: 6, total length: 148
[MASTER] 09:06:22.569 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.241120886282177 - fitness:22.134271706468446 - branchDistance:0.0 - coverage:9.0 - ex: 0 - tex: 8
[MASTER] 09:06:22.569 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.134271706468446, number of tests: 8, total length: 186
[MASTER] 09:06:22.917 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.241120886282177 - fitness:21.134271706468446 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 09:06:22.917 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.134271706468446, number of tests: 8, total length: 186
[MASTER] 09:06:23.749 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.298504872182688 - fitness:21.05036347393236 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 09:06:23.749 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.05036347393236, number of tests: 7, total length: 184
[MASTER] 09:06:25.719 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.299078625300989 - fitness:21.049530377400565 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 09:06:25.719 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.049530377400565, number of tests: 7, total length: 186
[MASTER] 09:06:26.653 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.3023041474654375 - fitness:21.044849023090585 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 09:06:26.653 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 21.044849023090585, number of tests: 7, total length: 178
[MASTER] 09:06:27.358 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.29907862530099 - fitness:18.709606425795933 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 09:06:27.358 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.709606425795933, number of tests: 7, total length: 182
[MASTER] 09:06:29.156 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.300028546959748 - fitness:18.70871095590477 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 09:06:29.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.70871095590477, number of tests: 7, total length: 184
[MASTER] 09:06:29.196 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.309210155942127 - fitness:18.700064164698496 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 8
[MASTER] 09:06:29.196 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 18.700064164698496, number of tests: 8, total length: 216
[MASTER] 09:06:30.997 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.304528686916255 - fitness:17.127089020998646 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 09:06:30.997 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.127089020998646, number of tests: 7, total length: 181
[MASTER] 09:06:32.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.307173124642107 - fitness:17.12534275639419 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 6
[MASTER] 09:06:32.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.12534275639419, number of tests: 7, total length: 172
[MASTER] 09:06:33.108 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.30800590713398 - fitness:17.124792980643427 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 09:06:33.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.124792980643427, number of tests: 7, total length: 188
[MASTER] 09:06:38.067 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.308672374804306 - fitness:17.12435305408719 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 09:06:38.067 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.12435305408719, number of tests: 7, total length: 160
[MASTER] 09:06:40.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.320991365151931 - fitness:17.116230020486412 - branchDistance:0.0 - coverage:8.0 - ex: 0 - tex: 4
[MASTER] 09:06:40.782 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 17.116230020486412, number of tests: 6, total length: 150
[MASTER] 09:06:41.454 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.308672374804306 - fitness:16.12435305408719 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:41.454 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.12435305408719, number of tests: 7, total length: 163
[MASTER] 09:06:41.644 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.320991365151931 - fitness:16.116230020486412 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:41.644 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.116230020486412, number of tests: 8, total length: 167
[MASTER] 09:06:44.353 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.321022157913399 - fitness:16.11620973636292 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:44.353 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.11620973636292, number of tests: 7, total length: 158
[MASTER] 09:06:45.728 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.321376721110898 - fitness:16.115976182163514 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:45.728 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.115976182163514, number of tests: 7, total length: 157
[MASTER] 09:06:46.869 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.321397644471224 - fitness:16.115962400164186 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:46.869 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.115962400164186, number of tests: 7, total length: 156
[MASTER] 09:06:49.405 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.321418093210074 - fitness:16.1159489308383 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:49.405 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 16.1159489308383, number of tests: 7, total length: 151
[MASTER] 09:06:50.216 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.32145904462034 - fitness:14.98252878344568 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:50.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.98252878344568, number of tests: 7, total length: 153
[MASTER] 09:06:52.326 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.321478877834668 - fitness:14.982519113634964 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:52.326 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.982519113634964, number of tests: 7, total length: 151
[MASTER] 09:06:54.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.32217751948404 - fitness:14.982178503510305 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:54.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.982178503510305, number of tests: 7, total length: 149
[MASTER] 09:06:55.582 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.322416328440347 - fitness:14.982062084135045 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 09:06:55.582 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.982062084135045, number of tests: 8, total length: 185
[MASTER] 09:06:59.836 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.322433451217432 - fitness:14.982053736929728 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:06:59.836 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.982053736929728, number of tests: 7, total length: 148
[MASTER] 09:07:00.288 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.322600340801984 - fitness:14.981972380750001 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:00.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981972380750001, number of tests: 7, total length: 146
[MASTER] 09:07:05.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.322627032344531 - fitness:14.98195936919755 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 09:07:05.589 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.98195936919755, number of tests: 8, total length: 156
[MASTER] 09:07:06.906 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.322665279968785 - fitness:14.98194072438855 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:06.906 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.98194072438855, number of tests: 7, total length: 135
[MASTER] 09:07:07.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.322677849708501 - fitness:14.981934596960528 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:07.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981934596960528, number of tests: 7, total length: 137
[MASTER] 09:07:13.273 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.322929449092054 - fitness:14.98181195093013 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:13.273 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.98181195093013, number of tests: 7, total length: 132
[MASTER] 09:07:16.627 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323017984171814 - fitness:14.981768794154188 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:16.627 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981768794154188, number of tests: 7, total length: 129
[MASTER] 09:07:20.070 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323150434997967 - fitness:14.981704231469534 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:20.070 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981704231469534, number of tests: 7, total length: 123
[MASTER] 09:07:20.202 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323399617172594 - fitness:14.981582771740037 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 09:07:20.203 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981582771740037, number of tests: 8, total length: 138
[MASTER] 09:07:22.897 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323419090579234 - fitness:14.981573279928098 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:22.897 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981573279928098, number of tests: 7, total length: 123
[MASTER] 09:07:25.639 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.32346777887136 - fitness:14.98154954818348 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:25.639 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.98154954818348, number of tests: 7, total length: 120
[MASTER] 09:07:27.612 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323473556834038 - fitness:14.981546731888079 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:27.612 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981546731888079, number of tests: 7, total length: 120
[MASTER] 09:07:29.556 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.32383572659492 - fitness:14.981370207585599 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:29.556 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981370207585599, number of tests: 7, total length: 118
[MASTER] 09:07:32.861 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323887414500355 - fitness:14.981345015234343 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:32.861 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981345015234343, number of tests: 7, total length: 120
[MASTER] 09:07:36.374 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323947144566109 - fitness:14.981315903412538 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:36.374 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981315903412538, number of tests: 7, total length: 117
[MASTER] 09:07:44.007 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.323999864103698 - fitness:14.98129020865202 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 09:07:44.007 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.98129020865202, number of tests: 7, total length: 114
[MASTER] 09:07:48.155 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.324050173218158 - fitness:14.981265688874167 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:48.155 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.981265688874167, number of tests: 7, total length: 115
[MASTER] 09:07:49.126 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.322806654185888 - fitness:14.126397384873489 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:49.126 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.126397384873489, number of tests: 7, total length: 111
[MASTER] 09:07:50.377 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.323342895863185 - fitness:14.12619612526445 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:50.377 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.12619612526445, number of tests: 7, total length: 111
[MASTER] 09:07:51.134 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.32388491180017 - fitness:14.125992711925594 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 09:07:51.135 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 14.125992711925594, number of tests: 8, total length: 131
[MASTER] 09:07:55.767 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.32389597388344 - fitness:13.457354710075156 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:55.767 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.457354710075156, number of tests: 7, total length: 120
[MASTER] 09:07:58.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.323906282391107 - fitness:13.457351639922866 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:07:58.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.457351639922866, number of tests: 7, total length: 120
[MASTER] 09:08:00.197 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.32393679424631 - fitness:13.457342552687688 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:08:00.197 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.457342552687688, number of tests: 7, total length: 120
[MASTER] 09:08:15.050 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.326674167082132 - fitness:13.456527413992946 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:08:15.050 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.456527413992946, number of tests: 7, total length: 112
[MASTER] 09:08:16.345 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.326679583138496 - fitness:13.456525801433514 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:08:16.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.456525801433514, number of tests: 7, total length: 113
[MASTER] 09:08:18.350 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.326744574691833 - fitness:13.456506451129034 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:08:18.350 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.456506451129034, number of tests: 7, total length: 111
[MASTER] 09:08:41.210 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.326812932476937 - fitness:13.45648609872533 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:08:41.211 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.45648609872533, number of tests: 7, total length: 108
[MASTER] 09:08:52.614 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.326860404623538 - fitness:13.45647196476554 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 09:08:52.614 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.45647196476554, number of tests: 8, total length: 128
[MASTER] 09:09:00.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.326909590574612 - fitness:13.45645732062918 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:09:00.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.45645732062918, number of tests: 7, total length: 108
[MASTER] 09:09:15.404 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.326947330331684 - fitness:13.456446084422188 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:09:15.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.456446084422188, number of tests: 7, total length: 106
[MASTER] 09:09:26.271 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.32698128562634 - fitness:13.456435974997637 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 6
[MASTER] 09:09:26.271 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.456435974997637, number of tests: 8, total length: 139
[MASTER] 09:09:30.846 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.328390773335464 - fitness:13.45601636481268 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:09:30.846 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.45601636481268, number of tests: 7, total length: 106
[MASTER] 09:09:34.789 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.32839651855136 - fitness:13.456014654570765 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:09:34.789 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.456014654570765, number of tests: 7, total length: 105
[MASTER] 09:09:38.440 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.32844221542779 - fitness:13.456001051514676 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:09:38.440 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.456001051514676, number of tests: 7, total length: 105
[MASTER] 09:10:10.030 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.32876232883617 - fitness:13.45590576198768 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:10:10.030 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.45590576198768, number of tests: 7, total length: 105
[MASTER] 09:10:15.316 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.328796258423548 - fitness:13.455895662217426 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 4
[MASTER] 09:10:15.316 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 13.455895662217426, number of tests: 7, total length: 105
[MASTER] 09:10:59.579 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1885 generations, 1541648 statements, best individuals have fitness: 13.455895662217426
[MASTER] 09:10:59.582 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 89%
* Total number of goals: 45
* Number of covered goals: 40
* Generated 7 tests with total length 104
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     13 / 0           
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
[MASTER] 09:10:59.772 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:10:59.796 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 80 
[MASTER] 09:10:59.863 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 09:10:59.864 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 09:10:59.865 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 45
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'PosixParser_ESTest' to evosuite-tests
* Done!

* Computation finished
