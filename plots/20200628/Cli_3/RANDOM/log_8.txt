* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.TypeHandler
* Starting client
* Connecting to master process on port 19526
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_3_fixed/target/classes/
[MASTER] 10:35:53.246 [logback-1] WARN  CheapPurityAnalyzer - org.apache.commons.lang.math.NumberUtils was not found in the inheritance tree. Using DEFAULT value for cheap-purity analysis
[MASTER] 10:35:53.246 [logback-1] WARN  InheritanceTree - Class not in inheritance graph: org.apache.commons.lang.math.NumberUtils
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.TypeHandler
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 10:35:53.319 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 31
[MASTER] 10:35:53.448 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:35:54.771 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_1_tmp__ESTest.class' should be in target project, but could not be found!
Generated test with 1 assertions.
[MASTER] 10:35:54.800 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 10:35:55.179 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 10:35:55.200 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 10:35:55.200 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 2 | Tests with assertion: 1
* Generated 1 tests with total length 17
* GA-Budget:
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          1 / 300         
[MASTER] 10:35:55.612 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:35:55.636 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 11 
[MASTER] 10:35:55.652 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 10:35:55.653 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createNumber:NoClassDefFoundError]
[MASTER] 10:35:55.653 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: createNumber:NoClassDefFoundError at 10
[MASTER] 10:35:55.653 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createNumber:NoClassDefFoundError]
[MASTER] 10:35:55.907 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
* Going to analyze the coverage criteria
[MASTER] 10:35:55.910 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 6%
* Total number of goals: 31
* Number of covered goals: 2
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeHandler_ESTest' to evosuite-tests
* Done!

* Computation finished
