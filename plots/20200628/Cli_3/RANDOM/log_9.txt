* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.TypeHandler
* Starting client
* Connecting to master process on port 20545
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_3_fixed/target/classes/
[MASTER] 10:41:10.594 [logback-1] WARN  CheapPurityAnalyzer - org.apache.commons.lang.math.NumberUtils was not found in the inheritance tree. Using DEFAULT value for cheap-purity analysis
[MASTER] 10:41:10.595 [logback-1] WARN  InheritanceTree - Class not in inheritance graph: org.apache.commons.lang.math.NumberUtils
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.TypeHandler
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 10:41:10.697 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 31
[MASTER] 10:41:10.778 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 10:41:12.280 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 10:41:12.309 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 10:41:12.683 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/cli/TypeHandler_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 1 | Tests with assertion: 1
[MASTER] 10:41:12.702 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 10:41:12.706 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 14
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 10:41:13.197 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 10:41:13.215 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 10 
[MASTER] 10:41:13.227 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 10:41:13.228 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createValue:ClassCastException, createNumber:NoClassDefFoundError, ClassCastException:createValue-47,]
[MASTER] 10:41:13.228 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: createValue:ClassCastException at 9
[MASTER] 10:41:13.228 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [createValue:ClassCastException, createNumber:NoClassDefFoundError, ClassCastException:createValue-47,]
[MASTER] 10:41:13.228 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: createNumber:NoClassDefFoundError at 2
[MASTER] 10:41:13.228 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [createValue:ClassCastException, createNumber:NoClassDefFoundError, ClassCastException:createValue-47,]
[MASTER] 10:41:13.458 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 3 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 10:41:13.463 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 13%
* Total number of goals: 31
* Number of covered goals: 4
* Generated 1 tests with total length 3
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'TypeHandler_ESTest' to evosuite-tests
* Done!

* Computation finished
