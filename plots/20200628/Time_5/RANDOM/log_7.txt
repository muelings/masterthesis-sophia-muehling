* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.joda.time.Period
* Starting client
* Connecting to master process on port 13087
* Analyzing classpath: 
  - ../defects4j_compiled/Time_5_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.joda.time.Period
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
[MASTER] 09:03:21.658 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 131
[MASTER] 09:03:27.141 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 09:03:28.748 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:03:28.764 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 09:03:29.099 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 0 assertions.
[MASTER] 09:03:32.763 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 09:03:33.058 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 09:03:33.069 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 09:03:33.333 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/joda/time/Period_7_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 09:03:33.347 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 09:03:33.347 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 430 | Tests with assertion: 1
* Generated 1 tests with total length 16
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                         11 / 300         
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 09:03:34.305 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 09:03:34.319 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 14 
[MASTER] 09:03:34.329 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 09:03:34.329 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 09:03:34.329 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: normalizedStandard:UnsupportedOperationException at 13
[MASTER] 09:03:34.329 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [UnsupportedOperationException:normalizedStandard-1640,UnsupportedOperationException:normalizedStandard-1640, normalizedStandard:UnsupportedOperationException]
[MASTER] 09:03:34.489 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 09:03:34.493 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 15%
* Total number of goals: 131
* Number of covered goals: 19
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Period_ESTest' to evosuite-tests
* Done!

* Computation finished
