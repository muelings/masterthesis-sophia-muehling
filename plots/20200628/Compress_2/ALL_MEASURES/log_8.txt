* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting client
* Connecting to master process on port 9161
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 11:46:57.120 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 11:46:57.124 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 59
* Using seed 1593078414534
* Starting evolution
[MASTER] 11:46:58.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:122.083333332402 - branchDistance:0.0 - coverage:78.333333332402 - ex: 0 - tex: 10
[MASTER] 11:46:58.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.083333332402, number of tests: 6, total length: 140
[MASTER] 11:48:01.365 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:116.083333332402 - branchDistance:0.0 - coverage:72.333333332402 - ex: 0 - tex: 12
[MASTER] 11:48:01.365 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.083333332402, number of tests: 7, total length: 183
[MASTER] 11:48:01.831 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:114.083333332402 - branchDistance:0.0 - coverage:70.333333332402 - ex: 0 - tex: 10
[MASTER] 11:48:01.831 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.083333332402, number of tests: 9, total length: 229
[MASTER] 11:48:02.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:104.4242424233111 - branchDistance:0.0 - coverage:72.333333332402 - ex: 0 - tex: 12
[MASTER] 11:48:02.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.4242424233111, number of tests: 8, total length: 209
[MASTER] 11:48:02.146 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:103.4242424233111 - branchDistance:0.0 - coverage:71.333333332402 - ex: 0 - tex: 6
[MASTER] 11:48:02.146 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.4242424233111, number of tests: 8, total length: 175
[MASTER] 11:48:02.801 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:101.37878787785655 - branchDistance:0.0 - coverage:69.28787878694746 - ex: 0 - tex: 4
[MASTER] 11:48:02.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.37878787785655, number of tests: 8, total length: 175
[MASTER] 11:48:35.073 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:101.17878787785656 - branchDistance:0.0 - coverage:69.08787878694747 - ex: 0 - tex: 12
[MASTER] 11:48:35.073 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.17878787785656, number of tests: 10, total length: 224
[MASTER] 11:48:35.298 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:100.0909090909091 - branchDistance:0.0 - coverage:68.0 - ex: 0 - tex: 10
[MASTER] 11:48:35.298 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.0909090909091, number of tests: 8, total length: 163
[MASTER] 11:48:35.675 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:100.06388206388206 - branchDistance:0.0 - coverage:67.97297297297297 - ex: 0 - tex: 8
[MASTER] 11:48:35.676 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.06388206388206, number of tests: 9, total length: 215
[MASTER] 11:48:35.912 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:100.0581222056632 - branchDistance:0.0 - coverage:67.9672131147541 - ex: 0 - tex: 10
[MASTER] 11:48:35.912 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.0581222056632, number of tests: 9, total length: 218
[MASTER] 11:48:36.184 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:100.0564263322884 - branchDistance:0.0 - coverage:67.9655172413793 - ex: 0 - tex: 10
[MASTER] 11:48:36.184 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.0564263322884, number of tests: 9, total length: 220
[MASTER] 11:48:36.346 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:97.86388206388207 - branchDistance:0.0 - coverage:65.77297297297298 - ex: 0 - tex: 12
[MASTER] 11:48:36.347 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.86388206388207, number of tests: 10, total length: 261
[MASTER] 11:48:46.679 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:95.84545454452322 - branchDistance:0.0 - coverage:63.75454545361413 - ex: 0 - tex: 12
[MASTER] 11:48:46.679 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.84545454452322, number of tests: 9, total length: 218
[MASTER] 11:48:46.867 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:95.04545454452322 - branchDistance:0.0 - coverage:62.95454545361413 - ex: 0 - tex: 10
[MASTER] 11:48:46.867 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 95.04545454452322, number of tests: 9, total length: 229
[MASTER] 11:48:47.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 2.6666666666666665 - fitness:93.84545454452322 - branchDistance:0.0 - coverage:61.75454545361413 - ex: 0 - tex: 8
[MASTER] 11:48:47.678 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.84545454452322, number of tests: 8, total length: 207
[MASTER] 11:48:47.730 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:89.27297297297298 - branchDistance:0.0 - coverage:65.77297297297298 - ex: 0 - tex: 10
[MASTER] 11:48:47.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.27297297297298, number of tests: 10, total length: 223
[MASTER] 11:48:48.608 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:85.25454545361413 - branchDistance:0.0 - coverage:61.75454545361413 - ex: 0 - tex: 12
[MASTER] 11:48:48.608 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.25454545361413, number of tests: 10, total length: 243
[MASTER] 11:49:09.658 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:84.55454545268282 - branchDistance:0.0 - coverage:61.05454545268282 - ex: 0 - tex: 14
[MASTER] 11:49:09.658 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.55454545268282, number of tests: 11, total length: 283
[MASTER] 11:49:31.247 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:84.25454545454545 - branchDistance:0.0 - coverage:60.75454545454545 - ex: 0 - tex: 12
[MASTER] 11:49:31.247 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.25454545454545, number of tests: 10, total length: 247
[MASTER] 11:49:41.583 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:83.25454545454545 - branchDistance:0.0 - coverage:59.75454545454545 - ex: 0 - tex: 8
[MASTER] 11:49:41.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.25454545454545, number of tests: 9, total length: 231
[MASTER] 11:49:41.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:79.45454545361413 - branchDistance:0.0 - coverage:55.95454545361413 - ex: 0 - tex: 14
[MASTER] 11:49:41.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.45454545361413, number of tests: 11, total length: 258
[MASTER] 11:50:23.035 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.205128205128205 - fitness:77.87660950962231 - branchDistance:0.0 - coverage:61.05454545268282 - ex: 0 - tex: 12
[MASTER] 11:50:23.035 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.87660950962231, number of tests: 11, total length: 274
[MASTER] 11:50:23.502 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:77.45454545454545 - branchDistance:0.0 - coverage:53.95454545454545 - ex: 0 - tex: 12
[MASTER] 11:50:23.502 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.45454545454545, number of tests: 12, total length: 293
[MASTER] 11:50:54.214 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.205128205128205 - fitness:76.07660951055362 - branchDistance:0.0 - coverage:59.25454545361413 - ex: 0 - tex: 10
[MASTER] 11:50:54.214 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.07660951055362, number of tests: 12, total length: 315
[MASTER] 11:50:54.307 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.205128205128205 - fitness:72.07660950962232 - branchDistance:0.0 - coverage:55.25454545268281 - ex: 0 - tex: 16
[MASTER] 11:50:54.307 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.07660950962232, number of tests: 12, total length: 295
[MASTER] 11:50:54.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.205128205128205 - fitness:70.77660951148495 - branchDistance:0.0 - coverage:53.95454545454545 - ex: 0 - tex: 16
[MASTER] 11:50:54.479 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.77660951148495, number of tests: 12, total length: 299
[MASTER] 11:51:35.203 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.205128205128205 - fitness:70.27660951055363 - branchDistance:0.0 - coverage:53.45454545361413 - ex: 0 - tex: 12
[MASTER] 11:51:35.203 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.27660951055363, number of tests: 12, total length: 294
[MASTER] 11:51:35.855 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.205128205128205 - fitness:70.2632405265964 - branchDistance:0.0 - coverage:53.44117646965691 - ex: 0 - tex: 12
[MASTER] 11:51:35.855 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 70.2632405265964, number of tests: 12, total length: 293
[MASTER] 11:51:57.326 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:69.99999999906868 - branchDistance:0.0 - coverage:53.45454545361413 - ex: 0 - tex: 14
[MASTER] 11:51:57.326 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.99999999906868, number of tests: 12, total length: 296
[MASTER] 11:52:07.650 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 310s and 44 generations, 47661 statements, best individuals have fitness: 69.99999999906868
[MASTER] 11:52:07.654 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 46%
* Total number of goals: 59
* Number of covered goals: 27
* Generated 12 tests with total length 296
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        310 / 300          Finished!
	- ZeroFitness :                     70 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 11:52:07.906 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:52:07.945 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 227 
[MASTER] 11:52:08.041 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 11:52:08.041 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 11:52:08.043 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 11:52:08.044 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 11:52:08.045 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 11:52:08.045 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 11:52:08.045 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 11:52:08.045 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 11:52:08.045 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 11:52:08.045 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 11:52:08.046 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 59
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
