* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Starting client
* Connecting to master process on port 21195
* Analyzing classpath: 
  - ../defects4j_compiled/Compress_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.compress.archivers.ar.ArArchiveInputStream
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 59
[MASTER] 11:36:28.126 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
* Using seed 1593077785505
* Starting evolution
[MASTER] 11:36:28.131 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:36:29.185 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:143.75 - branchDistance:0.0 - coverage:100.0 - ex: 0 - tex: 14
[MASTER] 11:36:29.186 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 143.75, number of tests: 7, total length: 182
[MASTER] 11:36:29.282 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:137.25 - branchDistance:0.0 - coverage:93.5 - ex: 0 - tex: 18
[MASTER] 11:36:29.282 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 137.25, number of tests: 9, total length: 238
[MASTER] 11:36:30.085 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:130.58333333333331 - branchDistance:0.0 - coverage:86.83333333333333 - ex: 0 - tex: 12
[MASTER] 11:36:30.085 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.58333333333331, number of tests: 8, total length: 183
[MASTER] 11:36:30.150 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:110.33333333333333 - branchDistance:0.0 - coverage:86.83333333333333 - ex: 0 - tex: 16
[MASTER] 11:36:30.150 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.33333333333333, number of tests: 10, total length: 185
[MASTER] 11:37:01.687 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:109.0 - branchDistance:0.0 - coverage:85.5 - ex: 0 - tex: 16
[MASTER] 11:37:01.687 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.0, number of tests: 10, total length: 185
[MASTER] 11:37:02.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:108.0 - branchDistance:0.0 - coverage:84.5 - ex: 0 - tex: 16
[MASTER] 11:37:02.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.0, number of tests: 10, total length: 187
[MASTER] 11:37:02.775 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:105.0 - branchDistance:0.0 - coverage:81.5 - ex: 0 - tex: 16
[MASTER] 11:37:02.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.0, number of tests: 11, total length: 207
[MASTER] 11:37:13.324 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:104.52666666666667 - branchDistance:0.0 - coverage:76.16666666666666 - ex: 0 - tex: 16
[MASTER] 11:37:13.324 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.52666666666667, number of tests: 11, total length: 240
[MASTER] 11:37:24.002 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:103.3 - branchDistance:0.0 - coverage:79.8 - ex: 0 - tex: 16
[MASTER] 11:37:24.002 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.3, number of tests: 12, total length: 229
[MASTER] 11:37:24.054 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:102.82666666666668 - branchDistance:0.0 - coverage:74.46666666666667 - ex: 0 - tex: 14
[MASTER] 11:37:24.054 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.82666666666668, number of tests: 10, total length: 219
[MASTER] 11:37:24.096 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:99.5 - branchDistance:0.0 - coverage:76.0 - ex: 0 - tex: 18
[MASTER] 11:37:24.096 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.5, number of tests: 11, total length: 218
[MASTER] 11:37:35.040 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:99.02666666666667 - branchDistance:0.0 - coverage:70.66666666666666 - ex: 0 - tex: 12
[MASTER] 11:37:35.040 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.02666666666667, number of tests: 11, total length: 225
[MASTER] 11:37:35.330 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.1666666666666665 - fitness:97.02666666666667 - branchDistance:0.0 - coverage:68.66666666666666 - ex: 0 - tex: 16
[MASTER] 11:37:35.330 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 97.02666666666667, number of tests: 11, total length: 242
[MASTER] 11:37:36.100 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:93.42307692307692 - branchDistance:0.0 - coverage:69.92307692307692 - ex: 0 - tex: 20
[MASTER] 11:37:36.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.42307692307692, number of tests: 12, total length: 233
[MASTER] 11:37:36.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:92.42307692307692 - branchDistance:0.0 - coverage:68.92307692307692 - ex: 0 - tex: 22
[MASTER] 11:37:36.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.42307692307692, number of tests: 13, total length: 254
[MASTER] 11:37:37.149 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:91.16666666666666 - branchDistance:0.0 - coverage:67.66666666666666 - ex: 0 - tex: 16
[MASTER] 11:37:37.149 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.16666666666666, number of tests: 11, total length: 170
[MASTER] 11:37:47.971 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:90.94313725490196 - branchDistance:0.0 - coverage:67.44313725490196 - ex: 0 - tex: 18
[MASTER] 11:37:47.971 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.94313725490196, number of tests: 12, total length: 204
[MASTER] 11:37:59.156 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:90.10980392156863 - branchDistance:0.0 - coverage:66.60980392156863 - ex: 0 - tex: 16
[MASTER] 11:37:59.156 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 90.10980392156863, number of tests: 11, total length: 227
[MASTER] 11:38:00.256 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:89.94313725490196 - branchDistance:0.0 - coverage:66.44313725490196 - ex: 0 - tex: 18
[MASTER] 11:38:00.256 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 89.94313725490196, number of tests: 12, total length: 214
[MASTER] 11:38:00.781 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:88.44313725490196 - branchDistance:0.0 - coverage:64.94313725490196 - ex: 0 - tex: 24
[MASTER] 11:38:00.781 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 88.44313725490196, number of tests: 13, total length: 236
[MASTER] 11:38:01.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:87.10980392156863 - branchDistance:0.0 - coverage:63.60980392156863 - ex: 0 - tex: 26
[MASTER] 11:38:01.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.10980392156863, number of tests: 14, total length: 276
[MASTER] 11:38:01.918 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:86.10980392156863 - branchDistance:0.0 - coverage:62.60980392156863 - ex: 0 - tex: 18
[MASTER] 11:38:01.918 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 86.10980392156863, number of tests: 11, total length: 234
[MASTER] 11:38:12.436 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:85.10980392156863 - branchDistance:0.0 - coverage:61.60980392156863 - ex: 0 - tex: 16
[MASTER] 11:38:12.436 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 85.10980392156863, number of tests: 11, total length: 231
[MASTER] 11:38:23.029 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:82.07450980392157 - branchDistance:0.0 - coverage:58.57450980392157 - ex: 0 - tex: 20
[MASTER] 11:38:23.029 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.07450980392157, number of tests: 13, total length: 278
[MASTER] 11:38:34.080 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:80.83333333333334 - branchDistance:0.0 - coverage:57.333333333333336 - ex: 0 - tex: 18
[MASTER] 11:38:34.081 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.83333333333334, number of tests: 12, total length: 281
[MASTER] 11:38:34.220 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:79.10980392156863 - branchDistance:0.0 - coverage:55.60980392156863 - ex: 0 - tex: 22
[MASTER] 11:38:34.220 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 79.10980392156863, number of tests: 14, total length: 306
[MASTER] 11:38:34.427 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:78.58092006033183 - branchDistance:0.0 - coverage:55.08092006033183 - ex: 0 - tex: 22
[MASTER] 11:38:34.427 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.58092006033183, number of tests: 13, total length: 273
[MASTER] 11:38:34.670 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:78.10980392156863 - branchDistance:0.0 - coverage:54.60980392156863 - ex: 0 - tex: 20
[MASTER] 11:38:34.670 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.10980392156863, number of tests: 13, total length: 290
[MASTER] 11:38:36.235 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:78.07745098039216 - branchDistance:0.0 - coverage:54.57745098039216 - ex: 0 - tex: 20
[MASTER] 11:38:36.235 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 78.07745098039216, number of tests: 13, total length: 305
[MASTER] 11:38:36.364 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:77.85392156862744 - branchDistance:0.0 - coverage:54.35392156862745 - ex: 0 - tex: 22
[MASTER] 11:38:36.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.85392156862744, number of tests: 14, total length: 326
[MASTER] 11:38:46.863 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:77.81862745098039 - branchDistance:0.0 - coverage:54.318627450980394 - ex: 0 - tex: 20
[MASTER] 11:38:46.863 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.81862745098039, number of tests: 13, total length: 312
[MASTER] 11:38:57.833 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:77.33333333333333 - branchDistance:0.0 - coverage:53.83333333333333 - ex: 0 - tex: 20
[MASTER] 11:38:57.833 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.33333333333333, number of tests: 13, total length: 299
[MASTER] 11:38:58.961 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:77.17307692307692 - branchDistance:0.0 - coverage:53.67307692307692 - ex: 0 - tex: 20
[MASTER] 11:38:58.961 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.17307692307692, number of tests: 13, total length: 308
[MASTER] 11:38:59.197 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:77.08333333333333 - branchDistance:0.0 - coverage:53.58333333333333 - ex: 0 - tex: 20
[MASTER] 11:38:59.197 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.08333333333333, number of tests: 13, total length: 322
[MASTER] 11:38:59.503 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:76.9230769221456 - branchDistance:0.0 - coverage:53.4230769221456 - ex: 0 - tex: 20
[MASTER] 11:38:59.503 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.9230769221456, number of tests: 13, total length: 311
[MASTER] 11:39:11.027 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:76.833333332402 - branchDistance:0.0 - coverage:53.333333332402006 - ex: 0 - tex: 20
[MASTER] 11:39:11.027 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.833333332402, number of tests: 13, total length: 327
[MASTER] 11:39:12.357 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:76.81818181725049 - branchDistance:0.0 - coverage:53.3181818172505 - ex: 0 - tex: 18
[MASTER] 11:39:12.357 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.81818181725049, number of tests: 13, total length: 316
[MASTER] 11:39:24.558 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:76.59465240548579 - branchDistance:0.0 - coverage:53.09465240548579 - ex: 0 - tex: 20
[MASTER] 11:39:24.558 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.59465240548579, number of tests: 14, total length: 335
[MASTER] 11:39:24.927 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:76.31818181725049 - branchDistance:0.0 - coverage:53.3181818172505 - ex: 1 - tex: 19
[MASTER] 11:39:24.928 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 76.31818181725049, number of tests: 13, total length: 315
[MASTER] 11:39:24.951 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:75.92307692307692 - branchDistance:0.0 - coverage:52.42307692307692 - ex: 0 - tex: 20
[MASTER] 11:39:24.951 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.92307692307692, number of tests: 13, total length: 317
[MASTER] 11:39:36.184 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:75.81818181818181 - branchDistance:0.0 - coverage:52.31818181818182 - ex: 0 - tex: 16
[MASTER] 11:39:36.184 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.81818181818181, number of tests: 13, total length: 317
[MASTER] 11:39:36.199 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:75.31818181818181 - branchDistance:0.0 - coverage:52.31818181818182 - ex: 1 - tex: 17
[MASTER] 11:39:36.199 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.31818181818181, number of tests: 13, total length: 313
[MASTER] 11:39:37.266 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:75.19954751131222 - branchDistance:0.0 - coverage:52.19954751131222 - ex: 1 - tex: 21
[MASTER] 11:39:37.266 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.19954751131222, number of tests: 13, total length: 308
[MASTER] 11:39:37.583 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:75.09001280409731 - branchDistance:0.0 - coverage:52.09001280409731 - ex: 1 - tex: 19
[MASTER] 11:39:37.583 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.09001280409731, number of tests: 14, total length: 346
[MASTER] 11:40:10.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:75.03288084464555 - branchDistance:0.0 - coverage:52.19954751131222 - ex: 2 - tex: 22
[MASTER] 11:40:10.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.03288084464555, number of tests: 14, total length: 346
[MASTER] 11:40:10.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.066666666666666 - fitness:74.92334613743064 - branchDistance:0.0 - coverage:52.09001280409731 - ex: 2 - tex: 20
[MASTER] 11:40:10.480 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.92334613743064, number of tests: 15, total length: 367
[MASTER] 11:40:10.621 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.166666666666666 - fitness:74.65452893312957 - branchDistance:0.0 - coverage:52.09001280409731 - ex: 1 - tex: 19
[MASTER] 11:40:10.621 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.65452893312957, number of tests: 14, total length: 336
[MASTER] 11:40:14.204 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.166666666666666 - fitness:74.62387441780231 - branchDistance:0.0 - coverage:52.059358288770056 - ex: 1 - tex: 19
[MASTER] 11:40:14.204 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.62387441780231, number of tests: 14, total length: 335
[MASTER] 11:40:14.267 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.166666666666666 - fitness:74.4878622664629 - branchDistance:0.0 - coverage:52.09001280409731 - ex: 2 - tex: 22
[MASTER] 11:40:14.267 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.4878622664629, number of tests: 15, total length: 403
[MASTER] 11:40:15.605 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.166666666666666 - fitness:74.45720775113564 - branchDistance:0.0 - coverage:52.059358288770056 - ex: 2 - tex: 20
[MASTER] 11:40:15.605 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 74.45720775113564, number of tests: 15, total length: 384
[MASTER] 11:40:17.803 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:73.76769162210338 - branchDistance:0.0 - coverage:52.059358288770056 - ex: 2 - tex: 22
[MASTER] 11:40:17.803 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.76769162210338, number of tests: 16, total length: 427
[MASTER] 11:40:29.556 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:73.68435828877006 - branchDistance:0.0 - coverage:52.059358288770056 - ex: 3 - tex: 19
[MASTER] 11:40:29.556 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.68435828877006, number of tests: 16, total length: 423
[MASTER] 11:40:31.607 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:73.63435828877006 - branchDistance:0.0 - coverage:52.059358288770056 - ex: 4 - tex: 20
[MASTER] 11:40:31.607 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 73.63435828877006, number of tests: 17, total length: 459
[MASTER] 11:40:31.805 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.411111111111111 - fitness:66.04105394268441 - branchDistance:0.0 - coverage:52.154219948849104 - ex: 2 - tex: 18
[MASTER] 11:40:31.805 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 66.04105394268441, number of tests: 14, total length: 331
[MASTER] 11:40:32.174 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.411111111111111 - fitness:65.94619228260537 - branchDistance:0.0 - coverage:52.059358288770056 - ex: 2 - tex: 18
[MASTER] 11:40:32.174 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.94619228260537, number of tests: 14, total length: 351
[MASTER] 11:40:32.607 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.411111111111111 - fitness:65.86285894927204 - branchDistance:0.0 - coverage:52.059358288770056 - ex: 3 - tex: 17
[MASTER] 11:40:32.607 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.86285894927204, number of tests: 15, total length: 381
[MASTER] 11:40:54.973 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.411111111111111 - fitness:65.54467713109022 - branchDistance:0.0 - coverage:51.741176470588236 - ex: 3 - tex: 17
[MASTER] 11:40:54.973 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.54467713109022, number of tests: 15, total length: 375
[MASTER] 11:40:55.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.5777777777777775 - fitness:65.53133191100274 - branchDistance:0.0 - coverage:51.741176470588236 - ex: 1 - tex: 21
[MASTER] 11:40:55.977 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.53133191100274, number of tests: 17, total length: 400
[MASTER] 11:41:06.861 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.5777777777777775 - fitness:65.36466524433607 - branchDistance:0.0 - coverage:51.741176470588236 - ex: 2 - tex: 20
[MASTER] 11:41:06.861 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.36466524433607, number of tests: 17, total length: 433
[MASTER] 11:41:07.629 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.5777777777777775 - fitness:65.28133191100274 - branchDistance:0.0 - coverage:51.741176470588236 - ex: 3 - tex: 19
[MASTER] 11:41:07.629 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.28133191100274, number of tests: 16, total length: 396
[MASTER] 11:41:21.884 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.5777777777777775 - fitness:65.23133191100274 - branchDistance:0.0 - coverage:51.741176470588236 - ex: 4 - tex: 20
[MASTER] 11:41:21.884 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.23133191100274, number of tests: 17, total length: 413
[MASTER] 11:41:22.901 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.5777777777777775 - fitness:65.19799857766941 - branchDistance:0.0 - coverage:51.741176470588236 - ex: 5 - tex: 21
[MASTER] 11:41:22.901 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.19799857766941, number of tests: 17, total length: 413
[MASTER] 11:41:33.647 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 305s and 127 generations, 146472 statements, best individuals have fitness: 65.19799857766941
[MASTER] 11:41:33.651 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 46%
* Total number of goals: 59
* Number of covered goals: 27
* Generated 17 tests with total length 413
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     65 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        305 / 300          Finished!
[MASTER] 11:41:33.933 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:41:33.994 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 273 
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test7, uniqueExceptions: [read:IndexOutOfBoundsException]
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: read:IndexOutOfBoundsException at 20
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.121 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - Test9, uniqueExceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getNextArEntry:MockIOException at 21
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - Test11, uniqueExceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getNextArEntry:MockIOException at 20
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: getNextArEntry()Lorg/apache/commons/compress/archivers/ar/ArArchiveEntry;
[MASTER] 11:41:34.122 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 11:41:34.128 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.129 [logback-1] WARN  RegressionSuiteMinimizer - Test11, uniqueExceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.129 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getNextArEntry:MockIOException at 21
[MASTER] 11:41:34.129 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: getNextArEntry()Lorg/apache/commons/compress/archivers/ar/ArArchiveEntry;
[MASTER] 11:41:34.129 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 21
[MASTER] 11:41:34.142 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.142 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.142 [logback-1] WARN  RegressionSuiteMinimizer - Test13, uniqueExceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.142 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getNextArEntry:MockIOException at 20
[MASTER] 11:41:34.143 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: getNextArEntry()Lorg/apache/commons/compress/archivers/ar/ArArchiveEntry;
[MASTER] 11:41:34.143 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 11:41:34.156 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.156 [logback-1] WARN  RegressionSuiteMinimizer - Test13, uniqueExceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.156 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getNextArEntry:MockIOException at 21
[MASTER] 11:41:34.156 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: getNextArEntry()Lorg/apache/commons/compress/archivers/ar/ArArchiveEntry;
[MASTER] 11:41:34.156 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 21
[MASTER] 11:41:34.166 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.167 [logback-1] WARN  RegressionSuiteMinimizer - Test14, uniqueExceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.167 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getNextArEntry:MockIOException at 20
[MASTER] 11:41:34.167 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: getNextArEntry()Lorg/apache/commons/compress/archivers/ar/ArArchiveEntry;
[MASTER] 11:41:34.167 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 11:41:34.174 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 1.0
[MASTER] 11:41:34.174 [logback-1] WARN  RegressionSuiteMinimizer - Test14, uniqueExceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.174 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: getNextArEntry:MockIOException at 21
[MASTER] 11:41:34.174 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: getNextArEntry()Lorg/apache/commons/compress/archivers/ar/ArArchiveEntry;
[MASTER] 11:41:34.174 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 21
[MASTER] 11:41:34.184 [logback-1] WARN  RegressionSuiteMinimizer - Test15 - Difference in number of exceptions: 0.0
[MASTER] 11:41:34.184 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [read:IndexOutOfBoundsException, getNextArEntry:MockIOException]
[MASTER] 11:41:34.184 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 11:41:34.184 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 11:41:34.184 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 11:41:34.185 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 11:41:34.646 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 16 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 11:41:34.648 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 27%
* Total number of goals: 59
* Number of covered goals: 16
* Generated 2 tests with total length 16
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'ArArchiveInputStream_ESTest' to evosuite-tests
* Done!

* Computation finished
