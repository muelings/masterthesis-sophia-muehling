* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.mockito.internal.util.Timer
* Starting client
* Connecting to master process on port 19662
* Analyzing classpath: 
  - ../defects4j_compiled/Mockito_2_fixed/build/classes/main/
* Finished analyzing classpath
* Generating tests for class org.mockito.internal.util.Timer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 10
[MASTER] 07:50:16.576 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:50:17.034 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 07:50:18.116 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/mockito/internal/util/Timer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:50:18.125 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 07:50:18.429 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/mockito/internal/util/Timer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 07:50:18.439 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
*=*=*=* Total tests: 3 | Tests with assertion: 1
[MASTER] 07:50:18.439 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 2
* GA-Budget:
	- MaxTime :                          1 / 300         
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 07:50:18.770 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:50:18.780 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 1 
[MASTER] 07:50:18.789 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 07:50:18.789 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NoClassDefFoundError, NoClassDefFoundError:validateInput-32,]
[MASTER] 07:50:18.789 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: NoClassDefFoundError at 0
[MASTER] 07:50:18.790 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NoClassDefFoundError, NoClassDefFoundError:validateInput-32,]
[MASTER] 07:50:18.799 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
[MASTER] 07:50:18.803 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 20%
* Total number of goals: 10
* Number of covered goals: 2
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Timer_ESTest' to evosuite-tests
* Done!

* Computation finished
