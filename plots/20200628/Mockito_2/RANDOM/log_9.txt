* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.mockito.internal.util.Timer
* Starting client
* Connecting to master process on port 19732
* Analyzing classpath: 
  - ../defects4j_compiled/Mockito_2_fixed/build/classes/main/
* Finished analyzing classpath
* Generating tests for class org.mockito.internal.util.Timer
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 10
[MASTER] 07:45:00.644 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:45:00.942 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:45:02.061 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/mockito/internal/util/Timer_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:45:02.070 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 07:45:02.386 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/mockito/internal/util/Timer_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 1 | Tests with assertion: 1
[MASTER] 07:45:02.393 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:45:02.394 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 1 tests with total length 2
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- MaxTime :                          1 / 300         
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 07:45:02.661 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:45:02.675 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 1 
[MASTER] 07:45:02.681 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 07:45:02.681 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NoClassDefFoundError, NoClassDefFoundError:validateInput-32,]
[MASTER] 07:45:02.681 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: NoClassDefFoundError at 0
[MASTER] 07:45:02.681 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NoClassDefFoundError, NoClassDefFoundError:validateInput-32,]
[MASTER] 07:45:02.696 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 1 
[MASTER] 07:45:02.700 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 20%
* Total number of goals: 10
* Number of covered goals: 2
* Generated 1 tests with total length 1
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Timer_ESTest' to evosuite-tests
* Done!

* Computation finished
