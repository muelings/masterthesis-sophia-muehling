* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 10318
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 181
[MASTER] 07:26:45.085 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:26:50.313 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var12);  // (Primitive) Original Value: 0 | Regression Value: 1
[MASTER] 07:26:50.313 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var12 == var9);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:26:50.313 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var12 == var9);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:26:50.321 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:26:50.321 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 07:26:51.384 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:26:51.395 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var12);  // (Primitive) Original Value: 0 | Regression Value: 1
[MASTER] 07:26:51.395 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var12 == var9);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:26:51.395 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var12 == var9);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:26:51.396 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:26:51.396 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 07:27:07.738 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var25);  // (Primitive) Original Value: 0 | Regression Value: 1
[MASTER] 07:27:07.738 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var25 == var24);  // (Comp) Original Value: false | Regression Value: true
[MASTER] 07:27:07.738 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var25 == var23);  // (Comp) Original Value: false | Regression Value: true
[MASTER] 07:27:07.741 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:27:07.741 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 07:27:08.096 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:27:08.107 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var25);  // (Primitive) Original Value: 0 | Regression Value: 1
[MASTER] 07:27:08.107 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var25 == var24);  // (Comp) Original Value: false | Regression Value: true
[MASTER] 07:27:08.107 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertFalse(var25 == var23);  // (Comp) Original Value: false | Regression Value: true
[MASTER] 07:27:08.108 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:27:08.108 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 07:27:11.675 [logback-1] WARN  RegressionAssertionCounter - =============================== HAD TIMEOUT ===============================
[MASTER] 07:27:12.025 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var12);  // (Primitive) Original Value: 0 | Regression Value: 1
[MASTER] 07:27:12.026 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:27:12.026 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:27:12.320 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_5_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:27:12.326 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var12);  // (Primitive) Original Value: 0 | Regression Value: 1
[MASTER] 07:27:12.328 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:27:12.328 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 07:27:12.588 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_7_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:27:12.594 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var12);  // (Primitive) Original Value: 0 | Regression Value: 1
[MASTER] 07:27:12.595 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:27:12.595 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 07:27:12.596 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
Keeping 1 assertions.
*** Random test generation finished.
*=*=*=* Total tests: 2094 | Tests with assertion: 1
* Generated 1 tests with total length 13
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                         27 / 300         
[MASTER] 07:27:13.382 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:27:13.395 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 9 
[MASTER] 07:27:13.402 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[]}
[MASTER] 07:27:13.478 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 4 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 07:27:13.482 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 12%
* Total number of goals: 181
* Number of covered goals: 21
* Generated 1 tests with total length 4
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
