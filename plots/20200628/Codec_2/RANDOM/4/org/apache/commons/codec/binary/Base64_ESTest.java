/*
 * This file was automatically generated by EvoSuite
 * Thu Jun 25 05:21:26 GMT 2020
 */

package org.apache.commons.codec.binary;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigInteger;
import org.apache.commons.codec.binary.Base64;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Base64_ESTest extends Base64_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      BigInteger bigInteger0 = BigInteger.TEN;
      byte[] byteArray0 = Base64.toIntegerBytes(bigInteger0);
      Base64 base64_0 = new Base64(68, byteArray0);
      base64_0.encode(byteArray0, (-928), (-928));
      int int0 = base64_0.avail();
      assertEquals(0, int0); // (Primitive) Original Value: 0 | Regression Value: 1
  }
}
