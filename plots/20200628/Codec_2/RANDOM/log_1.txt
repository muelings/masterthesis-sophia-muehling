* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 8551
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 181
[MASTER] 07:05:08.393 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:05:08.938 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var36);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 07:05:08.938 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var36 == var35);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:05:08.938 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var36 == var20);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:05:08.949 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:05:08.949 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
[MASTER] 07:05:10.111 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:05:10.141 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var36);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 07:05:10.141 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var36 == var35);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:05:10.141 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var36 == var20);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:05:10.146 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:05:10.146 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
Generated test with 3 assertions.
[MASTER] 07:05:10.493 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/codec/binary/Base64_3_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 07:05:10.517 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertEquals(0, var36);  // (Primitive) Original Value: 0 | Regression Value: 2
[MASTER] 07:05:10.517 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var36 == var35);  // (Comp) Original Value: true | Regression Value: false
[MASTER] 07:05:10.517 [logback-1] WARN  RegressionAssertionCounter - +++++ Assertion: assertTrue(var36 == var20);  // (Comp) Original Value: true | Regression Value: false
Keeping 3 assertions.
[MASTER] 07:05:10.521 [logback-1] WARN  RegressionAssertionCounter - Assertions ^^^^^^^^^
[MASTER] 07:05:10.521 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 3
*** Random test generation finished.
[MASTER] 07:05:10.521 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 22 | Tests with assertion: 1
* Generated 1 tests with total length 37
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          2 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
[MASTER] 07:05:11.268 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:05:11.289 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 24 
[MASTER] 07:05:11.310 [logback-1] WARN  RegressionSuiteMinimizer - unique assertions: {PrimitiveAssertion=[], EqualsAssertion=[]}
[MASTER] 07:05:11.742 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
[MASTER] 07:05:11.748 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 14%
* Total number of goals: 181
* Number of covered goals: 25
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
