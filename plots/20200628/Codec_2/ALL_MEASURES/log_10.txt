* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.codec.binary.Base64
* Starting client
* Connecting to master process on port 10651
* Analyzing classpath: 
  - ../defects4j_compiled/Codec_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.codec.binary.Base64
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 07:54:43.203 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 07:54:43.208 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 181
* Using seed 1593064481068
* Starting evolution
[MASTER] 07:54:43.933 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:582.3752525243212 - branchDistance:0.0 - coverage:179.3752525243212 - ex: 0 - tex: 4
[MASTER] 07:54:43.933 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 582.3752525243212, number of tests: 5, total length: 110
[MASTER] 07:54:43.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:543.2703821178986 - branchDistance:0.0 - coverage:140.2703821178986 - ex: 0 - tex: 6
[MASTER] 07:54:43.993 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 543.2703821178986, number of tests: 5, total length: 125
[MASTER] 07:54:44.154 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:535.4175824147885 - branchDistance:0.0 - coverage:132.41758241478846 - ex: 0 - tex: 14
[MASTER] 07:54:44.154 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 535.4175824147885, number of tests: 9, total length: 194
[MASTER] 07:54:44.240 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:282.38809523576697 - branchDistance:0.0 - coverage:130.63809523576694 - ex: 0 - tex: 18
[MASTER] 07:54:44.240 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 282.38809523576697, number of tests: 10, total length: 294
[MASTER] 07:54:44.355 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:223.8516013846419 - branchDistance:0.0 - coverage:130.08237061541112 - ex: 0 - tex: 8
[MASTER] 07:54:44.355 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.8516013846419, number of tests: 7, total length: 153
[MASTER] 07:54:47.041 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:210.6688330179017 - branchDistance:0.0 - coverage:58.91883301790169 - ex: 0 - tex: 2
[MASTER] 07:54:47.041 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 210.6688330179017, number of tests: 7, total length: 165
[MASTER] 07:54:47.551 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:202.9512404253091 - branchDistance:0.0 - coverage:51.2012404253091 - ex: 0 - tex: 4
[MASTER] 07:54:47.551 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 202.9512404253091, number of tests: 7, total length: 172
[MASTER] 07:54:48.107 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:201.65160138464188 - branchDistance:0.0 - coverage:107.8823706154111 - ex: 0 - tex: 8
[MASTER] 07:54:48.107 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 201.65160138464188, number of tests: 8, total length: 173
[MASTER] 07:54:48.151 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.6666666666666665 - fitness:200.2745088235775 - branchDistance:0.0 - coverage:48.5245088235775 - ex: 0 - tex: 6
[MASTER] 07:54:48.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 200.2745088235775, number of tests: 8, total length: 195
[MASTER] 07:54:48.730 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:175.45855790638103 - branchDistance:0.0 - coverage:81.68932713715024 - ex: 0 - tex: 4
[MASTER] 07:54:48.730 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 175.45855790638103, number of tests: 7, total length: 145
[MASTER] 07:54:49.254 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:156.84364562567927 - branchDistance:0.0 - coverage:63.07441485644849 - ex: 0 - tex: 8
[MASTER] 07:54:49.254 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 156.84364562567927, number of tests: 8, total length: 217
[MASTER] 07:54:50.585 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:154.22168484136554 - branchDistance:0.0 - coverage:60.45245407213476 - ex: 0 - tex: 4
[MASTER] 07:54:50.585 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.22168484136554, number of tests: 7, total length: 160
[MASTER] 07:54:50.718 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:147.59031229234594 - branchDistance:0.0 - coverage:53.82108152311516 - ex: 0 - tex: 8
[MASTER] 07:54:50.718 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 147.59031229234594, number of tests: 8, total length: 228
[MASTER] 07:54:50.969 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:145.11031229234595 - branchDistance:0.0 - coverage:51.341081523115164 - ex: 0 - tex: 8
[MASTER] 07:54:50.970 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 145.11031229234595, number of tests: 9, total length: 245
[MASTER] 07:54:51.255 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:142.17697895901262 - branchDistance:0.0 - coverage:48.40774818978183 - ex: 0 - tex: 10
[MASTER] 07:54:51.255 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.17697895901262, number of tests: 9, total length: 248
[MASTER] 07:54:53.841 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:141.17697895901262 - branchDistance:0.0 - coverage:47.40774818978183 - ex: 0 - tex: 10
[MASTER] 07:54:53.841 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.17697895901262, number of tests: 9, total length: 249
[MASTER] 07:54:54.243 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:138.14065933972802 - branchDistance:0.0 - coverage:44.37142857049725 - ex: 0 - tex: 10
[MASTER] 07:54:54.243 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.14065933972802, number of tests: 9, total length: 245
[MASTER] 07:54:55.122 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:136.14065933972802 - branchDistance:0.0 - coverage:42.37142857049725 - ex: 0 - tex: 12
[MASTER] 07:54:55.122 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.14065933972802, number of tests: 10, total length: 278
[MASTER] 07:54:56.332 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:135.14065933972802 - branchDistance:0.0 - coverage:41.371428570497244 - ex: 0 - tex: 10
[MASTER] 07:54:56.334 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.14065933972802, number of tests: 11, total length: 293
[MASTER] 07:54:57.287 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:134.0073260063947 - branchDistance:0.0 - coverage:40.23809523716391 - ex: 0 - tex: 10
[MASTER] 07:54:57.287 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.0073260063947, number of tests: 11, total length: 278
[MASTER] 07:54:58.762 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:133.74259170687776 - branchDistance:0.0 - coverage:39.973360937647 - ex: 0 - tex: 12
[MASTER] 07:54:58.762 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 133.74259170687776, number of tests: 10, total length: 269
[MASTER] 07:54:58.978 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:132.4569789590126 - branchDistance:0.0 - coverage:38.68774818978183 - ex: 0 - tex: 10
[MASTER] 07:54:58.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.4569789590126, number of tests: 9, total length: 268
[MASTER] 07:54:59.208 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:132.1769789590126 - branchDistance:0.0 - coverage:38.40774818978182 - ex: 0 - tex: 14
[MASTER] 07:54:59.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.1769789590126, number of tests: 10, total length: 283
[MASTER] 07:54:59.843 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:129.0073260063947 - branchDistance:0.0 - coverage:35.23809523716392 - ex: 0 - tex: 10
[MASTER] 07:54:59.843 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 129.0073260063947, number of tests: 10, total length: 264
[MASTER] 07:55:00.431 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:127.00732600639469 - branchDistance:0.0 - coverage:33.23809523716392 - ex: 0 - tex: 12
[MASTER] 07:55:00.431 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.00732600639469, number of tests: 10, total length: 282
[MASTER] 07:55:02.251 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:126.87399267306135 - branchDistance:0.0 - coverage:33.10476190383058 - ex: 0 - tex: 10
[MASTER] 07:55:02.251 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.87399267306135, number of tests: 11, total length: 297
[MASTER] 07:55:02.733 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 3.333333333333333 - fitness:126.14065933972802 - branchDistance:0.0 - coverage:32.371428570497244 - ex: 0 - tex: 14
[MASTER] 07:55:02.733 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.14065933972802, number of tests: 11, total length: 296
[MASTER] 07:55:02.868 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:109.99280538209145 - branchDistance:0.0 - coverage:33.61780538209145 - ex: 0 - tex: 12
[MASTER] 07:55:02.869 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 109.99280538209145, number of tests: 12, total length: 336
[MASTER] 07:55:04.994 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:108.99280538209145 - branchDistance:0.0 - coverage:32.61780538209145 - ex: 0 - tex: 12
[MASTER] 07:55:04.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.99280538209145, number of tests: 12, total length: 333
[MASTER] 07:55:05.034 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:107.99280538209145 - branchDistance:0.0 - coverage:31.61780538209145 - ex: 0 - tex: 14
[MASTER] 07:55:05.034 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.99280538209145, number of tests: 12, total length: 339
[MASTER] 07:55:07.815 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:106.02137681066287 - branchDistance:0.0 - coverage:29.64637681066288 - ex: 0 - tex: 16
[MASTER] 07:55:07.816 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.02137681066287, number of tests: 13, total length: 371
[MASTER] 07:55:08.964 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:104.99280538209145 - branchDistance:0.0 - coverage:28.61780538209145 - ex: 0 - tex: 16
[MASTER] 07:55:08.964 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.99280538209145, number of tests: 13, total length: 360
[MASTER] 07:55:13.302 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:103.99280538209145 - branchDistance:0.0 - coverage:27.61780538209145 - ex: 0 - tex: 16
[MASTER] 07:55:13.302 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 103.99280538209145, number of tests: 13, total length: 363
[MASTER] 07:55:14.161 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:102.99280538209145 - branchDistance:0.0 - coverage:26.61780538209145 - ex: 0 - tex: 18
[MASTER] 07:55:14.161 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.99280538209145, number of tests: 14, total length: 399
[MASTER] 07:55:15.144 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:101.68804347732954 - branchDistance:0.0 - coverage:25.313043477329543 - ex: 0 - tex: 18
[MASTER] 07:55:15.144 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.68804347732954, number of tests: 14, total length: 399
[MASTER] 07:55:18.357 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:101.62142857049724 - branchDistance:0.0 - coverage:25.246428570497248 - ex: 0 - tex: 18
[MASTER] 07:55:18.357 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.62142857049724, number of tests: 14, total length: 385
[MASTER] 07:55:19.364 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:100.65947204875812 - branchDistance:0.0 - coverage:24.28447204875812 - ex: 0 - tex: 18
[MASTER] 07:55:19.364 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.65947204875812, number of tests: 13, total length: 363
[MASTER] 07:55:20.436 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:99.64999999906868 - branchDistance:0.0 - coverage:23.274999999068676 - ex: 0 - tex: 20
[MASTER] 07:55:20.436 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.64999999906868, number of tests: 15, total length: 403
[MASTER] 07:55:22.587 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:99.62142857049724 - branchDistance:0.0 - coverage:23.246428570497248 - ex: 0 - tex: 18
[MASTER] 07:55:22.587 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.62142857049724, number of tests: 14, total length: 407
[MASTER] 07:55:25.072 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:96.65947204875812 - branchDistance:0.0 - coverage:20.28447204875812 - ex: 0 - tex: 20
[MASTER] 07:55:25.072 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.65947204875812, number of tests: 15, total length: 428
[MASTER] 07:55:26.680 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:96.62142857049724 - branchDistance:0.0 - coverage:20.246428570497248 - ex: 0 - tex: 22
[MASTER] 07:55:26.680 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.62142857049724, number of tests: 15, total length: 416
[MASTER] 07:55:30.355 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.333333333333333 - fitness:96.61774193455255 - branchDistance:0.0 - coverage:20.242741934552548 - ex: 0 - tex: 22
[MASTER] 07:55:30.355 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 96.61774193455255, number of tests: 15, total length: 420
[MASTER] 07:55:31.077 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:84.72011278102357 - branchDistance:0.0 - coverage:20.246428570497248 - ex: 0 - tex: 22
[MASTER] 07:55:31.077 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.72011278102357, number of tests: 15, total length: 422
[MASTER] 07:55:33.908 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:84.71642614507887 - branchDistance:0.0 - coverage:20.242741934552548 - ex: 0 - tex: 24
[MASTER] 07:55:33.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.71642614507887, number of tests: 16, total length: 429
[MASTER] 07:55:35.191 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:83.72011278102357 - branchDistance:0.0 - coverage:19.246428570497248 - ex: 0 - tex: 24
[MASTER] 07:55:35.191 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.72011278102357, number of tests: 16, total length: 451
[MASTER] 07:55:36.514 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:83.71642614507887 - branchDistance:0.0 - coverage:19.242741934552548 - ex: 0 - tex: 20
[MASTER] 07:55:36.514 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 83.71642614507887, number of tests: 15, total length: 434
[MASTER] 07:55:40.052 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:82.72011278102357 - branchDistance:0.0 - coverage:18.246428570497248 - ex: 0 - tex: 22
[MASTER] 07:55:40.052 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.72011278102357, number of tests: 16, total length: 467
[MASTER] 07:55:47.880 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.84511278102357 - branchDistance:0.0 - coverage:17.37142857049725 - ex: 0 - tex: 22
[MASTER] 07:55:47.880 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.84511278102357, number of tests: 16, total length: 467
[MASTER] 07:55:49.893 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.84338117929197 - branchDistance:0.0 - coverage:17.369696968765645 - ex: 0 - tex: 24
[MASTER] 07:55:49.893 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.84338117929197, number of tests: 16, total length: 474
[MASTER] 07:55:52.453 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:81.34511278102357 - branchDistance:0.0 - coverage:16.87142857049725 - ex: 0 - tex: 22
[MASTER] 07:55:52.453 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 81.34511278102357, number of tests: 16, total length: 473
[MASTER] 07:56:04.277 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:80.84338117929197 - branchDistance:0.0 - coverage:16.369696968765645 - ex: 0 - tex: 24
[MASTER] 07:56:04.277 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.84338117929197, number of tests: 16, total length: 487
[MASTER] 07:56:06.832 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:80.34338117929197 - branchDistance:0.0 - coverage:15.869696968765645 - ex: 0 - tex: 24
[MASTER] 07:56:06.832 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.34338117929197, number of tests: 16, total length: 485
[MASTER] 07:56:09.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 5.333333333333333 - fitness:80.34142614507887 - branchDistance:0.0 - coverage:15.867741934552548 - ex: 0 - tex: 20
[MASTER] 07:56:09.479 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 80.34142614507887, number of tests: 16, total length: 459
[MASTER] 07:56:10.061 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:72.68787878694746 - branchDistance:0.0 - coverage:16.869696968765645 - ex: 0 - tex: 24
[MASTER] 07:56:10.061 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.68787878694746, number of tests: 17, total length: 516
[MASTER] 07:56:12.402 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:71.68787878694746 - branchDistance:0.0 - coverage:15.869696968765645 - ex: 0 - tex: 24
[MASTER] 07:56:12.402 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.68787878694746, number of tests: 17, total length: 490
[MASTER] 07:56:20.957 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:71.45454545361414 - branchDistance:0.0 - coverage:15.63636363543231 - ex: 0 - tex: 20
[MASTER] 07:56:20.957 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.45454545361414, number of tests: 17, total length: 469
[MASTER] 07:56:25.450 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:71.45259041940105 - branchDistance:0.0 - coverage:15.634408601219214 - ex: 0 - tex: 20
[MASTER] 07:56:25.450 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 71.45259041940105, number of tests: 17, total length: 471
[MASTER] 07:56:33.964 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.333333333333333 - fitness:69.95259041940105 - branchDistance:0.0 - coverage:14.134408601219215 - ex: 0 - tex: 20
[MASTER] 07:56:33.964 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.95259041940105, number of tests: 16, total length: 454
[MASTER] 07:56:49.108 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:65.47440860121922 - branchDistance:0.0 - coverage:16.234408601219215 - ex: 0 - tex: 20
[MASTER] 07:56:49.108 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.47440860121922, number of tests: 16, total length: 427
[MASTER] 07:56:58.235 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:63.37440860121922 - branchDistance:0.0 - coverage:14.134408601219215 - ex: 0 - tex: 22
[MASTER] 07:56:58.235 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.37440860121922, number of tests: 17, total length: 449
[MASTER] 07:57:23.079 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.333333333333333 - fitness:62.87440860121922 - branchDistance:0.0 - coverage:14.134408601219215 - ex: 1 - tex: 21
[MASTER] 07:57:23.080 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.87440860121922, number of tests: 16, total length: 400
[MASTER] 07:57:27.133 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:57.70583717264778 - branchDistance:0.0 - coverage:14.134408601219215 - ex: 1 - tex: 21
[MASTER] 07:57:27.133 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 57.70583717264778, number of tests: 16, total length: 398
[MASTER] 07:57:32.748 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.70583717264778 - branchDistance:0.0 - coverage:13.134408601219215 - ex: 1 - tex: 21
[MASTER] 07:57:32.748 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.70583717264778, number of tests: 17, total length: 410
[MASTER] 07:57:37.942 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.53917050598112 - branchDistance:0.0 - coverage:13.134408601219215 - ex: 2 - tex: 22
[MASTER] 07:57:37.942 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.53917050598112, number of tests: 18, total length: 435
[MASTER] 07:57:41.045 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.45583717264778 - branchDistance:0.0 - coverage:13.134408601219215 - ex: 3 - tex: 23
[MASTER] 07:57:41.045 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.45583717264778, number of tests: 18, total length: 470
[MASTER] 07:57:47.282 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.405837172647786 - branchDistance:0.0 - coverage:13.134408601219215 - ex: 4 - tex: 22
[MASTER] 07:57:47.283 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.405837172647786, number of tests: 19, total length: 511
[MASTER] 07:57:56.421 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.401058200126876 - branchDistance:0.0 - coverage:13.129629628698305 - ex: 4 - tex: 22
[MASTER] 07:57:56.422 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.401058200126876, number of tests: 19, total length: 501
[MASTER] 07:58:02.013 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.36772486679354 - branchDistance:0.0 - coverage:13.129629628698305 - ex: 5 - tex: 23
[MASTER] 07:58:02.013 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.36772486679354, number of tests: 20, total length: 537
[MASTER] 07:58:13.986 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.34391534298402 - branchDistance:0.0 - coverage:13.129629628698305 - ex: 6 - tex: 24
[MASTER] 07:58:13.986 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.34391534298402, number of tests: 21, total length: 565
[MASTER] 07:58:22.329 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.337474119151494 - branchDistance:0.0 - coverage:13.123188404865777 - ex: 6 - tex: 24
[MASTER] 07:58:22.329 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.337474119151494, number of tests: 21, total length: 560
[MASTER] 07:58:26.249 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.177248676317355 - branchDistance:0.0 - coverage:12.96296296203164 - ex: 6 - tex: 24
[MASTER] 07:58:26.249 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.177248676317355, number of tests: 21, total length: 550
[MASTER] 07:58:27.939 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.17080745248483 - branchDistance:0.0 - coverage:12.956521738199111 - ex: 6 - tex: 24
[MASTER] 07:58:27.939 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.17080745248483, number of tests: 21, total length: 559
[MASTER] 07:58:30.367 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:56.15939153346021 - branchDistance:0.0 - coverage:12.96296296203164 - ex: 7 - tex: 27
[MASTER] 07:58:30.367 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 56.15939153346021, number of tests: 23, total length: 603
[MASTER] 07:58:31.187 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:55.36309523716391 - branchDistance:0.0 - coverage:12.166666665735343 - ex: 7 - tex: 25
[MASTER] 07:58:31.187 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.36309523716391, number of tests: 22, total length: 591
[MASTER] 07:58:31.752 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:55.19642857049725 - branchDistance:0.0 - coverage:11.999999999068677 - ex: 7 - tex: 27
[MASTER] 07:58:31.752 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 55.19642857049725, number of tests: 23, total length: 607
[MASTER] 07:58:37.656 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:54.21428571335439 - branchDistance:0.0 - coverage:10.999999999068677 - ex: 6 - tex: 26
[MASTER] 07:58:37.656 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.21428571335439, number of tests: 22, total length: 598
[MASTER] 07:58:41.346 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:54.18253968160836 - branchDistance:0.0 - coverage:10.999999999068677 - ex: 8 - tex: 28
[MASTER] 07:58:41.346 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 54.18253968160836, number of tests: 24, total length: 670
[MASTER] 07:58:42.114 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:53.69642857049725 - branchDistance:0.0 - coverage:10.499999999068677 - ex: 7 - tex: 25
[MASTER] 07:58:42.114 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 53.69642857049725, number of tests: 22, total length: 598
[MASTER] 07:58:48.952 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:52.69642857049725 - branchDistance:0.0 - coverage:9.499999999068677 - ex: 7 - tex: 27
[MASTER] 07:58:48.952 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.69642857049725, number of tests: 23, total length: 629
[MASTER] 07:59:00.297 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:52.68253968160836 - branchDistance:0.0 - coverage:9.499999999068677 - ex: 8 - tex: 28
[MASTER] 07:59:00.297 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.68253968160836, number of tests: 24, total length: 643
[MASTER] 07:59:14.517 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:52.67142857049725 - branchDistance:0.0 - coverage:9.499999999068677 - ex: 9 - tex: 29
[MASTER] 07:59:14.517 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.67142857049725, number of tests: 24, total length: 659
[MASTER] 07:59:38.254 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:52.18253968160836 - branchDistance:0.0 - coverage:8.999999999068677 - ex: 8 - tex: 28
[MASTER] 07:59:38.254 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.18253968160836, number of tests: 24, total length: 639
[MASTER] 07:59:44.150 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 8.333333333333334 - fitness:52.17142857049725 - branchDistance:0.0 - coverage:8.999999999068677 - ex: 9 - tex: 29
[MASTER] 07:59:44.151 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 52.17142857049725, number of tests: 25, total length: 669
[MASTER] 07:59:44.307 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

[MASTER] 07:59:44.310 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 373 generations, 533749 statements, best individuals have fitness: 52.17142857049725
* Coverage of criterion REGRESSION: 95%
* Total number of goals: 181
* Number of covered goals: 172
* Generated 25 tests with total length 669
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     52 / 0           
	- MaxTime :                        301 / 300          Finished!
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
[MASTER] 07:59:44.769 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 07:59:44.904 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 470 
[MASTER] 07:59:45.054 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.054 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 23
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 22
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.055 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 22
[MASTER] 07:59:45.060 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.060 [logback-1] WARN  RegressionSuiteMinimizer - Test7, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.060 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 20
[MASTER] 07:59:45.060 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.060 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 07:59:45.064 [logback-1] WARN  RegressionSuiteMinimizer - Test8 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.064 [logback-1] WARN  RegressionSuiteMinimizer - Test8, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.064 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 20
[MASTER] 07:59:45.064 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.064 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 07:59:45.069 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.069 [logback-1] WARN  RegressionSuiteMinimizer - Test9, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.069 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 21
[MASTER] 07:59:45.069 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.069 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 21
[MASTER] 07:59:45.074 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.074 [logback-1] WARN  RegressionSuiteMinimizer - Test10, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.074 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 21
[MASTER] 07:59:45.074 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.074 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 21
[MASTER] 07:59:45.080 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.080 [logback-1] WARN  RegressionSuiteMinimizer - Test11, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.080 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 24
[MASTER] 07:59:45.080 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.080 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 24
[MASTER] 07:59:45.087 [logback-1] WARN  RegressionSuiteMinimizer - Test12 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.087 [logback-1] WARN  RegressionSuiteMinimizer - Test13 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.087 [logback-1] WARN  RegressionSuiteMinimizer - Test13, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.087 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 20
[MASTER] 07:59:45.087 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.087 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 20
[MASTER] 07:59:45.093 [logback-1] WARN  RegressionSuiteMinimizer - Test14 - Difference in number of exceptions: 1.0
[MASTER] 07:59:45.093 [logback-1] WARN  RegressionSuiteMinimizer - Test14, uniqueExceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.093 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: readResults:ArrayIndexOutOfBoundsException at 23
[MASTER] 07:59:45.093 [logback-1] WARN  RegressionSuiteMinimizer - removing statementB: readResults([BII)I
[MASTER] 07:59:45.093 [logback-1] WARN  RegressionSuiteMinimizer - removed exceptionB throwing line 23
[MASTER] 07:59:45.099 [logback-1] WARN  RegressionSuiteMinimizer - Test16 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Test17 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Test19 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Test20 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Test21 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Test22 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Test24 - Difference in number of exceptions: 0.0
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [readResults:ArrayIndexOutOfBoundsException]
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 12: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 13: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 14: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 15: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 16: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 17: no assertions
[MASTER] 07:59:45.100 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 18: no assertions
[MASTER] 07:59:45.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 19: no assertions
[MASTER] 07:59:45.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 20: no assertions
[MASTER] 07:59:45.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 21: no assertions
[MASTER] 07:59:45.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 22: no assertions
[MASTER] 07:59:45.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 23: no assertions
[MASTER] 07:59:45.101 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 24: no assertions
[MASTER] 07:59:45.292 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 5 
[MASTER] 07:59:45.293 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 46%
* Total number of goals: 181
* Number of covered goals: 83
* Generated 1 tests with total length 5
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Base64_ESTest' to evosuite-tests
* Done!

* Computation finished
