/*
 * This file was automatically generated by EvoSuite
 * Thu Jun 25 05:21:17 GMT 2020
 */

package org.apache.commons.codec.binary;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.commons.codec.binary.Base64;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Base64_ESTest extends Base64_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      Base64 base64_0 = new Base64();
      byte[] byteArray0 = new byte[9];
      base64_0.encode(byteArray0, (int) (byte)21, (int) (byte) (-16));
      byte[] byteArray1 = Base64.discardNonBase64(byteArray0);
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.ArrayIndexOutOfBoundsException : null
      base64_0.readResults(byteArray1, 0, 2074);
  }
}
