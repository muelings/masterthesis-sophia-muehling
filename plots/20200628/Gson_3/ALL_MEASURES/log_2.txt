* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: com.google.gson.internal.ConstructorConstructor
* Starting client
* Connecting to master process on port 14171
* Analyzing classpath: 
  - ../defects4j_compiled/Gson_3_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class com.google.gson.internal.ConstructorConstructor
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 19:22:00.289 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 19:22:00.451 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 66
* Using seed 1593105717209
* Starting evolution
[MASTER] 19:23:15.857 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:209.0 - branchDistance:0.0 - coverage:104.0 - ex: 0 - tex: 0
[MASTER] 19:23:15.857 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 209.0, number of tests: 3, total length: 0
[MASTER] 19:27:01.546 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 19:27:01.553 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 152 generations, 0 statements, best individuals have fitness: 209.0
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 35
* Number of covered goals: 0
* Generated 4 tests with total length 0
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                    209 / 0           
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 19:27:01.816 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 19:27:01.839 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 0 
[MASTER] 19:27:01.853 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 19:27:01.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 19:27:01.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 19:27:01.854 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 19:27:01.854 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 35
* Number of covered goals: 0
[MASTER] 19:27:01.855 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'ConstructorConstructor_ESTest' to evosuite-tests
* Done!

* Computation finished
