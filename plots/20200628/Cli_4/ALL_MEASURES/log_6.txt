* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 14696
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 11:20:31.575 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 11:20:31.580 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 74
* Using seed 1593163228621
* Starting evolution
[MASTER] 11:20:33.226 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:253.99999949730878 - branchDistance:0.0 - coverage:96.99999949730878 - ex: 0 - tex: 6
[MASTER] 11:20:33.226 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 253.99999949730878, number of tests: 5, total length: 121
[MASTER] 11:20:33.566 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:245.9999996526825 - branchDistance:0.0 - coverage:88.99999965268249 - ex: 0 - tex: 16
[MASTER] 11:20:33.566 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 245.9999996526825, number of tests: 8, total length: 231
[MASTER] 11:20:36.388 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:238.99999951990347 - branchDistance:0.0 - coverage:81.99999951990347 - ex: 0 - tex: 14
[MASTER] 11:20:36.388 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 238.99999951990347, number of tests: 8, total length: 198
[MASTER] 11:20:38.649 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:237.9999996545415 - branchDistance:0.0 - coverage:80.99999965454153 - ex: 0 - tex: 16
[MASTER] 11:20:38.649 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 237.9999996545415, number of tests: 8, total length: 237
[MASTER] 11:20:38.862 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.99999993621873 - branchDistance:0.0 - coverage:76.99999993621873 - ex: 0 - tex: 14
[MASTER] 11:20:38.862 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.99999993621873, number of tests: 8, total length: 219
[MASTER] 11:20:39.489 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:233.99999988670103 - branchDistance:0.0 - coverage:76.99999988670103 - ex: 0 - tex: 14
[MASTER] 11:20:39.490 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 233.99999988670103, number of tests: 8, total length: 250
[MASTER] 11:20:39.760 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:232.99999997243646 - branchDistance:0.0 - coverage:75.99999997243646 - ex: 0 - tex: 16
[MASTER] 11:20:39.760 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 232.99999997243646, number of tests: 8, total length: 229
[MASTER] 11:20:40.404 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:229.9999999742955 - branchDistance:0.0 - coverage:72.99999997429549 - ex: 0 - tex: 20
[MASTER] 11:20:40.404 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 229.9999999742955, number of tests: 10, total length: 291
[MASTER] 11:20:40.555 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:229.99999997243646 - branchDistance:0.0 - coverage:72.99999997243646 - ex: 0 - tex: 16
[MASTER] 11:20:40.555 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 229.99999997243646, number of tests: 8, total length: 231
[MASTER] 11:20:41.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:227.99999992291876 - branchDistance:0.0 - coverage:70.99999992291876 - ex: 0 - tex: 16
[MASTER] 11:20:41.907 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 227.99999992291876, number of tests: 8, total length: 252
[MASTER] 11:20:44.078 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:225.0 - branchDistance:0.0 - coverage:68.0 - ex: 0 - tex: 18
[MASTER] 11:20:44.078 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 225.0, number of tests: 9, total length: 289
[MASTER] 11:20:45.507 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:223.99999999814096 - branchDistance:0.0 - coverage:66.99999999814096 - ex: 0 - tex: 18
[MASTER] 11:20:45.507 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 223.99999999814096, number of tests: 9, total length: 282
[MASTER] 11:20:46.331 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:222.0 - branchDistance:0.0 - coverage:65.0 - ex: 0 - tex: 18
[MASTER] 11:20:46.331 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 222.0, number of tests: 9, total length: 263
[MASTER] 11:20:47.428 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:221.0 - branchDistance:0.0 - coverage:64.0 - ex: 0 - tex: 18
[MASTER] 11:20:47.428 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 221.0, number of tests: 9, total length: 287
[MASTER] 11:20:48.720 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:216.0 - branchDistance:0.0 - coverage:59.0 - ex: 0 - tex: 16
[MASTER] 11:20:48.720 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 216.0, number of tests: 8, total length: 234
[MASTER] 11:20:54.645 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:214.0 - branchDistance:0.0 - coverage:57.0 - ex: 0 - tex: 16
[MASTER] 11:20:54.645 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 214.0, number of tests: 8, total length: 227
[MASTER] 11:21:01.264 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:213.0 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 16
[MASTER] 11:21:01.265 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 213.0, number of tests: 9, total length: 239
[MASTER] 11:21:03.121 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:138.0 - branchDistance:0.0 - coverage:72.0 - ex: 0 - tex: 16
[MASTER] 11:21:03.121 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.0, number of tests: 9, total length: 240
[MASTER] 11:21:04.117 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:126.0 - branchDistance:0.0 - coverage:60.0 - ex: 0 - tex: 16
[MASTER] 11:21:04.117 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.0, number of tests: 9, total length: 241
[MASTER] 11:21:04.553 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:123.0 - branchDistance:0.0 - coverage:57.0 - ex: 0 - tex: 16
[MASTER] 11:21:04.553 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 123.0, number of tests: 9, total length: 238
[MASTER] 11:21:05.539 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:122.0 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 16
[MASTER] 11:21:05.539 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.0, number of tests: 9, total length: 235
[MASTER] 11:21:09.279 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:121.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 18
[MASTER] 11:21:09.279 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 9, total length: 234
[MASTER] 11:21:13.463 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.5 - branchDistance:0.0 - coverage:54.5 - ex: 0 - tex: 22
[MASTER] 11:21:13.463 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.5, number of tests: 11, total length: 283
[MASTER] 11:21:24.286 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.0 - branchDistance:0.0 - coverage:54.0 - ex: 0 - tex: 22
[MASTER] 11:21:24.286 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.0, number of tests: 11, total length: 279
[MASTER] 11:21:56.844 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:105.0 - branchDistance:0.0 - coverage:39.0 - ex: 0 - tex: 20
[MASTER] 11:21:56.844 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 105.0, number of tests: 11, total length: 250
[MASTER] 11:21:58.940 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:104.0 - branchDistance:0.0 - coverage:38.0 - ex: 0 - tex: 22
[MASTER] 11:21:58.940 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 104.0, number of tests: 12, total length: 293
[MASTER] 11:22:13.750 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:102.0 - branchDistance:0.0 - coverage:36.0 - ex: 0 - tex: 18
[MASTER] 11:22:13.750 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.0, number of tests: 12, total length: 265
[MASTER] 11:22:14.681 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:101.0 - branchDistance:0.0 - coverage:35.0 - ex: 0 - tex: 20
[MASTER] 11:22:14.681 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.0, number of tests: 12, total length: 260
[MASTER] 11:22:16.873 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:99.99999998451344 - branchDistance:0.0 - coverage:33.99999998451344 - ex: 0 - tex: 20
[MASTER] 11:22:16.873 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.99999998451344, number of tests: 12, total length: 278
[MASTER] 11:23:45.822 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:98.99999998451344 - branchDistance:0.0 - coverage:32.99999998451344 - ex: 0 - tex: 18
[MASTER] 11:23:45.822 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.99999998451344, number of tests: 11, total length: 223
[MASTER] 11:23:52.312 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:98.99999997008811 - branchDistance:0.0 - coverage:32.99999997008811 - ex: 0 - tex: 18
[MASTER] 11:23:52.312 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.99999997008811, number of tests: 12, total length: 255
[MASTER] 11:24:05.866 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:98.9999999527243 - branchDistance:0.0 - coverage:32.999999952724295 - ex: 0 - tex: 18
[MASTER] 11:24:05.866 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 98.9999999527243, number of tests: 11, total length: 222

[MASTER] 11:25:32.608 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 703 generations, 807208 statements, best individuals have fitness: 98.9999999527243
[MASTER] 11:25:32.612 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 76%
* Total number of goals: 74
* Number of covered goals: 56
* Generated 11 tests with total length 190
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     99 / 0           
	- RMIStoppingCondition
[MASTER] 11:25:32.902 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:25:32.940 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 160 
[MASTER] 11:25:33.041 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 11:25:33.044 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 11:25:33.044 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 1.0
[MASTER] 11:25:33.044 [logback-1] WARN  RegressionSuiteMinimizer - Test4, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:25:33.044 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 9
[MASTER] 11:25:33.045 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 1.0
[MASTER] 11:25:33.045 [logback-1] WARN  RegressionSuiteMinimizer - Test5, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:25:33.045 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 7
[MASTER] 11:25:33.045 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 7
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 11:25:33.050 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 11:25:33.051 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 11:25:33.051 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 11:25:33.181 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 7 
[MASTER] 11:25:33.183 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 15%
* Total number of goals: 74
* Number of covered goals: 11
* Generated 1 tests with total length 7
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 3
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
