* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 17035
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 11:26:17.365 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 11:26:17.378 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 74
* Using seed 1593163574324
* Starting evolution
[MASTER] 11:26:18.552 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:274.9999999619232 - branchDistance:0.0 - coverage:117.99999996192324 - ex: 0 - tex: 12
[MASTER] 11:26:18.552 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 274.9999999619232, number of tests: 7, total length: 186
[MASTER] 11:26:19.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:261.9999990412043 - branchDistance:0.0 - coverage:104.99999904120433 - ex: 0 - tex: 12
[MASTER] 11:26:19.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 261.9999990412043, number of tests: 9, total length: 229
[MASTER] 11:26:19.509 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:173.99999999487773 - branchDistance:0.0 - coverage:94.99999999487773 - ex: 0 - tex: 16
[MASTER] 11:26:19.510 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 173.99999999487773, number of tests: 10, total length: 253
[MASTER] 11:26:21.634 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:171.99999999487773 - branchDistance:0.0 - coverage:92.99999999487773 - ex: 0 - tex: 18
[MASTER] 11:26:21.635 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 171.99999999487773, number of tests: 10, total length: 255
[MASTER] 11:26:22.053 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:170.9999999081801 - branchDistance:0.0 - coverage:91.99999990818007 - ex: 0 - tex: 16
[MASTER] 11:26:22.053 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 170.9999999081801, number of tests: 10, total length: 300
[MASTER] 11:26:22.420 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:167.99999999487773 - branchDistance:0.0 - coverage:88.99999999487773 - ex: 0 - tex: 12
[MASTER] 11:26:22.420 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 167.99999999487773, number of tests: 10, total length: 235
[MASTER] 11:26:23.501 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:163.99999999487773 - branchDistance:0.0 - coverage:84.99999999487773 - ex: 0 - tex: 12
[MASTER] 11:26:23.501 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 163.99999999487773, number of tests: 10, total length: 238
[MASTER] 11:26:25.881 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:163.99999951804082 - branchDistance:0.0 - coverage:84.99999951804082 - ex: 0 - tex: 10
[MASTER] 11:26:25.881 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 163.99999951804082, number of tests: 10, total length: 238
[MASTER] 11:26:26.440 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:162.99999999487773 - branchDistance:0.0 - coverage:83.99999999487773 - ex: 0 - tex: 14
[MASTER] 11:26:26.440 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 162.99999999487773, number of tests: 10, total length: 231
[MASTER] 11:26:26.772 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:161.99999994608135 - branchDistance:0.0 - coverage:82.99999994608133 - ex: 0 - tex: 14
[MASTER] 11:26:26.773 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 161.99999994608135, number of tests: 11, total length: 236
[MASTER] 11:26:27.414 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:160.99999994608135 - branchDistance:0.0 - coverage:81.99999994608133 - ex: 0 - tex: 16
[MASTER] 11:26:27.414 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 160.99999994608135, number of tests: 10, total length: 251
[MASTER] 11:26:27.558 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:155.99999951990347 - branchDistance:0.0 - coverage:76.99999951990347 - ex: 0 - tex: 16
[MASTER] 11:26:27.558 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 155.99999951990347, number of tests: 12, total length: 285
[MASTER] 11:26:28.783 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:154.99999951990347 - branchDistance:0.0 - coverage:75.99999951990347 - ex: 0 - tex: 16
[MASTER] 11:26:28.783 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 154.99999951990347, number of tests: 11, total length: 231
[MASTER] 11:26:29.165 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:153.99999999813735 - branchDistance:0.0 - coverage:74.99999999813735 - ex: 0 - tex: 18
[MASTER] 11:26:29.165 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 153.99999999813735, number of tests: 13, total length: 307
[MASTER] 11:26:29.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:153.99999951990347 - branchDistance:0.0 - coverage:74.99999951990347 - ex: 0 - tex: 16
[MASTER] 11:26:29.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 153.99999951990347, number of tests: 11, total length: 253
[MASTER] 11:26:29.792 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.0 - fitness:149.0 - branchDistance:0.0 - coverage:70.0 - ex: 0 - tex: 18
[MASTER] 11:26:29.792 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 149.0, number of tests: 12, total length: 265
[MASTER] 11:26:31.651 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:140.99999951990347 - branchDistance:0.0 - coverage:74.99999951990347 - ex: 0 - tex: 16
[MASTER] 11:26:31.651 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.99999951990347, number of tests: 11, total length: 249
[MASTER] 11:26:33.070 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:140.99999951948075 - branchDistance:0.0 - coverage:74.99999951948074 - ex: 0 - tex: 16
[MASTER] 11:26:33.070 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.99999951948075, number of tests: 12, total length: 262
[MASTER] 11:26:33.116 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:140.99999951947177 - branchDistance:0.0 - coverage:74.99999951947177 - ex: 0 - tex: 18
[MASTER] 11:26:33.116 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.99999951947177, number of tests: 11, total length: 254
[MASTER] 11:26:33.445 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:138.99999951990347 - branchDistance:0.0 - coverage:72.99999951990347 - ex: 0 - tex: 18
[MASTER] 11:26:33.445 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.99999951990347, number of tests: 12, total length: 272
[MASTER] 11:26:34.068 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:136.99999951990347 - branchDistance:0.0 - coverage:70.99999951990347 - ex: 0 - tex: 16
[MASTER] 11:26:34.068 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.99999951990347, number of tests: 11, total length: 260
[MASTER] 11:26:34.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:136.99999951948075 - branchDistance:0.0 - coverage:70.99999951948074 - ex: 0 - tex: 16
[MASTER] 11:26:34.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.99999951948075, number of tests: 12, total length: 263
[MASTER] 11:26:34.993 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:134.99999951990347 - branchDistance:0.0 - coverage:68.99999951990347 - ex: 0 - tex: 18
[MASTER] 11:26:34.994 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.99999951990347, number of tests: 12, total length: 277
[MASTER] 11:26:36.348 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:134.99999951948075 - branchDistance:0.0 - coverage:68.99999951948074 - ex: 0 - tex: 20
[MASTER] 11:26:36.348 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 134.99999951948075, number of tests: 14, total length: 307
[MASTER] 11:26:37.155 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:132.99999951990347 - branchDistance:0.0 - coverage:66.99999951990347 - ex: 0 - tex: 20
[MASTER] 11:26:37.155 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 132.99999951990347, number of tests: 13, total length: 294
[MASTER] 11:26:38.146 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:131.99999951990347 - branchDistance:0.0 - coverage:65.99999951990347 - ex: 0 - tex: 20
[MASTER] 11:26:38.146 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.99999951990347, number of tests: 13, total length: 294
[MASTER] 11:26:39.872 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:131.99999951804082 - branchDistance:0.0 - coverage:65.99999951804082 - ex: 0 - tex: 20
[MASTER] 11:26:39.872 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 131.99999951804082, number of tests: 13, total length: 291
[MASTER] 11:26:44.643 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:130.99999951804082 - branchDistance:0.0 - coverage:64.99999951804082 - ex: 0 - tex: 20
[MASTER] 11:26:44.643 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.99999951804082, number of tests: 12, total length: 265
[MASTER] 11:26:46.175 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:130.99999951760913 - branchDistance:0.0 - coverage:64.99999951760913 - ex: 0 - tex: 20
[MASTER] 11:26:46.176 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 130.99999951760913, number of tests: 12, total length: 271
[MASTER] 11:26:49.508 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:128.99999904073826 - branchDistance:0.0 - coverage:62.99999904073826 - ex: 0 - tex: 22
[MASTER] 11:26:49.508 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 128.99999904073826, number of tests: 13, total length: 323
[MASTER] 11:26:50.928 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:127.99999904073826 - branchDistance:0.0 - coverage:61.99999904073826 - ex: 0 - tex: 22
[MASTER] 11:26:50.928 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 127.99999904073826, number of tests: 13, total length: 311
[MASTER] 11:26:51.776 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:126.99999951943781 - branchDistance:0.0 - coverage:60.99999951943781 - ex: 0 - tex: 22
[MASTER] 11:26:51.776 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 126.99999951943781, number of tests: 13, total length: 323
[MASTER] 11:26:52.015 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:125.99999951943781 - branchDistance:0.0 - coverage:59.99999951943781 - ex: 0 - tex: 24
[MASTER] 11:26:52.015 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 125.99999951943781, number of tests: 14, total length: 360
[MASTER] 11:26:56.788 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:124.99999951943781 - branchDistance:0.0 - coverage:58.99999951943781 - ex: 0 - tex: 24
[MASTER] 11:26:56.788 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 124.99999951943781, number of tests: 14, total length: 380
[MASTER] 11:27:14.271 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:119.99999999813735 - branchDistance:0.0 - coverage:53.999999998137355 - ex: 0 - tex: 24
[MASTER] 11:27:14.271 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.99999999813735, number of tests: 14, total length: 339
[MASTER] 11:28:10.457 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:118.99999999813735 - branchDistance:0.0 - coverage:52.999999998137355 - ex: 0 - tex: 14
[MASTER] 11:28:10.457 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.99999999813735, number of tests: 12, total length: 258
[MASTER] 11:29:48.143 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:113.99999999813735 - branchDistance:0.0 - coverage:47.999999998137355 - ex: 0 - tex: 14
[MASTER] 11:29:48.143 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.99999999813735, number of tests: 9, total length: 156
[MASTER] 11:29:50.667 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:112.99999999813735 - branchDistance:0.0 - coverage:46.999999998137355 - ex: 0 - tex: 16
[MASTER] 11:29:50.668 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.99999999813735, number of tests: 10, total length: 164
[MASTER] 11:30:14.101 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:112.49999999813735 - branchDistance:0.0 - coverage:46.499999998137355 - ex: 0 - tex: 18
[MASTER] 11:30:14.101 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.49999999813735, number of tests: 11, total length: 163
[MASTER] 11:30:24.645 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:111.99999999813735 - branchDistance:0.0 - coverage:45.999999998137355 - ex: 0 - tex: 20
[MASTER] 11:30:24.645 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.99999999813735, number of tests: 12, total length: 173
[MASTER] 11:31:18.412 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 834 generations, 827792 statements, best individuals have fitness: 111.99999999813735
[MASTER] 11:31:18.416 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 66%
* Total number of goals: 74
* Number of covered goals: 49
* Generated 11 tests with total length 150
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                    112 / 0           
	- RMIStoppingCondition
[MASTER] 11:31:18.716 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:31:18.762 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 118 
[MASTER] 11:31:18.831 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 11:31:18.831 [logback-1] WARN  RegressionSuiteMinimizer - Test2 - Difference in number of exceptions: 0.0
[MASTER] 11:31:18.831 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 1.0
[MASTER] 11:31:18.831 [logback-1] WARN  RegressionSuiteMinimizer - Test3, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:31:18.831 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 7
[MASTER] 11:31:18.832 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 1.0
[MASTER] 11:31:18.832 [logback-1] WARN  RegressionSuiteMinimizer - Test4, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:31:18.832 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 9
[MASTER] 11:31:18.832 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 9
[MASTER] 11:31:18.836 [logback-1] WARN  RegressionSuiteMinimizer - Test5 - Difference in number of exceptions: 0.0
[MASTER] 11:31:18.836 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 0.0
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 0.0
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 6: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 11:31:18.837 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 11:31:18.895 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 8 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 11:31:18.896 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 26%
* Total number of goals: 74
* Number of covered goals: 19
* Generated 1 tests with total length 8
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 7
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
