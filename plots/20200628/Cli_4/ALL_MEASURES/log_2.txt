* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.cli.Parser
* Starting client
* Connecting to master process on port 6436
* Analyzing classpath: 
  - ../defects4j_compiled/Cli_4_fixed/target/classes/
* Finished analyzing classpath
* Generating tests for class org.apache.commons.cli.Parser
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 10:58:00.831 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 10:58:00.836 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 74
* Using seed 1593161878267
* Starting evolution
[MASTER] 10:58:02.368 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:270.9999999948777 - branchDistance:0.0 - coverage:113.99999999487773 - ex: 0 - tex: 8
[MASTER] 10:58:02.368 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 270.9999999948777, number of tests: 4, total length: 93
[MASTER] 10:58:02.600 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:268.99999985863855 - branchDistance:0.0 - coverage:111.99999985863856 - ex: 0 - tex: 8
[MASTER] 10:58:02.600 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 268.99999985863855, number of tests: 6, total length: 146
[MASTER] 10:58:02.642 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:263.999999883726 - branchDistance:0.0 - coverage:106.999999883726 - ex: 0 - tex: 6
[MASTER] 10:58:02.642 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 263.999999883726, number of tests: 4, total length: 81
[MASTER] 10:58:02.697 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:260.9999999326964 - branchDistance:0.0 - coverage:103.99999993269643 - ex: 0 - tex: 8
[MASTER] 10:58:02.697 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 260.9999999326964, number of tests: 6, total length: 96
[MASTER] 10:58:02.823 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:248.99999966241845 - branchDistance:0.0 - coverage:91.99999966241845 - ex: 0 - tex: 18
[MASTER] 10:58:02.823 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 248.99999966241845, number of tests: 10, total length: 230
[MASTER] 10:58:03.656 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:244.99999988970882 - branchDistance:0.0 - coverage:87.99999988970883 - ex: 0 - tex: 16
[MASTER] 10:58:03.656 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 244.99999988970882, number of tests: 10, total length: 253
[MASTER] 10:58:04.798 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:244.99999983546638 - branchDistance:0.0 - coverage:87.99999983546637 - ex: 0 - tex: 22
[MASTER] 10:58:04.798 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 244.99999983546638, number of tests: 11, total length: 241
[MASTER] 10:58:04.976 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:244.99999982014774 - branchDistance:0.0 - coverage:87.99999982014774 - ex: 0 - tex: 16
[MASTER] 10:58:04.977 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 244.99999982014774, number of tests: 9, total length: 218
[MASTER] 10:58:05.058 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:244.99999966241845 - branchDistance:0.0 - coverage:87.99999966241845 - ex: 0 - tex: 16
[MASTER] 10:58:05.058 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 244.99999966241845, number of tests: 9, total length: 203
[MASTER] 10:58:05.275 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:242.9999990426013 - branchDistance:0.0 - coverage:85.99999904260132 - ex: 0 - tex: 14
[MASTER] 10:58:05.275 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 242.9999990426013, number of tests: 8, total length: 234
[MASTER] 10:58:06.225 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:241.99999983546638 - branchDistance:0.0 - coverage:84.99999983546637 - ex: 0 - tex: 16
[MASTER] 10:58:06.225 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 241.99999983546638, number of tests: 9, total length: 220
[MASTER] 10:58:06.315 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:241.99999966241845 - branchDistance:0.0 - coverage:84.99999966241845 - ex: 0 - tex: 20
[MASTER] 10:58:06.315 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 241.99999966241845, number of tests: 11, total length: 294
[MASTER] 10:58:06.776 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:239.9999999774053 - branchDistance:0.0 - coverage:82.99999997740531 - ex: 0 - tex: 14
[MASTER] 10:58:06.776 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 239.9999999774053, number of tests: 9, total length: 255
[MASTER] 10:58:07.588 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:239.99999983732903 - branchDistance:0.0 - coverage:82.99999983732901 - ex: 0 - tex: 16
[MASTER] 10:58:07.588 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 239.99999983732903, number of tests: 8, total length: 242
[MASTER] 10:58:07.747 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:238.99999983732903 - branchDistance:0.0 - coverage:81.99999983732901 - ex: 0 - tex: 16
[MASTER] 10:58:07.747 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 238.99999983732903, number of tests: 9, total length: 245
[MASTER] 10:58:07.977 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:155.9999996605558 - branchDistance:0.0 - coverage:89.9999996605558 - ex: 0 - tex: 12
[MASTER] 10:58:07.978 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 155.9999996605558, number of tests: 8, total length: 182
[MASTER] 10:58:09.038 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:150.99999966241845 - branchDistance:0.0 - coverage:84.99999966241845 - ex: 0 - tex: 18
[MASTER] 10:58:09.038 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 150.99999966241845, number of tests: 11, total length: 265
[MASTER] 10:58:09.761 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:142.99999999813735 - branchDistance:0.0 - coverage:76.99999999813735 - ex: 0 - tex: 24
[MASTER] 10:58:09.761 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.99999999813735, number of tests: 13, total length: 328
[MASTER] 10:58:21.059 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:142.99999998217243 - branchDistance:0.0 - coverage:76.99999998217241 - ex: 0 - tex: 22
[MASTER] 10:58:21.059 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 142.99999998217243, number of tests: 13, total length: 310
[MASTER] 10:58:21.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:141.0 - branchDistance:0.0 - coverage:75.0 - ex: 0 - tex: 22
[MASTER] 10:58:21.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 141.0, number of tests: 13, total length: 324
[MASTER] 10:58:21.857 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:140.0 - branchDistance:0.0 - coverage:74.0 - ex: 0 - tex: 26
[MASTER] 10:58:21.857 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 140.0, number of tests: 14, total length: 367
[MASTER] 10:58:22.662 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:139.0 - branchDistance:0.0 - coverage:73.0 - ex: 0 - tex: 20
[MASTER] 10:58:22.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 139.0, number of tests: 13, total length: 320
[MASTER] 10:58:23.047 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:138.0 - branchDistance:0.0 - coverage:72.0 - ex: 0 - tex: 24
[MASTER] 10:58:23.047 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 138.0, number of tests: 14, total length: 351
[MASTER] 10:58:23.625 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:136.0 - branchDistance:0.0 - coverage:70.0 - ex: 0 - tex: 24
[MASTER] 10:58:23.625 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 136.0, number of tests: 14, total length: 342
[MASTER] 10:58:26.029 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:135.0 - branchDistance:0.0 - coverage:69.0 - ex: 0 - tex: 24
[MASTER] 10:58:26.029 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 135.0, number of tests: 14, total length: 347
[MASTER] 10:58:35.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:122.0 - branchDistance:0.0 - coverage:56.0 - ex: 0 - tex: 22
[MASTER] 10:58:35.662 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 122.0, number of tests: 14, total length: 306
[MASTER] 10:58:37.376 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:121.0 - branchDistance:0.0 - coverage:55.0 - ex: 0 - tex: 22
[MASTER] 10:58:37.376 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 121.0, number of tests: 14, total length: 306
[MASTER] 10:58:46.648 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:120.0 - branchDistance:0.0 - coverage:54.0 - ex: 0 - tex: 18
[MASTER] 10:58:46.648 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 120.0, number of tests: 15, total length: 306
[MASTER] 10:58:47.631 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:119.99999999813735 - branchDistance:0.0 - coverage:53.999999998137355 - ex: 0 - tex: 20
[MASTER] 10:58:47.631 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 119.99999999813735, number of tests: 14, total length: 273
[MASTER] 10:58:50.671 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:118.0 - branchDistance:0.0 - coverage:52.0 - ex: 0 - tex: 18
[MASTER] 10:58:50.671 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 118.0, number of tests: 15, total length: 307
[MASTER] 10:58:52.968 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:116.0 - branchDistance:0.0 - coverage:50.0 - ex: 0 - tex: 18
[MASTER] 10:58:52.968 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 116.0, number of tests: 15, total length: 301
[MASTER] 10:59:07.877 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:115.0 - branchDistance:0.0 - coverage:49.0 - ex: 0 - tex: 14
[MASTER] 10:59:07.877 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 115.0, number of tests: 13, total length: 218
[MASTER] 10:59:08.907 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:114.0 - branchDistance:0.0 - coverage:48.0 - ex: 0 - tex: 14
[MASTER] 10:59:08.908 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 114.0, number of tests: 13, total length: 218
[MASTER] 10:59:10.751 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:113.0 - branchDistance:0.0 - coverage:47.0 - ex: 0 - tex: 16
[MASTER] 10:59:10.751 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 113.0, number of tests: 13, total length: 234
[MASTER] 10:59:12.683 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:112.0 - branchDistance:0.0 - coverage:46.0 - ex: 0 - tex: 18
[MASTER] 10:59:12.683 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 112.0, number of tests: 14, total length: 243
[MASTER] 10:59:14.221 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:111.0 - branchDistance:0.0 - coverage:45.0 - ex: 0 - tex: 16
[MASTER] 10:59:14.221 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 111.0, number of tests: 14, total length: 282
[MASTER] 10:59:23.501 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:110.0 - branchDistance:0.0 - coverage:44.0 - ex: 0 - tex: 14
[MASTER] 10:59:23.501 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 110.0, number of tests: 13, total length: 211
[MASTER] 11:00:02.057 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:108.0 - branchDistance:0.0 - coverage:42.0 - ex: 0 - tex: 12
[MASTER] 11:00:02.057 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 108.0, number of tests: 10, total length: 174
[MASTER] 11:00:03.773 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:107.0 - branchDistance:0.0 - coverage:41.0 - ex: 0 - tex: 14
[MASTER] 11:00:03.774 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 107.0, number of tests: 11, total length: 196
[MASTER] 11:00:04.943 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:106.99999999906868 - branchDistance:0.0 - coverage:40.99999999906868 - ex: 0 - tex: 12
[MASTER] 11:00:04.943 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 106.99999999906868, number of tests: 11, total length: 200
[MASTER] 11:00:06.427 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:102.99999999906868 - branchDistance:0.0 - coverage:36.99999999906868 - ex: 0 - tex: 12
[MASTER] 11:00:06.427 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 102.99999999906868, number of tests: 11, total length: 203
[MASTER] 11:00:10.206 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:101.0 - branchDistance:0.0 - coverage:35.0 - ex: 0 - tex: 14
[MASTER] 11:00:10.206 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 101.0, number of tests: 12, total length: 236
[MASTER] 11:00:33.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:100.0 - branchDistance:0.0 - coverage:34.0 - ex: 0 - tex: 14
[MASTER] 11:00:33.050 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 100.0, number of tests: 11, total length: 207
[MASTER] 11:02:20.345 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:99.0 - branchDistance:0.0 - coverage:33.0 - ex: 0 - tex: 12
[MASTER] 11:02:20.345 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 99.0, number of tests: 11, total length: 129
[MASTER] 11:02:30.979 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:93.0 - branchDistance:0.0 - coverage:27.0 - ex: 0 - tex: 14
[MASTER] 11:02:30.979 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 93.0, number of tests: 11, total length: 128
[MASTER] 11:02:39.032 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:92.0 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 14
[MASTER] 11:02:39.032 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 92.0, number of tests: 11, total length: 132
[MASTER] 11:02:58.965 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 1.4 - fitness:91.5 - branchDistance:0.0 - coverage:25.5 - ex: 0 - tex: 16
[MASTER] 11:02:58.965 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 91.5, number of tests: 12, total length: 173
[MASTER] 11:03:01.863 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 794 generations, 761020 statements, best individuals have fitness: 91.5
[MASTER] 11:03:01.867 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 81%
* Total number of goals: 74
* Number of covered goals: 60
* Generated 12 tests with total length 165
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- MaxTime :                        301 / 300          Finished!
	- ZeroFitness :                     92 / 0           
	- ShutdownTestWriter :               0 / 0           
[MASTER] 11:03:02.156 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 11:03:02.203 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 130 
[MASTER] 11:03:02.310 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 11:03:02.311 [logback-1] WARN  RegressionSuiteMinimizer - Test3 - Difference in number of exceptions: 0.0
[MASTER] 11:03:02.311 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 11:03:02.311 [logback-1] WARN  RegressionSuiteMinimizer - Test6 - Difference in number of exceptions: 1.0
[MASTER] 11:03:02.312 [logback-1] WARN  RegressionSuiteMinimizer - Test6, uniqueExceptions: []
[MASTER] 11:03:02.312 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 28
[MASTER] 11:03:02.312 [logback-1] WARN  RegressionSuiteMinimizer - Test7 - Difference in number of exceptions: 1.0
[MASTER] 11:03:02.312 [logback-1] WARN  RegressionSuiteMinimizer - Test7, uniqueExceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:03:02.312 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: parse:MissingOptionException at 30
[MASTER] 11:03:02.313 [logback-1] WARN  RegressionSuiteMinimizer - Removed exceptionA throwing line 30
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Test9 - Difference in number of exceptions: 0.0
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Test10 - Difference in number of exceptions: 0.0
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Test11 - Difference in number of exceptions: 0.0
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [parse:MissingOptionException, MissingOptionException:checkRequiredOptions-309,MissingOptionException:checkRequiredOptions-309]
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 11:03:02.325 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 5: no assertions
[MASTER] 11:03:02.326 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 7: no assertions
[MASTER] 11:03:02.326 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 8: no assertions
[MASTER] 11:03:02.326 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 9: no assertions
[MASTER] 11:03:02.326 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 10: no assertions
[MASTER] 11:03:02.326 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 11: no assertions
[MASTER] 11:03:02.786 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 17 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 11:03:02.787 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 55%
* Total number of goals: 74
* Number of covered goals: 41
* Generated 1 tests with total length 17
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 10
* Writing JUnit test case 'Parser_ESTest' to evosuite-tests
* Done!

* Computation finished
