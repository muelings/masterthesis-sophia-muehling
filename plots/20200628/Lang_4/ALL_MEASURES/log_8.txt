* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.text.translate.LookupTranslator
* Starting client
* Connecting to master process on port 6478
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.text.translate.LookupTranslator
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 06:45:32.223 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 06:45:32.228 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Total number of test goals: 14
* Using seed 1593146729948
* Starting evolution
[Progress:>                             0%] [Cov:>                                  0%][MASTER] 06:45:33.299 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.25 - fitness:22.095238095238095 - branchDistance:0.0 - coverage:15.0 - ex: 0 - tex: 10
[MASTER] 06:45:33.299 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 22.095238095238095, number of tests: 6, total length: 314
[MASTER] 06:45:33.797 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:12.818181818181818 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 12
[MASTER] 06:45:33.797 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 12.818181818181818, number of tests: 8, total length: 249
[MASTER] 06:45:34.661 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:9.818181818181818 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 14
[MASTER] 06:45:34.661 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 9.818181818181818, number of tests: 9, total length: 286
[MASTER] 06:45:36.456 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:8.818181818181818 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 14
[MASTER] 06:45:36.456 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 8.818181818181818, number of tests: 8, total length: 341
[MASTER] 06:45:37.746 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:7.818181818181818 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 8
[MASTER] 06:45:37.747 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 7.818181818181818, number of tests: 8, total length: 378
[MASTER] 06:45:40.145 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.5 - fitness:6.818181818181818 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 14
[MASTER] 06:45:40.145 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.818181818181818, number of tests: 9, total length: 225
[MASTER] 06:45:44.886 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 4.571428571428571 - fitness:6.743589743589744 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 10
[MASTER] 06:45:44.886 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 6.743589743589744, number of tests: 7, total length: 135
[MASTER] 06:45:46.663 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.071428571428571 - fitness:5.525252525252525 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 06:45:46.663 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.525252525252525, number of tests: 6, total length: 129
[MASTER] 06:45:58.484 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.321428571428571 - fitness:5.3707317073170735 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:45:58.484 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.3707317073170735, number of tests: 3, total length: 66
[MASTER] 06:46:02.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.4603174603174605 - fitness:5.2893617021276595 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:02.363 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.2893617021276595, number of tests: 4, total length: 81
[MASTER] 06:46:23.714 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 6.521428571428571 - fitness:5.254510921177588 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:23.714 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 5.254510921177588, number of tests: 4, total length: 125
[MASTER] 06:46:24.262 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 7.928260869565218 - fitness:4.584124665205746 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 06:46:24.262 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 4.584124665205746, number of tests: 4, total length: 127
[MASTER] 06:46:26.049 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 9.938888888888888 - fitness:3.9253428136109703 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:26.049 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.9253428136109703, number of tests: 3, total length: 51
[MASTER] 06:46:28.207 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 10.938888888888888 - fitness:3.680316426244765 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:28.208 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.680316426244765, number of tests: 3, total length: 43
[MASTER] 06:46:33.113 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.938888888888888 - fitness:3.4731644482610564 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:33.113 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4731644482610564, number of tests: 3, total length: 46
[MASTER] 06:46:36.433 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 11.999353085190258 - fitness:3.4616609603793718 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:36.433 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.4616609603793718, number of tests: 3, total length: 50
[MASTER] 06:46:36.478 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.928260869565218 - fitness:3.297487123458717 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:36.478 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.297487123458717, number of tests: 3, total length: 50
[MASTER] 06:46:37.293 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.967917448405252 - fitness:3.2909642842751414 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:37.293 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.2909642842751414, number of tests: 3, total length: 49
[MASTER] 06:46:37.542 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 12.971733787105313 - fitness:3.290338514002693 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:37.542 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.290338514002693, number of tests: 3, total length: 50
[MASTER] 06:46:37.743 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.928260869565218 - fitness:3.143585262851318 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:37.743 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.143585262851318, number of tests: 3, total length: 50
[MASTER] 06:46:37.834 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 13.967917448405252 - fitness:3.137905965228945 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 6
[MASTER] 06:46:37.834 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 3.137905965228945, number of tests: 4, total length: 72
[MASTER] 06:46:41.171 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.835086880280567 - fitness:2.9007920913958896 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:46:41.171 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.9007920913958896, number of tests: 3, total length: 50
[MASTER] 06:46:41.820 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 15.854294563353797 - fitness:2.8986258890702805 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:41.820 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.8986258890702805, number of tests: 3, total length: 56
[MASTER] 06:46:42.775 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.854294563353797 - fitness:2.792285877577066 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:42.775 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.792285877577066, number of tests: 3, total length: 69
[MASTER] 06:46:45.095 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 16.860073866225388 - fitness:2.7917059156465287 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:45.095 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.7917059156465287, number of tests: 2, total length: 43
[MASTER] 06:46:45.460 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.860073866225388 - fitness:2.6967059740580117 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:45.460 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6967059740580117, number of tests: 2, total length: 43
[MASTER] 06:46:45.854 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 17.87684281626051 - fitness:2.6951987316668866 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 06:46:45.854 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6951987316668866, number of tests: 3, total length: 56
[MASTER] 06:46:46.647 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.860073866225388 - fitness:2.611272959785921 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:46.647 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.611272959785921, number of tests: 2, total length: 44
[MASTER] 06:46:46.996 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.86167378622939 - fitness:2.6111431667046325 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:46.996 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6111431667046325, number of tests: 2, total length: 44
[MASTER] 06:46:49.738 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.86255311969462 - fitness:2.6110718399172237 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:46:49.738 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6110718399172237, number of tests: 2, total length: 42
[MASTER] 06:46:50.216 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.862941012587697 - fitness:2.6110403781454474 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:46:50.216 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.6110403781454474, number of tests: 2, total length: 42
[MASTER] 06:46:50.715 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.86242395618977 - fitness:2.5338581972640704 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:50.715 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5338581972640704, number of tests: 3, total length: 117
[MASTER] 06:46:53.479 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.863014965461225 - fitness:2.5338147459979337 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:53.479 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5338147459979337, number of tests: 3, total length: 82
[MASTER] 06:46:56.182 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.868926872456868 - fitness:2.5333802353888206 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:46:56.182 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5333802353888206, number of tests: 2, total length: 42
[MASTER] 06:46:58.056 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.86965885267772 - fitness:2.5333264537716285 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 2
[MASTER] 06:46:58.056 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.5333264537716285, number of tests: 3, total length: 84
[MASTER] 06:47:01.009 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.86969287192411 - fitness:2.533323954328501 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:47:01.009 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.533323954328501, number of tests: 2, total length: 42
[MASTER] 06:47:02.946 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.86969287192411 - fitness:2.399231733421513 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:47:02.946 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.399231733421513, number of tests: 2, total length: 43
[MASTER] 06:47:03.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.888421334474327 - fitness:2.398086811334686 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 0
[MASTER] 06:47:03.678 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.398086811334686, number of tests: 2, total length: 43
[MASTER] 06:47:07.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 18.888415740224715 - fitness:2.108976824397298 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:07.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.108976824397298, number of tests: 2, total length: 42
[MASTER] 06:47:07.464 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.888415740224715 - fitness:2.031949593399645 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:47:07.464 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.031949593399645, number of tests: 2, total length: 59
[MASTER] 06:47:09.563 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.888415765134248 - fitness:2.031949591572788 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:47:09.563 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.031949591572788, number of tests: 2, total length: 69
[MASTER] 06:47:11.840 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88841827984157 - fitness:2.0319494071449964 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:11.840 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.0319494071449964, number of tests: 2, total length: 46
[MASTER] 06:47:21.252 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88842200217664 - fitness:2.0319491341502722 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:47:21.253 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.0319491341502722, number of tests: 3, total length: 66
[MASTER] 06:47:23.904 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88842302573134 - fitness:2.0319490590831535 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:23.904 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.0319490590831535, number of tests: 2, total length: 42
[MASTER] 06:47:30.174 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88842849409218 - fitness:2.0319486580357387 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:47:30.174 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.0319486580357387, number of tests: 2, total length: 51
[MASTER] 06:47:32.140 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88843584403954 - fitness:2.0319481189938458 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:32.140 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.0319481189938458, number of tests: 2, total length: 42
[MASTER] 06:47:32.841 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.888615924225643 - fitness:2.031934912111046 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:32.841 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.031934912111046, number of tests: 2, total length: 42
[MASTER] 06:47:33.799 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 19.88861768681126 - fitness:2.03193478284608 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:33.799 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 2.03193478284608, number of tests: 2, total length: 42
[MASTER] 06:47:37.804 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88861789679306 - fitness:1.9619470334254578 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:37.804 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619470334254578, number of tests: 2, total length: 43
[MASTER] 06:47:38.473 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.888618385488364 - fitness:1.9619470007853599 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:47:38.473 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619470007853599, number of tests: 2, total length: 52
[MASTER] 06:47:39.896 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88861928840472 - fitness:1.9619469404793242 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:39.896 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619469404793242, number of tests: 2, total length: 43
[MASTER] 06:47:43.288 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88861997886457 - fitness:1.9619468943633211 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:47:43.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619468943633211, number of tests: 3, total length: 124
[MASTER] 06:47:44.574 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.888620322769963 - fitness:1.9619468713937864 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:47:44.574 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619468713937864, number of tests: 2, total length: 44
[MASTER] 06:48:10.325 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.888621757618363 - fitness:1.96194677555988 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:10.326 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.96194677555988, number of tests: 2, total length: 43
[MASTER] 06:48:16.327 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.888623880697175 - fitness:1.9619466337589044 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:16.327 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619466337589044, number of tests: 2, total length: 43
[MASTER] 06:48:23.816 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.8886246202495 - fitness:1.961946584364022 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:48:23.816 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.961946584364022, number of tests: 2, total length: 50
[MASTER] 06:48:27.137 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.888626153325276 - fitness:1.9619464819695238 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:27.137 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619464819695238, number of tests: 2, total length: 43
[MASTER] 06:48:29.823 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 20.88862655033718 - fitness:1.9619464554530062 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:48:29.823 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.9619464554530062, number of tests: 2, total length: 49
[MASTER] 06:48:33.944 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 21.88862655033718 - fitness:1.8980742763059586 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:33.945 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.8980742763059586, number of tests: 2, total length: 44
[MASTER] 06:48:36.319 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.88862655033718 - fitness:1.839549594137229 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:36.319 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.839549594137229, number of tests: 2, total length: 52
[MASTER] 06:48:38.858 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 22.888626553872605 - fitness:1.8395495939389808 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:38.858 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.8395495939389808, number of tests: 2, total length: 45
[MASTER] 06:48:40.313 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.888626553872605 - fitness:1.7857278376022274 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:40.313 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7857278376022274, number of tests: 2, total length: 46
[MASTER] 06:48:40.478 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.88862799933537 - fitness:1.785727762930706 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:48:40.478 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.785727762930706, number of tests: 2, total length: 57
[MASTER] 06:48:44.571 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.888628586080273 - fitness:1.7857277326199072 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:44.571 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7857277326199072, number of tests: 2, total length: 46
[MASTER] 06:48:46.612 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.888629105420097 - fitness:1.7857277057912053 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 5
[MASTER] 06:48:46.612 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7857277057912053, number of tests: 3, total length: 100
[MASTER] 06:48:46.678 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 23.888629234931415 - fitness:1.785727699100749 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:46.678 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.785727699100749, number of tests: 2, total length: 46
[MASTER] 06:48:51.125 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.888630720016103 - fitness:1.7360638284070706 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:48:51.125 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7360638284070706, number of tests: 2, total length: 47
[MASTER] 06:49:04.723 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.888630977171132 - fitness:1.736063816129093 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:49:04.723 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.736063816129093, number of tests: 2, total length: 47
[MASTER] 06:49:08.801 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.888632128057445 - fitness:1.7360637611795338 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:49:08.801 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7360637611795338, number of tests: 2, total length: 47
[MASTER] 06:49:12.779 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.88863238241648 - fitness:1.7360637490350534 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:49:12.779 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7360637490350534, number of tests: 2, total length: 47
[MASTER] 06:49:18.177 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.888633709576094 - fitness:1.736063685669257 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:49:18.177 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.736063685669257, number of tests: 2, total length: 47
[MASTER] 06:49:23.409 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.888635023073007 - fitness:1.7360636229557989 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:49:23.409 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7360636229557989, number of tests: 2, total length: 59
[MASTER] 06:49:27.705 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.88863527173128 - fitness:1.7360636110835064 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:49:27.705 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7360636110835064, number of tests: 2, total length: 47
[MASTER] 06:49:46.891 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 24.888636199877325 - fitness:1.7360635667687907 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:49:46.891 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.7360635667687907, number of tests: 2, total length: 47
[MASTER] 06:50:00.469 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.888636199877325 - fitness:1.6900938285648714 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:50:00.469 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6900938285648714, number of tests: 2, total length: 48
[MASTER] 06:50:05.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.88863681487646 - fitness:1.6900938013449465 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:50:05.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6900938013449465, number of tests: 2, total length: 48
[MASTER] 06:50:14.363 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.888637670863083 - fitness:1.6900937634588926 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:50:14.363 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6900937634588926, number of tests: 2, total length: 48
[MASTER] 06:50:14.777 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.888639725682673 - fitness:1.6900936725123812 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 1
[MASTER] 06:50:14.777 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6900936725123812, number of tests: 2, total length: 49
[MASTER] 06:50:28.637 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 25.88864109675176 - fitness:1.6900936118287402 - branchDistance:0.0 - coverage:0.0 - ex: 1 - tex: 3
[MASTER] 06:50:28.637 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 1.6900936118287402, number of tests: 2, total length: 58
[MASTER] 06:50:33.249 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1639 generations, 1419999 statements, best individuals have fitness: 1.6900936118287402
[MASTER] 06:50:33.252 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 14
* Number of covered goals: 14
* Generated 2 tests with total length 48
* Resulting test suite's coverage: 0%
* GA-Budget:
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                      2 / 0           
	- RMIStoppingCondition
[MASTER] 06:50:33.441 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:50:33.467 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 45 
[MASTER] 06:50:33.525 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 0.0
[MASTER] 06:50:33.525 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 06:50:33.525 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 06:50:33.525 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 06:50:33.526 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 14
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'LookupTranslator_ESTest' to evosuite-tests
* Done!

* Computation finished
