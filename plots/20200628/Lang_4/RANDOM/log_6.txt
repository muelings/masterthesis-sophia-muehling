* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.lang3.text.translate.LookupTranslator
* Starting client
* Connecting to master process on port 14429
* Analyzing classpath: 
  - ../defects4j_compiled/Lang_4_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.lang3.text.translate.LookupTranslator
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 14
[MASTER] 06:34:54.480 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:34:54.574 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
[MASTER] 06:34:55.849 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/text/translate/LookupTranslator_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 06:34:55.858 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
Generated test with 1 assertions.
[MASTER] 06:34:56.153 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/apache/commons/lang3/text/translate/LookupTranslator_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 1 assertions.
[MASTER] 06:34:56.158 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 1
*** Random test generation finished.
[MASTER] 06:34:56.158 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 1 | Tests with assertion: 1
* Generated 1 tests with total length 3
* GA-Budget:
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
	- MaxTime :                          1 / 300         
[MASTER] 06:34:56.473 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 06:34:56.482 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 3 
[MASTER] 06:34:56.488 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 1.0
[MASTER] 06:34:56.488 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [ArrayIndexOutOfBoundsException]
[MASTER] 06:34:56.488 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: ArrayIndexOutOfBoundsException at 2
[MASTER] 06:34:56.488 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [ArrayIndexOutOfBoundsException]
[MASTER] 06:34:56.514 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 2 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 06:34:56.518 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 14%
* Total number of goals: 14
* Number of covered goals: 2
* Generated 1 tests with total length 2
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'LookupTranslator_ESTest' to evosuite-tests
* Done!

* Computation finished
