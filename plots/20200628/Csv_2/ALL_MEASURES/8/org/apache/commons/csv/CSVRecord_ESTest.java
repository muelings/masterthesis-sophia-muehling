/*
 * This file was automatically generated by EvoSuite
 * Thu Jun 25 12:33:07 GMT 2020
 */

package org.apache.commons.csv;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.shaded.org.mockito.Mockito.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.util.HashMap;
import java.util.function.BiFunction;
import org.apache.commons.csv.CSVRecord;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.evosuite.runtime.ViolatedAssumptionAnswer;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class CSVRecord_ESTest extends CSVRecord_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      String[] stringArray0 = new String[12];
      HashMap<String, Integer> hashMap0 = new HashMap<String, Integer>();
      Integer integer0 = new Integer(2914);
      BiFunction<Object, Object, Integer> biFunction0 = (BiFunction<Object, Object, Integer>) mock(BiFunction.class, new ViolatedAssumptionAnswer());
      hashMap0.merge("5", integer0, biFunction0);
      CSVRecord cSVRecord0 = new CSVRecord(stringArray0, hashMap0, "C", 2914);
      // EXCEPTION DIFF:
      // Different Exceptions were thrown:
      // Original Version:
      //     org.evosuite.runtime.mock.java.lang.MockIllegalArgumentException : Index for header '5' is 2914 but CSVRecord only has 12 values!
      // Modified Version:
      //     java.lang.ArrayIndexOutOfBoundsException : null
      // Undeclared exception!
      try { 
        cSVRecord0.get("5");
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // Index for header '5' is 2914 but CSVRecord only has 12 values!
         //
         verifyException("org.apache.commons.csv.CSVRecord", e);
         assertTrue(e.getMessage().equals("Index for header '5' is 2914 but CSVRecord only has 12 values!"));   
      }
  }
}
