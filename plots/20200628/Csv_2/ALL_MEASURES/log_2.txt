* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 10380
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 13:56:31.356 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 13:56:31.361 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593086189303
* Starting evolution
[MASTER] 13:56:33.037 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:84.33333333333333 - branchDistance:0.0 - coverage:23.333333333333332 - ex: 0 - tex: 2
[MASTER] 13:56:33.037 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 84.33333333333333, number of tests: 5, total length: 118
[MASTER] 13:56:33.136 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:82.0 - branchDistance:0.0 - coverage:21.0 - ex: 0 - tex: 6
[MASTER] 13:56:33.136 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 82.0, number of tests: 6, total length: 134
[MASTER] 13:56:33.244 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:75.0 - branchDistance:0.0 - coverage:14.0 - ex: 0 - tex: 6
[MASTER] 13:56:33.244 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 75.0, number of tests: 10, total length: 210
[MASTER] 13:56:33.732 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:72.0 - branchDistance:0.0 - coverage:11.0 - ex: 0 - tex: 10
[MASTER] 13:56:33.732 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 72.0, number of tests: 9, total length: 230
[MASTER] 13:56:34.024 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:69.66666666666667 - branchDistance:0.0 - coverage:8.666666666666668 - ex: 0 - tex: 4
[MASTER] 13:56:34.024 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 69.66666666666667, number of tests: 8, total length: 181
[MASTER] 13:56:35.309 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:68.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 0
[MASTER] 13:56:35.309 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.0, number of tests: 7, total length: 156
[MASTER] 13:56:35.631 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:67.33333333333333 - branchDistance:0.0 - coverage:6.333333333333333 - ex: 0 - tex: 6
[MASTER] 13:56:35.631 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.33333333333333, number of tests: 10, total length: 244
[MASTER] 13:56:35.646 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:67.0 - branchDistance:0.0 - coverage:6.0 - ex: 0 - tex: 4
[MASTER] 13:56:35.646 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 67.0, number of tests: 7, total length: 155
[MASTER] 13:56:36.557 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.0 - branchDistance:0.0 - coverage:4.0 - ex: 0 - tex: 4
[MASTER] 13:56:36.558 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.0, number of tests: 8, total length: 192
[MASTER] 13:56:39.465 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.0 - branchDistance:0.0 - coverage:3.0 - ex: 0 - tex: 4
[MASTER] 13:56:39.465 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.0, number of tests: 8, total length: 187
[MASTER] 13:56:41.909 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 2
[MASTER] 13:56:41.909 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 8, total length: 142
[MASTER] 13:56:42.069 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 2
[MASTER] 13:56:42.069 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 7, total length: 129
[MASTER] 13:56:42.339 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 13:56:42.339 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 7, total length: 133
[MASTER] 14:01:32.371 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)

* Search finished after 301s and 1761 generations, 1406634 statements, best individuals have fitness: 61.0
[MASTER] 14:01:32.374 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 4 tests with total length 43
* Resulting test suite's coverage: 0%
* GA-Budget:
	- RMIStoppingCondition
	- ShutdownTestWriter :               0 / 0           
	- ZeroFitness :                     61 / 0           
	- MaxTime :                        301 / 300          Finished!
[MASTER] 14:01:32.497 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:01:32.520 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 32 
[MASTER] 14:01:32.612 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 14:01:32.612 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 14:01:32.612 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 14:01:32.612 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 14:01:32.612 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 14:01:32.612 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
[MASTER] 14:01:32.613 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 25
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 1
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
