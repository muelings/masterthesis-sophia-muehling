* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.apache.commons.csv.CSVRecord
* Starting client
* Connecting to master process on port 9515
* Analyzing classpath: 
  - ../defects4j_compiled/Csv_2_fixed/target/classes
* Finished analyzing classpath
* Generating tests for class org.apache.commons.csv.CSVRecord
* Test criterion:
  - Regression
* Setting up search algorithm for REGRESSION suite generation
[MASTER] 14:12:20.539 [logback-1] WARN  TestSuiteFitnessFunction - ### initialising Regression-GA... ###
[MASTER] 14:12:20.544 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[Progress:>                             0%] [Cov:>                                  0%]* Total number of test goals: 25
* Using seed 1593087138268
* Starting evolution
[MASTER] 14:12:21.863 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:87.0 - branchDistance:0.0 - coverage:26.0 - ex: 0 - tex: 4
[MASTER] 14:12:21.863 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 87.0, number of tests: 5, total length: 128
[MASTER] 14:12:22.477 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:77.0 - branchDistance:0.0 - coverage:16.0 - ex: 0 - tex: 8
[MASTER] 14:12:22.477 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 77.0, number of tests: 8, total length: 186
[MASTER] 14:12:22.623 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:68.0 - branchDistance:0.0 - coverage:7.0 - ex: 0 - tex: 8
[MASTER] 14:12:22.623 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 68.0, number of tests: 10, total length: 254
[MASTER] 14:12:23.259 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:65.9989832231825 - branchDistance:0.0 - coverage:4.9989832231825115 - ex: 0 - tex: 4
[MASTER] 14:12:23.259 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 65.9989832231825, number of tests: 8, total length: 218
[MASTER] 14:12:24.288 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.93333333333334 - branchDistance:0.0 - coverage:3.933333333333333 - ex: 0 - tex: 6
[MASTER] 14:12:24.288 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.93333333333334, number of tests: 8, total length: 243
[MASTER] 14:12:25.771 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:64.6 - branchDistance:0.0 - coverage:3.6 - ex: 0 - tex: 6
[MASTER] 14:12:25.771 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 64.6, number of tests: 9, total length: 274
[MASTER] 14:12:26.869 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.333333333333336 - branchDistance:0.0 - coverage:2.333333333333333 - ex: 0 - tex: 6
[MASTER] 14:12:26.869 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.333333333333336, number of tests: 8, total length: 236
[MASTER] 14:12:28.074 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:63.0 - branchDistance:0.0 - coverage:2.0 - ex: 0 - tex: 6
[MASTER] 14:12:28.074 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 63.0, number of tests: 8, total length: 224
[MASTER] 14:12:28.115 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:62.0 - branchDistance:0.0 - coverage:1.0 - ex: 0 - tex: 6
[MASTER] 14:12:28.115 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 62.0, number of tests: 9, total length: 256
[MASTER] 14:12:32.831 [logback-1] WARN  TestSuiteFitnessFunction - OBJ distance: 0.0 - fitness:61.0 - branchDistance:0.0 - coverage:0.0 - ex: 0 - tex: 4
[MASTER] 14:12:32.831 [logback-1] WARN  TestSuiteFitnessFunction - Best Fitness 61.0, number of tests: 8, total length: 187

[MASTER] 14:17:21.555 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Search finished after 301s and 1536 generations, 1236922 statements, best individuals have fitness: 61.0
[MASTER] 14:17:21.559 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Coverage of criterion REGRESSION: 100%
* Total number of goals: 25
* Number of covered goals: 25
* Generated 5 tests with total length 50
* Resulting test suite's coverage: 0%
* GA-Budget:
	- ZeroFitness :                     61 / 0           
	- MaxTime :                        301 / 300          Finished!
	- ShutdownTestWriter :               0 / 0           
	- RMIStoppingCondition
[MASTER] 14:17:21.703 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 14:17:21.736 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 35 
[MASTER] 14:17:21.831 [logback-1] WARN  RegressionSuiteMinimizer - Test1 - Difference in number of exceptions: 0.0
[MASTER] 14:17:21.832 [logback-1] WARN  RegressionSuiteMinimizer - Test4 - Difference in number of exceptions: 0.0
[MASTER] 14:17:21.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 0: no assertions
[MASTER] 14:17:21.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 1: no assertions
[MASTER] 14:17:21.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 2: no assertions
[MASTER] 14:17:21.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 3: no assertions
[MASTER] 14:17:21.832 [logback-1] WARN  RegressionSuiteMinimizer - Removing test 4: no assertions
[MASTER] 14:17:21.833 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 0 
[MASTER] 14:17:21.833 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 0%
* Total number of goals: 25
* Number of covered goals: 0
* Generated 0 tests with total length 0
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Permissions denied during test execution: 
  - java.lang.RuntimePermission: 
         writeFileDescriptor: 2
* Writing JUnit test case 'CSVRecord_ESTest' to evosuite-tests
* Done!

* Computation finished
