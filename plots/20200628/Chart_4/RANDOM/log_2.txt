* EvoSuite 1.0.6
* Configuration: regressionSuite
* Going to generate test cases for class: org.jfree.chart.plot.XYPlot
* Starting client
* Connecting to master process on port 19677
* Analyzing classpath: 
  - ../defects4j_compiled/Chart_4_fixed/build
* Finished analyzing classpath
* Generating tests for class org.jfree.chart.plot.XYPlot
* Test criterion:
  - Regression
* Using RANDOM regression test generation
*** generating RANDOM regression tests
* Total number of test goals: 1079
[MASTER] 01:41:47.288 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:41:49.210 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
[MASTER] 01:41:50.762 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_1_tmp__ESTest.class' should be in target project, but could not be found!
[MASTER] 01:41:50.811 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
Generated test with 2 assertions.
[MASTER] 01:41:51.243 [logback-1] WARN  JUnitAnalyzer - Found unstable test named initializationError -> class java.lang.ClassNotFoundException: Class 'org/jfree/chart/plot/XYPlot_3_tmp__ESTest.class' should be in target project, but could not be found!
Keeping 2 assertions.
[MASTER] 01:41:51.288 [logback-1] WARN  RegressionAssertionCounter - Assertions generated for the individual: 2
*** Random test generation finished.
[MASTER] 01:41:51.288 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
*=*=*=* Total tests: 8 | Tests with assertion: 1
* Generated 1 tests with total length 33
* GA-Budget:
	- ShutdownTestWriter :               0 / 0           
	- MaxTime :                          4 / 300         
	- ZeroFitness :           9,223,372,036,854,775,807 / 0           
	- RMIStoppingCondition
[MASTER] 01:41:54.191 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
[MASTER] 01:41:54.250 [logback-1] WARN  RegressionSuiteMinimizer - Going to minimize test suite. Length: 23 
[MASTER] 01:41:54.292 [logback-1] WARN  RegressionSuiteMinimizer - Test0 - Difference in number of exceptions: 2.0
[MASTER] 01:41:54.292 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException, ArrayIndexOutOfBoundsException,, ArrayIndexOutOfBoundsException]
[MASTER] 01:41:54.292 [logback-1] WARN  RegressionSuiteMinimizer - checking exception: ArrayIndexOutOfBoundsException at 22
[MASTER] 01:41:54.292 [logback-1] WARN  RegressionSuiteMinimizer - Test0, uniqueExceptions: [NullPointerException, ArrayIndexOutOfBoundsException,, ArrayIndexOutOfBoundsException]
[MASTER] 01:41:54.292 [logback-1] WARN  RegressionSuiteMinimizer - checking exceptionB: NullPointerException at 3
[MASTER] 01:41:54.292 [logback-1] WARN  RegressionSuiteMinimizer - unique exceptions: [NullPointerException, ArrayIndexOutOfBoundsException,, ArrayIndexOutOfBoundsException]
[MASTER] 01:41:55.290 [logback-1] WARN  RegressionSuiteMinimizer - Minimized Length: 6 
[MASTER] 01:41:55.295 [logback-1] WARN  FitnessFunctions - No TestFitnessFactory defined for REGRESSION using default one (BranchCoverageFactory)
* Going to analyze the coverage criteria
* Coverage analysis for criterion REGRESSION
* Coverage of criterion REGRESSION: 5%
* Total number of goals: 1079
* Number of covered goals: 59
* Generated 1 tests with total length 6
* Resulting test suite's coverage: 0%
* Compiling and checking tests
* Writing JUnit test case 'XYPlot_ESTest' to evosuite-tests
* Done!

* Computation finished
