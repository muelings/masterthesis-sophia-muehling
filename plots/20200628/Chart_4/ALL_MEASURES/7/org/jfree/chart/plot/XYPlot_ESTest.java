/*
 * This file was automatically generated by EvoSuite
 * Thu Jun 25 00:20:30 GMT 2020
 */

package org.jfree.chart.plot;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.awt.geom.GeneralPath;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.gantt.XYTaskDataset;
import org.jfree.data.xy.XYDataset;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class XYPlot_ESTest extends XYPlot_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      XYPlot xYPlot0 = new XYPlot();
      TaskSeriesCollection taskSeriesCollection0 = new TaskSeriesCollection();
      XYTaskDataset xYTaskDataset0 = new XYTaskDataset(taskSeriesCollection0);
      xYPlot0.setDataset(32, (XYDataset) xYTaskDataset0);
      NumberAxis numberAxis0 = new NumberAxis("");
      // EXCEPTION DIFF:
      // The original version did not exhibit this exception:
      //     java.lang.NullPointerException : null
      xYPlot0.setDomainAxis(0, (ValueAxis) numberAxis0, true);
      // EXCEPTION DIFF:
      // The modified version did not exhibit this exception:
      //     java.lang.IllegalArgumentException : winding rule must be WIND_EVEN_ODD or WIND_NON_ZERO
      GeneralPath generalPath0 = null;
      try {
        generalPath0 = new GeneralPath(10);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // winding rule must be WIND_EVEN_ODD or WIND_NON_ZERO
         //
         verifyException("java.awt.geom.Path2D", e);
         assertTrue(e.getMessage().equals("winding rule must be WIND_EVEN_ODD or WIND_NON_ZERO"));   
      }
  }
}
