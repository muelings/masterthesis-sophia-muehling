Sources and Implementation of my master thesis on "Instance Space Analysis of Test Suite Augmentation Problems".

"defects4j_compiled" includes all compiled subject versions of Defects4J version 2.0.0 (clone from https://github.com/rjust/defects4j}{https://github.com/rjust/defects4j of 20 April 2020) 
 and results of CKJM extended version 2.3 (http://gromit.iiar.pwr.wroc.pl/p_inf/ckjm/down.html of 11 May 2020), as well as the following scripts:
- compileAll.sh: compile and calculate CKJM features of the first 5 versions of each Defects4J subject 
- defects4j_removegit.sh: remove git cache of each compiled version of Defects4J subjects
- getModified.sh: get modified classes of each subject version and calculate CKJM features
- calcCC.php: calculate the average CC value of each instance

"evosuite" includes the EvoSuite version 1.0.7-SNAPSHOT (clone from https://github.com/EvoSuite/evosuite}{https://github.com/EvoSuite/evosuite of 21 May 2020)
 and the following scripts:
- run_all.sh: run all experiments with EvoSuiteR on the selected Defetcs4J subjects
- run_allTests.sh: run Jacoco to get the following information for all resulting test suites, 
	- if the tests fail for the buggy version and run for the fixed version and
	- the coverage for instruction, branch, line, complexity and method
 
"metadata" includes all metadata for different MATILDA experiments (combined resulting measuring values from CKJM and Jacoco)

"plots" includes the generated test suites of the individual EvoSuiteR runs on Defects4J instances with according logs, as well as the statistics from all runs

"references" includes all papers that were found to be interesting for my research

"tests" includes all results from the Jacoco runs on the generated test suites, as well as the Project Object Model (POM) XML representation of the Jacoco test Maven project (also see run_allTests script in "evosuite")
 
"thesis" includes LateX document and linked sources of the thesis, as well as templates of the institute.