#!/bin/sh

evorun(){
 x=$1
 algorithm=$2
 budget=$3
 projectname=$4
 classname=$5
 dirname=$6
 mkdir ../plots/$projectname/
 mkdir ../plots/$projectname/$algorithm/
 java -Djava.ext.dirs="../defects4j_compiled/"$projectname"_fixed" -jar evosuite-1.0.6.jar -regressionSuite -projectCP "../defects4j_compiled/"$projectname"_fixed/"$dirname -Dregressioncp="../defects4j_compiled/"$projectname"_buggy/"$dirname -Dregression_fitness=$algorithm -Dsearch_budget=$budget -class $classname -Dreport_dir=../plots -Dconfiguration_id=regressionSuite -Doutput_variables=TARGET_CLASS,regression_fitness,search_budget,Total_Time,Size,Length,Coverage,Fitness,CoverageTimeline,FitnessTimeline -Dtimeline_interval=1000 >> ../plots/$projectname/$algorithm/"log_"$x".txt"
 mv evosuite-tests ../plots/$projectname/$algorithm/$x >> ../plots/$projectname/$algorithm/"log_"$x".txt"

}

evo10timesrun(){
 project=$1
 class=$2
 name=$3
 for i in 1 2 3 4 5 6 7 8 9 10; do
  evorun $i RANDOM 300 $project $class $name
  evorun $i ALL_MEASURES 300 $project $class $name
 done;
}

# evo10timesrun Chart_1 org.jfree.chart.renderer.category.AbstractCategoryItemRenderer build
# evo10timesrun Chart_2 org.jfree.data.general.DatasetUtilities build
# evo10timesrun Chart_3 org.jfree.data.time.TimeSeries build
evo10timesrun Chart_4 org.jfree.chart.plot.XYPlot build
evo10timesrun Chart_5 org.jfree.data.xy.XYSeries build

evo10timesrun Cli_1 org.apache.commons.cli.CommandLine target/classes/
evo10timesrun Cli_2 org.apache.commons.cli.PosixParser target/classes/
evo10timesrun Cli_3 org.apache.commons.cli.TypeHandler target/classes/
evo10timesrun Cli_4 org.apache.commons.cli.Parser target/classes/
evo10timesrun Cli_5 org.apache.commons.cli.Util target/classes/

# evo10timesrun Closure_1 com.google.javascript.jscomp.RemoveUnusedVars build/classes
evo10timesrun Closure_2 com.google.javascript.jscomp.TypeCheck build/classes
# evo10timesrun Closure_3 com.google.javascript.jscomp.FlowSensitiveInlineVariables build/classes
# evo10timesrun Closure_4 com.google.javascript.rhino.jstype.NamedType build/classes
evo10timesrun Closure_5 com.google.javascript.jscomp.InlineObjectLiterals build/classes

#evo10timesrun Codec_1 org.apache.commons.codec.language.Caverphone target/classes
#evo10timesrun Codec_1 org.apache.commons.codec.language.Metaphone target/classes
#evo10timesrun Codec_1 org.apache.commons.codec.language.SoundexUtils target/classes
evo10timesrun Codec_2 org.apache.commons.codec.binary.Base64 target/classes
evo10timesrun Codec_3 org.apache.commons.codec.language.DoubleMetaphone target/classes
# evo10timesrun Codec_4 org.apache.commons.codec.binary.Base64 target/classes
# evo10timesrun Codec_5 org.apache.commons.codec.binary.Base64 target/classes

evo10timesrun Collections_25 org.apache.commons.collections4.IteratorUtils target/classes/
evo10timesrun Collections_26 org.apache.commons.collections4.keyvalue.MultiKey target/classes/
evo10timesrun Collections_27 org.apache.commons.collections4.map.MultiValueMap target/classes/
evo10timesrun Collections_28 org.apache.commons.collections4.trie.AbstractPatriciaTrie target/classes/

evo10timesrun Compress_1 org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream target/classes
evo10timesrun Compress_2 org.apache.commons.compress.archivers.ar.ArArchiveInputStream target/classes
# evo10timesrun Compress_3 org.apache.commons.compress.archivers.tar.TarArchiveOutputStream target/classes
#evo10timesrun Compress_4 org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream target/classes
#evo10timesrun Compress_4 org.apache.commons.compress.archivers.tar.TarArchiveOutputStream target/classes
#evo10timesrun Compress_4 org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream target/classes
#evo10timesrun Compress_4 org.apache.commons.compress.changes.ChangeSetPerformer target/classes
# evo10timesrun Compress_5 org.apache.commons.compress.archivers.zip.ZipArchiveInputStream target/classes

evo10timesrun Csv_1 org.apache.commons.csv.ExtendedBufferedReader target/classes
evo10timesrun Csv_2 org.apache.commons.csv.CSVRecord target/classes
evo10timesrun Csv_3 org.apache.commons.csv.Lexer target/classes
evo10timesrun Csv_4 org.apache.commons.csv.CSVParser target/classes
evo10timesrun Csv_5 org.apache.commons.csv.CSVPrinter target/classes

evo10timesrun Gson_1 com.google.gson.TypeInfoFactory target/classes
# evo10timesrun Gson_2 com.google.gson.internal.bind.TypeAdapters target/classes
evo10timesrun Gson_3 com.google.gson.internal.ConstructorConstructor target/classes
#evo10timesrun Gson_4 com.google.gson.stream.JsonReader target/classes
#evo10timesrun Gson_4 com.google.gson.stream.JsonWriter target/classes
# evo10timesrun Gson_5 com.google.gson.internal.bind.util.ISO8601Utils target/classes

#evo10timesrun JacksonCore_1 com.fasterxml.jackson.core.io.NumberInput target/classes
#evo10timesrun JacksonCore_1 com.fasterxml.jackson.core.util.TextBuffer target/classes
#evo10timesrun JacksonCore_2 com.fasterxml.jackson.core.json.ReaderBasedJsonParser target/classes
#evo10timesrun JacksonCore_2 com.fasterxml.jackson.core.json.UTF8StreamJsonParser target/classes
# evo10timesrun JacksonCore_3 com.fasterxml.jackson.core.json.UTF8StreamJsonParser target/classes
evo10timesrun JacksonCore_4 com.fasterxml.jackson.core.util.TextBuffer target/classes
evo10timesrun JacksonCore_5 com.fasterxml.jackson.core.JsonPointer target/classes

# evo10timesrun JacksonDatabind_1 com.fasterxml.jackson.databind.ser.BeanPropertyWriter target/classes
# evo10timesrun JacksonDatabind_2 com.fasterxml.jackson.databind.util.TokenBuffer target/classes
# evo10timesrun JacksonDatabind_3 com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer target/classes
evo10timesrun JacksonDatabind_4 com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer target/classes
evo10timesrun JacksonDatabind_5 com.fasterxml.jackson.databind.introspect.AnnotatedClass target/classes

evo10timesrun JacksonXml_1 com.fasterxml.jackson.dataformat.xml.deser.FromXmlParser target/classes/
evo10timesrun JacksonXml_2 com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream target/classes/
evo10timesrun JacksonXml_3 com.fasterxml.jackson.dataformat.xml.deser.FromXmlParser target/classes/
evo10timesrun JacksonXml_4 com.fasterxml.jackson.dataformat.xml.ser.XmlSerializerProvider target/classes/
evo10timesrun JacksonXml_5 com.fasterxml.jackson.dataformat.xml.ser.XmlSerializerProvider target/classes/

# evo10timesrun Jsoup_1 org.jsoup.nodes.Document target/classes
# evo10timesrun Jsoup_2 org.jsoup.parser.Parser target/classes
#evo10timesrun Jsoup_3 org.jsoup.nodes.Element target/classes
#evo10timesrun Jsoup_3 org.jsoup.parser.Parser target/classes
#evo10timesrun Jsoup_3 org.jsoup.parser.Tag target/classes
evo10timesrun Jsoup_4 org.jsoup.nodes.Entities target/classes
evo10timesrun Jsoup_5 org.jsoup.parser.Parser target/classes

#evo10timesrun JxPath_1 org.apache.commons.jxpath.ri.model.dom.DOMNodePointer target/classes
#evo10timesrun JxPath_1 org.apache.commons.jxpath.ri.model.jdom.JDOMNodePointer target/classes
# evo10timesrun JxPath_2 org.apache.commons.jxpath.ri.compiler.Expression target/classes
evo10timesrun JxPath_3 org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer target/classes
#evo10timesrun JxPath_4 org.apache.commons.jxpath.ri.model.dom.DOMNodePointer target/classes
#evo10timesrun JxPath_4 org.apache.commons.jxpath.ri.model.jdom.JDOMNodePointer target/classes
evo10timesrun JxPath_5 org.apache.commons.jxpath.ri.model.NodePointer target/classes

evo10timesrun Lang_1 org.apache.commons.lang3.math.NumberUtils target/classes
#evo10timesrun Lang_2 "" target/classes
# evo10timesrun Lang_3 org.apache.commons.lang3.math.NumberUtils target/classes
evo10timesrun Lang_4 org.apache.commons.lang3.text.translate.LookupTranslator target/classes
# evo10timesrun Lang_5 org.apache.commons.lang3.LocaleUtils target/classes

#evo10timesrun Math_1 org.apache.commons.math3.fraction.BigFraction target/classes
#evo10timesrun Math_1 org.apache.commons.math3.fraction.Fraction target/classes
evo10timesrun Math_2 org.apache.commons.math3.distribution.HypergeometricDistribution target/classes
# evo10timesrun Math_3 org.apache.commons.math3.util.MathArrays target/classes
#evo10timesrun Math_4 org.apache.commons.math3.geometry.euclidean.threed.SubLine target/classes
#evo10timesrun Math_4 org.apache.commons.math3.geometry.euclidean.twod.SubLine target/classes
evo10timesrun Math_5 org.apache.commons.math3.complex.Complex target/classes

evo10timesrun Mockito_1 org.mockito.internal.invocation.InvocationMatcher build/classes/main/
evo10timesrun Mockito_2 org.mockito.internal.util.Timer build/classes/main/
evo10timesrun Mockito_3 org.mockito.internal.invocation.InvocationMatcher build/classes/main/
evo10timesrun Mockito_4 org.mockito.exceptions.Reporter build/classes/main/
evo10timesrun Mockito_5 org.mockito.internal.verification.VerificationOverTimeImpl build/classes/main/

#evo10timesrun Time_1 org.joda.time.field.UnsupportedDurationField target/classes
#evo10timesrun Time_1 org.joda.time.Partial target/classes
#evo10timesrun Time_2 org.joda.time.field.UnsupportedDurationField target/classes
#evo10timesrun Time_2 org.joda.time.Partial target/classes
evo10timesrun Time_3 org.joda.time.MutableDateTime target/classes
evo10timesrun Time_4 org.joda.time.Partial target/classes
evo10timesrun Time_5 org.joda.time.Period target/classes