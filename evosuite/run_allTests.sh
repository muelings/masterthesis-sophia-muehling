#!/bin/sh 

writeTestDataToCSV(){
	classname=$1
	destfile=$2
	testversion=$3
	testalg=$4
	INPUT=jacoco.csv
	OLDIFS=$IFS
	IFS=','
	
	Icov=0
	Bcov=0
	Lcov=0
	Ccov=0
	Mcov=0
	testruns=0
	testfails=0
	
	cdDir="site/jacoco"
	if [ $(ls -A $cdDir) ]; then
		cd $cdDir
		[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
		while read GROUP PACKAGE CLASS INSTRUCTION_MISSED INSTRUCTION_COVERED BRANCH_MISSED BRANCH_COVERED LINE_MISSED LINE_COVERED COMPLEXITY_MISSED COMPLEXITY_COVERED METHOD_MISSED METHOD_COVERED
		do
			if [ $PACKAGE'.'$CLASS = $classname ]; then
				Icov=$(awk "BEGIN {print 0.0+$INSTRUCTION_COVERED/($INSTRUCTION_MISSED+$INSTRUCTION_COVERED)}")
				Bcov=$(awk "BEGIN {print 0.0+$BRANCH_COVERED/($BRANCH_MISSED+$BRANCH_COVERED)}")
				Lcov=$(awk "BEGIN {print 0.0+$LINE_COVERED/($LINE_MISSED+$LINE_COVERED)}")
				Ccov=$(awk "BEGIN {print 0.0+$COMPLEXITY_COVERED/($COMPLEXITY_MISSED+$COMPLEXITY_COVERED)}")
				Mcov=$(awk "BEGIN {print 0.0+$METHOD_COVERED/($METHOD_MISSED+$METHOD_COVERED)}")
				echo "Instruction Coverage : $Icov"
				echo "Branch Coverage : $Bcov"
				echo "Line Coverage : $Lcov"
				echo "Complexity Coverage : $Ccov"
				echo "Method Coverage : $Mcov"
			fi
		done < $INPUT
		IFS=$OLDIFS
		
		cd ../../surefire-reports

		for f in *.txt
		do
			let testruns+=$(grep 'Tests run' $f | sed -e 's/Tests run: //g' -e 's/, .*//g')
			let testfails+=$(grep 'Tests run' $f | sed -e 's/Tests run: [0-9]*, Failures: [0-9]*, Errors: //g' -e 's/, Skipped.*//g')
		done
		cd ..
	
	else
		echo "$DIR is Empty"
	fi
	IFS=$OLDIFS
	echo $testversion/$testalg/$classname","$testfails","$testruns","$Icov","$Bcov","$Lcov","$Ccov","$Mcov >> $destfile
	
}

testrun(){
 algorithm=$1
 version=$2
 projectname=$3
 classname=$4
 
 path=$(echo $classname | sed -e 's/\./\//g')
 pWOTest=$(echo $classname | sed -e's/.[a-zA-Z_]\+$//g')
 pWOTSlash=$(echo $pWOTest | sed -e 's/\./\//g')

 cd ../plots/20200702/$projectname/$algorithm/
 file=../../../../tests/Subjects/$projectname"_"$version/$algorithm/target/test-classes/$path
 for d in */ ; do
	n=$(echo $d | sed -e 's/\///g')
	testfile=$d"/"$path"_ESTest.java"
	testscaffolding=$d"/"$path"_ESTest_scaffolding.java"
	if grep -Hrn --text test0 $d && [[ $d =~ ^[0-9] ]]; then
	  echo "test"
	  output=$(tr -d '\0' <$testfile)
	  echo $output | sed -e 's/separateClassLoader = true/separateClassLoader = false/g' -e 's/ESTest extends/'$n'_ESTest extends/g' -e 's/ESTest_scaffolding/'$n'_ESTest_scaffolding/g' >> $file"_"$n"_ESTest.java"
	  echo "scaff"
	  outputScaff=$(tr -d '\0' <$testscaffolding)
	  echo $outputScaff | sed -e 's/ESTest_scaffolding/'$n'_ESTest_scaffolding/g' >> $file"_"$n"_ESTest_scaffolding.java"
	  cp $file"_"$n"_ESTest.java" ../../../../tests/Subjects/$projectname"_"$version/$algorithm/src/test/java/$path"_"$n"_ESTest.java"
	  cp $file"_"$n"_ESTest_scaffolding.java" ../../../../tests/Subjects/$projectname"_"$version/$algorithm/src/test/java/$path"_"$n"_ESTest_scaffolding.java"
	  sync
	  cd ../../../../tests/
	  javac -cp "Subjects/"$projectname"_"$version/$algorithm/"target/classes;jar/junit-4.13.jar;jar/evosuite-1.0.6.jar;jar/*;." Subjects/$projectname"_"$version/$algorithm/target/test-classes/$path"_"$n"_ESTest_scaffolding.java" 
	  cd ../plots/20200702/$projectname/$algorithm/
	fi	 
 done
 cd ../../../../tests/
	javac -cp "Subjects/"$projectname"_"$version/$algorithm/"target/classes;jar/junit-4.13.jar;jar/evosuite-1.0.6.jar;Subjects/"$projectname"_"$version/$algorithm"/target/test-classes/"$path/"*;jar/*;." Subjects/$projectname"_"$version/$algorithm/target/test-classes/$path"*.java"
 cd ../evosuite
}

evoReport(){
 p=$1
 c=$2

 echo "WRITE REPORT"
 for vers in "buggy" "fixed"; do
	for alg in "RANDOM" "ALL_MEASURES"; do
		cd ../tests/Subjects/$p"_"$vers/$alg/target
		writeTestDataToCSV $c "C:/Users/Sophia Mühling/Documents/Studium/TestGenAug/Masterarbeit/masterthesis-sophia-muehling/tests/testdata.csv" $vers $alg
		cd ../../../../../evosuite
	done
 done 
}

evoCoverage(){
 project=$1
 class=$2
 cls=$3
 src=$4
 
 pWOTest=$(echo $class | sed -e's/.[a-zA-Z_]\+$//g')
 pWOTSlash=$(echo $pWOTest | sed -e 's/\./\//g')
 

 for vers in "buggy" "fixed"; do
  for alg in "RANDOM" "ALL_MEASURES"; do	
	mkdir -p ../tests/Subjects/$project"_"$vers/$alg/src/main/java
	mkdir -p ../tests/Subjects/$project"_"$vers/$alg/target/classes
	
	mkdir -p ../tests/Subjects/$project"_"$vers/$alg/src/test/java/$pWOTSlash
	mkdir -p ../tests/Subjects/$project"_"$vers/$alg/target/test-classes/$pWOTSlash
	
	cp -r ../defects4j_compiled/$project"_"$vers/$src/* ../tests/Subjects/$project"_"$vers/$alg/src/main/java
	cp -r ../defects4j_compiled/$project"_"$vers/$cls/* ../tests/Subjects/$project"_"$vers/$alg/target/classes
	cp -r ../tests/pom.xml ../tests/Subjects/$project"_"$vers/$alg/pom.xml
	
	sync
	echo TEST running for $alg $vers $project $class
	testrun $alg $vers $project $class
	cd ../tests/Subjects/$project"_"$vers/$alg
	mvn test
	mvn test -DskipTests
	
	cd ../../../../evosuite
  done
 done 
 
 evoReport $project $class
}

 echo "classname,testfailures,testtotal,instruction_coverage,branch_coverage,line_coverage,complexity_coverage,method_coverage" >> "C:/Users/Sophia Mühling/Documents/Studium/TestGenAug/Masterarbeit/masterthesis-sophia-muehling/tests/testdata.csv"

# evoCoverage Chart_1 org.jfree.chart.renderer.category.AbstractCategoryItemRenderer build source
# evoCoverage Chart_2 org.jfree.data.general.DatasetUtilities build source
# evoCoverage Chart_3 org.jfree.data.time.TimeSeries build source
evoCoverage Chart_4 org.jfree.chart.plot.XYPlot build source
evoCoverage Chart_5 org.jfree.data.xy.XYSeries build source

evoCoverage Cli_1 org.apache.commons.cli.CommandLine target/classes src/java
evoCoverage Cli_2 org.apache.commons.cli.PosixParser target/classes src/java
evoCoverage Cli_3 org.apache.commons.cli.TypeHandler target/classes src/java
evoCoverage Cli_4 org.apache.commons.cli.Parser target/classes src/java
evoCoverage Cli_5 org.apache.commons.cli.Util target/classes src/java

# evoCoverage Closure_1 com.google.javascript.jscomp.RemoveUnusedVars build/classes
evoCoverage Closure_2 com.google.javascript.jscomp.TypeCheck build/classes src
# evoCoverage Closure_3 com.google.javascript.jscomp.FlowSensitiveInlineVariables build/classes
# evoCoverage Closure_4 com.google.javascript.rhino.jstype.NamedType build/classes
evoCoverage Closure_5 com.google.javascript.jscomp.InlineObjectLiterals build/classes src

#evoCoverage Codec_1 org.apache.commons.codec.language.Caverphone target/classes src/java
#evoCoverage Codec_1 org.apache.commons.codec.language.Metaphone target/classes src/java
#evoCoverage Codec_1 org.apache.commons.codec.language.SoundexUtils target/classes src/java
evoCoverage Codec_2 org.apache.commons.codec.binary.Base64 target/classes src/java
evoCoverage Codec_3 org.apache.commons.codec.language.DoubleMetaphone target/classes src/java
# evoCoverage Codec_4 org.apache.commons.codec.binary.Base64 target/classes src/java
# evoCoverage Codec_5 org.apache.commons.codec.binary.Base64 target/classes src/java

evoCoverage Collections_25 org.apache.commons.collections4.IteratorUtils target/classes src/main/java
evoCoverage Collections_26 org.apache.commons.collections4.keyvalue.MultiKey target/classes src/main/java
evoCoverage Collections_27 org.apache.commons.collections4.map.MultiValueMap target/classes/ src/main/java
#evoCoverage Collections_28 org.apache.commons.collections4.trie.AbstractPatriciaTrie target/classes/ src/main/java

###TODO Compress
evoCoverage Compress_1 org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream target/classes src/main/java 
evoCoverage Compress_2 org.apache.commons.compress.archivers.ar.ArArchiveInputStream target/classes src/main/java
# evoCoverage Compress_3 org.apache.commons.compress.archivers.tar.TarArchiveOutputStream target/classes src
#evoCoverage Compress_4 org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream target/classes src
#evoCoverage Compress_4 org.apache.commons.compress.archivers.tar.TarArchiveOutputStream target/classes src
#evoCoverage Compress_4 org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream target/classes src
#evoCoverage Compress_4 org.apache.commons.compress.changes.ChangeSetPerformer target/classes src
# evoCoverage Compress_5 org.apache.commons.compress.archivers.zip.ZipArchiveInputStream target/classes src

evoCoverage Csv_1 org.apache.commons.csv.ExtendedBufferedReader target/classes src/main/java
evoCoverage Csv_2 org.apache.commons.csv.CSVRecord target/classes src/main/java
evoCoverage Csv_3 org.apache.commons.csv.Lexer target/classes src/main/java
evoCoverage Csv_4 org.apache.commons.csv.CSVParser target/classes src/main/java
evoCoverage Csv_5 org.apache.commons.csv.CSVPrinter target/classes src/main/java

evoCoverage Gson_1 com.google.gson.TypeInfoFactory target/classes gson/src/main/java
# evoCoverage Gson_2 com.google.gson.internal.bind.TypeAdapters target/classes gson/src/main/java
evoCoverage Gson_3 com.google.gson.internal.ConstructorConstructor target/classes gson/src/main/java
#evoCoverage Gson_4 com.google.gson.stream.JsonReader target/classes gson/src/main/java
#evoCoverage Gson_4 com.google.gson.stream.JsonWriter target/classes gson/src/main/java
# evoCoverage Gson_5 com.google.gson.internal.bind.util.ISO8601Utils target/classes gson/src/main/java

#evoCoverage JacksonCore_1 com.fasterxml.jackson.core.io.NumberInput target/classes src/main/java
#evoCoverage JacksonCore_1 com.fasterxml.jackson.core.util.TextBuffer target/classes src/main/java
#evoCoverage JacksonCore_2 com.fasterxml.jackson.core.json.ReaderBasedJsonParser target/classes src/main/java
#evoCoverage JacksonCore_2 com.fasterxml.jackson.core.json.UTF8StreamJsonParser target/classes src/main/java
# evoCoverage JacksonCore_3 com.fasterxml.jackson.core.json.UTF8StreamJsonParser target/classes src/main/java
evoCoverage JacksonCore_4 com.fasterxml.jackson.core.util.TextBuffer target/classes src/main/java
evoCoverage JacksonCore_5 com.fasterxml.jackson.core.JsonPointer target/classes src/main/java

# evoCoverage JacksonDatabind_1 com.fasterxml.jackson.databind.ser.BeanPropertyWriter target/classes src/main/java
# evoCoverage JacksonDatabind_2 com.fasterxml.jackson.databind.util.TokenBuffer target/classes src/main/java
# evoCoverage JacksonDatabind_3 com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer target/classes src/main/java
## evoCoverage JacksonDatabind_4 com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer target/classes src/main/java
evoCoverage JacksonDatabind_5 com.fasterxml.jackson.databind.introspect.AnnotatedClass target/classes src/main/java

##evoCoverage JacksonXml_1 com.fasterxml.jackson.dataformat.xml.deser.FromXmlParser target/classes src/main/java
evoCoverage JacksonXml_2 com.fasterxml.jackson.dataformat.xml.deser.XmlTokenStream target/classes src/main/java
##evoCoverage JacksonXml_3 com.fasterxml.jackson.dataformat.xml.deser.FromXmlParser target/classes src/main/java
##evoCoverage JacksonXml_4 com.fasterxml.jackson.dataformat.xml.ser.XmlSerializerProvider target/classes src/main/java
##evoCoverage JacksonXml_5 com.fasterxml.jackson.dataformat.xml.ser.XmlSerializerProvider target/classes src/main/java

# evoCoverage Jsoup_1 org.jsoup.nodes.Document target/classes src/main/java
# evoCoverage Jsoup_2 org.jsoup.parser.Parser target/classes src/main/java
#evoCoverage Jsoup_3 org.jsoup.nodes.Element target/classes src/main/java
#evoCoverage Jsoup_3 org.jsoup.parser.Parser target/classes src/main/java
#evoCoverage Jsoup_3 org.jsoup.parser.Tag target/classes src/main/java
evoCoverage Jsoup_4 org.jsoup.nodes.Entities target/classes src/main/java
evoCoverage Jsoup_5 org.jsoup.parser.Parser target/classes src/main/java

#evoCoverage JxPath_1 org.apache.commons.jxpath.ri.model.dom.DOMNodePointer target/classes src/java
#evoCoverage JxPath_1 org.apache.commons.jxpath.ri.model.jdom.JDOMNodePointer target/classes src/java
# evoCoverage JxPath_2 org.apache.commons.jxpath.ri.compiler.Expression target/classes src/java
evoCoverage JxPath_3 org.apache.commons.jxpath.ri.model.beans.NullPropertyPointer target/classes src/java
#evoCoverage JxPath_4 org.apache.commons.jxpath.ri.model.dom.DOMNodePointer target/classes src/java
#evoCoverage JxPath_4 org.apache.commons.jxpath.ri.model.jdom.JDOMNodePointer target/classes src/java
evoCoverage JxPath_5 org.apache.commons.jxpath.ri.model.NodePointer target/classes src/java

evoCoverage Lang_1 org.apache.commons.lang3.math.NumberUtils target/classes src/main/java
#evoCoverage Lang_2 "" target/classes src/main/java
# evoCoverage Lang_3 org.apache.commons.lang3.math.NumberUtils target/classes src/main/java
evoCoverage Lang_4 org.apache.commons.lang3.text.translate.LookupTranslator target/classes src/main/java
# evoCoverage Lang_5 org.apache.commons.lang3.LocaleUtils target/classes src/main/java

#evoCoverage Math_1 org.apache.commons.math3.fraction.BigFraction target/classes src/main/java
#evoCoverage Math_1 org.apache.commons.math3.fraction.Fraction target/classes src/main/java
evoCoverage Math_2 org.apache.commons.math3.distribution.HypergeometricDistribution target/classes src/main/java
# evoCoverage Math_3 org.apache.commons.math3.util.MathArrays target/classes src/main/java
#evoCoverage Math_4 org.apache.commons.math3.geometry.euclidean.threed.SubLine target/classes src/main/java
#evoCoverage Math_4 org.apache.commons.math3.geometry.euclidean.twod.SubLine target/classes src/main/java
evoCoverage Math_5 org.apache.commons.math3.complex.Complex target/classes src/main/java

##evoCoverage Mockito_1 org.mockito.internal.invocation.InvocationMatcher build/classes/main/ src
evoCoverage Mockito_2 org.mockito.internal.util.Timer build/classes/main/ src
##evoCoverage Mockito_3 org.mockito.internal.invocation.InvocationMatcher build/classes/main/ src
##evoCoverage Mockito_4 org.mockito.exceptions.Reporter build/classes/main/ src
##evoCoverage Mockito_5 org.mockito.internal.verification.VerificationOverTimeImpl build/classes/main/ src

#evoCoverage Time_1 org.joda.time.field.UnsupportedDurationField target/classes src/main/java
#evoCoverage Time_1 org.joda.time.Partial target/classes src/main/java
#evoCoverage Time_2 org.joda.time.field.UnsupportedDurationField target/classes src/main/java
#evoCoverage Time_2 org.joda.time.Partial target/classes src/main/java
##evoCoverage Time_3 org.joda.time.MutableDateTime target/classes src/main/java
##evoCoverage Time_4 org.joda.time.Partial target/classes src/main/java
##evoCoverage Time_5 org.joda.time.Period target/classes src/main/java