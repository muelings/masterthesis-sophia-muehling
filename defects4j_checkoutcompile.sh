#!/bin/sh

checkoutandcompile(){
	subject=$1
	for i in {1..10}
	do
		defects4j checkout -p $subject -v $i"b" -w /tmp/$subject"_"$i"_buggy"
		defects4j checkout -p $subject -v $i"f" -w /tmp/$subject"_"$i"_fixed"
		defects4j compile -w /tmp/$subject"_"$i"_buggy"
		defects4j compile -w /tmp/$subject"_"$i"_fixed"
	done;
}

checkoutandcompile Chart
checkoutandcompile Closure
checkoutandcompile Lang
checkoutandcompile Math
checkoutandcompile Mockito
checkoutandcompile Time
checkoutandcompile Codec
checkoutandcompile Compress
checkoutandcompile Csv
checkoutandcompile Gson
checkoutandcompile JacksonCore
checkoutandcompile JacksonDatabind
checkoutandcompile Jsoup
checkoutandcompile JxPath